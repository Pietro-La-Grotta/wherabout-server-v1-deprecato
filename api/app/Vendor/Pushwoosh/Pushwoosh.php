<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

// 
// https://www.pushwoosh.com/programming-push-notification/pushwoosh-push-notification-remote-api/
//

/**
 * Description of Pushwoosh
 * Factory per le notifiche push
 * @author pietro@studioleaves.com
 */
class Pushwoosh {
    
    /**
     * Costruttore privato
     */
    private function Pushwoosh(){}
    
    /**
     * Seleziona l'istanza di pushwoosh
     * @param function $F funzione di inclusione degli script
     * @return \Pushwoosh
     */
    public static function create($F){
        $F();
        return new Pushwoosh();
    }
    
    /*  ESEMPIO DI CHIAMATA A CREATE IN JOOMLA!:
     * 
     * $this->pushwosh = Pushwoosh::create(function(){
            JLoader::register("IPushParameter", "serveco/Pushwoosh/IPushParameter.php");
            JLoader::register("CreateParam", "serveco/Pushwoosh/CreateParam.php");
            JLoader::register("CustomData", "serveco/Pushwoosh/CustomData.php");
            JLoader::register("LanguageUpdateParam", "serveco/Pushwoosh/LanguageUpdateParam.php");
         });
     */
    
    // ESEMPIO DI CHIAMATA A CREATE IN CAKEPHP
    
    /*
     *  App::import('Vendor', 'Pushwoosh/Pushwoosh');
        
        $pushwoosh = Pushwoosh::create(function(){
            App::import('Vendor', 'Pushwoosh/IPushParameter');
            App::import('Vendor', 'Pushwoosh/CreateParam');
            App::import('Vendor', 'Pushwoosh/CustomData');
            App::import('Vendor', 'Pushwoosh/LanguageUpdateParam');
        });
     */
    
    
    /**
     * Url di connessione
     * @var string
     */
    private static $url = 'https://cp.pushwoosh.com/json/1.3/';
    
    /**
     * Risposta di pushwoosh
     * @var string
     */
    private $request, $response, $error, $info;
    
    /**
     * Modalità debug se true registra i dati scambiati con push woosh sul db;
     * @var bool
     */
    private $debug = false;
    
    /**
     * Imposta la modalità debug
     */
    public function setDebugMode(){
        $this->debug = true;
    }
    
    /**
     * IOS Background available notification
     * Se true aggiunge al json da inviare a pushwoosh indicazione perchè sui dispositivi ios la notifica possa essere utilizzata da servizi di background
     * @var boolean
     */
    private $iosBGAvailable = false;
    
    /**
     * Fa si che le notifiche ricevute da device ios possano essere utilizzate anche da servizi in background
     */
    public function setIosBGAvailable() {
        $this->iosBGAvailable = true;
    }

        
    /**
     * Effettua la richiesta a pushwoosh
     * @param string $method web service da richiamare
     * @param array $data array associativo che definisce i dati da passare a pushwoosh
     */
    private function pushcall($method, &$data){
        
        $url = self::$url.$method;
        $this->request = $request = json_encode(array('request' => $data));
   
        $ch = curl_init($url);
        
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        
//        curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
//        curl_setopt($ch, CURLOPT_TIMEOUT_MS, 1);
        
        curl_setopt($ch, CURLOPT_POSTFIELDS, $request);

        $this->response = curl_exec($ch);
        
        $this->error = curl_error($ch);
        $this->info = curl_getinfo($ch);
        
        curl_close($ch);
        
        if ($this->debug) $this->saveRequest();
    }
    
    /**
     * Selettore della richiesta inviata
     * @return string
     */
    public function getRequest() {
        return $this->request;
    }

    /**
     * Selettore della risposta ottenuta
     * @return string
     */
    public function getResponse() {
        return $this->response;
    }

    /**
     * Selettore dell' eventuale errore
     * @return string
     */
    public function getError() {
        return $this->error;
    }

    /**
     * Selettore delleinformazioni associate alla richiesta
     * @return string
     */
    public function getInfo() {
        return $this->info;
    }

    /**
     * Effetua nei confronti di Pushwoosh una richiesta per la consegna di una notifica push ad una lista di dispositivi
     * @param IPushParameter $params istanza di CreateParam;
     * @return boolean false se l'isanza di IPushParameter in input non è di tipo CreateParam
     */
    public function createMessage(IPushParameter &$params){
        
        if (!($params instanceof CreateParam)){
            $this->error = "params 0 must be instance of CreateParam";
            return false;
        }
        
        if (empty($params->receivers)){
            $this->error = "Empty list of receivers";
            return false;
        }
        
        $request = array('application' => $params->application,
                         'auth' => $params->auth,
                         'notifications' => array(array('send_date' => $params->timeToSend,
                                                        'ignore_user_timezone' => false,
                                                        'content' => $params->shortContent,
                                                        'ios_badges' => "+1",
                                                        'data' => (array)$params->customData
                                                        )));
        if($this->iosBGAvailable){
           $request['notifications'][0]['ios_root_params'] = array('aps' => array('content-available' => "1"));
        }
        
        $request['notifications'][0]['devices'] = $params->receivers;
        
        $this->pushcall('createMessage', $request);
    }
    
    /**
     * Invia a pushwoosh una richiesrta di aggiornamento della lingua impostata per un dispositivo
     * @param IPushParameter $params
     */
    public function updateLanguage(IPushParameter &$params){
        
        if (!($params instanceof LanguageUpdateParam)){
            $this->error = "params 0 must be instance of LanguageUpdateParam";
            return false;
        }
        
        $request = array('application' => $params->application,
                         'push_token' => $params->pushToken,
                         'language' => $params->language,
                         'hwid' => $params->deviceCode,
                         'device_type' => $params->deviceType);
        
        $this->pushcall('registerDevice', $request);
    }
    
    /**
     * Salvataggio delle informazioni scambiatre con pushwoosh nella tabella curl_requests 
     */
    private function saveRequest(){
        
        App::uses('CurlRequest', 'Model');
        $model = new CurlRequest();
				
        $model->save(array('request' => HTMLDecoder::encode(json_encode($this->request)),   
                           'response' => HTMLDecoder::encode(json_encode($this->response)), 
                           'info' => HTMLDecoder::encode(json_encode($this->info)), 
                           'error' => HTMLDecoder::encode(json_encode($this->error))));
        
        $model->clear();

        unset($model);
    }
}

?>
