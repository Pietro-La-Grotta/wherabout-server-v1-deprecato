/* 
 * Script usati per la lista degli eventi
 */

/**
 * Inizializzazione della tabella per la lista delle prenotazioni per un oggetto
 */
function init_data_table(){
    jQuery(document).ready(function() {
        
       var table = jQuery('#studio_leaves').DataTable({
           
            "lengthMenu": [[100, 200, 400, -1], [100, 200, 400, "All"]],
            "aaSorting": [[0,'desc']],
            "paging":   true,
            "aoColumnDefs": [{ 'bSortable': false, 
                               'aTargets': [5]
                           }]
        });
       
        table.column(0).visible(false);
        
         // selezione multipla delle righe
       jQuery('#studio_leaves tbody').on( 'click', 'tr', function (){
           jQuery(this).toggleClass('selected');
       });
       
       // Conferma delle prenotazioni selezionate
       
       jQuery('#confirm_button').click( function () {
            var x = table.rows('.selected').data().length;
            
            if (x < 1){
                alert('E\' necessario selezionare le prenotazioni da confermare');
                return;
            }
            
            if (confirm('Si è certi di volere confermare le prenotazioni selezionate?')){
                jQuery('#books_to_confirm').val(getSelectedList());
                document.confirm_booking_form.submit();  
            }
        });
        
        // Rimozione delle conferme
       
       jQuery('#remove_button').click( function () {
            var x = table.rows('.selected').data().length;
            
            if (x < 1){
                alert('E\' necessario selezionare le prenotazioni a cui rimuovere la conferma');
                return;
            }
            
            if (confirm('Si è certi di volere rimuovere le conferme alle  gli eventi selezionati?')){
                jQuery('#confirmation_to_remove').val(getSelectedList());
                document.remove_confirm_form.submit();  
            }
        });
        
        // Eliminazione delle prenotazioni
       
       jQuery('#delete_button').click( function () {
            var x = table.rows('.selected').data().length;
            
            if (x < 1){
                alert('E\' necessario selezionare le prenotazioni da eliminare');
                return;
            }
            
            if (confirm('Si è certi di volere eliminare le prenotazioni da eliminare?\nL\'operazione non è reversibile.')){
                jQuery('#delete_booking').val(getSelectedList());
                document.delete_booking_form.submit();  
            }
        });
       
    });
}

init_data_table();

var comodo;

/**
 * Genera la lista degli id delle righe selezionate
 * @returns {@exp;JSON@call;stringify} lista in formato json trasformata in stringa
 */
function getSelectedList(){
    comodo = [];
    jQuery('.selected').each(function (){
        comodo[comodo.length] = jQuery(this).attr("id");
    });
    return JSON.stringify(comodo);
}