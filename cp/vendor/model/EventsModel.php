<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of EventsModel
 * Accede ai dati relativi agli eventi
 * @author pietro@studioleaves.com
 */
class EventsModel {
    
    /**
     *
     * @var type filtri di ricerca
     */
    private $filters = array();
    
    /**
     * Accede alle variabili di sessione e carica i filtri di ricerca precedentemente preimpostati
     */
    public function loadFilter(){
        // ritrova tra le variabili di sessione il json contenente i parametri di ricerca
        $sp = json_decode(JFactory::getSession()->get('event_search_params'), true);
        if (!isset($sp)) return;
        
        // filtri da oggetto in sessione a attributo $this->filters
        
        $db = JFactory::getDbo();
        
        // stato dell'evento
        if (isset($sp['status'])){
            $this->filters['activated ='] = $db->q($sp['status']);
        }
        
        // titolo dell'evento
        if (isset($sp['title'])){
            $this->filters['title LIKE'] = $db->q('%'.HTMLDecoder::encode($sp['title']).'%');
        }
        
        if (isset($sp['supercategoria'])){  
            
            // in questo caso la ricerca è effettuata su una specifica sottocategoria
            
            if (isset($sp['categoria'])){
                $this->filters['idcategory ='] = $sp['categoria'];
            }
            else{
                
                // altrimenti si fa la ricerca su una supercategoria e quindi anche su tutte le sottocategorie
                
                $id = $sp['supercategoria'];
                
                $qo = $db->getQuery(true);

                $qo->select($db->qn('id'))
                   ->from($db->qn('categories'))
                   ->where($db->qn('idsupercategory').' = '.$db->q($id));

                $db->setQuery($qo);
                $results = $db->loadObjectList();

                $in = "('".$id."'";
                foreach ($results as $sid) 
                    $in .= ', '.$db->q($sid->id);

                $in .= ")";

                $this->filters['idcategory IN'] = $in;
            }
            
        }
            
        // limite inferiore dell'intervallo di tempo da considerare
        if (isset($sp['dateinf'])){
            $this->filters['date >= '] = $db->q(HTMLDecoder::mysqDate($sp['dateinf'])); 
        }
            
        // limite superiore dell'intervallo di tempo da considerare
        if (isset($sp['datesup'])){
            $this->filters['date <= '] = $db->q(HTMLDecoder::mysqDate($sp['datesup'])); 
        }
            
        // sesso
        if (isset($sp['sex'])){
            $this->filters['sex = '] = $db->q($sp['sex']); 
        }
            
        // scope
        if (isset($sp['scope'])){
            $this->filters['scope = '] = $db->q($sp['scope']); 
        }
            
        // description
        if (isset($sp['description'])){

            $tokens = explode(',', $sp['description']);
            $or = '(';
            $first = true;

            foreach ($tokens as $t){

                if (!$first) $or .= ' OR';
                $first = false;

                $or .= ' description LIKE '.$db->q('%'.HTMLDecoder::encode(trim($t)).'%');
            }

            $or .= ')';

            $this->filters[$or.' '] = '';
        }

        // città
        if (isset($sp['town'])){
            $this->filters['city LIKE '] = $db->q('%'.HTMLDecoder::encode($sp['town']).'%'); 
        }

        // limite inferiore su numero di partecipanti
        if (isset($sp['minpartecipant'])){
            $this->filters['sitcurrent >= '] = $db->q($sp['minpartecipant']); 
        }

        // limite superiore sul numero di partecipanti
        if (isset($sp['maxpartecipant'])){
            $this->filters['sitcurrent <= '] = $db->q($sp['maxpartecipant']); 
        }

        // limite inferiore sul numero di posti disponibili
        if (isset($sp['minplace'])){
            $this->filters['sittotal <= '] = $db->q($sp['minplace']); 
        }

        // limite superiore sul numero di posti disponibili
        if (isset($sp['maxplace'])){
            $this->filters['sittotal >= '] = $db->q($sp['maxplace']); 
        }

        // limite inferiore sull'età minima
        if (isset($sp['minagefrom'])){
            $this->filters['agemin >= '] = $db->q($sp['minagefrom']); 
        }

        // limite superiore sull'eta minima
        if (isset($sp['maxagefrom'])){
            $this->filters['agemin <= '] = $db->q($sp['maxagefrom']); 
        }

        // limite inferiore sull'età massima
        if (isset($sp['minageto'])){
            $this->filters['agemax >= '] = $db->q($sp['minageto']); 
        }

        // limite superiore sull'età massima 
        if (isset($sp['maxageto'])){
            $this->filters['agemax <= '] = $db->q($sp['maxageto']); 
        }
    }
    
    /**
     * Selezione lista eventi
     * 
     * create view cp_events_list as
       select e.*,
              u.name, u.surname,
	      c.name as c_name
       from events e
       join users u on e.iduser = u.iduser
       join categories c on e.idcategory = c.id
       order by e.idevent desc; 
     *	   
     * @return object list
     */
    public function select(){
        $db = JFactory::getDBO();
        
        $query = "select * from cp_events_list";
        
        if (count($this->filters) > 0){
            $first = true;
            
            foreach ($this->filters as $key => $value){
                
                if ($first){
                    $query .= ' WHERE';
                    $first = false;
                }
                else{
                    $query .= ' AND';
                }
                
                $query .= ' '.$key.' '.$value;
            }
        }

        $db->setQuery($query); 
        return $db->loadObjectList();
    }
    
    /**
     * Seleziona un evento data la chiave primaria dalla vista cp_events_list 
     * @param int $idevent pk evento
     * @return object
     */
    public function getById($idevent){
        $query = "select * from cp_events_list where idevent = ".HTMLDecoder::encode($idevent);
        $connection = JFactory::getDBO();
        $connection->setQuery($query); 
        $events = $connection->loadObjectList();
        return $events[0];
    }
    
    /**
     * Seleziona la lista delle categorie
     * @return objects list
     */
    public function getCategories(){
        $query = "SELECT `id`, `name` FROM `categories` ORDER BY `name`";
        $connection = JFactory::getDBO();
        $connection->setQuery($query); 
        return $connection->loadObjectList();
    }
    
    /**
     * Effettua il salvataggio dei dati associati ad un evento
     * Sarebbe stato più giusto fare un controller :)
     */
    public function save(){
        // incogruenze età e partecipanti
        if ($_POST['sittotal'] < $_POST['sitcurrentghost']){
            JFactory::getApplication()->redirect(JUri::base().'?show=edit&item='.JRequest::getVar('item'), 'Il numero dei posti disponibili è inferiore al numero dei partecipanti', 'error');
        }
        
        if ($_POST['agemax'] < $_POST['agemin']){
            JFactory::getApplication()->redirect(JUri::base().'?show=edit&item='.JRequest::getVar('item'), 'Età massima per partecipare inferiore a età minima', 'error');
        }
        
        $db = JFactory::getDBO();
        
        $fields = array();

        // update image
        if (strlen($_FILES['image']['name']) > 0){
            JLoader::register('ImageManager', 'vendor/tools/ImageManager.php');
            $imageManager = new ImageManager();
            $imageManager->target = EVENTS;
            $imageManager->input_form_name = 'image';
            
            // create a new query object to select old image
            $qo = $db->getQuery(true);
            $qo->select($db->qn('img'))
               ->from($db->qn('events'))
               ->where($db->qn('idevent').' = '.$db->q(JRequest::getVar('item')));
            
            $db->setQuery($qo);
            $results = $db->loadObjectList();
            $oldimage = $results[0];
            
            if($imageManager->save()){
                $fields['img'] = Utils::eventsImagePath().$imageManager->getUploadedFileName();
                $imageManager->delete($oldimage->img);
            }
            else{
                JFactory::getApplication()->redirect(JUri::base().'?show=edit&item='.JRequest::getVar('item'), $imageManager->messageError(), 'error');
                return;
            }
        }
        
        $fields['activated'] = $_POST['status'];
        $fields['title'] = HTMLDecoder::encode(strtoupper($_POST['title']));
        $fields['idcategory'] = ($_POST['categoria'] != -1) ? $_POST['categoria'] : $_POST['supercategoria'];
        $fields['date'] = HTMLDecoder::mysqDate($_POST['date']);
        $fields['hour'] = $_POST['hour'].':00';
        $fields['sex'] = $_POST['sex'];
        $fields['scope'] = $_POST['scope'];
        $fields['description'] = HTMLDecoder::encode($_POST['description']);
        $fields['city'] = HTMLDecoder::encode($_POST['town']);
        $fields['address'] = HTMLDecoder::encode($_POST['address']);
        $fields['agemin'] = $_POST['agemin'];
        $fields['agemax'] = $_POST['agemax'];
        $fields['sittotal'] = $_POST['sittotal'];
        $fields['latitude'] = $_POST['latitude'];
        $fields['longitude'] = $_POST['longitude'];
        
        
        $query = 'UPDATE events SET';
        
        $first = true;
        
        foreach ($fields as $f => $v){
            if (!$first) $query .= ',';
            $first = false;
            $query .= " `".$f."` = '".$v."'";
        }
        
        $query .= ' WHERE idevent = '.JRequest::getVar('item');
        
        //var_dump($query); die();
        
        $db->setQuery($query);
        $db->query();
    }
    
    /**
     * Modellazione della lista delle categorie estratta da chronoforms
     * @param array di array associativi corrispondenti alle tuple $rawdata
     * @return 
     */
    public function categorylist(&$raw){
         $tree = array(); 

        // fillin delle categorie
        foreach($raw as &$r){
            if (!isset($r['idsupercategory'])){
                $tree[$r['id']] = array('sup' => $r, 'sub' => array());
            }
        }
        
        // fillin delle sottocategorie
        foreach($raw as &$r){
            if (isset($r['idsupercategory'])){
                $tree[$r['idsupercategory']]['sub'][] = $r;
            }
        }
        
        $response = array();
        
        // generazione della risposta
        foreach ($tree as &$t){
            $response[] = array('id' => 'supc_'.$t['sup']['id'], 'name' => $t['sup']['name']);
            
            $sc = $t['sup']['id'];
            
            foreach ($t['sub'] as &$s){
                $response[] = array('id' => $s['id'].'_sc_'.$sc, 'name' => $s['name']);
            }
        }
        
        $raw = $response;
    }
    
    /**
     * Attivazione di una lista di eventi
     * @param string $events lista delle chievi primarie degli eventi da attivare
     *        es : "('0', '1', '47')"
     */
    public function activeEvents(&$events){
        $db = JFactory::getDbo();
        $qo = $db->getQuery(true);
        
        $qo->update($db->qn('events'))
           ->set(array($db->qn('activated').'='.$db->q(1)))
           ->where(array($db->qn('idevent').' IN '.$events));
        
        $db->setQuery($qo);
        $db->query();
    }
    
    /**
     * Disattivazione di una lista di eventi
     * @param string $events lista delle chievi primarie degli eventi da disattivare
     *        es : "('0', '1', '47')"
     */
    public function disableEvents(&$events){
        $db = JFactory::getDbo();
        $qo = $db->getQuery(true);
        
        $qo->update($db->qn('events'))
           ->set(array($db->qn('activated').'='.$db->q(0)))
           ->where(array($db->qn('idevent').' IN '.$events));
        
        $db->setQuery($qo);
        $db->query();
    }
    
    /**
     * Seleziona dalla vista cp_partecipants_list tutte le prenotazioni ad un evento
     * DDL Vista:
     * -- vista per la selezione della lista di utenti che partecipano ad un evento
          drop view if exists cp_partecipants_list;

          create view cp_partecipants_list as
          select e.*, u.email, u.name, u.surname
          from partecipates e join users u on e.iduser = u.iduser;
     * 
     * @param int $idevent pk evento
     * @return object list 
     */
    public function partecipates($idevent){
        $db = JFactory::getDbo();
        $qo = $db->getQuery(true);
        
        $qo->select('*')
           ->from($db->qn('cp_partecipants_list'))
           ->where($db->qn('idevent').'='.$db->q($idevent))
           ->order(array($db->qn('surname'), $db->qn('name')));
        
        $db->setQuery($qo);
        
        return $db->loadObjectList();
    }
    
    /**
     * Conferma di una lista di prenotazioni
     * @param string $booking lista delle pk delle prenotazioni da confermare in formato json 
     *        es : "['0', '1', '47']"
     */
    public function confirmBooking($idevent, $booking){
        $db = JFactory::getDbo();
        
        $booking = str_replace('"', '', $booking);
        $q = "CALL confirm_bookings(".$db->q($idevent).", ".$db->q(substr($booking, 1, strlen($booking) - 2)).")";
        
        $db->setQuery($q);
        $db->query();
    }
    
    /**
     * Rimozioni della conferma ad una lista di prenotazioni
     * @param string $bookings lista delle pk delle prenotazioni a cui togliere la conferma
     *        es : "('0', '1', '47')"
     */
    public function removeConfirmation($idevent, $booking){
        $db = JFactory::getDbo();
        
        $booking = str_replace('"', '', $booking);
        $q = "CALL remove_booking_confirmation(".$db->q($idevent).", '".substr($booking, 1, strlen($booking) - 2)."')";
        
        $db->setQuery($q);
        $db->query();
    }
}

?>
