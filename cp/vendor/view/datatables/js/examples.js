// questo file non viene caricato da nessuna pagina

var comodo;

/**
 * Genera la lista degli id delle righe selezionate
 * @returns {@exp;JSON@call;stringify} lista in formato json trasformata in stringa
 */
function getSelectedList(){
    comodo = [];
    jQuery('.selected').each(function (){
        comodo[comodo.length] = jQuery(this).attr("id");
    });
    return JSON.stringify(comodo);
}

/**
 * Inizializzazione della tabella per la lista delle prenotazioni per un oggetto
 */
function wall_reservation(){
    jQuery(document).ready(function() {
        
       var table = jQuery('#serveco_table').DataTable({
           
            "aaSorting": [[0,'desc']],
            "paging":   true,
        });
       
        table.column(0).visible(false);
    });
}


/**
 * Inizializzazione della tabella per la lista degli oggetti in bacheca
 * Inizializzazione strumenti selezione multipla ed elimina
 */
function wall_objects(){
    jQuery(document).ready(function() {
        
       var table = jQuery('#serveco_table').DataTable({
           
            "lengthMenu": [[50, 100, 300, -1], [50, 100, 300, "All"]],
            "aaSorting": [[0,'desc']],
            "paging":   true,
            "aoColumnDefs": [{ 'bSortable': false, 
                               'aTargets': [6]
                           }]
        });
       
        table.column(0).visible(false);
       
        // selezione multipla delle righe
       jQuery('#serveco_table tbody').on( 'click', 'tr', function (){
           jQuery(this).toggleClass('selected');
       });
       
       // Eliminazione dei template di notifica
       
       jQuery('#button_d').click( function () {
            var x = table.rows('.selected').data().length;
            
            if (x < 1){
                alert('Nessuna riga selezionata');
                return;
            }
            
            if (confirm('Si è certi di volere eliminare gli oggetti selezionati?')){
                jQuery('#delete_wall_input').val(getSelectedList());
                document.delete_wall.submit();  
            }
        });
    });
}

/**
 * Inizializzazione della lista delle notifiche aperiodiche innviate
 * Inizializzazione strumenti elimina e nuova
 * @returns {undefined}
 */
function wall_category_list(){
    jQuery(document).ready(function() {
        
       var table = jQuery('#serveco_table').DataTable({
           
            "aoColumnDefs": [{ 'bSortable': false, 
                               'aTargets': [4]
                           }]
        });
        
         // selezione multipla delle righe
       jQuery('#serveco_table tbody').on( 'click', 'tr', function (){
           jQuery(this).toggleClass('selected');
       });
       
       // Eliminazione dei template di notifica
       
       jQuery('#button_d').click( function () {
            var x = table.rows('.selected').data().length;
            
            if (x < 1){
                alert('Nessuna riga selezionata');
                return;
            }
            
            if (confirm('Si è certi di volere eliminare le categorie selezionate?')){
                jQuery('#delete_cw_input').val(getSelectedList());
                document.delete_cw_form.submit();  
            }
        });
       
         // nuova categoria
        
        jQuery('#button_n').click( function () {
            document.new_cw_form.submit();  
        }); 
    });
}

/**
 * Inizializzazione della lista di template di notifica aperiodica
 * Inizializzazione strumenti elimina e nuova
 * @returns {undefined}
 */
function imagelist(){
    jQuery(document).ready(function() {
        
       var table = jQuery('#serveco_table').DataTable({
            
            "aaSorting": [[0,'desc']],
            "paging":   true,
            "aoColumnDefs": [{ 'bSortable': false, 
                               'aTargets': [5]
                           }]
        });
       
        table.column(0).visible(false);
       
        // selezione multipla delle righe
       jQuery('#serveco_table tbody').on( 'click', 'tr', function (){
           jQuery(this).toggleClass('selected');
       });
       
       // Eliminazione dei template di notifica
       
       jQuery('#button_d').click( function () {
            var x = table.rows('.selected').data().length;
            
            if (x < 1){
                alert('Nessuna riga selezionata');
                return;
            }
            
            if (confirm('Si è certi di volere eliminare le immagini selezionate?')){
                jQuery('#delete_image_input').val(getSelectedList());
                document.delete_image_form.submit();  
            }
        });
    });
}


/**
 * Inizializzazione della lista delle notifiche aperiodiche innviate
 * Inizializzazione strumenti elimina e nuova
 * @returns {undefined}
 */
function dashboard_statistics(){
    jQuery(document).ready(function() {
        
       var table = jQuery('#serveco_table').DataTable({
             "paging":   false,
             "ordering": false,
             "info":     false
            
        });
       
        table.column(0).visible(false);
        
        jQuery('#button_n').click( function () {
            
            if (confirm('L\'aggiornamento delle statistiche potrebbe richiedere del tempo.\nProseguire?')){
                document.update_statistics_form.submit();  
            }
        }); 
    });
    
    
}

/**
 * Inizializzazione della lista delle notifiche aperiodiche innviate
 * Inizializzazione strumenti elimina e nuova
 * @returns {undefined}
 */
function notisendedlist(){
    jQuery(document).ready(function() {
        
       var table = jQuery('#serveco_table').DataTable({
            
            "aaSorting": [[5,'desc']],
            "paging":   true,
            "aoColumnDefs": [{ 'bSortable': false, 
                               'aTargets': [5]
                           }]
        });
       
        table.column(5).visible(false); 
    });
}



/**
 * Inizializzazione della lista di template di notifica aperiodica
 * Inizializzazione strumenti elimina e nuova
 * @returns {undefined}
 */
function notitemplatelist(){
    jQuery(document).ready(function() {
        
       var table = jQuery('#serveco_table').DataTable({
            
            "aaSorting": [[5,'desc']],
            "paging":   true,
            "aoColumnDefs": [{ 'bSortable': false, 
                               'aTargets': [1, 2, 3, 4, 5]
                           }]
        });
       
        table.column(5).visible(false);
       
        // selezione multipla delle righe
       jQuery('#serveco_table tbody').on( 'click', 'tr', function (){
           jQuery(this).toggleClass('selected');
       });
       
       // Eliminazione dei template di notifica
       
       jQuery('#button_d').click( function () {
            var x = table.rows('.selected').data().length;
            
            if (x < 1){
                alert('Nessuna riga selezionata');
                return;
            }
            
            if (confirm('Si è certi di volere eliminare i modelli di notifica selezionati?')){
                jQuery('#delete_notification_input').val(getSelectedList());
                document.delete_notification_form.submit();  
            }
        });
        
        // Nuova tipologia selezionabile
       
        jQuery('#button_n').click( function () {
            
            jQuery('#new_notification_input').val(jQuery('.selected').attr('id'));
            document.new_notification_form.submit();  
            
        }); 
    });
}

/**
 * Inizializzazione della lista di articoli avvertenze e modalità d'uso
 */
function compostinfolist(){
    jQuery(document).ready(function() {
        
       var table = jQuery('#serveco_table').DataTable({
            
            "aoColumnDefs": [{ 'bSortable': false, 
                               'aTargets': [3]
                           }]
        });
    });
}

/**
 * Inizializzazione della lista di articoli avvertenze e modalità d'uso
 */
function avvertenzelist(){
    jQuery(document).ready(function() {
        
       var table = jQuery('#serveco_table').DataTable({
            
            "aoColumnDefs": [{ 'bSortable': false, 
                               'aTargets': [3,4]
                           }]
        });
    });
}

/**
 * Inizializzazione della lista di risposte possibili per una domanda del questionario
 * Inizializzazione strumenti elimina e nuova
 * @returns {undefined}
 */
function ecoadvicelist(){
    jQuery(document).ready(function() {
        
       var table = jQuery('#serveco_table').DataTable({
            
            "aaSorting": [[3,'asc']],
            "paging":   false,
            "aoColumnDefs": [{ 'bSortable': false, 
                               'aTargets': [0, 2, 3]
                           }]
        });
       
        table.column(3).visible(false);
       
        // selezione multipla delle righe
       jQuery('#serveco_table tbody').on( 'click', 'tr', function (){
           jQuery(this).toggleClass('selected');
       });
       
       // Eliminazione delle comunicazioni
       
       jQuery('#button_d').click( function () {
            var x = table.rows('.selected').data().length;
            
            if (x < 1){
                alert('Nessuna riga selezionata');
                return;
            }
            
            if (confirm('Si è certi di volere eliminare gli eco-consigli selezionati?')){
                jQuery('#delete_advice_input').val(getSelectedList());
                document.delete_advice_form.submit();  
            }
        });
        
        // Nuova tipologia selezionabile
       
        jQuery('#button_n').click( function () {
            
            jQuery('#new_advice_input').val(jQuery('.selected').attr('id'));
            document.new_advice_form.submit();  
            
        }); 
    });
}

/**
 * Inizializzazione della lista di risposte possibili per una domanda del questionario
 * Inizializzazione strumenti elimina e nuova
 * @returns {undefined}
 */
function gameslist(){
    jQuery(document).ready(function() {
        
       var table = jQuery('#serveco_table').DataTable({
            
            "aaSorting": [[4,'asc']],
            "paging":   false,
            "aoColumnDefs": [{ 'bSortable': false, 
                               'aTargets': [0, 2, 3, 4]
                           }]
        });
       
        table.column(4).visible(false);
       
        // selezione multipla delle righe
       jQuery('#serveco_table tbody').on( 'click', 'tr', function (){
           jQuery(this).toggleClass('selected');
       });
       
       // Eliminazione delle comunicazioni
       
       jQuery('#button_d').click( function () {
            var x = table.rows('.selected').data().length;
            
            if (x < 1){
                alert('Nessuna riga selezionata');
                return;
            }
            
            if (confirm('Si è certi di volere eliminare i giochi selezionati?')){
                jQuery('#delete_game_input').val(getSelectedList());
                document.delete_game_form.submit();  
            }
        });
        
        // Nuova tipologia selezionabile
       
        jQuery('#button_n').click( function () {
            
            jQuery('#new_game_input').val(jQuery('.selected').attr('id'));
            document.new_game_form.submit();  
            
        }); 
    });
}

/**
 * Inizializzazione della lista di risposte possibili per una domanda del questionario
 * Inizializzazione strumenti elimina e nuova
 * @returns {undefined}
 */
function answerslist(){
    jQuery(document).ready(function() {
        
       var table = jQuery('#serveco_table').DataTable({
            
            "aaSorting": [[4,'asc']],
            "paging":   false,
            "aoColumnDefs": [{ 'bSortable': false, 
                               'aTargets': [0, 2, 4]
                           }]
        });
       
        table.column(4).visible(false);
       
        // selezione multipla delle righe
       jQuery('#serveco_table tbody').on( 'click', 'tr', function (){
           jQuery(this).toggleClass('selected');
       });
       
       // Eliminazione delle comunicazioni
       
       jQuery('#button_d1').click( function () {
            var x = table.rows('.selected').data().length;
            
            if (x < 1){
                alert('Nessuna riga selezionata');
                return;
            }
            
            if (confirm('Si è certi di volere eliminare le risposte selezionate?')){
                jQuery('#delete_answer_input').val(getSelectedList());
                document.delete_answer_form.submit();  
            }
        });
        
        // Nuova tipologia selezionabile
       
        jQuery('#button_n1').click( function () {
            
            jQuery('#delete_answer_input').val(jQuery('.selected').attr('id'));
            document.new_answer_form.submit();  
            
        }); 
    });
}

/**
 * Inizializzazione della lista delle domande del questionario
 * Inizializzazione strumenti elimina e nuova
 * @returns {undefined}
 */
function questionslist(){
    jQuery(document).ready(function() {
        
       var table = jQuery('#serveco_table').DataTable({
            
            "aaSorting": [[5,'asc']],
            "paging":   true,
            "aoColumnDefs": [{ 'bSortable': false, 
                               'aTargets': [0, 2, 4]
                           }]
        });
       
        table.column(5).visible(false);
       
        // selezione multipla delle righe
       jQuery('#serveco_table tbody').on( 'click', 'tr', function (){
           jQuery(this).toggleClass('selected');
       });
       
       // Eliminazione delle comunicazioni
       
       jQuery('#button_d').click( function () {
            var x = table.rows('.selected').data().length;
            
            if (x < 1){
                alert('Nessuna riga selezionata');
                return;
            }
            
            if (confirm('Si è certi di volere eliminare le domande selezionate?')){
                jQuery('#delete_question_input').val(getSelectedList());
                document.delete_question_form.submit();  
            }
        });
        
        // Nuova tipologia selezionabile
       
        jQuery('#button_n').click( function () {
            
            jQuery('#new_question_form').val(jQuery('.selected').attr('id'));
            document.new_question_form.submit();  
            
        }); 
    });
}

/**
 * Inizializzazione della lista delle tipologie di segnalazioni selezionabili dall'utente
 * Inizializzazione strumenti elimina e nuova
 * @returns {undefined}
 */
function tipologielist(){
    jQuery(document).ready(function() {
        
       var table = jQuery('#serveco_table').DataTable({
            
            "aaSorting": [[2,'desc']],
            "paging":   true,
            "aoColumnDefs": [{ 'bSortable': false, 
                               'aTargets': [1]
                           }]
        });
       
        table.column(2).visible(false);
       
        // selezione multipla delle righe
       jQuery('#serveco_table tbody').on( 'click', 'tr', function (){
           jQuery(this).toggleClass('selected');
       });
       
       // Eliminazione delle comunicazioni
       
       jQuery('#button_d').click( function () {
            var x = table.rows('.selected').data().length;
            
            if (x < 1){
                alert('Nessuna riga selezionata');
                return;
            }
            
            if (confirm('Si è certi di volere eliminare le comunicazioni selezionate?')){
                jQuery('#delete_tipo_input').val(getSelectedList());
                document.delete_tipo_form.submit();  
            }
        });
        
        // Nuova tipologia selezionabile
       
        jQuery('#button_n').click( function () {
            
            jQuery('#new_tipo_input').val(jQuery('.selected').attr('id'));
            document.new_tipo_form.submit();  
            
        }); 
    });
}

/**
 * Inizializzazione della lista delle categorie di utenti
 * Inizializzazione strumenti elimina e nuova
 * @param cols Numero di colonne della tabella
 * @returns {undefined}
 */
function comunicationslist(cols){
    jQuery(document).ready(function() {
        
       var table = jQuery('#serveco_table').DataTable({
            
            "lengthMenu": [[50, 100, 300, -1], [50, 100, 300, "All"]],
            "aaSorting": [[cols - 1,'desc']],
            "paging":   true,
            "aoColumnDefs": [{ 'bSortable': false, 
                               'aTargets': [cols - 2]
                           }]
        });
        
        table.column(cols - 1).visible(false);
       
       // seleziona una riga per volta
       jQuery('#serveco_table tbody').on( 'click', 'tr', function (){
           jQuery(this).toggleClass('selected');
       });
       
       // Eliminazione delle comunicazioni
       
       jQuery('#button_d').click( function () {
            var x = table.rows('.selected').data().length;
            
            if (x < 1){
                alert('Nessuna riga selezionata');
                return;
            }
            
            if (confirm('Si è certi di volere eliminare le comunicazioni selezionate?')){
                jQuery('#delete_signal_input').val(getSelectedList());
                document.delete_signal_form.submit();  
            }
        }); 
    });
}

/**
 * Inizializzazione della lista delle categorie di utenti
 * Inizializzazione strumenti elimina e nuova
 * @returns {undefined}
 */
function listedtermslist(){
    jQuery(document).ready(function() {
        
       var table = jQuery('#serveco_table').DataTable({
            "lengthMenu": [[50, 100, 300, -1], [50, 100, 300, "All"]],
            "aaSorting": [[3,'desc']],
            "paging":   true
        });
        
        table.column(3).visible(false);
       
        // selezione multipla delle righe
       jQuery('#serveco_table tbody').on( 'click', 'tr', function (){
           jQuery(this).toggleClass('selected');
       });
       
     // Eliminazione delle comunicazioni
       
       jQuery('#button_d').click( function () {
            var x = table.rows('.selected').data().length;
            
            if (x < 1){
                alert('Nessuna riga selezionata');
                return;
            }
            
            if (confirm('Si è certi di volere eliminare i termini selezionati?')){
                jQuery('#delete_dictionary_input').val(getSelectedList());
                document.delete_dictionary_form.submit();  
            }
        }); 
    });
}


/**
 * Inizializzazione della lista delle categorie di utenti
 * Inizializzazione strumenti elimina e nuova
 * @returns {undefined}
 */
function sendedtermslist(){
    jQuery(document).ready(function() {
        
       var table = jQuery('#serveco_table').DataTable({
            "lengthMenu": [[50, 100, 300, -1], [50, 100, 300, "All"]],
            "aaSorting": [[3,'desc']],
            "paging":   true
        });
        
        table.column(3).visible(false);
       
       // selezione multipla delle righe
       jQuery('#serveco_table tbody').on( 'click', 'tr', function (){
           jQuery(this).toggleClass('selected');
       });
        
        // Eliminazione di termini
       
        jQuery('#button_d').click( function () {
            var x = table.rows('.selected').data().length;
            
            if (x < 1){
                alert('Nessun termine selezionato');
                return;
            }
            
            if (confirm('Si è certi di voler eliminare i termini selezionati?')){
                jQuery('#delete_terms_input').val(getSelectedList());
                document.delete_terms_form.submit();  
            }
        }); 
        
        // Validazione di un termine
       
        jQuery('#button_c').click( function () {
            var x = table.rows('.selected').data().length;
            
            if (x < 1){
                alert('Nessun termine selezionato');
                return;
            }
            
            if (x > 1){
                alert("E' possibile validare un termine per volta");
                return;
            }
            
            jQuery('#validate_terms_input').val(jQuery('.selected').attr('id'));
            document.validate_terms_form.submit();  
            
        }); 
    });
}

/**
 * Inizializzazione della lista delle categorie di utenti
 * Inizializzazione strumenti elimina e nuova
 * @returns {undefined}
 */
function materialslist(){
    jQuery(document).ready(function() {
        
       var table = jQuery('#serveco_table').DataTable({
            
            "aaSorting": [[3,'desc']],
            "paging":   true
        });
        
        table.column(3).visible(false);
       
       // seleziona una riga per volta
       jQuery('#serveco_table tbody').on( 'click', 'tr', function () {
            if ( jQuery(this).hasClass('selected') ) {
                jQuery(this).removeClass('selected');
            }
            else {
                jQuery('tr.selected').removeClass('selected');
                jQuery(this).addClass('selected');
            }
        });
        
        // Eliminazione di una raccolta
       
        jQuery('#button_d').click( function () {
            var x = table.rows('.selected').data().length;
            
            if (x < 1){
                alert('Nessun materiale selezionata');
                return;
            }
            
            if (confirm('Si è certi di voler eliminare il materiale selezionato?')){
                jQuery('#delete_materials_input').val(jQuery('.selected').attr('id'));
                document.delete_materials_form.submit();  
            }
        }); 
        
        // Nuova raccolta
       
        jQuery('#button_n').click( function () {
            
            jQuery('#new_materials_input').val(jQuery('.selected').attr('id'));
            document.new_materials_form.submit();  
            
        }); 
    });
}

/**
 * Inizializzazione della lista di tutti gli utenti
 * @returns {undefined}
 */
function userslist(){
    jQuery(document).ready(function() {
        
        var table = jQuery('#serveco_table').DataTable({
            
            "lengthMenu": [[100, 200, 500, -1], [100, 200, 500, "All"]],
            "aaSorting": [[7,'desc']],
            "paging":   true,
            "aoColumnDefs": [{ 'bSortable': false, 
                               'aTargets': [6]
                           }]
        });
        
        table.column(7).visible(false);
    });
}

/**
 * Inizializzazione della lista delle categorie di utenti
 * Inizializzazione strumenti elimina e nuova
 * @returns {undefined}
 */
function ucategorylist(){
    jQuery(document).ready(function() {
        
       var table = jQuery('#serveco_table').DataTable();
       
       // seleziona una riga per volta
       jQuery('#serveco_table tbody').on( 'click', 'tr', function () {
            if ( jQuery(this).hasClass('selected') ) {
                jQuery(this).removeClass('selected');
            }
            else {
                jQuery('tr.selected').removeClass('selected');
                jQuery(this).addClass('selected');
            }
        });
        
        // Eliminazione di una raccolta
       
        jQuery('#button_d').click( function () {
            var x = table.rows('.selected').data().length;
            
            if (x < 1){
                alert('Nessuna categoria di utenti selezionata');
                return;
            }
            
            if (confirm('Si è certi di voler eliminare la categoria di utenti selezionata?')){
                jQuery('#delete_u_cat_input').val(jQuery('.selected').attr('id'));
                document.delete_u_cat_form.submit();  
            }
        }); 
        
        // Nuova raccolta
       
        jQuery('#button_n').click( function () {
            
            jQuery('#new_u_cat_input').val(jQuery('.selected').attr('id'));
            document.new_u_cat_form.submit();  
            
        }); 
    });
}

/**
 * Inizializzazione della lista delle raccolte differenziate
 * Inizializzazione strumenti elimina, clona
 * @returns {undefined}
 */
function garbagelist(){
    jQuery(document).ready(function() {
        
        var table = jQuery('#serveco_table').DataTable({
            
            "lengthMenu": [[50, 100, 150, -1], [50, 100, 150, "All"]],
            "aaSorting": [[5,'desc']],
            "paging":   true,
            "aoColumnDefs": [{ 'bSortable': false, 
                               'aTargets': [4]
                           }]
        });
 
       table.column(5).visible(false);
       
       // seleziona una riga per volta
       jQuery('#serveco_table tbody').on( 'click', 'tr', function () {
            if ( jQuery(this).hasClass('selected') ) {
                jQuery(this).removeClass('selected');
            }
            else {
                jQuery('tr.selected').removeClass('selected');
                jQuery(this).addClass('selected');
            }
        });
        
        // Eliminazione di una raccolta
       
        jQuery('#button_d').click( function () {
            var x = table.rows('.selected').data().length;
            
            if (x < 1){
                alert('Nessuna raccolta selezionata');
                return;
            }
            
            if (confirm('Si è certi di voler eliminare la raccolta selezionata?')){
                jQuery('#delete_garbage_input').val(jQuery('.selected').attr('id'));
                document.delete_garbage_form.submit();  
            }
        }); 
        
        // Clonazione di una raccolta
       
        jQuery('#button_c').click( function () {
            var x = table.rows('.selected').data().length;
            
            if (x < 1){
                alert('Nessuna raccolta selezionata');
                return;
            }
           
            if (confirm('Si è certi di voler clonare la zona selezionata?')){
                jQuery('#clone_garbage_input').val(jQuery('.selected').attr('id'));
                document.clone_garbage_form.submit();  
            }
        });
        
        // Nuova raccolta
       
        jQuery('#button_n').click( function () {
            
            jQuery('#new_garbage_input').val(jQuery('.selected').attr('id'));
            document.new_garbage_form.submit();  
            
        }); 
    });
}

/**
 * Inizializzazione della lista delle zone
 * Inizializzazione strumenti elimina, clona
 * @returns {undefined}
 */
function zoneslist(){
    jQuery(document).ready(function() {
        
        var table = jQuery('#serveco_table').DataTable({
            
            "aaSorting": [[5,'desc']],
            "paging":   true,
            "aoColumnDefs": [{ 'bSortable': false, 
                               'aTargets': [2]
                           }]
        });
 
       table.column(5).visible(false);
       
       // seleziona una riga per volta
       jQuery('#serveco_table tbody').on( 'click', 'tr', function () {
            if ( jQuery(this).hasClass('selected') ) {
                jQuery(this).removeClass('selected');
            }
            else {
                jQuery('tr.selected').removeClass('selected');
                jQuery(this).addClass('selected');
            }
        });
        
        // Eliminazione di una zona
       
        jQuery('#button_d').click( function () {
            var x = table.rows('.selected').data().length;
            
            if (x < 1){
                alert('Nessuna zona selezionata');
                return;
            }
            
            if (confirm('Si è certi di voler eliminare la zona selezionata?\nTutti gli utenti associati saranno inseriti nel comune generico')){
                jQuery('#delete_zone_input').val(jQuery('.selected').attr('id'));
                document.delete_zone_form.submit();  
            }
        });
        
        // Clonazione di una zona
       
        jQuery('#button_c').click( function () {
            var x = table.rows('.selected').data().length;
            
            if (x < 1){
                alert('Nessuna zona selezionata');
                return;
            }
               
            // il modulo è : instruments_zone_list
            if (confirm('Si è certi di voler clonare la zona selezionata?\nL\'operazione potrebbe richiedere alcuni secondi')){
                jQuery('#clone_zone_input').val(jQuery('.selected').attr('id'));
                document.clone_zone_form.submit();  
            }
        });
    });
}

/**
 * Inizializzazione base per la lista dei centri abitati
 * Abilitazione strumenti di clonazione e nuova istanza
 * http://www.datatables.net/examples/basic_init/zero_configuration.html
 */

function townslist(){
    jQuery(document).ready(function() {
    
        var table = jQuery('#serveco_table').DataTable({
            
            "aaSorting": [[7,'desc']],
            "paging":   false,
            "aoColumnDefs": [{ 'bSortable': false, 
                               'aTargets': [1, 2, 3]
                           }]
        });
 
       table.column(7).visible(false);
 
       jQuery('#serveco_table tbody').on( 'click', 'tr', function () {
        if ( jQuery(this).hasClass('selected') ) {
            jQuery(this).removeClass('selected');
        }
        else {
            jQuery('tr.selected').removeClass('selected');
            jQuery(this).addClass('selected');
        }
    } );
   
       // Strumento clonazione
       
        jQuery('#button_c').click( function () {
            var x = table.rows('.selected').data().length;
            
            if (x < 1){
                alert('Nessun centro abitato selezionato');
                return;
            }
            
            if (confirm('Si è certi di voler clonare il centro abitato selezionato?\nL\'operazione potrebbe richiedere un breve tempo di attesa.')){
                jQuery('#clone_town_input').val(jQuery('.selected').attr('id'));
                document.clone_town_form.submit();
            }
        });
        
        // Strumento nuovo centro abitato
        
        jQuery('#button_n').click( function () {
            document.new_town_form.submit();
        });
    });
}



