<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class PartecipatesController extends AppController {
    
    public function partecipate( $idevent, $iduser ) {
        if( !$this->Partecipate->hasPartecipated(  $iduser, $idevent) ){
            $object = new stdClass();
            $object->iduser         = $iduser;
            $object->idevent        = $idevent;
            $object->confirmed      = 0;
            $this->Partecipate->save( $object );
            return true;
        }
        else {
            return false;
        }
    }
    
    public function autopartecipate( $idevent, $iduser ) {
        if( !$this->Partecipate->hasPartecipated(  $iduser, $idevent) ){
            $object = new stdClass();
            $object->iduser         = $iduser;
            $object->idevent        = $idevent;
            $object->confirmed      = 1;
            $this->Partecipate->save( $object );
            return true;
        }
        else {
            return false;
        }
    }
   
    /**
     * Marcatura della prenotazione come accettata dal creatore
     * @param int $idevent pk evento
     * @param int $iduser pk utente che ha prenotato
     * @return true se la marcatura è andata a buon fine, false nel caso in cui la prenotazione non era presente
     */
    public function accept( $idevent, $iduser ) {
        // se esiste la prenotazione all'evento
        if( $this->Partecipate->hasBeenInvited(  $iduser, $idevent ) ){
            // viene eliminata la prenotazione all'evento
            $this->unpartecipate($iduser, $idevent);
            
            // Poi viene aggiunta una prenotazione dello stesso utente allo stesso evento però marcata come accettata
            $object = new stdClass();
            $object->iduser         = $iduser;
            $object->idevent        = $idevent;
            $object->confirmed      = 1;
            $this->Partecipate->save( $object );
            
            return 1;
        }
        elseif(  $this->Partecipate->hasBeenConfirmed(  $iduser, $idevent ) ) {
            return -1;
        }
        elseif(  !$this->Partecipate->hasPartecipated(  $iduser, $idevent ) ) { 
            return -2;
        }
        else { 
            return 0;
        }
    }
    
    /**
     * Rimozione di una prenotazione ad un evento
     * @param int $iduser pk utente che ha fatto la prenotazione
     * @param int $idevent pk evento
     * @return boolean true se la prenotazione esisteva, false altrimenti
     */
    public function unpartecipate( $iduser, $idevent ) {
        if( $this->Partecipate->hasPartecipated( $iduser, $idevent ) ){
            return $this->Partecipate->deleteAll(array('iduser' => $iduser, 'idevent' => $idevent), false) ;
        }
        else {
            return false;
        }
    }
    
    /**
     * Seleziona gli eventi a cui un utente ha fatto richiesta di partecipazione
     * @param int $iduser
     * @return lista delle tuple iduser, idevent, confirmned
     */
    public function getPartecipationList( $iduser ) {
        return $this->Partecipate->getPartecipationList( $iduser );  
    }
    
    public function deleteAllPartecipations( $idevent ) {
        $this->Partecipate->query( 'DELETE FROM partecipates WHERE idevent = '.$idevent);
    }

    
}