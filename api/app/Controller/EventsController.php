<?php

/**
 * UsersController is the main controller for user based request
 *ƒ
 * @author Christian Ruggiero
 * @creation 30/10/2014
 * @status in develop
 */
App::import('Vendor',       'Events/EventList');
App::import('Vendor',       'Events/MonthlyEventList');
App::import('Vendor',       'Events/IEventListController');
App::import('Vendor',       'Events/IEventDateListedController');
App::import('Vendor',       'Notifications/NotificationPayload');
App::import('Controller',   'Partecipates');
App::import('Controller',   'FriendCreatedEvents');
App::import('Controller',   'DateListedEvents');
App::import('Controller',   'ActivatedEvents');
App::import('Controller',   'PublicEvents');
App::import('Controller',   'UserFollows');
App::import('Controller',   'Users');
App::import('Controller',   'DateListedPartecipatedEvents');
App::import('Controller',   'UserPartecipateEvents');
App::import('Controller',   'DateListedFriendsCreatedEvents');
App::import('Controller',   'DateListedCreatedEvents');
App::import('Controller',   'ConfirmedPartecipates');
App::import('Controller',   'FollowCreatedEvents');
App::import('Controller',   'UserCreatedEvents');
App::import('Controller',   'Notifications');

class EventsController extends AppController{
    
    public function index() {
        //grab all events and pass it to the view:
        $events = $this->Event->find('all');
        $this->set('response', $events);
    }
    
   
    public function pagedlist() {
        if( isset( AppModel::getInputData()->lastdate ) && isset( AppModel::getInputData()->quantity ) && isset( AppModel::getInputData()->iduser )) {
            $lastDate       = AppModel::getInputData()->lastdate;
            $searchLimit    = AppModel::getInputData()->quantity;
            $iduser         = AppModel::getInputData()->iduser;
            $dateListedEventsController     = new DateListedEventsController();
            $eventsController               = new PublicEventsController();

            $responseObj = Utils::generateSuccess( SuccessCodes::OK );
            $responseObj->eventsdayslist = $this->getEventList($iduser, $lastDate, $searchLimit, $dateListedEventsController, $eventsController);
            $this->set( 'response',  $responseObj );
        }
        else {
            $this->set('response', Utils::generateSuccess( SuccessCodes::RS_INPUT_ERROR ));
        }
    }
    
    public function monthly() {
        if( isset( AppModel::getInputData()->year ) && isset( AppModel::getInputData()->month ) && isset( AppModel::getInputData()->iduser )) {
            $iduser     = AppModel::getInputData()->iduser;
            $year     = AppModel::getInputData()->year;
            $month      = AppModel::getInputData()->month;
            $publicEventsController = new PublicEventsController();
            $responseObj = Utils::generateSuccess( SuccessCodes::OK );
            $responseObj->eventsinadaylist = $this->getMonthlyEventList( $iduser, $month, $year, $publicEventsController );
            $this->set( 'response',  $responseObj );
        }
        else {
            $this->set('response',Utils::generateSuccess( SuccessCodes::RS_INPUT_ERROR ));
        }
    }
    
    public function distancelist() {
        if( isset( AppModel::getInputData()->latitude ) && isset( AppModel::getInputData()->longitude ) && isset( AppModel::getInputData()->iduser ) && isset( AppModel::getInputData()->maxdist ) ) {
            $latitude       = AppModel::getInputData()->latitude;
            $longitude      = AppModel::getInputData()->longitude;
            $maxdist        = AppModel::getInputData()->maxdist;
            $iduser         = AppModel::getInputData()->iduser;
            $eventsController               = new PublicEventsController();

            $responseObj = Utils::generateSuccess( SuccessCodes::OK );
            $responseObj->eventsdayslist = $this->getDistanceList( $iduser, $latitude, $longitude, $maxdist,  $eventsController  );
            $this->set( 'response',  $responseObj );
        }
        else {
            $this->set('response',Utils::generateSuccess( SuccessCodes::RS_INPUT_ERROR ));
        }
    }
    
    // lista degli eventi a cui partecipero'
    /**
     * @deprecated by /pevents/partecipatedevents
     */
    public function partecipatedevents() {
        if( isset( AppModel::getInputData()->lastdate ) && isset( AppModel::getInputData()->quantity ) && isset( AppModel::getInputData()->iduser) ) {
            $lastDate       = AppModel::getInputData()->lastdate;
            $searchLimit    = AppModel::getInputData()->quantity;
            $iduser         = AppModel::getInputData()->iduser;
            $dateListedEventsController     = new DateListedPartecipatedEventsController();
            $eventsController               = new UserPartecipateEventsController();

            $responseObj = Utils::generateSuccess( SuccessCodes::OK );
            $responseObj->eventsdayslist = $this->getEventList( $iduser, $lastDate, $searchLimit, $dateListedEventsController, $eventsController  );
            $this->set( 'response',  $responseObj );
        }
        else {
            $this->set('response',Utils::generateSuccess( SuccessCodes::RS_INPUT_ERROR ));
        }
    }
    
    //Lista Eventi creati dall'utente (tutti gli scope)
    
    /**
     * @deprecated Sostituito dal metodo /pevents/usercreatedevents 
     */
    public function usercreatedevents() {
        if( isset( AppModel::getInputData()->lastdate ) && isset( AppModel::getInputData()->quantity ) && isset( AppModel::getInputData()->iduser) ) {
            $lastDate       = AppModel::getInputData()->lastdate;
            $searchLimit    = AppModel::getInputData()->quantity;
            $iduser         = AppModel::getInputData()->iduser;
            $dateListedEventsController     = new DateListedCreatedEventsController();
            $eventsController               = new UserCreatedEventsController();

            $responseObj = Utils::generateSuccess( SuccessCodes::OK );
            $responseObj->eventsdayslist = $this->getEventList( $iduser, $lastDate, $searchLimit, $dateListedEventsController, $eventsController  );
            $this->set( 'response',  $responseObj );
        }
        else {
            $this->set('response',Utils::generateSuccess( SuccessCodes::RS_INPUT_ERROR ));
        }
    }

    /**
     * Deprecato con WalleventsController::filteredpagedlist()
     */
    public function filteredpagedlist() {
        if( isset( AppModel::getInputData()->lastdate ) && isset( AppModel::getInputData()->quantity ) && isset(AppModel::getInputData()->iduser)) {
            $lastdate = AppModel::getInputData()->lastdate;
            $searchLimit = AppModel::getInputData()->quantity;
            if( isset(AppModel::getInputData()->datefrom) && !empty(AppModel::getInputData()->datefrom) ) {
                    $datefrom = AppModel::getInputData()->datefrom;
                    $datefromdate = new DateTime(AppModel::getInputData()->datefrom );
                    $datefromdate->sub(new DateInterval('P1D'));
                    $lastdatedate = new DateTime( AppModel::getInputData()->lastdate );
                    
                    if( $datefromdate > $lastdatedate ) {
                        $lastdate = $datefromdate->format("Y-m-d");
                    } 
                } 
            
                
                
                
            if( isset( AppModel::getInputData()->follows ) && AppModel::getInputData()->follows == 1 ) {
                $publicEventController =    new FollowCreatedEventsController();
                $result =                   $publicEventController->getFilteredListPaged( AppModel::getInputData()->iduser, $lastdate, $searchLimit, AppModel::getInputData() );
            }
            else {
                $publicEventController =    new PublicEventsController();
                $result =                   $publicEventController->getFilteredListPaged( $lastdate, $searchLimit, AppModel::getInputData() );
            }
            
            
            
            
            $partecipatsController = new PartecipatesController();
            $partecipationList = $partecipatsController->getPartecipationList( AppModel::getInputData()->iduser );
            $partecipationArray = array();
            foreach ( $partecipationList as $singlePartecipation ) {   
                $partecipationArray [$singlePartecipation["Partecipate"]["idevent"]] = $singlePartecipation["Partecipate"]["confirmed"];
            }
            
            $eventList = new EventList();
            foreach ($result as $singleEvent) {
                $eventList->addEvent( $singleEvent["Event"]["date"], $singleEvent["Event"], $partecipationArray);
            }
            $responseObj = Utils::generateSuccess( SuccessCodes::OK );
            $responseObj->eventsdayslist = $eventList->eventsdayslist;
            $this->set( 'response',  $responseObj );
        }
        else {
            $this->set('response',Utils::generateSuccess( SuccessCodes::RS_INPUT_ERROR ));
        }
    }
    
    
    public function test() {
            if( isset( AppModel::getInputData()->lastdate ) && isset( AppModel::getInputData()->quantity ) && isset(AppModel::getInputData()->iduser)) {
            $lastdate = AppModel::getInputData()->lastdate;
            $searchLimit = AppModel::getInputData()->quantity;
            if( isset(AppModel::getInputData()->datefrom) && !empty(AppModel::getInputData()->datefrom) ) {
                    $datefrom = AppModel::getInputData()->datefrom;
                    $datefromdate = new DateTime(AppModel::getInputData()->datefrom );
                    $datefromdate->sub(new DateInterval('P1D'));
                    $lastdatedate = new DateTime( AppModel::getInputData()->lastdate );
                    var_dump($datefromdate);
                    var_dump($lastdatedate);
                    if( $datefromdate > $lastdatedate ) {
                        $lastdate = $datefromdate->format("Y-m-d");
                    } 
                } 
                var_dump($lastdate);
            if( isset( AppModel::getInputData()->follows ) && AppModel::getInputData()->follows == 1 ) {
                $publicEventController =    new FollowCreatedEventsController();
                $result =                   $publicEventController->getFilteredListPaged( AppModel::getInputData()->iduser, $lastdate, $searchLimit, AppModel::getInputData() );
            }
            else {
                $publicEventController =    new PublicEventsController();
                $result =                   $publicEventController->getFilteredListPaged( $lastdate, $searchLimit, AppModel::getInputData() );
            }
            $partecipatsController = new PartecipatesController();
            $partecipationList = $partecipatsController->getPartecipationList( AppModel::getInputData()->iduser );
            $partecipationArray = array();
            foreach ( $partecipationList as $singlePartecipation ) {   
                $partecipationArray [$singlePartecipation["Partecipate"]["idevent"]] = $singlePartecipation["Partecipate"]["confirmed"];
            }
            
            $eventList = new EventList();
            foreach ($result as $singleEvent) {
                $eventList->addEvent( $singleEvent["Event"]["date"], $singleEvent["Event"], $partecipationArray);
            }
            $responseObj = Utils::generateSuccess( SuccessCodes::OK );
            $responseObj->eventsdayslist = $eventList->eventsdayslist;
            $this->set( 'response',  $responseObj );
        }
        else {
            $this->set('response',Utils::generateSuccess( SuccessCodes::RS_INPUT_ERROR ));
        }
    }
    
    
    /**
     * @deprecated da WalleventsController::filteredpagedlist()
     */
    public function filteredmonthly() {
        if( isset(AppModel::getInputData()->month) && isset(AppModel::getInputData()->year) && isset(AppModel::getInputData()->iduser)) {
            $month = AppModel::getInputData()->month;
            $year = AppModel::getInputData()->year;
            
            $publicEventController = null;
            if( isset( AppModel::getInputData()->follows ) && AppModel::getInputData()->follows == "1" ) {
                $publicEventController =    new FollowCreatedEventsController();
                $result =                   $publicEventController->getFilteredListByMonth( AppModel::getInputData()->iduser, $year, $month, AppModel::getInputData() );
            }
            else {
                $publicEventController =    new PublicEventsController();
                $result =                   $publicEventController->getFilteredListByMonth( $year, $month, AppModel::getInputData() );
            }

            $partecipatsController = new PartecipatesController();
            $partecipationList = $partecipatsController->getPartecipationList( AppModel::getInputData()->iduser);
            $partecipationArray = array();
            foreach ( $partecipationList as $singlePartecipation ) {   
                $partecipationArray [$singlePartecipation["Partecipate"]["idevent"]] = $singlePartecipation["Partecipate"]["confirmed"];
            }
            
            $eventList = new MonthlyEventList();
            foreach ($result as $singleEvent) {
                $eventList->addEvent( $singleEvent[$publicEventController->getListName()]["date"], $singleEvent[$publicEventController->getListName()], $partecipationArray);
            }

            $responseObj = Utils::generateSuccess( SuccessCodes::OK );
            $responseObj->eventsinadaylist = $eventList->eventsinadaylist;
            $this->set( 'response',  $responseObj );
        }
        else {
            $this->set('response',Utils::generateSuccess( SuccessCodes::RS_INPUT_ERROR ));
        }
    }
    
     /**
     * @deprecated da WalleventsController::filtereddistancelist()
     */
    public function filtereddistancelist() {
        if( isset( AppModel::getInputData()->latitude ) && isset( AppModel::getInputData()->longitude ) && isset( AppModel::getInputData()->iduser ) && isset( AppModel::getInputData()->maxdist ) ) {
            $latitude       = AppModel::getInputData()->latitude;
            $longitude      = AppModel::getInputData()->longitude;
            $maxdist        = AppModel::getInputData()->maxdist;
            $iduser         = AppModel::getInputData()->iduser;
            if( isset( AppModel::getInputData()->follows ) && ( AppModel::getInputData()->follows == "1" )) {
                $publicEventController =    new FollowCreatedEventsController();
            }
            else {
                $publicEventController =    new PublicEventsController();
            }
            $responseObj = Utils::generateSuccess( SuccessCodes::OK );
            $responseObj->eventsdayslist = $this->getDistanceListFiltered( $iduser, $latitude, $longitude, $maxdist,  $publicEventController, AppModel::getInputData()  );
            $this->set( 'response',  $responseObj );
        }
        else {
            $this->set('response',Utils::generateSuccess( SuccessCodes::RS_INPUT_ERROR ));
        }
    }
    
    //Lista Eventi Organizzata dagli amicici (scope 0 e 2)
    public function friendscreatedevents() {
        if( isset( AppModel::getInputData()->lastdate ) && isset( AppModel::getInputData()->quantity ) && isset( AppModel::getInputData()->iduser) ) {
            $lastDate       = AppModel::getInputData()->lastdate;
            $searchLimit    = AppModel::getInputData()->quantity;
            $iduser         = AppModel::getInputData()->iduser;
            $dateListedEventsController     = new DateListedFriendsCreatedEventsController();
            $eventsController               = new FriendCreatedEventsController();

            $responseObj = Utils::generateSuccess( SuccessCodes::OK );
            $responseObj->eventsdayslist = $this->getEventList( $iduser, $lastDate, $searchLimit, $dateListedEventsController, $eventsController  );
            $this->set( 'response',  $responseObj );
            
        }
        else {
            $this->set('response',Utils::generateSuccess( SuccessCodes::RS_INPUT_ERROR ));
        }
    }
    
    //Lista Eventi Organizzata dagli amicici (scope 0 e 2) vista mensile
    public function friendscreatedeventsmonthly() {
        if( isset( AppModel::getInputData()->year ) && isset( AppModel::getInputData()->month ) && isset( AppModel::getInputData()->iduser )) {
            $iduser     = AppModel::getInputData()->iduser;
            $year       = AppModel::getInputData()->year;
            $month      = AppModel::getInputData()->month;
            $eventsController = new FriendCreatedEventsController();
            $responseObj = Utils::generateSuccess( SuccessCodes::OK );
            $responseObj->eventsinadaylist = $this->getMonthlyEventList( $iduser, $month, $year, $eventsController );
            $this->set( 'response',  $responseObj );
        }
        else {
            $this->set('response',Utils::generateSuccess( SuccessCodes::RS_INPUT_ERROR ));
        }
    }
    
    //Lista Eventi Organizzata dagli amicici (scope 0 e 2) vista mappa
    public function friendscreatedeventsdistancelist() {
        if( isset( AppModel::getInputData()->latitude ) && isset( AppModel::getInputData()->longitude ) && isset( AppModel::getInputData()->iduser ) && isset( AppModel::getInputData()->maxdist ) ) {
            $latitude       = AppModel::getInputData()->latitude;
            $longitude      = AppModel::getInputData()->longitude;
            $maxdist        = AppModel::getInputData()->maxdist;
            $iduser         = AppModel::getInputData()->iduser;
            $eventsController               = new FriendCreatedEventsController();

            $responseObj = Utils::generateSuccess( SuccessCodes::OK );
            $responseObj->eventsdayslist = $this->getDistanceList( $iduser, $latitude, $longitude, $maxdist,  $eventsController);
            $this->set( 'response',  $responseObj );
        }
        else {
            $this->set('response',Utils::generateSuccess( SuccessCodes::RS_INPUT_ERROR ));
        }
    }
    
    // NON VIENE MAI USATA
    public function filteredfriendscreatedevents() {
        if( isset( AppModel::getInputData()->lastdate ) && isset( AppModel::getInputData()->quantity ) && isset(AppModel::getInputData()->iduser)) {
            $lastdate       = AppModel::getInputData()->lastdate;
            $searchLimit    = AppModel::getInputData()->quantity;
            $iduser         = AppModel::getInputData()->iduser;
            if( isset(AppModel::getInputData()->datefrom) && !empty(AppModel::getInputData()->datefrom) ) {
                $datefrom = AppModel::getInputData()->datefrom;
                $datefromdate = new DateTime(AppModel::getInputData()->datefrom );
                $datefromdate->sub(new DateInterval('P1D'));
                $lastdatedate = new DateTime( AppModel::getInputData()->lastdate );
                if( $datefromdate > $lastdatedate ) {
                    $lastdate = $datefromdate->format("Y-m-d");
                } 
            }  
            $publicEventController =    new FriendCreatedEventsController();
            $result =                   $publicEventController->getFilteredListPaged( $iduser, $lastdate, $searchLimit, AppModel::getInputData() );
            
            $partecipatsController = new PartecipatesController();
            $partecipationList = $partecipatsController->getPartecipationList( $iduser );
            $partecipationArray = array();
            foreach ( $partecipationList as $singlePartecipation ) {   
                $partecipationArray [$singlePartecipation["Partecipate"]["idevent"]] = $singlePartecipation["Partecipate"]["confirmed"];
            }
            
            $eventList = new EventList();
            foreach ($result as $singleEvent) {
                $eventList->addEvent( $singleEvent["Event"]["date"], $singleEvent["Event"], $partecipationArray);
            }
            $responseObj = Utils::generateSuccess( SuccessCodes::OK );
            $responseObj->eventsdayslist = $eventList->eventsdayslist;
            $this->set( 'response',  $responseObj );
        }
        else {
            $this->set('response',Utils::generateSuccess( SuccessCodes::RS_INPUT_ERROR ));
        }
    }
    
    // NON VIENE MAI USATA
    public function filteredfriendscreatedeventsmonthly() {
        if( isset( AppModel::getInputData()->year ) && isset( AppModel::getInputData()->month ) && isset( AppModel::getInputData()->iduser )) {
            $month      = AppModel::getInputData()->month;
            $year       = AppModel::getInputData()->year;
            $iduser     = AppModel::getInputData()->iduser ;
            
            $publicEventController =    new FriendCreatedEventsController();
            $result =                   $publicEventController->getFilteredListByMonth( $iduser, $year, $month, AppModel::getInputData() );
            

            $partecipatsController = new PartecipatesController();
            $partecipationList = $partecipatsController->getPartecipationList( $iduser);
            $partecipationArray = array();
            foreach ( $partecipationList as $singlePartecipation ) {   
                $partecipationArray [$singlePartecipation["Partecipate"]["idevent"]] = $singlePartecipation["Partecipate"]["confirmed"];
            }
            
            $eventList = new EventList();
            foreach ($result as $singleEvent) {
                $eventList->addEvent( $singleEvent[$publicEventController->getListName()]["date"], $singleEvent[$publicEventController->getListName()], $partecipationArray);
            }
            $responseObj = Utils::generateSuccess( SuccessCodes::OK );
            $responseObj->eventsdayslist = $eventList->eventsdayslist;
            $this->set( 'response',  $responseObj );
        }
        else {
            $this->set('response',Utils::generateSuccess( SuccessCodes::RS_INPUT_ERROR ));
        }
    }
    
    // NON VIENE MAI USATA
    public function filteredfriendscreatedeventsdistancelist() {
        if( isset( AppModel::getInputData()->latitude ) && isset( AppModel::getInputData()->longitude ) && isset( AppModel::getInputData()->iduser ) && isset( AppModel::getInputData()->maxdist ) ) {
            $latitude       = AppModel::getInputData()->latitude;
            $longitude      = AppModel::getInputData()->longitude;
            $maxdist        = AppModel::getInputData()->maxdist;
            $iduser         = AppModel::getInputData()->iduser;
            $eventsController               = new FriendCreatedEventsController();
            $responseObj                    = Utils::generateSuccess( SuccessCodes::OK );
            $responseObj->eventsdayslist    = $this->getDistanceListFiltered( $iduser, $latitude, $longitude, $maxdist,  $eventsController, AppModel::getInputData()  );
            $this->set( 'response',  $responseObj );
        }
        else {
            $this->set( 'response', Utils::generateSuccess( SuccessCodes::RS_INPUT_ERROR ) );
        }
    }
    
    /**
     * Seleziona le informazioni associate ad un utente
     */
    public function getinfo() {
        if( isset( AppModel::getInputData()->idevent ) && isset(AppModel::getInputData()->iduser) ) {
            $idevent    = AppModel::getInputData()->idevent;
            $iduser     = AppModel::getInputData()->iduser;

            $result = $this->Event->getSingleEventInfo( $idevent );
            $usersController = new UsersController();
            $user = $usersController->getuserinfo( $result[0]["Event"]["iduser"] );
            $userFollowsController = new UserFollowsController();
            $followInfo = $userFollowsController->getFollowers($result[0]["Event"]["iduser"]);
   
            $responseObj = Utils::generateSuccess( SuccessCodes::OK );
            $eventObj  = new stdClass();
           
            $partecipatsController = new PartecipatesController();
            $partecipationList = $partecipatsController->getPartecipationList( $iduser );
            $partecipationArray = array();
            $eventObj->partecipate = 999;
            foreach ( $partecipationList as $singlePartecipation ) {   
                $partecipationArray [$singlePartecipation["Partecipate"]["idevent"]] = $singlePartecipation["Partecipate"]["confirmed"];
                if( $singlePartecipation["Partecipate"]["idevent"] == $result[0]["Event"]["idevent"] ) {
                    $eventObj->partecipate = $singlePartecipation["Partecipate"]["confirmed"];
                }
            }
            $eventObj->isfollowing = 0;
            if($userFollowsController->userFollowing($iduser,$result[0]["Event"]["iduser"] )) {
                $eventObj->isfollowing =1;
            }
            $eventObj->idevent          = $result[0]["Event"]["idevent"];
            $eventObj->iduser           = $result[0]["Event"]["iduser"];
            $eventObj->idcategory       = $result[0]["Event"]["idcategory"];
            $eventObj->title            = HTMLDecoder::decode($result[0]["Event"]["title"]);
            $eventObj->description      = HTMLDecoder::decode($result[0]["Event"]["description"]);
            $eventObj->img              = $result[0]["Event"]["img"];
            $eventObj->city             = HTMLDecoder::decode($result[0]["Event"]["city"]);
            $eventObj->address          = HTMLDecoder::decode($result[0]["Event"]["address"]);
            $eventObj->date             = $result[0]["Event"]["date"];
            $eventObj->hour             = $result[0]["Event"]["hour"];
            $eventObj->sex              = $result[0]["Event"]["sex"];
            $eventObj->agemax           = $result[0]["Event"]["agemax"];
            $eventObj->agemin           = $result[0]["Event"]["agemin"];
            $eventObj->sittotal         = $result[0]["Event"]["sittotal"];
            $eventObj->sitcurrent       = $result[0]["Event"]["sitcurrent"];
            $eventObj->latitude         = $result[0]["Event"]["latitude"];
            $eventObj->longitude        = $result[0]["Event"]["longitude"];
            $eventObj->scope            = $result[0]["Event"]["scope"];
            $eventObj->username         = HTMLDecoder::decode($user["User"]["name"]);
            $eventObj->usersurname      = HTMLDecoder::decode($user["User"]["surname"]);
            $eventObj->userimg          = $user["User"]["img"]; 
            $eventObj->userfollowers    = count($followInfo);
            $responseObj->data          = $eventObj;
            $this->set( 'response',  $responseObj );
        }
        else {
            $this->set('response',Utils::generateSuccess( SuccessCodes::RS_INPUT_ERROR ));
        }
    }
    

    public function acceptpartecipation() {
        if( isset( AppModel::getInputData()->idevent ) && isset( AppModel::getInputData()->iduser ) && isset( AppModel::getInputData()->idpartecipant ) ) {
            
            $partecipate      = new PartecipatesController();
            
            $idevent          = HTMLDecoder::encode( AppModel::getInputData()->idevent        );
            $iduser           = HTMLDecoder::encode( AppModel::getInputData()->iduser         ) ;  // creatore dell'evento
            $idpartecipant    = HTMLDecoder::encode( AppModel::getInputData()->idpartecipant  ) ;
            
            // selezione dell'evento
            $event = $this->Event->getSingleEventInfo( $idevent );
            
            // numero di persone che hanno prenotato
            $sitcurrent              = intval( $event[0]["Event"]["sitcurrent"] );
            // numero massimo di partecipanti
            $sittotal                = intval( $event[0]["Event"]["sittotal"] );
            
            if ($sitcurrent < $sittotal ) {
                
                // Controlla se la prenotazione esiste o se è già stata accettata. Se si viene marcata come accettata da parte del creatore dell'evento e si prosegue nell'if
                
                $acceptResult = $partecipate->accept( $idevent , $idpartecipant ) ;
                if ( $acceptResult > 0 ) { //0: si puo' procedere con l'accettazione
                    
                    $conditions['idevent']      = AppModel::getInputData()->idevent;
                    $data['Event.updatetime']   = "'".date('Y-m-d H:i:s')."'"; 
                    $data['Event.sitcurrent']   = 'Event.sitcurrent + 1';
                    
                    $this->Event->updateAll( $data, $conditions );
                    $this->Event->clear();
                    
                    // viene aggiornato il numero di partecipanti all'evento
                    $this->set( 'response',  Utils::generateSuccess( SuccessCodes::OK ) );
                    
                    //add notifications
                    $notificationsController    = new NotificationsController();
                    $notificationsController->addNotification ( $iduser, $idpartecipant, NotificationsController::NOTIFICATION_TYPE_BOOKING_ACCEPT, $idevent);   
                    $this->sendPush( $iduser, $idpartecipant, "",  NotificationsController::NOTIFICATION_TYPE_BOOKING_ACCEPT);
                }
                elseif ($acceptResult == -1) { //-1: l'utente è già un partecipante delll'evento
                    //$this->set( 'response',  Utils::generateSuccess( SuccessCodes::RS_JOININ_ALREADYJOINED ) );   // è bugggia !!  controlla solo se la prenotazione esiste (3 volte si :-(  ma comunque solo se è presente la prenotazione)
                    $this->set( 'response',  Utils::generateSuccess( SuccessCodes::RS_JOININ_ALREADYJOINED ) ); 
                }
                elseif ($acceptResult == -2) { //-2: l'utente non ha mai richiesta la partecipazione all'evento
                    $this->set( 'response',  Utils::generateSuccess( SuccessCodes::RS_JOININ_NOTBOOKED ) ); 
                }
                else{ //se c'è stato un errore non definito precedentemente
                    $this->set( 'response',  Utils::generateSuccess( SuccessCodes::RS_INPUT_ERROR ) ); 
                }
            }
            else {
                //restituisce un errore di FULL->evento già pieno
                $this->set( 'response',  Utils::generateSuccess( SuccessCodes::RS_INPUT_ERROR ) );//todo: assegnare un errore corretto
            }
            
        }
        else {
            $this->set( 'response',  Utils::generateSuccess( SuccessCodes::RS_INPUT_ERROR ) );
        }
    }
    
    
    public function partecipate() {
        if( isset( AppModel::getInputData()->idevent ) && isset( AppModel::getInputData()->iduser )) {
            $partecipate = new PartecipatesController();
            $idevent    = HTMLDecoder::encode( AppModel::getInputData()->idevent );
            $iduser     = HTMLDecoder::encode(AppModel::getInputData()->iduser) ;
            
            //full?
            $event      = $this->Event->getSingleEventInfo( $idevent );
            $sitcurrent = intval( $event[0]["Event"]["sitcurrent"]);
            $sittotal   = intval( $event[0]["Event"]["sittotal"]);
            
            if ($sitcurrent < $sittotal ) {
                //add notifications
                $idreceiver = $event[0]["Event"]["iduser"];
                if( $iduser == $idreceiver ) {
                    if($partecipate->autopartecipate( $idevent , $iduser )){
                        $this->set( 'response',  Utils::generateSuccess( SuccessCodes::OK ) );
                        $conditions['idevent']      = AppModel::getInputData()->idevent;
                        $data['Event.updatetime']   = "'".date('Y-m-d H:i:s')."'"; 
                        $data['Event.sitcurrent']   = 'Event.sitcurrent + 1'; 
                        $this->Event->updateAll( $data, $conditions );
                        $this->Event->clear();
                    } else {
                        $this->set( 'response',  Utils::generateSuccess( SuccessCodes::RS_JOININ_ALREADYJOINED ) );
                    }
                    
                } else {
                    if($partecipate->partecipate( $idevent , $iduser )) 
                    {
                        $this->set( 'response',  Utils::generateSuccess( SuccessCodes::OK ) );
                        $notificationsController    = new NotificationsController();
                        $notificationsController->addNotification ( $iduser, $idreceiver, NotificationsController::NOTIFICATION_TYPE_BOOKING_REQUEST, $idevent );
                        $this->sendPush( $iduser, $idreceiver, "",  NotificationsController::NOTIFICATION_TYPE_BOOKING_REQUEST  );
                    }
                }
            }
            else {
                //restituisce un errore di FULL->evento già pieno
                $this->set( 'response',  Utils::generateSuccess( SuccessCodes::RS_INPUT_ERROR ) );//todo: assegnare un errore corretto
            }
        }
        else {
            $this->set( 'response',  Utils::generateSuccess( SuccessCodes::RS_INPUT_ERROR ) );
        }
    }
    
    public function unpartecipate() {
        if( isset( AppModel::getInputData()->idevent ) && isset( AppModel::getInputData()->iduser )) {
            $partecipate = new PartecipatesController();
            if ( $partecipate->unpartecipate( HTMLDecoder::encode(AppModel::getInputData()->iduser), HTMLDecoder::encode(AppModel::getInputData()->idevent)) ) {
                $conditions['idevent']      = AppModel::getInputData()->idevent;
                $data['Event.updatetime']   = "'".date('Y-m-d H:i:s')."'"; 
                $data['Event.sitcurrent']   = 'Event.sitcurrent - 1'; 
                $this->Event->updateAll( $data, $conditions );
                $this->Event->clear();
                $this->set( 'response',  Utils::generateSuccess( SuccessCodes::OK ) );
            }
            else {
                $this->set( 'response',  Utils::generateSuccess( SuccessCodes::RS_INPUT_ERROR ) );
            }
        }
    }
    
    
    /**
     * Rimozione di un dato evento e dell'immagine di copertina ad esso associata se questa non è quella di default
     */
    public function remove() {
        
        if( isset( AppModel::getInputData()->idevent ) ) {
            $idevent = AppModel::getInputData()->idevent;
            
            $event = $this->Event->find('first', array('conditions' => array('idevent' => $idevent) , 'fields' => array('img')));
            
            
            
            if ($event['Event']['img'] != Utils::URL_BASE.Utils::EVENT_IMAGE_PATH.Utils::EVENT_DEFAULT_IMAGE){
                App::import('Vendor', 'ImageManager');
                ImageManager::$folder = '../../../../'.Utils::EVENT_IMAGE_PATH;
                ImageManager::delete($event['Event']['img']);
            }
            
//            $idevent = AppModel::getInputData()->idevent;
            $partecipates = new PartecipatesController();
            $partecipates->deleteAllPartecipations( $idevent );
            $this->Event->query('DELETE FROM events WHERE idevent = '.$idevent);
            $notificationController = new NotificationsController();
            $notificationController->removeEventNotification($idevent);
            $this->set( 'response',  Utils::generateSuccess( SuccessCodes::OK ) );
        }else {
                $this->set( 'response',  Utils::generateSuccess( SuccessCodes::RS_INPUT_ERROR ) );
            }
    }
    
    
    /**
     * Questa rest va rifatta da capo .... deve prevedere di poter accettare una lista di utenti invitati ad un evento in modo da accomunare gli accessi al db e alle api pushwoosh
     * Per il momento  
     */
    public function invite() {
        if( isset( AppModel::getInputData()->idevent ) && isset( AppModel::getInputData()->iduser ) && isset( AppModel::getInputData()->idreceiver )) {
            $idevent        = AppModel::getInputData()->idevent;
            $iduser         = AppModel::getInputData()->iduser;
            $idreceiver     = AppModel::getInputData()->idreceiver;
            
            
            //get event  info
//            $event = $this->Event->getSingleEventInfo($idevent);
//            $event = $event[0]["Event"];
            /*nel controllo commettato, viene controllato che effettivamente il creatore dell'evento è colui che ha
                inviato l'invito. non essendo previsto, è stato commentato             */
            //if( /*intval($event["iduser"]) == intval($iduser) */true ) {
                //get user sender info
                $userController       = new UsersController(); 
                $user = $userController->getuserinfo( $iduser );
                //check if the user really exists
                if($user != null) {
                    $user = $user["User"];
                    
                    //send notification to the receiver
                    $notificationsController            = new NotificationsController();
                    $notificationsController->addNotification   (   $iduser, 
                                                                    $idreceiver, 
                                                                    NotificationsController::NOTIFICATION_TYPE_EVENT_INVITATION,
                                                                    $idevent
                                                                );
                    
                    
                    
                    $this->sendPush( $iduser, $idreceiver, "",  NotificationsController::NOTIFICATION_TYPE_EVENT_INVITATION  );
                    
                    
                    /**
                     * Salvataggio istanza di invito sul database
                     */
                    
                    $this->loadModel('EventInvitation');
                    $this->EventInvitation->save(array('idsender' => $iduser,
                                                       'idreciver' => $idreceiver, 
                                                       'idevent' => $idevent));
                    unset($this->EventInvitation);
                    
                    /**
                     * Fine asalvataggio istanza di invito
                     */
                    
                    $this->set( 'response',  Utils::generateSuccess( SuccessCodes::OK ) );
                }
                else {
                    $this->set( 'response',  Utils::generateSuccess( SuccessCodes::RS_INPUT_ERROR ) );
                }
                
//            }
//            else {
//                $this->set( 'response',  Utils::generateSuccess( SuccessCodes::RS_INPUT_ERROR ) );
//            }
        }
        else {
            $this->set( 'response',  Utils::generateSuccess( SuccessCodes::RS_INPUT_ERROR ) );
        }
    }
    
//    public function addevent() {
//        if( !isset(AppModel::getInputData()->iduser)        || 
//            !isset(AppModel::getInputData()->idcategory)    || 
//            !isset(AppModel::getInputData()->title)         ||
//            !isset(AppModel::getInputData()->description)   ||
//            !isset(AppModel::getInputData()->img)           ||
//            !isset(AppModel::getInputData()->city)          ||
//            !isset(AppModel::getInputData()->date)          ||
//            !isset(AppModel::getInputData()->hour)          ||
//            !isset(AppModel::getInputData()->address)       ||
//            !isset(AppModel::getInputData()->sex)           ||
//            !isset(AppModel::getInputData()->agemin)        ||
//            !isset(AppModel::getInputData()->agemax)        ||
//            !isset(AppModel::getInputData()->sittotal)      ||
//            !isset(AppModel::getInputData()->latitude)      ||
//            !isset(AppModel::getInputData()->longitude)     ||
//            !isset(AppModel::getInputData()->scope)
//            ) {
//                $this->set( 'response', Utils::generateSuccess( SuccessCodes::RS_INPUT_ERROR ) );
//            } else {
//                $base64Img = AppModel::getInputData()->img;
//                $eventObj = new stdClass();
//                $eventObj->iduser         = HTMLDecoder::encode( AppModel::getInputData()->iduser         );
//                $eventObj->title          = HTMLDecoder::encode( AppModel::getInputData()->title          );
//                $eventObj->idcategory     = HTMLDecoder::encode( AppModel::getInputData()->idcategory     );
//                $eventObj->description    = HTMLDecoder::encode( AppModel::getInputData()->description    );
//                $eventObj->img            = Utils::URL_BASE . Utils::EVENT_IMAGE_PATH . Utils::EVENT_DEFAULT_IMAGE;
//                $eventObj->city           = HTMLDecoder::encode( AppModel::getInputData()->city           );
//                $eventObj->address        = HTMLDecoder::encode( AppModel::getInputData()->address        );
//                $eventObj->date           = HTMLDecoder::encode( AppModel::getInputData()->date           );
//                $eventObj->hour           = HTMLDecoder::hourDecode( HTMLDecoder::encode( AppModel::getInputData()->hour));
//                $eventObj->sex            = HTMLDecoder::encode( AppModel::getInputData()->sex            );
//                $eventObj->latitude       = HTMLDecoder::encode( AppModel::getInputData()->latitude       );
//                $eventObj->longitude      = HTMLDecoder::encode( AppModel::getInputData()->longitude      );
//                $eventObj->agemin         = HTMLDecoder::encode( AppModel::getInputData()->agemin         );
//                $eventObj->agemax         = HTMLDecoder::encode( AppModel::getInputData()->agemax         );
//                $eventObj->sittotal       = HTMLDecoder::encode( AppModel::getInputData()->sittotal       );
//                $eventObj->sitcurrent     = 0;
//                $eventObj->creationdate   = date( "Y-m-d H:i:s" );
//                $eventObj->updatetime     = date( "Y-m-d H:i:s" );
//                $eventObj->activated      = 1;
//                $eventObj->scope          = HTMLDecoder::encode( AppModel::getInputData()->scope          );
//                $canSendPush = true;
//                
//                //se l'evento è privato, non inviare nessuna push notification
//                if(intval($eventObj->scope) != 0) {
//                    $canSendPush = false;
//                }
//                $this->Event->save( $eventObj );
//                
//                
//                $imgPath = Utils::decodeEventBase64Image( $base64Img , $this->Event->id );
//                if( $imgPath == NULL )  {
//                    $imgPath = Utils::URL_BASE . Utils::EVENT_IMAGE_PATH . Utils::EVENT_DEFAULT_IMAGE;
//                }
//                
//                $imgPath = HTMLDecoder::encode($imgPath);
//                $query = "UPDATE events SET img='".$imgPath."' WHERE idevent = ".$this->Event->id;
//                $this->Event->query($query);
//                
//                $responseObj = Utils::generateSuccess( SuccessCodes::OK );
//                $responseObj->idevent = $this->Event->id;
//                $this->set( 'response',  $responseObj );
//                if( $canSendPush ) {
//                    $iduser = AppModel::getInputData()->iduser;
//                    $userFollowsController = new UserFollowsController();
//                    $notificationsController = new NotificationsController();
//                    $followers = $userFollowsController->getFollowers( $iduser  );
//                    $receiverlist = array();
//
//                    foreach ( $followers as $followerSingle ) {
//                        $receiverlist[] = $followerSingle["UserFollow"]["iduser"];
//                        $notificationsController->addNotification   (  $iduser ,
//                                                                        $followerSingle["UserFollow"]["iduser"], 
//                                                                        NotificationsController::NOTIFICATION_TYPE_FOLLOWERS_EVENT_CREATED,
//                                                                        $this->Event->id
//                                                );
//                    }
//                    $this->sendPush( $iduser, $receiverlist, "",  NotificationsController::NOTIFICATION_TYPE_FOLLOWERS_EVENT_CREATED  );
//                }
//                
//            }
//    } 
 
    
    
    /**
     * REST version 2.0 ex self::addevent()
     * Viene creato un nuovo evento ed inviate notifiche e notifiche push ai folllower se l'evento è pubblico
     * Il titolo dell'evento viene trasformato in maiuscolo
     * In output vi è il success 
     */
    public function addevent() {
        
        App::import('Vendor', 'ImageManager');
        ImageManager::$folder = '../../../../'.Utils::EVENT_IMAGE_PATH;
        $tryImage = ImageManager::base64toJpg(AppModel::getInputData()->img);
        
        $imageName = Utils::URL_BASE.Utils::EVENT_IMAGE_PATH.(($tryImage === false) ? Utils::EVENT_DEFAULT_IMAGE : $tryImage);
        
        $this->Event->addEvent($imageName);
        $idEvent = $this->Event->getLastInsertId();
               
        // se l'evento è pubblico invia le notifiche e le notifiche push ai followers
        // questa parte deve essere ottimizzata mediante la creazione di una stored procedure
        if(intval(AppModel::getInputData()->scope) == 0) {
            
            $iduser = AppModel::getInputData()->iduser;
            $userFollowsController = new UserFollowsController();
            $notificationsController = new NotificationsController();
            $followers = $userFollowsController->getFollowers( $iduser  );
            $receiverlist = array();

            foreach ( $followers as $followerSingle ) {
                $receiverlist[] = $followerSingle["UserFollow"]["iduser"];
                $notificationsController->addNotification   (  $iduser ,
                                                                $followerSingle["UserFollow"]["iduser"], 
                                                                NotificationsController::NOTIFICATION_TYPE_FOLLOWERS_EVENT_CREATED, 
                                                                $idEvent
                                        );
            }
            
            $this->sendPush( $iduser, $receiverlist, "",  NotificationsController::NOTIFICATION_TYPE_FOLLOWERS_EVENT_CREATED);
        }
        
        $this->set('response', array('Success' => 1, 'eventid' => "".$idEvent));
    } 
    
    
    
    
    private function sendPush( $idsender, $receiverlist, $message, $type ) {
        $pushcodeController     = new PushcodesController();
        return $pushcodeController->sendPush($type, $idsender, $receiverlist, $message, null, null, null);
    }
    
    
    
  
    public function updateold() {        
        $eventInfo = null;
        if( isset( AppModel::getInputData()->idevent ) ) {
            $userCreatedEvent           = new UserCreatedEventsController();
            $eventInfo                  = $userCreatedEvent->getSingleEventInfo( AppModel::getInputData()->idevent );
            $eventInfo                  = $eventInfo[0]['UserCreatedEvent'];
        }
        if( $eventInfo != null ) {
            $conditions['idevent'] = AppModel::getInputData()->idevent;
            $data = array();
            if( !empty( AppModel::getInputData()->idcategory     )  &&  (AppModel::getInputData()->idcategory >= 0) )  {
                $data['idcategory'] =     '"'.HTMLDecoder::encode( AppModel::getInputData()->idcategory        ).'"' ;
            }
            if( !empty( AppModel::getInputData()->title     ) )  {
                $data['title'] =     '"'.HTMLDecoder::encode( AppModel::getInputData()->title        ).'"' ;
            }
            if( !empty( AppModel::getInputData()->description     ) )  {
                $data['description'] =     '"'.HTMLDecoder::encode( AppModel::getInputData()->description        ).'"' ;
            }
            if( !empty( AppModel::getInputData()->img) && ( AppModel::getInputData()->img != "-1" ) )  {
                //var_dump(AppModel::getInputData()->img);
                $base64Img = HTMLDecoder::encode( AppModel::getInputData()->img );
                $imgPath = Utils::decodeEventBase64Image( $base64Img , AppModel::getInputData()->idevent);
                if( $imgPath == NULL )  {
                    $imgPath = Utils::URL_BASE . Utils::EVENT_IMAGE_PATH . Utils::EVENT_DEFAULT_IMAGE;
                }
                $data['img'] =     '"' . $imgPath . '"' ; 
            }
            if( !empty( AppModel::getInputData()->city     ) )  {
                $data['city'] =     '"'.HTMLDecoder::encode( AppModel::getInputData()->city        ).'"' ;
            }
            if( !empty( AppModel::getInputData()->address     ) )  {
                $data['address'] =     '"'.HTMLDecoder::encode( AppModel::getInputData()->address        ).'"' ;
            }
            if( !empty( AppModel::getInputData()->hour     ) )  {
                $data['hour'] =     '"'.HTMLDecoder::hourDecode(HTMLDecoder::encode( AppModel::getInputData()->hour    )    ).'"' ;
            }
            if( !empty( AppModel::getInputData()->date     ) )  {
                $data['date'] =     '"'.HTMLDecoder::encode( AppModel::getInputData()->date        ).'"' ;
            }
            if( isset( AppModel::getInputData()->sex     ) &&  (AppModel::getInputData()->sex >= 0)  )  {
                $data['sex'] =     '"'.HTMLDecoder::encode( AppModel::getInputData()->sex        ).'"' ;
            }
            if( isset( AppModel::getInputData()->agemin     ) &&  (AppModel::getInputData()->agemin >= 0)  )  {
                $data['agemin'] =     '"'.HTMLDecoder::encode( AppModel::getInputData()->agemin        ).'"' ;
            }
            if( isset( AppModel::getInputData()->agemax     ) &&  (AppModel::getInputData()->agemax >= 0) )  {
                $data['agemax'] =     '"'.HTMLDecoder::encode( AppModel::getInputData()->agemax        ).'"' ;
            }
            if( isset( AppModel::getInputData()->sittotal     ) &&  (AppModel::getInputData()->sittotal >= 0)  ) {
                $data['sittotal'] =     HTMLDecoder::encode( AppModel::getInputData()->sittotal        ) ;
            }
            if( isset( AppModel::getInputData()->latitude     ) &&  (AppModel::getInputData()->latitude != 0) )  {
                $data['latitude'] =     '"'.HTMLDecoder::encode( AppModel::getInputData()->latitude        ).'"' ;
            }
            if( isset( AppModel::getInputData()->longitude     ) &&  (AppModel::getInputData()->longitude != 0) )  {
                $data['longitude'] =     '"'.HTMLDecoder::encode( AppModel::getInputData()->longitude        ).'"' ;
            }
            if( isset( AppModel::getInputData()->scope     ) &&  (AppModel::getInputData()->scope >= 0))  {
                $data['scope'] =     '"'.HTMLDecoder::encode( AppModel::getInputData()->scope        ).'"' ;
            }

            if( $this->isEventConsistent( $eventInfo, $data ) ) {
                $data['updatetime'] = "'".date('Y-m-d H:i:s')."'"; 
                $this->Event->updateAll( $data, $conditions );
                $this->Event->clear();
                $this->set( 'response', Utils::generateSuccess( SuccessCodes::OK ) );
                
                $userPartecipatesController = new UserPartecipateEventsController();
                $notificationsController    = new NotificationsController();
                $partecipants = $userPartecipatesController->getPartecipants( $eventInfo["idevent"]);   
                $receiverlist = array();
                $iduser = $eventInfo["iduser"];
                foreach ( $partecipants as $partecipantSingle ) {
                    $receiverlist[] = $partecipantSingle[$userPartecipatesController->getListName()]["iduserpartecipates"];
                    $notificationsController->addNotification   (   $eventInfo["iduser"], 
                                                                    $partecipantSingle[$userPartecipatesController->getListName()]["iduserpartecipates"], 
                                                                    NotificationsController::NOTIFICATION_TYPE_PARTECIPANTS_EVENT_UPDATE,
                                                                    $eventInfo["idevent"]
                                            );
                }
                $this->sendPush( $iduser, $receiverlist, "",  NotificationsController::NOTIFICATION_TYPE_PARTECIPANTS_EVENT_UPDATE  );
            }
            else {
                $this->set( 'response', Utils::generateSuccess( SuccessCodes::RS_INPUT_ERROR ) );
            }
        }
        else {
            $this->set( 'response', Utils::generateSuccess( SuccessCodes::RS_INPUT_ERROR ) );
        }
    }
    
    /**
     * Modifica di un evento esistente
     * Invia le notifiche
     */
    public function update(){
        
        $event = $this->Event->find('first', array('conditions' => array('idevent' => HTMLDecoder::encode(AppModel::getInputData()->idevent)), 'fields' => array('img', 'sitcurrent')));
        
        if ($event['Event']['sitcurrent'] > AppModel::getInputData()->sittotal){
            $this->set('response', array('Success' => SuccessCodes::RS_INPUT_ERROR));
            return;
        }
        
        App::import('Vendor', 'ImageManager');
        ImageManager::$folder = '../../../../'.Utils::EVENT_IMAGE_PATH;
        $tryImage = ImageManager::base64toJpg(AppModel::getInputData()->img);
        
        // se è stata aggiornata l'immagine
        if ($tryImage !== false){
            $imageName = Utils::URL_BASE.Utils::EVENT_IMAGE_PATH.$tryImage;
            $this->Event->updateEvent($imageName);
            
            // cancella la vecchia immagine solo se non è quella di default
            if ($event['Event']['img'] != Utils::URL_BASE.Utils::EVENT_IMAGE_PATH.Utils::EVENT_DEFAULT_IMAGE){
                ImageManager::delete($event['Event']['img']);
            }
        }
        else{ // altrimenti agggiorna evento e lascia immagine di default
            $this->Event->updateEvent();
        }
        
        // invio delle notifiche -- Da ottimizzare
        
        $iduser = AppModel::getInputData()->iduser;
        $idevent = AppModel::getInputData()->idevent;
        
        $userPartecipatesController = new UserPartecipateEventsController();
        $notificationsController    = new NotificationsController();
        
        $partecipants = $userPartecipatesController->getPartecipants($idevent, true);
        
        // per la selezione dei partecipanti era sufficiente utilizzare la tabella senza scomodare una vista che tra l'altro è assurda :,)
        $receiverlist = array();
       
        foreach ( $partecipants as $partecipantSingle ) {
            $receiverlist[] = $partecipantSingle[$userPartecipatesController->getListName()]["iduserpartecipates"];
            $notificationsController->addNotification   (   $iduser, 
                                                            $partecipantSingle[$userPartecipatesController->getListName()]["iduserpartecipates"], 
                                                            NotificationsController::NOTIFICATION_TYPE_PARTECIPANTS_EVENT_UPDATE,
                                                            $idevent
                                    );
        }
        
        $this->sendPush( $iduser, $receiverlist, "",  NotificationsController::NOTIFICATION_TYPE_PARTECIPANTS_EVENT_UPDATE);
        
        $this->set('response', array('Success' => SuccessCodes::OK));
    }

    private function isEventConsistent( $event, $updatedEvent ) {
        $sitcurrent     = intval( $event["sitcurrent"] );
        $sittotal       = intval( $updatedEvent["sittotal"] );
        if( $sitcurrent > $sittotal ){
            return false;
        } 
        else {
            return true; 
        }
       
    }
    
    public function confirmedpartecipants() {
        if( isset( AppModel::getInputData()->idevent ) ) {
            $idevent       = AppModel::getInputData()->idevent;
            $partecipants = new ConfirmedPartecipatesController();
            $partecipantsList = $partecipants->getList( $idevent );
            $partecipantsArray = array();
            foreach ($partecipantsList as $partecipant) {
                $partecipantsArray[] = $partecipant["ConfirmedPartecipate"];
            }
            
            $responseObj = Utils::generateSuccess( SuccessCodes::OK );
            $responseObj->users = $partecipantsArray;
            $this->set( 'response',  $responseObj );
        }
        else {
            $this->set('response',Utils::generateSuccess( SuccessCodes::RS_INPUT_ERROR ));
        }
    }
    
    /**
     * Costruzione di una lista di eventi
     * @param int $iduser pk di chi inoltra la richiesta
     * @param  string $lastDate data dell'ultimo evento selezionato nella richiesta precedente
     * @param type $searchLimit
     * @param IEventDateListedController $dateListedevents
     * @param IEventListController $listEventsController
     * @return type
     */
    private function getEventList($iduser, $lastDate, $searchLimit, IEventDateListedController $dateListedevents, IEventListController $listEventsController ){
        $total = 0;
        $limitDate = $lastDate;
        
       
        $listedEvents = $dateListedevents->getList( $lastDate, $iduser );
        foreach ( $listedEvents as $singleDate ) {
            $dateOccurance = intval( $singleDate[$dateListedevents->getListName()]["occurance"] );
            $total = $total + $dateOccurance;
            
            // vengono sommati gli eventi di ogni giorno fino al raggiungimento del limite
            if( $total < $searchLimit)  {
                $limitDate = $singleDate[$dateListedevents->getListName()]["date"];
            } 
            else {
                if ( $limitDate == $lastDate) {
                    $limitDate = $singleDate[$dateListedevents->getListName()]["date"];
                }
                break;
            }
        }

        /*
         * In pratica abbiamo in $lastDate il limite inferiore (non incluso) dell'intervallo da considerare
         * ed in $limitDate il limite superiore (incluso) dell'intervallo
         */
        
        $partecipatsController = new PartecipatesController();
        $partecipationList = $partecipatsController->getPartecipationList( AppModel::getInputData()->iduser );
        $partecipationArray = array();
        
        foreach ( $partecipationList as $singlePartecipation ) {   
            $partecipationArray [$singlePartecipation["Partecipate"]["idevent"]] = $singlePartecipation["Partecipate"]["confirmed"];
        }
  
        // $partecipationArray è un array associativo key = idevent, value = confirmed ovvero se l'amministratore ha confermato la partecipazione
        
        // qua finalmente vengono selezionati gli eventi appartenenti all'intervallo selezionato
        $pagedList = $listEventsController->getListPaged( $lastDate, $limitDate, $iduser );
           
        $eventList = new EventList();
        foreach ($pagedList as $singleEvent) {
            $eventList->addEvent( $singleEvent[$listEventsController->getListName()]["date"], $singleEvent[$listEventsController->getListName()], $partecipationArray );
        }

        return $eventList->eventsdayslist;
    }
    
    private function getDistanceList($iduser, $latitude, $longitude, $maxdist, IEventListController $listEventsController ){

        $partecipatsController = new PartecipatesController();
        $partecipationList = $partecipatsController->getPartecipationList( $iduser );
        $partecipationArray = array();
        foreach ( $partecipationList as $singlePartecipation ) {   
            $partecipationArray [$singlePartecipation["Partecipate"]["idevent"]] = $singlePartecipation["Partecipate"]["confirmed"];
        }
  
        $pagedList = $listEventsController->getDistanceList( $iduser, $latitude, $longitude, $maxdist );
        $eventList = new EventList();
        foreach ($pagedList as $singleEvent) {
            $eventList->addEvent( $singleEvent[$listEventsController->getListName()]["date"], $singleEvent[$listEventsController->getListName()], $partecipationArray );
        }

        return $eventList->eventsdayslist;
    }
    
    private function getDistanceListFiltered($iduser, $latitude, $longitude, $maxdist, IEventListController $listEventsController, $filters ){

        $partecipatsController = new PartecipatesController();
        $partecipationList = $partecipatsController->getPartecipationList( $iduser );
        $partecipationArray = array();
        foreach ( $partecipationList as $singlePartecipation ) {   
            $partecipationArray [$singlePartecipation["Partecipate"]["idevent"]] = $singlePartecipation["Partecipate"]["confirmed"];
        }
  
        $distancedList = $listEventsController->getFilteredDistanceList( $iduser, $latitude, $longitude, $maxdist, $filters );
        $eventList = new EventList();
        foreach ($distancedList as $singleEvent) {
            $eventList->addEvent( $singleEvent[$listEventsController->getListName()]["date"], $singleEvent[$listEventsController->getListName()], $partecipationArray );
        }
        return $eventList->eventsdayslist;
    }
    
    private function getMonthlyEventList($iduser, $month, $year, IEventListController $listEventsController ){

        $partecipatsController = new PartecipatesController();
        $partecipationList = $partecipatsController->getPartecipationList( AppModel::getInputData()->iduser );
        $partecipationArray = array();
        foreach ( $partecipationList as $singlePartecipation ) {   
            $partecipationArray [$singlePartecipation["Partecipate"]["idevent"]] = $singlePartecipation["Partecipate"]["confirmed"];
        }
        $pagedList = $listEventsController->getListByMonth(  $year, $month, $iduser );

        $eventList = new MonthlyEventList();
        foreach ($pagedList as $singleEvent) {
            $eventList->addEvent( $singleEvent[$listEventsController->getListName()]["date"] );
        }
        return $eventList->eventsinadaylist;
    }
    
    /*
     * @todo: remove it in the final release
     * @description: only for test
     */
    public function fill() {
        $data = array();
        for( $i=0; $i< 1000; $i++ )
        {
            $data[] = $this->genereteRandomEvent($i);
        }
        $this->Event->saveMany($data);
        $this->set('response', "filled");
    }
    
    /*
     * @todo: remove it in the final release
     * @description: only for test
     */
    public function clear() {
        $this->Event->deleteAll(array('Event.idevent >=' => 0), true);
        $this->set('response', "cleared");
    }
    
    /*
     * @todo: remove it in the final release
     * @description: only for test
     */
    private function genereteRandomEvent($i)
    {
        $object = new stdClass();
        $int = mt_rand(1422745200,
                       1448924400);
        $object->iduser         = mt_rand(1,3);
        $object->title          = "Event ".$i;
        $object->idcategory     = mt_rand(0,5);
        $object->description    = $this->generateString(40);
        $object->img            = HTMLDecoder::encode( "http://lorempixel.com/400/200/" );
        $object->city           = "Bari";
        $object->address        = "Via " . $this->generateString( mt_rand( 5, 15 ) ) . ", ".mt_rand( 1, 300 );
        $object->date           = date("Y-m-d",$int);
        $object->hour           = mt_rand(0,23).":".mt_rand(0,59);
        $object->sex            = mt_rand(0,3);
        //$object->latitude       = mt_rand(41161,41361) / 1000; //cerignola
        //$object->longitude      = mt_rand(15775,15975) / 1000; //cerignola
        //$object->latitude       = mt_rand(41100,41150) / 1000; //bari
        //$object->longitude      = mt_rand(16800,16850) / 1000; //bari
        $object->latitude       = mt_rand(41100,41361) / 1000;
        $object->longitude      = mt_rand(15775,16850) / 1000;
        $object->agemin         = mt_rand(0,30);
        $object->agemax         = mt_rand($object->agemin,99);
        $object->sittotal       = mt_rand(2,99);
        $object->sitcurrent     = mt_rand(0,$object->sittotal);
        $object->creationdate   = date("Y-m-d H:i:s",$int);
        $object->updatetime     = date("Y-m-d H:i:s",$int);
        $object->activated      = mt_rand(0,1);
        $object->scope          = mt_rand(0,2);
        $object->category       = mt_rand(0,8);
        return $object;
    }
    
    /*
     * @todo: remove it in the final release
     * @description: only for test
     */
    private function generateString( $length ) {
        $chars = 'abcdefghijklmnopqrstuvwxyz  ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789   ';
        $count = mb_strlen($chars);

        for ($i = 0, $result = ''; $i < $length; $i++) {
            $index = rand(0, $count - 1);
            $result .= mb_substr($chars, $index, 1);
        }
        return $result;
    }
}