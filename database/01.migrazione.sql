-- FUNZIONI E PROCEDURE

DROP FUNCTION IF EXISTS distanza;
DROP PROCEDURE IF EXISTS confirm_bookings;
DROP PROCEDURE IF EXISTS delete_old_notification;
DROP PROCEDURE IF EXISTS remove_booking_confirmation;
DROP PROCEDURE IF EXISTS save_chat_message;

DELIMITER $$

CREATE FUNCTION `distanza`(store_lat decimal(23,20), store_lon decimal(23,20), user_lat decimal(23,20), user_lon decimal(23,20)) RETURNS decimal(10,0)
    DETERMINISTIC
begin

  declare dist DECIMAL(10);
  
  select (( 1000 * 6371 * acos( cos( radians(user_lat) ) * cos( radians( store_lat ) ) * cos( radians( store_lon ) - radians(user_lon) ) + sin( radians(user_lat) ) * sin( radians( store_lat ) ) ) ) ) into dist;

  return dist;

end $$

DELIMITER ;

DELIMITER $$

CREATE PROCEDURE `confirm_bookings`(id_event_ INT, booking_list_ TEXT)
begin
declare com_list TEXT;  -- lista delle prenotazioni da confermare
declare loc INT; -- posizione del delimitatore nella lista
declare id_booking_ INT; -- pk prenotazione
declare confirmed_ INT(1); -- variabile di lettura di confirmed
declare sit_to_block_ INT; -- numero di posti che vengono bloccati
	
	SET com_list = booking_list_;
	SET sit_to_block_ = 0;
	
	WHILE LENGTH(com_list) > 0 DO  -- c'è almeno una occorrenza di prenotazione da elaborare
            
		SET loc = LOCATE(',', com_list);
		
		IF  loc > 0 THEN -- ci sono almeno due occorrenze
		
			SET id_booking_ = CAST(LEFT(com_list, loc - 1) AS SIGNED);
			SET com_list = SUBSTRING(com_list, loc + 1);
		
		ELSE  -- c'è una sola occorrenza
		
			SET id_booking_ = CAST(com_list AS SIGNED);
			SET com_list = '';
			
		END IF;

		SELECT `confirmed` INTO confirmed_ FROM `partecipates` WHERE `id` = id_booking_;
		
		-- setta la conferma solo alle prenotazioni non ancora confermate
		IF confirmed_ = 0 THEN
		
			UPDATE `partecipates` SET `confirmed` = 1 WHERE `id` = id_booking_;
			SET sit_to_block_ = sit_to_block_ + 1;
		
	    END IF;
		
	END WHILE;
	
	-- aggiornamento dei posti disponibili all'evento
	UPDATE `events` SET `sitcurrent` = `sitcurrent` + sit_to_block_ WHERE `idevent` = id_event_;
	
end $$

DELIMITER ;


DELIMITER $$

CREATE PROCEDURE `delete_old_notification`(idreceiver_ int(11), max_notification_ int(3))
begin
     
	DECLARE done INT DEFAULT FALSE;
	DECLARE a, b INT;
	DECLARE cur1 CURSOR FOR SELECT id FROM notifications where idreceiver = idreceiver_ order by id desc;
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
	
	OPEN cur1;
	SET b = 0;
	
	read_loop: LOOP
	
		SET b = b + 1;
		FETCH cur1 INTO a;
		
		IF done THEN
			LEAVE read_loop;
		END IF;
		
		if (b > max_notification_) then
			delete from notifications where id = a;
		end if;
	end LOOP;
	
	close cur1;
	
end $$

DELIMITER ;

DELIMITER $$

CREATE `remove_booking_confirmation`(id_event_ INT, unbooking_list_ TEXT)
begin
declare com_list TEXT;  -- lista delle prenotazioni da confermare
declare loc INT; -- posizione del delimitatore nella lista
declare id_booking_ INT; -- pk prenotazione
declare confirmed_ INT(1); -- variabile di lettura di confirmed
declare sit_to_free_ INT; -- numero di posti che vengono liberate
	
	SET com_list = unbooking_list_;
	SET sit_to_free_ = 0;
	
	WHILE LENGTH(com_list) > 0 DO  -- c'è almeno una occorrenza di prenotazione da elaborare
            
		SET loc = LOCATE(',', com_list);
		
		IF  loc > 0 THEN -- ci sono almeno due occorrenze
		
			SET id_booking_ = CAST(LEFT(com_list, loc - 1) AS SIGNED);
			SET com_list = SUBSTRING(com_list, loc + 1);
		
		ELSE  -- c'è una sola occorrenza
		
			SET id_booking_ = CAST(com_list AS SIGNED);
			SET com_list = '';
			
		END IF;

		SELECT `confirmed` INTO confirmed_ FROM `partecipates` WHERE `id` = id_booking_;
		
		-- setta la conferma solo alle prenotazioni non ancora confermate
		IF confirmed_ = 1 THEN
		
			UPDATE `partecipates` SET `confirmed` = 0 WHERE `id` = id_booking_;
			SET sit_to_free_ = sit_to_free_ + 1;
		
	    END IF;
		
	END WHILE;
	
	-- aggiornamento dei posti disponibili all'evento
	UPDATE `events` SET `sitcurrent` = `sitcurrent` - sit_to_free_ WHERE `idevent` = id_event_;
	
end $$

DELIMITER;

DELIMITER $$

CREATE PROCEDURE `save_chat_message`(idreceiver_ int(11), idsender_ int(11), message_ TEXT, creation_time_ varchar(45), max_notification_ int(3))
begin
     
	DECLARE done INT DEFAULT FALSE;
	DECLARE a varchar(64);
	DECLARE username_ varchar(100);
	
	DECLARE cur1 CURSOR FOR SELECT hwid FROM pushcodes where iduser = idreceiver_ and os = 0;
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

	SELECT CONCAT(`name`, ' ', `surname`) into username_ from users where iduser = idsender_;
	
	-- inserimento delle notifiche push che potrebbero andare perdute su ios
	
	OPEN cur1;
	
	read_loop: LOOP
	
		FETCH cur1 INTO a;
		
		IF done THEN
			LEAVE read_loop;
		END IF;
		
		INSERT INTO `chats`(`hwid`, `idsender`, `sendername`,`idreceiver`, `message`, `creation_time`) VALUES (a, idsender_, username_, idreceiver_, message_, creation_time_);
	
	end LOOP;
	
	close cur1;
	
    -- inserimento delle notifiche locali
	
	INSERT INTO `notifications`(`type`, `idsender`, `idreceiver`) VALUES (7, idsender_, idreceiver_);
	
	-- eliminazione delle vecchie notifiche locali
	
	call delete_old_notification(idreceiver_, max_notification_);
	
end $$

DELIMITER ;



-- creazione di un repository di risorse che sia interversione

update events set img = replace(img, 'http://www.join-me.it/v1/resources/event/', 'http://www.join-me.it/resources/event/');
update users set img = replace(img, 'http://www.join-me.it/web-service/dev/app/webroot/images/user/', 'http://www.join-me.it/resources/user/');

http://app.join-me.it/resources/event/20150728154930_3536.jpeg