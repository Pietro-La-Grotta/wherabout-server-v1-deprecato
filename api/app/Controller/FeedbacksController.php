<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
App::import('Vendor', 'TCPConnectionManager');
// TCPConnectionManager::flushAndClose(json_encode(array('Success' => SuccessCodes::OK))); 
/**
 * Description of FeedbacksController
 * Gestione delle chiamate relative ai feedback
 * @author pietro@studioleaves.com
 */
class FeedbacksController extends AppController{
    
    /**
     * Data la chiave primaria di un utente, seleziona la lista dei feedback associati all’utente
     */
    public function getall(){
        TCPConnectionManager::flushAndClose(json_encode(array('Success' => SuccessCodes::OK, 
                                                              'feedbacklist' => $this->Feedback->getUserFeedbacks())));
        die();
    }
    
    /**
     * Seleziona la lista degli eventi a cui è possibile lasciare un feedback, ovvero tutti gli eventi:
     * 1) a cui l’utente che invia la richiesta si è prenotato
     * 2) sono già trascorsi
     * 3) a cui l’utente che invia la richiesta non ha ancora lasciato un feedback
     * 4) a cui l’utente che invia la richiesta non ha deciso di non voler lasciare un feedback
     * 5) che non sono stati creati dall’utente che invia la richiesta
     * 
     * Attenzione!!! : L’ordinamento degli eventi è opposto a quello della lista discover della wall. Verranno visualizzati prima gli eventi di ieri, poi avantieri e così via.
     * 
     */
    public function events(){
        
        $input = AppModel::getInputData();
        
        $conditions = array();
        
        $iduser = HTMLDecoder::encode($input->iduser);
        $conditions['iduser !='] = $iduser; // gli eventi non devono essere stati creati dall'utente che invia la richiesta
        $conditions['idpartecipant'] = $iduser; //l'utente che ha inviato la richiesta deve aver fatto la prenotazione all'evento 
        $conditions['refuse'] = NULL; // l'utente che invia la richiesta non deve aver già lasciato un feedback per quell'evento e non deve aver neanche cliccato su "non voglio lasciare un feedback"
        $conditions['date <'] = HTMLDecoder::encode($input->lastdate);  // inizio della pagina richiesta
        
        $limite = $this->Feedback->limite($conditions, $input->limit);
        
        if ($limite === false){
            TCPConnectionManager::flushAndClose(json_encode(array('Success' => SuccessCodes::OK, 'eventsdayslist' => array())));
            die();
        }
        
        $conditions['date >='] = $limite; // fine della pagina richiesta
        
        TCPConnectionManager::flushAndClose(json_encode(array('Success' => SuccessCodes::OK, 'eventsdayslist' => $this->Feedback->eventsToFeedback($conditions))));
        die();
    }
    
    /**
     * Data la pk dell’evento, seleziona le informazioni necessarie per la costruzione della dialog contenente il form per l’inserimento del feedback
     */
    public function forminfo(){
        
        $idevent = HTMLDecoder::encode(AppModel::getInputData()->idevent);
        
        $this->loadModel('Event');
        $raw = $this->Event->find('first', array('conditions' => array('idevent' => $idevent),
                                                 'fields' => array('iduser', 'title')));
        unset($this->Event);
        
        $idplanner = $raw['Event']['iduser'];
        $eventTitle = HTMLDecoder::decode($raw['Event']['title']);
        
        $this->loadModel('User');
        $planner = $this->User->find('first', array('conditions' => array('iduser' => $idplanner), 
                                              'fields' => array("concat(name, ' ', surname) as plannername", 'up_feedback', 'down_feedback')));
        
        TCPConnectionManager::flushAndClose(json_encode(array('Success' => SuccessCodes::OK, 
                                                              'idplanner' => $idplanner,
                                                              'plannerName' => HTMLDecoder::decode($planner[0]['plannername']),
                                                              'idevent' => $idevent,
                                                              'eventTitle' => $eventTitle,
                                                              'upFeedback' => $planner['User']['up_feedback'], 
                                                              'downFeedback' => $planner['User']['down_feedback'])));
        die();
        
    }
    
    /**
     * L’utente decide di non voler mai inserire un feedback per un dato evento a cui si è prenotato
     * Viene inserita nella tabella feedback una tupla per tenere traccia dell'informazione
     */
    public function dontsend(){
        
        $this->Feedback->refuseFeedback();
        TCPConnectionManager::flushAndClose(json_encode(array('Success' => SuccessCodes::OK)));
        die();
    }
    
    /**
     * L’utente invia un feedback
     */
    public function send(){
        
        $input = AppModel::getInputData(); 
        $this->Feedback->addFeedback();
        
        $vote = intval($input->vote);
        $feedback = array();
        
        if ($vote == 0) $feedback['User.down_feedback'] = 'User.down_feedback+1';
        if ($vote == 1) $feedback['User.up_feedback'] = 'User.up_feedback+1';
        
        $this->loadModel('User');
        $this->User->updateAll($feedback, array('iduser' => $input->idplanner));
        
        TCPConnectionManager::flushAndClose(json_encode(array('Success' => SuccessCodes::OK)));
        die();
    }
    
    /**
     * All’avvio l’app invia una richiesta al server per sapere se deve proporre all’utente l’inserimento di un feedback
     */
    public function advice(){
        
        $event = $this->Feedback->mostRecentToFeedback();
        
        $response = ($event === false) // non ci sono eventi per cui consigliare di lasciare il feedback
                  ? array('Success' => SuccessCodes::OK, 
                          'idevent' => 0, 
                          'eventTitle' => '')
                                       // c'è un evento per cui consigliare di lasciare un feedback               
                  : array('Success' => SuccessCodes::OK, 
                          'idevent' => $event['idevent'], 
                          'eventTitle' => HTMLDecoder::decode($event['title']));
            
        TCPConnectionManager::flushAndClose(json_encode($response));
        
        die();
    }
}

?>
