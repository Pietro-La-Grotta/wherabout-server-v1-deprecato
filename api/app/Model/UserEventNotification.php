<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Event
 *
 * @author Christian
 */
App::uses('AppModel', 'Model');
class UserEventNotification extends AppModel{
    
    public function getAllNotifications( $idreceiver) {
        return $this->find( 'all', array( 'conditions' => array('idreceiver' => $idreceiver), 'order' => array('date DESC') ) );
    }
     
    
}
