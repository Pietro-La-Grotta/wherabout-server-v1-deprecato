<?php


/**
 * PushController is the main controller for push based request and it's used by :
 *  - UserController
 *
 * @author Christian Ruggiero
 *@creation 24/11/2014
 * @status in develop
 */
class UserFriendsController extends AppController{
    public function getSingleUserInfo( $iduser ) {
        return $this->UserFollow->getSingleUserInfo( $iduser );
    }
    
    /**
     * Quando un utente invia una richiesta di amicizia ad un altro utente vengono inserite due tuple nel db, 
     * una [sender, receiver, status = 0 (richiesta di amicizia inviata ed in attesa di accettazione)], 
     * l'altra [receiver, sender status = 2 (richiesta di amicizia ricevuta e che può essere accettata o rifiutata)]
     *   
     * @param int $iduser sender pk (chi fa la richiesta di amicizia)
     * @param int $iduserfriend receiver pk (chi riceve la richiesta)
     * @return se null io dati in input non sono settati
     */
    public function friend( $iduser, $iduserfriend ) {
        if( isset( $iduser ) && isset( $iduserfriend ) ) {
            $userObj = new stdClass();
            $userObj->iduser = $iduser;
            $userObj->idfriend = $iduserfriend;
            $userObj->accepted = "0";
            
            $friendObj = new stdClass();
            $friendObj->iduser = $iduserfriend;
            $friendObj->idfriend = $iduser;
            $friendObj->accepted = "2";
            
            $updateArray = array();
            $updateArray[] = $userObj;
            $updateArray[] = $friendObj;
            
            return $this->UserFriend->saveMany($updateArray);
        }
        else {
            return null;
        }
    }
    
    
    public function unfriend( $iduser, $iduserfriend ) {
        if( isset( $iduser ) && isset( $iduserfriend ) ) {
            return $this->UserFriend->deleteFriend( $iduser, $iduserfriend );
        }
        else {
            return false;
        }
    }
    
    
    public function userFriendship( $iduser, $iduserfriend ) { 
        if ( isset($iduser) && isset($iduserfriend) && ( intval( $this->UserFriend->find( 'count', array( 'conditions' => array( 'iduser' => $iduser, 'idfriend' => $iduserfriend) ) ) > 0 ) ) ) {
            return TRUE;
        } 
        else {
            return FALSE;
        }
    }
    
    public function userFriendshipStatus( $iduser, $iduserfriend ) {  
        if ( isset($iduser) && isset($iduserfriend) )  {
            $friendship = $this->UserFriend->find( 'first', array( 'conditions' => array( 'iduser' => $iduser, 'idfriend' => $iduserfriend) ) );   
            if($friendship == null) { 
                return 9999;
            }
            else {
                return intval( $friendship["UserFriend"]["accepted"] );
            }
        } 
        else {
            return null;
        }
    }
    
    public function userFriends( $iduser ) { 
        if ( isset($iduser) ) {
            return intval( $this->UserFriend->find( 'count', array( 'conditions' => array( 'iduser' => $iduser ) ) ) ); 
        } 
        else {
            return 0;
        }
    }
    
    /**
     * Lo status delle tuple inserite in user_friends al momento dell'invio della richiesta di amicizia viene settato ad 1 (richiesta di amicizia attiva) 
     * @param int $iduser receiver pk (utente che ha ricevuto la richiesta di amicizia e che la ha accettata)
     * @param int $iduserfriend sender pk (utente che ha inviato la richiesta di amicizia e che è in attesa di accettazione)
     * @return null se i dati in input non sono settati
     */
    public function accept($iduser, $iduserfriend) {
        if( isset( $iduser ) && isset( $iduserfriend ) ) {
            return $this->UserFriend->acceptFriend( $iduser, $iduserfriend );
        }
        else {
            return null;
        }
    }
    
}
