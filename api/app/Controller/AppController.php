<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses(      'Controller',   'Controller' );
App::import(    'Model',        'AppModel' );
App::import(    'Vendor',       'HTMLDecoder' );
App::import(    'Vendor',       'Utils' );

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
    
     /**
     * Nome della variabile in post contenente il json in input
     */
    const INPUT_VAR_NAME = "toServer";
    
    /**
     * Decodifica json e set layout ajax
     */
    public function beforeFilter() {
        
        parent::beforeFilter();
        
        $this->storeRequest();
        
        $superObject = json_decode($this->request->data[self::INPUT_VAR_NAME]);

        // Ignezione dei dati in input nel model layer e nel filtro della lingua
        AppModel::setIdUser($superObject->authKey);
        AppModel::setLanguage($superObject->lang);
        
        // @todo authKey check
        AppModel::setInput(($superObject->data != "") ? $superObject->data : null);
        
        $this->layout = "ajax";
       
        unset($this->request->data[self::INPUT_VAR_NAME]); 
         
    }
    
    /**
     * Salvataggio della richiesta in input sul db a scopo di debug
     */
    private function storeRequest(){
        if (isset($this->request->data[self::INPUT_VAR_NAME])){
           
            $this->loadModel("Requestlog");

            $this->Requestlog->set(array('url' => Router::url( $this->here, true ), 'json' => HTMLDecoder::encode($this->request->data[self::INPUT_VAR_NAME])));
            $this->Requestlog->save();
        
            unset($this->Requestlog);
        }  
    }
}
