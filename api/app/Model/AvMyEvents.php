<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AvMyEvents
 * Associa agli eventi info riguardo al fatto che il creatore di ogni evento vi partecipi o meno
 * @author pietro@studioleaves.com
 */
class AvMyEvents extends AppModel{
    
    /**
     * SQL vista
     * 
     * create view av_my_events as 
       select e.*, if(p.confirmed is null, 999, p.confirmed) partecipate, u.img userimg 
       from `events` e join `users` u on e.iduser = u.iduser 
                  left join partecipates p on e.`idevent` = p.`idevent` and e.`iduser` = p.`iduser`;
     */
    
    
    /**
     * Seleziona una pagina di eventi creati dall'utente e li organizza nella struttura dati di output richiesta
     * @param date $sup limite superiore dell'intervallo di date di appartenenza degli eventi
     * @return mixed
     */
    public function select($sup){
        
        $raw = $this->find('all', array('conditions' => array('date > ' => self::$inputData->lastdate, 
                                                              'date <= ' => $sup, 
                                                              'iduser' => self::$inputData->iduser,
                                                              'activated' => 1
                                                              ),
                                        'fields' => array('idevent', 'iduser', 'sex', 'sittotal', 'sitcurrent', 'idcategory', 'title', 'city', 'address', 'agemin', 'agemax', 'date', 'hour', 'scope', 'userimg', 'partecipate', 'latitude', 'longitude', 'img as imgcover'),
                                        'order' => array('date ASC')));
        
        $focus = '0000-00-00';
        $events = array();
        
        foreach ($raw as $r){
            // decodifica campi codificati
            $r['AvMyEvents']['title'] = HTMLDecoder::decode($r['AvMyEvents']['title']);
            $r['AvMyEvents']['city'] = HTMLDecoder::decode($r['AvMyEvents']['city']);
            $r['AvMyEvents']['address'] = HTMLDecoder::decode($r['AvMyEvents']['address']);
            
            if ($r['AvMyEvents']['date'] != $focus){
                // nuova partizione giorno
                $focus = $r['AvMyEvents']['date'];
                $events[] = array('date' => $focus, 'eventlistinfo' => array()); 
            }
            // inserimento evento nel set partizionato per giorni
            $events[count($events) - 1]['eventlistinfo'][] = $r['AvMyEvents'];
        }
        
        return $events;
    }
}

?>
