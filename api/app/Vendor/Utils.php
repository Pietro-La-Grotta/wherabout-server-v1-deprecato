<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Utils
 *
 * @author Christian
 */

App::uses('File', 'Utility');
App::uses('Folder', 'Utility');
class Utils {
//    //pushwoosh
//    const PW_APPID                      = "C0450-6D982";
//    const PW_AUTH                       = "qweasdzxc";
    const PW_SHOTRMESSAGE_LENGTH        = 20;
    //sitebase url
    //const URL_BASE                      = "http://www.join-me.it/web-service/dev/app/webroot/";
    const URL_BASE                      = "http://www.join-me.it/";
    //image needs
    const IMAGE_EXTENSION               = ".jpg";
    //users
    const USER_DEFAULT_IMAGE            = "avatardefault.png";
    const USER_IMAGE_PATH               = "resources/user/";
    //event
    const EVENT_DEFAULT_IMAGE           = "default.jpg";
    const EVENT_IMAGE_PATH              = "resources/event/";
    
    /**
     * Massimo numero di item che può contenere la lista degli utenti a cui è possibile inoltrare una richiesta di amicizia
     */
    const UNFRIEND_LIST_LEN = 20;
    
    
    
    /*Stampare l'ultima query*/
    /*
        $dbo = $this->getDatasource();
        $logs = $dbo->getLog();
        $lastLog = end($logs['log']);
        var_dump($lastLog['query']); 
     
     */
    
    public static function getDistanceFromCoords($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo ) {
        return 1000 * 6371 * acos( cos( deg2rad(floatval($latitudeFrom)) ) * cos( deg2rad(floatval($latitudeTo)) ) * cos( deg2rad($longitudeTo) - deg2rad(floatval($longitudeFrom)) ) + sin( deg2rad(floatval($latitudeFrom)) ) * sin( deg2rad(floatval($latitudeTo)) ) ) ;
    }
    
    public static function decodeUserBase64Image( $base64Img, $outputfile ){
        if( !isset($base64Img) || $base64Img == null || $base64Img == "" || $base64Img == "null" || $base64Img == "-1")  
        {
            $base64Img = Utils::USER_IMAGE_DEFAULT_BASE_64;
        }
        
        $file = new File( Utils::USER_IMAGE_PATH . $outputfile . Utils::IMAGE_EXTENSION, true );
        $data = base64_decode( $base64Img );
        if( !$file->open( "wb" )                    )   return Utils::URL_BASE . Utils::USER_IMAGE_PATH . Utils::USER_DEFAULT_IMAGE;
        if( !$file->write( $data, "wb" , true ) )       return Utils::URL_BASE . Utils::USER_IMAGE_PATH . Utils::USER_DEFAULT_IMAGE;
        $file->close();
        return Utils::URL_BASE . Utils::USER_IMAGE_PATH . $outputfile . Utils::IMAGE_EXTENSION;
    }
    
    public static function decodeEventBase64Image( $base64Img, $outputfile ){
        if( !isset($base64Img) || $base64Img == null || $base64Img == "" || $base64Img == "null" || $base64Img == "-1")  {
            return Utils::URL_BASE . Utils::EVENT_IMAGE_PATH . Utils::EVENT_DEFAULT_IMAGE;
        }
        $file = new File( Utils::EVENT_IMAGE_PATH . $outputfile . Utils::IMAGE_EXTENSION, true );
        $data = base64_decode( $base64Img );
        if( !$file->open( "wb" )                    )   {
            return Utils::URL_BASE . Utils::EVENT_IMAGE_PATH . Utils::EVENT_DEFAULT_IMAGE;
        }
        if( !$file->write( $data, "wb" , true ) )  {
            return Utils::URL_BASE . Utils::EVENT_IMAGE_PATH . Utils::EVENT_DEFAULT_IMAGE;
        }
        $file->close();
        return Utils::URL_BASE . Utils::EVENT_IMAGE_PATH . $outputfile . Utils::IMAGE_EXTENSION;
    }
    
    public static function encodePassword( $rawData ) {
        return md5(md5($rawData));
    }

    public static function generatePassword( $length ) {
        $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        $count = mb_strlen($chars);

        for ($i = 0, $result = ''; $i < $length; $i++) {
            $index = rand(0, $count - 1);
            $result .= mb_substr($chars, $index, 1);
        }

        return $result;
    }
    //email type
    const EMAIL_PASSOWRD_RESET = 0;
    //const SENDER_EMAIL = 'pietro@studioleaves.com';
    
    /**
     * Funzione per l'inviop delle email agli
     * @param type $type
     * @param type $recipient
     * @param type $vars
     */
    public static function sendEmail ( $type, $recipient, $vars = null ) {
        App::uses('CakeEmail', 'Network/Email');
        $Email = new CakeEmail();
        switch ($type) {
            case Utils::EMAIL_PASSOWRD_RESET:
                $Email->template("default", "forgottenPwd")
                ->emailFormat('html')
                ->from(array('pietro@studioleaves.com' => 'Whereabout'))
                ->to($recipient)
                ->subject("Forgotten Password")
                ->viewVars($vars)
                ->send();
                break;
            default :
                //do nothing
                break;
        }
        
        
  }
    
    public static function generateSuccess( $successCode ) {
        $object = new stdClass();
        $object->Success = "";
        switch ($successCode) {
            case SuccessCodes::RS_REGISTRATIONOK:
                $object->Success = SuccessCodes::RS_REGISTRATIONOK;
                break;
            case SuccessCodes::OK:
                $object->Success = SuccessCodes::OK;
                break;
            case SuccessCodes::ALL_USER_DELETED:
                $object->Success = SuccessCodes::ALL_USER_DELETED;
                break;
            case SuccessCodes::PASSWORD_RESET_OK:
                $object->Success = SuccessCodes::PASSWORD_RESET_OK;
                break;
            case SuccessCodes::RS_REGISTRATION_DUPLICATE:
                $object->Success = SuccessCodes::RS_REGISTRATION_DUPLICATE;
                break;
            case SuccessCodes::RS_INPUT_ERROR:
                $object->Success = SuccessCodes::RS_INPUT_ERROR;
                break;
            case SuccessCodes::RS_REGISTRATION_PUSHCODE_ERROR:
                $object->Success = SuccessCodes::RS_REGISTRATION_PUSHCODE_ERROR;
                break;
            case SuccessCodes::RS_LOGIN_USERNOTEXIST:
                $object->Success = SuccessCodes::RS_LOGIN_USERNOTEXIST;
                break;
            case SuccessCodes::RS_LOGIN_PUSHCODE_ERROR:
                $object->Success = SuccessCodes::RS_LOGIN_PUSHCODE_ERROR;
                break;
            case SuccessCodes::RS_LOGIN_PASSWORDWRONG:
                $object->Success = SuccessCodes::RS_LOGIN_PASSWORDWRONG;
                break;
            case SuccessCodes::RS_LOGIN_OK:
                $object->Success = SuccessCodes::RS_LOGIN_OK;
                break;
            case SuccessCodes::RS_JOININ_ALREADYJOINED:
                $object->Success = SuccessCodes::RS_JOININ_ALREADYJOINED;
                break;
            case SuccessCodes::RS_JOININ_NOTBOOKED:
                $object->Success = SuccessCodes::RS_JOININ_NOTBOOKED;
                break;
            default :
                $object->Success = SuccessCodes::RS_INPUT_ERROR;
                break;
        }
        return $object;
    }
}

class SuccessCodes {
    const OK                                = 1;
    const RS_INPUT_ERROR                    = 2;
    const USER_USERUPDATED                  = 100;
    const PASSWORD_RESET_OK                 = 101;
    const ALL_USER_DELETED                  = 102;
    
    //registration 
    const RS_REGISTRATION_PUSHCODE_ERROR    = 3;
    const RS_REGISTRATIONOK                 = 5;
    const RS_REGISTRATION_DUPLICATE         = 6;
    
    //login
    const RS_LOGIN_OK                       = 7;
    const RS_LOGIN_USERNOTEXIST             = 8;
    const RS_LOGIN_PASSWORDWRONG            = 9;
    const RS_LOGIN_PUSHCODE_ERROR           = 10;
    const BANNED_USER                       = 103;
    
    //join event
    const RS_JOININ_ALREADYJOINED           = 11;
    const RS_JOININ_NOTBOOKED               = 12;
    
    // la richiesta all'api di google per il ritrovamento delle coordinate gps ha prodotto empty set
    const ADDRESS_NOT_FOUND = 123;
    
    // la richiesta all'api di google non è stata accolta (magari perchè è scaduta l'API_KEY)
    const GEOCODING_SERVICE_ERROR = 124;
}

