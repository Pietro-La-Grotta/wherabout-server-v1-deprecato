<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AvPartecipantCodes
 * Accesso ai push code dei partecipanti agli eventi
 * @author pietro@studioleaves.com
 */
class AvPartecipantCodes extends AppModel{
    
    /**
     * Selziona i push codes dei partecipanti ad un dato evento
     * @return array
     */
    public function getPartecipantCodes(){
        $raw = $this->find('all', array('conditions' =>array('idevent' => HTMLDecoder::encode(self::$inputData->idevent),
                                                             'idpartecipant != ' => HTMLDecoder::encode(self::$inputData->iduser)),
                                        'fields' => array('pushcode')));
        
        $partecipantcodes = array();
        
        foreach ($raw as &$r){
            $partecipantcodes[] = $r['AvPartecipantCodes']['pushcode'];
        }
        
        return $partecipantcodes;
    }
}

?>
