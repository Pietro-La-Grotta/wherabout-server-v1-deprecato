<?php

App::uses('DispatcherFilter', 'Routing');

/**
 * Controlla se la richiesta è stata effettuata tramite mobile
 *
 * @package Cake.Routing.Filter
 */
class MobileRequest extends DispatcherFilter {

/**
 * Default priority for all methods in this filter
 * This filter should run before the request gets parsed by router
 *
 * @var integer
 */
	public $priority = 1;

/**
 * Controlla se la richiesta è stata effettuata tramite mobile
 *
 * @param CakeEvent $event containing the request and response object
 * @return CakeResponse if the client is requesting a recognized asset, null otherwise
 */
	public function beforeDispatch(CakeEvent $event) {
            $request = new CakeRequest();
            if (!$request->is('mobile')) throw new BadRequestException("Not mobile request"); 
        }


}
