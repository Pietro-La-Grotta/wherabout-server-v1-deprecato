<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ServicesController
 * Contiene una collezione di servizi aggiunti
 * @author pietro@studioleaves.com
 */
class ServicesController extends AppController{
    
    /**
     * Selezione delle coordinate gps dato un indirizzo in formato testuale
     */
    public function getgps(){
        App::uses('HttpSocket', 'Network/Http');
        
        $HttpSocket = new HttpSocket();
        $results = $HttpSocket->get('http://maps.googleapis.com/maps/api/geocode/json', 'address='.urlencode(AppModel::getInputData()->address));
        $data = json_decode($results->body);
        
        // l'indirizzo inserito non esiste
        if ($data->status == "ZERO_RESULTS"){
            $this->set('response', array('Success' => SuccessCodes::ADDRESS_NOT_FOUND, 'geography' => NULL));
            
            return;
        }
        
        // l'indirizzo inserito esiste. Se c'è ambiguità tra più indirizzi, viene selezionato il primo della lista
        if ($data->status == "OK"){
            $this->set('response', array('Success' => SuccessCodes::OK, 
                                         'geometry' => array('latitude' => "".$data->results[0]->geometry->location->lat,
                                                             'longitude' => "".$data->results[0]->geometry->location->lng,
                                                             'formatted' => "".$data->results[0]->formatted_address
                                                            )
                                        )
                       );
            
            return;
        }
        
        // è possibile che il servizio sia scaduto o non più raggiungibile
        $this->set('response', array('Success' => SuccessCodes::GEOCODING_SERVICE_ERROR, 'geography' => NULL));
    }
    
    /**
     * Seleziona il contenuto della pagina termini e condiozioni
     */
    public function terms(){
        $this->loadModel('Content');
        $this->set('response', array('Success' => SuccessCodes::OK, 
                                    'html' => $this->Content->getTermsConditions()));
        unset($this->Content);
    }
    
    /**
     * Seleziona il contenuto della pagina termini e condiozioni
     */
    public function privacy(){
        $this->loadModel('Content');
        $this->set('response', array('Success' => SuccessCodes::OK, 
                                     'html' => $this->Content->getInfoPrivacy()));
        unset($this->Content);
    }
}

?>
