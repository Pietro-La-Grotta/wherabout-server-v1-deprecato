<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Event
 * Accesso alle informazioni relative agli eventi
 * @author Christian, Pedro
 */
//App::uses('AppModel', 'Model');
class Event extends AppModel{
    
    public function getListPaged( $lastdate, $limitdate ) {
        return $this->find( 'all', array( 'conditions' => array('date > ' => $lastdate, 'date <= ' => $limitdate) ) );
    }
    
    public function getListByMonth( $year, $month ) {
        
        if( isset($year) && isset($month) ) {
            $yearMin = $year;
            $monthMin = $month;
            
            $yearMax = $year;
            $monthMax = $month + 1;
            if( $monthMax > 12) {
                $monthMax = 1;
                $yearMax++;
            }
            
            $dateMin = $yearMin."-".$monthMin."-"."00";
            $dateMax = $yearMax."-".$monthMax."-"."00";
            return $this->find( 'all', array( 'conditions' => array( 'date >' => $dateMin, 'date <' => $dateMax, "activated" => 1, "scope" => 0) ) );
        }
        else {
            return null;
        }
    }
     
    public function getSingleEventInfo( $idevent ) {
        
        if( isset($idevent)) {
            return $this->find( 'all', array( 'conditions' => array( 'idevent' => $idevent ) ) );
        }
        else {
            return null;
        }
    }
    
    /**
     * Aggiunge un nuovo evento
     * Si occupa della codifica degli attributi
     * @param string $imageUrl immagine associata all'evento
     */
    public function addEvent($imageUrl){
        $this->set(array('iduser' => HTMLDecoder::encode(self::$inputData->iduser),
                          'title' => HTMLDecoder::encode(strtoupper(self::$inputData->title)),
                          'idcategory' => HTMLDecoder::encode(self::$inputData->idcategory),
                          'description' => HTMLDecoder::encode(self::$inputData->description),
                          'img' => $imageUrl,
                          'city' => HTMLDecoder::encode(self::$inputData->city),
                          'address' => HTMLDecoder::encode(self::$inputData->address), 
                          'date' => HTMLDecoder::encode(self::$inputData->date),
                          'hour' => HTMLDecoder::hourDecode(HTMLDecoder::encode(self::$inputData->hour)), 
                          'sex' => HTMLDecoder::encode(self::$inputData->sex),
                          'latitude' => HTMLDecoder::encode(self::$inputData->latitude),
                          'longitude' => HTMLDecoder::encode(self::$inputData->longitude),
                          'agemin' => HTMLDecoder::encode(self::$inputData->agemin),
                          'agemax' => HTMLDecoder::encode(self::$inputData->agemax),
                          'sittotal' => HTMLDecoder::encode(self::$inputData->sittotal),
                          'creationdate' => (object)array('type' => 'expression', 'value' => 'NOW()'),
                          'activated' => 1,
                          'scope' => HTMLDecoder::encode(self::$inputData->scope),
                          'event_grade' => HTMLDecoder::encode(self::$inputData->event_grade)
                        ));
        
        $this->save();
    }
    
    /**
     * Modifica degli attributi di un evento
     * Si occupa della codifica degli attributi
     * @param string $imageUrl immagine associata all'evento
     */
    public function updateEvent($imageUrl = null){
        
        $query = "UPDATE `events` SET `iduser` = ".HTMLDecoder::encode(self::$inputData->iduser).",
                                      `idcategory` = ".HTMLDecoder::encode(self::$inputData->idcategory).",
                                      `title` = '".HTMLDecoder::encode(strtoupper(self::$inputData->title))."', 
                                      `description` = '".HTMLDecoder::encode(self::$inputData->description)."',
                                      ".(isset($imageUrl) ? "`img` = '".$imageUrl."'," : "")."
                                      `city` = '".HTMLDecoder::encode(self::$inputData->city)."',
                                      `address` = '".HTMLDecoder::encode(self::$inputData->address)."',
                                      `date` = '".HTMLDecoder::encode(self::$inputData->date)."',
                                      `hour` = '".HTMLDecoder::hourDecode(HTMLDecoder::encode(self::$inputData->hour))."',
                                      `sex` = ".HTMLDecoder::encode(self::$inputData->sex).",
                                      `agemin` = ".HTMLDecoder::encode(self::$inputData->agemin).",
                                      `agemax` = ".HTMLDecoder::encode(self::$inputData->agemax).",
                                      `latitude` = '".HTMLDecoder::encode(self::$inputData->latitude)."',
                                      `longitude` = '".HTMLDecoder::encode(self::$inputData->longitude)."',
                                      `sittotal` = ".HTMLDecoder::encode(self::$inputData->sittotal).",    
                                      `scope` = ".HTMLDecoder::encode(self::$inputData->scope).",
                                      `event_grade` = ".HTMLDecoder::encode(self::$inputData->event_grade)." 
                  WHERE `idevent` = ".HTMLDecoder::encode(self::$inputData->idevent);
        
        $this->query($query);
    }
}
