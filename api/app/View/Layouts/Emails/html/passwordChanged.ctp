<?php
/**
 *
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts.Email.html
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">
<html>
<head>
	<title>Wherabout Email</title>
</head>
<body>
    <div style="width: 100%;">
        <div style="width: 70%;margin-left:15%;text-align: justify;">
            <div>
                <h1>WhereAbout</h1>
                <h3>Hi, <?php echo strtoupper($username)?>!!!</h3>
                <p>
                    Your password to access at Wherabout has been modified by system.<br>
                    You can continue to use all Wherabout services accessing with new password: <strong><?php echo $password; ?></strong>
                    To change this password you can access into Wherabout and select Update Profile into your profile page.<br> 
                </p>
              
                <p>
                    Thanks to use Wherabout
                </p>
            <div>
        </div>
    </div>
</body>
</html>