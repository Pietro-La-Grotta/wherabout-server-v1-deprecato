<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UsernameCell
 * Cognome e nome dell'utente con collegamnento al dettaglio
 * @author pietro@studioleaves.com
 */
class UsernameCell implements ICell{
     
    private static $menu = 'index.php/utenti?show=edit&item=';
    private static $title = 'Vai al profilo utente';

    private $tuple;
    private $id_label_field;
    private $surname_label_field;
    private $name_label_field;
   
    /**
     * Inizializza le variabili di istanza
     * @param string $id_label_field - etichetta dell'attributo della tupla utente contenente la pk
     * @param string $surname_label_field - etichetta dell'attributo della tupla utente contenente il cognome
     * @param string $name_label_field - etichetta dell'attributo della tupla utente contenente il nome 
     */
    function __construct($id_label_field, $surname_label_field, $name_label_field) {
        $this->id_label_field = $id_label_field;
        $this->surname_label_field = $surname_label_field;
        $this->name_label_field = $name_label_field;
    }

    /**
     * Stampa il contenuto html
     * @return string
     */
    public function html() {
        $html = "";
            
            $username = $this->tuple[$this->surname_label_field].' '.$this->tuple[$this->name_label_field];
            
            $html .= '<td>';
                $html .= '<a href="'.Juri::base().self::$menu.$this->tuple[$this->id_label_field].'" title="'.self::$title.'" target="_blank">';
                $html .= $username;
                $html .= '</a>';
            $html .= '</td>';
            
        return $html;
    }

    /**
     * Setter per la tupla
     * @param array $tuple
     */
    public function data(&$tuple) {
        $this->tuple = $tuple;
    }    
}

?>
