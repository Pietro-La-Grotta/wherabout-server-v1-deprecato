<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UusersController
 * Versione più snella di users controller 
 * @author pietro@studioleaves.com
 */
class UusersController extends AppController{
    
    /**
     * Registrazione di un nuovo utente
     * Salvataggio dati utente, salvataggio avatar, salvataggio dati per pushwoosh
     */
    public function register(){
        $this->loadModel('User');
       
        if ($this->User->emailExists()){
            $this->set( 'response', array('Success' => SuccessCodes::RS_REGISTRATION_DUPLICATE));
            return;
        }
        
        // registrazione con facebook
        if ($this->User->registerByFacebook()){
            // facebook id duplicato
            if ($this->User->facebookExists()){
                $this->set( 'response', array('Success' => SuccessCodes::RS_REGISTRATION_DUPLICATE));
                return;
            }
        }
        // registrazione tramite google
        elseif ($this->User->registerByGoogle()){
            // google id duplicato
            if ($this->User->googleExists()){
                $this->set( 'response', array('Success' => SuccessCodes::RS_REGISTRATION_DUPLICATE));
                return;
            }
        }
        
        $input =  AppModel::getInputData();
        
        // Avatar
        App::import('Vendor', 'ImageManager');
        ImageManager::$folder = '../../../../'.Utils::USER_IMAGE_PATH;
        $tryImage = ImageManager::base64toJpg($input->img);
        
        $imageUrl = Utils::URL_BASE.Utils::USER_IMAGE_PATH.(($tryImage === false) ? Utils::USER_DEFAULT_IMAGE : $tryImage);
        // salvataggio utente
        $iduser = $this->User->addUser($imageUrl);
        
        //momentaneamente mettiamo l'immagine con l'id
        //ImageManager::duplicate((($tryImage === false) ? Utils::USER_DEFAULT_IMAGE : $tryImage), $iduser);
        
        unset($this->User);
        
        // salvataggio push code
        // qua però andrebbe fatta una stored procedure
     
        App::import('Controller',   'Pushcodes');
        $pushcodeController = new PushcodesController();
        
        if( $pushcodeController->savePushcode($iduser, $input->pushcode, $input->hwid, $input->osType)){
            $this->set('response',  array('Success' => SuccessCodes::RS_REGISTRATIONOK, 'iduser' => $iduser, 'img' => $imageUrl)); 
        }
        else {
            $this->set( 'response', array('Success' => SuccessCodes::RS_REGISTRATION_PUSHCODE_ERROR));
        }
        
    }
    
    /**
     * Login utente tramite facebook, google o email
     * Gestione della variazione delle impostazioni poushwoosh
     */
    public function login(){
        
        $this->loadModel('User');
        $input = AppModel::getInputData();
        $user = false;
        
        // ricerca dell'utente nel database
        if (isset($input->idfacebook)){
            $user = $this->User->loginByFacebook();
        }
        elseif(isset($input->idgoogle)){
            $user = $this->User->loginByGoogle();
        }
        elseif (isset($input->email) && isset($input->password)) {
            $user = $this->User->loginByEmail();
        }
        
        // utente non trovato
        if ($user === false){
            $this->set('response', array('Success' => SuccessCodes::RS_LOGIN_USERNOTEXIST));
            return;
        }
        
        // utente bannato
        if ($user['activated'] != 1){
            $this->set('response', array('Success' => SuccessCodes::BANNED_USER));
            return;
        }
        
        // aggiornamento delle impostazioni pushwoosh -> il login va completamente a buon fine solo se il PushcodesController::savePushcode restituisce true
        App::import('Controller',   'Pushcodes');
        $pushcodeController = new PushcodesController();
        
        if( $pushcodeController->savePushcode($user['iduser'], $input->pushcode, $input->hwid, $input->osType)){
            $this->set( 'response', array('Success' => SuccessCodes::OK, 'User' => $user));
        }
        else{
            $this->set( 'response', array('Success' => SuccessCodes::RS_REGISTRATION_PUSHCODE_ERROR));
        }
    }
    
    /**
     * Aggiorna i dati associati al profilo di un utente
     */
    public function update(){
        $this->loadModel('User');
        // seleziopna i vecchi dati associati all'utente
        $olduser = $this->User->getUserById(false);
        
        // decodifica dell'immagine se questa è stata modificata
        App::import('Vendor', 'ImageManager');
        ImageManager::$folder = '../../../../'.Utils::USER_IMAGE_PATH;
        $tryImage = ImageManager::base64toJpg(AppModel::getInputData()->img);
        
        // modifica dei dati associati all'utente
        $this->User->updateUser($tryImage);
        
        // eliminazione della vecchia immagine associata all'utente, se questa non è quella di default
        if ($tryImage != false && $olduser['img'] != Utils::URL_BASE.Utils::USER_IMAGE_PATH.Utils::USER_DEFAULT_IMAGE){
            ImageManager::delete($olduser['img']);
        }
        
        // la risposta è sicuramenrte positiva
        $this->set('response', array('Success' => SuccessCodes::OK));
    }
    
    /**
     * Invia all'utente che ha inoltrato la richiesta una mail contenente un link per resettare la password
     */
    public function recovery(){
       
        $email = AppModel::getInputData()->email;
        
        $this->loadModel('User');
        $raw = $this->User->find('all', array('conditions' => array('email' => $email), 'fields' => array('iduser', 'name', 'surname')));
        
        if (count($raw) === 0){
            $this->set('response', array('Success' => SuccessCodes::RS_LOGIN_USERNOTEXIST));
            return;
        }
        
        App::import('Vendor', 'EmailManager');
        
        EmailManager::newPasswordLink($email, array('iduser' => $raw[0]['User']['iduser'],
                                                    'username' => $raw[0]['User']['name'].' '.$raw[0]['User']['surname'],
                                                    'limit' => time() + (15 * 60)));  // 15 minuti di tempo per resettare la password
        
        $this->set('response', array('Success' => SuccessCodes::OK));
    }
    
    /**
     * Action svolta quando l'utente clicca sul link nella mail che riceve al cambio della password
     * Genera una nuova password, la salva sul database ed invia all'utente una mail contenente la nuova password
     * Non genera alcuna risposta
     */
    public function newpassword(){
        $clear = Utils::generatePassword(8);
        $encoded = md5(md5($clear));
        
        $this->loadModel('User');
        $this->User->updateAll(array('password' => "'".$encoded."'"), array('iduser' => AppModel::getInputData()->iduser));
        $user = $this->User->getUserById(false);
        
        unset($this->User);
        
        App::import('Vendor', 'EmailManager');
        
        EmailManager::passwordChanged($user['email'], array('username' => $user['name'].' '.$user['surname'],
                                                          'password' => $clear));
        
        die();
    }
    
    /**
     * Un utente che riceve una richiesta di amicizia, la rifiuta.
     * Vengono eliminate dal db le tuple che definiscono lo stato dell'amicizia ambo i lati
     */
    public function refusefriend(){
        $this->loadModel('UserFriend');
        $this->UserFriend->unacceptFriend();
        $this->set('response', array('Success' => SuccessCodes::OK));
    }
    
    /**
     * Riceve il comando dal client spedisce la risposta e avvia l'elaborazione per:
     * - accettare l'amicizia
     * - rimuovere la notifica locale per la ricezione della richiesta di amicizia
     * - inviare la notifica push per l'accettazione della richiesta di amicizia
     */
    public function acceptfriend() {
        //accettare l'amicizia
        $this->loadModel('UserFriend');
        $this->UserFriend->acceptFriendship();
        unset($this->UserFriend);

        // rimuovere la notifica locale
        $this->loadModel('Notification');
        $this->Notification->delFriendReqest();
        unset($this->Notification);
        
        // invio risposta al client
        App::import('Vendor', 'TCPConnectionManager');
        TCPConnectionManager::flushAndClose(json_encode(array('Success' => SuccessCodes::OK)));
        
        $in = AppModel::getInputData();
        
        // inserimento della notifica locale
        App::import('Controller',   'Notifications');
        $notificationsController = new NotificationsController();
        $notificationsController->addNotification($in->iduser, $in->iduserfriend, NotificationsController::NOTIFICATION_TYPE_FRIENDSHIP_ACCEPTED, NULL);
        unset($notificationsController);
        
        // invio della notifica push
        App::import('Controller',   'Pushcodes');
        $pushcodeController     = new PushcodesController();
        $pushcodeController->sendPush(NotificationsController::NOTIFICATION_TYPE_FRIENDSHIP_ACCEPTED, $in->iduser, $in->iduserfriend, "", null, null, null);
        
        die();
    }
    
    /**
     * Seleziona la lista dei non amici in base ad una query string con token che possono essere contenute sia nel nome che nel cognome 
     * e le ordina in base ad un peso associato agli utenti rispetto alla ricerca in corso
     */
    public function unfriends(){
        
        $data = AppModel::getInputData();
        $iduser = HTMLDecoder::decode($data->iduser);
        $querystring = HTMLDecoder::decode($data->query_string);
        
        // selezione degli amici da escludere dalla lista
        $this->loadModel('UserFriend');
        $raw = $this->UserFriend->find('all', array('conditions' => array('iduser' => $iduser), 
                                                    'fields' => array('idfriend')));
        
        unset($this->UserFriend);
        
        $friends = array();
        
        foreach ($raw as &$r){
            $friends[] = $r['UserFriend']['idfriend'];
        }
        
        // selezione degli utenti ordinati per peso
        $this->loadModel('User');
        $users = $this->User->find('all', array('conditions' => array('NOT' => array('iduser' => $friends)),
                                                'fields' => array('iduser', 'name', 'surname', 'img', 'user_rank(name, surname, "'.$querystring.'") as rank'),
                                                'order' => array('rank desc', 'iduser desc'),
                                                'limit' => Utils::UNFRIEND_LIST_LEN));
        
        unset($this->User);
        
        // formattazione del risultato
        $result = array();
        
        foreach($users as &$u){
            if ($u[0]['rank'] > 0){
                $result[] = array('iduser' => $iduser,
                                  'idfriend' => $u['User']['iduser'],
                                  'accepted' => 10, // valore a caso > 2
                                  'name' => HTMLDecoder::decode($u['User']['name']),
                                  'surname' => HTMLDecoder::decode($u['User']['surname']),
                                  'img' => $u['User']['img']);
            } 
        }
        
        unset($users);
        
        App::import('Vendor', 'TCPConnectionManager');
        TCPConnectionManager::flushAndClose(json_encode(array('Success' => SuccessCodes::OK, 'users' => $result)));
        
        die();
    }
    
//    public function test(){
//        $this->loadModel('UserFollow');
//        $this->UserFollow->saveMany(array(array('iduser' => 2, 'idfollow' => 1), array('iduser' => 2, 'idfollow' => 3)));
//        $this->UserFollow->showLastQuery();
//        die();
//    }
}

?>
