<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
App::uses('CakeEmail', 'Network/Email');
/**
 * Description of EmailManager
 * Gestione dell'invio delle email
 * @author pietro@studioleaves.com
 */
class EmailManager {
   
    /**
     * Mittente delle email
     */
    const STAFF_EMAIL = 'info@wherabout.it';
    
    /**
     * Nome che deve essere visualizzato come mittente della mail
     */
    const STAFF_NAME = 'Wherabout';
    
    /**
     * Oggetto della mail con cui viene spedito il link per il recupero della password
     */
    const RECOVERY_LINK_SUBJECT = 'Password recovery request received';
    
    /**
     * Oggetto della mail con cui viene spedita all'utente la nuova password
     */
    const PASSWORD_CHANGED_SUBJECT = 'Your password has been changed';
    
    /**
     * Url per raggiungere il servizio che invia la richiesta di recupero della password
     * Alla stringa va concatenato : {iduser}-{limit} ovvero pk utente e timestamp limit per cambiare la password
     */
    const EMAIL_SERVICE_URL = 'http://app.join-me.it/v1/email/resetpsw.php?param=passwordupdate-'; 
    
    /**
     * Invia all'utente che ne ha fatto richiesta una mail contenente un link che permette di resettare la password
     * @param string $recipient Email destinatario
     * @param string $subject Oggetto della mail
     * @param stdClass $vars : {
     *                          'iduser': int,
     *                          'usermane': 'Nome e Cognome',
     *                          'limit': 'maxtimestamp'
     *                         } 
     */
    public static function newPasswordLink($recipient, $vars) {
        
        $Email = new CakeEmail();
        
        $Email->template("default", "resetPasswordLink")
                ->emailFormat('html')
                ->from(array(self::STAFF_EMAIL => self::STAFF_NAME))
                ->to($recipient)
                ->subject(self::RECOVERY_LINK_SUBJECT)
                ->viewVars($vars)
                ->send();
    }
    
    /**
     * Invia all'utente che ne ha fatto richiesta una mail contenente un link che permette di resettare la password
     * @param string $recipient Email destinatario
     * @param string $subject Oggetto della mail
     * @param stdClass $vars : {
     *                          'usermane': 'Nome e Cognome'
     *                          'password': 'Nuova Password in chiaro'
     *                         } 
     */
    public static function passwordChanged($recipient, $vars) {
        
        $Email = new CakeEmail();
        
        $Email->template("default", "passwordChanged")
                ->emailFormat('html')
                ->from(array(self::STAFF_EMAIL => self::STAFF_NAME))
                ->to($recipient)
                ->subject(self::PASSWORD_CHANGED_SUBJECT)
                ->viewVars($vars)
                ->send();
    }
}

?>
