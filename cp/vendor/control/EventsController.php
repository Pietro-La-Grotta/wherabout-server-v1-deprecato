<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of EventsController
 * Strumenti per la gestione della lista degli eventi
 * @author pietro@studioleaves.com
 */
class EventsController {
    
    /**
     * Vista del controller: 
     * list -> lista eventi
     * search -> Visualizza il form per la ricerca avanzata
     * edit -> dettaglio di un evento
     * partecipants -> lista degli utenti che hanno richiesto di partecipare all'evento 
     * @var string
     */
    private $show;
    
    /**
     * Eventuali operazioni da effettuare prima della visualizzazione : 
     * @var string
     */
    private $action;
    
    /**
     * Inizializza $view e $action utilizzando i dati in get
     * Effettua il caricamento degli script necessari per l'esecuzione delle richieste
     */
    public function EventsController(){
        $this->show = JRequest::getVar('show', 'list');
        $this->action = JRequest::getVar('action', 'noaction');
        
        JLoader::register('EventsModel', 'vendor/model/EventsModel.php');
        JLoader::register('EventsView', 'vendor/view/events/EventsView.php');
    }
    
    public function getShow() {
        return $this->show;
    }

    public function getAction() {
        return $this->action;
    }

    /**
     * Inizializza i campi del form
     * @param $form riferimento alla variabi9le form di chronoforms
     */
    public function initForm(&$form){
        
        $eventDetail = Travel::getInstance()->get('eventDetail');
        JLoader::register('CategoryModel', 'vendor/model/CategoryModel.php');
        
        $form->data['status'] = $eventDetail->activated;
        $form->data['title'] = HTMLDecoder::decode($eventDetail->title);
        
        $m = new CategoryModel();
        $dc = $m->getById($eventDetail->idcategory);
        
        $form->data['supercategoria'] = (isset($dc->idsupercategory)) ? $dc->idsupercategory : $eventDetail->idcategory; 
            
        $form->data['date'] = HTMLDecoder::italianDateTime($eventDetail->date, true);
        $form->data['hour'] = substr($eventDetail->hour, 0, 5); 
        $form->data['sex'] = $eventDetail->sex;
        $form->data['scope'] = $eventDetail->scope;
        $form->data['description'] = HTMLDecoder::decode($eventDetail->description); 
        $form->data['town'] = HTMLDecoder::decode($eventDetail->city);
        $form->data['address'] = HTMLDecoder::decode($eventDetail->address);
        $form->data['agemin'] = $eventDetail->agemin;
        $form->data['agemax'] = $eventDetail->agemax;
        $form->data['sitcurrentghost'] = 
        $form->data['sitcurrent'] = $eventDetail->sitcurrent;
        $form->data['sittotal'] = $eventDetail->sittotal; 
        $form->data['latitude'] = $eventDetail->latitude;
        $form->data['longitude'] = $eventDetail->longitude; 
    }
    
    /**
     * Caricamento degli script necessari per l'utilizzo delle mappe di google
     */
    public function loadMaps(){
        $doc = JFactory::getDocument();
        $doc->addScript("https://maps.googleapis.com/maps/api/js?key=AIzaSyBc_OauagOz9w00rZghDats5G3VScaRVrU");
        $doc->addScript('vendor/js/maps.js');
    }
    
    /**
     * Script per inizializzazione delle variabili da usare nella mappa
     * @param $form
     */
    public function initGps(){ 
        
        $eventDetail = Travel::getInstance()->get('eventDetail');
        
        echo '<script>
                var event_latitude = '.$eventDetail->latitude.';
                var event_longitude = '.$eventDetail->longitude.';
                var event_name = \''.$eventDetail->title.'\'
             </script>';
    }
    
     /**
     * Inizializza i campi del form per la ricerca tra gli eventi utilizzando i dati contenuti nella session
     * Nota le chiavi di accesso ai dati contenuti in session devono essere le stesse dei nomi dei campi del form
     * @param $form riferimento alla variabile form di chronoforms
     */
    public function initSearchForm(&$form){
        
        // se è stata fatta richiesta di pulizia dei filtri si cancellano i dati contenuti in sessione e non si inizializza nulla
        if ($this->action == 'reset'){
            JFactory::getSession()->clear('event_search_params');
            return;
        }
        
        // ritrova tra le variabili di sessione il json contenente i parametri di ricerca ed inizializza il form
        $sp = json_decode(JFactory::getSession()->get('event_search_params'), true);
        //var_dump($sp); die();
        if (!isset($sp)) return;
        
        foreach ($sp as $param => $value){
            $form->data[$param] = $value;
        }
    }
    
    /**
     * Imposta i nuovi crirteri di ricerca nella variabile di sessione 
     */
    public function saveSearchParam(){
        JFactory::getSession()->clear('event_search_params');
        
        $param = array();
        
        foreach ($_POST as $key => $val){
            
            // condizioni per il salto
            if (strlen($val) == 0) continue;
            if ($key == 'button16') continue;
            if (($key == 'categoria') and ($val == "-1")) continue;
            
            $param[$key] = $val;
        }
        
        JFactory::getSession()->set('event_search_params', json_encode($param));
        JFactory::getApplication()->redirect(JUri::base().'?show=list');
    }
    
    /**
     * Controlla se ci sono delle operazioni da effettuare sulla lista di eventi e se ci sono le esegue
     */
    public function performListActions(){
        // eventi da abilitare
        if (isset($_POST['events_to_active'])){
            $this->activeEvents();
            return;
        }
        
        // eventi da disabilitare
        if (isset($_POST['events_to_disable'])) 
            $this->disableEvents();
    }
    
    /**
     * Attivazione di eventi selezionati
     */
    private function activeEvents(){
        $events = str_replace(array('[', ']', '"'), array('(', ')', "'"), $_POST['events_to_active']);
        $model = new EventsModel();
        $model->activeEvents($events);
    }
    
    /**
     * Disabilitazione di eventi selezionati
     */
    private function disableEvents(){
        $events = str_replace(array('[', ']', '"'), array('(', ')', "'"), $_POST['events_to_disable']);
        $model = new EventsModel();
        $model->disableEvents($events);
    }
    
    /**
     * Controlla se ci sono delle operazioni da effettuare sulla lista delle prenotazioni
     */
    public function bokingListActions(){
        // eventi da abilitare
        if (isset($_POST['books_to_confirm'])){
            $this->confirmBooking();
            return;
        }
        
        // eventi da disabilitare
        if (isset($_POST['confirmation_to_remove'])) 
            $this->removeConfirmation();
    }
    
    /**
     * Attivazione di eventi selezionati
     */
    private function confirmBooking(){
        $model = new EventsModel();
        $model->confirmBooking(JRequest::getVar('item'), $_POST['books_to_confirm']);
    }
    
    /**
     * Disabilitazione di eventi selezionati
     */
    private function removeConfirmation(){
        $model = new EventsModel();
        $model->removeConfirmation(JRequest::getVar('item'), $_POST['confirmation_to_remove']);
    }
}

?>
