<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Event
 *
 * @author Christian
 */
App::uses('AppModel', 'Model');
class UserPartecipateEvent extends AppModel{
    
    public function getListPaged( $lastdate, $limitdate, $iduser ) {
        return $this->find( 'all', array( 'conditions' => array('date > ' => $lastdate, 'date <= ' => $limitdate, 'iduserpartecipates' => $iduser ), 'order' => array('date ASC') ) );
    }
    
    public function getListByMonth( $year, $month, $iduser ) {
        
        if( isset($year) && isset($month) ) {
            $yearMin = $year;
            $monthMin = $month;
            
            $yearMax = $year;
            $monthMax = $month + 1;
            if( $monthMax > 12) {
                $monthMax = 1;
                $yearMax++;
            }
            
            $dateMin = $yearMin."-".$monthMin."-"."00";
            $dateMax = $yearMax."-".$monthMax."-"."00";
            return $this->find( 'all', array( 'conditions' => array( 'date >' => $dateMin, 'date <' => $dateMax, 'iduserpartecipates' => $iduser ) , 'order' => array('date ASC') ) );
        }
        else {
            return null;
        }
    }
    
    public function getSingleEventInfo( $idevent ) {
        if( isset($idevent)) {
            return $this->find( 'all', array( 'conditions' => array( 'idevent' => $idevent ) ) );
        }
        else {
            return null;
        }
    }
    
    /**
     * Seleziona le pk dei partecipanti ad un evento
     * @param int $idevent pk evento
     * @param [boolean $noPlanner] default false : non escludere l'organizzatore dalla lista, true: escludere l'organizzatore dalla vista  
     * @return list of array
     */
    public function getPartecipants($idevent, $noPlanner = false) {
        
        $conditions = array();
        $conditions['idevent'] = $idevent;
        if ($noPlanner) $conditions[] = 'iduser != iduserpartecipates';
        
        return $this->find('all', array('conditions' => $conditions, 'fields' => array('iduserpartecipates')));
    }
}
