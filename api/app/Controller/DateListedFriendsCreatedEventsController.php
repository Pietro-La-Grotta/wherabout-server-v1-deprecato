<?php

/**
 * 
 *
 * @author Christian Ruggiero
 * @creation 17/11/2014
 * @status in develop
 */
App::import('Vendor', 'Events/IEventDateListedController');
class DateListedFriendsCreatedEventsController extends AppController implements IEventDateListedController{
    private $LIST_NAME = 'DateListedFriendsCreatedEvent';
    
    public function getList( $basedate, $iduser ){
        return $this->DateListedFriendsCreatedEvent->getList( $basedate, $iduser );
    }
    
    public function getListName(  ){
        return $this->LIST_NAME;
    }
}