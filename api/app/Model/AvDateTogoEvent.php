<?php

/*
 * SQL : 
 * create view av_date_togo_events as
   select p.iduser, count(e.idevent) occurrence, e.date
   from partecipates p join events e on p.idevent = e.idevent and p.confirmed = 1
   group by p.iduser, e.date;
 * 
 * DESCRIZIONE ogni tupla contiene pk user a cui è associato il numero di booking confermati dal creatore dell'evento raggruppati nello stesso giorno
 */




/**
 * Description of AvDateTogoEvent
 * accede ai dati della vista 
 * @author pietro@studioleaves.com
 */
class AvDateTogoEvent extends AppModel{
   /**
     * Seleziona il limite superiore dell'intervallo di date a cui devono appartenere gli eventi da selezionare nel caso di selezione degli eventi a cui un utente partecipa
     * @return false se il limite superiore corrisponde a quello inferiore; string (aaa-mm-gg) data che indica il limite superiore dell'intervallo da considerare 
     */
    public function getLastDate(){
        $dates = $this->find( 'all', array( 'conditions' => array( 'date >' => self::$inputData->lastdate, 
                                                                   'iduser' => self::$inputData->iduser), 
                                            'order' => array('date ASC')));
        $items = count($dates);
        if ($items == 0) return false;
        
        $events = $i = 0;
        $maxevents = self::$inputData->quantity; 
        
        do {
            $lastdate = $dates[$i]['AvDateTogoEvent']['date'];
            $events += $dates[$i]['AvDateTogoEvent']['occurrence'];
            $i++;
        } 
        while (($i < $items) and ($events < $maxevents));
        
        return $lastdate;
    }
}

?>
