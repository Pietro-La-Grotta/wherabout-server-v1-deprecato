/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Trasformaziuoni sul codice hatml generato da chronoform
 * @param {int} id_sel pk categoria evento
 */
function init_sub_categories(id_sel){
    //alert(id_sel);
    jQuery("option[value*='supc_']").each(function(index) {
        jQuery(this).css('display', 'none');   
    });
    
    jQuery("option[value*='_sc_']").each(function(index) {
        var codes = jQuery(this).attr('value').split('_sc_');
        jQuery(this).attr('value', codes[0]);
        jQuery(this).attr('sc', codes[1]);
        jQuery(this).attr('class', 'subc');
    });
    
    jQuery('#categoria').prepend('<option value="-1" class="notSelectedCat">Nessuna Sottocategoria</option>');
    var thereIsSelected = false;
    
    
    jQuery("option[class='subc']").each(function(index){
        
        if (jQuery(this).attr('value') == id_sel){
            thereIsSelected = true;
            jQuery(this).attr('selected', 'selected');
        }
    });
    
    if (!thereIsSelected){
        jQuery('.notSelectedCat').attr('selected', 'selected');
    }
    
    filter_subcategory();
    
    jQuery('#supercategoria').change(function(){
        
        filter_subcategory();
        jQuery('.notSelectedCat').attr('selected', 'selected');
    });
}

/**
 * All'evento change di supercategory asggiorna il dropdown menu delle sottocategorie
 */
function filter_subcategory(){
    var sp = jQuery('#supercategoria').find(":selected").val();
    
    jQuery("option[class='subc']").css('display', 'none');
    
    jQuery("option[class='subc']").each(function(index){
        if (jQuery(this).attr('sc') == sp){
            jQuery(this).css('display', 'inline');
        }
    });
} 


