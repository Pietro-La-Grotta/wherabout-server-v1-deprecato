<?php

App::uses('DispatcherFilter', 'Routing');

/**
 * Controlla se la richiesta è stata effettuata tramite metodo POST
 *
 * @package Cake.Routing.Filter
 */
class PostRequest extends DispatcherFilter {

/**
 * Default priority for all methods in this filter
 * This filter should run before the request gets parsed by router
 *
 * @var integer
 */
	public $priority = 1;

/**
 * Controlla se la richiesta è stata effettuata tramite metodo POST
 *
 * @param CakeEvent $event containing the request and response object
 * @return CakeResponse if the client is requesting a recognized asset, null otherwise
 */
	public function beforeDispatch(CakeEvent $event) {
            
            $request = new CakeRequest();

            if (!$request->is('post'))throw new BadRequestException("Not POST request"); 
        }
}
