<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Event
 *
 * @author Christian
 */
App::uses('AppModel', 'Model');
class FollowCreatedEvent extends AppModel{
    
    public function getListPaged( $lastdate, $limitdate, $iduser ) {
        return $this->find( 'all', array( 'conditions' => array('date > ' => $lastdate, 'date <= ' => $limitdate, 'idselecteduser' => $iduser ), 'order' => array('date ASC') ) );
    }
    
    public function getListByMonth( $year,  $month, $iduser) {
        
        if( isset($year) && isset($month) && isset($iduser) ) {
            $yearMin = $year;
            $monthMin = $month;
            
            $yearMax = $year;
            $monthMax = $month + 1;
            if( $monthMax > 12) {
                $monthMax = 1;
                $yearMax++;
            }
            
            $dateMin = $yearMin."-".$monthMin."-"."00";
            $dateMax = $yearMax."-".$monthMax."-"."00";
            return $this->find( 'all', array( 'conditions' => array( 'idselecteduser' => $iduser ,'date >' => $dateMin, 'date <' => $dateMax, "scope" => 1, "scope" => 0) ) );
        }
        else {
            return null;
        }
    }
    
    public function getSingleEventInfo( $idevent ) {
        
        if( isset($idevent)) {
            return $this->find( 'all', array( 'conditions' => array( 'idevent' => $idevent ) ) );
        }
        else {
            return null;
        }
    }
     
    //filtered
    
    public function getFilteredListPaged( $iduser, $lastdate, $searchLimit, $filters ) {
        $filter = "";
        
        if( !empty($filters->title) ) {
            $filter = $filter." AND title LIKE '%".HTMLDecoder::encode($filters->title)."%'";
        }
        
        if( !empty($filters->city) ) {
            $filter = $filter." AND city LIKE '%".HTMLDecoder::encode($filters->city)."%'";
        }
        
        if( isset($filters->idcategory) && $filters->idcategory >= 0 ) {
            $filter = $filter." AND idcategory = ".HTMLDecoder::encode($filters->idcategory);
        }

        if( isset($filters->sex) && $filters->sex >= 0 ) {
            $filter = $filter." AND sex = ".HTMLDecoder::encode($filters->sex);
        }

        if( isset($filters->job) && $filters->job >= 0) {
            $filter = $filter." AND job = ".HTMLDecoder::encode($filters->job);
        }
        
        if( isset($filters->agemin) && $filters->agemin >= 0 ) {
            $filter = $filter." AND agemin >= ".HTMLDecoder::encode($filters->agemin);
        }

        if( isset($filters->agemax) && $filters->agemax >= 0 ) {
            $filter = $filter." AND agemax <= ".HTMLDecoder::encode($filters->agemax);
        }

        if( isset($filters->status) && $filters->status == 0) {
            $filter = $filter." AND sitcurrent < (sittotal/2)";
        }

        if( isset($filters->status) && $filters->status == 1) {
            $filter = $filter." AND sitcurrent > (sittotal/2) AND sitcurrent < sittotal";
        }

        if( isset($filters->status) && $filters->status == 2) {
            $filter = $filter." AND sitcurrent = sittotal";
        }

        if( !empty($filters->dateto) ) {
            $filter = $filter." AND date <= ".HTMLDecoder::encode($filters->dateto);
        }
        
        $filter = $filter." AND idselecteduser = ". $iduser;

        $query = "SELECT * FROM follow_created_events as Event WHERE date > ".$lastdate." AND date <= (SELECT MAX(date) from (SELECT date FROM follow_created_events WHERE date > ".$lastdate." ".$filter." ORDER BY date ASC LIMIT ".$searchLimit.") as myevents) ". $filter;
        $query = $query." ORDER By date ASC;";
        return $this->query($query);
    }
    
    public function getFilteredListByMonth( $iduser, $year, $month, $filters ) {
        
        if( isset($year) && isset($month) ) {
            $yearMin = $year;
            $monthMin = $month;
            
            $yearMax = $year;
            $monthMax = $month + 1;
            if( $monthMax > 12) {
                $monthMax = 1;
                $yearMax++;
            }
            $filter = array();
            $dateMin = $yearMin."-".$monthMin."-"."00";
            $dateMax = $yearMax."-".$monthMax."-"."00";
            $filter ['idselecteduser'] = $iduser;
            if( !empty($filters->title) ) {
                $filter ['title LIKE'] = "%".$filters->title."%";
            }
            
            if( !empty($filters->city) ) {
                $filter ['city LIKE'] = "%".$filters->city."%";
            }
            
            if( isset($filters->idcategory)  && $filters->idcategory >= 0 ) {
                $filter ['idcategory'] = $filters->idcategory;
            }

            if( isset($filters->sex)  && $filters->sex >= 0 ) {
                $filter ['sex = '] = $filters->sex;
            }

            if( isset($filters->job) && $filters->job >= 0) {
                $filter ['job = '] = $filters->job;
            }
            
            if( isset($filters->agemin) && $filters->agemin >= 0 ) {
                $filter ['agemin >= '] = $filters->agemin;
            }

            if( isset($filters->agemax) && $filters->agemax >= 0 ) {
                $filter ['agemax <= '] = $filters->agemax;
            }

            if( isset($filters->status) && $filters->status == 0) {
                $filter ['sitcurrent - (sittotal/2) <' ] = '0';
            }

            if( isset($filters->status) && $filters->status == 1) {
                $filter ['sitcurrent - (sittotal/2) >' ] = '0';
                $filter ['(sitcurrent) - (sittotal) <' ] = '0';
            }

            if( isset($filters->status) && $filters->status == 2) {
                $filter ['(sitcurrent) - (sittotal) =' ] = '0';
            }
            
            $filter['date >='] = $dateMin;
            $filter['date <='] = $dateMax;
            return $this->find( 'all', array( 'conditions' => array( $filter ) ) );

        }
        else {
            return null;
        }
    }
    
    public function getFilteredDistanceList($iduser, $filters ) {
        
        $filter = array();
        $filter ['idselecteduser'] = $iduser;
        
        if( !empty($filters->title) ) {
                $filter ['title LIKE'] = "%".$filters->title."%";
        }
            
        if( !empty($filters->city) ) {
            $filter ['city LIKE'] = "%".$filters->city."%";
        }
        
        if( isset($filters->idcategory) && $filters->idcategory >= 0  ) {
            $filter ['idcategory'] = $filters->idcategory;
        }

        if( isset($filters->sex) && $filters->sex >= 0   ) {
            $filter ['sex = '] = $filters->sex;
        }

        if( isset($filters->job) && $filters->job >= 0) {
            $filter ['job = '] = $filters->job;
        }
        
        if( isset($filters->agemin) && $filters->agemin >= 0 ) {
            $filter ['agemin >= '] = $filters->agemin;
        }

        if( isset($filters->agemax) && $filters->agemax >= 0) {
            $filter ['agemax <= '] = $filters->agemax;
        }

        if( isset($filters->status) && $filters->status == 0) {
                $filter ['sitcurrent - (sittotal/2) <' ] = '0';
        }

        if( isset($filters->status) && $filters->status == 1) {
            $filter ['sitcurrent - (sittotal/2) >' ] = '0';
            $filter ['(sitcurrent) - (sittotal) <' ] = '0';
        }

        if( isset($filters->status) && $filters->status == 2) {
            $filter ['(sitcurrent) - (sittotal) =' ] = '0';
        }

        if( !empty($filters->datefrom) ) {
            $filter['date >='] = $filters->datefrom;
        }/*
        else {
            $filter['date >='] = date('Y-m-d');
        }*/
        
        if( !empty($filters->dateto) ) {
            $filter['date <='] = $filters->dateto;
        }
        return $this->find( 'all', array( 'conditions' => array( $filter ) ) );
    }
    
}
