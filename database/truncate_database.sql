drop function if exists truncate_database;
DELIMITER $$
create procedure `truncate_database`() 
begin
    TRUNCATE TABLE chats;
    TRUNCATE TABLE event_invitations;
    TRUNCATE TABLE notifications;
    TRUNCATE TABLE partecipates;
    TRUNCATE TABLE pushcodes;
    TRUNCATE TABLE curl_requests;
    TRUNCATE TABLE requestlogs;
    TRUNCATE TABLE user_follows;
    TRUNCATE TABLE user_friends;
    TRUNCATE TABLE users;
    TRUNCATE TABLE events;
end $$
DELIMITER ;