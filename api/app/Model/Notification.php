<?php

/**
 * Accesso ai dati relativi alle notifiche locali
 */
class Notification extends AppModel{
    
    public function getAllNotifications( $idreceiver) {
        return $this->find( 'all', array( 'conditions' => array('idreceiver' => $idreceiver), 'order' => array('Notification.date DESC') ) );
    }
    
    public function getNotificationNumber( $idreceiver ) {
        return $this->find( 'count', array( 'conditions' => array('idreceiver' => $idreceiver) ) );
    }  
    
    public function getOldestNotification( $idreceiver ) {
        return $this->find( 'first', array( 'conditions' => array('idreceiver' => $idreceiver), 'order' => array('Notification.date ASC') ) );
    }   
    
    /**
     * Elimina la notifica locale di ricezione della richiesta di amicizia che viene accettata
     */
    public function delFriendReqest(){
       $this->deleteAll(array('idreceiver' => HTMLDecoder::encode(self::$inputData->iduser),
                              'idsender' => HTMLDecoder::encode(self::$inputData->iduserfriend),
                              'type' => 3), false); 
    }
        
    
    
    
    /**
     * Invoca la stored procedure per aggiungere le notifiche visualizzate dai fallower del creatore di un evento
     * quando un evento pubblico viene creato
     * @param int $idEvent pk evento creato
     * @param int $notificatioType codice tipologia di notifica estratto da NotificationsController
     * @param int $bufferLenght lunghezza del buffer di noifiche per ogni utente
     */
    public function createEventNotification($idEvent, $notificatioType, $bufferLenght){
        $this->query("CALL create_event_notifications(".$idEvent.", ".$notificatioType.", ".$bufferLenght.")");
    }
    
    /**
     * Invoca la stored procedure per aggiungere le notifiche visualizzate dai partecipanti ad un evento
     * quando un evento pubblico viene modificato
     * @param int $idEvent pk evento creato
     * @param int $notificatioType codice tipologia di notifica estratto da NotificationsController
     * @param int $bufferLenght lunghezza del buffer di noifiche per ogni utente
     */
    public function updateEventNotification($idEvent, $notificatioType, $bufferLenght){
        $this->query("CALL update_event_notifications(".$idEvent.", ".$notificatioType.", ".$bufferLenght.")");
    }
}
