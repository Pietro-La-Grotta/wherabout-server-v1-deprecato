<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * SQL
 * drop view if exists av_togo_events; 
   create view av_togo_events as 
   select p.iduser participant, p.idevent, p.confirmed partecipate, 
          e.iduser creator, e.sex, e.sittotal, e.sitcurrent, e.idcategory, e.title, e.city, e.address, e.agemin, e.agemax, e.date, e.hour, e.scope, e.latitude, e.longitude, e.img imgcover,
          u.img userimg
   from partecipates p join events e on p.idevent = e.idevent and p.confirmed = 1
                       join users u on e.iduser = u.iduser
   where e.activated = 1
   order by e.date ASC;
 * 
 * La vista associa alle prenotazioni confermate informazioni relative all'evento stesso e l'immagine dell'utente che ha creato l'evento
 */

/**
 * Description of AvTogoEvent
  * Accesso ai dati della vista av_togo_events
 * @author pietro@studioleaves.com
 */
class AvTogoEvent extends AppModel{
    
    /**
     * Seleziona una pagina di eventi a cui l'utente si è prenotato e che sono stati confemati dal creatore dell'evento e li organizza nella struttura dati di output richiesta
     * @param date $sup limite superiore dell'intervallo di date di appartenenza degli eventi
     * @return mixed
     */
    public function select($sup){
        $raw = $this->find('all', array('conditions' => array('date > ' => self::$inputData->lastdate, 
                                                              'date <= ' => $sup, 
                                                              'participant' => self::$inputData->iduser),
                                        //'fields' => array('idevent', 'iduser', 'sex', 'sittotal', 'sitcurrent', 'idcategory', 'title', 'city', 'address', 'agemin', 'agemax', 'date', 'hour', 'scope', 'userimg', 'partecipate', 'latitude', 'longitude'),
                                        //'order' => array('date ASC')
            ));
        
        $focus = '0000-00-00';
        $events = array();
        
        foreach ($raw as $r){
            // decodifica campi codificati
            $r['AvTogoEvent']['title'] = HTMLDecoder::decode($r['AvTogoEvent']['title']);
            $r['AvTogoEvent']['city'] = HTMLDecoder::decode($r['AvTogoEvent']['city']);
            $r['AvTogoEvent']['address'] = HTMLDecoder::decode($r['AvTogoEvent']['address']);
            $r['AvTogoEvent']['iduser'] = $r['AvTogoEvent']['creator'];
            unset($r['AvTogoEvent']['creator']);
            
            if ($r['AvTogoEvent']['date'] != $focus){
                // nuova partizione giorno
                $focus = $r['AvTogoEvent']['date'];
                $events[] = array('date' => $focus, 'eventlistinfo' => array());
            }
            
            // inserimento evento nel set partizionato per giorni
            $events[count($events) - 1]['eventlistinfo'][] = $r['AvTogoEvent'];
        }
        
        return $events;
    }
}

?>
