<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Event
 *
 * @author Christian
 */ 
App::uses('AppModel', 'Model');
class Category extends AppModel{

    private static $STANDARD_LANAGUAGE  = "en_GB";
    public function getCategoryList() {
        $stringList = $this->find( 'all', array( 'conditions' => array('language'=> self::$langauge), 'order' => array('id ASC') ) ) ;
        if ( empty( $stringList ) ) {
            $stringList = $this->find( 'all', array( 'conditions' => array('language'=> Category::$STANDARD_LANAGUAGE), 'order' => array('id ASC') ) ) ;
        }
        return $stringList;
    }
}
