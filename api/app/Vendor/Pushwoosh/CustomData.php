<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CustomData
 * Oggetto custom da inviare al client
 * @author pietro@studioleaves.com
 */
class CustomData {
    
    /**
     * Tipologia di notifica:
     * 1 -> notifica aperiodica
     * 2 -> notifica periodica
     * @var int 
     */
    public $type;
    
    /**
     * Chiave primaria utilizzata dall'app al tap sulla notifica
     * Se la notifica è aperiodica è l'id del contenuto da caricare altrimenti
     * se la notifica è periodica indica l'id della tipologia di raccolta da visualizzare
     * @var int 
     */
    public $id;
}

?>
