<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CreatedCell
 * Numero di eventi creati da un utente con link a tale lista
 * @author pietro@studioleaves.com
 */
class CreatedCell implements ICell{

    private static $href_title = "Lista eventi creati dall&#39;utente";
    private static $href_params = "index.php/utenti?show=created&item=";
    
    private $tuple;
    private $created_events_label_fields;
    private $iduser_label_fields;
   
    /**
     * Inizializza le variabili di instanza
     * @param string $created_events_label_fields nome del campo della tupla contenente il numero di eventi creati
     * @param string $iduser_label_fields nome del campo della tupla contenente pk utente
     */
    function __construct($created_events_label_fields, $iduser_label_fields) {
        $this->created_events_label_fields = $created_events_label_fields;
        $this->iduser_label_fields = $iduser_label_fields;
    }

        
    /**
     * Stampa il contenuto html
     * @return string
     */
    public function html() {
        $html = "";
            
            $label = $this->tuple[$this->created_events_label_fields];
            
            $html .= '<td align="center">';
                $html .= '<a href="'.Juri::base().self::$href_params.$this->tuple[$this->iduser_label_fields].'" title="'.self::$href_title.'" target="_blank">';
                $html .= $label;
                $html .= '</a>';
            $html .= '</td>';
            
        return $html;
    }

    /**
     * Setter per la tupla
     * @param array $tuple
     */
    public function data(&$tuple) {
        $this->tuple = $tuple;
    }    
}

?>
