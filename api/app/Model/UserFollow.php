<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Event
 *
 * @author Christian
 */
//App::uses('AppModel', 'Model');
class UserFollow extends AppModel{
    public function getSingleUserInfo( $iduser ) {
        if( isset($iduser)) {
            return $this->find( 'all', array( 'conditions' => array( 'iduser' => $iduser ) ) );
        }
        else {
            return null;
        }
    }
    
    public function deleteFollow( $iduser, $iduserfollow ) { 
        $this->query("DELETE FROM user_follows WHERE iduser = ".$iduser." AND idfollow = ".$iduserfollow);
        return true;
    }
    
    public function getUserFollowers( $iduserfollow ) {
        return $this->find( 'all', array( 'conditions' => array('idfollow' => $iduserfollow) ) );
    }
}
