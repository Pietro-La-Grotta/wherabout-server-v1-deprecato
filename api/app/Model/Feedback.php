<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Feeddback
 *
 * @author pietro@studioleaves.com
 */
class Feedback extends AppModel{
    
    /**
     * Aggiunge un feedback inviato da un utente
     */
    public function addFeedback(){
        
        $this->set(array('idplanner' => HTMLDecoder::encode(self::$inputData->idplanner),
                         'idsender' => HTMLDecoder::encode(self::$inputData->iduser),
                         'idevent' => HTMLDecoder::encode(self::$inputData->idevent),
                         'vote' => HTMLDecoder::encode(self::$inputData->vote),
                         'comment' => HTMLDecoder::encode(self::$inputData->comment)));
        
        $this->save();
    }
    
    /**
     * Inserisce una tupla per tenere traccia del fatto che l'utente non vuol lasciare feedback circa quell'evento
     */
    public function refuseFeedback(){
        
        $this->set(array('idplanner' => HTMLDecoder::encode(self::$inputData->idplanner),
                         'idsender' => HTMLDecoder::encode(self::$inputData->iduser),
                         'idevent' => HTMLDecoder::encode(self::$inputData->idevent),
                         'refuse' => 1));
        
        $this->save();
    }
    
    /**
     * Seleziona una pagina della lista dei feedback associati ad un utente
     */
    public function getUserFeedbacks(){
        
        $this->useTable = 'av_user_feedbacks';
        
        $planner = HTMLDecoder::encode(self::$inputData->iduser);
        $limit = HTMLDecoder::encode(self::$inputData->limit);
        $page = HTMLDecoder::encode(self::$inputData->page);
        
        $raw = $this->find('all', array('conditions' => array('idplanner' => $planner),
                                 'limit' => $limit,
                                 'offset' =>  $limit * $page,
                                 'order' => array('creation_time desc')));
        
        $feedbacks = array();
        
        foreach($raw as &$r){
            
            $feedbacks[] = array('senderId' => $r['Feedback']['senderid'],
                                 'senderName' => HTMLDecoder::decode($r['Feedback']['sendername']),
                                 'senderImage' => $r['Feedback']['img'],
                                 'sendingTime' => HTMLDecoder::italianDateTime($r['Feedback']['creation_time'], true),
                                 'eventId' => $r['Feedback']['idevent'],
                                 'eventName' => HTMLDecoder::decode($r['Feedback']['event_title']),
                                 'vote' => $r['Feedback']['vote'],
                                 'comment' => HTMLDecoder::decode($r['Feedback']['comment']));
        }
        
        return $feedbacks;
    }
    
    /**
     * Seleziona l'ultima data della pagina di eventi da selezionare 
     * considerando che si dovranno estrarre un numero di eventi quanto meno supèeriore possibile al minimo numero di eventi da selezionare
     * @param array $conditions array associativo delle condizioni da inserire nella chiamata al metodo find
     * @param int $maxEvents numero minimo di eventi da selezionare
     * @return date limite inferiore della selezione aaaa-mm--gg [false se non ci sono eventi da mostrare]
     */
    public function limite(&$conditions, $maxEvents){
        
        $this->useTable = 'av_events_to_feedbacks';
        
        // selezione del numero di eventi raggruppati per data
        $raw = $this->find('all', array('conditions' => $conditions,
                                        'fields' => array('count(idevent) as occourrences', 'date'),
                                        'order' => array('date desc', 'idevent desc'),
                                        'group' => array('date')));
        
        
        if (($n = count($raw)) == 0) return false;
        
        // calcolo dell'ultima data da mostrare nella pagina richiesta
        $events = $i = 0;
        $mindate = '';
        
        do {
            
            $events += intval($raw[$i][0]['occourrences']);
            $mindate = $raw[$i]['Feedback']['date'];
            
        } while (($events <= $maxEvents) and (++$i < $n));
        
        return $mindate;
    }
    
    /**
     * Seleziona la lista degli eventi a cui è possibile associare un feedback
     * @param $conditions array delle condizioni da passare al metodo find
     * @return array of EventsInDayList
     */
    public function eventsToFeedback(&$conditions){
        
        $this->useTable = 'av_events_to_feedbacks';
        
        $rawevents = $this->find('all', array('conditions' => $conditions,
                                        'order' => array('date desc', 'idevent desc')));
        
        // lista degli eventi
        $eventsdaylist = array();
        $curday = "";
        
        // filling degli eventi
        foreach($rawevents as &$ev){
            $e = $ev['Feedback'];
            
            // se necessario istanziamo un nuovo giorno
            if ($e['date'] != $curday){

                $curday = $e['date']; 
                $eventsdaylist[] = array('date' => $curday, 'eventlistinfo' => array());
            }

            // inserimento dell'evento nella lista
            $eventsdaylist[count($eventsdaylist) - 1]['eventlistinfo'][] = $this->eventDecode($e); 
        }
        
        return $eventsdaylist; 
    }
    
    /**
     * Dato una tupla estratta dal db nel formato della vista av_events_to_feedbacks, formatta l'array associativo secondo il protocollo per l'invio al client
     * @param array $e Tupla estratta da db
     * @return Array associativo per un evento da inviare al client 
     */
    private function eventDecode($e){
        
        // decodifica delle stringhe
        $e['title'] = HTMLDecoder::decode($e["title"]);
        $e['city'] = HTMLDecoder::decode($e["city"]);
        $e['address'] = HTMLDecoder::decode($e["address"]);

        if (isset($e['img'])){
            $e['imgcover'] = $e['img'];
            unset($e['img']);
        }

        if (isset($e['plannerimg'])){
            $e['userimg'] = $e['plannerimg'];
            unset($e['plannerimg']);
        }

        // partecipazione dell'utente che ha inoltrato la richiesta all'evento
        $e['partecipate'] = 1;
        
        return $e;
    }
    
    /**
     * Controlla se ci sono degli eventi trascorsi a cui inviare un feedback
     * Se si selezione le info necessarie per inviare il feedback all'evento, altrimenti return false
     * @return array associativo {'idevent', 'title'} da decodificare OR false
     */
    public function mostRecentToFeedback(){
        
        $iduser = HTMLDecoder::encode(self::$inputData->iduser);
        $conditions['iduser !='] = $iduser; // gli eventi non devono essere stati creati dall'utente che invia la richiesta
        $conditions['idpartecipant'] = $iduser; //l'utente che ha inviato la richiesta deve aver fatto la prenotazione all'evento 
        $conditions['refuse'] = NULL; // l'utente che invia la richiesta non deve aver già lasciato un feedback per quell'evento e non deve aver neanche cliccato su "non voglio lasciare un feedback"
        $conditions['date <'] = date('Y-m-d');  // inizio della pagina richiesta : a partire da ieri
        
        $this->useTable = 'av_events_to_feedbacks';
        
        $events = $this->find('all', array('conditions' => $conditions,
                                           'order' => array('date desc', 'idevent desc'),
                                           'fields' => array('idevent', 'title'),
                                           'limit' => 1));
        
        if (count($events) == 0) return false; // non ci sono eventi per cui consigliare di lasciare il feedback
        
        return $events[0]['Feedback'];
    }
}

?>
