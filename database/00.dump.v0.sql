-- dump effettuato in data 24/08/2015

--
-- Struttura stand-in per le viste `activated_events`
--
CREATE TABLE IF NOT EXISTS `activated_events` (
`idevent` int(11)
,`iduser` int(11)
,`idcategory` int(11)
,`title` varchar(100)
,`description` varchar(1000)
,`img` varchar(128)
,`city` varchar(45)
,`address` varchar(45)
,`date` date
,`hour` time
,`sex` int(1)
,`agemin` int(2)
,`agemax` int(2)
,`sittotal` int(3)
,`sitcurrent` int(3)
,`latitude` varchar(10)
,`longitude` varchar(10)
,`scope` int(11)
,`creationdate` datetime
,`updatetime` timestamp
,`activated` int(1)
,`userimg` varchar(128)
,`job` int(3)
);
-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `activated_users`
--
CREATE TABLE IF NOT EXISTS `activated_users` (
`iduser` int(11)
,`email` varchar(256)
,`idgoogle` varchar(256)
,`idfacebook` varchar(256)
,`name` varchar(45)
,`surname` varchar(45)
,`img` varchar(128)
,`birthdate` date
,`sex` varchar(45)
,`job` int(3)
,`interests` varchar(255)
,`creationdate` datetime
,`updatetime` timestamp
,`password` varchar(45)
,`activated` int(1)
);
-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `av_count_created_events`
--
CREATE TABLE IF NOT EXISTS `av_count_created_events` (
`date` date
,`iduser` int(11)
,`occurance` bigint(21)
);
-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `av_date_togo_events`
--
CREATE TABLE IF NOT EXISTS `av_date_togo_events` (
`iduser` int(11)
,`occurrence` bigint(21)
,`date` date
);
-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `av_my_events`
--
CREATE TABLE IF NOT EXISTS `av_my_events` (
`idevent` int(11)
,`iduser` int(11)
,`idcategory` int(11)
,`title` varchar(100)
,`description` varchar(1000)
,`img` varchar(128)
,`city` varchar(45)
,`address` varchar(45)
,`date` date
,`hour` time
,`sex` int(1)
,`agemin` int(2)
,`agemax` int(2)
,`sittotal` int(3)
,`sitcurrent` int(3)
,`latitude` varchar(10)
,`longitude` varchar(10)
,`scope` int(11)
,`creationdate` datetime
,`updatetime` timestamp
,`activated` int(1)
,`partecipate` bigint(11)
,`userimg` varchar(128)
);
-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `av_my_friends`
--
CREATE TABLE IF NOT EXISTS `av_my_friends` (
`iduser` int(11)
,`idfriend` int(11)
,`accepted` int(5)
,`name` varchar(45)
,`surname` varchar(45)
,`img` varchar(128)
);
-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `av_togo_events`
--
CREATE TABLE IF NOT EXISTS `av_togo_events` (
`participant` int(11)
,`idevent` int(11)
,`partecipate` int(1)
,`creator` int(11)
,`sex` int(1)
,`sittotal` int(3)
,`sitcurrent` int(3)
,`idcategory` int(11)
,`title` varchar(100)
,`city` varchar(45)
,`address` varchar(45)
,`agemin` int(2)
,`agemax` int(2)
,`date` date
,`hour` time
,`scope` int(11)
,`latitude` varchar(10)
,`longitude` varchar(10)
,`imgcover` varchar(128)
,`userimg` varchar(128)
);
-- --------------------------------------------------------

--
-- Struttura della tabella `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idsupercategory` int(11) DEFAULT NULL,
  `name` varchar(52) NOT NULL DEFAULT 'Undefined',
  `language` varchar(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dump dei dati per la tabella `categories`
--

INSERT INTO `categories` (`id`, `idsupercategory`, `name`, `language`) VALUES
(1, NULL, 'FESTE & RICEVIMENTI ', 'en_GB'),
(2, NULL, 'BUSINESS & CAREERS', 'en_GB'),
(3, NULL, 'MUSIC & DANCE', 'en_GB'),
(4, 0, 'Aperitivo', 'en_GB'),
(5, 0, 'Brunch', 'en_GB'),
(6, 0, 'Cena', 'en_GB'),
(7, 0, 'Colazione', 'en_GB'),
(8, 0, 'Pranzo', 'en_GB'),
(0, NULL, 'FOOD & DRINK', 'en_GB'),
(10, 0, 'Altro', 'en_GB'),
(11, 1, 'Cocktail Party ', 'en_GB'),
(12, 1, 'Dance Party  ', 'en_GB'),
(13, 1, 'Tea Party', 'en_GB'),
(14, 1, 'Vernissage', 'en_GB'),
(15, 1, 'Altro', 'en_GB'),
(16, 2, 'Business Meeting ', 'en_GB'),
(17, 2, 'Coworking', 'en_GB'),
(18, 2, 'Conference', 'en_GB'),
(19, 2, 'Gruppo di Studio', 'en_GB'),
(20, 2, 'Altro', 'en_GB'),
(21, 3, 'Concerto', 'en_GB'),
(22, 3, 'DJ Set', 'en_GB'),
(23, 3, 'Jam Session', 'en_GB'),
(24, 3, 'Altro', 'en_GB'),
(25, NULL, 'ARTE & CULTURA', 'en_GB'),
(26, NULL, 'SPORT & COMPETIZIONI', 'en_GB'),
(27, NULL, 'GITE & ESCURSIONI ', 'en_GB'),
(28, NULL, 'RADUNI & MOVIMENTI', 'en_GB'),
(29, NULL, 'SVAGO & TEMPO LIBERO', 'en_GB'),
(30, 25, 'Cinema', 'en_GB'),
(31, 25, 'Fiere & Mercatini', 'en_GB'),
(32, 25, 'Incontri & Dibattiti', 'en_GB'),
(33, 25, 'Mostre & Esposizioni', 'en_GB'),
(34, 25, 'Teatro', 'en_GB'),
(35, 25, 'Altro', 'en_GB'),
(36, 26, 'Atletica', 'en_GB'),
(37, 26, 'Basket', 'en_GB'),
(38, 26, 'Bowling', 'en_GB'),
(39, 26, 'Caccia', 'en_GB'),
(40, 26, 'Calcio', 'en_GB'),
(41, 26, 'Ciclismo', 'en_GB'),
(42, 26, 'Danza', 'en_GB'),
(43, 26, 'Equitazione', 'en_GB'),
(44, 26, 'Golf', 'en_GB'),
(45, 26, 'Jogging', 'en_GB'),
(46, 26, 'Karting', 'en_GB'),
(47, 26, 'Nuoto', 'en_GB'),
(48, 26, 'Pallavolo', 'en_GB'),
(49, 26, 'Pesca', 'en_GB'),
(50, 26, 'Soft Air', 'en_GB'),
(51, 26, 'Squash', 'en_GB'),
(52, 26, 'Tennis', 'en_GB'),
(53, 26, 'Vela & Canoa', 'en_GB'),
(54, 26, 'Altro', 'en_GB'),
(55, 27, 'Bicicletta & Mountain Bike', 'en_GB'),
(56, 27, 'Moto Touring', 'en_GB'),
(57, 27, 'Trekking & Alpinismo', 'en_GB'),
(58, 27, 'Uscite Fotografiche', 'en_GB'),
(59, 27, 'Visite Turistiche', 'en_GB'),
(60, 27, 'Altro', 'en_GB'),
(61, 28, 'Autoraduni', 'en_GB'),
(63, 28, 'Flash Mob', 'en_GB'),
(64, 28, 'Motoraduni', 'en_GB'),
(62, 28, 'Cortei', 'en_GB'),
(65, 28, 'Altro', 'en_GB'),
(66, 29, 'Animali Domestici', 'en_GB'),
(67, 29, 'Beneficenza & Volontariato', 'en_GB'),
(68, 29, 'Fumetti & Video Games', 'en_GB'),
(69, 29, 'Giochi da Tavolo', 'en_GB'),
(70, 29, 'Giochi di Ruolo ', 'en_GB'),
(72, 29, 'Modellismo', 'en_GB'),
(73, 29, 'Altro', 'en_GB');

-- --------------------------------------------------------

--
-- Struttura della tabella `chats`
--

CREATE TABLE IF NOT EXISTS `chats` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idsender` int(11) NOT NULL,
  `sendername` varchar(100) NOT NULL COMMENT 'username del mittente',
  `idreceiver` int(11) NOT NULL,
  `hwid` varchar(64) NOT NULL COMMENT 'hardware id destinatario',
  `message` text NOT NULL COMMENT 'testo del messaggio',
  `creation_time` varchar(45) NOT NULL COMMENT 'timestamp alla creazione del messaggio su device mittente',
  PRIMARY KEY (`id`),
  KEY `users_fk_12` (`idsender`),
  KEY `users_fk_13` (`idreceiver`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Immagazzina tutte le chat relative a tutti gli user' AUTO_INCREMENT=401 ;

--
-- Dump dei dati per la tabella `chats`
--

INSERT INTO `chats` (`id`, `idsender`, `sendername`, `idreceiver`, `hwid`, `message`, `creation_time`) VALUES
(1, 71, 'Michele Addante', 135, '56f8bf5cbc721ec2', 'quetso è il messaggio del secolo', '635646195791993169'),
(2, 71, 'Michele Addante', 135, 'provaprova', 'quetso è il messaggio del secolo', '635646195791993169'),
(4, 71, 'Michele Addante', 135, '56f8bf5cbc721ec2', 'quetso è il messaggio del secolo', '635646195791993168'),
(5, 71, 'Michele Addante', 135, 'provaprova', 'quetso è il messaggio del secolo', '635646195791993168'),
(7, 71, 'Michele Addante', 135, '56f8bf5cbc721ec2', 'quetso è il messaggio del secolo', '635646195791993167'),
(8, 71, 'Michele Addante', 135, 'provaprova', 'quetso è il messaggio del secolo', '635646195791993167'),
(10, 71, 'Michele Addante', 135, '56f8bf5cbc721ec2', 'quetso è il messaggio del secolo', '635646195791993166'),
(11, 71, 'Michele Addante', 135, 'provaprova', 'quetso è il messaggio del secolo', '635646195791993166'),
(13, 71, 'Michele Addante', 135, '56f8bf5cbc721ec2', 'quetso è il messaggio del secolo', '635646195791993165'),
(14, 71, 'Michele Addante', 135, 'provaprova', 'quetso è il messaggio del secolo', '635646195791993165'),
(16, 71, 'Michele Addante', 135, '56f8bf5cbc721ec2', 'quetso è il messaggio del secolo', '635646195791993164'),
(17, 71, 'Michele Addante', 135, 'provaprova', 'quetso è il messaggio del secolo', '635646195791993164'),
(19, 71, 'Michele Addante', 135, '56f8bf5cbc721ec2', 'quetso è il messaggio del secolo', '635646195791993163'),
(20, 71, 'Michele Addante', 135, 'provaprova', 'quetso è il messaggio del secolo', '635646195791993163'),
(57, 142, 'Pietro La Grotta', 189, 'bad8a98a-f566-4d9a-807f-fdce91ed29fd', 'H4sIAAAAAAAAA0tUKElVyMvPU8jMK0ktSS0uTgQAOmeCCBIAAAA=', '635705868968028280'),
(58, 142, 'Pietro La Grotta', 189, 'bad8a98a-f566-4d9a-807f-fdce91ed29fd', 'H4sIAAAAAAAAA8tNVEjOz01VyE3MBACEjIQiCwAAAA==', '635705869375025440'),
(59, 142, 'Pietro La Grotta', 189, 'bad8a98a-f566-4d9a-807f-fdce91ed29fd', 'H4sIAAAAAAAAA8vLz1PIzVQoyExMTlVIzs8rSy0qTiwCMxVKUgGURVlLHgAAAA==', '635705870950601590'),
(60, 142, 'Pietro La Grotta', 189, 'bad8a98a-f566-4d9a-807f-fdce91ed29fd', 'H4sIAAAAAAAAA8tNLS5OTE/PzFdIBACSXk6VCwAAAA==', '635705872386751600'),
(61, 142, 'Pietro La Grotta', 189, 'bad8a98a-f566-4d9a-807f-fdce91ed29fd', 'H4sIAAAAAAAAA8tNLS5OTE/PzFdIAgAoD0cMCwAAAA==', '635705872440464990'),
(62, 142, 'Pietro La Grotta', 189, 'bad8a98a-f566-4d9a-807f-fdce91ed29fd', 'H4sIAAAAAAAAA8tNLS5OTE/PVMhXSAYAi5/+SQwAAAA=', '635705872512578730'),
(63, 168, 'Mxg Etto', 182, '1ad76778-930d-44eb-9217-9feda60d5dea', 'H4sIAAAAAAAAAwvKTE4tKy3JV3g0YxcAeejuawwAAAA=', '635709304664761300'),
(65, 168, 'Mxg Etto', 182, 'af63e1d5-c8d0-4369-9ac2-015079e7b8c6', 'H4sIAAAAAAAAAwvKTE4tKy3JV3g0YxcAeejuawwAAAA=', '635709304664761300'),
(66, 168, 'Mxg Etto', 182, 'fc851fa5-3f89-4244-a90f-faeee29c98b3', 'H4sIAAAAAAAAAwvKTE4tKy3JV3g0YxcAeejuawwAAAA=', '635709304664761300'),
(67, 168, 'Mxg Etto', 182, '159823b9-411d-4e47-b369-835a8d5f743c', 'H4sIAAAAAAAAAwvKTE4tKy3JV3g0YxcAeejuawwAAAA=', '635709304664761300'),
(68, 187, 'Jessica Enin', 182, '1ad76778-930d-44eb-9217-9feda60d5dea', 'H4sIAAAAAAAAA3POTMxXAAC6m0EPBQAAAA==', '635709328292958090'),
(70, 187, 'Jessica Enin', 182, 'af63e1d5-c8d0-4369-9ac2-015079e7b8c6', 'H4sIAAAAAAAAA3POTMxXAAC6m0EPBQAAAA==', '635709328292958090'),
(71, 187, 'Jessica Enin', 182, 'fc851fa5-3f89-4244-a90f-faeee29c98b3', 'H4sIAAAAAAAAA3POTMxXAAC6m0EPBQAAAA==', '635709328292958090'),
(72, 187, 'Jessica Enin', 182, '159823b9-411d-4e47-b369-835a8d5f743c', 'H4sIAAAAAAAAA3POTMxXAAC6m0EPBQAAAA==', '635709328292958090'),
(74, 12, 'CLAUDIO CERIALI', 182, '1ad76778-930d-44eb-9217-9feda60d5dea', 'H4sIAAAAAAAAAw3KsQnAMAwEwFW+yxiZ5eMI/CAsI2QXmT6u70roAY0tVsCJySxrmvwUw0B32LZx8JGL+Z51dQqpZnsV7x8on5NjQwAAAA==', '635709428535534980'),
(75, 12, 'CLAUDIO CERIALI', 182, 'bad8a98a-f566-4d9a-807f-fdce91ed29fd', 'H4sIAAAAAAAAAw3KsQnAMAwEwFW+yxiZ5eMI/CAsI2QXmT6u70roAY0tVsCJySxrmvwUw0B32LZx8JGL+Z51dQqpZnsV7x8on5NjQwAAAA==', '635709428535534980'),
(76, 12, 'CLAUDIO CERIALI', 182, 'af63e1d5-c8d0-4369-9ac2-015079e7b8c6', 'H4sIAAAAAAAAAw3KsQnAMAwEwFW+yxiZ5eMI/CAsI2QXmT6u70roAY0tVsCJySxrmvwUw0B32LZx8JGL+Z51dQqpZnsV7x8on5NjQwAAAA==', '635709428535534980'),
(77, 12, 'CLAUDIO CERIALI', 182, '159823b9-411d-4e47-b369-835a8d5f743c', 'H4sIAAAAAAAAAw3KsQnAMAwEwFW+yxiZ5eMI/CAsI2QXmT6u70roAY0tVsCJySxrmvwUw0B32LZx8JGL+Z51dQqpZnsV7x8on5NjQwAAAA==', '635709428535534980'),
(78, 12, 'CLAUDIO CERIALI', 182, 'fc851fa5-3f89-4244-a90f-faeee29c98b3', 'H4sIAAAAAAAAAw3KsQnAMAwEwFW+yxiZ5eMI/CAsI2QXmT6u70roAY0tVsCJySxrmvwUw0B32LZx8JGL+Z51dQqpZnsV7x8on5NjQwAAAA==', '635709428535534980'),
(79, 188, 'Marcello Guaglio', 182, '1ad76778-930d-44eb-9217-9feda60d5dea', 'H4sIAAAAAAAAAwXB2wkAIAwDwH+nyATuJG3AgpDio/N7ZzHUO1rRBZvEEk7FeplCbhmdOEmPK3yu6XXTLQAAAA==', '635710066226956170'),
(80, 188, 'Marcello Guaglio', 182, 'bad8a98a-f566-4d9a-807f-fdce91ed29fd', 'H4sIAAAAAAAAAwXB2wkAIAwDwH+nyATuJG3AgpDio/N7ZzHUO1rRBZvEEk7FeplCbhmdOEmPK3yu6XXTLQAAAA==', '635710066226956170'),
(81, 188, 'Marcello Guaglio', 182, 'af63e1d5-c8d0-4369-9ac2-015079e7b8c6', 'H4sIAAAAAAAAAwXB2wkAIAwDwH+nyATuJG3AgpDio/N7ZzHUO1rRBZvEEk7FeplCbhmdOEmPK3yu6XXTLQAAAA==', '635710066226956170'),
(82, 188, 'Marcello Guaglio', 182, '159823b9-411d-4e47-b369-835a8d5f743c', 'H4sIAAAAAAAAAwXB2wkAIAwDwH+nyATuJG3AgpDio/N7ZzHUO1rRBZvEEk7FeplCbhmdOEmPK3yu6XXTLQAAAA==', '635710066226956170'),
(83, 188, 'Marcello Guaglio', 182, 'fc851fa5-3f89-4244-a90f-faeee29c98b3', 'H4sIAAAAAAAAAwXB2wkAIAwDwH+nyATuJG3AgpDio/N7ZzHUO1rRBZvEEk7FeplCbhmdOEmPK3yu6XXTLQAAAA==', '635710066226956170'),
(179, 203, 'Testandroid Pedro', 197, 'fc851fa5-3f89-4244-a90f-faeee29c98b3', 'H4sIAAAAAAAAA4uKqooCACqysboEAAAA', '635718727113007940'),
(180, 179, 'michele addante', 131, '3abe161b-120e-4687-85e5-1743b5900288', 'H4sIAAAAAAAAAwtJLS5RKM3LysrLyssEQT0FBc9cMCsrM09PwTMLJAIGXAAI7PFXLAAAAA==', '635719659440693180'),
(181, 179, 'michele addante', 131, '3abe161b-120e-4687-85e5-1743b5900288', 'H4sIAAAAAAAAAwsqKgIAqCh7MwMAAAA=', '635719662154616470'),
(183, 199, 'Testipad Michele', 197, 'bad8a98a-f566-4d9a-807f-fdce91ed29fd', 'H4sIAAAAAAAAA3MpKQEA2fXplAMAAAA=', '635720586828931500'),
(186, 182, 'Michele Addante', 197, 'bad8a98a-f566-4d9a-807f-fdce91ed29fd', 'H4sIAAAAAAAAA0vOTMwHAHFROu4EAAAA', '635723826073862170'),
(187, 203, 'Testandroid Pedro', 197, 'bad8a98a-f566-4d9a-807f-fdce91ed29fd', 'H4sIAAAAAAAAA0tKAgCuG661AgAAAA==', '635723835861386020'),
(188, 205, 'Matteo Maretto', 197, 'bad8a98a-f566-4d9a-807f-fdce91ed29fd', 'H4sIAAAAAAAAAwsuBgDuPEMZAgAAAA==', '635724086092418730'),
(193, 163, 'Gaetano Lenoci', 131, '3abe161b-120e-4687-85e5-1743b5900288', 'H4sIAAAAAAAAAx3LwRGAMAhE0Va2JytgcB05BDKB9GQfNmbM7R/ePwxNcBr0JjRS8D67m+ESVQtMXyBWCpjF4dtPBwmihyHtv3qMkgrwA09OyZJUAAAA', '635730061553329530'),
(194, 163, 'Gaetano Lenoci', 131, '3abe161b-120e-4687-85e5-1743b5900288', 'H4sIAAAAAAAAAwvOVMhNVMjLz1PIyFcoy0/PyUxUSFUAAOBfrqUWAAAA', '635730756155175070'),
(195, 163, 'Gaetano Lenoci', 131, '3abe161b-120e-4687-85e5-1743b5900288', 'H4sIAAAAAAAAA3POTMxXSFQoKS0pSVUoLE3NyUlVSM4vTlXIy89TALIBu5XqCSAAAAA=', '635730756412195650'),
(196, 163, 'Gaetano Lenoci', 131, '3abe161b-120e-4687-85e5-1743b5900288', 'H4sIAAAAAAAAA3POTMxXyE0sSs5XSFUoyM9UKAajxKSkxOKSxLyqRAUAWzeb3yIAAAA=', '635730756574435450'),
(197, 163, 'Gaetano Lenoci', 131, '3abe161b-120e-4687-85e5-1743b5900288', 'H4sIAAAAAAAAA/NUyM1MzVQozs/LV0gsKsosSyzJVMhUAACIuplJFwAAAA==', '635730756716438480'),
(198, 163, 'Gaetano Lenoci', 131, '3abe161b-120e-4687-85e5-1743b5900288', 'H4sIAAAAAAAAA/NUyM1MzVQozs/LV0gsKsosSyzJVADxc1IyFQpSixQAYbopoSEAAAA=', '635730756862400530'),
(199, 163, 'Gaetano Lenoci', 131, '3abe161b-120e-4687-85e5-1743b5900288', 'H4sIAAAAAAAAA/NUyM1MzVQozs/LV0gsKsosSyzJVADxc1IyFQpSixQAYbopoSEAAAA=', '635730756987777690'),
(200, 163, 'Gaetano Lenoci', 131, '3abe161b-120e-4687-85e5-1743b5900288', 'H4sIAAAAAAAAA3POTMxXSFQoKEotLslXSIbwSkpLSlIVAOF6rlMbAAAA', '635730757116734000'),
(201, 163, 'Gaetano Lenoci', 131, '3abe161b-120e-4687-85e5-1743b5900288', 'H4sIAAAAAAAAAwvOVMhNVEjJVEjOSFUozlQAAMdT8y8QAAAA', '635730757292755670'),
(202, 163, 'Gaetano Lenoci', 131, '3abe161b-120e-4687-85e5-1743b5900288', 'H4sIAAAAAAAAA/NUSMxVyMsvUUgEwrTUcgUAYz90NBEAAAA=', '635730757800414410'),
(203, 163, 'Gaetano Lenoci', 131, '3abe161b-120e-4687-85e5-1743b5900288', 'H4sIAAAAAAAAA3POTMxXyEjMVEhLLCnJV8hJVCjKTE4EYYXi0hwgNymxOFUBAE5zFiAmAAAA', '635730772605089390'),
(204, 163, 'Gaetano Lenoci', 131, '3abe161b-120e-4687-85e5-1743b5900288', 'H4sIAAAAAAAAA/NUyE0sK1bISMzLy1coy8wryVfIzFEoKErNzcxXSMlUAAChix2TIAAAAA==', '635730772955057690'),
(205, 163, 'Gaetano Lenoci', 131, '3abe161b-120e-4687-85e5-1743b5900288', 'H4sIAAAAAAAAA3POTMxXyE0sSs5XyEhUSElNzizOVyhILVLIzFFITsxJzsxXAAAZ4DNUIwAAAA==', '635730773429905380'),
(206, 163, 'Gaetano Lenoci', 131, '3abe161b-120e-4687-85e5-1743b5900288', 'H4sIAAAAAAAAAwvOVPBUyE0sK1bIzUzNVEjMzUzOVEhJLMlUKEgtUshJVCjIrKpKVMjMB7EBTAjk6C0AAAA=', '635730775432287090'),
(207, 163, 'Gaetano Lenoci', 131, '3abe161b-120e-4687-85e5-1743b5900288', 'H4sIAAAAAAAAAzMEALfv3IMBAAAA', '635730776827739880'),
(208, 163, 'Gaetano Lenoci', 131, '3abe161b-120e-4687-85e5-1743b5900288', 'H4sIAAAAAAAAAzNSAABGZseXAgAAAA==', '635730777018344390'),
(209, 163, 'Gaetano Lenoci', 131, '3abe161b-120e-4687-85e5-1743b5900288', 'H4sIAAAAAAAAAzMGAJuO0m0BAAAA', '635730777266612530'),
(210, 163, 'Gaetano Lenoci', 131, '3abe161b-120e-4687-85e5-1743b5900288', 'H4sIAAAAAAAAAzMBADgbtvMBAAAA', '635730777446987630'),
(211, 163, 'Gaetano Lenoci', 131, '3abe161b-120e-4687-85e5-1743b5900288', 'H4sIAAAAAAAAAzMFAK4rsYQBAAAA', '635730778430678830'),
(212, 163, 'Gaetano Lenoci', 131, '3abe161b-120e-4687-85e5-1743b5900288', 'H4sIAAAAAAAAAzMDABR6uB0BAAAA', '635730778530545850'),
(213, 163, 'Gaetano Lenoci', 131, '3abe161b-120e-4687-85e5-1743b5900288', 'H4sIAAAAAAAAAzMHAIJKv2oBAAAA', '635730778774010050'),
(214, 163, 'Gaetano Lenoci', 131, '3abe161b-120e-4687-85e5-1743b5900288', 'H4sIAAAAAAAAA7MAABNXAPoBAAAA', '635730778896179430'),
(215, 163, 'Gaetano Lenoci', 131, '3abe161b-120e-4687-85e5-1743b5900288', 'H4sIAAAAAAAAA7MEAIVnB40BAAAA', '635730779138436960'),
(216, 163, 'Gaetano Lenoci', 131, '3abe161b-120e-4687-85e5-1743b5900288', 'H4sIAAAAAAAAAzM0AADhJV2hAgAAAA==', '635730779257979520'),
(218, 163, 'Gaetano Lenoci', 182, 'bad8a98a-f566-4d9a-807f-fdce91ed29fd', 'H4sIAAAAAAAAAw3LwQ2AQAhFwVZeL1aCSCKJ8g27a/16nsyWJm5rF8FclIo76XT1IUZwij0KtyencA3jiPcn73yjg1VYzz9c4gNYSHb3TwAAAA==', '635731671929722650'),
(219, 163, 'Gaetano Lenoci', 182, 'bad8a98a-f566-4d9a-807f-fdce91ed29fd', 'H4sIAAAAAAAAA/NUyM1MzVQoxoYSk5ISi0sS86oSFZJS81IVUhVKShXy8vMUMhIzFUrzFAAfZC3JPAAAAA==', '635731671965606030'),
(220, 142, 'Pietro La Grotta', 182, 'bad8a98a-f566-4d9a-807f-fdce91ed29fd', 'H4sIAAAAAAAAA0vOTMwHAHFROu4EAAAA', '635732720643778230'),
(258, 12, 'CLAUDIO CERIALI', 195, '12bbe457-15be-433a-bc8e-530aae799072', 'H4sIAAAAAAAAA0vLzMtXSFTINQYAWVBqMQkAAAA=', '635734136697273860'),
(259, 12, 'CLAUDIO CERIALI', 205, 'af7ba4ac-ee32-4f33-9648-dddb2f8f4487', 'H4sIAAAAAAAAAxXK0QmAMAwFwFXeBF2hXw6SloCB1Mhr6jzu4mLq73EbxD0o6LsiDVNHo9QKQaNeijW78blhjmkZ6HEgVwZNHKfyg/EvoUspLxOC/FhPAAAA', '635734292367771270'),
(269, 165, 'Martina Malloggi', 195, '12bbe457-15be-433a-bc8e-530aae799072', 'H4sIAAAAAAAAA3MsKsosSyzJ5AIA4naDZwkAAAA=', '635735177493324320'),
(279, 186, 'Gaetano Lenoci', 131, '3abe161b-120e-4687-85e5-1743b5900288', 'H4sIAAAAAAAAAzMEALfv3IMBAAAA', '635736103241120030'),
(280, 186, 'Gaetano Lenoci', 131, '3abe161b-120e-4687-85e5-1743b5900288', 'H4sIAAAAAAAAAzMCAA2+1RoBAAAA', '635736103266529460'),
(281, 186, 'Gaetano Lenoci', 131, '3abe161b-120e-4687-85e5-1743b5900288', 'H4sIAAAAAAAAAzMGAJuO0m0BAAAA', '635736103289811670'),
(282, 186, 'Gaetano Lenoci', 131, '3abe161b-120e-4687-85e5-1743b5900288', 'H4sIAAAAAAAAAwvOVMhNVMjLz1PIyFcoy0/PyUxUSFUoSc0tyFdIyVQAANucpKEfAAAA', '635736104300299490'),
(283, 186, 'Gaetano Lenoci', 131, '3abe161b-120e-4687-85e5-1743b5900288', 'H4sIAAAAAAAAAx3K2w2AMAwDwFW8C5NYqRGR2gbSx/wgvu8OZ4CYa07hWapVuJUjuiAUx+rEjjGTsOinshGVaDS7/LNCDEvfSv33BZHWhjxRAAAA', '635736105124079130'),
(286, 163, 'Gaetano Lenoci', 182, 'bad8a98a-f566-4d9a-807f-fdce91ed29fd', 'H4sIAAAAAAAAAwvOVMhNVEjJVEjOSFVIzi9OVCjJVMhIVAAA6vTPSRgAAAA=', '635736830851305660'),
(287, 163, 'Gaetano Lenoci', 182, 'bad8a98a-f566-4d9a-807f-fdce91ed29fd', 'H4sIAAAAAAAAAwvOVMhNVMjLz1MozldIzs9NVSjOVAAADOc0YBUAAAA=', '635736831064469090'),
(288, 163, 'Gaetano Lenoci', 182, 'bad8a98a-f566-4d9a-807f-fdce91ed29fd', 'H4sIAAAAAAAAAxXJwQ2AMAwEwVauF78p4hSMZMnYKHHojTeNEX6rWWFPbOwtoRBWMTQhzrkbF9WEE+fq24q47H0wtGv881C3pmBE4gMdg2tkSQAAAA==', '635736831836060220'),
(290, 198, 'Testandroid Michele', 199, 'fc851fa5-3f89-4244-a90f-faeee29c98b3', 'H4sIAAAAAAAAA0tLSwMAzGZtJAMAAAA=', '635736904899360880'),
(315, 163, 'Gaetano Lenoci', 131, '3abe161b-120e-4687-85e5-1743b5900288', 'H4sIAAAAAAAAA3POTMxXyEjMVMjMUygoyk8vSszNTVRIyVQoykxOLUpOVEhVKE5VAADHaZ2DJgAAAA==', '635739413937976140'),
(316, 163, 'Gaetano Lenoci', 131, '3abe161b-120e-4687-85e5-1743b5900288', 'H4sIAAAAAAAAAwvOVMhNVMjLz1MozlcoTlXIzVRIScxUAACC+XK7FwAAAA==', '635739414977813870'),
(317, 163, 'Gaetano Lenoci', 131, '3abe161b-120e-4687-85e5-1743b5900288', 'H4sIAAAAAAAAAwvOVMhNVEjJVCjOTC4tylcoKcovSy1KhPITc1PzSlIVEvPySvOSMxUAd7KUsy0AAAA=', '635739415801325750'),
(318, 163, 'Gaetano Lenoci', 131, '3abe161b-120e-4687-85e5-1743b5900288', 'H4sIAAAAAAAAA3POTMxXyE0sSgaSmQo5icXJmQqleQolpfkKBYlFqUWpCgDHnyS4IgAAAA==', '635739416263312480'),
(319, 163, 'Gaetano Lenoci', 131, '3abe161b-120e-4687-85e5-1743b5900288', 'H4sIAAAAAAAAA3POTMxXSFQoKEotLslXKC5ILcpXyMvPU0jOVCguSVRISs1LBUqDRACqUR7oKgAAAA==', '635739418134585450'),
(320, 163, 'Gaetano Lenoci', 131, '3abe161b-120e-4687-85e5-1743b5900288', 'H4sIAAAAAAAAA3POTMxXyE0sSs5XSFUoKVXIy89TKCvNz1RISyxKVQAAni6PxB4AAAA=', '635739418527342980'),
(321, 163, 'Gaetano Lenoci', 131, '3abe161b-120e-4687-85e5-1743b5900288', 'H4sIAAAAAAAAA/NUyM1MzVQozs/LV0gsKsosSyzJVADxc1IyFQpSixQycxSKE3Py81IVAJDh0SkrAAAA', '635739422396686950'),
(322, 163, 'Gaetano Lenoci', 131, '3abe161b-120e-4687-85e5-1743b5900288', 'H4sIAAAAAAAAAw3J0Q2AIBBEwVZeL1ayOdaEBL0oB/Xr/M7RlYhaVeZZHsNETtPEdvPrf0NTmKtzKqIn6xYfgvkY4zoAAAA=', '635739423380262140'),
(323, 163, 'Gaetano Lenoci', 131, '3abe161b-120e-4687-85e5-1743b5900288', 'H4sIAAAAAAAAA3POTMxXyEjMVEhLLCnJV0jOz0msyszPS1VIVSgpVShOzVQozUtUAABHicHuJgAAAA==', '635739424059776880'),
(324, 163, 'Gaetano Lenoci', 131, '3abe161b-120e-4687-85e5-1743b5900288', 'H4sIAAAAAAAAAw3H0QmAMAxAwVXeOn47QaABSzUpSRtwe72/O3ikkkvMnOq2nNbZRnmucKaEhpKbc7xTaXL/cT5fe0akOAAAAA==', '635739428040278970'),
(325, 163, 'Gaetano Lenoci', 131, '3abe161b-120e-4687-85e5-1743b5900288', 'H4sIAAAAAAAAA3POTMxXyE0sSs5XSFUoKVXISQSSiUCRzByF5IxUhdxMhYzETAUAxmvbqycAAAA=', '635739429054850250'),
(337, 131, 'ioioio ioioio', 182, 'bad8a98a-f566-4d9a-807f-fdce91ed29fd', 'H4sIAAAAAAAAA3POTMwHAE/+CE4EAAAA', '635742183909735850'),
(338, 131, 'ioioio ioioio', 182, 'bad8a98a-f566-4d9a-807f-fdce91ed29fd', 'H4sIAAAAAAAAA3PJVEjJL0tVKE7NtAcApCy/BwwAAAA=', '635742183972214380'),
(340, 131, 'ioioio ioioio', 182, 'bad8a98a-f566-4d9a-807f-fdce91ed29fd', 'H4sIAAAAAAAAA1PILylJzUstSlVILS6GEqhC2HkA5FYeADgAAAA=', '635742818024267970'),
(341, 131, 'ioioio ioioio', 182, 'bad8a98a-f566-4d9a-807f-fdce91ed29fd', 'H4sIAAAAAAAAA8svKUnNSy1KVUgtLk5VyKeUV6lQmZ6eXlam4OUOANOOV21aAAAA', '635742818090239340'),
(342, 131, 'ioioio ioioio', 182, 'bad8a98a-f566-4d9a-807f-fdce91ed29fd', 'H4sIAAAAAAAAA8svKUnNSy1KVUgtLk5VyCedV6lQmZ6eXlam4OWOhgBgIBEzWwAAAA==', '635742818164793480'),
(343, 131, 'ioioio ioioio', 182, 'bad8a98a-f566-4d9a-807f-fdce91ed29fd', 'H4sIAAAAAAAAA8svKUnNSy1KVUgtLk5VyMfNAwD7JwjsKQAAAA==', '635742818226757900'),
(344, 182, 'Michele Addante', 131, '3abe161b-120e-4687-85e5-1743b5900288', 'H4sIAAAAAAAAA0vOTMwHAHFROu4EAAAA', '635742818495853800'),
(345, 131, 'ioioio ioioio', 182, 'bad8a98a-f566-4d9a-807f-fdce91ed29fd', 'H4sIAAAAAAAAA0suzclXKEktKUlVSMtMTwQANiGeqw8AAAA=', '635742820523497700'),
(346, 182, 'Michele Addante', 131, '3abe161b-120e-4687-85e5-1743b5900288', 'H4sIAAAAAAAAA8vNzynJV0jJz0lOBQDhSAYFCwAAAA==', '635742821119876790'),
(347, 182, 'Michele Addante', 131, '3abe161b-120e-4687-85e5-1743b5900288', 'H4sIAAAAAAAAA0vJyEjJSFFIAZEZAG9HgysNAAAA', '635742821860639790'),
(348, 182, 'Michele Addante', 131, '3abe161b-120e-4687-85e5-1743b5900288', 'H4sIAAAAAAAAA8tIyUipLFZIKQXSSSkANaHesA4AAAA=', '635742821878480250'),
(349, 182, 'Michele Addante', 131, '3abe161b-120e-4687-85e5-1743b5900288', 'H4sIAAAAAAAAA8tIKUupTAEAWwZ/jAYAAAA=', '635742821888877710'),
(350, 131, 'ioioio ioioio', 182, 'bad8a98a-f566-4d9a-807f-fdce91ed29fd', 'H4sIAAAAAAAAA0vOSFUoSCwuzszPS1XIT05NzMtMzs8DABO1CmoWAAAA', '635742824459401020'),
(351, 182, 'Michele Addante', 131, '3abe161b-120e-4687-85e5-1743b5900288', 'H4sIAAAAAAAAA0srSatMq1SoTAZCMAkAz9YCXhEAAAA=', '635742843695088720'),
(352, 182, 'Michele Addante', 131, '3abe161b-120e-4687-85e5-1743b5900288', 'H4sIAAAAAAAAA0vOKEovqkxJVzBNLykrKlMA8sqKAGHkLZsVAAAA', '635742859905788280'),
(353, 163, 'Gaetano Lenoci', 131, '3abe161b-120e-4687-85e5-1743b5900288', 'H4sIAAAAAAAAA3POSFUoyM9UKMlUSMzNV0jMSwYKFKcqAAB6ARrdGAAAAA==', '635742864989810190'),
(354, 163, 'Gaetano Lenoci', 131, '3abe161b-120e-4687-85e5-1743b5900288', 'H4sIAAAAAAAAA/NUyM1MzVQozs/LV0gsKsosSyzJVADxc1IyFQpSixQycxQANlLsXyQAAAA=', '635742918072705680'),
(355, 163, 'Gaetano Lenoci', 131, '3abe161b-120e-4687-85e5-1743b5900288', 'H4sIAAAAAAAAA/NUyM1MzVQozs/LV0gsKsosSyzJVADxc1IyFQBQe/6BHQAAAA==', '635742955829417220'),
(356, 163, 'Gaetano Lenoci', 131, '3abe161b-120e-4687-85e5-1743b5900288', 'H4sIAAAAAAAAAxXK0QmAMAxF0VXuTk7wSAMNWCJNu5N7uJj6ezhHMEQLrDuWJZ6b2mZelQjTRyPECnrSfK38+6V5ajovbc/BJEAAAAA=', '635742956186344510'),
(357, 163, 'Gaetano Lenoci', 131, '3abe161b-120e-4687-85e5-1743b5900288', 'H4sIAAAAAAAAA3POTMxXSFQoKEotLslXSFVIL0qsykxVSMxLzi9KBErkpJYAxaHSAKu+y1ctAAAA', '635742956693236180'),
(358, 163, 'Gaetano Lenoci', 131, '3abe161b-120e-4687-85e5-1743b5900288', 'H4sIAAAAAAAAAwvOVMhNVMjLz1PIyFcoy0/PyUxUSFUoSc0tyFdIyVQAANucpKEfAAAA', '635742958915285310'),
(359, 163, 'Gaetano Lenoci', 131, '3abe161b-120e-4687-85e5-1743b5900288', 'H4sIAAAAAAAAAwvOVMhNVEjJVCjOTC4tylfIy89TOLxCoTRPoSC1KDM/JR9IFyfmlaQqpCoU5IOUKQAAUFYgfDMAAAA=', '635742959522323800'),
(360, 163, 'Gaetano Lenoci', 131, '3abe161b-120e-4687-85e5-1743b5900288', 'H4sIAAAAAAAAAwvOVMhNVMjLz1MozldIzs9NBXKBQplAbl6+QnFZanpOZmJJvkJiTk6qgoUCACTpyywvAAAA', '635742960302470250'),
(361, 163, 'Gaetano Lenoci', 131, '3abe161b-120e-4687-85e5-1743b5900288', 'H4sIAAAAAAAAAw3IwQ2AMAwDwFW8C5O4JRWWqgY1oQ+mYRcWg3veJjoOCjl9MR2diIvYFacPFXXl+4BonAZ1lH+bquO0iW5YrBy3IXw4IpnCBx4WicFUAAAA', '635742960432354760'),
(362, 163, 'Gaetano Lenoci', 182, 'bad8a98a-f566-4d9a-807f-fdce91ed29fd', 'H4sIAAAAAAAAAxXLuQ2AQAxFwVZeO5RhtA6+5LXRXvUD4QRz0eViVhY2ho4t8TuaeHygYFpUOs2DXrfCcdZmfm+n8QJ6y8j1QwAAAA==', '635743004330136150'),
(363, 163, 'Gaetano Lenoci', 182, 'bad8a98a-f566-4d9a-807f-fdce91ed29fd', 'H4sIAAAAAAAAAx2O0Q3EMAhDV/FONwFK0spSCxEkmen2uMUu5AeQbPz8IV6BmiIMxd6GS7yBD/rUYajEYgzJY6pgWQyX7dSr+f50RrfUqYtyNsokgvh90c2H7JQ4M0Odr0FUd3JbRItoiVPcLvXgwmplnBZ2K1FkN+vTmNCYz0jlD7ULqEK3AAAA', '635743005755779430'),
(364, 163, 'Gaetano Lenoci', 182, 'bad8a98a-f566-4d9a-807f-fdce91ed29fd', 'H4sIAAAAAAAAAy3LQQ6AIBBD0av0Oh6jASJNBoco4W6uvZhMdNUu3t/QVAQ2JSHVgitGbIQMu/w8HLmYcUFiahBdz4388aHu/0ElModHOJdlRC9n+iL0YAAAAA==', '635743005801767280'),
(365, 163, 'Gaetano Lenoci', 182, 'bad8a98a-f566-4d9a-807f-fdce91ed29fd', 'H4sIAAAAAAAAA/NUyM1MzVQoBqKSxLy8fIWCotSCxKLEvJR8hZxEhdI8hYJ8hZRMhdzEHJCigtLDm4DcolSF5IxUhcMrQLpKgApSS0AkmnoAj5xBCFwAAAA=', '635743005848154440'),
(366, 163, 'Gaetano Lenoci', 182, 'bad8a98a-f566-4d9a-807f-fdce91ed29fd', 'H4sIAAAAAAAAAxWMwQ1CMQxDV/E6jGFKEJZKUrUph07EHixG/sWyn2zf8JYJKzyQMZ0pyNF2MeH3xUpm4GF5KbEdI9BZM+IZ8xAfJWEXW5u4s72sVTHN6yvG2NMqqescllXHHw436OF1AAAA', '635743005911594000'),
(367, 163, 'Gaetano Lenoci', 182, 'bad8a98a-f566-4d9a-807f-fdce91ed29fd', 'H4sIAAAAAAAAA2WKQQ6AIAwEv7J/8SUNVNkEKCnV9xvUm6edncxGMTTxZCiCrInTkIqCFaFtvEfceQnYPyfYxRVnx0E3ZC4cDzSpa0JnrGoGM23+mxudNDuGeQAAAA==', '635743634916680350'),
(368, 163, 'Gaetano Lenoci', 182, 'fc851fa5-3f89-4244-a90f-faeee29c98b3', 'H4sIAAAAAAAAA2WKQQ6AIAwEv7J/8SUNVNkEKCnV9xvUm6edncxGMTTxZCiCrInTkIqCFaFtvEfceQnYPyfYxRVnx0E3ZC4cDzSpa0JnrGoGM23+mxudNDuGeQAAAA==', '635743634916680350'),
(369, 163, 'Gaetano Lenoci', 182, 'bad8a98a-f566-4d9a-807f-fdce91ed29fd', 'H4sIAAAAAAAAAxWMwQ3DMAzEVrldMonqKsgBjuQoch6eqHtksdo/giC4URyC7JkKxdUlMxxVcXeFeXJQQQNTKsUcyxpOQiL4LDPjU1hRDsXhuEsw09ENlZ85axqogsYxBPTFj0co0ULtq6GrLbLv72/umxN/Ue47cpcAAAA=', '635743634980098250'),
(370, 163, 'Gaetano Lenoci', 182, 'fc851fa5-3f89-4244-a90f-faeee29c98b3', 'H4sIAAAAAAAAAxWMwQ3DMAzEVrldMonqKsgBjuQoch6eqHtksdo/giC4URyC7JkKxdUlMxxVcXeFeXJQQQNTKsUcyxpOQiL4LDPjU1hRDsXhuEsw09ENlZ85axqogsYxBPTFj0co0ULtq6GrLbLv72/umxN/Ue47cpcAAAA=', '635743634980098250'),
(371, 163, 'Gaetano Lenoci', 182, 'bad8a98a-f566-4d9a-807f-fdce91ed29fd', 'H4sIAAAAAAAAAyWN0RWDUAhDV8k2HcAJcvDVco4PWhE/OpF7uJiof4HkJoOiE6NCPg3iQRw7IkVahCMNvQSnSR3fRTeuXm79iRDORRJvLg2EsOA1CbWb89Hh1/FLzk97T2vX1kDDS32jmT7xqtNi/+qVOAHeybRBlAAAAA==', '635743635057676420'),
(372, 163, 'Gaetano Lenoci', 182, 'fc851fa5-3f89-4244-a90f-faeee29c98b3', 'H4sIAAAAAAAAAyWN0RWDUAhDV8k2HcAJcvDVco4PWhE/OpF7uJiof4HkJoOiE6NCPg3iQRw7IkVahCMNvQSnSR3fRTeuXm79iRDORRJvLg2EsOA1CbWb89Hh1/FLzk97T2vX1kDDS32jmT7xqtNi/+qVOAHeybRBlAAAAA==', '635743635057676420'),
(373, 163, 'Gaetano Lenoci', 182, 'bad8a98a-f566-4d9a-807f-fdce91ed29fd', 'H4sIAAAAAAAAAy3Iuw2AMAwE0FWuYyAmCMTFSXYOOZ8i0yMhuqd3ElFQic57pjBSy7IQfjBCF93wWGJZq/rENgede+vPLhfoeAE6YHu4SwAAAA==', '635743635142389110'),
(374, 163, 'Gaetano Lenoci', 182, 'fc851fa5-3f89-4244-a90f-faeee29c98b3', 'H4sIAAAAAAAAAy3Iuw2AMAwE0FWuYyAmCMTFSXYOOZ8i0yMhuqd3ElFQic57pjBSy7IQfjBCF93wWGJZq/rENgede+vPLhfoeAE6YHu4SwAAAA==', '635743635142389110'),
(375, 163, 'Gaetano Lenoci', 182, 'bad8a98a-f566-4d9a-807f-fdce91ed29fd', 'H4sIAAAAAAAAAxXHwQ2AMAwEwVa2Hcowyj1OcmLkoNSPmN9cTMvsWkV0+8Rr/ucwjxonO7KWGEpm3U4hPluKPgI4AAAA', '635743635295137740'),
(376, 163, 'Gaetano Lenoci', 182, 'fc851fa5-3f89-4244-a90f-faeee29c98b3', 'H4sIAAAAAAAAAxXHwQ2AMAwEwVa2Hcowyj1OcmLkoNSPmN9cTMvsWkV0+8Rr/ucwjxonO7KWGEpm3U4hPluKPgI4AAAA', '635743635295137740'),
(377, 163, 'Gaetano Lenoci', 182, 'bad8a98a-f566-4d9a-807f-fdce91ed29fd', 'H4sIAAAAAAAAAzWMwRECMQwDW1FPVCCMb6KH4ww5riq6oDHEg59G2tVNKGL2xGgEl87GTqx84kxcxD1nYoeqRKj/aFG4tE2/JhY9PIRoqzHMOw/Rz64+b5RwMMLUpq/zJ30BrAM41HoAAAA=', '635743635389346660'),
(378, 163, 'Gaetano Lenoci', 182, 'fc851fa5-3f89-4244-a90f-faeee29c98b3', 'H4sIAAAAAAAAAzWMwRECMQwDW1FPVCCMb6KH4ww5riq6oDHEg59G2tVNKGL2xGgEl87GTqx84kxcxD1nYoeqRKj/aFG4tE2/JhY9PIRoqzHMOw/Rz64+b5RwMMLUpq/zJ30BrAM41HoAAAA=', '635743635389346660'),
(379, 163, 'Gaetano Lenoci', 182, 'bad8a98a-f566-4d9a-807f-fdce91ed29fd', 'H4sIAAAAAAAAAw3LsQnAMAwF0VVup0ygBBUfZMtYWi51Fou7g8ddYhgzJ5U8WUbJULD0vays0q1wwuhtQ31S0w0/eF6xNbw7+QEg2yvISgAAAA==', '635743635480060730'),
(380, 163, 'Gaetano Lenoci', 182, 'fc851fa5-3f89-4244-a90f-faeee29c98b3', 'H4sIAAAAAAAAAw3LsQnAMAwF0VVup0ygBBUfZMtYWi51Fou7g8ddYhgzJ5U8WUbJULD0vays0q1wwuhtQ31S0w0/eF6xNbw7+QEg2yvISgAAAA==', '635743635480060730'),
(381, 163, 'Gaetano Lenoci', 182, 'bad8a98a-f566-4d9a-807f-fdce91ed29fd', 'H4sIAAAAAAAAAxXI0QmAMBAD0FWyi5OEWmmgXKDeOZN7uJj6Pt8mGoPCwUxjEkttqJ9JPDd2odcyFGiOZKT+i/L15USW8QLzESBtQwAAAA==', '635743636285434900'),
(382, 163, 'Gaetano Lenoci', 182, 'fc851fa5-3f89-4244-a90f-faeee29c98b3', 'H4sIAAAAAAAAAxXI0QmAMBAD0FWyi5OEWmmgXKDeOZN7uJj6Pt8mGoPCwUxjEkttqJ9JPDd2odcyFGiOZKT+i/L15USW8QLzESBtQwAAAA==', '635743636285434900'),
(383, 163, 'Gaetano Lenoci', 182, 'bad8a98a-f566-4d9a-807f-fdce91ed29fd', 'H4sIAAAAAAAAAxXLsQ2AMAxFwVXeOoxhFBdfcmJkB+YHyivuYMpF50qsSo9t8TuGuLxQ0Ba5nOHBzFPhOPumv/cCMCpDjT8AAAA=', '635743636312566210'),
(384, 163, 'Gaetano Lenoci', 182, 'fc851fa5-3f89-4244-a90f-faeee29c98b3', 'H4sIAAAAAAAAAxXLsQ2AMAxFwVXeOoxhFBdfcmJkB+YHyivuYMpF50qsSo9t8TuGuLxQ0Ba5nOHBzFPhOPumv/cCMCpDjT8AAAA=', '635743636312566210'),
(385, 163, 'Gaetano Lenoci', 182, 'bad8a98a-f566-4d9a-807f-fdce91ed29fd', 'H4sIAAAAAAAAAwvOVMhNVEjJVEjOSFUozlQ4vEIhLbGkJB8ompOqkKiQl1pcXJqXD5MGANxnKoEvAAAA', '635743636341695890'),
(386, 163, 'Gaetano Lenoci', 182, 'fc851fa5-3f89-4244-a90f-faeee29c98b3', 'H4sIAAAAAAAAAwvOVMhNVEjJVEjOSFUozlQ4vEIhLbGkJB8ompOqkKiQl1pcXJqXD5MGANxnKoEvAAAA', '635743636341695890'),
(387, 163, 'Gaetano Lenoci', 182, 'bad8a98a-f566-4d9a-807f-fdce91ed29fd', 'H4sIAAAAAAAAA1XH0Q2AIAwA0VVuFUch0MRGoMS2zi9+mryPu+Ok/E3bJm47dFJL7zRlpj1GIYRqQ/AoSmSEYRdLbpR1m7sOpaUwxJXs8f0LXHXQF2YAAAA=', '635743636613623890'),
(388, 163, 'Gaetano Lenoci', 182, 'fc851fa5-3f89-4244-a90f-faeee29c98b3', 'H4sIAAAAAAAAA1XH0Q2AIAwA0VVuFUch0MRGoMS2zi9+mryPu+Ok/E3bJm47dFJL7zRlpj1GIYRqQ/AoSmSEYRdLbpR1m7sOpaUwxJXs8f0LXHXQF2YAAAA=', '635743636613623890'),
(389, 163, 'Gaetano Lenoci', 182, 'bad8a98a-f566-4d9a-807f-fdce91ed29fd', 'H4sIAAAAAAAAAx2M0RGEMBBCW6Gnq2DVqMzEkMmufljN9XKN3eoXMA/4EIehqcGFWW5wGmrKaVjoXY0TK+P3RS/jIX0wJ5dqGOa94CD2dDam7N63hXKIKJ58NQ8uVJ6zYqNGS6r+xjiFP7eigKiAAAAA', '635743636671413280'),
(390, 163, 'Gaetano Lenoci', 182, 'fc851fa5-3f89-4244-a90f-faeee29c98b3', 'H4sIAAAAAAAAAx2M0RGEMBBCW6Gnq2DVqMzEkMmufljN9XKN3eoXMA/4EIehqcGFWW5wGmrKaVjoXY0TK+P3RS/jIX0wJ5dqGOa94CD2dDam7N63hXKIKJ58NQ8uVJ6zYqNGS6r+xjiFP7eigKiAAAAA', '635743636671413280'),
(395, 287, 'Fabrizio Sanfilippo', 165, '548c7702-174d-4407-abdb-0fc01475cc7e', 'H4sIAAAAAAAAA3POTMxXKMhMTE4tSlVwS0wqyqzKzNfT01MoTs1USMlUcE4sSczLTFSwBwCJ7U/AKQAAAA==', '635755300491206550'),
(397, 287, 'Fabrizio Sanfilippo', 165, '548c7702-174d-4407-abdb-0fc01475cc7e', 'H4sIAAAAAAAAA0tNS0stKcksS8xNzStJVcjIV0hLLCnJVyjNS1RIyc9NzEtJBLIVCvLVFYpLSgsygVwrTQBCE1jMNQAAAA==', '635755301925455360'),
(399, 287, 'Fabrizio Sanfilippo', 165, '548c7702-174d-4407-abdb-0fc01475cc7e', 'H4sIAAAAAAAAA3MvSqzKTFVIzCkpSi0pScwryVew0gQA+cN//hUAAAA=', '635755303235479340'),
(400, 165, 'Martina Malloggi', 12, '31747172-19a7-4b0c-a545-08afbf75bc45', 'H4sIAAAAAAAAA/PMTUpNzszJyS8pyQcAdpzkSwwAAAA=', '635755305693345500');

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `confirmed_partecipates`
--
CREATE TABLE IF NOT EXISTS `confirmed_partecipates` (
`iduser` int(11)
,`name` varchar(45)
,`surname` varchar(45)
,`img` varchar(128)
,`idevent` int(11)
);
-- --------------------------------------------------------

--
-- Struttura della tabella `contents`
--

CREATE TABLE IF NOT EXISTS `contents` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'pk',
  `content` text NOT NULL COMMENT 'contenuto',
  `code` varchar(20) NOT NULL COMMENT 'codice mnemmonico',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Testi statici del sistema' AUTO_INCREMENT=3 ;

--
-- Dump dei dati per la tabella `contents`
--

INSERT INTO `contents` (`id`, `content`, `code`) VALUES
(1, '<DIV style=&quot;width:95%;margin:auto auto;text-align:justify;font-size: 9pt&quot;>\n\n<P ALIGN=CENTER STYLE=&quot;margin-bottom: 1.4cm; line-height: 100%; text-transform: uppercase;&quot;>termini d&rsquo;uso di wherabout</P>\n<p><strong>1.&nbsp;Servizi offerti</strong></p>\n<p>1.1. La società Wherabout S.r.l. (CF./P.IVA. 09074750960), con sede in via Mario Bianco n. 3, 20131 Milano – REA MI/2066857 – emal: info@wherabout.it  – (in seguito la &quot;Società&quot;) concede l''uso dell''applicazione per dispositivi mobili denominata &quot;Wherabout&quot;, alle condizioni ed ai termini previsti nel presente contratto (in seguito &quot;Termini d''Uso&quot;). </p>\n<p>1.2. Wherabout consiste in una piattaforma software che mette a disposizione degli utenti strumenti e funzioni preposti a gestire la ricerca, la promozione e la prenotazione di eventi creati dagli utenti medesimi all''interno dell''Applicazione (in seguito, alternativamente, il &quot;Servizio&quot; od anche l'' &quot;Applicazione&quot;). </p>\n<p>1.3. L''utente potrà utilizzare il Servizio per creare e compilare locandine di eventi affinché altri utenti possano rivolgere all''utente le loro richieste di partecipazione. Al fine di ricercare e/o creare gli eventi, ed al fine di gestire le richieste di partecipazione trasmesse o ricevute, l''utente potrà usufruire delle funzioni di volta in volta implementate nell''Applicazione. </p><br>\n<p style=&quot;text-transform:uppercase;&quot;><strong>l''utente che accede o comunque usufruisce dell''applicazione dichiara di conoscere ed accetta i presenti termini d''uso. l''utente dichiara altresì di aver letto l''informativa sulla privacy reperibile all''indirizzo web www.wherabout.it.</strong></p>\n<p><strong>2. Requisiti di accesso e profilo dell''utente.</strong></p>\n<p>2.1. L''accesso al Servizio ed il suo utilizzo è vietato ai minori di 18 anni. Accedendo al Servizio, o comunque usufruendone, l''utente dichiara di essere maggiorenne.</p>\n<p>2.2. Nel corso della procedura di accesso all''Applicazione, l''utente è tenuto a registrare il proprio profilo al fine di poter usufruire dell''Applicazione. Nel corso di tale procedura di registrazione, l''utente dovrà fornire, in via obbligatoria ed in maniera veritiera, i propri dati identificativi e, segnatamente, il suo nome e cognome, la sua età, il sesso, il suo indirizzo e-mail e la città che costituisce il suo principale domicilio (in seguito i &quot;Dati obbligatori&quot;). Al momento di tale registrazione, od anche in un secondo tempo, l''utente potrà completare i dati del proprio profilo indicando i propri interessi e fornendo altresì un''immagine che lo rappresenti. I Dati obbligatori (ad eccezione dell''indirizzo e-mail), la città, gli interessi e l''immagine dell''utente, rappresentano informazioni che potranno essere viste dagli altri utenti che usufruiranno dell''Applicazione. Le locandine degli eventi creati dagli utenti potranno altresì essere condivise su altre piattaforme software mediante gli strumenti di &quot;sharing&quot; implementati nell''Applicazione e messi a disposizione degli utenti. </p>\n<p>2.3. Dopo che l''utente avrà completato la procedura di registrazione, i dati quali il nome, il cognome, l''età, il sesso e l''indirizzo e-mail, non saranno più modificabili nel corso dell''utilizzo dell''Applicazione. L''utente che intenda modificare tali dati dovrà contattare la Società fornendo idoneo documento che attesti che la modifica dei propri dati sia necessaria al fine di rispecchiare una modifica del reale profilo dell''utente (ad esempio perché è cambiato il domicilio principale e/o l''e-mail dell''utente). I dati quali gli interessi, la città e l''immagine dell''utente potranno invece essere da questo modificati autonomamente nel corso dell''utilizzo dell''Applicazione.</p>\n<p>2.4. Pur non eseguendo controlli e/o verifiche in ordine alla corrispondenza od alla completezza dei Dati obbligatori forniti dall''utente, la Società si riserva la facoltà di richiedere all''utente copia di un documento di identità che attesti la correttezza o la completezza dei predetti dati. Qualora l''Utente si rifiuti o non fornisca idonea documentazione entro il termine indicato dalla Società nella comunicazione, la Società potrà disattivare o cancellare definitivamente, senza alcun preavviso, il profilo dell''utente interrompendo così l''erogazione del Servizio nei suoi confronti senza corrispondere alcunché a titolo di indennizzo.  </p>\n<p><strong>3. Fornitura e interruzione del servizio.</strong></p>\n<p>3.1. La Società fornisce il Servizio così com''è, inclusivo degli eventuali &quot;bug&quot; che ne potrebbero inficiare l''utilizzo,  senza alcuna garanzia in ordine al suo funzionamento od alla sua compatibilità con i dispositivi sui quali l''utente ha eseguito l''installazione. La Società non garantisce la continuità del Servizio o la permanenza e l''integrità dei dati caricati nell''Applicazione da parte dell''utente.  </p>\n<p>3.2. Al fine di adeguare le caratteristiche del Servizio alle esigenze della maggior parte degli utenti che ne usufruiranno la Società potrà implementare, a sua discrezione, nuove funzioni all''interno dell''Applicazione e/o rimuovere funzioni esistenti od apportarvi modifiche. Nel caso in cui vengano apportate modifiche al Servizio o nel caso in cui vengano implementate o modificate le relative funzioni, la Società non garantisce il mantenimento e/o l''integrità dei dati e delle informazioni relative al pregresso utilizzo del Servizio da parte dell''utente. Tali dati ed informazioni potrebbero comprendere, a titolo esemplificativo, i dati relativi al profilo dell''utente, i dati degli eventi già creati dall''utente all''interno dell''Applicazione così come ogni altra informazione ad essi relativa (come il numero e l''identità dei loro partecipanti, le immagini e/o le didascalie esplicative). </p>\n<p>3.3. La Società si riserva altresì la facoltà di interrompere definitivamente l''erogazione del Servizio o di sue singole funzionalità in qualsiasi momento e senza alcun preavviso. Nel caso in cui il Servizio venga interrotto o cessi, per qualsiasi ragione, tutti i dati e le informazioni che l''utente avrà caricato ed inserito nell''Applicazione durante il periodo del suo pregresso utilizzo potranno essere cancellati a discrezione della Società e senza alcun preavviso, o potranno comunque risultare corrotti o non essere più ripristinabili. </p>\n<p><strong>4. Utilizzo del Servizio.</strong></p>\n<p>4.1. La Società non svolge alcun controllo sugli eventi e sui contenuti creati dagli utenti all''interno dell''Applicazione né svolge alcuna verifica in ordine alla legittimità, correttezza e/o alla veridicità degli stessi, così come dei dati identificativi degli utenti.</p>\n<p>4.2. L''utente sarà il solo e pieno responsabile delle informazioni e dei contenuti (dati, immagini, descrizioni, messaggi e/o indicazioni di qualsiasi tipo) che inserirà all''interno dell''Applicazione e delle conseguenze che dovessero, a qualsiasi titolo, insorgere a danno e/o pregiudizio di terzi a causa dell''inserimento e/o dell''utilizzo delle predette informazioni e contenuti. L''utente sarà altresì responsabile di ogni conseguenza che dovesse derivare a danno e/o pregiudizio di terzi a causa della condotta che lo stesso manterrà all''interno dell''Applicazione. Tale condotta può ad esempio concernere le modalità con cui l''utente utilizzerà le funzioni dell''Applicazione (inviare notifiche, gestire le prenotazioni degli eventi, modificare le informazioni ad essi relative, inviare messaggi ecc.).  </p>\n<p>4.3. L''utente si impegna ad utilizzare l''Applicazione inserendo informazioni e contenuti (dati, immagini, descrizioni, messaggi o indicazioni di qualsiasi tipo) che:</p>\n<ol style=&quot;list-style-type: lower-alpha;&quot;>\n<li>siano veritieri, siano nella legittima disponibilità dell''utente e corrispondano ai reali intendimenti dell''utente;</li>\n<li>non violino diritti, a qualsiasi titolo, di terze parti, ivi compresi diritti di proprietà intellettuale ed industriale;</li>\n<li>non siano diffamatori, denigratori, offensivi o comunque lesivi dell''immagine altrui;</li>\n<li>non siano osceni, contrari al pubblico pudore o comunque contenenti imprecazioni o immagini di nudo;</li>\n<li>non siano comunque contrari alle linee guida di buona condotta pubblicate nel sito web della Società www.wherabout.it oltre che allo spirito di buona fede che anima la community degli utenti dell''Applicazione.</li>\n</ol>\n<p>4.4. L''utente si impegna a non pubblicare e/o promuovere gli stessi eventi creati all''interno dell''Applicazione utilizzando piattaforme software di terze parti, fatto solo salvo il caso in cui la condivisione di tali eventi su altre piattaforme avvenga per il tramite di funzioni integrate all''interno dell''Applicazione medesima. L''utente si impegna altresì a non pubblicare o diffondere le informazioni relative ai profili e/o agli eventi degli altri utenti salvo ciò non sia consentito per il tramite di funzioni di condivisione integrate all''interno dell''Applicazione medesima. </p>\n<p>4.5. L''utilizzo dell''Applicazione viene concesso per finalità private e non a scopo di lucro o commerciale. In nessun caso l''utente potrà utilizzare l''Applicazione per promuovere servizi o prodotti propri o di terze parti. A titolo meramente esemplificativo di quanto precede, l''utente non potrà:</p>\n<ol>\n<li>creare eventi che abbiano ad oggetto pubblicità di prodotti e/o servizi di qualsiasi natura;</li>\n<li>creare eventi che abbiano ad oggetto l''offerta d''acquisto di un prodotto e/o un servizio;</li>\n<li>creare eventi la cui partecipazione sia comunque subordinata all''elargizione di una somma di denaro a favore dell''utente o di terzi, salve solo le spese accessorie e funzionali allo svolgimento delle attività previste dall''evento e pur sempre qualora dette spese non siano connesse alla promozione e/o alla vendita di un prodotto e/o un servizio dell''utente o di terzi con i quali l''utente abbia in essere un rapporto di tipo lavorativo e/o di carattere commerciale.</li>\n</ol>\n<p>4.6. L''utente non potrà in nessun caso utilizzare l''Applicazione o creare eventi al fine di promuovere esplicitamente incontri a sfondo sessuale o comunque attività illegali. </p>\n<p><strong>5. Diritti di proprietà.</strong></p>\n<p>5.1. La società è la sola, e resterà l''esclusiva, titolare dei segni distintivi (tra cui marchi registrati e non registrati, denominativi e/o figurativi, denominazioni societarie e nomi a dominio quali, a titolo esemplificativo, i segni contenenti la parola &quot;wherabout&quot; od il logo dell''Applicazione &quot;W&quot;) utilizzati per contraddistinguere e promozionare l''Applicazione. La Società è inoltre la sola, e resterà l''esclusiva, titolare del software e delle interfacce grafiche (ivi incluse le immagini e qualsiasi altra sezione grafica) su cui si basa l''Applicazione. </p>\n<p>5.2. L''utente dovrà limitarsi al mero utilizzo dell''Applicazione in qualità di suo fruitore e per le finalità che le sono proprie nei limiti previsti dai presenti Termini d''Uso. Fatto salvo ciò, l''utente non potrà: </p>\n<ol style=&quot;list-style-type: lower-alpha;&quot;>\n<li>riprodurre, sfruttare o utilizzare, in tutto o in parte, detti segni distintivi, detto software e dette interfacce, in qualsiasi contesto ed a qualsiasi fine, economico e non; </li>\n<li>eseguire studi di &quot;reverse engineering&quot;, decompilare o modificare, in tutto o in parte, il software alla base dell''Applicazione;</li>\n<li>eseguire tentativi di indicizzazione e/o di monitoraggio dei contenuti dell''Applicazione.</li>\n</ol>\n<p><strong>6. Diritti sui contenuti dell''utente.</strong></p>\n<p>6.1. La Società non rivendica ed acquista la proprietà dei contenuti caricati dall''utente durante l''utilizzo dell''Applicazione. L''utente, per contro, concede alla società una licenza a titolo gratuito, non esclusiva e trasferibile, per l''utilizzo dei contenuti caricati dall''utente quali le immagini delle locandine degli eventi ed i relativi testi descrittivi, al fine di esemplificare e/o promuovere l''Applicazione sui media, sul web o su supporti cartacei di qualsiasi tipo. </p>\n<p><strong>7. Disattivazione.</strong></p>\n<p>7.1. La Società si riserva la facoltà di disattivare o cancellare, anche in via definitiva, e senza alcun preavviso, i profili di quegli utenti che dovessero contenere dati o informazioni (ivi inclusa l''immagine dell''utente allorché questa raffiguri una persona) non veritieri o comunque incompleti. Tale disattivazione e/o cancellazione potrà essere effettuata a completa discrezione della Società, ogni qual volta questa lo ritenga necessario a fronte dell''incompletezza dei predetti dati ed informazioni od anche solo quando vi sia il sospetto di una loro difformità rispetto al reale profilo dell''utente. L''utente che riterrà che il proprio profilo sia stato cancellato o disattivato per errore (nonostante la completezza o la veridicità dei propri dati) potrà contattare la Società per segnalare l''errore e per chiedere, ove sia possibile, il rispristino del proprio profilo, che potrà comunque avvenire con perdita dei dati dell''utente relativi al suo uso pregresso dell''Applicazione. Al fine di richiedere detto ripristino, l''utente sarà comunque tenuto a trasmettere alla società copia di un documento di identità idoneo a comprovare la correttezza dei propri dati.  </p>\n<p>7.2. La Società potrà altresì disattivare o cancellare, anche in via definitiva e senza alcun preavviso, i profili di quegli utenti che abbiano utilizzato l''Applicazione in maniera difforme alle condizioni, nessuna esclusa, definite nei presenti Termini d''Uso. In tal caso, la società potrà anche decidere, a propria discrezione, di vietare nuovi accessi all''Applicazione da parte dell''utente per il futuro. </p>\n<p>7.3. In ogni caso, la Società potrà, senza alcun preavviso, modificare o cancellare parte di quei dati e/o di quelle informazioni inserite dall''utente nell''Applicazione (quali, a titolo esemplificativo, immagini, testi e/o messaggi) contrari alle condizioni, nessuna esclusa, contenute nei presenti Termini d''Uso o comunque alle linee guida di buona condotta pubblicate nel sito web della Società www.wherabout.it o comunque allo spirito di buona fede che anima la community degli utenti dell''Applicazione. </p>\n<p><strong>8. Regime di responsabilità e manleva.</strong></p>\n<p>8.1. L''utente si impegna a mantenere indenne la Società, le società da questa controllate e/o con essa affiliate, i suoi dipendenti, agenti e/o aventi causa, da qualsiasi conseguenza pregiudizievole e/o pretesa di terze parti che dovesse insorgere a causa della violazione da parte dell''utente delle condizioni, nessuna esclusa, previste nei presenti Termini d''Uso o comunque a causa dell''uso dell''Applicazione da parte dell''utente.</p>\n<p>8.2. L''utente riconosce ed accetta che a fronte di qualsiasi interruzione e/o disattivazione e/o modifica e/o cancellazione del Servizio o di taluno dei suoi contenuti o delle sue funzioni, così come nell''eventualità in cui dovessero verificarsi perdite improvvise di dati ed informazioni contenute nell''Applicazione, nessuna esclusa (come ad esempio i dati relativi al profilo dell''utente, agli eventi, alle partecipazioni degli altri utenti, ai messaggi od alle immagini caricate nell''Applicazione), la Società non potrà essere ritenuta responsabile e non sarà tenuta a corrispondere all''utente alcuna indennità o risarcimento, a qualsiasi titolo.</p>\n<p>8.3. L''utente riconosce ed accetta che la Società non potrà essere ritenuta responsabile del comportamento mantenuto dagli altri utenti dell''applicazione, ivi incluso il mancato rispetto degli impegni da questi assunti a fronte della prenotazione agli eventi da parte dell''utente.  </p>\n<p><strong>9. Modifiche dei Termini d''Uso.</strong></p>\n<p>9.1. La Società si riserva la facoltà di modificare in tutto o in parte le condizioni incluse nei presenti Termini d''Uso dandone avviso all''utente all''interno dell''Applicazione. Ogni qual volta l''utente continui ad utilizzare l''Applicazione a seguito di dette modifiche, egli dichiara ed accetta di essere vincolato alle nuove condizioni dei Termini d''Uso.</p>\n<p><strong>10. Nullità.</strong></p>\n<p>10.1. Qualora una clausola od una condizione prevista dai presenti Termini d''Uso dovesse essere ritenuta nulla, i presenti Termini d''Uso dovranno essere interpretati come aventi ad oggetto una clausola od una condizione che si attenga il più possibile al tenore di quella nulla. Se tale sostituzione non dovesse essere possibile, le restanti clausole e condizioni presenti nei Termini d''uso si intenderà comunque valide ed efficaci. </p>\n<p><strong>11. Comunicazioni.</strong></p>\n<p>11.1 Tutte le comunicazione che l''utente vorrà rivolgere alla Società dovranno avvenire a mezzo e-mail o lettera raccomandata ai recapiti della Società indicati al paragrafo 1.1.</p>\n<p><strong>12. Foro competente e legge applicabile.</strong></p>\n<p>12.1. Il presente contratto è regolato dalla legge italiana.</p>\n<p>12.2. Fatte salve le disposizioni di legge inderogabili ove applicabili, in ordine a qualunque controversia tra la società e l''utente, relativa all''interpretazione, all''applicazione, all''esecuzione, all''inadempimento ed alla risoluzione dei presenti Termini d''Uso o in qualsiasi modo connessa agli stessi, sarà competente in via esclusiva il Foro di Milano.</p>\n</DIV>', 'TERMS_CONDITIONS'),
(2, '<DIV STYLE=&quot;width:95%;margin:auto auto&quot;>\n<P CLASS=&quot;western&quot; ALIGN=CENTER STYLE=&quot;margin-bottom: 0cm; line-height: 100%; text-transform: uppercase;&quot;>\n<FONT SIZE=2><B>trattamento dei dati personali e informativa in\nmateria di cookies.</B></FONT></P>\n<P CLASS=&quot;western&quot; ALIGN=JUSTIFY STYLE=&quot;margin-bottom: 0cm; line-height: 100%&quot;>\n<BR>\n</P>\n<P CLASS=&quot;western&quot; ALIGN=JUSTIFY STYLE=&quot;margin-bottom: 0cm; line-height: 100%&quot;>\n<FONT SIZE=2>Gentile Utente, </FONT>\n</P>\n<P CLASS=&quot;western&quot; ALIGN=JUSTIFY STYLE=&quot;margin-bottom: 0cm; line-height: 100%&quot;>\n<BR>\n</P>\n<P CLASS=&quot;western&quot; ALIGN=JUSTIFY STYLE=&quot;margin-bottom: 0cm; line-height: 100%&quot;>\n<FONT SIZE=2>ai sensi dell&rsquo;art. 13 d.lgs. 196/2003 (di seguito:\n&lsquo;Codice Privacy&rsquo;) ed in relazione ai dati che fornirai o\nche verranno reperiti dai cookies per farti usufruire dei servizi\nprestati da Wherabout S.r.l. (in seguito la &ldquo;Societ&agrave;&rdquo;),\ntra cui i servizi offerti nel sito web </FONT><FONT COLOR=&quot;#0000ff&quot;><U><A HREF=&quot;http://www.wherabout.it/&quot;><FONT SIZE=2>www.wherabout.it</FONT></A></U></FONT><FONT SIZE=2>\nnonch&eacute; l&rsquo;applicazione della Societ&agrave; denominata\n&ldquo;Wherabout&rdquo;, ti informiamo di quanto segue:</FONT></P>\n<OL>\n	<LI><P CLASS=&quot;western&quot; ALIGN=JUSTIFY STYLE=&quot;margin-bottom: 0cm; line-height: 100%&quot;>\n	<FONT SIZE=2><B>Modalit&agrave; di raccolta dei dati e loro\n	tipologie. </B></FONT>\n	</P>\n</OL>\n<P CLASS=&quot;western&quot; ALIGN=JUSTIFY STYLE=&quot;margin-bottom: 0cm; line-height: 100%&quot;>\n<FONT SIZE=2>In qualit&agrave; di utente interessato potresti voler\naccedere al sito web della Societ&agrave; e/o alle relative funzioni\ntra cui, ad esempio, l&rsquo;uso di </FONT><FONT SIZE=2><I>form</I></FONT><FONT SIZE=2>\npreposti alla ricezione di informazioni di carattere utilitario e/o\ncommerciale (in seguito i &ldquo;Servizi&rdquo;), e potresti anche\nessere interessato ad eseguire il download, l&rsquo;iscrizione e\nl&rsquo;accesso all&rsquo;applicazione per dispositivi mobili e\npiattaforme web sviluppata dalla Societ&agrave; e denominata\n&ldquo;Wherabout&rdquo; (in seguito la &ldquo;Applicazione&rdquo;).\nAl momento di detti accessi, download ed iscrizione e/o in qualsiasi\naltro momento in cui usufruirai dei Servizi e/o dell&rsquo;Applicazione,\npotresti essere invitato e/o decidere di fornire informazioni che ti\nriguardano. Tali informazioni potrebbero comprendere il tuo nome,\ncognome, et&agrave;, sesso, citt&agrave;, indirizzo postale e/o\nelettronico, numeri di telefono, coordinate bancarie e/o immagini\ndella tua persona; inoltre, accedendo al sito web della Societ&agrave;,\ni cookies ivi presenti potrebbero reperire informazioni relative alle\ntue preferenze di navigazione (in seguito, collettivamente, i\n&ldquo;Dati&rdquo;). &Egrave; possibile che alcuni di questi Dati\nsiano necessari e/o obbligatori al fine di consentire la corretta\nerogazione dei Servizi e l&rsquo;uso delle funzionalit&agrave;\ndell&rsquo;Applicazione. </FONT>\n</P>\n<OL START=2>\n	<LI><P CLASS=&quot;western&quot; ALIGN=JUSTIFY STYLE=&quot;margin-bottom: 0cm; line-height: 100%&quot;>\n	<FONT SIZE=2><B>Finalit&agrave; del trattamento dei dati.</B></FONT></P>\n</OL>\n<P CLASS=&quot;western&quot; ALIGN=JUSTIFY STYLE=&quot;margin-bottom: 0cm; line-height: 100%&quot;>\n<FONT SIZE=2>I Dati verranno trattati al solo fine di dare esecuzione\nai termini ed ai termini d&rsquo;uso da te accettati in fase di\ndownload ed attivazione dell&rsquo;Applicazione e/o al solo fine di\nerogare i Servizi. Detto trattamento, consente alla Societ&agrave; di\nfarti usufruire dei Servizi e dell&rsquo;Applicazione e, pertanto,\ndi: ultimare l&rsquo;attivazione dei Servizi o dell&rsquo;Applicazione,\nassegnarti un nome utente ed una password, permetterti di accedere ai\nServizi e/o all&rsquo;Applicazione consentendoti di gestire e di\nusufruire di ogni loro funzionalit&agrave; nel modo corretto,\ngarantirti un&rsquo;esperienza ottimale nella navigazione del sito\nweb della Societ&agrave;. </FONT>\n</P>\n<OL START=3>\n	<LI><P CLASS=&quot;western&quot; ALIGN=JUSTIFY STYLE=&quot;margin-bottom: 0cm; line-height: 100%&quot;>\n	<FONT SIZE=2><B>Modalit&agrave; del trattamento dei dati.</B></FONT></P>\n</OL>\n<P CLASS=&quot;western&quot; ALIGN=JUSTIFY STYLE=&quot;margin-bottom: 0cm; line-height: 100%&quot;>\n<FONT SIZE=2>Il trattamento dei Dati verr&agrave; realizzato per\nmezzo delle operazioni o complesso di operazioni indicate all&rsquo;art.\n4, comma 1, lett. a), Codice Privacy: raccolta, registrazione,\norganizzazione, osservazione, consultazione, elaborazione,\nmodificazione, selezione, estrazione, raffronto, utilizzo,\ninterconnessione, blocco, comunicazione, cancellazione e distruzione\ndei dati. Le operazioni possono essere svolte con o senza l&rsquo;ausilio\ndi strumenti elettronici o comunque automatizzati. Il trattamento &egrave;\nsvolto dal titolare e/o dagli incaricati del trattamento. </FONT>\n</P>\n<OL START=4>\n	<LI><P CLASS=&quot;western&quot; ALIGN=JUSTIFY STYLE=&quot;margin-bottom: 0cm; line-height: 100%&quot;>\n	<FONT SIZE=2><B>Rifiuto di conferimento dei dati e prestazione del\n	consenso. </B></FONT>\n	</P>\n</OL>\n<P CLASS=&quot;western&quot; ALIGN=JUSTIFY STYLE=&quot;margin-bottom: 0cm; line-height: 100%&quot;>\n<FONT SIZE=2>Il conferimento dei Dati per le finalit&agrave; di\ntrattamento di cui al punto 2) potrebbe essere necessario e\nindispensabile affinch&eacute; tu possa usufruire dei Servizi e/o\ndell&rsquo;Applicazione nel modo corretto. Il tuo eventuale rifiuto\ndi fornire alcuni di questi Dati potrebbe pertanto essere d&rsquo;ostacolo\nalla fornitura dei Servizi e delle funzionalit&agrave;\ndell&rsquo;Applicazione o comunque condurre all&rsquo;interruzione\ndei rapporti contrattuali tra te e la Societ&agrave;. </FONT>\n</P>\n<OL START=5>\n	<LI><P CLASS=&quot;western&quot; ALIGN=JUSTIFY STYLE=&quot;margin-bottom: 0cm; line-height: 100%&quot;>\n	<FONT SIZE=2><B>Comunicazione dei dati.</B></FONT></P>\n</OL>\n<P CLASS=&quot;western&quot; ALIGN=JUSTIFY STYLE=&quot;margin-bottom: 0cm; line-height: 100%&quot;>\n<FONT SIZE=2>Potranno venire a conoscenza dei Dati i soggetti di\nvolta in volta nominati dal titolare quali responsabili del\ntrattamento o incaricati del trattamento. </FONT>\n</P>\n<OL START=6>\n	<LI><P CLASS=&quot;western&quot; ALIGN=JUSTIFY STYLE=&quot;margin-bottom: 0cm; line-height: 100%&quot;>\n	<FONT SIZE=2><B>Atre tipologie di dati non trattati. </B></FONT>\n	</P>\n</OL>\n<P CLASS=&quot;western&quot; ALIGN=JUSTIFY STYLE=&quot;margin-bottom: 0cm; line-height: 100%&quot;>\n<FONT SIZE=2>In nessun caso verranno trattati dati personali\nsensibili che l&rsquo;utente decider&agrave; eventualmente di\ncomunicare durante l&rsquo;utilizzo dei Servizi o dell&rsquo;Applicazione.\nTali dati comprendono, cos&igrave; come definiti dall&rsquo;articolo\n4 lettera d) del Decreto Legislativo 30 giugno 2003, n. 196, &ldquo;i\ndati personali idonei a rilevare l&rsquo;origine razziale ed etnica,\nle convinzioni religiose, filosofiche o di altro genere, le opinioni\npolitiche, l&rsquo;adesione a partiti, sindacati, associazioni od\norganizzazioni a carattere religioso, filosofico, politico o\nsindacale, nonch&eacute; i dati personali idonei a rivelare lo stato\ndi salute e la vita sessuale&rdquo;.</FONT></P>\n<OL START=7>\n	<LI><P CLASS=&quot;western&quot; ALIGN=JUSTIFY STYLE=&quot;margin-bottom: 0cm; line-height: 100%&quot;>\n	<FONT SIZE=2><B>Titolare/responsabile del trattamento.</B></FONT></P>\n</OL>\n<P CLASS=&quot;western&quot; ALIGN=JUSTIFY STYLE=&quot;margin-bottom: 0cm; line-height: 100%&quot;>\n<FONT SIZE=2>Il titolare del trattamento dei Dati &egrave; Wherabout\nS.r.l.</FONT><FONT SIZE=2>, via Mario Bianco n. 3, 20135 &ndash;\nMilano (e-mail </FONT><FONT COLOR=&quot;#0000ff&quot;><U><A HREF=&quot;mailto:wherabout@wherabout.it&quot;><FONT SIZE=2>wherabout@wherabout.it</FONT></A></U></FONT><FONT SIZE=2>).\nPotrai ottenere un elenco completo dei responsabili del trattamento\nnominati dal Titolare contattando direttamente il titolare del\ntrattamento senza alcuna formalit&agrave;, utilizzando il recapito\npostale o il contatto e-mail sopra indicati.</FONT></P>\n<OL START=8>\n	<LI><P CLASS=&quot;western&quot; ALIGN=JUSTIFY STYLE=&quot;margin-bottom: 0cm; line-height: 100%&quot;>\n	<FONT SIZE=2><B>Trasferimento dei dati all&rsquo;estero.</B></FONT></P>\n</OL>\n<P CLASS=&quot;western&quot; ALIGN=JUSTIFY STYLE=&quot;margin-bottom: 0cm; line-height: 100%&quot;>\n<FONT SIZE=2>I Dati possono essere trasferiti verso Paesi dell&rsquo;Unione\nEuropea ed eccezionalmente verso Paesi terzi rispetto all&rsquo;Unione\nEuropea, nell&rsquo;ambito esclusivo delle finalit&agrave; di cui al\npunto 2). </FONT>\n</P>\n<OL START=9>\n	<LI><P CLASS=&quot;western&quot; ALIGN=JUSTIFY STYLE=&quot;margin-bottom: 0cm; line-height: 100%&quot;>\n	<FONT SIZE=2><B>Diritti dell&rsquo;interessato.</B></FONT></P>\n</OL>\n<P CLASS=&quot;western&quot; ALIGN=JUSTIFY STYLE=&quot;margin-right: -0cm; margin-bottom: 0cm; line-height: 100%&quot;>\n<FONT SIZE=2>Hai diritto in qualunque momento di ottenere la conferma\ndell&rsquo;esistenza o meno dei tuoi Dati e di conoscerne il\ncontenuto e l&rsquo;origine, verificarne l&rsquo;esattezza o\nchiederne l&rsquo;integrazione o l&rsquo;aggiornamento, oppure la\nrettificazione (art. 7 Codice Privacy). Ai sensi del medesimo\narticolo, hai diritto di chiedere la cancellazione, la trasformazione\nin forma anonima o il blocco dei dati trattati in violazione di\nlegge, nonch&eacute; di opporti per motivi legittimi al loro\ntrattamento. Le richieste, ai sensi del presente articolo, andranno\nrivolte al titolare del trattamento.</FONT></P>\n<P CLASS=&quot;western&quot; ALIGN=CENTER STYLE=&quot;margin-bottom: 0cm; line-height: 100%&quot;>\n<BR>\n</P>\n<P CLASS=&quot;western&quot; ALIGN=CENTER STYLE=&quot;margin-bottom: 0cm; line-height: 100%&quot;>\n<BR>\n</P>\n<P CLASS=&quot;western&quot; ALIGN=CENTER STYLE=&quot;margin-bottom: 0cm; line-height: 100%&quot;>\n<BR>\n</P>\n<P CLASS=&quot;western&quot; ALIGN=CENTER STYLE=&quot;margin-bottom: 0cm; line-height: 100%&quot;>\n<BR>\n</P>\n<P CLASS=&quot;western&quot; ALIGN=CENTER STYLE=&quot;margin-bottom: 0cm; line-height: 100%; text-transform: uppercase;&quot;>\n<FONT SIZE=2><B>ulteriori informazioni relative ai cookies.</B></FONT></P>\n<P CLASS=&quot;western&quot; ALIGN=JUSTIFY STYLE=&quot;margin-bottom: 0cm; line-height: 100%&quot;>\n<BR>\n</P>\n<OL START=10>\n	<LI><P CLASS=&quot;western&quot; ALIGN=JUSTIFY STYLE=&quot;margin-bottom: 0cm; line-height: 100%&quot;>\n	<FONT SIZE=2><B>Cosa sono i cookies?</B></FONT></P>\n</OL>\n<P CLASS=&quot;western&quot; ALIGN=JUSTIFY STYLE=&quot;margin-bottom: 0cm; line-height: 100%&quot;>\n<FONT SIZE=2>I cookies sono piccoli file che vengono inviati da un\nsito web al dispositivo utilizzato dall&rsquo;utente nel momento in\ncui egli accede e consulta il sito. Il contenuto di tali file viene\npoi ritrasmesso al medesimo sito web alla visita successiva\ndell&rsquo;utente. I cookie c.d. di &quot;terze parti&quot; vengono,\ninvece, impostati da un sito web diverso da quello che l&#39;utente sta\nvisitando. </FONT>\n</P>\n<OL START=11>\n	<LI><P CLASS=&quot;western&quot; ALIGN=JUSTIFY STYLE=&quot;margin-bottom: 0cm; line-height: 100%&quot;>\n	<FONT SIZE=2><B>A cosa servono i cookies?</B></FONT></P>\n</OL>\n<P CLASS=&quot;western&quot; ALIGN=JUSTIFY STYLE=&quot;margin-bottom: 0cm; line-height: 100%&quot;>\n<FONT SIZE=2>I cookies possono essere utilizzati per finalit&agrave;\ndiverse, tra cui la raccolta di informazioni necessarie per\nconsentire processi di autenticazione, per eseguire il monitoraggio\ndelle sessioni di navigazione, per memorizzare informazioni relative\na specifiche configurazioni riguardanti gli utenti, per tracciare le\npreferenze dell&rsquo;utente a fini pubblicitari ecc.</FONT></P>\n<OL START=12>\n	<LI><P CLASS=&quot;western&quot; ALIGN=JUSTIFY STYLE=&quot;margin-bottom: 0cm; line-height: 100%&quot;>\n	<FONT SIZE=2><B>Cosa sono i cookies &quot;tecnici&quot;?</B></FONT></P>\n</OL>\n<P CLASS=&quot;western&quot; ALIGN=JUSTIFY STYLE=&quot;margin-bottom: 0cm; line-height: 100%&quot;>\n<FONT SIZE=2>Sono i cookies necessari a consentire una corretta\nnavigazione del sito, l&rsquo;erogazione dei servizi richiesti\ndall&rsquo;utente e, pi&ugrave; in generale, a migliorare il\nfunzionamento del portale web. I cookie tecnici non vengono\nutilizzati per scopi ulteriori, come ad esempio scopi pubblicitari\ne/o di profilazione delle preferenze dell&rsquo;utente: senza il loro\nutilizzo, alcune operazioni effettuate sul sito potrebbero non essere\npossibili o sarebbero comunque pi&ugrave; complesse e, in alcuni\ncasi, meno sicure.</FONT></P>\n<OL START=13>\n	<LI><P CLASS=&quot;western&quot; ALIGN=JUSTIFY STYLE=&quot;margin-bottom: 0cm; line-height: 100%&quot;>\n	<FONT SIZE=2><B>Cosa sono i cookies di profilazione&quot;?</B></FONT></P>\n</OL>\n<P CLASS=&quot;western&quot; ALIGN=JUSTIFY STYLE=&quot;margin-bottom: 0cm; line-height: 100%&quot;>\n<FONT SIZE=2>Sono i cookies utilizzati per tracciare la navigazione\ndell&#39;utente in rete e creare profili sulle sue preferenze, abitudini,\nscelte, ecc. Grazie a questi cookie possono essere trasmessi\nall&#39;utente messaggi pubblicitari e/o commerciali rispondenti alle\nnecessit&agrave; dell&rsquo;utente e/o relativi ai suoi interessi.</FONT></P>\n<OL START=14>\n	<LI><P CLASS=&quot;western&quot; ALIGN=JUSTIFY STYLE=&quot;margin-bottom: 0cm; line-height: 100%&quot;>\n	<FONT SIZE=2><B>Quali cookies utilizza il sito wherabout.it?</B></FONT></P>\n</OL>\n<P ALIGN=JUSTIFY STYLE=&quot;margin-bottom: 0cm; line-height: 100%&quot;><FONT FACE=&quot;Times New Roman, serif&quot;><FONT SIZE=3><FONT FACE=&quot;Calibri, sans-serif&quot;><FONT SIZE=2>Il\nsito web www.wherabout.it utilizza unicamente cookies &ldquo;tecnici&rdquo;,\nper garantirti un&rsquo;esperienza pi&ugrave; funzionale, compresi i</FONT></FONT><FONT FACE=&quot;Calibri, sans-serif&quot;><FONT SIZE=2><B>\n</B></FONT></FONT><STRONG><FONT FACE=&quot;Calibri, sans-serif&quot;><FONT SIZE=2><SPAN STYLE=&quot;font-weight: normal&quot;>cookies\nanalytics forniti da google, e sempre allo scopo di </SPAN></FONT></FONT></STRONG><FONT FACE=&quot;Calibri, sans-serif&quot;><FONT SIZE=2>ottimizzare\nil portale grazie al reperimento di informazioni in forma aggregata\nsul numero degli utenti e su come questi vi accedono. </FONT></FONT></FONT></FONT>\n</P>\n<OL START=15>\n	<LI><P CLASS=&quot;western&quot; ALIGN=JUSTIFY STYLE=&quot;margin-bottom: 0cm; line-height: 100%&quot;>\n	<FONT SIZE=2><B>Come posso gestire i cookies?</B></FONT></P>\n</OL>\n<P ALIGN=JUSTIFY STYLE=&quot;margin-bottom: 0cm; line-height: 100%&quot;><FONT FACE=&quot;Times New Roman, serif&quot;><FONT SIZE=3><FONT COLOR=&quot;#292929&quot;><FONT FACE=&quot;Calibri, sans-serif&quot;><FONT SIZE=2>Potrai\nin</FONT></FONT></FONT><FONT FACE=&quot;Calibri, sans-serif&quot;><FONT SIZE=2>\nogni caso esprimere le tue preferenze in merito &nbsp;all&rsquo;uso\ndei cookie anche mediante le impostazioni del browser. Modificando\ntali impostazioni, si pu&ograve; infatti accettare o rifiutare i\ncookies oppure decidere di ricevere un messaggio di avviso prima di\naccettare un cookie dal sito web visitato. I cookies, inoltre,\n&nbsp;possono essere eliminati cancellando il contenuto della\ncartella &ldquo;cookie&rdquo; utilizzata dal browser. Il pannello di\namministrazione dei cookies pu&ograve; variare a seconda del browser\nutilizzato dall&rsquo;utente. Ti invitiamo pertanto a consultare i\nsiti web dedicati.</FONT></FONT></FONT></FONT></P>\n<P CLASS=&quot;western&quot; STYLE=&quot;margin-bottom: 0cm; line-height: 100%&quot;><BR>\n</P>\n<P CLASS=&quot;western&quot; STYLE=&quot;margin-bottom: 0.35cm&quot;><BR><BR>\n</P>\n</DIV>', 'INFO_PRIVACY');

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `cp_booked_by_users`
--
CREATE TABLE IF NOT EXISTS `cp_booked_by_users` (
`id_booking` int(11)
,`id_partecipant` int(11)
,`confirmed` int(1)
,`idevent` int(11)
,`title` varchar(100)
,`date` date
,`city` varchar(45)
,`category_name` varchar(107)
);
-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `cp_count_booked_events`
--
CREATE TABLE IF NOT EXISTS `cp_count_booked_events` (
`iduser` int(11)
,`partecipates_events` bigint(21)
);
-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `cp_count_created_events`
--
CREATE TABLE IF NOT EXISTS `cp_count_created_events` (
`iduser` int(11)
,`created_events` bigint(21)
);
-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `cp_events_list`
--
CREATE TABLE IF NOT EXISTS `cp_events_list` (
`idevent` int(11)
,`iduser` int(11)
,`idcategory` int(11)
,`title` varchar(100)
,`description` varchar(1000)
,`img` varchar(128)
,`city` varchar(45)
,`address` varchar(45)
,`date` date
,`hour` time
,`sex` int(1)
,`agemin` int(2)
,`agemax` int(2)
,`sittotal` int(3)
,`sitcurrent` int(3)
,`latitude` varchar(10)
,`longitude` varchar(10)
,`scope` int(11)
,`creationdate` datetime
,`updatetime` timestamp
,`activated` int(1)
,`name` varchar(45)
,`surname` varchar(45)
,`c_name` varchar(107)
);
-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `cp_partecipants_list`
--
CREATE TABLE IF NOT EXISTS `cp_partecipants_list` (
`id` int(11)
,`iduser` int(11)
,`idevent` int(11)
,`confirmed` int(1)
,`email` varchar(256)
,`name` varchar(45)
,`surname` varchar(45)
);
-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `cp_users_lists`
--
CREATE TABLE IF NOT EXISTS `cp_users_lists` (
`iduser` int(11)
,`email` varchar(256)
,`idgoogle` varchar(256)
,`idfacebook` varchar(256)
,`name` varchar(45)
,`surname` varchar(45)
,`img` varchar(128)
,`birthdate` date
,`sex` varchar(45)
,`job` int(3)
,`city` varchar(45)
,`interests` varchar(255)
,`creationdate` datetime
,`updatetime` timestamp
,`password` varchar(45)
,`activated` int(1)
,`created_events` bigint(20)
,`partecipates_events` bigint(20)
);
-- --------------------------------------------------------

--
-- Struttura della tabella `curl_requests`
--

CREATE TABLE IF NOT EXISTS `curl_requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'pk',
  `creation_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Momento di invio della richiesta',
  `request` text NOT NULL COMMENT 'Richiesta inviata tramite curl',
  `response` text NOT NULL COMMENT 'Risposta del modulo curl',
  `info` text NOT NULL COMMENT 'Informazioni associate alla richiesta',
  `error` text NOT NULL COMMENT 'Errori generati dalla richiesta',
  `os` int(1) NOT NULL COMMENT 'sistema operativo: 1:ios, 3:android',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=39 ;

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `date_listed_created_events`
--
CREATE TABLE IF NOT EXISTS `date_listed_created_events` (
`date` date
,`iduser` int(11)
,`occurance` bigint(21)
);
-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `date_listed_events`
--
CREATE TABLE IF NOT EXISTS `date_listed_events` (
`date` date
,`occurance` bigint(21)
);
-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `date_listed_friends_created_events`
--
CREATE TABLE IF NOT EXISTS `date_listed_friends_created_events` (
`date` date
,`idselecteduser` int(11)
,`occurance` bigint(21)
);
-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `date_listed_partecipated_events`
--
CREATE TABLE IF NOT EXISTS `date_listed_partecipated_events` (
`date` date
,`iduserpartecipates` int(11)
,`occurance` bigint(21)
);
-- --------------------------------------------------------

--
-- Struttura della tabella `events`
--

CREATE TABLE IF NOT EXISTS `events` (
  `idevent` int(11) NOT NULL AUTO_INCREMENT,
  `iduser` int(11) NOT NULL,
  `idcategory` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` varchar(1000) DEFAULT NULL,
  `img` varchar(128) DEFAULT NULL,
  `city` varchar(45) NOT NULL,
  `address` varchar(45) NOT NULL,
  `date` date NOT NULL,
  `hour` time DEFAULT '00:00:00',
  `sex` int(1) NOT NULL COMMENT '1: Male\n2: Female\n3: Gay\n\n\n',
  `agemin` int(2) NOT NULL DEFAULT '0' COMMENT 'età minima per un evento',
  `agemax` int(2) NOT NULL DEFAULT '99' COMMENT 'età masssima dei partecipanti all''evento',
  `sittotal` int(3) NOT NULL,
  `sitcurrent` int(3) NOT NULL DEFAULT '0',
  `latitude` varchar(10) NOT NULL,
  `longitude` varchar(10) NOT NULL,
  `scope` int(11) NOT NULL COMMENT '0: pubblico, 1: privato, 2:friends only',
  `creationdate` datetime DEFAULT NULL,
  `updatetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `activated` int(1) DEFAULT NULL COMMENT '0: non attivati, 1: attivato',
  PRIMARY KEY (`idevent`),
  KEY `idUser_idx` (`iduser`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=391 ;

--
-- Dump dei dati per la tabella `events`
--

INSERT INTO `events` (`idevent`, `iduser`, `idcategory`, `title`, `description`, `img`, `city`, `address`, `date`, `hour`, `sex`, `agemin`, `agemax`, `sittotal`, `sitcurrent`, `latitude`, `longitude`, `scope`, `creationdate`, `updatetime`, `activated`) VALUES
(1, 71, 2, 'FRIZZY PAZZY PARTY', 'Frizzy pazzy party Frizzy pazzy party Frizzy pazzy party Frizzy pazzy party Frizzy pazzy party Frizzy pazzy party Frizzy pazzy party Frizzy pazzy party Frizzy pazzy party Frizzy pazzy party Frizzy pazzy party Frizzy pazzy party Frizzy pazzy party Frizzy pazzy party Frizzy pazzy party Frizzy pazzy party Frizzy pazzy party Frizzy pazzy party Frizzy pazzy party Frizzy pazzy party Frizzy pazzy party', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150328095855_6246.jpeg', 'Bari', '', '2015-03-29', '09:58:00', 6, 4, 10, 10, 1, '41.1138613', '16.8737383', 0, '2015-03-28 09:58:55', '2015-06-24 10:00:32', 0),
(2, 71, 5, 'ANCORA AL LAGO', 'Ancora al lago\n\nAncora al lago\nAncora al lago\n\nAncora al lago\n\nAncora al lago\n\nAncora al lago\nAncora al lago\n\nAncora al lago\n\n\nAncora al lago\n\nAncora al lago\nAncora al lago\n\nAncora al lago\n\n\n\nAncora al lago\n\nAncora al lago\nAncora al lago\n\nAncora al lago\n', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150328100115_1360.jpeg', 'Triggiano', '', '2015-03-29', '14:00:00', 6, 3, 9, 10, 1, '41.0621627', '16.926017', 0, '2015-03-28 10:01:15', '2015-06-24 10:00:32', 0),
(3, 71, 6, 'SCONTRO TRA NINJA', 'sempre loro ', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150328100319_6115.jpeg', 'Milano', '', '2015-03-31', '14:00:00', 0, 5, 9, 8, 5, '45.4654219', '9.1859243', 0, '2015-03-28 10:02:40', '2015-06-24 10:00:32', 0),
(4, 71, 3, 'IL CLUB DEL LIBRO', 'Solo scrittori sadici', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150328100655_5912.jpeg', 'Roma', '', '2015-03-30', '10:06:00', 6, 5, 9, 10, 0, '41.9027835', '12.4963655', 0, '2015-03-28 10:06:55', '2015-06-24 10:00:32', 0),
(5, 71, 4, 'MUTANTI PARTY', 'Mutanti Party Mutanti Party Mutanti Party Mutanti Party Mutanti Party Mutanti Party Mutanti Party Mutanti Party Mutanti Party Mutanti Party Mutanti Party Mutanti Party Mutanti Party Mutanti Party Mutanti Party Mutanti Party Mutanti Party Mutanti Party Mutanti Party Mutanti Party Mutanti Party Mutanti Party Mutanti Party Mutanti Party Mutanti Party Mutanti Party Mutanti Party Mutanti Party Mutanti Party Mutanti Party\nMutanti Party Mutanti Party Mutanti Party Mutanti Party Mutanti Party Mutanti Party\n\nMutanti Party Mutanti Party Mutanti Party Mutanti Party Mutanti Party Mutanti Party\n', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150328100836_7436.jpeg', 'Bari', '', '2015-04-01', '10:06:00', 6, 0, 9, 5, 3, '41.1263183', '16.8712019', 0, '2015-03-28 10:08:36', '2015-06-24 10:00:32', 0),
(6, 79, 7, 'TUTTO NIENTE ', 'H', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'roma', '', '2015-04-03', '16:22:00', 6, 0, 7, 12, 1, '41.9027835', '12.4963655', 0, '2015-03-30 16:23:03', '2015-06-24 10:00:32', 0),
(7, 81, 6, 'TANTO TANO', 'Haha I am a beautiful person who is the best way to get the best way to get the best way to get the best way to ', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'otranto', '', '2015-04-25', '10:14:00', 6, 0, 8, 12, 0, '40.1438978', '18.4911678', 0, '2015-03-31 10:14:31', '2015-06-24 10:00:32', 0),
(8, 83, 7, 'FIORI', 'Hah the best way to get the best way to get ', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'lecce', '', '2015-04-03', '11:00:00', 6, 0, 8, 12, 0, '40.3515155', '18.1750161', 0, '2015-03-31 11:00:23', '2015-06-24 10:00:32', 0),
(9, 12, 2, 'CENA PESCE ', 'addrfggff', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150331121647_6266.jpeg', 'conegliano', '', '2015-04-01', '12:14:00', 6, 5, 14, 58, 0, '45.888304', '12.302763', 0, '2015-03-31 12:14:51', '2015-06-24 10:00:32', 0),
(10, 90, 6, 'PROVA', 'Hahhs', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'roma', '', '2015-04-17', '12:22:00', 3, 0, 7, 128, 1, '41.9027835', '12.4963655', 0, '2015-03-31 12:23:12', '2015-06-24 10:00:32', 0),
(11, 12, 1, 'JOGGING', 'ggfghh', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150331122750_8667.jpeg', 'Padova', '', '2015-04-02', '12:27:00', 3, 16, 37, 10, 2, '45.404516', '11.872893', 0, '2015-03-31 12:27:50', '2015-06-24 10:00:32', 0),
(12, 12, 1, 'UN SALTO TRA LE NUVOLE', 'gggbhvgffddfccvvv-hhhh', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150331123114_8013.jpeg', 'Bolzano', '', '2015-04-02', '12:27:00', 0, 32, 87, 10, 1, '46.50312', '11.358966', 0, '2015-03-31 12:30:44', '2015-06-24 10:00:32', 0),
(13, 12, 0, 'GRIGLIATA', 'ggcghvvb', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150331123408_8056.jpeg', 'mestre', '', '2015-04-01', '12:33:00', 6, 24, 31, 30, 1, '45.49064', '12.243864', 0, '2015-03-31 12:34:08', '2015-06-24 10:00:32', 0),
(14, 91, 2, 'CINEMA', 'hdhdBhhdh\ndhhd', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150331130908_9372.jpeg', 'milano', '', '2015-04-02', '13:08:00', 3, 20, 30, 5, 1, '45.4520578', '9.19526763', 0, '2015-03-31 13:09:08', '2015-06-24 10:00:32', 1),
(15, 91, 3, 'FUN', 'ftcgccyvgcg\ntvycc', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150331131202_6151.jpeg', 'milano', '', '2015-04-09', '13:08:00', 6, 27, 51, 89, 2, '45.4496260', '9.20657616', 0, '2015-03-31 13:12:02', '2015-07-27 16:58:48', 0),
(16, 91, 2, 'CINEMA', 'vhvhvhv\ng chvhv\n', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150331132258_2946.jpeg', 'roma', '', '2015-04-08', '13:22:00', 3, 20, 40, 5, 0, '41.9027835', '12.4963655', 0, '2015-03-31 13:22:58', '2015-07-27 16:58:48', 0),
(17, 93, 6, 'PROVA DEFINITIVA', 'Ggg', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'roma', '', '2015-04-17', '13:43:00', 3, 0, 7, 125, 2, '41.9027835', '12.4963655', 0, '2015-03-31 13:43:27', '2015-06-24 10:00:32', 0),
(18, 12, 1, 'PESCA SUB ', 'rrfggggg', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150331135523_1522.jpeg', 'napoli', '', '2015-04-01', '13:54:00', 0, 0, 7, 5, 2, '40.844499', '14.268529', 0, '2015-03-31 13:55:23', '2015-06-24 10:00:32', 0),
(19, 91, 1, 'TREK', 'dhhhdrd\nhdhhdhd\nndjhhbrrhfbbeh', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150331145944_8164.jpeg', 'torino', '', '2015-04-11', '17:22:00', 0, 60, 100, 5, 0, '45.0719403', '7.68961127', 0, '2015-03-31 14:59:44', '2015-07-27 16:58:48', 0),
(20, 91, 3, 'LET&#039;S PARTY', 'hxbsdvb', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150331150707_5358.jpeg', 'milano', '', '2015-04-11', '17:22:00', 3, 20, 58, 15, 2, '45.4610830', '9.18471347', 0, '2015-03-31 15:07:07', '2015-07-27 16:58:48', 0),
(21, 91, 1, 'TENNIS', 'hdhdhhdf\ngdBgd\nGEhh', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150331154258_4009.jpeg', 'treviso', '', '2015-04-08', '18:42:00', 6, 20, 38, 4, 0, '45.6668893', '12.2430437', 2, '2015-03-31 15:42:58', '2015-07-27 16:58:48', 0),
(22, 94, 7, 'PROVA PEOVA', 'jsjss', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'otranto', '', '2015-05-01', '16:08:00', 6, 0, 11, 56, 1, '40.1438978', '18.4911678', 0, '2015-03-31 16:14:23', '2015-06-24 10:00:32', 0),
(23, 92, 7, 'TEST DEL@DEMONIO', 'Hhb', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150409162135_2199.jpeg', 'roma', '', '2015-04-18', '16:41:00', 6, 0, 8, 268, 0, '41.9027835', '12.4963655', 0, '2015-03-31 16:41:56', '2015-06-24 10:00:32', 0),
(24, 91, 3, 'BEER', 'hdhbddvsd\nhdhdd\nbehdf\nGEhhdd\ngr', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150331173401_2215.jpeg', 'milano', '', '2015-04-11', '18:42:00', 6, 30, 39, 5, 1, '45.4610378', '9.18475639', 0, '2015-03-31 17:34:01', '2015-07-27 16:58:48', 0),
(25, 91, 0, 'PIC NIC', 'jdhgashhdd\ngufiDVD bohgiggfini', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150401081404_6971.jpeg', 'milano', '', '2015-04-15', '22:42:00', 3, 20, 40, 6, 0, '45.4934753', '9.22598395', 0, '2015-04-01 08:14:04', '2015-06-24 10:00:32', 1),
(26, 91, 1, 'KART', 'tgthg\nghgh', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150401081620_9598.jpeg', 'milano', '', '2015-04-14', '02:42:00', 4, 20, 40, 10, 0, '45.4709269', '9.25507888', 0, '2015-04-01 08:16:20', '2015-06-24 10:00:32', 1),
(27, 91, 2, 'ACQUA', 'ggdf\nfgyh\nhhhhhhhhhelsingborg', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150401081942_67.jpeg', 'genova', '', '2015-04-29', '07:42:00', 3, 19, 20, 3, 0, '45.4740892', '9.21282134', 0, '2015-04-01 08:19:42', '2015-06-24 10:00:32', 1),
(28, 91, 3, 'STREET PARTY', 'ggffgg', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150401082129_6422.jpeg', 'milano', '', '2015-04-30', '13:42:00', 2, 30, 36, 50, 0, '45.4819148', '9.22101583', 0, '2015-04-01 08:21:29', '2015-06-24 10:00:32', 1),
(29, 91, 2, 'PUB', 'ghhvyg\ngghhhhhhhhjjjjjjjjjjj', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150401082309_9592.jpeg', 'milano', '', '2015-04-27', '20:38:00', 6, 25, 36, 20, 0, '45.4724370', '9.21957481', 0, '2015-04-01 08:23:09', '2015-06-24 10:00:32', 1),
(30, 91, 2, 'LUNA PARK', 'hdjdidd\nbdhhd\nbehhhd\nhdBhadbdBd\nhdBd\nhdhdhdhhhdhbbd', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150401082627_8035.jpeg', 'milano', 'via casoretto', '2015-04-30', '15:38:00', 3, 30, 40, 10, 0, '45.4892658', '9.23161692', 0, '2015-04-01 08:26:27', '2015-06-24 10:00:32', 1),
(31, 91, 2, 'GTFSF', 'ghgghg', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'milano', '', '2015-05-07', '15:37:00', 6, 10, 20, 5, 0, '45.4654219', '9.1859243', 0, '2015-04-02 09:38:24', '2015-07-27 16:58:48', 0),
(32, 106, 7, 'VHHHHIV', 'vv', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'foggia', '', '2015-04-30', '11:00:00', 6, 0, 8, 23, 0, '41.4621984', '15.5446302', 0, '2015-04-02 11:00:28', '2015-06-24 10:00:32', 0),
(33, 71, 5, 'EVENTO LUNGO', 'evendo con descrizione molto lunga evendo con descrizione molto lunga evendo con descrizione molto lunga evendo con descrizione molto lunga evendo con descrizione molto lunga evendo con descrizione molto lunga evendo con descrizione molto lunga evendo con descrizione molto lunga evendo con descrizione molto lunga eve evendo con descrizione molto lunga evendo con descrizione molto lunga evendo con descrizione molto lunga evendo con descrizione molto lunga evendo con descrizione molto lunga evendo con descrizione molto lunga ndo con descrizione molto lunga evendo con descrizione molto lunga evendo con descrizione molto lunga', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'bari', '', '2015-04-16', '12:23:00', 3, 5, 9, 3, 3, '41.1171432', '16.8718715', 0, '2015-04-02 12:23:21', '2015-06-24 10:00:32', 0),
(34, 12, 2, 'POKER', 'fgyhh', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150403120418_6097.jpeg', 'Conegliano', '', '2015-04-08', '13:02:00', 0, 35, 45, 9, 1, '45.8843662', '12.3032243', 0, '2015-04-03 12:04:18', '2015-06-24 10:00:32', 0),
(35, 91, 2, 'STREET FIGHT', 'jrhhdhhd', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150403120611_9224.jpeg', 'milano', '', '2015-04-15', '15:05:00', 6, 20, 51, 23, 0, '45.4611580', '9.18488480', 0, '2015-04-03 12:06:11', '2015-06-24 10:00:32', 1),
(36, 91, 2, 'BEACH RAVE', 'hdhshhde\nyehehr\nhdhd', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150403120750_5806.jpeg', 'milano', '', '2015-04-29', '19:05:00', 3, 20, 50, 10, 0, '45.4654219', '9.1859243', 0, '2015-04-03 12:07:50', '2015-06-24 10:00:32', 1),
(37, 91, 3, 'GF', 'hdjhhdvhdgd\njdhd', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150403193349_328.jpeg', 'roma', '', '2015-05-06', '14:05:00', 0, 30, 34, 2, 0, '41.9018817', '12.4988619', 0, '2015-04-03 19:33:49', '2015-07-27 16:58:48', 0),
(38, 91, 3, 'POIDHD', 'hdhdhd\nhdhdb\nhdhd', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150403193510_9186.jpeg', 'milano', 'via casoretto', '2015-05-07', '09:05:00', 0, 30, 54, 2, 1, '45.4889621', '9.2288858', 0, '2015-04-03 19:35:10', '2015-07-27 16:58:48', 0),
(39, 91, 2, 'RACE ', 'hhvedd\nhdbhd\nbdhd', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150403193611_435.jpeg', 'milano', '', '2015-05-09', '00:05:00', 6, 25, 60, 10, 0, '45.4621356', '9.18413478', 0, '2015-04-03 19:36:11', '2015-07-27 16:58:48', 0),
(40, 91, 1, 'RUNNING', 'hdB gasd\nhsubhdd\njdhbd', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150403193739_7467.jpeg', 'milano', 'via palmanova', '2015-05-03', '10:05:00', 1, 25, 30, 5, 0, '45.5043774', '9.2468852', 0, '2015-04-03 19:37:39', '2015-07-27 16:58:48', 0),
(41, 91, 1, 'BIKING', 'gdhhdr\nGEbd\nbebd\nbebr\nbdbd\nbdvdhd\n\n\n\nhdhd\nhehr\nhdBd\n\n\nbehbdr\n', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150403193907_2433.jpeg', 'milano', '', '2015-05-05', '14:05:00', 3, 40, 50, 10, 0, '45.4654219', '9.1859243', 0, '2015-04-03 19:39:07', '2015-07-27 16:58:48', 0),
(42, 91, 3, 'DINNER', 'ghbss\nbdbd\ndf\n\nd\nd.d..d.f..\nr\n\n\nf\n\n\nr\n\nf\nr\n', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150403194040_1988.jpeg', 'milano', 'via roma', '2015-05-04', '19:05:00', 0, 40, 50, 1, 0, '45.4005752', '9.2780284', 0, '2015-04-03 19:40:40', '2015-07-27 16:58:48', 0),
(43, 91, 1, 'SPORT HDHHDJJJJD HEHHHE HHEGR', 'hdbd\nfand\ndhd\nbd\ndbf\nbedb', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150403194151_7413.jpeg', 'milano', 'via senato', '2015-05-06', '21:05:00', 6, 20, 30, 10, 0, '45.4705759', '9.1972035', 0, '2015-04-03 19:41:51', '2015-07-27 16:58:48', 0),
(44, 91, 7, 'DDDAAA', 'hdghed\nhdbd\nhrnf\nhr', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150403194309_3835.jpeg', 'milano', 'via repubblica', '2015-05-09', '19:05:00', 3, 30, 35, 14, 0, '45.5315549', '9.1373578', 2, '2015-04-03 19:43:09', '2015-07-27 16:58:48', 0),
(45, 91, 2, 'CINEMA', 'gdbned\nnedf\n\n\nf\n\nf\nf\nf\nf\n\nf\nerg\nfhdjjrjjejjdijed\nd\n\ne\nr', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150403194608_3344.jpeg', 'milano', '', '2015-05-05', '19:05:00', 6, 35, 49, 10, 0, '45.4654219', '9.1859243', 0, '2015-04-03 19:46:08', '2015-07-27 16:58:48', 0),
(46, 91, 1, 'SPORT', 'fhhde\nfjfinjd\nndnf', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150403194724_7722.jpeg', 'milano', 'via porpora', '2015-05-04', '19:05:00', 6, 30, 40, 20, 0, '45.486071', '9.2275896', 0, '2015-04-03 19:47:24', '2015-07-27 16:58:48', 0),
(47, 91, 1, 'MARATONA', 'gshdd\ndhdhd', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150403194821_5849.jpeg', 'milano', '', '2015-05-07', '23:05:00', 6, 13, 99, 999, 0, '45.4654219', '9.1859243', 0, '2015-04-03 19:48:21', '2015-07-27 16:58:48', 0),
(48, 107, 6, 'TEST PER MICHELE', 'test', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150409162958_3922.jpeg', 'g gvycyccrcy', 'gfycgvyctvu', '2015-04-22', '16:34:00', 5, 6, 9, 25, 1, '41.1216556', '16.8601509', 0, '2015-04-04 16:34:35', '2015-06-24 10:00:32', 0),
(49, 12, 1, 'SALVA UN AGNELLO', 'ciao se anche tu vuoi sostenere la nostra causa...vieni in piazza Garibaldi e fai sentire la tua voce...', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'milano', 'Piazza Garibaldi', '2015-04-06', '09:00:00', 6, 0, 100, 999, 0, '45.4635118', '9.087913', 0, '2015-04-04 19:02:20', '2015-06-24 10:00:32', 0),
(50, 106, 7, 'SDF', 'hh', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'roma', '', '2015-04-30', '10:12:00', 5, 0, 7, 5, 0, '41.9027835', '12.4963655', 0, '2015-04-07 10:13:06', '2015-06-24 10:00:32', 0),
(51, 106, 6, 'HHH', 'ghfdr', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'torino', '', '2015-04-30', '10:16:00', 3, 0, 7, 56, 0, '45.070312', '7.6868565', 0, '2015-04-07 10:16:13', '2015-06-24 10:00:32', 0),
(52, 106, 6, 'UHGG', 'ggff', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'firenze', '', '2015-05-01', '10:16:00', 3, 0, 8, 63, 0, '43.7695604', '11.2558136', 0, '2015-04-07 10:17:05', '2015-06-24 10:00:32', 0),
(53, 106, 7, 'HHHJJJ222222', 'gggf', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'roma', '', '2015-04-24', '10:17:00', 6, 0, 7, 65, 0, '41.9027835', '12.4963655', 0, '2015-04-07 10:17:34', '2015-06-24 10:00:32', 0),
(54, 106, 7, 'JJGFF', 'hhy', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'rimini', '', '2015-05-01', '10:17:00', 3, 0, 8, 35, 0, '44.0678288', '12.5695158', 0, '2015-04-07 10:18:07', '2015-06-24 10:00:32', 0),
(55, 106, 5, 'GGH22222', 'Justin', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'firenze', '', '2015-04-24', '15:08:00', 3, 0, 7, 233, 0, '43.7695604', '11.2558136', 0, '2015-04-07 10:21:41', '2015-06-24 10:00:32', 0),
(56, 106, 7, 'GGJI', 'hgg', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'domodossola', '', '2015-04-30', '10:25:00', 3, 0, 8, 63, 0, '46.113222', '8.2919754', 0, '2015-04-07 10:25:28', '2015-06-24 10:00:32', 0),
(57, 106, 7, 'PRPGAHHHHH', 'ggsbs', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'ruvo', '', '2015-05-01', '13:18:00', 3, 8, 24, 65, 0, '41.1172905', '16.4837252', 0, '2015-04-07 11:14:41', '2015-06-24 10:00:32', 0),
(58, 109, 6, 'HHHHH', 'ghhgg', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'roma', '', '2015-04-24', '16:50:00', 3, 0, 6, 63, 1, '41.9027835', '12.4963655', 0, '2015-04-07 16:50:28', '2015-06-24 10:00:32', 0),
(59, 109, 5, 'YYTRR', 'gggcdr', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'asti', '', '2015-04-24', '16:50:00', 6, 0, 8, 25, 1, '18.8043913', '75.1680394', 0, '2015-04-07 16:51:10', '2015-06-24 10:00:32', 0),
(60, 109, 7, 'JJHCC', 'hhggvv', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'foggia', '', '2015-05-01', '16:51:00', 3, 0, 6, 65, 1, '41.4621984', '15.5446302', 0, '2015-04-07 16:51:42', '2015-06-24 10:00:32', 0),
(61, 109, 7, 'HYFFF', 'gv', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'trento', '', '2015-04-30', '16:52:00', 6, 0, 7, 56, 1, '46.0700915', '11.1197626', 0, '2015-04-07 16:52:13', '2015-06-24 10:00:32', 0),
(62, 109, 7, 'HHFF', 'ffrgv', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'roma', '', '2015-04-23', '16:52:00', 3, 0, 7, 52, 1, '41.9027835', '12.4963655', 0, '2015-04-07 16:52:58', '2015-06-24 10:00:32', 0),
(63, 109, 6, 'KKHGC', 'hgcf', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'termoli', '', '2015-04-23', '16:53:00', 3, 0, 8, 5, 1, '42.0005331', '14.9952839', 0, '2015-04-07 16:53:27', '2015-06-24 10:00:32', 0),
(64, 101, 2, 'UN BICCHIERE ', 'Hshshd', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150409093238_4952.jpeg', 'Bari', '', '2015-04-10', '11:03:00', 6, 1, 3, 10, 1, '41.1171432', '16.8718715', 0, '2015-04-08 11:04:18', '2015-06-24 10:00:32', 0),
(65, 101, 8, 'TEST IOS 2 IGNORARE', 'Ignorare', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150408112626_9990.jpeg', 'triggiano', 'via Ferrari 10', '2015-04-09', '11:24:00', 3, 0, 2, 5, 0, '41.0679444', '16.9362911', 0, '2015-04-08 11:26:26', '2015-06-24 10:00:32', 0),
(66, 101, 6, 'TEST IOS 3 IGNORARE MOD 2', 'Hdhd', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'Bari', '', '2015-04-12', '11:44:00', 1, 0, 4, 5, 1, '41.1171432', '16.8718715', 0, '2015-04-08 11:45:57', '2015-06-24 10:00:32', 0),
(69, 101, 5, 'TEST IOS5 IGNORARE', 'Desfcth hhhb hhbbzhxb hvv', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150408170807_7664.jpeg', 'Valenzano', '', '2015-04-10', '17:06:00', 3, 2, 3, 523, 2, '41.0442712', '16.8791006', 0, '2015-04-08 17:08:07', '2015-06-24 10:00:32', 0),
(70, 111, 3, 'TFGG', 'ghhh', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'cerignola', 'via urbe', '2015-04-29', '17:17:00', 6, 0, 7, 63, 0, '41.260335', '15.901997', 0, '2015-04-08 17:18:00', '2015-06-24 10:00:32', 0),
(71, 101, 3, 'TEST TEST E ANCORA TEST', 'Test', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'Bari', '', '2015-04-09', '17:18:00', 1, 0, 28, 52, 0, '41.1171432', '16.8718715', 0, '2015-04-08 17:19:00', '2015-06-24 10:00:32', 0),
(73, 111, 6, 'YYYYY', 'hhhhh', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'cerignola', 'via stella', '2015-04-29', '17:35:00', 6, 0, 6, 63, 0, '41.2629597', '15.882379', 0, '2015-04-08 17:35:33', '2015-06-24 10:00:32', 0),
(74, 101, 7, 'TEST MOD IOS', 'Hdhd', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'triggiano', '', '2015-04-10', '10:18:00', 1, 0, 2, 42, 1, '41.0621627', '16.926017', 0, '2015-04-09 10:18:49', '2015-06-24 10:00:32', 0),
(75, 101, 8, 'ANCORA IOS', 'Hdhd', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'Bari', '', '2015-04-10', '10:20:00', 1, 0, 14, 4, 0, '41.1171432', '16.8718715', 0, '2015-04-09 10:20:39', '2015-06-24 10:00:32', 0),
(76, 122, 7, 'BBHGF', 'dddf', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150409130251_9568.jpeg', 'foggia', '', '2015-04-23', '13:01:00', 6, 0, 8, 53, 0, '41.4621984', '15.5446302', 0, '2015-04-09 13:02:15', '2015-06-24 10:00:32', 0),
(77, 128, 6, 'RUOTATO', 'vgg', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150409145812_6833.jpeg', 'livorno', '', '2015-04-23', '14:57:00', 3, 0, 8, 53, 0, '43.548473', '10.3105674', 0, '2015-04-09 14:58:12', '2015-06-24 10:00:32', 0),
(78, 128, 7, 'GGGG', 'uuuu', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150409145930_8934.jpeg', 'livorno', '', '2015-04-30', '14:59:00', 6, 0, 8, 52, 0, '43.548473', '10.3105674', 0, '2015-04-09 14:59:30', '2015-06-24 10:00:32', 0),
(79, 128, 6, 'VV', 'ggg', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150409160701_90.png', 'rimini', '', '2015-04-22', '15:02:00', 6, 0, 8, 22, 0, '44.0678288', '12.5695158', 0, '2015-04-09 15:02:58', '2015-06-24 10:00:32', 0),
(80, 128, 6, 'FFFFFG', 'tfff', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150409151650_4695.jpeg', 'firenze', '', '2015-04-30', '15:15:00', 3, 0, 9, 56, 0, '43.7695604', '11.2558136', 0, '2015-04-09 15:16:50', '2015-06-24 10:00:32', 0),
(81, 128, 7, 'BB', 'hhgg', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150409151825_833.jpeg', 'trinitapoli', '', '2015-04-23', '15:18:00', 6, 0, 8, 65, 0, '41.3592428', '16.0788413', 0, '2015-04-09 15:18:25', '2015-06-24 10:00:32', 0),
(82, 128, 5, 'VHHHHH', 'hhhh', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150409152348_2759.jpeg', 'roma', '', '2015-05-01', '15:23:00', 3, 1, 9, 32, 0, '41.9027835', '12.4963655', 0, '2015-04-09 15:23:48', '2015-06-24 10:00:32', 0),
(83, 128, 8, 'GGG', 'hh', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150409153458_4784.png', 'parigi', '', '2015-04-24', '15:34:00', 3, 0, 7, 53, 0, '48.856614', '2.3522219', 0, '2015-04-09 15:34:58', '2015-06-24 10:00:32', 0),
(84, 129, 6, 'VVV', 'Gggg', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150409161819_4039.jpeg', 'roma', '', '2015-05-02', '16:14:00', 6, 0, 8, 566, 0, '41.9027835', '12.4963655', 0, '2015-04-09 16:14:56', '2015-06-24 10:00:32', 0),
(85, 130, 6, 'METAL SLUG', 'Arcade', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150409171023_4835.jpeg', 'tokyo', '', '2015-05-01', '16:30:00', 3, 0, 8, 68, 0, '35.6894875', '139.691706', 0, '2015-04-09 16:31:32', '2015-06-24 10:00:32', 0),
(86, 131, 6, 'SUPER MARIO', 'Jhhhh', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150409173443_8225.jpeg', 'rimini', '', '2015-05-02', '17:31:00', 3, 0, 8, 23, 0, '44.0678288', '12.5695158', 0, '2015-04-09 17:32:13', '2015-06-24 10:00:32', 0),
(87, 131, 6, 'GGGG', 'Gggg', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150409173538_1806.jpeg', 'cerignola', '', '2015-05-01', '17:35:00', 3, 3, 7, 52, 0, '41.2656592', '15.8939163', 0, '2015-04-09 17:35:38', '2015-06-24 10:00:32', 0),
(88, 131, 6, 'NJJJ', 'Ggvcx', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150410143013_2475.jpeg', 'roma', '', '2015-04-23', '17:42:00', 3, 0, 8, 65, 0, '41.9027835', '12.4963655', 0, '2015-04-09 17:42:42', '2015-06-24 10:00:32', 0),
(89, 131, 6, 'HYY', 'Hhg', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150409174348_4966.jpeg', 'troia', '', '2015-05-08', '17:43:00', 3, 0, 7, 55, 1, '41.3597802', '15.3081141', 0, '2015-04-09 17:43:48', '2015-06-24 10:00:32', 0),
(90, 131, 6, 'BBBB', 'Gggy', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150409174827_533.jpeg', 'firenze', '', '2015-05-02', '17:47:00', 3, 0, 8, 321, 0, '43.7695604', '11.2558136', 0, '2015-04-09 17:47:49', '2015-06-24 10:00:32', 0),
(91, 131, 6, 'HHH', 'Ghhh', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150409175235_9040.jpeg', 'cerignola', '', '2015-04-30', '17:52:00', 3, 0, 7, 21, 0, '41.2656592', '15.8939163', 0, '2015-04-09 17:52:35', '2015-06-24 10:00:32', 0),
(92, 131, 6, 'YYYY', 'Yyyy', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150409175751_5304.jpeg', 'toronto', '', '2015-04-24', '17:57:00', 3, 0, 6, 52, 0, '43.653226', '-79.383184', 0, '2015-04-09 17:57:51', '2015-06-24 10:00:32', 0),
(93, 131, 6, 'GGGG', 'Fff', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150410095421_2488.jpeg', 'roma', '', '2015-04-23', '09:51:00', 3, 0, 7, 23, 1, '41.9027835', '12.4963655', 0, '2015-04-10 09:53:04', '2015-06-24 10:00:32', 0),
(94, 131, 6, 'BHHGG', 'Ffff', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150410095535_4102.jpeg', 'tokyo', '', '2015-04-23', '09:55:00', 3, 0, 7, 12, 0, '35.6894875', '139.691706', 0, '2015-04-10 09:55:35', '2015-06-24 10:00:32', 0),
(95, 135, 6, 'TORNEO DI VIDEOGIOCHI', 'Per tutti i gusti. ', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150410154810_3795.jpeg', 'trani', '', '2015-04-24', '15:42:00', 3, 0, 100, 100, 1, '41.2774857', '16.4178334', 0, '2015-04-10 15:43:20', '2015-06-24 10:00:32', 0),
(97, 135, 6, 'SLEED ', 'Jshhs', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150410155257_9727.jpeg', 'barletta', '', '2015-04-17', '15:52:00', 3, 0, 8, 54, 0, '41.3196635', '16.2838207', 0, '2015-04-10 15:52:58', '2015-06-24 10:00:32', 0),
(98, 135, 2, 'GGGG', 'Yyy', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150410155507_292.jpeg', 'bisceglie', '', '2015-04-24', '15:54:00', 3, 0, 7, 12, 0, '41.242726', '16.502065', 0, '2015-04-10 15:55:07', '2015-06-24 10:00:32', 0),
(99, 135, 6, 'GG', 'Gyy', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150410155816_1147.jpeg', 'roma', '', '2015-05-01', '15:58:00', 6, 0, 8, 12, 0, '41.9027835', '12.4963655', 0, '2015-04-10 15:58:16', '2015-06-24 10:00:32', 0),
(100, 12, 2, 'SU', 'fdrgf', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150410232041_1200.jpeg', 'coneglino', 'via vecellio', '2015-04-24', '09:00:00', 1, 25, 35, 40, 0, '45.8884665', '12.2990411', 0, '2015-04-10 23:20:41', '2015-06-24 10:00:32', 0),
(101, 101, 6, 'TEST MICHELE', 'Hshshd', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150411163901_7063.jpeg', 'Bari', '', '2015-04-15', '16:38:00', 1, 0, 26, 52, 1, '41.1171432', '16.8718715', 0, '2015-04-11 16:39:01', '2015-06-24 10:00:32', 0),
(102, 101, 7, 'SOLO PER MICHELE', 'Ggg', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'Bari', '', '2015-04-12', '16:45:00', 6, 0, 27, 5, 1, '41.1171432', '16.8718715', 0, '2015-04-11 16:46:08', '2015-06-24 10:00:32', 0),
(103, 101, 7, 'CJZJXJXJXB', 'Gvg', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'Bari', '', '2015-04-12', '17:06:00', 6, 0, 17, 5, 0, '41.1171432', '16.8718715', 0, '2015-04-11 17:07:37', '2015-06-24 10:00:32', 0),
(104, 101, 7, 'TEST 100', 'Gggg', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'Bari', '', '2015-04-12', '17:22:00', 1, 0, 13, 2, 1, '41.1171432', '16.8718715', 0, '2015-04-11 17:23:16', '2015-06-24 10:00:32', 0),
(105, 101, 7, 'TEST 101', 'Hshshd', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'triggiano', '', '2015-04-12', '17:26:00', 6, 0, 29, 52, 0, '41.0666171', '16.9265156', 0, '2015-04-11 17:27:36', '2015-06-24 10:00:32', 0),
(106, 140, 2, 'GLI ZOMBIE', 'Evento con zombie vicino casa', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150414112159_2799.jpeg', 'Triggiano', 'Via verazzano 6', '2015-04-15', '11:20:00', 6, 0, 24, 82, 1, '41.0674523', '16.9354962', 0, '2015-04-14 11:21:59', '2015-06-24 10:00:32', 0),
(107, 140, 1, 'TEST IPHONE 6 PLUS', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'Triggiano', '', '2015-04-21', '08:10:00', 6, 0, 12, 5, 0, '41.0613951', '16.9357069', 0, '2015-04-15 08:12:11', '2015-06-24 10:00:32', 0),
(108, 145, 6, 'HHH', 'Ggff', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'torino', '', '2015-04-15', '16:16:00', 6, 0, 8, 45, 0, '45.070312', '7.6868565', 0, '2015-04-15 12:16:14', '2015-06-24 10:00:32', 0),
(109, 71, 6, 'TEST', 'cvgg', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'bari', '', '2015-04-21', '12:45:00', 1, 7, 9, 5, 2, '41.1171432', '16.8718715', 0, '2015-04-15 12:45:49', '2015-06-24 10:00:32', 0),
(110, 164, 6, 'MICHELE ANDROID EVENT', 'ciai', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'Bari', '', '2015-04-24', '16:14:00', 6, 0, 13, 2, 0, '41.1171432', '16.8718715', 0, '2015-04-22 16:14:43', '2015-06-24 10:00:32', 0),
(111, 12, 1, 'BUNGEE JUMPING, UN SALTO NEL VUOTO!!!', 'Organizzo un salto nel vuoto da 100 metri di altezza!indossa l&#039;imbragatura carica l&#039;adrenalina e vola dal ponte di Salle!! per qualsiasi info scrivetimi!! ', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150504135447_9606.jpeg', 'bolzano', '', '2015-05-16', '09:00:00', 6, 25, 45, 10, 3, '46.4982953', '11.3547582', 0, '2015-05-04 13:54:47', '2015-06-24 10:00:32', 0),
(112, 12, 1, ' PESCATA IN COMPAGNIA!', 'Si parte presto per una giornata di pesca,se non avete l&#039;attrezzatura io ho tutto!!Si divide la spesa del carburante e ricordatevi una bottiglia di prosecco!Una pasta è offerta da me,ovviamente a base di pesce fresco!!', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150504142922_9685.jpeg', 'caorle', '', '2015-05-10', '07:00:00', 0, 45, 70, 6, 1, '45.6030238', '12.8859589', 0, '2015-05-04 14:29:22', '2015-06-24 10:00:32', 0),
(113, 12, 2, 'VESPA BIKINI ', 'Un raduno tutto in bikini !!!', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150504144506_5442.jpeg', 'lignano', '', '2015-05-07', '16:00:00', 1, 18, 50, 100, 2, '44.3610983', '10.5144956', 0, '2015-05-04 14:44:10', '2015-06-24 10:00:32', 0),
(114, 12, 1, 'BEACH VOLLEY', 'torneo notturno Beach volley ,vi aspettiamo numerosi!!', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150504145421_2111.jpeg', 'jesolo', '', '2015-05-07', '22:00:00', 6, 18, 50, 80, 1, '45.5334198', '12.6438498', 0, '2015-05-04 14:54:21', '2015-06-24 10:00:32', 0),
(115, 12, 1, 'TORNEO CALCETTO', 'torneo amichevole con premio alla squadra vincente!!', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150504155553_1744.jpeg', 'conegliano', '', '2015-05-07', '15:00:00', 4, 16, 45, 32, 3, '45.881228', '12.305734', 0, '2015-05-04 15:55:53', '2015-06-24 10:00:32', 0),
(116, 12, 1, 'DOPPIO TENNIS', 'cerchiamo 2 giocatori per un doppio .livello esperto.', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150504160237_4926.jpeg', 'bologna', '', '2015-05-07', '18:00:00', 2, 17, 50, 2, 0, '44.494887', '11.3426163', 0, '2015-05-04 16:02:37', '2015-06-24 10:00:32', 0),
(117, 12, 1, 'TORNEO DI BOWLING', 'riservato a giocatori esperti .', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150504160629_2699.jpeg', 'susegana', '', '2015-05-08', '20:00:00', 4, 30, 45, 15, 1, '45.8512777', '12.2495073', 0, '2015-05-04 16:06:29', '2015-06-24 10:00:32', 0),
(118, 12, 2, 'CONCERTO REGGE', 'Locanda al contadino,\nalle prime 20 prenotazioni ,un panino e birra gratis!!!', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150504161156_3829.jpeg', 'conegliano', 'via matteotti', '2015-05-09', '20:00:00', 6, 0, 100, 20, 0, '45.8841620', '12.3087714', 0, '2015-05-04 16:11:56', '2015-06-24 10:00:32', 0),
(119, 12, 1, 'GARA DI SCHERMA', 'iscrizioni aperte!!', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150504165854_8843.jpeg', 'conegiano', '', '2015-05-08', '16:58:00', 6, 18, 25, 30, 2, '45.889197', '12.3006368', 0, '2015-05-04 16:58:54', '2015-06-24 10:00:32', 0),
(121, 12, 0, 'VOLKSWAGEN CAMPER', 'raduno ', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150504214715_6237.jpeg', 'pieve di soligo', '', '2015-05-09', '21:46:00', 6, 0, 100, 999, 1, '45.8999793', '12.1735423', 0, '2015-05-04 21:47:15', '2015-06-24 10:00:32', 0),
(122, 12, 1, 'LANCIO CON PARACADUTE', 'https://youtu.be/zG22qQydPVQ', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150505132057_7003.jpeg', 'Trieste', '', '2015-05-11', '13:20:00', 6, 18, 60, 10, 2, '45.6432394', '13.7748304', 0, '2015-05-05 13:20:57', '2015-06-24 10:00:32', 0),
(123, 169, 7, 'GIORNATA DELLA LETTURA ', 'Giornata della lettura Giornata della letturaGiornata della letturaGiornata della letturaGiornata della letturaGiornata della letturaGiornata della letturaGiornata della letturaGiornata della lettura Giornata della letturaGiornata della letturaGiornata della letturaGiornata della letturaGiornata della letturaGiornata della letturaGiornata della lettura', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150506162228_8086.jpeg', 'Milano', '', '2015-05-08', '16:11:00', 4, 0, 13, 5, 1, '45.4737858', '9.17690448', 0, '2015-05-06 16:14:21', '2015-06-24 10:00:32', 0),
(124, 12, 2, 'CONCERTO ', '“Le rivoltelle”, la band rock al femminile\n\n\n', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150506163431_7434.jpeg', 'cascina', '', '2015-05-10', '16:33:00', 6, 0, 100, 999, 1, '43.6759139', '10.5558967', 0, '2015-05-06 16:34:31', '2015-06-24 10:00:32', 0),
(125, 177, 7, 'NUOVO UTENTE MICHELE', 'Quello vecchio è disabilitato', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150507143814_3058.jpeg', 'Bari', '', '2015-05-08', '14:36:00', 6, 0, 17, 5, 3, '41.1171432', '16.8718715', 0, '2015-05-07 14:38:14', '2015-06-24 10:00:32', 0),
(126, 168, 2, 'EXPO', 'Una bicchierata', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150507144907_3934.jpeg', 'milano', '', '2015-05-16', '16:00:00', 6, 20, 50, 25, 3, '45.4654219', '9.1859243', 0, '2015-05-07 14:42:30', '2015-06-24 10:00:32', 0),
(127, 176, 7, 'TEST', 'cgvv', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'bari', '', '2015-05-11', '15:52:00', 6, 4, 38, 5, 1, '41.1171432', '16.8718715', 0, '2015-05-07 15:53:29', '2015-06-24 10:00:32', 0),
(128, 12, 2, 'HIP HOP', 'corso di danza hip hop,aperte le iscrizioni', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150508101708_4952.jpeg', 'milano', '', '2015-05-09', '10:16:00', 6, 14, 39, 30, 2, '45.4621657', '9.18612767', 0, '2015-05-08 10:17:08', '2015-06-24 10:00:32', 0),
(129, 12, 1, 'PATTINAGGIO SUL GHIACCIO', 'Aperto a tutti\nUn corso di 10 lezioni ', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150508114431_8934.jpeg', 'san vendemiano', '', '2015-05-10', '11:44:00', 6, 7, 100, 30, 2, '45.8911895', '12.3267227', 0, '2015-05-08 11:44:31', '2015-06-24 10:00:32', 0),
(130, 91, 1, 'NEED FOR SPEED', 'ultimate race', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150511122242_9440.jpeg', 'milano', '', '2015-05-25', '20:00:00', 3, 30, 35, 10, 2, '45.4585124', '9.20651145', 0, '2015-05-11 12:22:42', '2015-07-27 16:59:16', 0),
(131, 91, 1, 'RUNNING', 'gshhss\nshhs', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150511123150_6710.jpeg', 'milano', '', '2015-05-27', '16:31:00', 6, 30, 60, 10, 1, '45.4674136', '9.18077062', 2, '2015-05-11 12:31:50', '2015-07-27 16:59:16', 0),
(133, 91, 2, 'CINEMA', 'hsuhdd\nhdhhd\njd', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150511123544_8090.jpeg', 'milano', '', '2015-05-28', '19:35:00', 6, 20, 26, 20, 1, '45.4629626', '9.18681330', 0, '2015-05-11 12:35:44', '2015-07-27 16:59:16', 0),
(134, 91, 2, 'GARDALAND', 'jd&#039;unhd\nhdhhd\nhdhhdhdd\njdBjdd\njdhd\nbdBhd\nhdhhd\nhdhd', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150511123716_409.jpeg', 'verona', '', '2015-05-30', '17:36:00', 6, 18, 80, 15, 2, '45.4344791', '10.9898542', 0, '2015-05-11 12:37:16', '2015-07-27 16:59:16', 0),
(135, 91, 2, 'PARTY NIGHT', 'hdhhdd\nhardhd\nhdb', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150511124019_6883.jpeg', 'treviso', '', '2015-05-31', '23:30:00', 3, 20, 40, 20, 0, '45.6636292', '12.2424330', 0, '2015-05-11 12:40:19', '2015-07-27 16:59:16', 0),
(136, 91, 1, 'TENNIS MATCH', 'hdhdd\nhfhd\nhd', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150511124153_5884.jpeg', 'milano', '', '2015-05-29', '15:41:00', 3, 30, 40, 4, 1, '45.4684814', '9.19118463', 0, '2015-05-11 12:41:53', '2015-07-27 16:59:16', 0),
(138, 91, 2, 'BEACH PARTY', 'hgdd\nhdhd\nhdb', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150511144758_7344.jpeg', 'milano', '', '2015-06-05', '21:47:00', 6, 19, 36, 20, 1, '45.4749997', '9.20099582', 2, '2015-05-11 14:47:58', '2015-07-27 16:58:48', 0),
(140, 91, 1, 'BIKING', 'hdhhdd\njrhdd\njdd', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150512093225_174.jpeg', 'torino', '', '2015-06-05', '05:31:00', 1, 20, 40, 10, 2, '45.0673054', '7.69198872', 0, '2015-05-12 09:32:25', '2015-07-27 16:58:48', 0),
(141, 131, 7, 'CANE', 'Cane', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150512122130_197.jpeg', 'roma', '', '2015-05-29', '12:21:00', 6, 0, 8, 18, 2, '41.9027835', '12.4963655', 0, '2015-05-12 12:21:30', '2015-06-24 10:00:32', 0),
(142, 12, 1, 'PROVA SFUMATURA+BLUR ANTEPRIMA ', 'sdffffggggghgggggg\nttgggghh', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150512141200_857.jpeg', 'bologna', '', '2015-05-13', '14:11:00', 6, 0, 4, 89, 1, '44.494887', '11.3426163', 0, '2015-05-12 14:12:00', '2015-06-24 10:00:32', 0),
(147, 91, 2, 'FUSI DI TESTA', 'hdhhdd\nhdhdd\nhfinf\nhd', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150512183044_9318.jpeg', 'milano', '', '2015-06-06', '23:30:00', 6, 30, 40, 10, 1, '45.4606317', '9.18259184', 2, '2015-05-12 18:30:44', '2015-07-27 16:58:48', 0),
(148, 131, 5, 'IL BELLO DI VIVERE', 'Ola ', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150513153658_2177.jpeg', 'cerignola', 'via urbe 1', '2015-05-29', '15:36:00', 3, 0, 7, 12, 5, '41.260703', '15.87552', 0, '2015-05-13 15:36:59', '2015-06-24 10:00:32', 0),
(149, 142, 4, 'TEST MY EENT WITH MY FRIENDS', 'thi is a test to fix a bug', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'bari', 'via celentano 10', '2015-05-16', '20:00:00', 6, 18, 40, 15, 0, '41.1208744', '16.8729391', 2, '2015-05-13 15:55:04', '2015-06-24 10:00:32', 0),
(150, 177, 6, 'TEST EVENTO PRIVATO DDD', 'Serve per vedere alcune cose', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'Bari', 'via Giulio petroni 100', '2015-05-17', '20:00:00', 6, 15, 50, 20, 0, '41.1047024', '16.8674555', 1, '2015-05-13 17:11:27', '2015-06-24 10:00:32', 0),
(151, 91, 1, 'GET FIT', 'hdhhdjd\nhdhhdjd\nhfhhf\nbdhr\nhdhr', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150513192505_8240.jpeg', 'treviso', '', '2015-06-04', '20:10:00', 1, 20, 40, 4, 0, '45.6574247', '12.2333588', 0, '2015-05-13 19:25:05', '2015-07-27 16:58:48', 0),
(152, 91, 2, 'SUMMER TIME', 'hsuhdd\nhdhhfd\nhdhdd\nhdhr', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150513192648_4452.jpeg', 'rimini', '', '2015-06-06', '18:00:00', 0, 30, 40, 20, 1, '44.0713769', '12.5795103', 0, '2015-05-13 19:26:48', '2015-07-27 16:58:48', 0),
(153, 91, 2, 'ROCK NIGHT', 'hfhjjfd\njfujjf\nnfhjjdjn', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150513192923_1294.jpeg', 'treviso', '', '2015-06-16', '23:30:00', 2, 30, 35, 10, 2, '45.6644654', '12.2538103', 0, '2015-05-13 19:29:23', '2015-07-27 16:59:16', 0),
(155, 91, 3, 'CINEMA', 'hdhhdd\nhdhdd\nhdjfff\nhdhdf\n', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150513193626_1325.jpeg', 'milano', '', '2015-06-17', '20:00:00', 3, 20, 30, 4, 2, '45.4683993', '9.19149443', 0, '2015-05-13 19:36:26', '2015-07-27 16:59:16', 0),
(156, 91, 3, 'PICK NIC #SOLONDONNE', 'hehude\nheujr\nhrhjr\nhrjjr\nhrhr', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150513195336_1493.jpeg', 'milano', 'viale montenero', '2015-06-05', '15:00:00', 1, 20, 50, 5, 0, '45.4586551', '9.20621138', 0, '2015-05-13 19:53:36', '2015-07-27 16:58:48', 0),
(157, 91, 3, 'PRIVATE LOUNGE', 'hdhhdd\nhhdhdd\nhdhdhd', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150513220209_7323.jpeg', 'verona', '', '2015-06-03', '21:00:00', 0, 25, 30, 2, 0, '45.4366156', '10.9962610', 1, '2015-05-13 22:02:09', '2015-07-27 16:58:48', 0),
(158, 142, 7, 'CONCERTO DEL SECOLO', 'il concerto più fantastico che ci sia', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'bari', 'via buccari , 25', '2015-05-14', '19:59:00', 6, 45, 99, 10, 2, '41.110634', '16.8700141', 0, '2015-05-14 09:53:56', '2015-06-24 10:00:32', 0),
(159, 91, 3, 'JOIN ME', 'gdgdvdd\nhdhGEd\nhdhd', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150514104909_506.jpeg', 'montebelluna', '', '2015-06-03', '09:48:00', 0, 20, 40, 3, 0, '45.7781957', '12.0319366', 0, '2015-05-14 10:49:09', '2015-07-27 16:58:48', 0),
(160, 91, 1, 'POWER TRAINING FOR WOMAN', 'hsubhdd\nhdhhdd\nhdhGEd\n', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150515094242_8427.jpeg', 'milano', '', '2015-06-17', '09:00:00', 1, 30, 40, 5, 2, '45.4735122', '9.18136104', 0, '2015-05-15 09:42:42', '2015-06-24 10:00:32', 1),
(161, 91, 2, 'HSUBDHD', 'hdhhdd', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150518143131_9427.jpeg', 'milano', '', '2015-05-28', '15:29:00', 3, 8, 18, 4, 2, '45.4616420', '9.19239766', 0, '2015-05-18 14:31:31', '2015-06-24 10:00:32', 1),
(162, 168, 2, 'VINTAGE MELODY MARKET #3', 'Mercatino vintage e musica nel pomeriggio', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150519102656_649.jpeg', 'conegliano', 'bar radio golden', '2015-05-23', '10:25:00', 6, 18, 99, 999, 2, '45.8866975', '12.3020518', 0, '2015-05-19 10:25:52', '2015-06-24 10:00:32', 0),
(163, 12, 6, 'NBA CARES', 'Cares', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150519122230_4928.jpeg', 'bari', 'via capruzzi', '2015-05-30', '12:22:00', 6, 0, 38, 12, 3, '41.116962', '16.868685', 0, '2015-05-19 12:22:30', '2015-06-24 10:00:32', 0),
(164, 131, 6, 'DATEMI LE FERIE', 'Ho sonno', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150519130205_4882.jpeg', 'roma', '', '2015-08-15', '13:01:00', 3, 0, 100, 100, 1, '41.9027835', '12.4963655', 0, '2015-05-19 13:02:05', '2015-06-24 10:00:32', 0),
(165, 12, 1, 'PASSIONE VOLO', 'ygggyhhgfffghhhhh\ngggyyh', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150519153644_8037.jpeg', 'pordenone', '', '2015-05-24', '15:36:00', 3, 9, 36, 10, 3, '45.9626398', '12.6551362', 0, '2015-05-19 15:36:44', '2015-06-24 10:00:32', 0),
(172, 180, 6, 'INSTUDIO ', 'Hdhhd', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150519175347_5644.jpeg', 'Bari', '', '2016-02-03', '17:52:00', 1, 0, 13, 5, 0, '41.1171432', '16.8718715', 0, '2015-05-19 17:53:47', '2015-06-24 10:00:32', 0),
(173, 180, 4, 'GGGGF', 'Gggg', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'Bari', 'fucsia', '2024-11-26', '17:57:00', 3, 0, 14, 5, 0, '41.1171432', '16.8718715', 0, '2015-05-19 17:58:13', '2015-06-24 10:00:32', 0),
(174, 131, 6, 'EVENTO PRIVATO', 'Privato a privato a privato a privato a privato a privato di testa fastidioso il tuo commento sulla mappa del mio amico mi ha detto di essere un ', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150520113846_5920.jpeg', 'firenze', '', '2015-05-30', '11:38:00', 6, 0, 7, 123, 2, '43.7695604', '11.2558136', 1, '2015-05-20 11:38:46', '2015-06-24 10:00:32', 0),
(175, 131, 6, 'EVENTO SOLO AMICI', 'Ciao a tutti i giorni fa in loco e poi mi alleno un messaggio privato e le sue opere d&#039;arte e tu la mia mail personale di un suo. ', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150520114123_3340.jpeg', 'trento', '', '2015-05-29', '11:41:00', 3, 0, 8, 456, 2, '46.0700915', '11.1197626', 2, '2015-05-20 11:41:23', '2015-06-24 10:00:32', 0),
(176, 182, 7, 'SOLO AMICI 2', 'test', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'bari', '', '2015-05-26', '15:42:00', 3, 5, 9, 5, 0, '41.1171432', '16.8718715', 1, '2015-05-25 15:42:57', '2015-06-24 10:00:32', 0),
(179, 131, 6, 'PROVA', 'Ciao marco ha detto il ministro delle due ', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'roma', '', '2015-05-29', '16:23:00', 3, 0, 8, 123, 0, '41.9027835', '12.4963655', 1, '2015-05-25 16:24:04', '2015-06-24 10:00:32', 0),
(181, 131, 5, 'SOLO AMICI STRETTI', 'Ciao hai fatto colazione in albergo di ', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'trento', '', '2015-05-29', '16:31:00', 3, 8, 11, 15, 0, '46.0700915', '11.1197626', 1, '2015-05-25 16:31:48', '2015-06-24 10:00:32', 0),
(182, 131, 6, 'DIARIO', 'G di burro e marmellata di testa ', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150525170021_6005.jpeg', 'roma', '', '2015-05-29', '17:00:00', 3, 0, 7, 555, 0, '41.9027835', '12.4963655', 1, '2015-05-25 17:00:21', '2015-06-24 10:00:32', 0),
(183, 184, 2, 'TEST IGNORARE', 'descrizione delle descrizioni', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'bari', '', '2015-05-27', '17:52:00', 0, 5, 21, 5, 0, '41.1171432', '16.8718715', 0, '2015-05-26 17:53:50', '2015-06-24 10:00:32', 0),
(184, 184, 6, 'TEST IGNORARE PARTE SECONDA', 'ignorare sempre parte due', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'bari', '', '2015-05-27', '18:01:00', 0, 7, 9, 5, 0, '41.1171432', '16.8718715', 0, '2015-05-26 18:02:13', '2015-06-24 10:00:32', 0),
(185, 184, 2, 'TEST IGNORARE PARTE TRI', 'ignorami', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'bari', '', '2015-05-28', '18:06:00', 3, 7, 9, 5, 0, '41.1171432', '16.8718715', 0, '2015-05-26 18:06:56', '2015-06-24 10:00:32', 0),
(186, 182, 2, 'FESTIVAL DELLA GAZZOSA', 'tutta la gazzosa che vuoi. oceani di gazzosa', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150603144732_2794.jpeg', 'Bari', '', '2015-06-04', '14:47:00', 3, 5, 9, 5, 0, '41.1171432', '16.8718715', 0, '2015-06-03 14:47:32', '2015-06-24 10:00:32', 0),
(187, 182, 6, 'VALENCIA', 'vieni qui a Valencia', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150603145404_5668.jpeg', 'Bari', '', '2015-06-30', '14:53:00', 3, 5, 9, 583, 1, '41.1171432', '16.8718715', 0, '2015-06-03 14:54:04', '2015-06-24 10:00:32', 0),
(188, 182, 6, 'TEST', 'test', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'bari', '', '2015-09-17', '15:05:00', 0, 28, 100, 5, 0, '41.1171432', '16.8718715', 0, '2015-06-03 15:06:04', '2015-06-24 10:00:32', 0),
(189, 182, 3, 'TEST 2', 'bxbxbx', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'bari', 'via dante 6', '2016-02-25', '15:07:00', 0, 32, 49, 5, 0, '39.842718', '9.646133', 0, '2015-06-03 15:07:35', '2015-06-24 10:00:32', 0),
(190, 182, 6, 'TEST', 'ciao', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150612150128_7297.jpeg', 'bari', 'Via Dante 3', '2015-06-14', '15:18:00', 3, 7, 32, 5, 0, '39.843003', '9.646137', 0, '2015-06-03 15:18:47', '2015-06-24 10:00:32', 0),
(191, 182, 6, 'TEST', 'ggg', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'bari', '', '2015-09-17', '15:31:00', 0, 27, 31, 5, 0, '41.1171432', '16.8718715', 0, '2015-06-03 15:32:03', '2015-06-24 10:00:32', 0),
(192, 182, 6, 'CIAO', 'ciai', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'bari', '', '2015-06-28', '16:09:00', 0, 6, 8, 5, 0, '41.1171432', '16.8718715', 0, '2015-06-03 16:09:37', '2015-06-24 10:00:32', 0);
INSERT INTO `events` (`idevent`, `iduser`, `idcategory`, `title`, `description`, `img`, `city`, `address`, `date`, `hour`, `sex`, `agemin`, `agemax`, `sittotal`, `sitcurrent`, `latitude`, `longitude`, `scope`, `creationdate`, `updatetime`, `activated`) VALUES
(193, 182, 6, 'TESTGV', 'gvgg', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'bari', '', '2015-06-25', '16:32:00', 3, 6, 38, 5, 0, '41.1171432', '16.8718715', 0, '2015-06-03 16:33:08', '2015-06-24 10:00:32', 0),
(194, 182, 1, 'TEST', 'ciao', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150612150056_7287.jpeg', 'Bari', 'Via Dante', '2015-06-17', '16:48:00', 1, 15, 21, 5, 1, '39.8493334', '9.64374592', 0, '2015-06-03 16:49:07', '2015-06-24 10:00:32', 0),
(196, 182, 2, 'TITOLO', 'fghcvg', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'bari', '', '2015-08-26', '16:58:00', 0, 6, 9, 5, 0, '41.1171432', '16.8718715', 0, '2015-06-03 16:59:31', '2015-06-24 10:00:32', 0),
(197, 182, 6, 'TITOLO SENZA FANTASIA', 'descrizione senza fantasia', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'bari', '', '2015-09-30', '17:38:00', 1, 8, 9, 6, 0, '41.1171432', '16.8718715', 0, '2015-06-03 17:38:27', '2015-06-24 10:00:32', 0),
(198, 182, 6, 'TITOLO', 'descri', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'bari', '', '2015-09-17', '17:56:00', 0, 30, 32, 5, 0, '41.1171432', '16.8718715', 0, '2015-06-03 17:57:03', '2015-06-24 10:00:32', 0),
(199, 182, 6, 'BASTA EVENTI', 'bxhdbd', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'bari', '', '2015-06-25', '18:06:00', 0, 6, 30, 5, 0, '41.1171432', '16.8718715', 0, '2015-06-03 18:05:57', '2015-06-24 10:00:32', 0),
(200, 182, 7, 'TEST N', 'descrizione descrizione descrizione descrizione descrizione descrizione descrizione descrizione descrizione descrizione descrizione descrizione', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150618144148_7767.jpeg', 'bari', 'via verrazzano', '2015-06-24', '18:46:00', 3, 7, 22, 5, 1, '41.0682257', '16.9357756', 0, '2015-06-03 18:46:20', '2015-06-24 10:00:32', 0),
(202, 182, 6, 'ANCORA.SHARE TEST', 'hxvd', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'bari', '', '2015-10-28', '10:56:00', 1, 5, 9, 5, 0, '41.1171432', '16.8718715', 0, '2015-06-04 10:57:14', '2015-06-24 10:00:32', 0),
(203, 182, 6, 'SHARE EVENT', 'alla creazione', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'bari', '', '2015-09-24', '11:05:00', 1, 2, 6, 5, 0, '41.1171432', '16.8718715', 0, '2015-06-04 11:05:33', '2015-06-24 10:00:32', 0),
(204, 182, 2, 'TEST SHARE 3', 'ciai ciao ciao', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'bari', '', '2015-11-26', '11:12:00', 0, 6, 53, 5, 0, '41.1171432', '16.8718715', 0, '2015-06-04 11:13:03', '2015-06-24 10:00:32', 0),
(205, 182, 6, 'TEST SHARE 4', 'descrizione', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'bari', '', '2015-10-01', '11:31:00', 1, 7, 22, 5, 0, '41.1171432', '16.8718715', 0, '2015-06-04 11:31:43', '2015-06-24 10:00:32', 0),
(206, 182, 7, 'TEST MIC', 'descrizione', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'bari', '', '2015-11-19', '11:44:00', 1, 6, 28, 6, 0, '41.1171432', '16.8718715', 0, '2015-06-04 11:45:08', '2015-06-24 10:00:32', 0),
(207, 182, 6, 'CIAO SHARE', 'share', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'bari', '', '2015-11-27', '12:03:00', 3, 6, 9, 5, 0, '41.1171432', '16.8718715', 0, '2015-06-04 12:04:04', '2015-06-24 10:00:32', 0),
(208, 182, 6, 'ENNESSIMISSIMO.SHAREBXBDBXB', 'bzbxvvxvx', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'bari', '', '2015-11-26', '12:05:00', 3, 5, 33, 5, 0, '41.1171432', '16.8718715', 0, '2015-06-04 12:06:03', '2015-06-24 10:00:32', 0),
(209, 182, 4, 'LAST SHARE TEST', 'descrizione', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'bari', '', '2015-09-24', '12:07:00', 3, 7, 9, 5, 0, '41.1171432', '16.8718715', 0, '2015-06-04 12:08:03', '2015-06-24 10:00:32', 0),
(210, 182, 6, 'TITOLO', 'descrizione', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'bari', '', '2015-10-30', '12:27:00', 1, 6, 22, 5, 0, '41.1171432', '16.8718715', 0, '2015-06-04 12:28:07', '2015-06-24 10:00:32', 0),
(211, 182, 7, 'BBXBC', 'hxhxbx', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'bari', '', '2015-11-20', '12:34:00', 1, 7, 11, 5, 0, '41.1171432', '16.8718715', 0, '2015-06-04 12:34:41', '2015-06-24 10:00:32', 0),
(212, 182, 6, 'HZHDHDB', 'hsvdvd', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'bari', '', '2015-10-08', '12:53:00', 3, 7, 9, 5, 0, '41.1171432', '16.8718715', 0, '2015-06-04 12:53:42', '2015-06-24 10:00:32', 0),
(213, 182, 5, 'HCHCHC', 'vjvjvj', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'bari', '', '2015-10-02', '12:57:00', 3, 7, 19, 5, 0, '41.1171432', '16.8718715', 0, '2015-06-04 12:57:56', '2015-06-24 10:00:32', 0),
(214, 182, 2, 'VHCHCGG', 'cia', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'bari', '', '2015-10-22', '13:03:00', 3, 4, 21, 5, 0, '41.1171432', '16.8718715', 0, '2015-06-04 13:03:30', '2015-06-24 10:00:32', 0),
(215, 182, 7, 'VGVVVVV', 'vggg', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'bari', '', '2015-09-25', '13:10:00', 3, 7, 36, 5, 0, '41.1171432', '16.8718715', 0, '2015-06-04 13:10:19', '2015-06-24 10:00:32', 0),
(216, 91, 3, 'CINEMA', 'hshshshdsjhardjshd\nhdhdh', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150607192959_1834.jpeg', 'milano', '', '2015-06-09', '20:29:00', 3, 20, 25, 10, 0, '45.4611580', '9.18507758', 0, '2015-06-07 19:29:59', '2015-07-27 16:58:48', 0),
(217, 91, 1, 'MARATONA', 'hhsjsdd\nhsuhdd\nhdg', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150607193105_8284.jpeg', 'milano', '', '2015-06-12', '20:30:00', 3, 30, 40, 100, 2, '45.4656995', '9.18252781', 0, '2015-06-07 19:31:05', '2015-07-27 16:58:48', 0),
(219, 91, 1, 'KART', 'hdhhdd\nhdhgashdhd', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150607193400_1036.jpeg', 'milano', '', '2015-06-13', '20:20:00', 0, 15, 30, 12, 2, '45.4589790', '9.17727436', 0, '2015-06-07 19:34:00', '2015-06-24 10:00:32', 1),
(220, 91, 0, 'MANICURE', 'hdhdudd\nbdgdd\nhdhdd\nhd', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150607193553_6972.jpeg', 'milano', '', '2015-06-17', '14:35:00', 3, 18, 90, 3, 3, '45.4692180', '9.18874181', 0, '2015-06-07 19:35:53', '2015-06-24 10:00:32', 1),
(222, 91, 1, 'BIKE', 'hsuee\nhdhhr\nhehr', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150608203853_9284.jpeg', 'milano', '', '2015-06-13', '10:00:00', 3, 30, 40, 10, 2, '45.4705789', '9.17477890', 0, '2015-06-08 20:38:53', '2015-07-27 17:00:27', 0),
(223, 91, 2, 'XRAY', 'GEhdhdd\nhadhjdd\nbehd\njr', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150608204809_168.jpeg', 'milano', '', '2015-06-14', '14:40:00', 0, 15, 20, 4, 0, '45.4611129', '9.18458472', 0, '2015-06-08 20:48:09', '2015-06-24 10:00:32', 1),
(224, 182, 3, 'TEST CROP IMAGE', 'Test', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150609104621_2729.jpeg', 'Bari', '', '2016-06-09', '10:37:00', 1, 3, 16, 5, 0, '41.1171432', '16.8718715', 0, '2015-06-09 10:39:39', '2015-06-24 10:00:32', 0),
(225, 182, 5, 'TEST', 'Descrizione', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150609124709_3052.jpeg', 'Bari', 'via dante', '2016-06-09', '12:40:00', 1, 5, 20, 5, 0, '39.8422430', '9.64664490', 0, '2015-06-09 12:41:53', '2015-06-24 10:00:32', 0),
(228, 131, 12, 'IOIOIO', 'Mac OS ', 'http://www.join-me.it/web-service/dev/images/event/20150612155444775.jpeg', 'cerignola', 'via delle botteghe oscure 44', '2015-06-19', '10:30:00', 3, 20, 54, 62, 1, '41.2656592', '15.8939163', 0, '2015-06-10 15:25:13', '2015-06-24 10:00:32', 0),
(229, 12, 11, 'JOGGIN', 'Si corre tutte le sere in compagnia!!', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150611111717_6318.jpeg', 'conegliano', 'via matteotti 100', '2015-06-14', '20:00:00', 3, 25, 35, 10, 2, '45.8841273', '12.3131562', 0, '2015-06-11 11:17:17', '2015-06-24 10:00:32', 0),
(230, 142, 11, 'STORIA MOLTO MOLTO... ', 'questa è proprio una cosa bella. ', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'bari', 'Corso cavour 34', '2015-06-20', '10:56:00', 1, 1, 34, 52, 3, '41.1227169', '16.8727647', 0, '2015-06-12 10:57:19', '2015-06-24 10:00:32', 0),
(231, 182, 13, 'EVENTO IN FIERA', 'Testo', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150612152042_9743.jpeg', 'Bari', 'Fiera del levante', '2015-07-12', '15:18:00', 1, 0, 17, 5, 0, '41.1329013', '16.8397274', 0, '2015-06-12 15:20:42', '2015-06-24 10:00:32', 0),
(233, 188, 12, 'INVESTIAMO L&#039;ORRIDO CON LO SCHIACCIASASS', 'INT Milano', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150613170521_34.jpeg', 'Milano', 'Via Venezian 1', '2015-06-17', '16:00:00', 3, 0, 100, 25, 1, '45.4746616', '9.2321498', 0, '2015-06-13 17:05:21', '2015-06-24 10:00:32', 0),
(234, 182, 11, 'I FIORI', 'mi sento un po ambiguo', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150615095100_5234.jpeg', 'Bari', '', '2018-06-15', '09:49:00', 3, 4, 16, 5, 0, '41.1171432', '16.8718715', 0, '2015-06-15 09:51:00', '2015-06-24 10:00:32', 0),
(235, 182, 14, 'CASCATE', 'fluuush', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150615105725_1840.jpeg', 'bari', '', '2023-06-15', '10:56:00', 3, 12, 43, 5, 1, '41.1171432', '16.8718715', 0, '2015-06-15 10:57:25', '2015-06-24 10:00:32', 0),
(236, 182, 13, 'DEDEED', 'dede', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150615110947_9273.jpeg', 'bari', '', '2021-06-15', '11:09:00', 0, 0, 9, 5, 0, '40.6806380', '17.0411682', 0, '2015-06-15 11:09:47', '2015-06-24 10:00:32', 0),
(238, 12, 11, 'HHGGH', 'aaaaaasssaaa', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150615205144_3309.jpeg', 'milano', '', '2015-06-20', '20:51:00', 3, 0, 8, 95, 1, '45.4654219', '9.1859243', 0, '2015-06-15 20:51:44', '2015-06-24 10:00:32', 0),
(239, 12, 54, 'YOGA ALL&#039;ARIA APERTA', 'pratica yoga con noi nel parco della tua città\n ', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150616150333_2155.jpeg', 'Conegliano', '', '2015-06-30', '14:59:00', 3, 20, 70, 10, 1, '45.889197', '12.3006368', 0, '2015-06-16 14:59:51', '2015-06-24 10:00:32', 0),
(240, 91, 58, 'STREET PHOTOGRAPHY', 'Per chi volesse unirsi, si salpa all&#039;alba per un contest fotografico di gruppo! \n\nL&#039;itinerario passa per il Duomo, Piazza Castello e la zona di Brera. Noi siamo Canoniani sfegatati ma tutte le altre fazioni sono ben venute!\n\nIl mio corredo include un grandangolo (EF 16/35 f. 2.8) un fisso (EF 50 f. 1.8) ed uno zoom (EF 70/200 f 4). Per chi volesse provarne uno sul campo, massima disponibilita&#039; ovviamente.\n\nAperte le prenotazioni...ci sentiamo in chat per eventuali dettagli.\n', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150616182446_7321.jpeg', 'Milano', '', '2015-07-18', '06:00:00', 3, 25, 40, 8, 4, '45.4639462', '9.18917432', 0, '2015-06-16 18:24:46', '2015-07-20 10:16:47', 1),
(241, 91, 58, 'MOBILE PHOTOTOUR', 'Ed eccoci qui per un nuovo appuntamento rivolto a tutti gli appassionati di Mobile Photography! \r\n\r\nQuesta volta si pensava di scatenare la creativita&#039; tra la folla: il tema e&#039; &quot;Gente in Movimento&quot;. Per chi non si e&#039; unito alla scorsa escursione, ricordo che si parte armati di soli telefoni cellulari, per cio&#039; sono banditi &quot;cannoni&quot; e macchine fotografiche compatte di qualsiasi dimensione!\r\n\r\nVotiamo lo scatto migliore a fine giornata e il vincitore avra&#039; offerta la cena (il ristorante lo scegliamo prima tutti insieme...!).\r\n\r\nDimenticavo..in copertina la foto vincitrice della volta scorsa: bravo Luca!\r\n\r\nVi aspetto numerosi!', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150617145944_6472.jpeg', 'Milano', '', '2015-08-08', '15:00:00', 3, 20, 30, 6, 6, '45.4663111', '9.1976206', 0, '2015-06-17 14:59:44', '2015-07-27 16:51:17', 1),
(242, 142, 10, 'TEST', 'test', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'bari', '', '2015-06-18', '15:01:00', 3, 6, 7, 5, 0, '41.1171432', '16.8718715', 0, '2015-06-17 15:01:27', '2015-06-24 10:00:32', 0),
(243, 142, 10, 'GDGDGD', 'ggv', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'bari', '', '2015-06-18', '15:22:00', 3, 7, 11, 5, 0, '41.1171432', '16.8718715', 0, '2015-06-17 15:22:42', '2015-06-24 10:00:32', 0),
(244, 12, 52, 'TENNIS', 'Ciao a tutti,\ncerco un compagno per una partita a tennis di livello intermedio senza pretese,vorrei solo divertirmi e fare un po&#039; di sport!\nIo sono di Roma sud-est,per quanto riguarda il campo pensavo di incontrarci al tennis club APPIO CLAUDIO,viale Appio Claudio 115.\nPer chi si unirà,ci potremmo sentire in chat ed eventualmente scegliere una struttura più comoda a entrambi.', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150618112826_8872.jpeg', 'Roma ', 'via appio claudio 115', '2015-07-17', '20:00:00', 3, 35, 45, 1, 0, '41.8449403', '12.5597161', 0, '2015-06-18 11:28:26', '2015-06-23 08:31:09', 0),
(245, 91, 44, 'GOLF TRAINING', 'Sono alle prime armi e mi farebbe piacere trovare un collega che condivida questa mia passione! \n\nHo prenotato al golf club Monteverdi una prima sessione di prova di un paio d&#039;ore. Se qualcuno volesse unirsi sarebbe perfetto!\n\nL&#039;entrata al golf club e&#039; gratuita. Solo 10 euro per l&#039;affitto dell&#039;attrezzatura per chi non l&#039;avesse già. \n\nIo parto da Treviso in macchina e posso eventualmente dare un passaggio a chi abita in zona.\n\nSentiamoci per i dettagli!\n', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150624111257_808.jpeg', 'Brescia', '', '2015-07-18', '10:00:00', 3, 30, 40, 4, 2, '45.582036', '10.2691802', 0, '2015-06-20 12:23:59', '2015-07-11 09:11:49', 1),
(246, 91, 57, 'TREKKING ADAMELLO', 'Si parte da Milano alla volta del suggestivo Parco Adamello nel cuore delle Alpi Retiche.\n\nL&#039;itinerario non e&#039; impegnativo (e&#039; prevista una camminata di 4 ore circa imboccando il noto Sentiero n. 1) ma ci porta nel cuore della riserva. Se saremo fortunati non mancheranno avvistamenti di caprioli, camosci e cervi..\n\nIl tutto con molta calma..anche per chi volesse scattare qualche fotografia! \n\nUna macchina con tre posti liberi e&#039; a disposizione per i primi che si prenoteranno. \n\nUnica pecca.. partenza alle 7. Ci si trova in stazione Cairoli.\n\nAperte le iscrizioni!\n', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150620124912_8181.jpeg', 'Milano', '', '2015-07-26', '07:00:00', 3, 25, 35, 10, 4, '45.4677962', '9.18238967', 0, '2015-06-20 12:49:12', '2015-07-05 14:29:43', 1),
(247, 91, 53, 'TROPPO ESTREMO? PROVIAMOCI!', 'Se come noi avete sempre desiderato provarci e&#039; finalmente  arrivato il momento giusto! \n\nPensavamo di formare un gruppo di amici per un primo avventuroso tentativo sul fiume Adda. \n\nAbbiamo già individuato il centro sportivo, dovremmo solo poter confermare il numero esatto di persone entro due giorni prima della partenza, quindi affrettatevi a prenotare cosi&#039; ci mettiamo d&#039; accordo sui dettagli. \n\nIn macchina abbiamo posto per 3.\n\nA presto.\n', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150620130603_6578.jpeg', 'milano', '', '2015-08-23', '09:00:00', 3, 23, 26, 3, 3, '45.458127', '9.18781142', 0, '2015-06-20 13:06:03', '2015-07-27 17:17:54', 1),
(248, 91, 54, 'BEACH BREAK: SURF A SAN REMO!', 'Partiamo da Genova per San Remo dove c&#039;e&#039; un fantastico spot poco affollato. E&#039; un Beach Break che solitamente regala onde dai 2 ai 3 metri e le previsioni meteo per domenica sono ottime!\n\r\nSiamo muniti di due &quot;go pro&quot; montate sulle tavole e ci sara&#039; da divertirsi!\r\n\r\nCi troviamo sul lungo mare di Genova alle 14.30 (l&#039;indirizzo e&#039; sulla mappa). \r\n\r\nChi vuole unirsi?\r\n', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150620141136_752.jpeg', 'Genova', '', '2015-08-02', '14:30:00', 0, 25, 30, 3, 3, '44.4050534', '8.92748828', 0, '2015-06-20 14:11:36', '2015-07-28 14:38:30', 1),
(249, 142, 6, 'EVENTO TEST BADGE', 'evento test badge badge', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'bari', '', '2015-06-23', '15:59:00', 3, 0, 21, 10, 1, '41.1171432', '16.8718715', 0, '2015-06-22 15:59:31', '2015-06-24 10:00:32', 0),
(251, 91, 63, 'HARLEY SUMMER WEEK', 'Dopo l&#039;evento del mese scorso che ha visto piu&#039; di 3.000 Bikers sfilare per le strade capitoline e&#039; arrivato l&#039;appuntamento di agosto. \n\nLa Summer Bikers&#039; Week di quest&#039;anno si inaugura con la prima tappa in partenza da Bologna con direzione Riccione, dove avra&#039; luogo il Custom Biker Show: imperdibile kermess delle ultime novita&#039;, con eventi e competizioni di ogni tipo. \n\nSono gia&#039; attesi piu&#039; di 4.000 partecipanti che faranno riscaldare l&#039;atmosfera con le loro amate celebrita&#039; a due ruote. \n\nCosa aspettiamo? Creiamo un gruppo di Bikers in partenza da Milano e uniamoci al corteo! Pernottiamo direttamente a Riccione il primo giorno e rientriamo domenica in serata.\n\n\nForza, si parte!', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150622175455_9773.jpeg', 'Milano', '', '2015-07-18', '09:00:00', 3, 40, 50, 20, 4, '45.4868798', '9.24974597', 0, '2015-06-22 17:54:55', '2015-07-27 17:00:27', 0),
(252, 12, 21, 'ROCK SESSION', 'L’HUB 66 di Treviso organizza un concerto strepitoso durante il quale si alterneranno le migliori band del momento.\n\nPerché non ci organizziamo per fare un salto? L’entrata è libera fino alle 20.00. \n\nCi presentiamo: siamo tre studenti al terzo anno di Architettura… Rock addicted ovviamente!  Partiamo da Venezia con due macchine ed abbiamo 4 posti liberi. \n\nSe volete un passaggio e fare due chiacchere unitevi!\n\nCi sentiamo in chat per qualsiasi dettaglio.\n\nA presto\n', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150623190959_6146.jpeg', 'Venezia', 'via s.donà 358', '2015-07-24', '20:00:00', 3, 21, 25, 4, 4, '45.5033418', '12.2787996', 0, '2015-06-23 18:45:55', '2015-07-17 12:51:37', 1),
(253, 12, 38, 'TORNEO DI BOOWLING', 'Stavamo pensando di organizzare un piccolo torneo di Bowling. Ciascuno partecipa con 10 euro ed il malloppo, tolte le spese della pista, verrà assegnato ai vincitori!\n\nSiamo già in quattro e sarebbe grandioso se riuscissimo a creare un gruppo agguerrito piuttosto numeroso fino ad un massimo di altre 8 persone (lo spazio della pista è limitato). \n\nSe vi piace questo sport potrebbe essere un’ottima occasione per conoscerci.\n\nVi aspettiamo!   \n', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150623191944_1771.jpeg', 'treviso', 'via nobel 32 ', '2015-07-25', '20:30:00', 0, 35, 40, 8, 3, '45.7223885', '12.2508273', 0, '2015-06-23 19:19:44', '2015-07-27 16:30:48', 0),
(254, 12, 5, 'BRUNCH A BASE DI PESCE', 'A Milano ha aperto un nuovo locale che serve un Brunch domenicale strepitoso: un assortimento di specialità a base di pesce per un menù fisso di 25 €. Io posso garantire per la tartare, le capesante gratinate, il fritto ed i fagottini di granchio (deliziosi)! \n\nLa varietà degli assaggi è molto ricca e la location un ulteriore punto a favore (si mangia immersi in un vero e proprio giardino pensile). \n\nSiamo già in quattro (studenti di medicina a Milano) e stavamo pensando di organizzare una tavolata di gruppo per fare due chiacchere. Se qualcuno volesse unirsi ben venga!\n\nPrenoto io i posti per i primi che si uniranno entro giovedì 23 luglio.\n\nCi sentiamo in chat per i dettagli. \n', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150623194251_3340.jpeg', 'Milano', 'piazza cinque giornate 1', '2015-06-23', '21:30:00', 3, 24, 28, 8, 0, '45.4624959', '9.206551', 0, '2015-06-23 19:42:51', '2015-07-27 16:30:48', 0),
(255, 190, 23, 'A LOVE SUPREME', 'Il capolavoro di John Coltrane in una sessione di ascolto guidato. Segue jam session! ', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150624145431_1863.jpeg', 'Milano', 'Teatro Dal Verme', '2015-07-11', '22:00:00', 3, 0, 100, 30, 2, '45.4671528', '9.1813197', 0, '2015-06-24 14:54:31', '2015-07-27 16:58:48', 0),
(256, 12, 5, 'BRUNCH A BASE  DI PESCE', 'A Milano ha aperto un nuovo locale che serve un Brunch domenicale strepitoso: un assortimento di specialità a base di pesce per un menù fisso di 25 €. Io posso garantire per la tartare, le capesante gratinate, il fritto ed i fagottini di granchio (deliziosi)! \n\nLa varietà degli assaggi è molto ricca e la location un ulteriore punto a favore (si mangia immersi in un vero e proprio giardino pensile). \n\nSiamo già in quattro (studenti di medicina a Milano) e stavamo pensando di organizzare una tavolata di gruppo per fare due chiacchere. Se qualcuno volesse unirsi ben venga!\n\nPrenoto io i posti per i primi che si uniranno entro giovedì 23 luglio.\n\nCi sentiamo in chat per i dettagli.\n ', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150624162030_7967.jpeg', 'Milano', 'piazza cinque giornate 1', '2015-08-02', '21:00:00', 3, 24, 28, 3, 3, '45.4624959', '9.206551', 0, '2015-06-24 16:20:30', '2015-07-27 16:30:48', 0),
(257, 194, 54, 'TORNEO DI BILIARDO', 'In occasione del lancio dell&#039;app un evento di portata storica. Il primo (e tanto atteso) torneo di biliardo per pochi intimi a casa di Claudio. \n\nIo metto il biliardo. \n\nLui butta da bere.\n\nRisate assicurate. Grossi premi in palio. Le donne non sono ammesse se non ai piani superiori. \n', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150626175636_3856.jpeg', 'San Pietro di Feletto', 'via Tiziano Vecellio 17m', '2015-07-25', '20:00:00', 0, 30, 40, 8, 3, '45.902517', '12.2909285', 2, '2015-06-26 17:56:36', '2015-07-27 17:00:27', 0),
(258, 198, 7, 'TEST ANDROID MICHELE', 'test ', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150629125032_1249.jpeg', 'bari', '', '2016-01-30', '10:17:00', 3, 5, 9, 5, 4, '41.1171432', '16.8718715', 0, '2015-06-29 10:18:05', '2015-07-27 16:30:08', 0),
(259, 197, 11, 'TEST PRIVATO IOS', 'Privato', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150629102126_9505.jpeg', 'Bari', '', '2017-07-29', '10:20:00', 3, 0, 17, 5, 2, '41.1171432', '16.8718715', 1, '2015-06-29 10:21:26', '2015-07-27 16:30:08', 0),
(260, 197, 8, 'TEST PRIVATO 2', 'Test', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'Firenze', '', '2023-06-29', '10:21:00', 3, 0, 25, 10, 2, '43.7695604', '11.2558136', 1, '2015-06-29 10:38:31', '2015-07-27 17:00:27', 0),
(261, 197, 11, 'TEST PRIVATO 3', 'Test', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'Roma', '', '2016-11-01', '10:43:00', 3, 18, 47, 5, 2, '41.9027835', '12.4963655', 1, '2015-06-29 10:44:36', '2015-07-02 09:29:43', 1),
(263, 197, 1, 'TEST IOS MICHELE PER IPAD', 'Descrizione', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'Bari', '', '2024-06-29', '11:45:00', 3, 0, 14, 5, 1, '41.1171432', '16.8718715', 0, '2015-06-29 11:49:17', '2015-07-27 16:30:08', 0),
(264, 195, 11, 'APPLE TEST', 'iOS', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150630081831_2880.jpeg', 'Milano', '', '2015-07-20', '17:30:00', 3, 20, 25, 5, 2, '45.4701724', '9.19790123', 0, '2015-06-30 08:18:31', '2015-07-27 16:32:04', 0),
(266, 198, 1, 'FRIENDS ONLY EVENT BY MICHELE.ANDROID', 'test', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'bari', '', '2016-03-25', '12:44:00', 1, 7, 38, 5, 1, '41.1171432', '16.8718715', 2, '2015-07-01 12:45:24', '2015-07-27 16:30:08', 0),
(267, 200, 11, 'TEST', 'des', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150709161528_5717.jpeg', 'bari', '', '2015-10-31', '11:00:00', 0, 0, 6, 5, 0, '41.1171432', '16.8718715', 1, '2015-07-02 11:01:29', '2015-07-27 17:03:17', 0),
(268, 195, 59, 'TEST APPLE IOS', 'Test', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150705162839_8087.jpeg', 'Milano', '', '2015-08-05', '10:00:00', 3, 20, 30, 10, 2, '45.4654219', '9.1859243', 2, '2015-07-05 16:28:39', '2015-07-27 16:32:04', 0),
(269, 195, 62, 'TEST IOS ', 'Test', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150705163242_2878.jpeg', 'Roma', '', '2015-10-05', '02:30:00', 3, 40, 45, 100, 1, '41.9007722', '12.4961112', 1, '2015-07-05 16:32:42', '2015-07-27 16:58:48', 0),
(270, 12, 67, 'PER UN POSTO MIGLIORE', 'Ciao... domenica prossima ci troviamo\nnuovamente in centro per tornare a pulire\nle strade ed i muri del nostro quartiere!\n\nlo scorso mese e stato un successo...\na fine giornata ricco buffet organizzato dalla\nnostra associazione....\n\nvi aspettiamo numerosi\n\n', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150706145915_8997.jpeg', 'Treviso', 'Piazza del Duomo', '2015-07-12', '09:00:00', 3, 0, 100, 100, 2, '45.6664887', '12.2426153', 0, '2015-07-06 14:53:48', '2015-07-27 16:30:48', 0),
(272, 179, 1, 'TITOLO', 'Ggggg', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150708163756_4343.jpeg', 'bari', '', '2015-08-14', '16:36:00', 3, 0, 8, 5, 2, '41.1120115', '16.8595754', 0, '2015-07-08 16:37:56', '2015-07-27 16:30:08', 0),
(274, 179, 2, 'GGG', 'Gghg', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'bari', '', '2015-08-08', '17:14:00', 3, 0, 12, 6, 0, '41.1351191', '16.8405031', 2, '2015-07-08 17:17:10', '2015-07-27 16:30:08', 0),
(275, 179, 13, 'HHHHH', 'Hhhhh', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'bari', '', '2015-08-14', '17:44:00', 3, 0, 24, 6, 0, '41.1171432', '16.8718715', 1, '2015-07-08 17:45:29', '2015-07-27 16:30:08', 0),
(276, 179, 16, 'BUBYB', 'Yvh', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'bari', '', '2015-09-04', '17:45:00', 3, 0, 11, 6, 0, '41.1171432', '16.8718715', 1, '2015-07-08 17:46:10', '2015-07-27 16:30:08', 0),
(277, 179, 14, 'UBUGHBH', 'Hbhbhb', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'bari', '', '2015-08-21', '17:55:00', 1, 0, 19, 7, 0, '41.1171432', '16.8718715', 1, '2015-07-08 17:56:00', '2015-07-27 16:30:08', 0),
(282, 206, 4, 'PROSECCO TOUR', 'Tour in the hills of Prosecco with great drinks and start in the center of Conegliano', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150713222711_6326.jpeg', 'Conegliano', '', '2015-07-24', '18:00:00', 0, 35, 45, 24, 8, '45.889197', '12.3006368', 0, '2015-07-13 22:27:12', '2015-07-27 16:31:15', 0),
(284, 208, 0, 'GO VEGAN', 'vegan is better! ', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150715111526_4213.jpeg', 'bari', '', '2015-07-18', '20:00:00', 3, 0, 100, 999, 2, '41.1285363', '16.8690347', 0, '2015-07-15 11:15:26', '2015-07-27 16:31:15', 0),
(286, 197, 0, 'TEST IOS DA RIMUOVERE', 'Test', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'Bari', '', '2015-07-16', '14:44:00', 3, 0, 19, 5, 0, '41.1171432', '16.8718715', 0, '2015-07-15 14:45:49', '2015-07-16 09:00:30', 0),
(288, 163, 8, 'TEST CANCELLAZIONE 1', 'I miei sono arrivati i soldi ', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'cerignola', '', '2016-09-17', '11:51:00', 0, 0, 10, 23, 1, '41.2656592', '15.8939163', 0, '2015-07-16 11:52:10', '2015-07-27 16:30:08', 0),
(289, 163, 7, 'CANCELLAZIONE 2', 'Ciao marco ha deciso per per ', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'roma', '', '2015-10-09', '11:52:00', 0, 0, 8, 12, 2, '41.9027835', '12.4963655', 0, '2015-07-16 11:52:47', '2015-07-27 16:31:15', 0),
(290, 163, 8, 'CANCELLAZIONE 3', 'Si ma di sicuro troverai sicuramente annunci ', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'roma', '', '2015-09-27', '11:54:00', 3, 0, 8, 132, 3, '41.9027835', '12.4963655', 0, '2015-07-16 11:54:53', '2015-07-27 16:31:15', 0),
(293, 214, 66, 'APPUNTAMENTO GIOCO PER CANI!', 'Ciao a tutti, padroni di quattrozampe! Vorrei incontrarci al parco nell&#039;area cani per poter far giocare assieme i nostri amici pelosi,  e magari bersi una birra assieme mentre ci raccontiamo le loro imprese più buffe!! \r\nVorrei cani equilibrati, amichevoli,  ben socializzati; se hai un cucciolo e vuoi che impari a socializzare con altri pelosi sei superbenvenuto!!\r\n\r\nMarta', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150717105133_9478.jpeg', 'Milano', 'parco della vetra', '2015-08-12', '17:30:00', 3, 16, 100, 3, 3, '45.4584468', '9.18273467', 0, '2015-07-17 10:51:33', '2015-07-29 11:43:42', 1),
(294, 214, 8, 'SUSHI &amp; FRIVOLEZZE', 'Mi rivolgo alle fanciulle appassionate di sushi che come me sono rimaste a Milano a lavorare: incontriamoci e combattiamo caldo e noia con del bel sashimi fresco!! ', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150717111606_4408.jpeg', 'Milano', 'Lambrate', '2015-08-08', '12:30:00', 1, 25, 35, 5, 5, '45.4839139', '9.2339253', 0, '2015-07-17 11:16:06', '2015-07-31 12:42:35', 1),
(296, 213, 21, 'AUTOBUS MILANO-ROMA CONCERTO THE RAININGS', 'Il 13 settembre a Roma ci sarà l&#039;unica data italiana dei RAININGS! chi altro ha un progetto di andarci partendo da Milano e provincia? Se siamo almeno 20 possiamo affittare un autobus dal sito www.autobus.it e risparmiare sul treno, e farci il ritorno dormendo senza bisogno di pernottare a Roma! Che ne dite? Partiremmo da Milano Centrale verso le 10 per essere lì massimo alle 17. Dell&#039;organizzazione mi occupo io. Scrivetemi in pvt!', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150717115719_3431.jpeg', 'Milano', 'stazione centrale', '2015-09-13', '10:00:00', 3, 18, 60, 50, 3, '45.4858877', '9.2042827', 0, '2015-07-17 11:57:19', '2015-07-27 16:53:59', 0),
(297, 214, 35, 'ORDINE COLLETTIVO LIBRI', 'Appassionati di libri a rapporto!!\r\n\r\nSto per fare un ordine sul sito www.leggichetifabene.com: oltre i 70 euro di spesa il sito fa spediziono gratuite, e applica uno sconto del 10% ! \r\n\r\nHo contrattato il sito e sono disposti a farci pagare singolarmente i tomi scelti: cosa stiamo aspettando?? \r\n\r\nScrivetemi in pvt di che zona di Milano siete cosi possiamo scegliere una zona comoda per tutti poi per il ritiro dei libri!', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150717120814_9495.jpeg', 'Milano', '', '2015-07-28', '10:00:00', 3, 0, 100, 3, 3, '45.4832214', '9.18601568', 0, '2015-07-17 12:08:14', '2015-07-28 12:13:43', 1),
(298, 163, 8, 'LA SPERANZA', 'I miei sono arrivati i soldi per ', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150720094853_6726.jpeg', 'cerignola', '', '2015-07-31', '09:48:00', 3, 0, 8, 123, 2, '41.2656592', '15.8939163', 0, '2015-07-20 09:48:53', '2015-07-27 16:22:50', 0),
(299, 213, 45, 'RUNNERS DOMENICALI CERCASI!', 'Adoro correre la domenica mattina al parco e cerco altri appassionati con cui fare quattro chiacchiere durante la corsa e scambiarsi magari qualche consiglio riguardo a riscaldamento, circuiti etc! Corro al parco Sempione solitamente, ma sono curiosa di conoscere i vostri percorsi preferiti!\r\nPerché non iniziare già questa domenica?', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150720112835_9690.jpeg', 'Milano', 'Parco Sempione', '2015-08-09', '09:00:00', 3, 20, 35, 3, 3, '45.4720981', '9.1772243', 0, '2015-07-20 11:28:35', '2015-07-27 16:52:05', 1),
(300, 163, 7, 'TEST IMMAGINE NON COMPRESSA', 'Non compressa ', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150721094905_9950.jpeg', 'cerignola', '', '2016-07-01', '09:48:00', 3, 0, 12, 123, 0, '41.2656592', '15.8939163', 0, '2015-07-21 09:49:05', '2015-07-27 16:22:50', 0),
(301, 163, 7, 'COMORESSIONE 50', 'I miei sono arrivati i ', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150721095323_5926.jpeg', 'cerignola', '', '2016-05-06', '09:53:00', 0, 0, 6, 12, 1, '41.2656592', '15.8939163', 0, '2015-07-21 09:53:23', '2015-07-27 16:22:50', 0),
(302, 212, 1, 'TEST COMPRESSIONE IOS 1.0', 'Gggg', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150721100608_4992.jpeg', 'Bari', '', '2016-01-29', '10:05:00', 3, 0, 13, 5, 0, '41.1171432', '16.8718715', 0, '2015-07-21 10:06:08', '2015-07-27 16:22:50', 0),
(303, 212, 11, 'TEST COMPRESSIONE 1.0', 'Hhh', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150721100849_907.jpeg', 'Bari', '', '2016-01-01', '10:06:00', 1, 0, 13, 5, 0, '41.1171432', '16.8718715', 0, '2015-07-21 10:08:49', '2015-07-27 16:22:50', 0),
(304, 163, 7, 'DUE MEGA COVER', 'I miei sono arrivati i soldi ', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150721100942_4943.jpeg', 'cerignola', '', '2015-07-31', '10:09:00', 3, 0, 6, 123, 1, '41.2656592', '15.8939163', 0, '2015-07-21 10:09:42', '2015-07-27 16:22:50', 0),
(305, 212, 7, 'TEST COMPRESSIONE 0.5', 'Ggg', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150721101341_7586.jpeg', 'Bari', '', '2016-01-02', '10:12:00', 3, 0, 15, 5, 0, '41.1171432', '16.8718715', 0, '2015-07-21 10:13:41', '2015-07-27 16:22:50', 0),
(306, 212, 11, 'TEST COMP 0.5 IOS', 'Gg', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150721101654_9400.jpeg', 'Bari', '', '2016-01-02', '10:13:00', 3, 0, 10, 5, 0, '41.1171432', '16.8718715', 0, '2015-07-21 10:16:54', '2015-07-27 16:22:50', 0),
(307, 163, 8, 'CHIARA 0.50', 'Ciao marco mi sono appena ', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150721103421_9359.jpeg', 'roma', '', '2016-08-27', '10:34:00', 3, 0, 8, 12, 0, '41.9027835', '12.4963655', 0, '2015-07-21 10:34:21', '2015-07-27 16:22:50', 0),
(308, 182, 65, 'GUNDAM CONVENTION', 'appassionati di gundam unitevi', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150725071948_5882.jpeg', 'Bari', '', '2016-02-27', '07:19:00', 1, 10, 20, 10, 0, '41.1171432', '16.8718715', 0, '2015-07-25 07:19:48', '2015-07-27 16:22:50', 0),
(309, 182, 1, 'GELATI', 'hsgd', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150725072305_6647.jpeg', 'Bari', '', '2016-02-22', '07:22:00', 1, 10, 20, 5, 0, '41.1171432', '16.8718715', 0, '2015-07-25 07:23:05', '2015-07-27 16:22:50', 0),
(310, 182, 65, 'GUNDAM CONCENTION_2', 'hdhdgd', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150725072651_7939.jpeg', 'Bari', '', '2016-03-17', '07:26:00', 1, 10, 20, 5, 0, '41.1171432', '16.8718715', 0, '2015-07-25 07:26:51', '2015-07-27 16:22:50', 0),
(311, 182, 61, 'TEST MY EVENT', 'hdhdh', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150725072937_4918.jpeg', 'Bari', '', '2016-02-19', '07:29:00', 0, 5, 20, 5, 1, '41.1171432', '16.8718715', 0, '2015-07-25 07:29:37', '2015-07-27 16:22:50', 0),
(312, 182, 4, 'TEST MY EVENT 2', 'ggg', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150725073445_6100.jpeg', 'bari', '', '2016-02-11', '07:34:00', 1, 10, 20, 5, 1, '41.1171432', '16.8718715', 0, '2015-07-25 07:34:45', '2015-07-27 16:22:50', 0),
(313, 182, 8, 'TEST DATA', 'ggt', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150725074016_6823.jpeg', 'bari', '', '2016-01-01', '07:40:00', 0, 6, 18, 5, 1, '41.1171432', '16.8718715', 0, '2015-07-25 07:40:16', '2015-07-27 16:22:50', 0),
(315, 194, 66, 'WEEK END DA LUPI ', 'Facciamo branco in consiglio.\n\nCampeggeremo, griglieremo, cammineremo tra i boschi, andremo a caccia di prede selvatiche: tutto con in nostri lupi!! Ululeremo alla luna cercando di risvegliare il lato più misterioso ed affascinante dei nostri cani e tireremo fuori il vero lupo che c&#039;è in loro. \n\nUn occasione unica per fare branco e conoscerci tutti insieme.\n', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150726140249_9652.jpeg', 'Piana del Cansiglio', '', '2015-08-14', '15:00:00', 3, 0, 100, 100, 4, '46.0665577', '12.4052172', 0, '2015-07-26 14:02:49', '2015-07-31 14:08:20', 0),
(316, 205, 65, 'LA SCELTA', 'Eccoci siamo giunti nella tana del bianconiglio ragazzi.\r\nE dobbiamo decidere \r\nPillola rossa \r\nPillola blu \r\nSiete invitati a scegliere.\r\nNeo', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150726181203_8625.jpeg', 'conegliano', '', '2015-08-01', '18:07:00', 3, 31, 37, 3, 3, '45.9066715', '12.3046506', 0, '2015-07-26 18:12:03', '2015-07-28 10:01:08', 0),
(317, 131, 10, 'BROKEN SWORD', 'ottenere esse ottenere', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150727102550_7384.jpeg', 'foggia', '', '2015-10-09', '10:25:00', 0, 0, 30, 23, 2, '41.4621984', '15.5446302', 0, '2015-07-27 10:25:50', '2015-07-27 16:20:54', 0),
(318, 131, 11, 'SHENMUE 3', 'ottenere esse', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150727102734_9781.jpeg', 'roma', '', '2015-07-28', '10:27:00', 0, 0, 13, 233, 2, '41.9027835', '12.4963655', 0, '2015-07-27 10:27:34', '2015-07-27 16:20:54', 0),
(319, 224, 8, 'CUCINARE A STARE', 'cucina le cose che poi te le mangi', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150727160900_9865.jpeg', 'bari', '', '2015-07-30', '10:00:00', 3, 20, 60, 40, 0, '41.128735', '16.851505', 0, '2015-07-27 16:09:00', '2015-07-27 16:20:54', 0),
(320, 225, 11, 'YYYYYYYY', 'ggghhh', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'roma', '', '2015-07-28', '16:25:00', 0, 0, 12, 23, 0, '41.9027835', '12.4963655', 0, '2015-07-27 16:25:23', '2015-07-27 16:20:28', 0),
(323, 198, 10, 'TEST DA RIMUOVERE', 'test', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'bari', '', '2016-02-17', '13:11:00', 3, 5, 9, 5, 0, '41.1171432', '16.8718715', 1, '2015-07-28 13:11:21', '2015-07-28 11:13:57', 1),
(324, 239, 45, 'RUNNING', 'corsa amatoriale a Sacile alle 6 di mattina', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150728160352_8431.jpeg', 'sacile', 'via Martiri Sfriso', '2015-08-02', '06:00:00', 3, 20, 100, 5, 5, '45.9530761', '12.4963217', 0, '2015-07-28 13:41:38', '2015-07-30 11:11:49', 1),
(326, 194, 58, 'IL BRAMITO DEL CERVO', 'Una notte nei boschi del Cansiglio per spiare e fotografare i cervi in amore, ascoltare il loro bramito, catturare un immagine di un&#039;incornata fra maschi, per la conquista della femmina, ascoltare il silenzio della foresta.\r\n\r\nA tu per tu con la natura, mimetizzati fra faggi e pini, attenderemo pazientemente e silenziosamente uno fra gli animali più selvatici e discreti delle nostre montagne. \r\n\r\nL&#039;invito è aperto a fotografi amatoriali e o professionisti, ma possibilmente forniti della giusta attrezzatura. ( consigliato almeno un 200 millimetri ) \r\n\r\nCi mimetizzeremo nella foresta, quindi è consigliata una tenuta idonea alla vegetazione. \r\n\r\nL&#039;evento potrà subire delle variazioni di orario e o data in base al tempo. \r\n\r\nPartenza da Conegliano Veneto.\r\nPer chi vuole si pranza tutti insieme in malga coro in ristorante.\r\n', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150728154930_3536.jpeg', 'Piana del Cansiglio', '', '2015-10-04', '05:00:00', 3, 20, 50, 8, 4, '46.0665577', '12.4052172', 0, '2015-07-28 15:49:30', '2015-08-19 08:38:23', 1),
(328, 245, 7, 'TEST EVENT', 'dhhd', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'bari', '', '2016-02-05', '17:07:00', 3, 4, 9, 5, 0, '41.1171432', '16.8718715', 1, '2015-07-28 17:07:14', '2015-07-29 05:59:56', 1),
(330, 228, 68, 'PARTITA', 'testhdh dudhrj uguhuvy  ubuvu  ygugubub  ugyuyifihv   ugiviyvyv  \ngyvy\nvygygy\n\n\n6g6g6g6 y7h\n', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150729111402_7332.jpeg', 'giovinazzo', '', '2016-03-17', '11:08:00', 3, 10, 49, 5, 1, '41.1866839', '16.6735966', 1, '2015-07-29 11:14:02', '2015-07-29 10:47:55', 1),
(331, 163, 8, 'EVENTO PER MICHELE', 'Ciao a odoroso il tuo commento devi essere in un momento in un momento in un momento in un momento in un momento di rinascita di testa fastidioso di un po di mal di testa e il mio é un buon profiler e ', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150729111727_2139.jpeg', 'gioia del colle', '', '2015-08-16', '11:17:00', 3, 0, 8, 23, 1, '40.7990706', '16.9221587', 1, '2015-07-29 11:17:27', '2015-07-29 09:19:25', 1),
(333, 228, 12, 'YDYFYFH', 'jdhdhhr', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150729172453_8655.jpeg', 'bari', '', '2015-09-11', '17:19:00', 3, 10, 20, 5, 0, '41.1171432', '16.8718715', 1, '2015-07-29 17:24:53', '2015-07-29 15:24:53', 1),
(334, 228, 14, 'TFFTVT', 'tctvt', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150729172545_183.jpeg', 'bari', 'via dante', '2015-08-13', '17:20:00', 3, 9, 15, 5, 0, '39.8423682', '9.6462039', 1, '2015-07-29 17:25:45', '2015-07-30 09:00:31', 1),
(335, 228, 11, 'TVRVRVT', 'vrvrv4', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150729172705_3847.jpeg', 'bari', '', '2015-08-27', '17:21:00', 1, 10, 15, 5, 0, '41.1171432', '16.8718715', 1, '2015-07-29 17:27:05', '2015-07-30 07:21:12', 1),
(336, 228, 11, 'GDHDHHD', 'hdhhxhx', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150730105955_4041.jpeg', 'bari', '', '2015-08-20', '10:54:00', 3, 42, 44, 5, 0, '41.1171432', '16.8718715', 1, '2015-07-30 10:59:55', '2015-07-30 08:59:55', 1),
(337, 131, 12, 'TEST MICHELE 1', 'ottenere esse', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150730112204_5180.jpeg', 'carapelle', '', '2015-08-08', '11:21:00', 3, 0, 11, 123, 0, '41.3646216', '15.6905513', 1, '2015-07-30 11:22:04', '2015-07-30 09:22:04', 1),
(338, 131, 11, 'MICHELE TEST 2', 'ottenere esse', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150730112627_1449.jpeg', 'carapelle', '', '2015-08-28', '11:25:00', 3, 0, 17, 65, 0, '41.3646216', '15.6905513', 1, '2015-07-30 11:26:27', '2015-07-30 09:26:27', 1),
(339, 131, 12, 'TEST MICHELE 3', 'ottenere esse', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150730112829_8509.jpeg', 'foggia', '', '2015-08-28', '11:28:00', 3, 0, 12, 25, 0, '41.4621984', '15.5446302', 1, '2015-07-30 11:28:29', '2015-07-30 09:28:29', 1),
(340, 182, 10, 'PAPERINIK RULRZZ,CCC EDIT', 'hxhxhx', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150730115754_4205.jpeg', 'bari', '', '2015-11-18', '11:36:00', 1, 4, 36, 5, 1, '41.1171432', '16.8718715', 1, '2015-07-30 11:36:28', '2015-07-31 05:43:29', 1),
(341, 182, 11, 'CHE UOMO EDIT 2', 'un uomo che ciao', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150730114127_1139.jpeg', 'triggiano', 'via verrazzano &#039;', '2015-08-20', '11:40:00', 3, 0, 6, 5, 1, '41.0682257', '16.9357756', 1, '2015-07-30 11:41:27', '2015-08-18 12:47:11', 1),
(342, 182, 7, 'CHE UOMO PARTE DUE', 'bxbxbxbx', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150730114818_2016.jpeg', 'bari', '', '2015-12-17', '11:47:00', 3, 7, 9, 5, 0, '41.1171432', '16.8718715', 1, '2015-07-30 11:48:18', '2015-07-30 09:48:18', 1),
(343, 182, 10, 'IL TRAMONTO ..... CHE NOIA', 'meglio i videogiochi', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150730115307_5185.jpeg', 'bari', 'via principe amedeo', '2015-07-30', '16:49:00', 3, 7, 9, 5, 2, '41.1222404', '16.8621693', 1, '2015-07-30 11:49:56', '2015-07-30 18:04:55', 1),
(344, 182, 8, 'ANCORA UN EVENTO', 'un noioso evento', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150730115551_3715.jpeg', 'cassano', '', '2015-08-22', '11:54:00', 3, 6, 9, 5, 0, '40.890868', '16.7690532', 1, '2015-07-30 11:54:21', '2015-07-30 09:55:51', 1),
(345, 182, 1, 'CHCH EDIT 4', 'bxbx', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150731152631_8021.jpeg', 'cassano', '', '2015-12-04', '15:56:00', 0, 5, 21, 5, 0, '40.890868', '16.7690532', 1, '2015-07-30 11:57:14', '2015-08-18 12:46:41', 1),
(346, 163, 54, 'SURF IN SPIAGGIA A BARI', 'Surf in spiaggia a Bari! ', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150730122323_7164.jpeg', 'bari', '', '2016-06-03', '12:23:00', 3, 0, 100, 100, 4, '41.1171432', '16.8718715', 1, '2015-07-30 12:23:23', '2015-07-30 18:24:05', 1),
(347, 182, 1, 'GXGG', 'hxxx', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150730122922_294.jpeg', 'bari', '', '2015-09-03', '12:29:00', 3, 18, 25, 5, 0, '41.1171432', '16.8718715', 1, '2015-07-30 12:29:22', '2015-07-30 10:51:47', 1),
(348, 182, 1, 'VFVVV', 'vvvvv', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'bari', '', '2015-09-10', '12:54:00', 3, 6, 9, 5, 0, '41.1171432', '16.8718715', 1, '2015-07-30 12:55:21', '2015-07-30 11:06:03', 1),
(349, 182, 8, 'GVGGG', 'vvvh', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'bari', '', '2015-08-27', '13:16:00', 3, 5, 16, 5, 2, '41.1171432', '16.8718715', 1, '2015-07-30 13:16:59', '2015-07-31 14:03:04', 1),
(350, 182, 14, 'CFGG EDIT', 'vvgg', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150730131849_5862.jpeg', 'bari', '', '2015-08-20', '13:18:00', 3, 6, 9, 5, 0, '41.1171432', '16.8718715', 1, '2015-07-30 13:18:49', '2015-07-31 05:43:11', 1),
(352, 265, 58, 'VIDEOGIOCHI CHE PASSIONE', 'che passione i videogiochi', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150730162912_3601.jpeg', 'foggia', '', '2016-06-10', '16:28:00', 3, 0, 13, 32, 0, '41.4621984', '15.5446302', 0, '2015-07-30 16:29:12', '2015-07-30 19:47:17', 0),
(353, 163, 7, 'METAL SLUG', 'Ciao a tutte e quattro i dati della società ', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150730200738_4061.jpeg', 'cerignola', '', '2016-04-16', '20:07:00', 3, 0, 8, 123, 1, '41.2656592', '15.8939163', 1, '2015-07-30 20:07:38', '2015-07-30 18:08:18', 1),
(354, 163, 7, 'TEST', 'Ciao hai in mente un altro ', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150730201346_1960.jpeg', 'roma', '', '2016-04-30', '20:13:00', 3, 0, 7, 12, 2, '41.9027835', '12.4963655', 1, '2015-07-30 20:13:46', '2015-07-30 18:15:39', 1),
(355, 182, 7, 'NEW PRIVATE EDIT 2', 'hdhdh', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150731073832_3897.jpeg', 'bari', '', '2015-08-14', '07:38:00', 3, 100, 100, 5, 1, '41.1171432', '16.8718715', 1, '2015-07-31 07:38:32', '2015-08-05 12:00:33', 1),
(357, 182, 10, 'GXGG', 'bxbbcb', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150731110812_6247.jpeg', 'bari', '', '2015-09-10', '11:13:00', 1, 6, 26, 5, 0, '41.1171432', '16.8718715', 1, '2015-07-31 11:08:12', '2015-07-31 09:08:12', 1),
(358, 182, 10, 'GXGG', 'bxbbcb', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150731110823_8986.jpeg', 'bari', '', '2015-09-10', '11:13:00', 1, 6, 26, 5, 0, '41.1171432', '16.8718715', 1, '2015-07-31 11:08:23', '2015-07-31 09:08:23', 1),
(359, 182, 1, 'BXHHX', 'hshdbd', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150731121842_9305.jpeg', 'bari', '', '2015-10-30', '12:18:00', 3, 3, 9, 5, 0, '41.1171432', '16.8718715', 1, '2015-07-31 12:18:42', '2015-07-31 10:18:42', 1),
(360, 182, 1, 'JXHX', 'bxbx', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150731122113_2579.jpeg', 'bari', '', '2015-08-21', '12:20:00', 1, 6, 9, 5, 0, '41.1171432', '16.8718715', 1, '2015-07-31 12:21:13', '2015-07-31 10:21:13', 1),
(361, 182, 8, 'GXGGX', 'gxgxgx', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150731123626_9483.jpeg', 'bari', '', '2016-01-14', '12:35:00', 3, 5, 9, 5, 0, '41.1171432', '16.8718715', 1, '2015-07-31 12:36:26', '2015-07-31 10:36:26', 1),
(362, 182, 13, 'VGG', 'gggg', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150731130053_6585.jpeg', 'bari', '', '2015-08-27', '13:00:00', 3, 2, 6, 5, 0, '41.1171432', '16.8718715', 1, '2015-07-31 13:00:53', '2015-07-31 11:00:53', 1),
(363, 142, 6, 'CENA VEGANA A BASE DI ZUCCA', 'La zucca è uno degli ortaggi piu ricchi e sani, ovviamente se proviene, come nel nostro caso, da agrucoltura naturale. Sfiziosissimi piatti a base di zucca possono essere preparati e con poco sforzi e tanta fantasia si fa della noiosa sera di alloween una cena colorata di arancione', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150731150657_9364.jpeg', 'Bari', '', '2015-11-01', '20:00:00', 3, 25, 45, 15, 3, '41.121099', '16.880444', 0, '2015-07-31 15:05:54', '2015-07-31 14:19:54', 0),
(364, 131, 12, 'MICHELE TEST', 'che passione oceanicon', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150731152341_902.jpeg', 'firenze', '', '2015-08-28', '15:23:00', 3, 0, 12, 300, 1, '43.7695604', '11.2558136', 1, '2015-07-31 15:23:41', '2015-07-31 13:24:44', 1),
(365, 182, 8, 'GGG', 'ggbb', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150731152348_5788.jpeg', 'bari', '', '2015-10-01', '15:22:00', 3, 5, 32, 5, 0, '41.1171432', '16.8718715', 1, '2015-07-31 15:23:48', '2015-07-31 13:23:48', 1),
(366, 182, 7, 'VVG', 'gvvg', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'bari', '', '2015-09-11', '16:21:00', 3, 2, 9, 5, 0, '41.1171432', '16.8718715', 1, '2015-07-31 16:22:18', '2015-07-31 14:22:18', 1);
INSERT INTO `events` (`idevent`, `iduser`, `idcategory`, `title`, `description`, `img`, `city`, `address`, `date`, `hour`, `sex`, `agemin`, `agemax`, `sittotal`, `sitcurrent`, `latitude`, `longitude`, `scope`, `creationdate`, `updatetime`, `activated`) VALUES
(367, 182, 8, 'GVVG', 'vvvv', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'bari', '', '2015-08-28', '16:22:00', 0, 5, 9, 5, 0, '41.1171432', '16.8718715', 1, '2015-07-31 16:23:15', '2015-07-31 14:23:15', 1),
(369, 182, 7, 'BB', 'bbh', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150731170114_6083.jpeg', 'bari', '', '2015-11-26', '17:00:00', 3, 7, 44, 5, 0, '41.1171432', '16.8718715', 1, '2015-07-31 17:01:14', '2015-07-31 15:01:14', 1),
(371, 182, 7, ' GVVH', 'gvfbh', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'bari', '', '2015-09-30', '17:16:00', 0, 5, 9, 5, 0, '41.1171432', '16.8718715', 1, '2015-07-31 17:16:53', '2015-07-31 15:16:53', 1),
(372, 182, 5, 'HGGGFFV', 'vgchh', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150731171827_5343.jpeg', 'bari', '', '2015-11-12', '17:18:00', 3, 26, 46, 5, 0, '41.1171432', '16.8718715', 1, '2015-07-31 17:18:27', '2015-07-31 15:18:27', 1),
(373, 182, 7, 'BUVU', 'vvgg', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150731172154_7658.jpeg', 'bari', '', '2015-12-16', '17:21:00', 3, 6, 35, 5, 0, '41.1171432', '16.8718715', 1, '2015-07-31 17:21:54', '2015-07-31 15:21:54', 1),
(374, 272, 69, 'PARTITA DI SPACE QUEST', 'partita amatoriale con il nonno di warhammer. #retrotabletop', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150805172029_2007.jpeg', 'bologna', '', '2015-08-05', '20:15:00', 3, 16, 100, 4, 0, '44.4913915', '11.3399941', 0, '2015-08-01 22:15:06', '2015-08-05 15:20:29', 1),
(375, 182, 7, 'TEST', 'Test', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150803121133_4112.jpeg', 'Bari', '', '2015-08-21', '12:06:00', 3, 0, 23, 5, 0, '41.1171432', '16.8718715', 1, '2015-08-03 12:11:33', '2015-08-03 10:11:33', 1),
(376, 182, 8, 'HDHD', 'Hzha', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150803124158_344.jpeg', 'Bari', '', '2015-08-20', '12:40:00', 1, 0, 12, 5, 0, '41.1171432', '16.8718715', 1, '2015-08-03 12:41:58', '2015-08-03 10:41:58', 1),
(378, 200, 10, 'RFRRFRRF', 'rf', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150805161933_2728.jpeg', 'bari', '', '2015-08-08', '16:18:00', 3, 100, 100, 100, 1, '41.1171432', '16.8718715', 1, '2015-08-05 16:19:33', '2015-08-05 15:02:34', 1),
(379, 243, 59, 'SPETTACOLO ALBERO DELLA VITA IN EXPO!', 'Ciao a tutti! Vorrei andare in Expo in orario serale a fare un giro per i padiglioni, ma soprattutto vorrei finalmente vedere lo spettacolo dell&#039;accensione dell&#039;albero della vita! \n\nCi si ritrova in Duomo, per i dettagli sentiamoci via chat e accordiamoci per la soluzione più comoda per tutti!', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150812100436_7281.jpeg', 'Milano', '', '2015-09-09', '18:00:00', 3, 25, 45, 4, 4, '45.464567', '9.188508', 0, '2015-08-12 10:04:36', '2015-08-18 15:00:08', 1),
(380, 287, 58, 'FOTO DI UNA NOTTE DI MEZZ&#039;ESTATE', 'Immaginate il buio di una spiaggia deserta...il cielo stellato....il rumore del mare....ora provate ad immaginare che il buio venga illuminato da piccole stelle incendiate....tutto prende luce....I contorni prendono il colore del fuoco....ora provate ad immaginare di poter catturare con le vostre reflex tutto questo....bene ecco cosa vi propongo...una serata di suggestione e condivisione fotografica.... l&#039;invito è rivolto a tutti gli appassionati di fotografia che siano neofiti o professionisti.... bisogna solo essere muniti di reflex, treppiedi e taaaaanta voglia di condividere le proprie conoscenze.... ', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150812115007_2757.jpeg', 'catania', 'Playa', '2015-08-18', '20:45:00', 3, 16, 80, 10, 0, '37.4852388', '15.0873962', 0, '2015-08-12 11:49:03', '2015-08-12 09:50:07', 1),
(382, 285, 32, 'BOOK/MOVIE CLUB: &quot;IL PROFUMO&quot; DI P. SÜSKIND', 'Primo ma spero non ultimo appuntamento per parlare insieme di libri e film letti di comune accordo, parlare dell&#039;autore, di opere affini, delle nostre impressioni. Ottima occasione per scoprire anche dei bei locali tranquilli nella caotica Milano.\nSceglierò sempre film che abbiano anche una versione cinematografica: potete scegliere la versione che più vi piace o perché no, anche entrambi!\n\nCi vediamo il 15 settembre (un mese abbondante per la lettura/visione sotto l&#039;ombrellone) alle 18.30 alla Creperia/Caffè Vecchia Brera, comoda da Cairoli, Montenapoleone e Lanza (rossa, gialla e verde: non avete scuse!). Se avete diverse eigenze di orario fatemi sapere!', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150812121329_9688.jpeg', 'Milano', 'via dell&#039;orso 20', '2015-09-15', '18:30:00', 3, 18, 60, 9, 2, '45.4690993', '9.1856081', 0, '2015-08-12 12:13:29', '2015-08-17 19:26:48', 1),
(384, 91, 58, 'MILANO PHOTO TOUR', 'Trascorrere le ferie a Milano non e&#039; certo un piacere ma offre qualche vantaggio: l&#039;occasione di vivere la città senza una cortina di fumo e smog o, in assenza dei due, di nebbia! Il momento migliore per catturare alcuni scorci della città nel suo splendore. Al tramonto ed oltre, magari con la cornice di un cielo stellato tra i vicoli più bui. Propongo un&#039;uscita fotografica attrezzati di reflex e cavalleto. Ho con me un Canon 24/70 f. 2.8 L ed un Sigma 12-24 f. 4.5 II DG HSM...casomai qualcuno fosse interessato a provare queste ottiche!\n\nChe ne dite? Potremmo iniziare dalla zona di Porta Nuova, poi vediamo..      \n\nFatemi sapere! ', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150812143414_8510.jpeg', 'milano', '', '2015-08-27', '18:00:00', 3, 25, 50, 5, 3, '45.4797726', '9.19123291', 0, '2015-08-12 14:31:01', '2015-08-23 14:28:42', 1),
(385, 299, 22, 'EGO DRONEMUSIC LIVE / DJ SET @ CINEMA MELIES, CONEGLIANO.', 'Aperitivo e serata in musica con Ego Dronemusic al Cinema Georges Melies di Conegliano! Musica elettronica e sperimentale, per fan di Kraftwerk, Aphex Twin, Fuck Buttons e tutti i gruppi della Warp Records. A seguire, dj set retro-trance, per fan dei vecchi dischi della Bonzai Music. Ingresso libero, consumazioni non obbligatorie a prezzi popolari. Dalle ore 8.30 alla mezzanotte!', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150813180053_6299.jpeg', 'Conegliano Veneto', 'Cinema Georges Melies', '2015-08-29', '20:30:00', 3, 15, 99, 999, 2, '45.8862501', '12.3052088', 0, '2015-08-13 17:55:49', '2015-08-17 11:00:36', 1),
(388, 316, 42, 'ANGELSDANCING TEAM DIABLO PA', ' inaugurazione succursale angelsdancing Palermo team Diablo ', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'Palermo ', '', '2015-09-08', '21:00:00', 3, 0, 85, 500, 1, '38.1156879', '13.3612671', 0, '2015-08-19 03:38:14', '2015-08-19 01:43:25', 1),
(389, 391, 53, 'TEST', 'test', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/default.jpg', 'cecina', '', '2015-09-26', '08:00:00', 3, 20, 50, 5, 0, '43.30853', '10.5188593', 1, '2015-08-23 13:25:41', '2015-08-23 11:25:41', 1),
(390, 392, 11, 'SHOT &amp; DRINK', 'Cocktail Party , con un menù al quanto invitante accompagnato da musica a 360&#039; (Latino Regaetton Dembow , Dance 90, Evergreen, e le hit più belle del momento) . \nIl Cocktail Party si svolgerà presso Bar Comunale sito Via Rozzi 13 (Teramo)', 'http://www.join-me.it/web-service/dev/app/webroot/images/event/20150823143806_4132.jpeg', 'Teramo', 'Via Rozzi 13', '2015-08-29', '20:30:00', 3, 20, 40, 70, 1, '42.6606887', '13.6986104', 0, '2015-08-23 14:38:06', '2015-08-23 13:13:15', 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `event_invitations`
--

CREATE TABLE IF NOT EXISTS `event_invitations` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'pk',
  `idsender` int(11) NOT NULL COMMENT 'pk utente che ha inoltrato l''invito',
  `idreciver` int(11) NOT NULL COMMENT 'pk destinatario invito',
  `idevent` int(11) NOT NULL COMMENT 'pk evento',
  `creation_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'data creazione istanza',
  PRIMARY KEY (`id`),
  KEY `fk_sender_user` (`idsender`),
  KEY `pk_receiver_index` (`idreciver`),
  KEY `pk_event_index` (`idevent`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=374 ;

--
-- Dump dei dati per la tabella `event_invitations`
--

INSERT INTO `event_invitations` (`id`, `idsender`, `idreciver`, `idevent`, `creation_time`) VALUES
(1, 66, 71, 2, '2015-03-28 09:02:04'),
(2, 71, 66, 5, '2015-03-28 09:57:23'),
(3, 12, 1, 11, '2015-03-31 10:28:16'),
(4, 12, 8, 11, '2015-03-31 10:28:16'),
(5, 12, 10, 11, '2015-03-31 10:28:16'),
(6, 12, 14, 11, '2015-03-31 10:28:17'),
(7, 12, 19, 11, '2015-03-31 10:28:17'),
(8, 12, 71, 11, '2015-03-31 10:28:18'),
(9, 12, 14, 13, '2015-03-31 10:34:36'),
(10, 12, 10, 13, '2015-03-31 10:34:36'),
(11, 12, 71, 13, '2015-03-31 10:34:37'),
(12, 12, 8, 13, '2015-03-31 10:34:37'),
(13, 12, 1, 13, '2015-03-31 10:34:37'),
(14, 12, 90, 13, '2015-03-31 10:34:37'),
(15, 12, 19, 13, '2015-03-31 10:34:38'),
(16, 71, 90, 12, '2015-03-31 10:50:05'),
(17, 93, 94, 3, '2015-03-31 11:49:18'),
(18, 93, 94, 17, '2015-03-31 11:50:08'),
(19, 12, 10, 18, '2015-03-31 11:55:54'),
(20, 12, 71, 18, '2015-03-31 11:55:55'),
(21, 91, 12, 15, '2015-03-31 13:07:39'),
(22, 91, 83, 15, '2015-03-31 13:07:40'),
(23, 91, 71, 15, '2015-03-31 13:07:40'),
(24, 91, 12, 20, '2015-03-31 13:08:30'),
(25, 91, 71, 20, '2015-03-31 13:08:43'),
(26, 91, 83, 20, '2015-03-31 13:08:46'),
(27, 91, 83, 24, '2015-04-01 06:17:19'),
(28, 91, 71, 24, '2015-04-01 06:17:19'),
(29, 91, 12, 24, '2015-04-01 06:17:20'),
(30, 101, 134, 74, '2015-04-10 13:04:08'),
(31, 101, 134, 35, '2015-04-10 13:07:25'),
(32, 101, 134, 24, '2015-04-10 13:47:31'),
(33, 101, 134, 20, '2015-04-10 14:07:44'),
(34, 101, 134, 69, '2015-04-10 14:34:51'),
(35, 101, 107, 69, '2015-04-10 14:35:39'),
(36, 101, 133, 69, '2015-04-10 14:35:49'),
(37, 12, 1, 100, '2015-04-10 21:21:19'),
(38, 12, 14, 100, '2015-04-10 21:21:19'),
(39, 12, 71, 100, '2015-04-10 21:21:20'),
(40, 12, 1, 50, '2015-04-12 16:36:43'),
(41, 12, 8, 50, '2015-04-12 16:36:43'),
(42, 12, 10, 50, '2015-04-12 16:36:43'),
(43, 12, 14, 50, '2015-04-12 16:36:44'),
(44, 12, 71, 50, '2015-04-12 16:36:44'),
(45, 12, 90, 50, '2015-04-12 16:36:44'),
(46, 12, 91, 50, '2015-04-12 16:36:45'),
(47, 140, 71, 89, '2015-04-14 09:31:18'),
(48, 71, 144, 35, '2015-04-15 10:12:50'),
(49, 71, 144, 94, '2015-04-15 10:41:07'),
(50, 71, 144, 109, '2015-04-15 12:46:46'),
(51, 71, 144, 108, '2015-04-15 12:48:00'),
(52, 71, 144, 101, '2015-04-15 13:18:31'),
(53, 71, 144, 25, '2015-04-15 13:19:09'),
(54, 71, 144, 93, '2015-04-15 13:23:13'),
(55, 71, 144, 63, '2015-04-15 13:56:56'),
(56, 71, 144, 77, '2015-04-15 13:57:51'),
(57, 71, 144, 81, '2015-04-15 14:00:40'),
(58, 71, 144, 100, '2015-04-15 14:01:18'),
(59, 71, 144, 97, '2015-04-16 08:17:51'),
(60, 71, 140, 97, '2015-04-16 08:18:29'),
(61, 71, 140, 79, '2015-04-16 14:55:36'),
(62, 71, 140, 92, '2015-04-16 15:18:47'),
(63, 71, 140, 56, '2015-04-16 15:25:09'),
(64, 71, 140, 32, '2015-04-16 15:25:43'),
(65, 71, 140, 55, '2015-04-17 14:23:43'),
(66, 158, 164, 76, '2015-04-22 15:29:42'),
(67, 164, 158, 79, '2015-04-22 15:30:23'),
(68, 12, 91, 111, '2015-05-04 12:01:43'),
(69, 12, 1, 111, '2015-05-04 12:01:43'),
(70, 12, 10, 111, '2015-05-04 12:01:44'),
(71, 12, 71, 111, '2015-05-04 12:01:44'),
(72, 12, 14, 111, '2015-05-04 13:59:10'),
(73, 12, 8, 111, '2015-05-04 13:59:10'),
(74, 12, 90, 111, '2015-05-04 13:59:19'),
(75, 12, 8, 115, '2015-05-04 15:00:35'),
(76, 12, 1, 115, '2015-05-04 15:00:35'),
(77, 12, 91, 115, '2015-05-04 15:00:36'),
(78, 12, 14, 115, '2015-05-04 15:00:36'),
(79, 12, 71, 115, '2015-05-04 15:00:36'),
(80, 12, 10, 115, '2015-05-04 15:00:36'),
(81, 12, 90, 115, '2015-05-04 15:00:37'),
(82, 12, 8, 112, '2015-05-04 15:01:47'),
(83, 12, 1, 112, '2015-05-04 15:01:47'),
(84, 12, 91, 112, '2015-05-04 15:01:48'),
(85, 12, 14, 112, '2015-05-04 15:01:48'),
(86, 12, 71, 112, '2015-05-04 15:01:48'),
(87, 12, 10, 112, '2015-05-04 15:01:48'),
(88, 12, 90, 112, '2015-05-04 15:01:48'),
(89, 12, 131, 113, '2015-05-04 15:03:26'),
(90, 12, 131, 111, '2015-05-04 15:07:05'),
(91, 12, 8, 122, '2015-05-05 11:21:31'),
(92, 12, 1, 122, '2015-05-05 11:21:32'),
(93, 12, 131, 122, '2015-05-05 11:21:33'),
(94, 12, 91, 122, '2015-05-05 11:21:34'),
(95, 12, 14, 122, '2015-05-05 11:21:35'),
(96, 12, 10, 122, '2015-05-05 11:21:36'),
(97, 12, 71, 122, '2015-05-05 11:21:38'),
(98, 12, 90, 122, '2015-05-05 11:21:39'),
(99, 12, 168, 129, '2015-05-08 09:45:11'),
(100, 12, 8, 129, '2015-05-08 09:45:11'),
(101, 91, 12, 130, '2015-05-11 10:23:46'),
(102, 91, 71, 130, '2015-05-11 10:23:46'),
(103, 91, 168, 134, '2015-05-11 10:37:59'),
(104, 91, 12, 134, '2015-05-11 10:38:00'),
(105, 91, 71, 134, '2015-05-11 10:38:00'),
(106, 91, 12, 136, '2015-05-11 12:52:12'),
(107, 91, 168, 136, '2015-05-11 12:52:13'),
(108, 91, 71, 136, '2015-05-11 12:52:14'),
(109, 168, 12, 140, '2015-05-12 07:33:46'),
(110, 12, 91, 141, '2015-05-12 10:23:23'),
(111, 12, 1, 141, '2015-05-12 10:23:23'),
(112, 91, 12, 142, '2015-05-12 16:19:41'),
(113, 91, 71, 142, '2015-05-12 16:19:45'),
(114, 91, 168, 142, '2015-05-12 16:19:48'),
(115, 91, 12, 147, '2015-05-12 16:31:29'),
(116, 91, 71, 147, '2015-05-12 16:31:30'),
(117, 91, 168, 147, '2015-05-12 16:31:30'),
(118, 91, 12, 151, '2015-05-13 17:30:14'),
(119, 91, 71, 151, '2015-05-13 17:30:14'),
(120, 91, 168, 151, '2015-05-13 17:30:15'),
(121, 91, 12, 157, '2015-05-13 20:03:10'),
(122, 91, 71, 157, '2015-05-13 20:03:10'),
(123, 91, 168, 157, '2015-05-13 20:03:11'),
(124, 142, 177, 158, '2015-05-14 08:12:26'),
(125, 91, 71, 111, '2015-05-14 08:41:13'),
(126, 91, 168, 111, '2015-05-14 08:41:13'),
(127, 91, 168, 130, '2015-05-14 09:07:19'),
(128, 12, 14, 136, '2015-05-17 11:59:57'),
(129, 12, 131, 136, '2015-05-17 11:59:58'),
(130, 12, 168, 136, '2015-05-17 12:00:00'),
(131, 91, 12, 153, '2015-05-17 16:05:28'),
(132, 91, 71, 153, '2015-05-17 16:05:28'),
(133, 91, 101, 153, '2015-05-17 16:05:28'),
(134, 91, 168, 153, '2015-05-17 16:05:29'),
(135, 91, 12, 155, '2015-05-18 12:27:29'),
(136, 91, 71, 155, '2015-05-18 12:27:29'),
(137, 91, 168, 155, '2015-05-18 12:27:30'),
(138, 12, 1, 162, '2015-05-19 09:12:45'),
(139, 12, 10, 162, '2015-05-19 09:12:46'),
(140, 12, 142, 162, '2015-05-19 09:12:47'),
(141, 131, 12, 161, '2015-05-19 10:41:05'),
(142, 131, 135, 161, '2015-05-19 10:41:05'),
(143, 131, 91, 161, '2015-05-19 10:41:06'),
(144, 131, 71, 161, '2015-05-19 10:41:06'),
(145, 131, 142, 161, '2015-05-19 10:41:07'),
(146, 131, 101, 161, '2015-05-19 10:41:07'),
(147, 131, 140, 161, '2015-05-19 10:41:08'),
(148, 131, 12, 133, '2015-05-19 10:41:45'),
(149, 131, 135, 133, '2015-05-19 10:41:45'),
(150, 131, 91, 133, '2015-05-19 10:41:45'),
(151, 131, 71, 133, '2015-05-19 10:41:46'),
(152, 131, 142, 133, '2015-05-19 10:41:47'),
(153, 131, 101, 133, '2015-05-19 10:41:47'),
(154, 131, 140, 133, '2015-05-19 10:41:48'),
(155, 131, 12, 148, '2015-05-19 10:57:25'),
(156, 131, 135, 148, '2015-05-19 10:57:25'),
(157, 131, 71, 148, '2015-05-19 10:57:25'),
(158, 131, 179, 148, '2015-05-19 10:57:26'),
(159, 131, 142, 148, '2015-05-19 10:57:27'),
(160, 131, 140, 148, '2015-05-19 10:57:27'),
(161, 131, 101, 148, '2015-05-19 10:57:28'),
(162, 12, 168, 165, '2015-05-19 13:38:29'),
(163, 131, 179, 174, '2015-05-20 09:39:48'),
(164, 131, 142, 174, '2015-05-20 09:39:49'),
(165, 12, 177, 165, '2015-05-20 13:28:14'),
(166, 12, 142, 165, '2015-05-20 13:28:15'),
(167, 12, 131, 165, '2015-05-20 13:28:15'),
(168, 168, 91, 162, '2015-05-21 11:00:55'),
(169, 12, 91, 175, '2015-05-25 09:09:42'),
(170, 12, 168, 175, '2015-05-25 09:09:43'),
(171, 12, 142, 175, '2015-05-25 09:09:51'),
(172, 131, 179, 155, '2015-05-26 11:04:05'),
(173, 131, 168, 155, '2015-05-26 11:04:06'),
(174, 131, 142, 155, '2015-05-26 11:04:07'),
(175, 131, 140, 155, '2015-05-26 11:04:09'),
(176, 131, 101, 155, '2015-05-26 11:04:09'),
(177, 131, 182, 155, '2015-05-26 11:04:10'),
(178, 131, 71, 155, '2015-05-26 11:04:10'),
(179, 131, 135, 155, '2015-05-26 11:04:11'),
(180, 131, 163, 155, '2015-05-26 11:04:12'),
(181, 131, 12, 155, '2015-05-26 11:04:12'),
(182, 12, 8, 161, '2015-05-26 11:05:31'),
(183, 12, 71, 161, '2015-05-26 11:05:31'),
(184, 12, 168, 161, '2015-05-26 11:05:32'),
(185, 12, 142, 161, '2015-05-26 11:05:33'),
(186, 12, 131, 141, '2015-05-26 11:06:56'),
(187, 12, 168, 141, '2015-05-26 11:06:57'),
(188, 168, 91, 161, '2015-05-26 11:09:06'),
(189, 91, 12, 222, '2015-06-10 07:40:57'),
(190, 91, 71, 222, '2015-06-10 07:41:01'),
(191, 91, 168, 222, '2015-06-10 11:02:59'),
(192, 168, 91, 229, '2015-06-11 09:29:33'),
(193, 12, 91, 229, '2015-06-11 15:17:44'),
(194, 12, 131, 229, '2015-06-11 15:17:45'),
(195, 91, 101, 222, '2015-06-12 17:01:32'),
(196, 91, 71, 229, '2015-06-12 17:02:16'),
(197, 91, 168, 229, '2015-06-12 17:02:17'),
(198, 142, 131, 230, '2015-06-16 10:05:38'),
(199, 142, 177, 230, '2015-06-16 10:05:39'),
(200, 142, 182, 230, '2015-06-16 10:08:45'),
(201, 12, 91, 230, '2015-06-16 10:45:35'),
(202, 12, 1, 160, '2015-06-16 12:52:12'),
(203, 12, 10, 160, '2015-06-16 12:52:12'),
(204, 12, 142, 160, '2015-06-16 12:52:12'),
(205, 12, 90, 160, '2015-06-16 12:52:13'),
(206, 142, 131, 160, '2015-06-16 12:53:49'),
(207, 142, 182, 160, '2015-06-16 12:53:50'),
(208, 142, 177, 160, '2015-06-16 12:53:50'),
(209, 12, 8, 239, '2015-06-16 13:22:39'),
(210, 12, 1, 239, '2015-06-16 13:22:39'),
(211, 12, 131, 239, '2015-06-16 13:22:40'),
(212, 12, 90, 239, '2015-06-16 13:22:40'),
(213, 12, 142, 239, '2015-06-16 13:22:41'),
(214, 12, 168, 239, '2015-06-16 13:22:41'),
(215, 12, 177, 239, '2015-06-16 13:22:42'),
(216, 12, 71, 239, '2015-06-16 13:22:42'),
(217, 12, 169, 239, '2015-06-16 13:22:42'),
(218, 12, 10, 239, '2015-06-16 13:22:42'),
(219, 12, 91, 239, '2015-06-16 13:22:43'),
(220, 91, 188, 239, '2015-06-16 14:32:27'),
(221, 142, 182, 242, '2015-06-17 13:25:29'),
(222, 182, 142, 187, '2015-06-17 13:34:01'),
(223, 182, 142, 230, '2015-06-17 13:35:40'),
(224, 182, 131, 230, '2015-06-17 13:35:40'),
(225, 182, 142, 240, '2015-06-18 10:34:13'),
(229, 142, 182, 244, '2015-06-18 10:38:23'),
(230, 182, 131, 239, '2015-06-18 10:46:34'),
(231, 182, 142, 239, '2015-06-18 10:46:35'),
(232, 194, 12, 257, '2015-06-26 16:27:10'),
(233, 12, 91, 257, '2015-06-26 17:00:54'),
(234, 12, 177, 257, '2015-06-26 17:12:53'),
(235, 91, 194, 246, '2015-06-27 06:32:52'),
(236, 91, 188, 246, '2015-06-27 06:32:52'),
(237, 197, 198, 259, '2015-06-29 08:21:56'),
(238, 197, 198, 260, '2015-06-29 08:41:46'),
(239, 197, 198, 261, '2015-06-29 08:45:11'),
(240, 198, 197, 240, '2015-06-29 08:46:41'),
(242, 195, 91, 251, '2015-06-30 06:13:26'),
(243, 197, 199, 255, '2015-06-30 07:43:47'),
(244, 199, 197, 255, '2015-06-30 07:44:20'),
(246, 199, 197, 245, '2015-06-30 07:45:18'),
(247, 197, 199, 256, '2015-06-30 07:46:20'),
(248, 197, 199, 263, '2015-06-30 08:23:15'),
(249, 197, 199, 253, '2015-06-30 08:24:03'),
(250, 197, 199, 246, '2015-06-30 08:24:35'),
(251, 197, 199, 248, '2015-06-30 08:25:19'),
(252, 195, 91, 268, '2015-07-05 14:29:26'),
(253, 195, 91, 269, '2015-07-05 14:36:24'),
(254, 165, 12, 241, '2015-07-10 10:25:18'),
(255, 12, 165, 270, '2015-07-10 13:06:50'),
(256, 12, 91, 270, '2015-07-10 13:06:51'),
(257, 12, 194, 270, '2015-07-10 13:06:52'),
(258, 165, 12, 252, '2015-07-16 15:03:24'),
(259, 165, 12, 284, '2015-07-16 15:17:34'),
(260, 165, 91, 284, '2015-07-16 15:17:35'),
(261, 165, 12, 256, '2015-07-16 15:21:07'),
(262, 165, 91, 256, '2015-07-16 15:21:07'),
(264, 165, 12, 272, '2015-07-16 17:38:11'),
(265, 165, 91, 272, '2015-07-16 17:38:12'),
(266, 165, 213, 252, '2015-07-17 12:16:59'),
(267, 165, 91, 293, '2015-07-17 12:21:27'),
(268, 165, 213, 293, '2015-07-17 12:21:28'),
(269, 165, 12, 293, '2015-07-17 12:21:34'),
(270, 165, 213, 251, '2015-07-17 13:20:28'),
(271, 165, 214, 251, '2015-07-17 13:20:28'),
(272, 165, 213, 264, '2015-07-17 15:47:31'),
(273, 165, 91, 264, '2015-07-17 15:47:32'),
(274, 165, 214, 264, '2015-07-17 15:47:33'),
(275, 214, 213, 241, '2015-07-20 10:33:07'),
(276, 163, 182, 264, '2015-07-20 13:33:13'),
(277, 182, 163, 264, '2015-07-20 14:42:29'),
(278, 182, 131, 264, '2015-07-20 14:42:30'),
(279, 182, 142, 264, '2015-07-20 14:42:31'),
(280, 163, 182, 282, '2015-07-20 15:26:56'),
(281, 163, 214, 282, '2015-07-20 15:26:57'),
(282, 163, 131, 282, '2015-07-20 15:26:58'),
(283, 163, 12, 282, '2015-07-20 15:26:59'),
(284, 91, 12, 297, '2015-07-20 17:10:28'),
(285, 91, 165, 297, '2015-07-20 17:10:29'),
(286, 182, 142, 308, '2015-07-25 05:20:21'),
(287, 182, 131, 308, '2015-07-25 05:20:22'),
(288, 182, 163, 308, '2015-07-25 05:20:23'),
(289, 194, 12, 315, '2015-07-26 12:04:12'),
(290, 194, 165, 315, '2015-07-26 12:04:13'),
(291, 194, 91, 315, '2015-07-26 12:04:14'),
(292, 194, 163, 315, '2015-07-26 12:04:14'),
(293, 165, 12, 316, '2015-07-26 20:32:05'),
(294, 165, 194, 316, '2015-07-26 20:32:06'),
(295, 165, 217, 316, '2015-07-26 20:32:06'),
(296, 165, 214, 316, '2015-07-26 20:32:07'),
(297, 165, 91, 316, '2015-07-26 20:32:08'),
(298, 165, 163, 316, '2015-07-26 21:10:30'),
(299, 165, 213, 316, '2015-07-26 21:10:30'),
(302, 165, 12, 324, '2015-07-28 11:51:40'),
(303, 165, 213, 324, '2015-07-28 11:51:41'),
(304, 165, 217, 324, '2015-07-28 11:51:42'),
(305, 165, 214, 324, '2015-07-28 11:52:34'),
(306, 165, 194, 324, '2015-07-28 11:59:12'),
(307, 165, 163, 324, '2015-07-28 11:59:13'),
(308, 165, 91, 324, '2015-07-28 11:59:14'),
(309, 165, 205, 324, '2015-07-28 11:59:14'),
(310, 182, 198, 324, '2015-07-28 12:29:16'),
(311, 182, 142, 324, '2015-07-28 12:29:29'),
(312, 182, 224, 324, '2015-07-28 12:30:01'),
(313, 182, 131, 324, '2015-07-28 12:30:58'),
(314, 182, 163, 324, '2015-07-28 12:32:09'),
(315, 12, 91, 324, '2015-07-28 12:53:32'),
(316, 12, 194, 324, '2015-07-28 12:53:34'),
(317, 12, 177, 324, '2015-07-28 12:53:34'),
(318, 194, 12, 326, '2015-07-28 13:52:48'),
(319, 194, 91, 326, '2015-07-28 13:52:48'),
(320, 165, 217, 248, '2015-07-28 14:39:23'),
(323, 182, 223, 326, '2015-07-29 05:56:23'),
(324, 194, 252, 326, '2015-07-29 07:10:30'),
(325, 163, 182, 326, '2015-07-29 08:57:55'),
(326, 163, 182, 324, '2015-07-29 08:58:21'),
(327, 163, 182, 331, '2015-07-29 09:17:50'),
(328, 131, 163, 299, '2015-07-29 10:12:13'),
(329, 131, 135, 299, '2015-07-29 10:12:13'),
(330, 131, 182, 299, '2015-07-29 10:12:14'),
(331, 131, 179, 299, '2015-07-29 10:12:14'),
(332, 131, 142, 299, '2015-07-29 10:12:15'),
(333, 131, 101, 299, '2015-07-29 10:12:15'),
(334, 131, 140, 299, '2015-07-29 10:12:16'),
(335, 228, 182, 324, '2015-07-30 10:37:11'),
(336, 182, 228, 343, '2015-07-30 10:38:02'),
(337, 182, 228, 341, '2015-07-30 10:38:49'),
(338, 182, 228, 347, '2015-07-30 10:39:58'),
(339, 182, 228, 340, '2015-07-30 10:40:34'),
(340, 182, 228, 344, '2015-07-30 10:46:53'),
(341, 182, 228, 345, '2015-07-30 10:47:55'),
(342, 182, 228, 342, '2015-07-30 10:54:10'),
(343, 182, 228, 348, '2015-07-30 10:56:36'),
(344, 228, 182, 326, '2015-07-30 11:15:35'),
(345, 182, 228, 349, '2015-07-30 11:21:07'),
(346, 182, 228, 350, '2015-07-30 11:21:31'),
(347, 182, 163, 341, '2015-07-30 16:52:20'),
(348, 182, 163, 347, '2015-07-30 16:55:49'),
(349, 182, 163, 349, '2015-07-30 18:03:48'),
(350, 182, 163, 343, '2015-07-30 18:04:37'),
(351, 163, 182, 353, '2015-07-30 18:07:58'),
(352, 163, 182, 354, '2015-07-30 18:14:05'),
(353, 182, 163, 354, '2015-07-30 18:15:02'),
(354, 131, 182, 364, '2015-07-31 13:24:56'),
(355, 131, 182, 363, '2015-07-31 13:30:50'),
(356, 131, 182, 326, '2015-07-31 13:31:42'),
(357, 163, 182, 349, '2015-07-31 14:02:45'),
(359, 12, 241, 385, '2015-08-18 13:35:21'),
(360, 12, 194, 385, '2015-08-18 13:35:22'),
(361, 12, 314, 385, '2015-08-18 13:35:23'),
(362, 12, 217, 385, '2015-08-18 13:35:24'),
(363, 12, 165, 385, '2015-08-18 13:35:24'),
(364, 12, 168, 385, '2015-08-18 13:35:25'),
(365, 12, 252, 385, '2015-08-18 13:35:25'),
(366, 165, 239, 385, '2015-08-19 20:35:46'),
(367, 165, 194, 385, '2015-08-19 20:35:46'),
(368, 165, 314, 385, '2015-08-19 20:35:47'),
(369, 165, 217, 385, '2015-08-19 20:35:48'),
(370, 165, 205, 385, '2015-08-19 20:35:50'),
(371, 165, 252, 385, '2015-08-19 20:35:51'),
(372, 165, 258, 385, '2015-08-19 20:35:51'),
(373, 142, 182, 384, '2015-08-20 12:37:48');

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `follow_created_events`
--
CREATE TABLE IF NOT EXISTS `follow_created_events` (
`img` varchar(128)
,`idevent` int(11)
,`sex` int(1)
,`sittotal` int(3)
,`sitcurrent` int(3)
,`idcategory` int(11)
,`title` varchar(100)
,`description` varchar(1000)
,`city` varchar(45)
,`address` varchar(45)
,`agemin` int(2)
,`agemax` int(2)
,`date` date
,`hour` time
,`scope` int(11)
,`latitude` varchar(10)
,`longitude` varchar(10)
,`userimg` varchar(128)
,`job` int(3)
,`iduser` int(11)
,`idselecteduser` int(11)
);
-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `friend_created_events`
--
CREATE TABLE IF NOT EXISTS `friend_created_events` (
`idevent` int(11)
,`sex` int(1)
,`sittotal` int(3)
,`sitcurrent` int(3)
,`idcategory` int(11)
,`title` varchar(100)
,`description` varchar(1000)
,`city` varchar(45)
,`address` varchar(45)
,`agemin` int(2)
,`agemax` int(2)
,`date` date
,`hour` time
,`scope` int(11)
,`latitude` varchar(10)
,`longitude` varchar(10)
,`img` varchar(128)
,`userimg` varchar(128)
,`job` int(3)
,`iduser` int(11)
,`idselecteduser` int(11)
);
-- --------------------------------------------------------

--
-- Struttura della tabella `jobs`
--

CREATE TABLE IF NOT EXISTS `jobs` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) NOT NULL,
  `language` varchar(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=34 ;

--
-- Dump dei dati per la tabella `jobs`
--

INSERT INTO `jobs` (`id`, `name`, `language`) VALUES
(0, 'Area affari legali & finanza', 'en_GB'),
(1, 'Area agricoltura & enologia', 'en_GB'),
(2, 'Area amministrazione & contabilità', 'en_GB'),
(3, 'Area edilizia & immobiliare', 'en_GB'),
(4, 'Area editoria & media', 'en_GB'),
(5, 'Area informatica & telecomunicazioni', 'en_GB'),
(6, 'Area industria & artigianato', 'en_GB'),
(7, 'Area intrattenimento & spettacolo', 'en_GB'),
(8, 'Area istruzione & formazione', 'en_GB'),
(9, 'Area medicina & sanità', 'en_GB'),
(10, 'Area marketing & comunicazione', 'en_GB'),
(11, 'Area trasporti & servizi', 'en_GB'),
(12, 'Area turismo & ristorazione', 'en_GB'),
(13, 'Area vendite & commercio', 'en_GB'),
(14, 'Architetto', 'en_GB'),
(15, 'Artigiano', 'en_GB'),
(16, 'Artista', 'en_GB'),
(17, 'Avvocato', 'en_GB'),
(18, 'Imprenditore', 'en_GB'),
(19, 'Ingegnere', 'en_GB'),
(20, 'Insegnante', 'en_GB'),
(21, 'Medico', 'en_GB'),
(22, 'Manager', 'en_GB'),
(23, 'Militare', 'en_GB'),
(24, 'PR', 'en_GB'),
(25, 'Studente', 'en_GB'),
(26, 'Tecnico specializzato', 'en_GB'),
(27, 'Altro', 'en_GB');

-- --------------------------------------------------------

--
-- Struttura della tabella `notifications`
--

CREATE TABLE IF NOT EXISTS `notifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `type` int(1) NOT NULL DEFAULT '0',
  `idsender` int(11) NOT NULL,
  `idreceiver` int(11) NOT NULL,
  `idevent` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3752 ;

--
-- Dump dei dati per la tabella `notifications`
--

INSERT INTO `notifications` (`id`, `date`, `type`, `idsender`, `idreceiver`, `idevent`) VALUES
(1, '2015-03-28 08:58:55', 0, 71, 66, 1),
(4, '2015-03-28 09:01:15', 0, 71, 66, 2),
(3, '2015-03-28 08:59:31', 6, 71, 66, 1),
(11, '2015-03-28 09:04:14', 7, 71, 66, NULL),
(2101, '2015-07-06 12:53:48', 0, 12, 169, 270),
(7, '2015-03-28 09:02:40', 0, 71, 66, 3),
(10, '2015-03-28 09:03:35', 6, 71, 66, 2),
(9, '2015-03-28 09:03:34', 6, 71, 66, 3),
(12, '2015-03-28 09:04:27', 7, 71, 66, NULL),
(13, '2015-03-28 09:06:55', 0, 71, 66, 4),
(14, '2015-03-28 09:08:36', 0, 71, 66, 5),
(15, '2015-03-28 09:57:23', 2, 71, 66, 5),
(18, '2015-03-28 09:58:15', 7, 71, 66, NULL),
(17, '2015-03-28 09:57:43', 6, 71, 66, 5),
(21, '2015-03-30 14:25:40', 7, 80, 79, NULL),
(20, '2015-03-30 14:25:13', 4, 79, 80, NULL),
(22, '2015-03-30 14:27:05', 7, 79, 80, NULL),
(23, '2015-03-30 14:27:16', 7, 80, 79, NULL),
(24, '2015-03-30 14:27:26', 7, 80, 79, NULL),
(25, '2015-03-30 14:27:29', 7, 79, 80, NULL),
(26, '2015-03-30 14:28:48', 7, 79, 80, NULL),
(27, '2015-03-30 14:28:56', 7, 80, 79, NULL),
(28, '2015-03-30 14:30:29', 7, 79, 80, NULL),
(29, '2015-03-30 14:30:40', 7, 79, 80, NULL),
(30, '2015-03-30 14:30:47', 7, 80, 79, NULL),
(31, '2015-03-30 14:31:55', 7, 80, 79, NULL),
(32, '2015-03-30 14:31:58', 7, 80, 79, NULL),
(33, '2015-03-30 14:32:00', 7, 80, 79, NULL),
(454, '2015-04-11 12:28:39', 7, 71, 101, NULL),
(3159, '2015-07-30 10:23:23', 0, 163, 91, 346),
(383, '2015-04-07 10:08:14', 7, 92, 106, NULL),
(39, '2015-03-30 14:34:45', 7, 71, 79, NULL),
(1401, '2015-05-19 13:36:44', 0, 12, 169, 165),
(794, '2015-04-18 08:41:34', 7, 71, 140, NULL),
(468, '2015-04-11 14:46:08', 0, 101, 133, 102),
(475, '2015-04-11 15:07:37', 0, 101, 134, 103),
(800, '2015-04-18 09:47:52', 7, 140, 71, NULL),
(45, '2015-03-30 14:41:04', 6, 71, 81, 5),
(47, '2015-03-30 14:41:51', 7, 71, 79, NULL),
(48, '2015-03-30 14:41:54', 7, 71, 79, NULL),
(49, '2015-03-30 14:41:56', 7, 71, 79, NULL),
(50, '2015-03-30 14:41:58', 7, 71, 79, NULL),
(51, '2015-03-30 14:42:01', 7, 71, 79, NULL),
(52, '2015-03-30 14:42:01', 4, 71, 81, NULL),
(54, '2015-03-30 14:42:41', 7, 71, 81, NULL),
(55, '2015-03-30 14:42:56', 7, 71, 81, NULL),
(492, '2015-04-11 15:37:18', 7, 136, 101, NULL),
(59, '2015-03-31 08:16:23', 7, 80, 81, NULL),
(58, '2015-03-31 08:15:46', 4, 81, 80, NULL),
(60, '2015-03-31 08:16:43', 7, 81, 80, NULL),
(494, '2015-04-12 16:36:43', 2, 12, 8, 50),
(62, '2015-03-31 08:24:41', 7, 71, 81, NULL),
(63, '2015-03-31 08:35:23', 7, 80, 81, NULL),
(493, '2015-04-12 16:36:43', 2, 12, 1, 50),
(65, '2015-03-31 08:46:15', 4, 71, 80, NULL),
(67, '2015-03-31 08:47:30', 7, 71, 80, NULL),
(495, '2015-04-12 16:36:43', 2, 12, 10, 50),
(69, '2015-03-31 08:54:03', 4, 71, 83, NULL),
(491, '2015-04-11 15:36:25', 4, 101, 136, NULL),
(72, '2015-03-31 08:54:42', 7, 71, 83, NULL),
(73, '2015-03-31 08:54:46', 7, 71, 83, NULL),
(498, '2015-04-12 16:36:44', 2, 12, 90, 50),
(1311, '2015-05-18 12:27:30', 2, 91, 168, 155),
(76, '2015-03-31 08:56:11', 7, 71, 83, NULL),
(77, '2015-03-31 08:56:16', 7, 71, 83, NULL),
(78, '2015-03-31 08:56:33', 7, 71, 83, NULL),
(808, '2015-04-18 09:57:34', 7, 140, 71, NULL),
(80, '2015-03-31 08:58:12', 7, 71, 83, NULL),
(83, '2015-03-31 09:02:19', 7, 80, 83, NULL),
(82, '2015-03-31 09:01:29', 4, 83, 80, NULL),
(84, '2015-03-31 09:03:51', 7, 80, 83, NULL),
(85, '2015-03-31 09:04:13', 7, 83, 80, NULL),
(86, '2015-03-31 09:06:38', 7, 80, 83, NULL),
(87, '2015-03-31 09:07:22', 7, 80, 83, NULL),
(90, '2015-03-31 09:21:51', 7, 85, 83, NULL),
(89, '2015-03-31 09:21:20', 4, 83, 85, NULL),
(91, '2015-03-31 09:22:18', 7, 83, 85, NULL),
(92, '2015-03-31 09:23:01', 7, 83, 85, NULL),
(783, '2015-04-17 16:01:27', 7, 131, 140, NULL),
(94, '2015-03-31 09:53:14', 7, 85, 83, NULL),
(95, '2015-03-31 09:53:41', 7, 85, 83, NULL),
(96, '2015-03-31 09:53:45', 7, 83, 85, NULL),
(99, '2015-03-31 09:55:03', 4, 71, 85, NULL),
(98, '2015-03-31 09:54:20', 6, 71, 85, 3),
(101, '2015-03-31 09:55:59', 7, 71, 85, NULL),
(102, '2015-03-31 10:01:08', 7, 83, 85, NULL),
(103, '2015-03-31 10:01:40', 7, 83, 85, NULL),
(104, '2015-03-31 10:01:47', 7, 83, 85, NULL),
(105, '2015-03-31 10:02:42', 7, 83, 85, NULL),
(106, '2015-03-31 10:04:21', 7, 83, 85, NULL),
(107, '2015-03-31 10:04:46', 7, 85, 83, NULL),
(108, '2015-03-31 10:04:52', 7, 85, 83, NULL),
(109, '2015-03-31 10:05:06', 7, 85, 83, NULL),
(110, '2015-03-31 10:05:19', 7, 85, 83, NULL),
(111, '2015-03-31 10:14:51', 0, 12, 14, 9),
(112, '2015-03-31 10:14:51', 0, 12, 16, 9),
(113, '2015-03-31 10:14:51', 0, 12, 10, 9),
(117, '2015-03-31 10:17:37', 5, 12, 81, 7),
(115, '2015-03-31 10:17:24', 5, 12, 83, 8),
(1226, '2015-05-12 16:19:45', 2, 91, 71, 142),
(122, '2015-03-31 10:24:39', 7, 90, 85, NULL),
(514, '2015-04-15 10:11:56', 4, 71, 144, NULL),
(121, '2015-03-31 10:24:21', 4, 90, 85, NULL),
(123, '2015-03-31 10:25:10', 7, 85, 90, NULL),
(124, '2015-03-31 10:25:23', 7, 85, 90, NULL),
(125, '2015-03-31 10:25:35', 7, 90, 85, NULL),
(126, '2015-03-31 10:25:52', 7, 90, 85, NULL),
(127, '2015-03-31 10:25:56', 7, 90, 85, NULL),
(128, '2015-03-31 10:26:10', 7, 90, 85, NULL),
(811, '2015-04-18 09:57:50', 7, 140, 71, NULL),
(130, '2015-03-31 10:26:50', 4, 71, 90, NULL),
(132, '2015-03-31 10:27:24', 7, 71, 90, NULL),
(133, '2015-03-31 10:27:50', 0, 12, 14, 11),
(134, '2015-03-31 10:27:50', 0, 12, 16, 11),
(135, '2015-03-31 10:27:50', 0, 12, 10, 11),
(136, '2015-03-31 10:28:16', 2, 12, 1, 11),
(137, '2015-03-31 10:28:16', 2, 12, 8, 11),
(138, '2015-03-31 10:28:16', 2, 12, 10, 11),
(139, '2015-03-31 10:28:16', 2, 12, 14, 11),
(140, '2015-03-31 10:28:17', 2, 12, 19, 11),
(823, '2015-04-18 15:02:48', 7, 71, 140, NULL),
(142, '2015-03-31 10:28:25', 7, 90, 85, NULL),
(1242, '2015-05-13 13:36:59', 0, 131, 169, 148),
(813, '2015-04-18 10:01:35', 7, 140, 71, NULL),
(829, '2015-04-18 15:07:47', 7, 71, 140, NULL),
(146, '2015-03-31 10:30:24', 7, 90, 85, NULL),
(147, '2015-03-31 10:30:44', 0, 12, 14, 12),
(148, '2015-03-31 10:30:44', 0, 12, 16, 12),
(149, '2015-03-31 10:30:44', 0, 12, 10, 12),
(150, '2015-03-31 10:31:12', 7, 71, 90, NULL),
(151, '2015-03-31 10:31:20', 7, 71, 90, NULL),
(815, '2015-04-18 10:02:32', 7, 71, 140, NULL),
(154, '2015-03-31 10:34:08', 0, 12, 14, 13),
(155, '2015-03-31 10:34:08', 0, 12, 16, 13),
(156, '2015-03-31 10:34:08', 0, 12, 10, 13),
(157, '2015-03-31 10:34:35', 2, 12, 14, 13),
(158, '2015-03-31 10:34:36', 2, 12, 10, 13),
(819, '2015-04-18 14:50:11', 7, 71, 140, NULL),
(160, '2015-03-31 10:34:37', 2, 12, 8, 13),
(161, '2015-03-31 10:34:37', 2, 12, 1, 13),
(162, '2015-03-31 10:34:37', 2, 12, 90, 13),
(163, '2015-03-31 10:34:38', 2, 12, 19, 13),
(164, '2015-03-31 10:34:48', 4, 12, 90, NULL),
(830, '2015-04-21 15:25:08', 7, 71, 140, NULL),
(1533, '2015-06-10 13:25:13', 0, 131, 169, 228),
(170, '2015-03-31 10:36:34', 7, 12, 90, NULL),
(171, '2015-03-31 10:45:29', 7, 85, 90, NULL),
(172, '2015-03-31 10:45:48', 7, 71, 90, NULL),
(173, '2015-03-31 10:46:03', 7, 71, 90, NULL),
(827, '2015-04-18 15:04:50', 7, 140, 71, NULL),
(175, '2015-03-31 10:47:58', 7, 71, 90, NULL),
(176, '2015-03-31 10:48:32', 7, 90, 85, NULL),
(177, '2015-03-31 10:48:36', 7, 90, 85, NULL),
(178, '2015-03-31 10:48:55', 7, 90, 85, NULL),
(179, '2015-03-31 10:48:57', 7, 90, 85, NULL),
(180, '2015-03-31 10:48:58', 7, 90, 85, NULL),
(828, '2015-04-18 15:06:07', 7, 71, 140, NULL),
(182, '2015-03-31 10:49:33', 7, 71, 90, NULL),
(183, '2015-03-31 10:50:04', 2, 71, 90, 12),
(198, '2015-03-31 11:45:41', 7, 94, 93, NULL),
(185, '2015-03-31 10:54:33', 7, 90, 85, NULL),
(186, '2015-03-31 10:55:03', 7, 85, 90, NULL),
(187, '2015-03-31 10:55:05', 7, 85, 90, NULL),
(188, '2015-03-31 10:55:08', 7, 85, 90, NULL),
(189, '2015-03-31 10:57:17', 7, 85, 90, NULL),
(190, '2015-03-31 10:57:28', 7, 85, 90, NULL),
(191, '2015-03-31 10:57:53', 7, 90, 85, NULL),
(192, '2015-03-31 11:00:51', 7, 90, 85, NULL),
(193, '2015-03-31 11:02:08', 7, 85, 90, NULL),
(194, '2015-03-31 11:02:17', 7, 90, 85, NULL),
(197, '2015-03-31 11:45:30', 6, 12, 90, 12),
(196, '2015-03-31 11:45:05', 4, 93, 94, NULL),
(199, '2015-03-31 11:45:54', 7, 94, 93, NULL),
(204, '2015-03-31 11:46:55', 7, 93, 94, NULL),
(201, '2015-03-31 11:46:12', 7, 93, 94, NULL),
(202, '2015-03-31 11:46:32', 7, 93, 94, NULL),
(1524, '2015-06-10 10:50:22', 6, 91, 188, 220),
(205, '2015-03-31 11:47:02', 7, 93, 94, NULL),
(206, '2015-03-31 11:47:46', 7, 93, 94, NULL),
(207, '2015-03-31 11:47:57', 7, 93, 94, NULL),
(208, '2015-03-31 11:48:33', 7, 93, 94, NULL),
(209, '2015-03-31 11:49:18', 2, 93, 94, 3),
(223, '2015-03-31 11:55:04', 7, 94, 93, NULL),
(211, '2015-03-31 11:50:07', 2, 93, 94, 17),
(214, '2015-03-31 11:50:48', 7, 93, 94, NULL),
(213, '2015-03-31 11:50:26', 6, 93, 94, 17),
(215, '2015-03-31 11:50:57', 7, 93, 94, NULL),
(216, '2015-03-31 11:51:19', 7, 94, 93, NULL),
(217, '2015-03-31 11:51:42', 7, 94, 93, NULL),
(218, '2015-03-31 11:51:46', 7, 94, 93, NULL),
(219, '2015-03-31 11:51:48', 7, 94, 93, NULL),
(220, '2015-03-31 11:51:50', 7, 94, 93, NULL),
(221, '2015-03-31 11:51:54', 7, 94, 93, NULL),
(222, '2015-03-31 11:54:03', 6, 71, 94, 3),
(224, '2015-03-31 11:55:23', 0, 12, 14, 18),
(225, '2015-03-31 11:55:23', 0, 12, 16, 18),
(226, '2015-03-31 11:55:23', 0, 12, 10, 18),
(227, '2015-03-31 11:55:54', 2, 12, 10, 18),
(831, '2015-04-21 15:30:39', 7, 71, 135, NULL),
(1239, '2015-05-13 13:36:59', 0, 131, 135, 148),
(3171, '2015-07-30 10:38:01', 2, 182, 228, 343),
(1881, '2015-06-26 17:12:53', 2, 12, 177, 257),
(234, '2015-03-31 13:06:01', 3, 91, 83, NULL),
(1903, '2015-06-28 15:22:47', 6, 91, 188, 240),
(1240, '2015-05-13 13:36:59', 0, 131, 158, 148),
(239, '2015-03-31 13:07:40', 2, 91, 83, 15),
(914, '2015-04-25 08:35:08', 7, 131, 71, NULL),
(2103, '2015-07-06 12:53:48', 0, 12, 194, 270),
(915, '2015-04-25 08:35:10', 7, 131, 71, NULL),
(1476, '2015-05-26 11:06:57', 2, 12, 168, 141),
(245, '2015-03-31 13:08:46', 2, 91, 83, 20),
(248, '2015-03-31 13:30:28', 7, 94, 93, NULL),
(2426, '2015-07-20 14:42:30', 2, 182, 142, 264),
(249, '2015-03-31 13:30:41', 7, 93, 94, NULL),
(250, '2015-03-31 13:30:50', 7, 93, 94, NULL),
(251, '2015-03-31 13:30:59', 7, 93, 94, NULL),
(252, '2015-03-31 13:31:28', 7, 94, 93, NULL),
(971, '2015-05-04 15:00:36', 2, 12, 10, 115),
(2213, '2015-07-16 15:04:42', 4, 142, 179, NULL),
(258, '2015-03-31 13:47:04', 6, 71, 92, 3),
(257, '2015-03-31 13:47:03', 4, 71, 92, NULL),
(260, '2015-03-31 13:49:29', 7, 71, 92, NULL),
(263, '2015-03-31 14:27:52', 7, 98, 94, NULL),
(262, '2015-03-31 14:25:47', 4, 94, 98, NULL),
(264, '2015-03-31 14:28:47', 7, 94, 98, NULL),
(265, '2015-03-31 14:29:08', 7, 94, 98, NULL),
(266, '2015-03-31 14:34:25', 7, 94, 98, NULL),
(267, '2015-03-31 14:35:48', 7, 94, 98, NULL),
(268, '2015-03-31 14:36:06', 7, 94, 98, NULL),
(269, '2015-03-31 14:36:40', 7, 94, 98, NULL),
(270, '2015-03-31 14:37:44', 7, 94, 98, NULL),
(271, '2015-03-31 14:39:17', 7, 98, 94, NULL),
(272, '2015-03-31 14:39:28', 7, 94, 98, NULL),
(275, '2015-03-31 14:56:49', 7, 94, 98, NULL),
(274, '2015-03-31 14:42:24', 4, 92, 98, NULL),
(276, '2015-03-31 14:58:53', 7, 94, 98, NULL),
(277, '2015-03-31 15:01:00', 7, 98, 94, NULL),
(280, '2015-04-01 06:17:18', 2, 91, 83, 24),
(279, '2015-03-31 15:02:10', 6, 94, 98, 22),
(1481, '2015-05-28 07:20:28', 6, 91, 142, 161),
(2873, '2015-07-28 11:13:14', 7, 182, 198, NULL),
(3117, '2015-07-29 11:43:42', 1, 214, 12, 293),
(299, '2015-04-02 10:23:21', 0, 71, 66, 33),
(1343, '2015-05-19 10:41:07', 2, 131, 101, 161),
(289, '2015-04-01 12:39:00', 7, 92, 103, NULL),
(288, '2015-04-01 12:38:16', 4, 92, 103, NULL),
(290, '2015-04-01 12:39:38', 7, 103, 92, NULL),
(291, '2015-04-01 12:39:39', 7, 103, 92, NULL),
(2241, '2015-07-17 07:53:24', 5, 212, 179, 272),
(1543, '2015-06-11 09:17:17', 0, 12, 14, 229),
(1947, '2015-06-30 06:20:11', 6, 91, 195, 247),
(1666, '2015-06-16 13:22:42', 2, 12, 177, 239),
(2781, '2015-07-27 08:27:34', 0, 131, 158, 318),
(1253, '2015-05-13 17:30:14', 2, 91, 71, 151),
(300, '2015-04-02 10:23:21', 0, 71, 85, 33),
(1793, '2015-06-22 14:20:49', 7, 189, 142, NULL),
(1341, '2015-05-19 10:41:06', 2, 131, 71, 161),
(303, '2015-04-02 10:23:21', 0, 71, 92, 33),
(306, '2015-04-02 14:02:26', 3, 69, 83, NULL),
(3338, '2015-07-30 18:04:55', 6, 182, 163, 343),
(307, '2015-04-02 14:02:38', 5, 69, 83, 8),
(3336, '2015-07-30 18:04:37', 2, 182, 163, 343),
(309, '2015-04-02 16:04:13', 7, 71, 92, NULL),
(310, '2015-04-02 16:04:17', 7, 71, 92, NULL),
(1269, '2015-05-13 20:03:10', 2, 91, 168, 157),
(1692, '2015-06-17 12:54:57', 1, 182, 140, 194),
(313, '2015-04-02 16:04:59', 7, 71, 92, NULL),
(314, '2015-04-02 16:05:08', 7, 71, 92, NULL),
(3099, '2015-07-29 09:31:27', 7, 182, 260, NULL),
(316, '2015-04-03 08:09:06', 7, 71, 92, NULL),
(3381, '2015-07-30 18:23:09', 7, 182, 163, NULL),
(318, '2015-04-03 10:04:18', 0, 12, 14, 34),
(319, '2015-04-03 10:04:18', 0, 12, 16, 34),
(320, '2015-04-03 10:04:18', 0, 12, 10, 34),
(1348, '2015-05-19 10:41:46', 2, 131, 71, 133),
(322, '2015-04-03 15:21:55', 5, 71, 81, 7),
(333, '2015-04-03 16:31:11', 5, 101, 94, 22),
(1373, '2015-05-19 11:02:05', 0, 131, 135, 164),
(2388, '2015-07-20 12:31:53', 6, 163, 179, 290),
(3520, '2015-08-02 09:50:41', 4, 12, 241, NULL),
(3106, '2015-07-29 10:12:15', 2, 131, 140, 299),
(332, '2015-04-03 16:28:10', 6, 71, 101, 33),
(1378, '2015-05-19 11:03:14', 7, 131, 179, NULL),
(335, '2015-04-03 17:33:49', 0, 91, 101, 37),
(336, '2015-04-03 17:35:10', 0, 91, 101, 38),
(337, '2015-04-03 17:36:12', 0, 91, 101, 39),
(338, '2015-04-03 17:37:39', 0, 91, 101, 40),
(339, '2015-04-03 17:39:07', 0, 91, 101, 41),
(340, '2015-04-03 17:40:40', 0, 91, 101, 42),
(341, '2015-04-03 17:41:51', 0, 91, 101, 43),
(342, '2015-04-03 17:46:08', 0, 91, 101, 45),
(343, '2015-04-03 17:47:24', 0, 91, 101, 46),
(344, '2015-04-03 17:48:21', 0, 91, 101, 47),
(353, '2015-04-04 14:02:07', 3, 101, 94, NULL),
(349, '2015-04-03 18:56:19', 6, 71, 101, 33),
(775, '2015-04-17 14:59:53', 7, 71, 140, NULL),
(1369, '2015-05-19 10:58:01', 6, 131, 179, 148),
(1385, '2015-05-19 11:12:23', 7, 131, 71, NULL),
(354, '2015-04-04 14:04:09', 3, 71, 93, NULL),
(1274, '2015-05-14 08:12:25', 2, 142, 177, 158),
(358, '2015-04-04 14:38:34', 6, 107, 101, 48),
(357, '2015-04-04 14:38:32', 4, 107, 101, NULL),
(362, '2015-04-04 14:53:35', 6, 107, 101, 48),
(361, '2015-04-04 14:53:34', 4, 107, 101, NULL),
(3016, '2015-07-28 16:43:56', 7, 231, 142, NULL),
(365, '2015-04-04 16:52:41', 7, 12, 90, NULL),
(366, '2015-04-04 16:52:44', 7, 12, 90, NULL),
(367, '2015-04-04 16:52:46', 7, 12, 90, NULL),
(368, '2015-04-04 16:52:48', 7, 12, 90, NULL),
(369, '2015-04-04 16:52:50', 7, 12, 90, NULL),
(2725, '2015-07-26 12:04:12', 2, 194, 165, 315),
(371, '2015-04-04 17:02:20', 0, 12, 14, 49),
(372, '2015-04-04 17:02:20', 0, 12, 16, 49),
(373, '2015-04-04 17:02:20', 0, 12, 10, 49),
(1851, '2015-06-24 14:20:30', 0, 12, 14, 256),
(375, '2015-04-04 17:42:35', 6, 107, 101, 48),
(1106, '2015-05-08 08:05:14', 7, 177, 176, NULL),
(1679, '2015-06-16 15:24:12', 1, 182, 140, 194),
(382, '2015-04-07 10:07:47', 4, 106, 92, NULL),
(386, '2015-04-07 10:21:45', 7, 92, 108, NULL),
(385, '2015-04-07 10:17:23', 4, 92, 108, NULL),
(387, '2015-04-07 10:33:03', 7, 92, 108, NULL),
(388, '2015-04-07 10:34:37', 7, 92, 108, NULL),
(389, '2015-04-07 10:35:34', 7, 92, 108, NULL),
(390, '2015-04-07 10:36:24', 7, 92, 108, NULL),
(391, '2015-04-07 10:36:45', 7, 92, 108, NULL),
(394, '2015-04-08 14:16:00', 1, 107, 101, 48),
(393, '2015-04-08 09:09:50', 6, 92, 101, 23),
(395, '2015-04-08 14:16:46', 1, 107, 101, 48),
(396, '2015-04-08 14:43:19', 1, 107, 101, 48),
(397, '2015-04-08 14:45:11', 1, 107, 101, 48),
(398, '2015-04-08 14:46:54', 1, 107, 101, 48),
(399, '2015-04-08 14:47:16', 1, 107, 101, 48),
(402, '2015-04-09 08:17:24', 1, 101, 101, 66),
(401, '2015-04-09 07:42:24', 6, 107, 101, 48),
(403, '2015-04-09 08:17:59', 1, 101, 101, 66),
(404, '2015-04-09 14:29:58', 1, 107, 101, 48),
(409, '2015-04-10 13:04:07', 2, 101, 134, 74),
(474, '2015-04-11 15:07:37', 0, 101, 133, 103),
(3512, '2015-07-31 16:09:32', 4, 228, 182, NULL),
(1303, '2015-05-17 12:00:00', 2, 12, 168, 136),
(411, '2015-04-10 13:07:25', 2, 101, 134, 35),
(1395, '2015-05-19 12:33:29', 7, 131, 179, NULL),
(804, '2015-04-18 09:55:23', 7, 140, 71, NULL),
(414, '2015-04-10 13:41:32', 5, 135, 111, 73),
(415, '2015-04-10 13:47:22', 1, 135, 135, 95),
(416, '2015-04-10 13:47:31', 2, 101, 134, 24),
(417, '2015-04-10 13:48:10', 1, 135, 135, 95),
(418, '2015-04-10 13:48:41', 1, 135, 135, 95),
(995, '2015-05-04 19:47:15', 0, 12, 14, 121),
(1697, '2015-06-17 12:59:44', 0, 91, 168, 241),
(422, '2015-04-10 14:07:44', 2, 101, 134, 20),
(423, '2015-04-10 14:07:54', 7, 131, 135, NULL),
(438, '2015-04-10 14:34:51', 2, 101, 134, 69),
(1000, '2015-05-05 11:20:57', 0, 12, 14, 122),
(1005, '2015-05-05 11:21:31', 2, 12, 8, 122),
(1631, '2015-06-16 12:52:12', 2, 12, 1, 160),
(1019, '2015-05-06 14:34:31', 0, 12, 169, 124),
(1119, '2015-05-08 08:17:08', 0, 12, 169, 128),
(1126, '2015-05-08 09:44:31', 0, 12, 169, 129),
(3134, '2015-07-29 12:30:01', 7, 194, 12, NULL),
(439, '2015-04-10 14:35:39', 2, 101, 107, 69),
(440, '2015-04-10 14:35:49', 2, 101, 133, 69),
(1248, '2015-05-13 17:26:48', 0, 91, 177, 152),
(1194, '2015-05-12 12:12:00', 0, 12, 169, 142),
(444, '2015-04-10 14:42:31', 7, 131, 135, NULL),
(447, '2015-04-10 21:20:41', 0, 12, 14, 100),
(1596, '2015-06-15 18:51:44', 0, 12, 14, 238),
(448, '2015-04-10 21:20:41', 0, 12, 16, 100),
(449, '2015-04-10 21:20:41', 0, 12, 10, 100),
(1749, '2015-06-20 10:49:12', 0, 91, 168, 246),
(451, '2015-04-10 21:21:19', 2, 12, 1, 100),
(452, '2015-04-10 21:21:19', 2, 12, 14, 100),
(1464, '2015-05-26 11:04:11', 2, 131, 135, 155),
(455, '2015-04-11 12:31:36', 7, 71, 101, NULL),
(456, '2015-04-11 14:32:58', 6, 101, 134, 74),
(1327, '2015-05-19 10:22:30', 0, 131, 135, 163),
(461, '2015-04-11 14:39:01', 0, 101, 133, 101),
(462, '2015-04-11 14:39:01', 0, 101, 134, 101),
(1530, '2015-06-10 13:25:13', 0, 131, 135, 228),
(792, '2015-04-18 08:40:55', 7, 71, 140, NULL),
(469, '2015-04-11 14:46:08', 0, 101, 134, 102),
(797, '2015-04-18 08:57:46', 7, 71, 140, NULL),
(799, '2015-04-18 09:47:10', 7, 140, 71, NULL),
(1792, '2015-06-22 14:20:33', 7, 189, 142, NULL),
(479, '2015-04-11 15:23:16', 0, 101, 133, 104),
(480, '2015-04-11 15:23:16', 0, 101, 134, 104),
(801, '2015-04-18 09:50:01', 7, 140, 71, NULL),
(1420, '2015-05-20 13:28:14', 2, 12, 177, 165),
(484, '2015-04-11 15:27:36', 0, 101, 133, 105),
(485, '2015-04-11 15:27:36', 0, 101, 134, 105),
(802, '2015-04-18 09:50:18', 7, 140, 71, NULL),
(803, '2015-04-18 09:55:21', 7, 140, 71, NULL),
(496, '2015-04-12 16:36:44', 2, 12, 14, 50),
(805, '2015-04-18 09:55:30', 7, 140, 71, NULL),
(1753, '2015-06-20 11:06:03', 0, 91, 168, 247),
(1271, '2015-05-13 20:25:34', 4, 91, 101, NULL),
(1468, '2015-05-26 11:05:31', 2, 12, 8, 161),
(806, '2015-04-18 09:57:03', 7, 140, 71, NULL),
(1386, '2015-05-19 11:12:26', 7, 131, 71, NULL),
(807, '2015-04-18 09:57:06', 7, 140, 71, NULL),
(1776, '2015-06-22 13:18:40', 7, 91, 190, NULL),
(1773, '2015-06-22 13:13:18', 4, 91, 190, NULL),
(515, '2015-04-15 10:12:50', 2, 71, 144, 35),
(516, '2015-04-15 10:35:37', 7, 71, 144, NULL),
(517, '2015-04-15 10:35:50', 7, 71, 144, NULL),
(518, '2015-04-15 10:36:17', 7, 71, 144, NULL),
(519, '2015-04-15 10:36:59', 7, 71, 144, NULL),
(520, '2015-04-15 10:37:31', 7, 71, 144, NULL),
(521, '2015-04-15 10:40:25', 7, 71, 144, NULL),
(522, '2015-04-15 10:41:06', 2, 71, 144, 94),
(523, '2015-04-15 10:45:49', 0, 71, 66, 109),
(524, '2015-04-15 10:45:49', 0, 71, 85, 109),
(3702, '2015-08-18 13:35:25', 2, 12, 252, 385),
(3443, '2015-07-31 13:05:54', 0, 142, 179, 363),
(527, '2015-04-15 10:45:49', 0, 71, 92, 109),
(528, '2015-04-15 10:45:49', 0, 71, 101, 109),
(790, '2015-04-17 17:51:43', 7, 131, 140, NULL),
(530, '2015-04-15 10:45:49', 0, 71, 144, 109),
(531, '2015-04-15 11:14:59', 7, 71, 144, NULL),
(532, '2015-04-15 12:29:28', 7, 71, 144, NULL),
(533, '2015-04-15 12:31:36', 7, 71, 144, NULL),
(534, '2015-04-15 12:46:45', 2, 71, 144, 109),
(535, '2015-04-15 12:47:33', 7, 71, 144, NULL),
(536, '2015-04-15 12:48:00', 2, 71, 144, 108),
(539, '2015-04-15 13:18:31', 2, 71, 144, 101),
(538, '2015-04-15 12:49:22', 6, 71, 144, 109),
(540, '2015-04-15 13:19:08', 2, 71, 144, 25),
(541, '2015-04-15 13:22:13', 7, 71, 144, NULL),
(542, '2015-04-15 13:22:30', 7, 71, 144, NULL),
(543, '2015-04-15 13:23:12', 2, 71, 144, 93),
(544, '2015-04-15 13:56:55', 2, 71, 144, 63),
(545, '2015-04-15 13:57:51', 2, 71, 144, 77),
(546, '2015-04-15 14:00:39', 2, 71, 144, 81),
(547, '2015-04-15 14:01:17', 2, 71, 144, 100),
(1525, '2015-06-10 11:02:58', 2, 91, 168, 222),
(1470, '2015-05-26 11:05:31', 2, 12, 168, 161),
(795, '2015-04-18 08:42:35', 7, 71, 140, NULL),
(1574, '2015-06-12 17:02:16', 2, 91, 168, 229),
(553, '2015-04-16 08:17:51', 2, 71, 144, 97),
(1668, '2015-06-16 13:22:42', 2, 12, 169, 239),
(816, '2015-04-18 10:02:35', 7, 71, 140, NULL),
(817, '2015-04-18 10:10:06', 7, 140, 140, NULL),
(818, '2015-04-18 14:49:12', 7, 140, 71, NULL),
(820, '2015-04-18 14:50:15', 7, 71, 140, NULL),
(821, '2015-04-18 15:00:50', 7, 71, 140, NULL),
(822, '2015-04-18 15:02:04', 7, 71, 140, NULL),
(561, '2015-04-16 11:03:45', 7, 131, 135, NULL),
(1419, '2015-05-20 10:39:09', 6, 131, 179, 174),
(1418, '2015-05-20 10:39:07', 6, 131, 142, 174),
(1440, '2015-05-25 09:09:43', 2, 12, 142, 175),
(1439, '2015-05-25 09:09:43', 2, 12, 168, 175),
(3027, '2015-07-28 17:38:35', 7, 239, 12, NULL),
(567, '2015-04-16 11:09:16', 7, 131, 135, NULL),
(568, '2015-04-16 11:09:24', 7, 131, 135, NULL),
(809, '2015-04-18 09:57:39', 7, 140, 71, NULL),
(810, '2015-04-18 09:57:45', 7, 140, 71, NULL),
(573, '2015-04-16 11:16:07', 7, 131, 135, NULL),
(574, '2015-04-16 11:16:13', 7, 131, 135, NULL),
(824, '2015-04-18 15:03:53', 7, 140, 71, NULL),
(826, '2015-04-18 15:04:29', 7, 140, 71, NULL),
(812, '2015-04-18 10:01:30', 7, 140, 71, NULL),
(814, '2015-04-18 10:01:41', 7, 140, 71, NULL),
(825, '2015-04-18 15:04:26', 7, 71, 140, NULL),
(911, '2015-04-25 08:35:01', 7, 131, 71, NULL),
(1095, '2015-05-07 14:17:20', 7, 177, 176, NULL),
(912, '2015-04-25 08:35:04', 7, 131, 71, NULL),
(592, '2015-04-16 11:49:09', 7, 131, 101, NULL),
(593, '2015-04-16 11:49:12', 7, 131, 101, NULL),
(594, '2015-04-16 11:49:14', 7, 131, 101, NULL),
(595, '2015-04-16 11:49:16', 7, 131, 101, NULL),
(596, '2015-04-16 11:49:19', 7, 131, 101, NULL),
(597, '2015-04-16 11:57:04', 7, 131, 101, NULL),
(598, '2015-04-16 11:57:06', 7, 131, 101, NULL),
(599, '2015-04-16 11:57:09', 7, 131, 101, NULL),
(600, '2015-04-16 11:57:11', 7, 131, 101, NULL),
(601, '2015-04-16 11:57:13', 7, 131, 101, NULL),
(602, '2015-04-16 11:57:18', 7, 131, 135, NULL),
(603, '2015-04-16 11:57:24', 7, 131, 135, NULL),
(2363, '2015-07-20 10:16:59', 6, 91, 206, 241),
(3014, '2015-07-28 16:39:36', 7, 182, 223, NULL),
(1443, '2015-05-25 09:12:12', 6, 131, 142, 175),
(3513, '2015-07-31 17:08:49', 4, 195, 217, NULL),
(913, '2015-04-25 08:35:06', 7, 131, 71, NULL),
(1276, '2015-05-14 08:12:54', 6, 142, 177, 158),
(930, '2015-05-04 12:29:22', 0, 12, 14, 112),
(619, '2015-04-16 12:39:30', 7, 131, 135, NULL),
(1365, '2015-05-19 10:57:28', 2, 131, 101, 148),
(621, '2015-04-16 12:40:12', 7, 131, 135, NULL),
(3487, '2015-07-31 15:12:27', 1, 182, 182, 345),
(1786, '2015-06-22 14:11:30', 4, 189, 142, NULL),
(1461, '2015-05-26 11:04:09', 2, 131, 101, 155),
(3357, '2015-07-30 18:15:49', 7, 182, 163, NULL),
(1943, '2015-06-30 06:20:03', 6, 91, 195, 251),
(3618, '2015-08-10 06:18:00', 7, 91, 277, NULL),
(2107, '2015-07-07 11:33:42', 7, 203, 197, NULL),
(1677, '2015-06-16 14:49:08', 1, 182, 140, 194),
(1678, '2015-06-16 14:57:51', 1, 182, 140, 194),
(1362, '2015-05-19 10:57:25', 2, 131, 179, 148),
(979, '2015-05-04 15:01:48', 2, 12, 10, 112),
(1691, '2015-06-17 12:34:57', 1, 182, 140, 194),
(1012, '2015-05-05 11:21:39', 2, 12, 90, 122),
(1278, '2015-05-14 08:41:13', 2, 91, 168, 111),
(2160, '2015-07-13 09:04:58', 7, 197, 198, NULL),
(3053, '2015-07-28 20:28:23', 7, 252, 194, NULL),
(2547, '2015-07-23 16:03:13', 3, 198, 197, NULL),
(1726, '2015-06-18 09:28:26', 0, 12, 14, 244),
(1307, '2015-05-17 16:05:28', 2, 91, 101, 153),
(763, '2015-04-17 13:53:20', 7, 71, 140, NULL),
(764, '2015-04-17 13:55:23', 7, 71, 140, NULL),
(765, '2015-04-17 14:23:43', 2, 71, 140, 55),
(3604, '2015-08-06 18:54:29', 7, 279, 278, NULL),
(773, '2015-04-17 14:59:27', 7, 71, 140, NULL),
(774, '2015-04-17 14:59:52', 7, 71, 140, NULL),
(3514, '2015-07-31 17:09:52', 7, 195, 91, NULL),
(3521, '2015-08-02 11:14:38', 4, 165, 241, NULL),
(1779, '2015-06-22 13:58:40', 7, 91, 190, NULL),
(2987, '2015-07-28 15:07:14', 0, 245, 244, 328),
(1094, '2015-05-07 14:01:41', 7, 177, 176, NULL),
(767, '2015-04-17 14:25:55', 7, 131, 135, NULL),
(2485, '2015-07-21 09:49:05', 1, 213, 214, 296),
(769, '2015-04-17 14:27:41', 7, 131, 135, NULL),
(770, '2015-04-17 14:28:07', 7, 131, 135, NULL),
(3457, '2015-07-31 13:27:37', 7, 182, 131, NULL),
(772, '2015-04-17 14:59:12', 7, 71, 140, NULL),
(2933, '2015-07-28 13:49:30', 0, 194, 165, 326),
(782, '2015-04-17 16:01:13', 7, 131, 140, NULL),
(3504, '2015-07-31 15:56:22', 4, 131, 182, NULL),
(3624, '2015-08-12 10:14:24', 6, 243, 285, 379),
(787, '2015-04-17 16:03:35', 7, 131, 140, NULL),
(789, '2015-04-17 17:51:24', 7, 131, 140, NULL),
(832, '2015-04-21 15:35:55', 7, 71, 135, NULL),
(833, '2015-04-21 15:36:06', 7, 71, 135, NULL),
(834, '2015-04-21 15:36:15', 7, 71, 135, NULL),
(835, '2015-04-21 15:36:20', 7, 71, 135, NULL),
(836, '2015-04-21 16:21:52', 7, 71, 135, NULL),
(837, '2015-04-21 16:34:09', 7, 71, 135, NULL),
(838, '2015-04-21 16:34:15', 7, 71, 135, NULL),
(839, '2015-04-21 16:34:21', 7, 71, 135, NULL),
(840, '2015-04-21 16:34:35', 7, 71, 135, NULL),
(841, '2015-04-21 16:34:43', 7, 71, 135, NULL),
(842, '2015-04-21 16:34:51', 7, 71, 135, NULL),
(843, '2015-04-21 16:49:44', 7, 71, 135, NULL),
(844, '2015-04-21 16:49:50', 7, 71, 135, NULL),
(845, '2015-04-21 16:49:57', 7, 71, 135, NULL),
(846, '2015-04-21 16:50:03', 7, 71, 135, NULL),
(847, '2015-04-21 16:50:09', 7, 71, 135, NULL),
(848, '2015-04-21 16:50:17', 7, 71, 135, NULL),
(849, '2015-04-21 16:50:22', 7, 71, 135, NULL),
(854, '2015-04-22 14:44:50', 7, 158, 164, NULL),
(2635, '2015-07-24 22:07:51', 7, 91, 165, NULL),
(2774, '2015-07-27 08:25:50', 0, 131, 12, 317),
(2782, '2015-07-27 08:27:34', 0, 131, 12, 318),
(856, '2015-04-22 15:02:24', 7, 164, 158, NULL),
(857, '2015-04-22 15:02:30', 7, 164, 158, NULL),
(858, '2015-04-22 15:02:38', 7, 164, 158, NULL),
(859, '2015-04-22 15:03:12', 7, 164, 158, NULL),
(860, '2015-04-22 15:03:15', 7, 164, 158, NULL),
(861, '2015-04-22 15:03:28', 7, 164, 158, NULL),
(862, '2015-04-22 15:14:53', 7, 164, 158, NULL),
(863, '2015-04-22 15:14:55', 7, 164, 158, NULL),
(864, '2015-04-22 15:14:58', 7, 164, 158, NULL),
(865, '2015-04-22 15:15:00', 7, 164, 158, NULL),
(866, '2015-04-22 15:24:07', 7, 164, 158, NULL),
(867, '2015-04-22 15:24:10', 7, 164, 158, NULL),
(868, '2015-04-22 15:24:11', 7, 164, 158, NULL),
(869, '2015-04-22 15:25:53', 7, 164, 158, NULL),
(870, '2015-04-22 15:29:42', 2, 158, 164, 76),
(871, '2015-04-22 15:30:04', 7, 164, 158, NULL),
(872, '2015-04-22 15:30:22', 2, 164, 158, 79),
(873, '2015-04-22 15:37:46', 7, 164, 158, NULL),
(874, '2015-04-22 15:56:53', 7, 164, 158, NULL),
(875, '2015-04-22 16:05:22', 7, 158, 164, NULL),
(876, '2015-04-22 16:05:45', 7, 164, 158, NULL),
(877, '2015-04-22 16:05:48', 7, 164, 158, NULL),
(878, '2015-04-22 16:24:17', 7, 164, 158, NULL),
(879, '2015-04-22 16:35:44', 7, 164, 158, NULL),
(880, '2015-04-22 16:36:31', 7, 164, 158, NULL),
(881, '2015-04-22 16:36:34', 7, 164, 158, NULL),
(882, '2015-04-22 16:36:35', 7, 164, 158, NULL),
(883, '2015-04-22 16:36:37', 7, 164, 158, NULL),
(884, '2015-04-22 16:37:48', 7, 164, 158, NULL),
(885, '2015-04-22 16:37:50', 7, 164, 158, NULL),
(886, '2015-04-22 16:37:55', 7, 164, 158, NULL),
(887, '2015-04-22 16:38:02', 7, 164, 158, NULL),
(888, '2015-04-22 16:38:06', 7, 164, 158, NULL),
(889, '2015-04-22 16:38:08', 7, 164, 158, NULL),
(890, '2015-04-22 16:38:12', 7, 164, 158, NULL),
(891, '2015-04-22 16:38:36', 7, 164, 158, NULL),
(892, '2015-04-22 16:38:40', 7, 164, 158, NULL),
(893, '2015-04-22 16:38:44', 7, 164, 158, NULL),
(898, '2015-04-23 15:35:31', 7, 158, 164, NULL),
(895, '2015-04-23 11:12:07', 4, 164, 158, NULL),
(897, '2015-04-23 14:51:57', 4, 164, 158, NULL),
(899, '2015-04-23 15:35:51', 7, 164, 158, NULL),
(900, '2015-04-23 16:04:25', 1, 164, 158, 110),
(909, '2015-04-24 07:20:06', 5, 163, 135, 95),
(902, '2015-04-23 16:06:52', 6, 164, 158, 110),
(904, '2015-04-23 16:12:02', 6, 164, 158, 110),
(906, '2015-04-23 16:36:54', 4, 158, 164, NULL),
(908, '2015-04-23 16:49:40', 4, 164, 158, NULL),
(910, '2015-04-25 08:34:43', 7, 131, 140, NULL),
(1268, '2015-05-13 20:03:10', 2, 91, 71, 157),
(1277, '2015-05-14 08:41:13', 2, 91, 71, 111),
(1273, '2015-05-14 07:54:36', 4, 142, 177, NULL),
(3511, '2015-07-31 16:09:12', 4, 131, 182, NULL),
(922, '2015-05-04 11:54:47', 0, 12, 14, 111),
(923, '2015-05-04 11:54:47', 0, 12, 16, 111),
(924, '2015-05-04 11:54:47', 0, 12, 10, 111),
(3424, '2015-07-31 10:26:44', 7, 163, 131, NULL),
(2881, '2015-07-28 11:51:42', 2, 165, 217, 324),
(927, '2015-05-04 12:01:43', 2, 12, 1, 111),
(928, '2015-05-04 12:01:44', 2, 12, 10, 111),
(929, '2015-05-04 12:01:44', 2, 12, 71, 111),
(931, '2015-05-04 12:29:22', 0, 12, 16, 112),
(932, '2015-05-04 12:29:22', 0, 12, 10, 112),
(1555, '2015-06-12 08:57:19', 0, 142, 177, 230),
(934, '2015-05-04 12:44:10', 0, 12, 14, 113),
(935, '2015-05-04 12:44:10', 0, 12, 16, 113),
(936, '2015-05-04 12:44:10', 0, 12, 10, 113),
(1558, '2015-06-12 13:20:42', 0, 182, 179, 231),
(938, '2015-05-04 12:54:21', 0, 12, 14, 114),
(939, '2015-05-04 12:54:21', 0, 12, 16, 114),
(940, '2015-05-04 12:54:21', 0, 12, 10, 114),
(1884, '2015-06-26 17:18:06', 6, 91, 195, 240),
(942, '2015-05-04 13:55:53', 0, 12, 14, 115),
(943, '2015-05-04 13:55:53', 0, 12, 16, 115),
(944, '2015-05-04 13:55:53', 0, 12, 10, 115),
(3615, '2015-08-08 14:42:22', 7, 165, 252, NULL),
(1702, '2015-06-17 13:01:27', 0, 142, 177, 242),
(947, '2015-05-04 13:59:10', 2, 12, 14, 111),
(948, '2015-05-04 13:59:10', 2, 12, 8, 111),
(949, '2015-05-04 13:59:19', 2, 12, 90, 111),
(950, '2015-05-04 14:02:37', 0, 12, 14, 116),
(951, '2015-05-04 14:02:37', 0, 12, 16, 116),
(952, '2015-05-04 14:02:37', 0, 12, 10, 116),
(1578, '2015-06-15 07:51:00', 0, 182, 179, 234),
(954, '2015-05-04 14:06:29', 0, 12, 14, 117),
(955, '2015-05-04 14:06:29', 0, 12, 16, 117),
(956, '2015-05-04 14:06:29', 0, 12, 10, 117),
(1582, '2015-06-15 08:57:25', 0, 182, 179, 235),
(958, '2015-05-04 14:11:56', 0, 12, 14, 118),
(959, '2015-05-04 14:11:56', 0, 12, 16, 118),
(960, '2015-05-04 14:11:56', 0, 12, 10, 118),
(1584, '2015-06-15 09:09:47', 0, 182, 179, 236),
(962, '2015-05-04 14:58:54', 0, 12, 14, 119),
(963, '2015-05-04 14:58:54', 0, 12, 16, 119),
(964, '2015-05-04 14:58:54', 0, 12, 10, 119),
(1775, '2015-06-22 13:16:58', 7, 91, 190, NULL),
(966, '2015-05-04 15:00:35', 2, 12, 8, 115),
(967, '2015-05-04 15:00:35', 2, 12, 1, 115),
(1945, '2015-06-30 06:20:08', 6, 91, 195, 241),
(969, '2015-05-04 15:00:36', 2, 12, 14, 115),
(970, '2015-05-04 15:00:36', 2, 12, 71, 115),
(972, '2015-05-04 15:00:37', 2, 12, 90, 115),
(1601, '2015-06-15 18:51:44', 0, 12, 169, 238),
(974, '2015-05-04 15:01:47', 2, 12, 8, 112),
(975, '2015-05-04 15:01:47', 2, 12, 1, 112),
(1611, '2015-06-16 07:17:21', 7, 91, 188, NULL),
(977, '2015-05-04 15:01:48', 2, 12, 14, 112),
(978, '2015-05-04 15:01:48', 2, 12, 71, 112),
(980, '2015-05-04 15:01:48', 2, 12, 90, 112),
(1283, '2015-05-14 08:49:09', 0, 91, 177, 159),
(1615, '2015-06-16 10:05:39', 2, 142, 177, 230),
(3115, '2015-07-29 11:42:47', 4, 214, 131, NULL),
(1652, '2015-06-16 12:59:51', 0, 12, 169, 239),
(3597, '2015-08-05 07:27:43', 7, 163, 182, NULL),
(1260, '2015-05-13 17:36:26', 0, 91, 177, 155),
(1663, '2015-06-16 13:22:40', 2, 12, 90, 239),
(1593, '2015-06-15 18:45:07', 4, 91, 188, NULL),
(1791, '2015-06-22 14:19:47', 7, 142, 189, NULL),
(1597, '2015-06-15 18:51:44', 0, 12, 16, 238),
(996, '2015-05-04 19:47:15', 0, 12, 16, 121),
(997, '2015-05-04 19:47:15', 0, 12, 10, 121),
(1610, '2015-06-16 07:16:47', 7, 91, 188, NULL),
(1683, '2015-06-16 16:24:46', 0, 91, 188, 240),
(1001, '2015-05-05 11:20:57', 0, 12, 16, 122),
(1002, '2015-05-05 11:20:57', 0, 12, 10, 122),
(1609, '2015-06-16 07:14:57', 4, 91, 188, NULL),
(1699, '2015-06-17 12:59:44', 0, 91, 188, 241),
(1006, '2015-05-05 11:21:32', 2, 12, 1, 122),
(1725, '2015-06-17 14:09:42', 1, 182, 140, 194),
(1009, '2015-05-05 11:21:35', 2, 12, 14, 122),
(1010, '2015-05-05 11:21:36', 2, 12, 10, 122),
(1011, '2015-05-05 11:21:38', 2, 12, 71, 122),
(1027, '2015-05-06 14:45:33', 7, 12, 169, NULL),
(1014, '2015-05-06 14:34:31', 0, 12, 14, 124),
(1015, '2015-05-06 14:34:31', 0, 12, 16, 124),
(1016, '2015-05-06 14:34:31', 0, 12, 10, 124),
(3118, '2015-07-29 11:43:42', 1, 214, 195, 293),
(1731, '2015-06-18 09:28:26', 0, 12, 169, 244),
(1020, '2015-05-06 14:35:01', 6, 12, 169, 115),
(1282, '2015-05-14 08:49:09', 0, 91, 168, 159),
(2673, '2015-07-25 07:34:30', 7, 12, 195, NULL),
(1573, '2015-06-12 17:02:16', 2, 91, 71, 229),
(2978, '2015-07-28 14:38:30', 1, 91, 165, 248),
(1029, '2015-05-06 14:49:05', 7, 12, 169, NULL),
(3515, '2015-07-31 17:10:00', 7, 195, 91, NULL),
(1031, '2015-05-06 14:55:09', 7, 12, 169, NULL),
(3598, '2015-08-05 12:00:33', 1, 182, 182, 355),
(1306, '2015-05-17 16:05:28', 2, 91, 71, 153),
(1034, '2015-05-06 15:08:41', 7, 12, 169, NULL),
(1310, '2015-05-18 12:27:29', 2, 91, 71, 155),
(1313, '2015-05-18 12:31:31', 0, 91, 168, 161),
(1770, '2015-06-22 11:20:20', 6, 182, 190, 200),
(1038, '2015-05-06 15:14:54', 7, 12, 169, NULL),
(1041, '2015-05-07 12:16:52', 7, 176, 169, NULL),
(1040, '2015-05-07 12:16:05', 4, 169, 176, NULL),
(1042, '2015-05-07 12:17:55', 5, 176, 169, 123),
(1043, '2015-05-07 12:18:31', 7, 169, 176, NULL),
(1044, '2015-05-07 12:18:53', 7, 176, 169, NULL),
(1045, '2015-05-07 12:21:21', 7, 169, 176, NULL),
(1046, '2015-05-07 12:21:28', 7, 176, 169, NULL),
(1047, '2015-05-07 12:21:39', 7, 176, 169, NULL),
(1058, '2015-05-07 12:34:15', 3, 168, 169, NULL),
(1704, '2015-06-17 13:04:32', 1, 182, 140, 194),
(1051, '2015-05-07 12:29:24', 7, 176, 169, NULL),
(1052, '2015-05-07 12:30:06', 7, 176, 169, NULL),
(1053, '2015-05-07 12:30:09', 7, 176, 169, NULL),
(1054, '2015-05-07 12:30:18', 7, 169, 176, NULL),
(3103, '2015-07-29 10:12:14', 2, 131, 179, 299),
(1403, '2015-05-19 13:38:28', 2, 12, 168, 165),
(3316, '2015-07-30 16:55:48', 2, 182, 163, 347),
(1376, '2015-05-19 11:02:05', 0, 131, 169, 164),
(1404, '2015-05-19 14:19:26', 7, 131, 179, NULL),
(1330, '2015-05-19 10:22:30', 0, 131, 169, 163),
(1339, '2015-05-19 10:41:05', 2, 131, 135, 161),
(1820, '2015-06-23 16:45:55', 0, 12, 14, 252),
(3558, '2015-08-04 08:48:33', 7, 182, 131, NULL),
(2812, '2015-07-27 14:15:00', 7, 131, 186, NULL),
(1346, '2015-05-19 10:41:45', 2, 131, 135, 133),
(3425, '2015-07-31 10:33:23', 7, 163, 131, NULL),
(1360, '2015-05-19 10:57:25', 2, 131, 135, 148),
(3426, '2015-07-31 10:35:04', 7, 163, 131, NULL),
(3445, '2015-07-31 13:08:42', 6, 142, 199, 363),
(1784, '2015-06-22 14:00:30', 6, 142, 189, 249),
(3539, '2015-08-03 13:17:27', 4, 228, 182, NULL),
(1075, '2015-05-07 12:55:13', 6, 177, 176, 125),
(1415, '2015-05-20 09:39:48', 2, 131, 142, 174),
(3455, '2015-07-31 13:27:17', 7, 182, 131, NULL),
(1083, '2015-05-07 13:32:41', 7, 177, 176, NULL),
(2588, '2015-07-24 21:30:05', 4, 91, 217, NULL),
(1081, '2015-05-07 13:32:03', 4, 177, 176, NULL),
(1084, '2015-05-07 13:32:52', 7, 176, 177, NULL),
(1085, '2015-05-07 13:36:45', 7, 176, 177, NULL),
(1086, '2015-05-07 13:36:55', 7, 176, 177, NULL),
(1087, '2015-05-07 13:44:49', 7, 176, 177, NULL),
(1088, '2015-05-07 13:46:24', 7, 177, 176, NULL),
(1093, '2015-05-07 14:00:17', 7, 176, 177, NULL),
(1092, '2015-05-07 13:56:58', 6, 177, 140, 125),
(1091, '2015-05-07 13:54:05', 6, 176, 140, 127),
(1096, '2015-05-07 14:17:36', 7, 176, 177, NULL),
(1097, '2015-05-07 14:17:40', 7, 176, 177, NULL),
(1098, '2015-05-07 14:17:58', 7, 176, 177, NULL),
(1099, '2015-05-07 14:18:06', 7, 176, 177, NULL),
(1100, '2015-05-07 14:18:11', 7, 177, 176, NULL),
(1101, '2015-05-07 14:18:16', 7, 176, 177, NULL),
(1102, '2015-05-07 15:32:44', 1, 177, 176, 125),
(1103, '2015-05-07 15:32:44', 1, 177, 140, 125),
(2852, '2015-07-28 08:34:51', 7, 91, 194, NULL),
(1107, '2015-05-08 08:05:18', 7, 176, 177, NULL),
(1108, '2015-05-08 08:05:24', 7, 176, 177, NULL),
(1109, '2015-05-08 08:05:30', 7, 176, 177, NULL),
(1110, '2015-05-08 08:05:41', 7, 176, 177, NULL),
(1111, '2015-05-08 08:05:43', 7, 176, 177, NULL),
(1112, '2015-05-08 08:05:46', 7, 176, 177, NULL),
(1113, '2015-05-08 08:05:47', 7, 176, 177, NULL),
(1114, '2015-05-08 08:17:08', 0, 12, 14, 128),
(1115, '2015-05-08 08:17:08', 0, 12, 16, 128),
(1116, '2015-05-08 08:17:08', 0, 12, 10, 128),
(3552, '2015-08-04 08:43:23', 7, 131, 182, NULL),
(3166, '2015-07-30 10:24:26', 4, 163, 263, NULL),
(3452, '2015-07-31 13:27:11', 7, 182, 131, NULL),
(1121, '2015-05-08 09:44:31', 0, 12, 14, 129),
(1122, '2015-05-08 09:44:31', 0, 12, 16, 129),
(1123, '2015-05-08 09:44:31', 0, 12, 10, 129),
(1655, '2015-06-16 13:03:13', 1, 182, 140, 194),
(1742, '2015-06-18 10:46:35', 2, 182, 142, 239),
(1757, '2015-06-20 12:11:37', 0, 91, 168, 248),
(3453, '2015-07-31 13:27:12', 7, 182, 131, NULL),
(1129, '2015-05-08 09:45:11', 2, 12, 8, 129),
(2783, '2015-07-27 08:27:34', 0, 131, 169, 318),
(1777, '2015-06-22 13:18:52', 7, 91, 190, NULL),
(3454, '2015-07-31 13:27:14', 7, 182, 131, NULL),
(3594, '2015-08-05 07:27:07', 7, 163, 182, NULL),
(1781, '2015-06-22 13:59:31', 0, 142, 177, 249),
(3333, '2015-07-30 18:03:48', 2, 182, 163, 349),
(1138, '2015-05-11 10:23:46', 2, 91, 71, 130),
(1707, '2015-06-17 13:20:17', 1, 182, 140, 194),
(1520, '2015-06-10 07:41:01', 2, 91, 71, 222),
(2958, '2015-07-28 14:11:45', 1, 194, 194, 326),
(1674, '2015-06-16 14:32:27', 2, 91, 188, 239),
(3651, '2015-08-12 11:14:47', 7, 277, 91, NULL),
(1531, '2015-06-10 13:25:13', 0, 131, 158, 228),
(3722, '2015-08-19 01:43:07', 5, 317, 316, 388),
(1867, '2015-06-26 13:45:05', 7, 182, 142, NULL),
(1499, '2015-06-07 17:34:41', 6, 91, 168, 219),
(1151, '2015-05-11 10:38:00', 2, 91, 71, 134),
(2972, '2015-07-28 14:26:27', 7, 194, 12, NULL),
(1771, '2015-06-22 11:20:25', 6, 182, 190, 235),
(1487, '2015-06-07 17:29:59', 0, 91, 168, 216),
(1902, '2015-06-28 15:22:44', 4, 91, 195, NULL),
(1718, '2015-06-17 13:22:42', 0, 142, 177, 243),
(2894, '2015-07-28 11:56:59', 7, 182, 224, NULL),
(3312, '2015-07-30 16:51:14', 7, 182, 163, NULL),
(1286, '2015-05-14 14:19:34', 1, 142, 177, 158),
(2935, '2015-07-28 13:52:46', 2, 194, 12, 326),
(1490, '2015-06-07 17:31:05', 0, 91, 168, 217),
(3576, '2015-08-04 13:56:16', 7, 163, 182, NULL),
(1166, '2015-05-11 12:52:14', 2, 91, 71, 136),
(1660, '2015-06-16 13:22:39', 2, 12, 8, 239),
(3560, '2015-08-04 08:49:49', 7, 182, 131, NULL),
(3561, '2015-08-04 08:49:50', 7, 182, 131, NULL),
(2711, '2015-07-25 21:59:22', 1, 91, 206, 241),
(1787, '2015-06-22 14:12:44', 7, 142, 189, NULL),
(1733, '2015-06-18 10:34:13', 2, 182, 142, 240),
(1174, '2015-05-12 07:32:26', 0, 91, 177, 140),
(1496, '2015-06-07 17:34:00', 0, 91, 168, 219),
(3626, '2015-08-12 11:00:23', 7, 91, 277, NULL),
(1681, '2015-06-16 16:24:46', 0, 91, 168, 240),
(1511, '2015-06-08 18:48:09', 0, 91, 168, 223),
(1181, '2015-05-12 10:21:30', 0, 131, 135, 141),
(1182, '2015-05-12 10:21:30', 0, 131, 158, 141),
(3554, '2015-08-04 08:44:01', 7, 131, 182, NULL),
(1184, '2015-05-12 10:21:30', 0, 131, 169, 141),
(1745, '2015-06-20 10:23:59', 0, 91, 168, 245),
(1187, '2015-05-12 10:23:23', 2, 12, 1, 141),
(1508, '2015-06-08 18:38:53', 0, 91, 168, 222),
(1189, '2015-05-12 12:12:00', 0, 12, 14, 142),
(1190, '2015-05-12 12:12:00', 0, 12, 16, 142),
(1191, '2015-05-12 12:12:00', 0, 12, 10, 142),
(1690, '2015-06-17 12:33:50', 1, 182, 140, 194),
(1747, '2015-06-20 10:23:59', 0, 91, 188, 245),
(3678, '2015-08-17 11:11:19', 4, 243, 299, NULL),
(1544, '2015-06-11 09:17:17', 0, 12, 16, 229),
(1245, '2015-05-13 17:25:05', 0, 91, 177, 151),
(2546, '2015-07-23 16:03:08', 6, 198, 142, 258),
(1502, '2015-06-07 17:35:53', 0, 91, 168, 220),
(1237, '2015-05-12 16:31:30', 2, 91, 71, 147),
(1266, '2015-05-13 17:53:36', 0, 91, 177, 156),
(3328, '2015-07-30 18:00:55', 7, 182, 163, NULL),
(2274, '2015-07-17 12:21:27', 2, 165, 213, 293),
(1816, '2015-06-23 15:12:08', 1, 91, 190, 246),
(1251, '2015-05-13 17:29:23', 0, 91, 177, 153),
(1284, '2015-05-14 09:07:19', 2, 91, 168, 130),
(2880, '2015-07-28 11:51:41', 2, 165, 213, 324),
(1293, '2015-05-15 07:42:42', 0, 91, 168, 160),
(1647, '2015-06-16 12:59:51', 0, 12, 14, 239),
(3483, '2015-07-31 14:56:15', 1, 182, 182, 345),
(1661, '2015-06-16 13:22:39', 2, 12, 1, 239),
(1772, '2015-06-22 13:09:33', 1, 182, 190, 200),
(1301, '2015-05-17 11:59:57', 2, 12, 14, 136),
(1751, '2015-06-20 10:49:12', 0, 91, 188, 246),
(1774, '2015-06-22 13:13:20', 6, 91, 190, 246),
(2950, '2015-07-28 14:05:02', 1, 194, 194, 326),
(1308, '2015-05-17 16:05:28', 2, 91, 168, 153),
(2708, '2015-07-25 21:59:22', 1, 91, 195, 241),
(2276, '2015-07-17 12:51:37', 6, 12, 213, 252),
(1314, '2015-05-18 13:16:53', 7, 177, 176, NULL),
(1315, '2015-05-18 13:17:52', 7, 177, 176, NULL),
(1316, '2015-05-18 13:18:54', 7, 142, 177, NULL),
(3414, '2015-07-31 09:01:25', 7, 182, 131, NULL),
(1318, '2015-05-18 13:19:27', 7, 177, 176, NULL),
(1319, '2015-05-18 14:48:56', 7, 177, 176, NULL),
(2636, '2015-07-24 22:08:32', 7, 91, 165, NULL),
(1852, '2015-06-24 14:20:30', 0, 12, 16, 256),
(1323, '2015-05-19 09:12:45', 2, 12, 1, 162),
(1324, '2015-05-19 09:12:46', 2, 12, 10, 162),
(3003, '2015-07-28 16:26:01', 7, 91, 142, NULL),
(2287, '2015-07-17 15:02:06', 7, 12, 213, NULL),
(1328, '2015-05-19 10:22:30', 0, 131, 158, 163),
(2946, '2015-07-28 14:03:49', 7, 12, 194, NULL),
(3007, '2015-07-28 16:27:49', 7, 239, 12, NULL),
(3004, '2015-07-28 16:27:01', 7, 239, 12, NULL),
(1344, '2015-05-19 10:41:07', 2, 131, 140, 161),
(3008, '2015-07-28 16:35:50', 7, 217, 142, NULL),
(1350, '2015-05-19 10:41:47', 2, 131, 101, 133),
(1351, '2015-05-19 10:41:47', 2, 131, 140, 133),
(1353, '2015-05-19 10:47:47', 6, 131, 179, 141),
(1755, '2015-06-20 11:06:03', 0, 91, 188, 247),
(3021, '2015-07-28 17:25:06', 7, 165, 142, NULL),
(1361, '2015-05-19 10:57:25', 2, 131, 71, 148),
(3052, '2015-07-28 20:27:24', 7, 194, 252, NULL),
(1364, '2015-05-19 10:57:27', 2, 131, 140, 148),
(2967, '2015-07-28 14:20:36', 7, 194, 91, NULL),
(1414, '2015-05-20 09:39:48', 2, 131, 179, 174),
(1374, '2015-05-19 11:02:05', 0, 131, 158, 164),
(2187, '2015-07-15 12:43:36', 4, 198, 197, NULL),
(3714, '2015-08-18 19:24:57', 4, 287, 12, NULL),
(1379, '2015-05-19 11:03:28', 7, 131, 179, NULL),
(1380, '2015-05-19 11:03:41', 7, 131, 179, NULL),
(1381, '2015-05-19 11:03:53', 7, 131, 179, NULL),
(1958, '2015-06-30 08:23:14', 2, 197, 199, 263),
(1383, '2015-05-19 11:04:06', 7, 131, 179, NULL),
(1720, '2015-06-17 13:24:11', 7, 182, 142, NULL),
(1388, '2015-05-19 11:14:29', 7, 131, 71, NULL),
(3695, '2015-08-18 13:31:36', 4, 252, 314, NULL),
(1391, '2015-05-19 12:09:01', 7, 131, 142, NULL),
(1392, '2015-05-19 12:09:24', 7, 131, 179, NULL),
(2646, '2015-07-24 22:18:05', 7, 91, 165, NULL),
(2700, '2015-07-25 13:54:31', 7, 12, 165, NULL),
(1396, '2015-05-19 13:36:44', 0, 12, 14, 165),
(1397, '2015-05-19 13:36:44', 0, 12, 16, 165),
(1398, '2015-05-19 13:36:44', 0, 12, 10, 165),
(2810, '2015-07-27 14:13:21', 7, 131, 186, NULL),
(1402, '2015-05-19 13:36:44', 0, 12, 168, 165),
(1405, '2015-05-19 14:20:14', 7, 131, 179, NULL),
(3368, '2015-07-30 18:18:14', 7, 182, 163, NULL),
(1421, '2015-05-20 13:28:15', 2, 12, 142, 165),
(2306, '2015-07-20 07:58:17', 6, 214, 195, 294),
(1426, '2015-05-21 14:29:06', 5, 12, 180, 172),
(1425, '2015-05-21 14:28:11', 6, 12, 168, 165),
(3320, '2015-07-30 17:50:53', 7, 182, 163, NULL),
(3719, '2015-08-18 19:31:37', 7, 165, 287, NULL),
(2889, '2015-07-28 11:55:05', 7, 182, 224, NULL),
(3499, '2015-07-31 15:52:27', 7, 182, 131, NULL),
(3648, '2015-08-12 11:07:27', 7, 277, 91, NULL),
(1457, '2015-05-26 11:04:05', 2, 131, 179, 155),
(1458, '2015-05-26 11:04:05', 2, 131, 168, 155),
(1459, '2015-05-26 11:04:07', 2, 131, 142, 155),
(1460, '2015-05-26 11:04:08', 2, 131, 140, 155),
(1463, '2015-05-26 11:04:10', 2, 131, 71, 155),
(3167, '2015-07-30 10:24:49', 7, 163, 263, NULL),
(2849, '2015-07-28 08:02:33', 7, 194, 91, NULL),
(1483, '2015-05-28 07:20:35', 6, 91, 142, 155),
(1469, '2015-05-26 11:05:31', 2, 12, 71, 161),
(1471, '2015-05-26 11:05:33', 2, 12, 142, 161),
(1890, '2015-06-27 06:32:52', 2, 91, 188, 246),
(2709, '2015-07-25 21:59:22', 1, 91, 165, 241),
(1535, '2015-06-10 13:25:13', 0, 131, 168, 228),
(3302, '2015-07-30 15:22:48', 4, 163, 266, NULL),
(1540, '2015-06-10 14:50:53', 6, 91, 168, 222),
(1545, '2015-06-11 09:17:17', 0, 12, 10, 229),
(1863, '2015-06-24 15:35:02', 3, 12, 190, NULL),
(3577, '2015-08-04 13:56:20', 7, 163, 182, NULL),
(1548, '2015-06-11 09:17:17', 0, 12, 169, 229),
(1549, '2015-06-11 09:17:17', 0, 12, 168, 229),
(3693, '2015-08-18 12:53:49', 4, 12, 314, NULL),
(3461, '2015-07-31 13:29:48', 7, 182, 131, NULL),
(2952, '2015-07-28 14:05:02', 1, 194, 12, 326),
(3612, '2015-08-08 10:39:21', 7, 165, 252, NULL),
(3243, '2015-07-30 13:23:23', 1, 163, 263, 346),
(1571, '2015-06-12 17:01:32', 2, 91, 101, 222),
(1570, '2015-06-12 17:00:46', 7, 91, 188, NULL),
(1569, '2015-06-12 17:00:13', 7, 91, 188, NULL),
(1676, '2015-06-16 14:38:11', 1, 182, 140, 194),
(3579, '2015-08-04 13:56:53', 7, 182, 163, NULL),
(2977, '2015-07-28 14:38:30', 1, 91, 12, 248),
(1944, '2015-06-30 06:20:07', 6, 91, 195, 245),
(2682, '2015-07-25 11:46:12', 6, 206, 165, 282),
(3314, '2015-07-30 16:52:20', 2, 182, 163, 341),
(1759, '2015-06-20 12:11:37', 0, 91, 188, 248),
(1598, '2015-06-15 18:51:44', 0, 12, 10, 238),
(1908, '2015-06-29 08:21:55', 2, 197, 198, 259),
(3507, '2015-07-31 15:57:37', 4, 182, 131, NULL),
(1602, '2015-06-15 18:51:44', 0, 12, 168, 238),
(3501, '2015-07-31 15:53:12', 4, 131, 182, NULL),
(1613, '2015-06-16 09:21:14', 6, 182, 140, 194),
(1617, '2015-06-16 10:07:58', 4, 182, 142, NULL),
(1621, '2015-06-16 10:37:59', 6, 142, 188, 230),
(3349, '2015-07-30 18:14:05', 7, 182, 163, NULL),
(1624, '2015-06-16 10:38:53', 1, 142, 188, 230),
(2749, '2015-07-26 20:32:06', 2, 165, 194, 316),
(1627, '2015-06-16 10:40:53', 1, 142, 188, 230),
(2717, '2015-07-26 11:43:36', 4, 165, 194, NULL),
(2670, '2015-07-25 05:40:16', 0, 182, 179, 313),
(1632, '2015-06-16 12:52:12', 2, 12, 10, 160),
(1633, '2015-06-16 12:52:12', 2, 12, 142, 160),
(1634, '2015-06-16 12:52:13', 2, 12, 90, 160),
(1642, '2015-06-16 12:53:59', 6, 91, 142, 160),
(1639, '2015-06-16 12:53:50', 2, 142, 177, 160),
(2775, '2015-07-27 08:25:50', 0, 131, 169, 317),
(1648, '2015-06-16 12:59:51', 0, 12, 16, 239),
(1649, '2015-06-16 12:59:51', 0, 12, 10, 239),
(2094, '2015-07-05 14:40:54', 1, 91, 195, 240),
(3557, '2015-08-04 08:47:33', 7, 131, 182, NULL),
(1653, '2015-06-16 12:59:51', 0, 12, 168, 239),
(3629, '2015-08-12 11:01:10', 7, 91, 277, NULL),
(3559, '2015-08-04 08:49:47', 7, 182, 131, NULL),
(1664, '2015-06-16 13:22:40', 2, 12, 142, 239),
(1665, '2015-06-16 13:22:41', 2, 12, 168, 239),
(1667, '2015-06-16 13:22:42', 2, 12, 71, 239),
(1669, '2015-06-16 13:22:42', 2, 12, 10, 239),
(2957, '2015-07-28 14:10:09', 1, 194, 12, 326),
(2140, '2015-07-10 13:06:51', 2, 12, 194, 270),
(1694, '2015-06-17 12:56:35', 1, 142, 188, 230),
(2283, '2015-07-17 13:20:27', 2, 165, 213, 251),
(1700, '2015-06-17 12:59:58', 1, 182, 140, 194),
(2309, '2015-07-20 07:58:24', 6, 214, 195, 293),
(1709, '2015-06-17 13:20:52', 1, 142, 188, 230),
(3028, '2015-07-28 17:38:40', 7, 239, 12, NULL),
(1712, '2015-06-17 13:21:12', 1, 142, 188, 230),
(3329, '2015-07-30 18:01:07', 7, 182, 163, NULL),
(1715, '2015-06-17 13:21:45', 1, 142, 188, 230),
(2380, '2015-07-20 10:33:06', 2, 214, 213, 241),
(2435, '2015-07-20 15:03:21', 7, 163, 214, NULL),
(3590, '2015-08-05 07:26:12', 7, 182, 163, NULL),
(1722, '2015-06-17 13:34:01', 2, 182, 142, 187),
(1723, '2015-06-17 13:35:40', 2, 182, 142, 230),
(3562, '2015-08-04 08:54:06', 7, 131, 182, NULL),
(1727, '2015-06-18 09:28:26', 0, 12, 16, 244),
(1728, '2015-06-18 09:28:26', 0, 12, 10, 244),
(2188, '2015-07-15 12:45:49', 0, 197, 199, 286),
(1732, '2015-06-18 09:28:26', 0, 12, 168, 244),
(1790, '2015-06-22 14:19:25', 7, 142, 189, NULL),
(1789, '2015-06-22 14:17:14', 7, 142, 189, NULL),
(1788, '2015-06-22 14:12:56', 7, 142, 189, NULL),
(3169, '2015-07-30 10:31:47', 1, 163, 263, 346),
(3537, '2015-08-03 13:09:10', 4, 182, 228, NULL),
(2780, '2015-07-27 08:27:34', 0, 131, 135, 318),
(2909, '2015-07-28 12:28:10', 7, 198, 199, NULL),
(1743, '2015-06-18 12:39:50', 7, 182, 142, NULL),
(3155, '2015-07-30 09:41:50', 4, 182, 228, NULL),
(2488, '2015-07-21 09:56:57', 7, 12, 213, NULL),
(2550, '2015-07-23 16:04:54', 7, 198, 142, NULL),
(1794, '2015-06-22 14:21:19', 7, 189, 142, NULL),
(1795, '2015-06-22 14:21:37', 7, 142, 189, NULL),
(1796, '2015-06-22 14:22:18', 7, 142, 189, NULL),
(1797, '2015-06-22 14:24:56', 7, 142, 189, NULL),
(1798, '2015-06-22 14:27:20', 7, 142, 189, NULL),
(1799, '2015-06-22 14:27:25', 7, 142, 189, NULL),
(1800, '2015-06-22 14:27:32', 7, 142, 189, NULL),
(3180, '2015-07-30 10:40:54', 6, 182, 228, 347),
(2863, '2015-07-28 10:45:35', 7, 182, 224, NULL),
(3672, '2015-08-15 14:16:16', 6, 91, 195, 384),
(3341, '2015-07-30 18:05:50', 7, 182, 163, NULL),
(3201, '2015-07-30 11:11:47', 4, 239, 131, NULL),
(3006, '2015-07-28 16:27:21', 7, 239, 12, NULL),
(1807, '2015-06-22 15:54:55', 0, 91, 168, 251),
(2773, '2015-07-27 08:25:50', 0, 131, 158, 317),
(1809, '2015-06-22 15:54:55', 0, 91, 188, 251),
(1810, '2015-06-22 15:54:55', 0, 91, 190, 251),
(3214, '2015-07-30 12:20:15', 1, 163, 263, 346),
(2887, '2015-07-28 11:54:41', 7, 182, 198, NULL),
(2292, '2015-07-17 15:47:33', 2, 165, 214, 264),
(1821, '2015-06-23 16:45:55', 0, 12, 16, 252),
(1822, '2015-06-23 16:45:55', 0, 12, 10, 252),
(2569, '2015-07-24 15:38:43', 7, 165, 217, NULL),
(3197, '2015-07-30 10:54:09', 2, 182, 228, 342),
(1825, '2015-06-23 16:45:55', 0, 12, 169, 252),
(1826, '2015-06-23 16:45:55', 0, 12, 168, 252),
(1827, '2015-06-23 17:19:44', 0, 12, 14, 253),
(1828, '2015-06-23 17:19:44', 0, 12, 16, 253),
(1829, '2015-06-23 17:19:44', 0, 12, 10, 253),
(2821, '2015-07-27 16:02:49', 1, 91, 12, 248),
(1832, '2015-06-23 17:19:44', 0, 12, 169, 253),
(1833, '2015-06-23 17:19:44', 0, 12, 168, 253),
(1834, '2015-06-23 17:42:51', 0, 12, 14, 254),
(1835, '2015-06-23 17:42:51', 0, 12, 16, 254),
(1836, '2015-06-23 17:42:51', 0, 12, 10, 254),
(3005, '2015-07-28 16:27:06', 7, 239, 12, NULL),
(1839, '2015-06-23 17:42:51', 0, 12, 169, 254),
(1840, '2015-06-23 17:42:51', 0, 12, 168, 254),
(2850, '2015-07-28 08:32:45', 7, 91, 194, NULL),
(1853, '2015-06-24 14:20:30', 0, 12, 10, 256),
(2899, '2015-07-28 11:59:14', 2, 165, 205, 324),
(1856, '2015-06-24 14:20:30', 0, 12, 169, 256),
(1857, '2015-06-24 14:20:30', 0, 12, 168, 256),
(3062, '2015-07-29 06:00:22', 5, 223, 245, 328),
(1864, '2015-06-24 16:34:54', 7, 91, 190, NULL),
(1865, '2015-06-24 16:35:37', 7, 91, 190, NULL),
(2940, '2015-07-28 13:59:20', 6, 194, 12, 326),
(1868, '2015-06-26 13:45:12', 7, 182, 142, NULL),
(3339, '2015-07-30 18:05:30', 7, 182, 163, NULL),
(2806, '2015-07-27 14:11:47', 4, 131, 186, NULL),
(3490, '2015-07-31 15:26:17', 7, 228, 182, NULL),
(3726, '2015-08-19 08:38:23', 6, 194, 205, 326),
(3277, '2015-07-30 14:41:16', 4, 163, 266, NULL),
(3519, '2015-08-02 08:57:40', 4, 194, 241, NULL),
(3438, '2015-07-31 12:42:35', 1, 214, 214, 294),
(3146, '2015-07-29 18:35:08', 7, 165, 258, NULL),
(3335, '2015-07-30 18:04:24', 6, 182, 163, 349),
(3725, '2015-08-19 08:29:22', 4, 205, 165, NULL),
(1889, '2015-06-27 06:32:51', 2, 91, 194, 246),
(2696, '2015-07-25 13:46:24', 7, 12, 165, NULL),
(3575, '2015-08-04 13:56:11', 7, 163, 182, NULL),
(2955, '2015-07-28 14:10:09', 1, 194, 194, 326),
(3440, '2015-07-31 13:05:54', 0, 142, 91, 363),
(1894, '2015-06-27 12:15:51', 7, 91, 188, NULL),
(2960, '2015-07-28 14:11:45', 1, 194, 12, 326),
(2008, '2015-06-30 09:48:18', 7, 199, 197, NULL),
(2009, '2015-06-30 09:48:32', 7, 199, 197, NULL),
(1911, '2015-06-29 08:41:46', 2, 197, 198, 260),
(1910, '2015-06-29 08:22:57', 6, 197, 198, 259),
(1914, '2015-06-29 08:45:10', 2, 197, 198, 261),
(1913, '2015-06-29 08:42:22', 6, 197, 198, 260),
(2010, '2015-06-30 09:48:41', 7, 199, 197, NULL);
INSERT INTO `notifications` (`id`, `date`, `type`, `idsender`, `idreceiver`, `idevent`) VALUES
(1916, '2015-06-29 08:45:50', 6, 197, 198, 261),
(2012, '2015-06-30 09:49:09', 7, 199, 197, NULL),
(1919, '2015-06-29 09:36:20', 7, 197, 198, NULL),
(2011, '2015-06-30 09:48:44', 7, 199, 197, NULL),
(1921, '2015-06-29 09:37:12', 6, 198, 199, 258),
(2014, '2015-06-30 09:50:17', 7, 199, 197, NULL),
(2013, '2015-06-30 09:49:10', 7, 199, 197, NULL),
(1926, '2015-06-29 09:41:50', 4, 198, 199, NULL),
(1928, '2015-06-29 09:50:04', 4, 197, 199, NULL),
(1930, '2015-06-29 10:50:32', 1, 198, 198, 258),
(1931, '2015-06-29 10:50:32', 1, 198, 199, 258),
(3701, '2015-08-18 13:35:24', 2, 12, 168, 385),
(2816, '2015-07-27 14:27:06', 7, 226, 225, NULL),
(1957, '2015-06-30 07:49:53', 6, 12, 195, 256),
(3717, '2015-08-18 19:28:34', 7, 165, 287, NULL),
(1950, '2015-06-30 07:43:46', 2, 197, 199, 255),
(2015, '2015-06-30 09:50:29', 7, 199, 197, NULL),
(2664, '2015-07-25 05:34:45', 0, 182, 179, 312),
(2016, '2015-06-30 09:50:48', 7, 197, 199, NULL),
(1954, '2015-06-30 07:46:19', 2, 197, 199, 256),
(2638, '2015-07-24 22:09:54', 7, 91, 165, NULL),
(1956, '2015-06-30 07:49:49', 6, 12, 195, 252),
(1959, '2015-06-30 08:24:02', 2, 197, 199, 253),
(1960, '2015-06-30 08:24:35', 2, 197, 199, 246),
(1961, '2015-06-30 08:25:18', 2, 197, 199, 248),
(1962, '2015-06-30 08:25:56', 7, 197, 199, NULL),
(2019, '2015-06-30 09:55:04', 7, 199, 197, NULL),
(2020, '2015-06-30 09:55:13', 7, 199, 197, NULL),
(2021, '2015-06-30 09:55:14', 7, 199, 197, NULL),
(2022, '2015-06-30 09:55:16', 7, 199, 197, NULL),
(2023, '2015-06-30 09:55:33', 7, 199, 197, NULL),
(2024, '2015-06-30 09:55:53', 7, 199, 197, NULL),
(2025, '2015-06-30 09:56:17', 7, 199, 197, NULL),
(1970, '2015-06-30 08:35:19', 7, 197, 199, NULL),
(2026, '2015-06-30 09:56:24', 7, 199, 197, NULL),
(2027, '2015-06-30 09:56:29', 7, 199, 197, NULL),
(2028, '2015-06-30 09:56:45', 7, 199, 197, NULL),
(2029, '2015-06-30 10:42:47', 7, 197, 199, NULL),
(2076, '2015-07-02 09:29:43', 1, 197, 198, 261),
(3340, '2015-07-30 18:05:39', 7, 182, 163, NULL),
(1977, '2015-06-30 08:56:39', 7, 197, 199, NULL),
(1978, '2015-06-30 08:56:59', 7, 197, 199, NULL),
(2134, '2015-07-09 16:23:54', 3, 199, 197, NULL),
(2703, '2015-07-25 21:53:54', 1, 91, 165, 248),
(2162, '2015-07-13 09:26:27', 7, 203, 197, NULL),
(2163, '2015-07-13 16:23:30', 7, 205, 197, NULL),
(2732, '2015-07-26 12:27:55', 7, 194, 165, NULL),
(3523, '2015-08-03 10:42:43', 1, 182, 182, 345),
(2196, '2015-07-15 13:14:28', 4, 198, 197, NULL),
(2191, '2015-07-15 13:08:08', 4, 198, 197, NULL),
(2194, '2015-07-15 13:13:20', 4, 198, 197, NULL),
(2362, '2015-07-20 10:16:57', 6, 91, 206, 251),
(2870, '2015-07-28 11:11:21', 0, 198, 168, 323),
(1990, '2015-06-30 09:18:18', 7, 199, 197, NULL),
(1991, '2015-06-30 09:18:59', 7, 199, 197, NULL),
(1992, '2015-06-30 09:24:23', 7, 199, 197, NULL),
(1993, '2015-06-30 09:29:28', 7, 199, 197, NULL),
(1994, '2015-06-30 09:35:59', 7, 199, 197, NULL),
(1995, '2015-06-30 09:36:35', 7, 199, 197, NULL),
(1996, '2015-06-30 09:37:23', 7, 199, 197, NULL),
(1997, '2015-06-30 09:40:44', 7, 199, 197, NULL),
(1998, '2015-06-30 09:43:39', 7, 199, 197, NULL),
(1999, '2015-06-30 09:44:30', 7, 199, 197, NULL),
(2000, '2015-06-30 09:44:55', 7, 199, 197, NULL),
(2001, '2015-06-30 09:47:02', 7, 199, 197, NULL),
(2002, '2015-06-30 09:47:20', 7, 199, 197, NULL),
(2003, '2015-06-30 09:47:29', 7, 199, 197, NULL),
(2004, '2015-06-30 09:47:52', 7, 199, 197, NULL),
(2005, '2015-06-30 09:47:56', 7, 199, 197, NULL),
(2006, '2015-06-30 09:47:58', 7, 199, 197, NULL),
(2007, '2015-06-30 09:48:17', 7, 199, 197, NULL),
(2017, '2015-06-30 09:51:06', 7, 197, 199, NULL),
(2018, '2015-06-30 09:54:53', 7, 199, 197, NULL),
(2030, '2015-06-30 10:42:52', 7, 197, 199, NULL),
(2031, '2015-06-30 10:43:18', 7, 197, 199, NULL),
(2032, '2015-06-30 10:48:57', 7, 197, 199, NULL),
(2033, '2015-06-30 10:49:20', 7, 197, 199, NULL),
(2034, '2015-06-30 10:51:16', 7, 197, 199, NULL),
(2035, '2015-06-30 10:51:22', 7, 197, 199, NULL),
(2036, '2015-06-30 10:53:18', 7, 197, 199, NULL),
(2037, '2015-06-30 10:53:47', 7, 197, 199, NULL),
(2038, '2015-06-30 10:59:18', 7, 197, 199, NULL),
(2039, '2015-06-30 11:00:18', 7, 197, 199, NULL),
(2040, '2015-06-30 11:04:38', 7, 197, 199, NULL),
(2041, '2015-06-30 11:07:45', 7, 197, 199, NULL),
(2042, '2015-06-30 11:11:38', 7, 197, 199, NULL),
(2043, '2015-06-30 11:38:54', 7, 197, 199, NULL),
(2044, '2015-06-30 11:40:37', 7, 197, 199, NULL),
(2045, '2015-06-30 11:59:36', 7, 197, 199, NULL),
(2046, '2015-06-30 12:43:27', 7, 197, 199, NULL),
(2047, '2015-06-30 12:46:53', 7, 197, 199, NULL),
(2048, '2015-06-30 12:47:04', 7, 197, 199, NULL),
(2049, '2015-06-30 12:49:35', 7, 197, 199, NULL),
(2050, '2015-06-30 12:53:38', 7, 197, 199, NULL),
(2051, '2015-06-30 12:53:42', 7, 197, 199, NULL),
(2052, '2015-06-30 12:53:44', 7, 197, 199, NULL),
(2053, '2015-06-30 12:53:47', 7, 197, 199, NULL),
(2054, '2015-06-30 12:53:51', 7, 197, 199, NULL),
(2055, '2015-06-30 12:54:20', 7, 197, 199, NULL),
(2070, '2015-07-01 10:37:43', 3, 203, 199, NULL),
(2986, '2015-07-28 15:04:36', 7, 194, 12, NULL),
(2059, '2015-07-01 09:39:38', 4, 202, 199, NULL),
(3374, '2015-07-30 18:20:13', 7, 182, 163, NULL),
(2064, '2015-07-01 10:02:50', 4, 199, 198, NULL),
(3652, '2015-08-12 11:14:57', 7, 91, 277, NULL),
(3122, '2015-07-29 11:57:26', 7, 194, 12, NULL),
(2069, '2015-07-01 10:12:49', 4, 199, 203, NULL),
(2071, '2015-07-01 10:38:57', 4, 198, 203, NULL),
(2073, '2015-07-01 10:47:13', 6, 198, 203, 266),
(2075, '2015-07-02 09:29:43', 1, 197, 197, 261),
(2639, '2015-07-24 22:10:12', 7, 91, 165, NULL),
(2943, '2015-07-28 14:02:17', 7, 194, 12, NULL),
(3148, '2015-07-29 18:45:32', 7, 165, 258, NULL),
(3721, '2015-08-18 19:36:10', 7, 165, 12, NULL),
(3145, '2015-07-29 18:31:16', 7, 258, 165, NULL),
(3728, '2015-08-19 10:59:06', 4, 217, 314, NULL),
(2695, '2015-07-25 13:44:20', 7, 12, 165, NULL),
(3627, '2015-08-12 11:00:45', 7, 91, 277, NULL),
(2642, '2015-07-24 22:12:59', 7, 91, 165, NULL),
(2095, '2015-07-05 14:40:54', 1, 91, 188, 240),
(2096, '2015-07-06 12:53:48', 0, 12, 14, 270),
(2097, '2015-07-06 12:53:48', 0, 12, 16, 270),
(2098, '2015-07-06 12:53:48', 0, 12, 10, 270),
(2644, '2015-07-24 22:15:22', 7, 91, 165, NULL),
(2102, '2015-07-06 12:53:48', 0, 12, 168, 270),
(2106, '2015-07-07 11:20:52', 7, 197, 199, NULL),
(2966, '2015-07-28 14:19:40', 1, 194, 12, 326),
(3632, '2015-08-12 11:01:42', 7, 91, 277, NULL),
(3121, '2015-07-29 11:57:26', 7, 194, 12, NULL),
(3696, '2015-08-18 13:35:21', 2, 12, 241, 385),
(2968, '2015-07-28 14:21:35', 7, 91, 194, NULL),
(2126, '2015-07-09 14:15:28', 1, 200, 200, 267),
(2133, '2015-07-09 15:11:22', 7, 199, 197, NULL),
(3655, '2015-08-12 11:20:28', 7, 277, 91, NULL),
(2368, '2015-07-20 10:18:48', 7, 91, 213, NULL),
(2649, '2015-07-25 05:19:48', 0, 182, 179, 308),
(3488, '2015-07-31 15:15:53', 1, 182, 182, 345),
(2655, '2015-07-25 05:23:05', 0, 182, 179, 309),
(2713, '2015-07-25 22:01:03', 1, 12, 195, 256),
(2658, '2015-07-25 05:26:51', 0, 182, 179, 310),
(2161, '2015-07-13 09:10:07', 7, 182, 197, NULL),
(3600, '2015-08-05 15:19:28', 4, 272, 277, NULL),
(2182, '2015-07-15 09:32:20', 4, 208, 207, NULL),
(3658, '2015-08-12 11:44:59', 7, 91, 277, NULL),
(2686, '2015-07-25 11:53:58', 7, 12, 205, NULL),
(2661, '2015-07-25 05:29:37', 0, 182, 179, 311),
(2683, '2015-07-25 11:46:14', 4, 206, 165, NULL),
(3662, '2015-08-12 15:54:17', 1, 285, 243, 382),
(2759, '2015-07-26 21:29:58', 4, 205, 165, NULL),
(2745, '2015-07-26 16:12:03', 0, 205, 194, 316),
(2822, '2015-07-27 16:02:49', 1, 91, 165, 248),
(2879, '2015-07-28 11:50:46', 2, 165, 12, 324),
(2928, '2015-07-28 13:07:57', 4, 244, 245, NULL),
(2242, '2015-07-17 07:55:17', 3, 212, 179, NULL),
(2710, '2015-07-25 21:59:22', 1, 91, 214, 241),
(2251, '2015-07-17 09:31:38', 7, 214, 213, NULL),
(2851, '2015-07-28 08:34:09', 7, 194, 91, NULL),
(2247, '2015-07-17 09:30:43', 6, 214, 213, 294),
(2250, '2015-07-17 09:31:15', 4, 213, 214, NULL),
(2252, '2015-07-17 09:31:46', 7, 214, 213, NULL),
(2253, '2015-07-17 09:31:52', 7, 213, 214, NULL),
(2254, '2015-07-17 09:57:19', 0, 213, 214, 296),
(2257, '2015-07-17 10:08:14', 0, 214, 213, 297),
(2256, '2015-07-17 09:58:45', 6, 213, 214, 296),
(3209, '2015-07-30 11:21:06', 2, 182, 228, 349),
(3044, '2015-07-28 19:12:42', 4, 217, 194, NULL),
(2268, '2015-07-17 12:16:58', 2, 165, 213, 252),
(2697, '2015-07-25 13:47:33', 7, 12, 165, NULL),
(2882, '2015-07-28 11:51:42', 2, 165, 214, 324),
(3060, '2015-07-29 05:55:08', 6, 239, 258, 324),
(2698, '2015-07-25 13:48:54', 7, 12, 165, NULL),
(2271, '2015-07-17 12:17:50', 4, 165, 213, NULL),
(3353, '2015-07-30 18:14:36', 7, 182, 163, NULL),
(2936, '2015-07-28 13:52:48', 2, 194, 91, 326),
(2944, '2015-07-28 14:03:26', 7, 12, 194, NULL),
(2942, '2015-07-28 14:02:14', 7, 12, 194, NULL),
(2284, '2015-07-17 13:20:28', 2, 165, 214, 251),
(3125, '2015-07-29 12:19:46', 7, 194, 243, NULL),
(2290, '2015-07-17 15:47:31', 2, 165, 213, 264),
(2740, '2015-07-26 13:53:23', 7, 12, 194, NULL),
(2293, '2015-07-18 07:42:51', 7, 12, 213, NULL),
(2681, '2015-07-25 11:46:10', 6, 206, 205, 282),
(3387, '2015-07-30 18:24:05', 1, 163, 263, 346),
(2772, '2015-07-27 08:25:50', 0, 131, 135, 317),
(2351, '2015-07-20 09:30:13', 1, 213, 214, 299),
(3654, '2015-08-12 11:20:02', 7, 277, 91, NULL),
(2948, '2015-07-28 14:04:14', 7, 194, 12, NULL),
(2307, '2015-07-20 07:58:19', 6, 214, 195, 297),
(3525, '2015-08-03 12:51:58', 4, 182, 228, NULL),
(3152, '2015-07-30 08:13:25', 7, 12, 213, NULL),
(3041, '2015-07-28 19:11:47', 4, 217, 252, NULL),
(2730, '2015-07-26 12:26:05', 7, 165, 194, NULL),
(3451, '2015-07-31 13:27:06', 7, 182, 131, NULL),
(3522, '2015-08-03 09:59:10', 7, 182, 228, NULL),
(2728, '2015-07-26 12:04:44', 1, 194, 194, 315),
(3393, '2015-07-30 18:28:38', 7, 182, 163, NULL),
(3101, '2015-07-29 10:12:13', 2, 131, 135, 299),
(3364, '2015-07-30 18:17:34', 7, 182, 163, NULL),
(2872, '2015-07-28 11:12:22', 4, 182, 198, NULL),
(2757, '2015-07-26 21:10:30', 2, 165, 213, 316),
(3059, '2015-07-29 05:55:05', 6, 239, 165, 324),
(2886, '2015-07-28 11:54:40', 7, 182, 198, NULL),
(2451, '2015-07-20 15:26:57', 2, 163, 214, 282),
(3420, '2015-07-31 10:16:52', 7, 163, 131, NULL),
(3570, '2015-08-04 12:38:07', 7, 163, 131, NULL),
(3569, '2015-08-04 12:34:25', 7, 163, 131, NULL),
(3568, '2015-08-04 12:33:35', 7, 163, 131, NULL),
(2792, '2015-07-27 08:38:11', 7, 91, 194, NULL),
(3567, '2015-08-04 12:32:59', 7, 163, 131, NULL),
(3422, '2015-07-31 10:24:00', 7, 163, 131, NULL),
(3566, '2015-08-04 11:30:03', 7, 163, 131, NULL),
(2475, '2015-07-21 08:59:14', 7, 12, 213, NULL),
(2896, '2015-07-28 11:58:30', 2, 165, 194, 324),
(3564, '2015-08-04 09:53:11', 7, 182, 131, NULL),
(3210, '2015-07-30 11:21:30', 2, 182, 228, 350),
(2898, '2015-07-28 11:59:13', 2, 165, 91, 324),
(2993, '2015-07-28 16:24:30', 7, 163, 142, NULL),
(2997, '2015-07-28 16:24:51', 7, 163, 142, NULL),
(2949, '2015-07-28 14:04:15', 7, 194, 12, NULL),
(3051, '2015-07-28 20:27:06', 7, 194, 252, NULL),
(3291, '2015-07-30 14:51:31', 4, 163, 266, NULL),
(3050, '2015-07-28 19:27:39', 7, 252, 12, NULL),
(2346, '2015-07-20 09:28:35', 0, 213, 214, 299),
(3135, '2015-07-29 12:33:36', 7, 12, 194, NULL),
(2848, '2015-07-28 07:59:39', 7, 194, 91, NULL),
(2349, '2015-07-20 09:29:12', 6, 213, 214, 299),
(2352, '2015-07-20 09:33:22', 7, 214, 213, NULL),
(2353, '2015-07-20 09:33:40', 7, 214, 213, NULL),
(2354, '2015-07-20 09:34:37', 7, 214, 213, NULL),
(2355, '2015-07-20 09:36:42', 7, 213, 214, NULL),
(2356, '2015-07-20 09:40:18', 7, 214, 213, NULL),
(2566, '2015-07-24 08:32:14', 4, 214, 217, NULL),
(2358, '2015-07-20 10:16:43', 6, 91, 214, 241),
(3555, '2015-08-04 08:44:02', 7, 131, 182, NULL),
(3058, '2015-07-29 05:55:02', 6, 239, 214, 324),
(2372, '2015-07-20 10:29:31', 7, 91, 213, NULL),
(2916, '2015-07-28 12:33:24', 7, 182, 198, NULL),
(2777, '2015-07-27 08:25:50', 0, 131, 168, 317),
(2375, '2015-07-20 10:32:20', 1, 91, 195, 241),
(2733, '2015-07-26 12:28:30', 7, 165, 194, NULL),
(2377, '2015-07-20 10:32:20', 1, 91, 214, 241),
(2378, '2015-07-20 10:32:20', 1, 91, 206, 241),
(2953, '2015-07-28 14:06:24', 7, 91, 194, NULL),
(3299, '2015-07-30 15:18:45', 4, 163, 266, NULL),
(3553, '2015-08-04 08:43:58', 7, 131, 182, NULL),
(3325, '2015-07-30 18:00:03', 7, 182, 163, NULL),
(3327, '2015-07-30 18:00:38', 7, 182, 163, NULL),
(3045, '2015-07-28 19:15:05', 7, 12, 239, NULL),
(3309, '2015-07-30 15:26:17', 4, 163, 266, NULL),
(3354, '2015-07-30 18:15:01', 2, 182, 163, 354),
(2405, '2015-07-20 13:56:04', 1, 213, 214, 299),
(3551, '2015-08-03 15:06:47', 7, 182, 131, NULL),
(3435, '2015-07-31 12:42:35', 1, 214, 213, 294),
(3649, '2015-08-12 11:12:41', 7, 91, 277, NULL),
(3596, '2015-08-05 07:27:37', 7, 163, 182, NULL),
(3346, '2015-07-30 18:12:30', 7, 182, 163, NULL),
(3571, '2015-08-04 12:39:08', 7, 163, 131, NULL),
(3417, '2015-07-31 10:11:37', 7, 163, 131, NULL),
(2799, '2015-07-27 09:07:26', 7, 91, 194, NULL),
(2869, '2015-07-28 11:11:21', 0, 198, 197, 323),
(3656, '2015-08-12 11:21:23', 7, 91, 277, NULL),
(2431, '2015-07-20 15:00:13', 3, 163, 177, NULL),
(3485, '2015-07-31 15:07:24', 1, 182, 182, 345),
(3288, '2015-07-30 14:46:36', 4, 163, 266, NULL),
(3164, '2015-07-30 10:24:03', 6, 163, 263, 346),
(2867, '2015-07-28 11:04:09', 7, 182, 224, NULL),
(3384, '2015-07-30 18:23:41', 7, 182, 163, NULL),
(3192, '2015-07-30 10:45:24', 7, 163, 264, NULL),
(3246, '2015-07-30 13:26:10', 6, 163, 265, 346),
(3379, '2015-07-30 18:22:10', 7, 182, 163, NULL),
(3493, '2015-07-31 15:43:19', 7, 131, 182, NULL),
(3418, '2015-07-31 10:12:59', 7, 163, 131, NULL),
(2800, '2015-07-27 09:07:34', 7, 91, 194, NULL),
(2961, '2015-07-28 14:16:56', 7, 245, 244, NULL),
(2739, '2015-07-26 13:53:01', 6, 194, 165, 315),
(2680, '2015-07-25 11:46:08', 6, 206, 195, 282),
(2679, '2015-07-25 11:46:03', 4, 206, 168, NULL),
(3419, '2015-07-31 10:13:45', 7, 163, 131, NULL),
(2785, '2015-07-27 08:27:34', 0, 131, 168, 318),
(2467, '2015-07-21 08:05:24', 7, 214, 213, NULL),
(2468, '2015-07-21 08:05:39', 7, 213, 214, NULL),
(2469, '2015-07-21 08:05:57', 7, 213, 214, NULL),
(2470, '2015-07-21 08:06:30', 7, 213, 214, NULL),
(3421, '2015-07-31 10:17:32', 7, 163, 131, NULL),
(3423, '2015-07-31 10:25:37', 7, 163, 131, NULL),
(2791, '2015-07-27 08:38:06', 7, 91, 194, NULL),
(2964, '2015-07-28 14:19:40', 1, 194, 194, 326),
(2678, '2015-07-25 11:46:02', 6, 206, 168, 282),
(3456, '2015-07-31 13:27:20', 7, 182, 131, NULL),
(3002, '2015-07-28 16:25:41', 7, 188, 142, NULL),
(3293, '2015-07-30 15:05:31', 4, 163, 266, NULL),
(3015, '2015-07-28 16:41:46', 7, 194, 142, NULL),
(3036, '2015-07-28 19:09:08', 4, 194, 252, NULL),
(3593, '2015-08-05 07:27:04', 7, 163, 182, NULL),
(3541, '2015-08-03 13:17:39', 4, 182, 228, NULL),
(3369, '2015-07-30 18:18:22', 7, 182, 163, NULL),
(3578, '2015-08-04 13:56:27', 7, 163, 182, NULL),
(2549, '2015-07-23 16:04:43', 4, 198, 142, NULL),
(2551, '2015-07-23 16:05:24', 7, 142, 198, NULL),
(2552, '2015-07-23 16:05:35', 7, 198, 142, NULL),
(2553, '2015-07-23 16:05:42', 7, 198, 142, NULL),
(2554, '2015-07-23 16:07:18', 7, 142, 198, NULL),
(3158, '2015-07-30 10:23:23', 0, 163, 131, 346),
(3057, '2015-07-29 05:55:01', 4, 239, 165, NULL),
(2875, '2015-07-28 11:49:23', 7, 182, 198, NULL),
(2563, '2015-07-23 18:55:07', 4, 165, 217, NULL),
(3313, '2015-07-30 16:51:53', 7, 182, 163, NULL),
(2562, '2015-07-23 18:48:56', 4, 12, 217, NULL),
(2744, '2015-07-26 16:12:03', 0, 205, 12, 316),
(2571, '2015-07-24 16:00:49', 7, 165, 217, NULL),
(2761, '2015-07-27 08:01:18', 6, 214, 165, 297),
(2981, '2015-07-28 14:41:03', 4, 214, 246, NULL),
(2574, '2015-07-24 18:12:06', 7, 12, 217, NULL),
(3173, '2015-07-30 10:38:23', 6, 182, 228, 343),
(3266, '2015-07-30 14:30:41', 3, 163, 265, NULL),
(2825, '2015-07-27 16:03:20', 1, 91, 91, 247),
(2976, '2015-07-28 14:38:30', 1, 91, 91, 248),
(2828, '2015-07-27 16:08:17', 1, 91, 91, 241),
(2831, '2015-07-27 16:08:17', 1, 91, 214, 241),
(2847, '2015-07-27 19:12:46', 4, 213, 217, NULL),
(2843, '2015-07-27 16:15:14', 1, 12, 12, 256),
(2979, '2015-07-28 14:39:23', 2, 165, 217, 248),
(3040, '2015-07-28 19:11:11', 4, 165, 252, NULL),
(2826, '2015-07-27 16:03:20', 1, 91, 195, 247),
(2829, '2015-07-27 16:08:17', 1, 91, 195, 241),
(3120, '2015-07-29 11:45:45', 6, 194, 243, 326),
(2919, '2015-07-28 12:53:32', 2, 12, 194, 324),
(3315, '2015-07-30 16:53:44', 7, 182, 163, NULL),
(2651, '2015-07-25 05:20:21', 2, 182, 142, 308),
(3565, '2015-08-04 10:01:35', 7, 163, 131, NULL),
(3635, '2015-08-12 11:02:20', 7, 91, 277, NULL),
(3317, '2015-07-30 17:11:07', 7, 182, 163, NULL),
(3641, '2015-08-12 11:04:04', 7, 91, 277, NULL),
(3318, '2015-07-30 17:11:11', 7, 182, 163, NULL),
(3643, '2015-08-12 11:04:27', 5, 188, 194, 326),
(3547, '2015-08-03 13:43:25', 4, 182, 228, NULL),
(3321, '2015-07-30 17:51:26', 7, 182, 163, NULL),
(3322, '2015-07-30 17:51:30', 7, 182, 163, NULL),
(2674, '2015-07-25 11:45:53', 6, 206, 217, 282),
(2714, '2015-07-25 22:01:03', 1, 12, 165, 256),
(2731, '2015-07-26 12:27:40', 7, 194, 165, NULL),
(2734, '2015-07-26 12:29:10', 7, 165, 195, NULL),
(2735, '2015-07-26 12:29:58', 7, 194, 165, NULL),
(3716, '2015-08-18 19:27:29', 7, 287, 165, NULL),
(2748, '2015-07-26 20:32:05', 2, 165, 12, 316),
(2750, '2015-07-26 20:32:06', 2, 165, 217, 316),
(2751, '2015-07-26 20:32:07', 2, 165, 214, 316),
(3675, '2015-08-17 11:00:36', 6, 299, 91, 385),
(2754, '2015-07-26 21:09:20', 6, 205, 165, 316),
(2755, '2015-07-26 21:09:20', 6, 205, 12, 316),
(2793, '2015-07-27 08:38:15', 7, 91, 194, NULL),
(2794, '2015-07-27 08:38:37', 7, 91, 194, NULL),
(2795, '2015-07-27 08:38:43', 7, 91, 194, NULL),
(3737, '2015-08-20 10:58:48', 6, 91, 314, 384),
(3729, '2015-08-19 20:35:45', 2, 165, 239, 385),
(3745, '2015-08-23 11:19:02', 6, 91, 328, 384),
(3572, '2015-08-04 12:40:58', 7, 163, 131, NULL),
(3573, '2015-08-04 12:40:59', 7, 163, 131, NULL),
(3574, '2015-08-04 13:53:49', 7, 163, 182, NULL),
(2813, '2015-07-27 14:15:12', 7, 186, 131, NULL),
(2815, '2015-07-27 14:26:45', 4, 225, 226, NULL),
(2817, '2015-07-27 14:27:08', 7, 226, 225, NULL),
(2818, '2015-07-27 14:27:12', 7, 226, 225, NULL),
(2819, '2015-07-27 14:27:30', 7, 225, 226, NULL),
(2820, '2015-07-27 16:02:49', 1, 91, 91, 248),
(3049, '2015-07-28 19:25:44', 7, 194, 252, NULL),
(2827, '2015-07-27 16:03:20', 1, 91, 165, 247),
(2830, '2015-07-27 16:08:17', 1, 91, 165, 241),
(2832, '2015-07-27 16:08:17', 1, 91, 206, 241),
(2833, '2015-07-27 16:08:17', 1, 91, 12, 241),
(2846, '2015-07-27 17:17:54', 1, 91, 165, 247),
(2845, '2015-07-27 17:17:54', 1, 91, 195, 247),
(2844, '2015-07-27 17:17:54', 1, 91, 91, 247),
(2841, '2015-07-27 16:15:14', 1, 12, 195, 256),
(2842, '2015-07-27 16:15:14', 1, 12, 165, 256),
(3282, '2015-07-30 14:42:32', 4, 163, 266, NULL),
(3580, '2015-08-04 13:57:03', 7, 182, 163, NULL),
(3563, '2015-08-04 09:26:10', 7, 182, 131, NULL),
(2985, '2015-07-28 15:00:50', 7, 12, 194, NULL),
(2901, '2015-07-28 12:10:04', 7, 182, 224, NULL),
(2902, '2015-07-28 12:11:07', 7, 182, 224, NULL),
(2903, '2015-07-28 12:13:44', 6, 214, 165, 297),
(2975, '2015-07-28 14:38:23', 6, 239, 12, 324),
(2906, '2015-07-28 12:27:21', 7, 198, 199, NULL),
(2907, '2015-07-28 12:27:54', 7, 199, 198, NULL),
(3603, '2015-08-06 18:53:46', 4, 278, 279, NULL),
(2910, '2015-07-28 12:28:22', 7, 199, 198, NULL),
(2911, '2015-07-28 12:29:16', 2, 182, 198, 324),
(2912, '2015-07-28 12:29:29', 2, 182, 142, 324),
(2913, '2015-07-28 12:29:30', 2, 182, 224, 324),
(2914, '2015-07-28 12:30:02', 2, 182, 131, 324),
(2917, '2015-07-28 12:33:27', 7, 182, 198, NULL),
(2918, '2015-07-28 12:53:32', 2, 12, 91, 324),
(2920, '2015-07-28 12:53:34', 2, 12, 177, 324),
(2921, '2015-07-28 12:55:19', 7, 194, 12, NULL),
(2922, '2015-07-28 12:55:39', 7, 194, 12, NULL),
(2923, '2015-07-28 12:56:34', 7, 12, 194, NULL),
(2927, '2015-07-28 13:06:15', 7, 12, 194, NULL),
(2929, '2015-07-28 13:41:49', 7, 244, 245, NULL),
(2926, '2015-07-28 13:04:39', 7, 194, 12, NULL),
(2930, '2015-07-28 13:41:57', 7, 244, 245, NULL),
(2931, '2015-07-28 13:49:30', 0, 194, 12, 326),
(2934, '2015-07-28 13:49:30', 0, 194, 91, 326),
(2941, '2015-07-28 14:01:00', 7, 12, 194, NULL),
(2939, '2015-07-28 13:59:18', 6, 194, 91, 326),
(2945, '2015-07-28 14:03:38', 7, 194, 12, NULL),
(2947, '2015-07-28 14:04:00', 7, 194, 12, NULL),
(2951, '2015-07-28 14:05:02', 1, 194, 91, 326),
(2954, '2015-07-28 14:07:01', 7, 194, 91, NULL),
(2956, '2015-07-28 14:10:09', 1, 194, 91, 326),
(2959, '2015-07-28 14:11:45', 1, 194, 91, 326),
(2962, '2015-07-28 14:17:25', 7, 12, 194, NULL),
(2963, '2015-07-28 14:18:57', 7, 194, 12, NULL),
(2965, '2015-07-28 14:19:40', 1, 194, 91, 326),
(2969, '2015-07-28 14:21:43', 7, 12, 194, NULL),
(2970, '2015-07-28 14:22:42', 7, 91, 194, NULL),
(2971, '2015-07-28 14:23:15', 7, 194, 91, NULL),
(2974, '2015-07-28 14:38:20', 4, 239, 12, NULL),
(2984, '2015-07-28 15:00:25', 7, 12, 194, NULL),
(2990, '2015-07-28 15:08:53', 1, 245, 223, 328),
(2989, '2015-07-28 15:07:50', 6, 245, 223, 328),
(2991, '2015-07-28 16:20:40', 7, 12, 239, NULL),
(3411, '2015-07-31 08:45:47', 7, 182, 131, NULL),
(2994, '2015-07-28 16:24:32', 7, 91, 142, NULL),
(2995, '2015-07-28 16:24:36', 7, 163, 142, NULL),
(3527, '2015-08-03 12:58:10', 4, 182, 228, NULL),
(2998, '2015-07-28 16:24:57', 7, 91, 142, NULL),
(2999, '2015-07-28 16:25:19', 7, 91, 142, NULL),
(3000, '2015-07-28 16:25:19', 7, 245, 142, NULL),
(3587, '2015-08-05 07:25:25', 7, 163, 182, NULL),
(3592, '2015-08-05 07:26:37', 7, 182, 163, NULL),
(3020, '2015-07-28 17:17:43', 4, 182, 223, NULL),
(3048, '2015-07-28 19:25:26', 7, 194, 252, NULL),
(3047, '2015-07-28 19:23:01', 7, 12, 252, NULL),
(3026, '2015-07-28 17:38:18', 7, 239, 12, NULL),
(3042, '2015-07-28 19:11:59', 4, 12, 252, NULL),
(3054, '2015-07-28 20:28:54', 7, 194, 252, NULL),
(3061, '2015-07-29 05:56:23', 2, 182, 223, 326),
(3056, '2015-07-29 05:54:59', 4, 239, 241, NULL),
(3063, '2015-07-29 07:10:30', 2, 194, 252, 326),
(3545, '2015-08-03 13:33:02', 4, 182, 228, NULL),
(3460, '2015-07-31 13:29:44', 7, 182, 131, NULL),
(3583, '2015-08-05 07:25:00', 7, 182, 163, NULL),
(3586, '2015-08-05 07:25:13', 7, 182, 163, NULL),
(3362, '2015-07-30 18:16:57', 7, 182, 163, NULL),
(3098, '2015-07-29 09:29:46', 7, 182, 260, NULL),
(3692, '2015-08-18 12:47:11', 1, 182, 182, 341),
(3588, '2015-08-05 07:25:34', 7, 163, 182, NULL),
(3104, '2015-07-29 10:12:14', 2, 131, 142, 299),
(3105, '2015-07-29 10:12:15', 2, 131, 101, 299),
(3116, '2015-07-29 11:43:42', 1, 214, 165, 293),
(3111, '2015-07-29 10:47:55', 1, 228, 261, 330),
(3110, '2015-07-29 10:47:15', 6, 228, 261, 330),
(3114, '2015-07-29 11:42:08', 6, 214, 165, 294),
(3113, '2015-07-29 10:50:53', 4, 228, 261, NULL),
(3124, '2015-07-29 12:13:48', 4, 243, 194, NULL),
(3126, '2015-07-29 12:21:29', 7, 243, 194, NULL),
(3127, '2015-07-29 12:21:53', 7, 243, 194, NULL),
(3128, '2015-07-29 12:22:06', 7, 194, 243, NULL),
(3129, '2015-07-29 12:25:07', 7, 91, 214, NULL),
(3130, '2015-07-29 12:25:10', 7, 91, 214, NULL),
(3131, '2015-07-29 12:25:13', 7, 91, 214, NULL),
(3132, '2015-07-29 12:29:26', 7, 12, 194, NULL),
(3133, '2015-07-29 12:29:57', 7, 194, 12, NULL),
(3136, '2015-07-29 12:34:36', 7, 194, 252, NULL),
(3137, '2015-07-29 12:35:06', 7, 194, 252, NULL),
(3138, '2015-07-29 13:22:21', 7, 252, 194, NULL),
(3139, '2015-07-29 13:23:42', 7, 194, 252, NULL),
(3140, '2015-07-29 13:27:51', 7, 252, 194, NULL),
(3144, '2015-07-29 18:27:44', 7, 165, 258, NULL),
(3143, '2015-07-29 18:23:52', 4, 258, 165, NULL),
(3147, '2015-07-29 18:41:44', 7, 258, 165, NULL),
(3149, '2015-07-29 18:46:02', 7, 165, 258, NULL),
(3150, '2015-07-30 07:47:48', 7, 12, 213, NULL),
(3151, '2015-07-30 08:08:20', 7, 213, 12, NULL),
(3153, '2015-07-30 08:14:27', 7, 213, 12, NULL),
(3589, '2015-08-05 07:25:44', 7, 163, 182, NULL),
(3361, '2015-07-30 18:16:45', 7, 182, 163, NULL),
(3595, '2015-08-05 07:27:10', 7, 163, 182, NULL),
(3367, '2015-07-30 18:18:06', 7, 182, 163, NULL),
(3174, '2015-07-30 10:38:48', 2, 182, 228, 341),
(3383, '2015-07-30 18:23:29', 7, 182, 163, NULL),
(3176, '2015-07-30 10:39:58', 2, 182, 228, 347),
(3181, '2015-07-30 10:40:55', 6, 182, 228, 340),
(3178, '2015-07-30 10:40:34', 2, 182, 228, 340),
(3371, '2015-07-30 18:18:53', 7, 182, 163, NULL),
(3183, '2015-07-30 10:42:26', 1, 163, 263, 346),
(3607, '2015-08-07 10:19:07', 7, 91, 277, NULL),
(3185, '2015-07-30 10:44:09', 6, 163, 264, 346),
(3187, '2015-07-30 10:44:33', 4, 163, 264, NULL),
(3481, '2015-07-31 14:45:10', 1, 182, 182, 345),
(3531, '2015-08-03 13:04:40', 4, 182, 228, NULL),
(3193, '2015-07-30 10:46:53', 2, 182, 228, 344),
(3194, '2015-07-30 10:47:55', 2, 182, 228, 345),
(3373, '2015-07-30 18:19:58', 7, 182, 163, NULL),
(3376, '2015-07-30 18:20:22', 7, 182, 163, NULL),
(3198, '2015-07-30 10:56:35', 2, 182, 228, 348),
(3686, '2015-08-18 10:16:11', 7, 182, 142, NULL),
(3215, '2015-07-30 12:20:15', 1, 163, 264, 346),
(3216, '2015-07-30 12:21:24', 7, 163, 264, NULL),
(3486, '2015-07-31 15:12:09', 1, 182, 182, 345),
(3244, '2015-07-30 13:23:23', 1, 163, 264, 346),
(3249, '2015-07-30 13:26:53', 7, 163, 265, NULL),
(3248, '2015-07-30 13:26:28', 4, 163, 265, NULL),
(3250, '2015-07-30 13:27:12', 7, 163, 265, NULL),
(3482, '2015-07-31 14:45:24', 1, 182, 182, 345),
(3585, '2015-08-05 07:25:10', 7, 163, 182, NULL),
(3484, '2015-07-31 14:56:33', 1, 182, 182, 345),
(3489, '2015-07-31 15:16:14', 1, 182, 182, 345),
(3498, '2015-07-31 15:52:04', 4, 131, 182, NULL),
(3581, '2015-08-05 07:24:47', 7, 163, 182, NULL),
(3584, '2015-08-05 07:25:03', 7, 163, 182, NULL),
(3405, '2015-07-31 08:25:51', 4, 182, 131, NULL),
(3300, '2015-07-30 15:18:57', 7, 163, 266, NULL),
(3591, '2015-08-05 07:26:33', 7, 182, 163, NULL),
(3308, '2015-07-30 15:26:11', 3, 266, 163, NULL),
(3416, '2015-07-31 10:09:53', 7, 163, 131, NULL),
(3388, '2015-07-30 18:24:05', 1, 163, 264, 346),
(3389, '2015-07-30 18:24:05', 1, 163, 265, 346),
(3390, '2015-07-30 18:26:59', 7, 182, 163, NULL),
(3436, '2015-07-31 12:42:35', 1, 214, 195, 294),
(3437, '2015-07-31 12:42:35', 1, 214, 12, 294),
(3439, '2015-07-31 12:42:35', 1, 214, 165, 294),
(3441, '2015-07-31 13:05:54', 0, 142, 177, 363),
(3442, '2015-07-31 13:05:54', 0, 142, 12, 363),
(3556, '2015-08-04 08:44:11', 7, 182, 131, NULL),
(3582, '2015-08-05 07:24:53', 7, 163, 182, NULL),
(3529, '2015-08-03 12:59:09', 4, 228, 182, NULL),
(3533, '2015-08-03 13:08:24', 4, 228, 182, NULL),
(3535, '2015-08-03 13:08:51', 4, 228, 182, NULL),
(3543, '2015-08-03 13:28:43', 4, 228, 182, NULL),
(3548, '2015-08-03 15:06:14', 7, 182, 131, NULL),
(3549, '2015-08-03 15:06:31', 7, 131, 182, NULL),
(3550, '2015-08-03 15:06:38', 7, 131, 182, NULL),
(3605, '2015-08-06 18:54:42', 7, 278, 279, NULL),
(3606, '2015-08-07 09:44:31', 4, 91, 277, NULL),
(3608, '2015-08-07 13:33:28', 7, 91, 277, NULL),
(3609, '2015-08-07 13:34:24', 7, 91, 277, NULL),
(3610, '2015-08-07 17:52:31', 7, 277, 91, NULL),
(3611, '2015-08-08 10:38:41', 7, 165, 91, NULL),
(3613, '2015-08-08 13:23:14', 7, 252, 165, NULL),
(3614, '2015-08-08 13:23:51', 7, 252, 165, NULL),
(3616, '2015-08-08 14:42:29', 7, 165, 252, NULL),
(3617, '2015-08-08 15:19:46', 7, 252, 165, NULL),
(3625, '2015-08-12 10:49:36', 7, 277, 91, NULL),
(3622, '2015-08-12 08:07:57', 6, 243, 91, 379),
(3621, '2015-08-12 08:07:54', 4, 243, 91, NULL),
(3628, '2015-08-12 11:00:52', 7, 277, 91, NULL),
(3630, '2015-08-12 11:01:17', 7, 91, 277, NULL),
(3631, '2015-08-12 11:01:40', 7, 277, 91, NULL),
(3633, '2015-08-12 11:02:11', 7, 91, 277, NULL),
(3634, '2015-08-12 11:02:17', 7, 277, 91, NULL),
(3639, '2015-08-12 11:03:56', 7, 91, 277, NULL),
(3637, '2015-08-12 11:03:18', 7, 91, 277, NULL),
(3638, '2015-08-12 11:03:32', 6, 285, 243, 382),
(3640, '2015-08-12 11:04:03', 7, 277, 91, NULL),
(3642, '2015-08-12 11:04:21', 7, 277, 91, NULL),
(3644, '2015-08-12 11:04:45', 7, 91, 277, NULL),
(3645, '2015-08-12 11:05:14', 7, 91, 277, NULL),
(3646, '2015-08-12 11:06:14', 7, 91, 277, NULL),
(3647, '2015-08-12 11:06:48', 7, 277, 91, NULL),
(3650, '2015-08-12 11:14:17', 7, 277, 91, NULL),
(3653, '2015-08-12 11:16:11', 7, 277, 91, NULL),
(3657, '2015-08-12 11:22:55', 7, 277, 91, NULL),
(3659, '2015-08-12 11:49:57', 7, 91, 277, NULL),
(3660, '2015-08-12 15:03:39', 7, 277, 91, NULL),
(3663, '2015-08-12 15:54:24', 1, 285, 243, 382),
(3664, '2015-08-12 15:54:42', 1, 285, 243, 382),
(3676, '2015-08-17 11:00:39', 4, 299, 91, NULL),
(3709, '2015-08-18 19:14:34', 5, 165, 299, 385),
(3667, '2015-08-13 19:09:37', 1, 91, 91, 384),
(3669, '2015-08-14 06:01:42', 6, 243, 300, 379),
(3715, '2015-08-18 19:26:43', 7, 165, 287, NULL),
(3674, '2015-08-17 11:00:06', 6, 299, 195, 385),
(3682, '2015-08-17 19:26:48', 6, 285, 309, 382),
(3681, '2015-08-17 19:26:47', 4, 285, 309, NULL),
(3687, '2015-08-18 10:27:57', 7, 142, 142, NULL),
(3694, '2015-08-18 13:14:54', 4, 194, 314, NULL),
(3697, '2015-08-18 13:35:21', 2, 12, 194, 385),
(3698, '2015-08-18 13:35:22', 2, 12, 314, 385),
(3699, '2015-08-18 13:35:23', 2, 12, 217, 385),
(3700, '2015-08-18 13:35:24', 2, 12, 165, 385),
(3703, '2015-08-18 13:35:35', 5, 12, 299, 385),
(3704, '2015-08-18 13:42:05', 5, 168, 299, 385),
(3705, '2015-08-18 14:32:16', 5, 241, 299, 385),
(3708, '2015-08-18 19:13:51', 4, 165, 314, NULL),
(3707, '2015-08-18 15:00:08', 6, 243, 12, 379),
(3748, '2015-08-23 14:28:42', 1, 91, 91, 384),
(3712, '2015-08-18 19:19:42', 3, 165, 300, NULL),
(3713, '2015-08-18 19:24:55', 4, 287, 165, NULL),
(3718, '2015-08-18 19:29:53', 7, 287, 165, NULL),
(3720, '2015-08-18 19:32:03', 7, 287, 165, NULL),
(3738, '2015-08-20 12:37:47', 2, 142, 182, 384),
(3730, '2015-08-19 20:35:46', 2, 165, 194, 385),
(3731, '2015-08-19 20:35:47', 2, 165, 314, 385),
(3732, '2015-08-19 20:35:47', 2, 165, 217, 385),
(3733, '2015-08-19 20:35:48', 2, 165, 205, 385),
(3734, '2015-08-19 20:35:50', 2, 165, 252, 385),
(3735, '2015-08-19 20:35:51', 2, 165, 258, 385),
(3747, '2015-08-23 12:24:57', 3, 91, 328, NULL),
(3746, '2015-08-23 11:19:03', 6, 91, 391, 384),
(3740, '2015-08-21 08:40:56', 3, 337, 195, NULL),
(3741, '2015-08-21 08:42:27', 3, 337, 243, NULL),
(3742, '2015-08-21 12:58:08', 3, 342, 316, NULL),
(3743, '2015-08-22 14:06:58', 3, 12, 316, NULL),
(3749, '2015-08-23 14:28:42', 1, 91, 195, 384),
(3750, '2015-08-23 14:28:42', 1, 91, 328, 384),
(3751, '2015-08-24 05:02:40', 1, 392, 392, 390);

-- --------------------------------------------------------

--
-- Struttura della tabella `partecipates`
--

CREATE TABLE IF NOT EXISTS `partecipates` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'pk booking',
  `iduser` int(11) NOT NULL,
  `idevent` int(11) NOT NULL,
  `confirmed` int(1) NOT NULL COMMENT '0: se la richiesta non è stata confermata dal creatore; 1: altrimenti',
  PRIMARY KEY (`id`),
  KEY `fk_users_has_events_events1_idx` (`idevent`),
  KEY `fk_users_has_events_users1_idx` (`iduser`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=569 ;

--
-- Dump dei dati per la tabella `partecipates`
--

INSERT INTO `partecipates` (`id`, `iduser`, `idevent`, `confirmed`) VALUES
(1, 66, 1, 1),
(2, 66, 3, 1),
(3, 66, 2, 1),
(4, 66, 5, 1),
(5, 71, 6, 1),
(6, 81, 5, 1),
(7, 85, 3, 1),
(8, 12, 8, 0),
(9, 12, 5, 1),
(10, 12, 7, 0),
(11, 71, 11, 1),
(12, 12, 10, 1),
(13, 71, 13, 1),
(14, 90, 12, 1),
(15, 12, 3, 1),
(16, 93, 17, 1),
(17, 94, 17, 1),
(18, 94, 3, 1),
(19, 12, 18, 1),
(20, 71, 18, 1),
(21, 12, 14, 1),
(22, 91, 15, 1),
(23, 71, 15, 1),
(24, 91, 20, 1),
(25, 12, 20, 1),
(26, 92, 3, 1),
(27, 98, 22, 1),
(28, 91, 11, 1),
(29, 12, 24, 1),
(30, 12, 33, 1),
(31, 69, 8, 0),
(33, 101, 22, 0),
(34, 91, 33, 1),
(35, 91, 34, 1),
(36, 109, 62, 1),
(37, 109, 63, 1),
(38, 109, 58, 1),
(39, 109, 59, 1),
(40, 109, 61, 1),
(41, 109, 60, 1),
(42, 101, 69, 1),
(43, 101, 64, 1),
(44, 101, 48, 1),
(45, 135, 73, 0),
(46, 131, 95, 1),
(47, 134, 74, 1),
(48, 131, 69, 1),
(49, 101, 33, 0),
(50, 71, 101, 1),
(51, 71, 102, 1),
(52, 71, 103, 0),
(54, 71, 66, 1),
(55, 71, 104, 1),
(56, 12, 28, 0),
(57, 71, 106, 1),
(58, 131, 93, 1),
(59, 140, 33, 1),
(60, 144, 107, 0),
(61, 144, 109, 1),
(62, 140, 109, 1),
(63, 163, 95, 0),
(64, 12, 89, 1),
(65, 12, 43, 0),
(66, 12, 47, 0),
(67, 12, 42, 0),
(68, 12, 111, 1),
(69, 12, 112, 1),
(70, 12, 114, 1),
(71, 12, 113, 1),
(72, 12, 119, 1),
(73, 131, 113, 1),
(74, 131, 111, 1),
(75, 169, 115, 1),
(76, 12, 115, 1),
(77, 12, 123, 1),
(78, 12, 124, 1),
(79, 12, 122, 1),
(80, 176, 123, 0),
(81, 168, 119, 1),
(82, 168, 115, 1),
(83, 12, 126, 1),
(84, 168, 126, 1),
(85, 176, 125, 1),
(86, 140, 127, 1),
(87, 140, 125, 1),
(88, 12, 125, 1),
(89, 12, 117, 1),
(90, 12, 129, 1),
(91, 12, 128, 1),
(92, 168, 128, 1),
(93, 168, 129, 1),
(94, 91, 126, 1),
(95, 168, 134, 1),
(96, 91, 122, 1),
(97, 168, 121, 1),
(98, 12, 134, 1),
(99, 12, 130, 1),
(100, 12, 136, 1),
(101, 12, 140, 1),
(102, 12, 38, 1),
(103, 91, 142, 1),
(104, 91, 147, 1),
(105, 91, 140, 1),
(106, 177, 158, 1),
(107, 91, 130, 1),
(108, 91, 158, 1),
(109, 91, 111, 1),
(110, 91, 153, 1),
(111, 91, 155, 1),
(112, 91, 141, 1),
(113, 91, 148, 1),
(114, 142, 141, 1),
(115, 179, 148, 1),
(116, 12, 148, 1),
(117, 142, 148, 1),
(118, 12, 165, 1),
(119, 142, 174, 1),
(120, 179, 174, 1),
(121, 168, 165, 1),
(122, 12, 172, 0),
(123, 131, 162, 1),
(124, 12, 162, 1),
(125, 91, 163, 1),
(126, 91, 165, 1),
(127, 12, 131, 1),
(128, 142, 175, 1),
(129, 12, 175, 1),
(130, 12, 164, 1),
(131, 131, 163, 1),
(132, 163, 148, 0),
(133, 12, 163, 1),
(134, 12, 141, 0),
(135, 168, 141, 0),
(136, 131, 148, 1),
(137, 12, 133, 1),
(138, 142, 161, 1),
(139, 12, 161, 1),
(140, 142, 155, 1),
(141, 12, 138, 1),
(142, 168, 219, 1),
(143, 12, 152, 1),
(145, 91, 219, 1),
(146, 91, 217, 1),
(147, 12, 222, 1),
(148, 188, 220, 1),
(149, 182, 187, 1),
(150, 182, 164, 0),
(151, 182, 228, 1),
(152, 168, 222, 1),
(153, 12, 217, 1),
(154, 12, 229, 1),
(155, 91, 220, 1),
(156, 91, 229, 1),
(157, 188, 228, 0),
(160, 91, 238, 1),
(161, 91, 233, 1),
(162, 140, 194, 1),
(163, 182, 230, 1),
(164, 188, 230, 1),
(165, 12, 160, 1),
(166, 12, 153, 1),
(167, 142, 160, 1),
(168, 12, 230, 1),
(169, 12, 239, 1),
(170, 12, 220, 1),
(172, 12, 240, 1),
(173, 91, 240, 1),
(174, 91, 245, 1),
(175, 91, 241, 1),
(176, 91, 248, 1),
(177, 91, 246, 1),
(178, 91, 247, 1),
(182, 190, 200, 1),
(183, 190, 235, 1),
(184, 190, 246, 1),
(186, 189, 249, 1),
(188, 12, 248, 1),
(191, 12, 251, 1),
(192, 12, 246, 1),
(196, 91, 253, 1),
(200, 12, 255, 1),
(202, 194, 257, 1),
(207, 188, 240, 1),
(209, 197, 258, 1),
(210, 198, 258, 1),
(212, 198, 259, 1),
(213, 197, 259, 1),
(214, 197, 260, 1),
(216, 198, 260, 1),
(217, 197, 261, 1),
(219, 198, 261, 1),
(221, 199, 258, 1),
(225, 131, 263, 1),
(233, 195, 251, 1),
(234, 195, 245, 1),
(235, 195, 241, 1),
(237, 195, 247, 1),
(239, 195, 252, 1),
(240, 195, 256, 1),
(248, 203, 266, 1),
(250, 168, 258, 0),
(255, 165, 241, 1),
(257, 165, 246, 1),
(259, 91, 268, 1),
(260, 12, 264, 1),
(261, 195, 268, 1),
(263, 91, 269, 1),
(265, 165, 251, 1),
(268, 91, 255, 1),
(280, 12, 270, 1),
(282, 165, 270, 1),
(287, 12, 253, 1),
(291, 165, 252, 1),
(292, 165, 256, 1),
(296, 91, 252, 1),
(298, 208, 284, 1),
(300, 163, 290, 1),
(301, 163, 289, 1),
(302, 163, 288, 1),
(305, 165, 284, 1),
(313, 165, 272, 1),
(314, 12, 256, 1),
(315, 212, 272, 0),
(320, 213, 294, 1),
(323, 214, 296, 1),
(326, 165, 293, 1),
(328, 165, 296, 1),
(332, 213, 252, 1),
(334, 12, 293, 1),
(342, 165, 264, 1),
(347, 163, 272, 1),
(348, 163, 298, 1),
(349, 195, 294, 1),
(350, 195, 297, 1),
(351, 12, 294, 1),
(352, 195, 293, 1),
(353, 214, 294, 1),
(355, 214, 299, 1),
(358, 214, 241, 1),
(359, 165, 240, 1),
(360, 165, 248, 1),
(362, 206, 251, 1),
(363, 206, 241, 1),
(364, 165, 247, 1),
(365, 12, 241, 1),
(366, 179, 290, 1),
(368, 163, 253, 1),
(372, 91, 299, 1),
(373, 91, 297, 1),
(379, 12, 299, 1),
(380, 12, 296, 1),
(382, 142, 258, 1),
(387, 91, 298, 0),
(391, 182, 313, 1),
(392, 182, 312, 1),
(393, 182, 311, 1),
(394, 217, 282, 1),
(395, 163, 282, 1),
(396, 91, 282, 1),
(397, 12, 282, 1),
(398, 168, 282, 1),
(399, 195, 282, 1),
(400, 205, 282, 1),
(401, 165, 282, 1),
(405, 91, 257, 1),
(406, 12, 257, 1),
(407, 194, 315, 1),
(410, 12, 315, 1),
(411, 165, 315, 1),
(413, 91, 315, 1),
(418, 165, 316, 1),
(419, 12, 316, 1),
(427, 131, 301, 1),
(428, 131, 289, 1),
(429, 131, 290, 1),
(430, 131, 304, 1),
(431, 131, 298, 1),
(432, 131, 317, 1),
(433, 131, 318, 1),
(436, 91, 316, 1),
(437, 182, 317, 1),
(438, 182, 318, 1),
(442, 165, 297, 1),
(445, 194, 326, 1),
(448, 91, 326, 1),
(449, 12, 326, 1),
(450, 12, 324, 1),
(464, 214, 324, 1),
(465, 165, 324, 1),
(466, 258, 324, 1),
(467, 223, 328, 0),
(469, 182, 331, 1),
(474, 261, 330, 1),
(475, 165, 294, 1),
(478, 182, 340, 1),
(479, 182, 341, 1),
(481, 182, 343, 1),
(485, 182, 346, 1),
(486, 263, 346, 1),
(495, 264, 346, 1),
(496, 228, 345, 0),
(497, 228, 344, 0),
(498, 228, 348, 0),
(499, 228, 342, 0),
(500, 182, 324, 1),
(501, 228, 350, 0),
(503, 265, 346, 1),
(505, 163, 349, 1),
(507, 163, 343, 1),
(509, 182, 353, 1),
(511, 182, 354, 1),
(512, 163, 354, 1),
(513, 182, 355, 1),
(516, 142, 363, 1),
(518, 199, 363, 1),
(519, 131, 364, 1),
(521, 182, 363, 1),
(522, 182, 349, 1),
(523, 12, 363, 0),
(526, 200, 378, 1),
(528, 91, 379, 1),
(530, 285, 379, 1),
(532, 243, 382, 1),
(533, 188, 326, 0),
(534, 286, 382, 0),
(535, 91, 384, 1),
(538, 300, 379, 1),
(541, 195, 384, 1),
(542, 195, 385, 1),
(543, 91, 385, 1),
(545, 309, 382, 1),
(549, 12, 385, 0),
(550, 168, 385, 0),
(551, 241, 385, 0),
(553, 12, 379, 1),
(554, 165, 385, 0),
(555, 165, 384, 0),
(556, 317, 388, 0),
(557, 316, 388, 1),
(560, 205, 326, 1),
(561, 12, 384, 0),
(565, 328, 384, 1),
(568, 392, 390, 1);

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `public_events`
--
CREATE TABLE IF NOT EXISTS `public_events` (
`idevent` int(11)
,`iduser` int(11)
,`idcategory` int(11)
,`title` varchar(100)
,`description` varchar(1000)
,`img` varchar(128)
,`city` varchar(45)
,`address` varchar(45)
,`date` date
,`hour` time
,`sex` int(1)
,`agemin` int(2)
,`agemax` int(2)
,`sittotal` int(3)
,`sitcurrent` int(3)
,`latitude` varchar(10)
,`longitude` varchar(10)
,`scope` int(11)
,`creationdate` datetime
,`updatetime` timestamp
,`activated` int(1)
,`userimg` varchar(128)
,`job` int(3)
);
-- --------------------------------------------------------

--
-- Struttura della tabella `pushcodes`
--

CREATE TABLE IF NOT EXISTS `pushcodes` (
  `idpushcodes` int(11) NOT NULL AUTO_INCREMENT,
  `iduser` int(11) NOT NULL,
  `code` varchar(256) NOT NULL,
  `hwid` varchar(64) NOT NULL DEFAULT '0' COMMENT 'hwid',
  `os` int(1) NOT NULL DEFAULT '1' COMMENT 'tipologia sistema operativo. 0: ios, 1: android, 2: windows',
  PRIMARY KEY (`idpushcodes`,`iduser`),
  UNIQUE KEY `idpushcodes` (`idpushcodes`),
  KEY `idpushcodes_2` (`idpushcodes`),
  KEY `pushcodes_ibfk_1` (`iduser`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1144 ;

--
-- Dump dei dati per la tabella `pushcodes`
--

INSERT INTO `pushcodes` (`idpushcodes`, `iduser`, `code`, `hwid`, `os`) VALUES
(16, 13, 'APA91bEWfjvPFiNFvpwXGnpjIj0_Fy4if4deiN2KuQTddMxlpm2hR7zU5t3GeP0DZsNMVVMF5e2bfSNsWZ7HPsXx_XDesnJ4nlznWSXqhP2NuTxH1wbBleGS9LJscpQSSyjLPVP4ea1psCXrQu8M_x1QGPPEipHUg1mI3onpWkFHvvasPeIZrNU', '868cf9b5b983515a', 1),
(106, 31, 'APA91bH0ppjIu54UvSTakjwvfzXolmGusimQHU2UE9Hk8WQTpkqt1UL6hPWmChOnLgqkwqJUXG3gEmRGyed5N9x7ERSIneRF-jq1D-y-4czhpo1ZCq4nsAmyaA614WOls_2qJWDN0Dyj7x6tS2JDWzvhxVvj2PpYaA', '997c5b89c8b247fe', 1),
(143, 52, 'APA91bGrIsU_RS-I_mBFrl0afMCzfYaDLz4a-XCJXVO2BBwaxxXLZDBP7CLwLCqK9zGYoKtym25G8bjv9w165-8_MwEOQUNE8fb6Mt3rhohQISAD0VDrf1R1SwwrxJJVAxTIp97a-HSobAFqOi20J93OoBlPNltV-g', 'eaac7d56fa115507', 1),
(269, 140, 'auto_push_token_michele', 'auto_hardware_id_michele', 1),
(346, 131, 'testtesttest', 'provaprova', 1),
(361, 143, 'push_disabled_by_user', '53055209-fa26-43d1-91f7-af076f24660f', 0),
(375, 140, 'push_disabled_by_user', '95a558ed-536c-44ba-b283-beb60f69d0f0', 0),
(392, 91, 'APA91bESnt0OnSyM0trw-Rd06w3c0Uq7xjujDHGGVsCzhVJfky43zvMoBNb_ScIcP5NRg1jEGNiymOmU9sl3OQx350vuKB4hBC4Msyk7JH4aOFgRdSzbdQMAuC5OKXqkqhJyIXahiF_aTgYx6arePGTvz6fjtqX5lA', '11d154b1ff65b1ff', 1),
(469, 187, 'APA91bFTznmxkJHf8TLb1cD_-_yn7uMlvVNyv571LsiqFwpWF_hgirTsGrCFPCdjLmZfMtFqrtXbeZjR4Ppc48P67EeeQKVzRjV2-bMMcy8KZw5K961BXHmY1Q3rmaNVT8SFKh5SWt3aryJWj17uWltyiMYKG3B6Nw', '844a9e9e32dacec9', 1),
(486, 188, 'APA91bEqhW9NRQFMkm6vZIhuhKmLWY3NjppPH4LvQLTtIG1pql9vr8ySSSW_bF_sdHld4ao4up120y0o788CFY4OnZmi0P3tCg7Pr7bcdsysm4bjJ6m2CVQtzdVTDuQ98iPQPiKM5eTTkPiJGFEyDjoNkUIo4bS0YA', 'c5f45091651a7178', 1),
(521, 190, 'APA91bGPTLBNdGia4VzWRv3OWl7uS5PjsTCbrldT9xflBUStxlSObO4lrNLJWz3zXSMrPh1rgZAmZj8uppMXbXFr5SaEWLtjOQEIej43dUX9eWbVu2YCmusIsen_QcfeY3Lr0LHsECSro-N-zSkFF2McZ3SNdjEEMQ', '8fec4e61fad226f9', 1),
(526, 191, 'APA91bH9peCeZSTenkHDrbRectJq_ExjWrL3dUez3QvyByNXAoWZEAdlF2LtsT5u-nD-XcAHqH0SDNG01CjJC3w_UWBH_geWgwUiQtcWyxSI4HeUHn3By3f0oofjvjwhwac-mk5UeiRC7VY_2drVUA9isuggKLKEUg', '9ca51641740d21fe', 1),
(535, 140, 'push_disabled_by_user', '279e59ec-d23d-4dc3-9ea6-c9cc7baf1a2b', 0),
(537, 187, 'APA91bG_9yA1Ait_EqUpp0DeUB4fNxmwjkBfUNS47tDh9w6Aasde0Vy8p7KJ99LIoRLX8WhfsWv7gZP9qh7Gcupbx3r_BH_SyeIDKwGlyJ5NufV7x2uAkh83rmbaRRcPBXJzgwNjmhkQ', '7ae1e1c1ec295264', 1),
(539, 140, '095042a5c34b6cc7cdc3b831628a05e904c5931b2120c19cc6af42e5346cbb61', '956c3040-8597-4765-927e-66e64856fd80', 0),
(562, 168, 'APA91bFDFM678QcX21WWYeR_XbEV8GAkNjkGgpI4f68J1MmknlxeVfujHNCiACyfnXkz_4233OIi5PA7DlY3T9hyP7HlhF07KskXXvCXD9Ch1uQ6KNlX1sEo2qeXy2e0CFY-yabH4iF2', '247ee876a6f2746e', 1),
(573, 196, '136bf08e7042dc8533ea7991d409d001865eb5f31445596ac44a4750b299f52f', '2486ca32-55d4-44c8-8ec0-dd57a2e311b1', 0),
(588, 131, '144cb3772e5b4fb9ee6b384e822dda5a5b8463714c093048fbcadb01ec23f6df', '3abe161b-120e-4687-85e5-1743b5900288', 0),
(613, 200, '', 'c5f14815-2f33-426a-a57c-0359209c9d57', 0),
(615, 202, '547a0225a8e2d55dfa0727af2ad1d6ddd44a473b4405cc245b3e17cf0fb57a47', 'a1af664e-3074-4185-944e-e2c0b4eedc9f', 0),
(620, 194, '104ba1415bb0afee4f936414152a9615cb09c7b2543390d821860e2106272aac', 'b22ebb97-cd7e-495a-80e8-47ca4eff55bd', 0),
(625, 195, '53c7a4fca6d7e08cafb175f3463d3c0ba2f8858d3258fadbb89b6b7f09abe3c1', '12bbe457-15be-433a-bc8e-530aae799072', 0),
(644, 205, '2fb3cfec7bca3baf89e2b7e008a0cb0d571e0b51f7933f25e6cc4f8d840a9980', 'af7ba4ac-ee32-4f33-9648-dddb2f8f4487', 0),
(649, 206, '4d035c899cfc019ba0961949a35384cf3e6570caa8c8fb88757055c3f208b6e0', '64d80498-65ef-42ca-b7fa-7f16e61c7ed5', 0),
(659, 209, 'd43bb2046c983f1c3035fca3135e7ff521a6533d1857b12d54940815bbb0e09e', '0be42005-e3e7-48a8-a17d-dd9b0415cf16', 0),
(663, 200, '', '328d5922-f383-40d6-a3c9-0b7bbe1ce851', 0),
(664, 200, '', '80e8f38e-9eed-4c0a-90ea-2a1a5960d694', 0),
(665, 200, '', 'fc288154-d4a1-4a49-aba6-37b7858d7d27', 0),
(669, 210, 'APA91bEswrprCov66FLheeNA_ZHuhMjKu6LmK4bCkQODHgkyOAFF4c9fd6qRcVD9apAzpfA8Td6AiFLvlezl-_2aB7zt_aI6W4MV2lC9XydmQ4EtzC8t2BVR8bStGA4TVLLAbAIZgk_Y', 'a9ac4887d32c928c', 1),
(689, 213, 'APA91bFSRShJYeyPfwaiib2BsodSYRNXR0leHIG3aSbXe81lB_fOr_vhfM-614SH_PniGdqcEBphULKBGm4p4w4G_PVbL_6FegkPlhCPOOhhBDAAPaj14LlAxMBvFOz4RnqCeVGxcCyT', '5fc42648c0afb02b', 1),
(695, 91, 'APA91bHYb0SicllMeetTkoyxBd8gxG5lMVZiu117_iX0bvyaiRFDW4FzYFftZR-O03GuW6uWPk2k2BqtqdSAF3F9XRGGTeZVZBXyt_28-ySdv8gVIhiRYrw1uD765HoMmULdkVKEG2Ym', 'edb8c2696fc039c8', 1),
(700, 216, '', '159823b9-411d-4e47-b369-835a8d5f743c', 0),
(741, 217, '35dd223ab4c8c9522cb72e98e4babe068afbd1e3c2c97a9dfc70bc442289968e', '61291704-9970-4078-93f9-8adb4cdf312e', 0),
(744, 165, '0b34b44440e56d50381fce0e6eed9fdbad09686ce4b0a73e13c29460ec90d281', '076d0c4d-63c2-4d8a-8269-22703e8d6bfc', 0),
(747, 218, 'APA91bE-uRmgMv9nnbcNT1BrdZb3DPk05TjoRIVfJyLeRT3M71nOLP7Z4np35fYJUNp9jB-CFigo8YjRx0m-K9BXcaUPilSJs_otQBUcRhrLJ09LxkxDEz5Ear6k-p0frszrxXZpBENB', '9f67b46ed421caa0', 1),
(785, 229, 'APA91bHjlDpwGA6zTQSBoC2HQzs6Nejbi5MvjC0DaPJhdBzGYsiMjol1TdljUecksQQoTpLu75Nq5kRinX38xNDKQeZ_g1ENwMYY9NkjH6bHemXYLrSktectwHOORZpvVVpGCbEP5_Q6', '6a16f76746f66166', 1),
(798, 231, 'APA91bFsrOULoKsV2MbZoKGMX6YWK1xrjTa-pddAlYwnps_1fJsEgti6YiX0yvP3BsnGGigPsn0V1sDbfp-6Fpczw5BVrfGAZn3R-LhWHRvYLLzhYKHDJWwN2W362YHtoYHRPjuCNKdt', '46c82552fb4924f', 1),
(801, 233, 'APA91bHofUsZXek14WQV5QgtKObgDgwDvEkgXOdrXI_vdYrPIWbUQOkcU6L7qO2iNY8k7rHyGH1AaIiVaDe0oyxZbgML2iD_gflkHf8ybxzPXetFuAEHNanVz9OSgDa_3Bx2s2chTapD', '8f4b8df6986c81d4', 1),
(802, 234, 'APA91bHUiWoa-cjH1V71deE5jfJWOHoFCpuJ05hOe61O-u8Am_raET7wWBr3fJ-GO2FDfftLeh2y2qsXZyLVIsBgDCmsMZmWpYnMiM9XruGSzTdmHTWx4UxnWA6uoLp2WZDK7OpEM1H2', '4114fd3dce8a8c48', 1),
(803, 235, 'APA91bFBJk2GEdYdqqQoPJLAUlemIBnxo3EPXAdWv6Gw1cu-gw0Xmb7XadDQs8X1cSJ0tc-szJJUdJkVUj_k_cqQn4sx8uCi4uvNM9VtV76DchNu3cALwQl-zMYrULfJjENaCtqvRR00', 'b2e53a8f2bd3afdd', 1),
(807, 236, 'APA91bEYBcIVVzaP7Lat1U2xh-bEut5EAPzAbWQ9WR5TxjsFMjLnzEVaCmH1wBU1gJhQUWK97Uu29qr0k7XfcEl2ZXRGuAsIroYiF6LUufymM1Tg-BgpHKOl3e_aT8lAlXlzZl63C0iB', 'cc9729d78ba3eb4', 1),
(809, 237, 'APA91bHOe69drfdrdrh7IRT83wb5KNLXDmjoDZ3Jj7l4y5GjL2BSRx8dywq7xX3O-MC-x2ctkOmRukiC1xM1SnHRp73XbrYI1k8Onc6gPMRFRzOVUNBkpmuDWKSRAwz7XE6EqPcFQ1HD', '9b331bf495de10c5', 1),
(811, 238, 'APA91bFAlSJADx49zUl0GwQbSTwclqSoJunBA2qDyCk9VTqYa2e4TlbbxGfwkd4PpCUZ7l0yTb0Txg0Xkc4_ytQPxA5Ete9gI5N09Qw_aoZxumdgGyehF829hTs_fJa6CGjud5qZEEVu', '2ed1cdaa3a8de273', 1),
(812, 239, 'APA91bEcKfZzbtuHmyCw70_jkjeYZ0IayTBe6Ae5YDnOP5C679Xqg3pKzyi8clxixQ6xWsRTAJfUUv1My7K_HJOfJ8Dkf80UfB2B8txSGbCVp1gvBTbRCX9sJWyW-6P1duBZK1ijAfDD', '427b643cfa83c2a', 1),
(813, 240, 'APA91bFtQMkZxjf-snp0IV-r3b8DrDLVqHT5SnCCY3ZA6UzR--_FmDuTuwVkjQK58bhMPYO4mlevtCFd4gcUjF7AbfAIOQELnJPk8zkfPzdtr1vHr1XJ4mrQA4d_ZbjAQX5G_5i0rQ6r', '88833553dc7d7127', 1),
(814, 241, 'APA91bHK0oBZGtgEvvMpwMpxs_zp_qCQBU1b8d0-wYK44qXYSMSLelS20S3q59pZ8nPe_ixsY9JFA5gqhxTmNWR-c2pGhK2YbRrFNyUCK5__HaFvlnSGEhBpWY-7gzRQGE1EkM8neAe2', '42d99b73596a2617', 1),
(820, 242, 'APA91bFwN9jWriAmHXlow_vFcgIuqHV86HXD8hjNyoePLrQK81RMHOqTLmzX5uaDwFEO5Ms5mA-iaiSSmOJ5Ioh5OZP9VqDB-wrhoB6NW7leOcu7NV5XtJZa3Y6JtLKHreYwloGRlxpa', 'da32f31cc5dd53a3', 1),
(821, 243, 'APA91bFTXwgUCMuSwN9J3WVH2GZwBki4ZcJU32UACYks6vc_-CwPZz1LsNgO7X-NhTeGumfqY7Cs8ShUmbCDiFhRAMQh_5e4TsiEmV2HnMfCVHEVXguJBPGw6AJzEXBo3TPwKa-jYIJO', 'ce64c89b171f1b17', 1),
(827, 246, 'APA91bFgGa5Legms3R_fB-hR_I75vvBK4xrDo4097YNs3YhCZcjkTWX_epj3T6PpnSvXC_LKaNQ6IVyeY2Pr4x6eV_-zKFJYhaTO8yBvEv81mf1OH9rOp40lfQly0iGe3XkZPBrujaOo', 'd70b42465a5e68e3', 1),
(828, 12, 'APA91bGRch-2TorkLRD3sNsd0y-UiKYdiUD6-XAiBAz9uzWIzYcXUUeZE-fEw63Dvcln4B-JoeWobeuLDAMdoq1NZ3kFmG9ZD6J3IfHuSdGV3PvG8TQXA73FFQC_PjzJvHJh0bC56DLK', '9dc219a299e16315', 1),
(833, 248, 'APA91bGnOqUE7dFH60rfXN45bZ_lZ5iG4-tvghWpERI1pKBAed0jDbJJ8CbB6dpVH7k9D81ALedvlnpsUOxeu7eNAxWl2j9atH5Ar0SfW_KYjCPQvoOPiTOZNAHliFV2yb3ihbpU3Sqw', 'a7e9d173f24c961c', 1),
(834, 249, 'APA91bFN4Q8iR9ZAq24zrndIKdBYVVbqzpFkyD6O1JteiYPLQHqI5Zd-yWQ6LO-7RZN_-mZtvkRdTLbRYQ0x9XFhj_qdi-RlcudVL1Ht7f2VHS1q2pSAWpGky5--AFZVxLm4Nborxh1g', 'fdc2cdaa421a8a21', 1),
(835, 142, 'APA91bG4aUxjQkO9LvjU2-JbubocIupCd0tM7c3fsAdMVVpM3hDAUHPvGMYIfHrpj0GfgttMTFn6dmBJCbdLFi0tlzC0fFh8hSySmT2nclXu7rZfM9rs0q2F_QIR7iQuIcV7Q5ilJpHg', '1f40dadb910ac762', 1),
(837, 250, 'APA91bEWiqkNb8F6ezuMzNYm5dJuf1rT7Z9R4BgYYhYh0NcIOAZYQ-gqadHaqAkOpaDOu94XTswoLjj2rBSm9_1vQS-gkzDqUEMFz664t6iUFy3Pxk_rxxA88SQdLLZ_coo1UguXSNiK', 'bd85cadad2ca977', 1),
(840, 251, 'APA91bH9a8sMhY23STVTVxglkaDHXd9BlnZYhyrAbuWjgag6PrCvt3lzyOe866dxkoKjgVA3ncaREF21RVJAQJnihpg_q8u-qedd5rwgOCedbVpiJBLnpyfBmNDzUYBMMX380nCuub4_', '8a082f677405763c', 1),
(841, 252, 'APA91bHOWQIHMjLTfdMjrcu1rf_vresXguJZmWcbk0Rpl-6759LLdjI5SEuf_SrEiAA0J9S2u7ArEJRu1k_8bvY2xCLASx1eWIHwde4ZW6xvzYU-4_b0m1puR8iZ7B7MO_7BLc6yCsu_', '45cc70de43012bad', 1),
(845, 253, 'APA91bFvG5ek6Do9BR4hTI-46WcuxbpIaAjEam59vwZeLY6uWiLSiS1cHFtROi-FGpJCwBp-Im3KpnSj6j8NrhJltp1qpyEdBR3z3OzokpzCPd7t04q1FLP5SFjqrdkx-hZR2hiP5bLC', '135ad43b6d56b2df', 1),
(846, 254, 'APA91bGXGZ2tbBO7Vl_fHzi4YMR4AdTB3QCpoKajRp0Y2fAZq3QCBPosd7wi2R8mppMoqSieHa1wbcbVORsoPZBy7q5EOp4Mienp1KjoSKUuLfjvjpGaDHL6wz60XRYcoN3Sxujkcs0w', '41573fea5df1da69', 1),
(847, 255, 'APA91bEFmJrnqpJ8kdlmPP8jOl7NA1AmLLu5OjmuQmpQQ2aiQJ1G2G5vF1ZwJBONc37zlDPVNRBAtKSD6UuM5CELDoqtCvus7-g2dn50udp1MmbsjgvTE3EXZnoB9HLExDfCdT3Y4KKw', 'b1f41f72dd80b2b7', 1),
(848, 256, 'APA91bFCc5-Fq7097tWIs9JHU6R4PLHasYvQ8atsK0hGiMspyc1dn3W6spBxRAHBJuoUppQNaSdhhNbq2ELd3PR6Ho_1UHZbMjkV_TwLjO9WUh8JPLN8Y44pj6COBVksVocazXTQvdNF', '6a292c247344098f', 1),
(849, 241, 'APA91bERYfj5E9GFbcImq2TPoz0iMQ45AoTEB_e75OUHr9XkTx26PbS5YriT0beFwc-nnusvikiWkZvg1p4gob6jl_z1pZmRNj-2y1PTs5T73atjX5dWeDHStJ4hnfCvTXbalgxy-MRG', '47fc9ad41e3ecdf7', 1),
(850, 257, 'APA91bFVEUUbYNZekFx-aqDC8qGcX9cOaBKI2mfbfnsE4REthJ-47TB7n5bk4dHZ2jWAKrrE3vT12u36hyhFUnYeQgYyChfou8JXTMGYTdrlE3ET_RubwmFywsEPH-Bq9nQFmvBMvu3F', 'de82110e1075b9f4', 1),
(851, 258, 'APA91bERaivUPgY10mPatIeDQ3KyjRXAtTXtWK7waQccFQ-L5EEJ7zIP6pk3A6wlxRktSq0LBhowKZHg5UU2b9KagCZ5QlcqPiBGhakNh8HxsrOv-fs63_vVuJTXBa0hePN1-mbFd4Fy', '536d4b0799557d8d', 1),
(852, 258, 'APA91bHQNkVHp0AoebcQQujjbAMbvpB5tHGXlvlGoY8EdzZspuEXydWb-7rw-_KLRkHY2l_ltFA8NFT33uOHhfgzbAc9F1bG3TTPsoAhsKQSAkF9PrIl_1QH-ilc-14ie_l5msdavG79', '4603e891adfdee40', 1),
(853, 259, 'APA91bE76tQWwYb3Cor4fkxSkbGfuj0a9t5o3ha_8ZAa0GjHFxGI4_QW1fk30fqKWg8PFJ_MK3gMQiblLKoMu-z4aVHUFezGknBvhKaC14FUDVofmEyxiuu2Hk-00S_BHOha5apwlmEt', 'd2001eb98652d045', 1),
(869, 262, 'APA91bHbsiPsCGvoPhdHh1n5dlJdbFapc8iMSC1M38I3CC14lFWpEMkqb0wXu1Koa8Qp4bepc48UJTRF28bBNjYQXgKJ1fXerreqnKgBY7FugZrVrO9AMnvFD6D_O5OWCIIkO1ZF5DPZ', '7941d4731816ed1c', 1),
(915, 267, 'APA91bETDmCLZTg2Eb4R0wfWa-ttLJlA5YtjKQLxR8SDQsyjfboWLPqpaSDIXX2U2XVoI_OP7xtbcPG9StE8nKuhsv_GCdX4PZWfguzAj5HFN7LdO-XzGKZbJxqBNWdCn7PhsOeH7D9x', 'eb0e4aa07a547c3d', 1),
(925, 268, 'APA91bHJ8BLwXLEpyk1Rm79qqBSexmgow_yrIKn780vyPMZwp8IYLqUiHfeqeFyPw5rqOWzwgQOzeRKPto79aRF7Nj39gu1-ypt06QGh9yn6n3NvHoZ9bY3HrzWqkSySvsTg-WN82pbY', '7eb063c751cf04c', 1),
(942, 269, 'APA91bHdeLHoI0F9ApHTH16gVjD1iH1TRJs5Vg2ODmThyTBfXXC2kcGHWvkyudUWIZg8Kr4zLoIVDMLxYEvXtE-5Up6OnDXeQyohwpMu3VYaNrEdOwxV8Whl4N6DVayZgI9H8OvpU8b9', '4a4f030880ec6feb', 1),
(943, 182, 'APA91bGKKYZ3SzK4Z9rYQ0lmo3y00LSaW4gzhEhnteQMQ8uEoGjB4mDchXA-73GZtRWrWWlU0q41qB4LfkPJ9kkLoT47jWpOXj_KVz8B3FM1hNKtNN2TcyCONzG-QkveXaR3r6zGkmBE', '9aa9bff476a18991', 1),
(945, 163, 'APA91bHURHGDWxO3RYbqobCrNAPjcz_5j8MuVPNI8Um5_V13C3r4dcJzatJKuP-qzFJgi6khHNc0iRCluC3xbTSzNquwq2jTNosnwM8jg0mKwgj6beFTUmi040UhtHFzRThoH1lASR3W', '28c96e9f3834c8ca', 1),
(947, 271, 'APA91bGGRzy-3bs5U7p5FsFE3ew9lpN_KvAOH5F3poRHJ1ML23Br9wIJhc0Oft6hnsrwQRPsSZl7n3h6Z_n3sQe7gglVz1t6KFjWdC8SszXRjUxzIANtIgwxk5A8SrMPVqJB16tBRotD', '64ad03ce1f6b1702', 1),
(948, 272, 'APA91bF3ZJHNw2M2kkmUoLVIm0aRBby0hiZYElKTGWparVy5Z03YW13WMzApN4LiMq7XqyYnoDFPyqF9dWL29tFGaHTNg8_fr8YpR5GrDyo9AqoMBUQFaHkTqdpu-tsvRPHCEajy4Hwy', 'eb8a5eeded17ab96', 1),
(949, 273, 'APA91bGxsXkNOWQmclYXeJA9wyVpUPqbuYqz84n3clcQiY_N-_ZBSBYFaf2pajWTOPgCTW1bZx61pDIFI9b-trFnB3guY9rNrAsmFWL69aw6M4dKEPLL6Zjz-M9VuzAbrj2sYgzkZSKK', 'ef0aab10cb92a964', 1),
(954, 276, '', 'e17af1d8-2926-412e-a111-f03a09cf8361', 0),
(956, 228, 'APA91bEdRXup1ZEQI292JtZrLba4uIrQAUzHyYqr-uuDQvly_0HGD9XYO_p2cAanRbhUbkixirSHI4v-rd4MRcWeXMulEcuqmSkGl4GSz38s9LNTnrsq68xZD5KsL6UA9oNb5UUESarm', 'ae0a153a33b191ae', 1),
(958, 200, '', '280dc3fd-d8ea-4589-9a3c-3470aa667bf0', 0),
(959, 214, 'APA91bH4SRu_l-7ZJkWfKl7t0gZomL7AiBLzXGvt9SwAMMNRLgfMiXcZqRTEk_fQpOfs4SNj-0ozTk9O-XGtJ9DdGnkY-3qGhPTvPe96-f8VZOi6_k78Gr9PBuWkV7vAgcNBijGK5nkm', '88963772da079509', 1),
(962, 131, 'APA91bH57WKMTxldMm2H_JQTK4umEaztOp3Z_dAWaGgfJadrzNEQJjeM4DOxUeyh6iRo_VWlBeQyyamRhVT6y_MW6fD_evKccz4qmrAzHJu9_wua6f6vfzRaAZ24HPr3MJxISHLrBjIi', 'eaac7d56fa115577', 1),
(963, 200, '', 'af63e1d5-c8d0-4369-9ac2-015079e7b8c6', 0),
(969, 277, 'APA91bFhba6dy7BXes2N9c96fXvsAHyCtHTEtuCu23ncD97qX0tCHyEJz7emL0sXzlTnSJ5ov2PUyRLDhjEOURfXZWq4jH-zm2RFwdrgbv0jcMda-btQwQIL3FjQEI418KKrhCqSg5Uk', '98f1eab120b9e193', 1),
(971, 200, 'APA91bGGDY2jx5j0n8fGJH0bqW9dk8cuEzU0IZwc3wj5J9k9gSC44LZoOJDKNUW2cPEF2Tv0p144quRFZbyvlT1ugkQSxTradrT8gJ7i-HF2PsIF6KMtLcwX1IF5wrkJqUoR6Np5oa0r', '82d90a676e72cbaf', 1),
(972, 278, 'APA91bHrNOBp5XOURfwCKVail8bEXpcFCqK7RUKiEwM-Pyl5Z9ltKgoZLP3ANp-4QtR1Hqy38c6oO03-6cpW3ztIzxWv4VNrvpuFXdrcjOs97ndAgEBAjtAo4T6WIfEfQ4hKSx6LyFdn', 'cdef1d0d851a9dd7', 1),
(973, 279, 'APA91bFqJ3ZgkRodtP63aAfwHkzvSsKD7fMZqhkDmirlQXSM7Y_BrX9Pjw73vyVA_s3RCUqqn14C-ilPdFBMHo4rssFe9295MH2ZNRjfkw9jdDhPbgtE7wkAfKP0QTcMn_CDkIwyzPVM', '16c45f07bb852b4', 1),
(974, 280, 'APA91bGqdccPr3A3CJ2iv5ZJUgjMtxLK7Uwxiueu86zPpZgXl0MkJ4bKFDmv0ALGMOtwHAEoDBnDo3b9RipaX3NBAy2jh9iAmPXjhQeUlZM0yDNsTjWqRXUjVMgM7nG78yCEf8SmAh6j', '45a50df4476f1281', 1),
(975, 281, 'APA91bHsHpUO5j5L1N2bEehkL6eG-sNiXY3Imbl71TlZg9r45ZvDZmQsNywiICFUeLnZFxZ7oyquSDt4M-msskkkjz2jb1dHKdNLsAqZj_dRjik66Q5u7pv_mrk3lLwTrGigHnvuLCxA', 'f76ba8502d2e0926', 1),
(976, 282, 'APA91bFv-gaa9kmJAQ8rOamCzQ6GDdnqrXUngpy_EAaJJGbpK4llrwRYt3u7FVCOcbbO-zKHSPRBJBOc-quUXYZ1yIZgNUIQZLfhKiH64OSDQCme_QnVZJeilmA5PhnImQx55mEGkrHi', '6ed7a6b59c932636', 1),
(977, 283, 'APA91bHhWloCAV91I_skbcvRURltpcz0JaEDZ7kxi2Yak6VgMaViEaO7vRgyLoD6X7pwHjiRWiBWfHRhaKqR384ZV3fGkDzMU5OHPRz9bci6vC4UieSD9eSLmEpOjN_5QP5xKoN4sONE', '5bffd93a61ac91ca', 1),
(978, 284, 'APA91bHrYQ8bvy7ErVCiFW678gJFZr3pOGG4okuasUmXzlyLSV7iCiyrbAw9Jlc1kMszFkpD1nitIGEmeyQtO287OID-iUPk1Ru04LbornoOYl90XYRkVuoLDqztBWCe0XVXPSeWtW-M', '183cfbb4d7af7e87', 1),
(979, 285, 'APA91bGdqx9UxYM1FuwpJaEfUYuQ3_iKKDoh6t86KTqmdqbv7WsOUpLF1uNbHoNaGI3OX0YxNdfsEoqcIkIMVvY5qPGQZRk-Y1Gk0B48WR9rZc8iBqmHqSsyxLyEylmKGZnGPvSXKKyD', '8a8a19199da84d1a', 1),
(980, 286, 'APA91bHOQxyz9fYx5Bu7tYilMlVtQg4gPVjBONu8m_5r39JR2gqum5bve-vYy538O6zSnOyi2RZKr_PV3phubxzKv4q_SoFnFdnXCC0rFz0X8-E-_QU7OC4ozS2LuXcipNO-onlig3un', 'f002782f22073f17', 1),
(981, 287, 'APA91bECHRu0FcNvpN5RXTFor98WY2wVzHfHjj_UrH87g2tx2hKendSpWfaGTnFWuCX_GHStRVHBCRj7xlppUxpu3zaCVOTOJrpV4VUbpCtN6kr2CN92pXFNRa49W44k47a343BskWxI', '1697693c46ea9625', 1),
(982, 288, 'APA91bEvIuSEiZpaYnPppKNRGq4C9X82VGwL18t8X0Tyfqzv-Qf1oswhRL42eXkox2OjYu8XDmnn3aUbvvJs4yYqPYGJ9inE-JkvxnmW3mcSLxtdYsLaGDEpkj45Akmg3O4cHkLZbH9e', '1178f14f8a0eab03', 1),
(983, 289, 'APA91bGELnh6tR8d3kTTWAsw6x_qcrGTs_3z619kAcoEafTcdtOq07bhXYbrkKnXRA-f0aB9hQaHGG3nZOGgJfJaxubVcsKN2yzeWV0Kz_TCwjYnsLfIuKTvA12TrbgOGJvBzp5hHFtr', '8e9413663df31825', 1),
(984, 290, 'APA91bFEqGba0pH8ZZvvnQnJWOtIaf4cPLaUC5TiZOPH1NT0oSscH2862Uc9lWQMHY8ft9350YOj8-Eacsk2bkJ-Ppq9uzouL-rWLlEceoOj1bMxpASeA-QGJGyk48GqdmE3Mu22L-W9', 'e4c6ecddb5069401', 1),
(985, 291, 'APA91bFL-CYfr1XUrutYJyYNB4-FCkxiVIm6rEXBy12aR6l19DdE9L8E5AkkPU5Gw_LJmjN3ffaBTzrfbYJrDLSVg6EZlNl6e-T7YAsHzDsjQqSb-o-Rfj6kDXA_2Lldi7_DkdrBPz2p', '33dce533a0f0c98a', 1),
(986, 292, 'APA91bHDY7baX8LU5TGyHUza30Hs9F5cUssc_EGOavLDm5yaXvC_qNKWywIaUti9wWmuSYad6juKIjqOweZeObI4xdtxw43PEA6rcaNDrGP8L5fEaUB8gVDAEbVhejoB9FdZa4FTINrN', 'fe07bbc595f28aff', 1),
(987, 293, 'APA91bFtoeVavkymg0Z3yFoduKd_DVh2wCZ4hg3Qm0emKxi4X_SBIkLXDnpfClAX4o28f520VsmOP_MurLQ58Mr40klAQ8QV2v4jiK0jxFHXouh5sE4JWldVW-zYgRBVchqx2j6ZcAWP', 'bcb380bd9898f41', 1),
(988, 294, 'APA91bHDx_-tecKh8NMATd1IF98vuP1v4DY89HdmHyAnOMr7-XtDOkauJFxEy1BNq4WIWk4uPh_XYMgmlWFuPJTwbKOvMFAzOQR_qgvj3KHY7qbGbBlOs3fMwI_Rn95e3fxZOKn6SXUY', '6ae111755bb00e33', 1),
(989, 295, 'APA91bExPlD2UUzuLBOlb3U9KYhqV7-c39apaob1AH6zRqo6xRQqSFbOndv6Ub4gr04KDlb7-5dLYugbWdqgaJKnu9TyNm0kZvyjdG_EcQ6Q66J3ONQl6sGdRbeahyeRamiK5UXSU4xJ', '627c6bd2b3e80536', 1),
(990, 296, 'APA91bHcFFFxqwS5Aju9ZNhKxbCCX9PaEu6rUGoELMmbH9FGTyv-7rBQKOMtbFuJiNAx_2h9rOFie-vHgL2ou8ufl0McR7CzR_bghEDeTFJfJDn8PXgwYs72oIxOClAjbsvautlNS_CG', 'df8665840cc296e3', 1),
(991, 297, 'APA91bH2w47hagm-f-1c2F-Pf4b7w8tVmHZ87ZpR96FO1sywOSS5RWw0f7KFrC5mSSpoD9rf_Q-k4RLgc3d5fCwQ6lLxhhVknnLkT-2MaN_G93RgrO8ZAbQHlaE9hBMfk6nuQiNf77-l', 'a806b27ed2bcab3b', 1),
(992, 298, 'APA91bGAOHz1LzEkLg2TQlqSa6eXZRvCJoiAjnkL31thM8RYbTP9-n2k8hq8ElhuM53_WJU7TbqZusjH84dIS6maxAAROuF0wvQCkSzL_iSw0zzIcfe_OOKMF5qe9qOjK9Gbg3JPnHDl', 'fcfb201bea8e6417', 1),
(993, 299, 'APA91bEyIHf08I7TMwbrx7-lGqrXOzveH94g-FL0ZlL_p3yZ-ePz_xQjdjEbetJTUWK8rkhkdNs031UCpXfEIDTrHHD7AwZe8fC-IFloXnl41o_KPMdbwl8drES3o67fXgYlM_mX3WwF', '71929fdfb8fb26ff', 1),
(994, 300, 'APA91bFWPIah_1OKERTUUQrywyxQFDK5UyBeB1gh4YrMUlUPeiLM_u1QlMj3XaxdZ8KhJAF9rNscRn0QjTNuhP-H6FhE8aB8CyT3UNvUdWJ54QlnT5AeBZCuwZ9I6cLGdz46z4eKaGfU', '5e00cb61349e6fe2', 1),
(995, 301, 'APA91bHhOrXC-mdXvB-mGDQiIG4U7BgnMH0sW6SriQFo3ievzatd2VDdrVmsclr6vQcS5vWIBVQOWKCljQz6lW68u16JhJRK1IPatgrTIqABJ2IX-EC1-QorakrRVf-oxsM9ksfKMkkX', '39dcd97d32586a71', 1),
(996, 302, 'e4595b31208d90e11750ca456c9f313f95ca295dcea38d1999a0b67f18436ff8', '36de12f3-30b0-486f-9775-45f18632dd41', 0),
(997, 262, '51ed407fc4d70b721973fa8619a278a28abfd1fc7354d4df056ad8d431e9193c', 'ed9b1b44-19cc-4e1b-840e-dd250c203703', 0),
(998, 302, '06df648f24ca2bb0469f6f1ecd03b4ca3853b1a183e16bd2a87118386487c903', 'dabdd67c-8be2-4f65-a6d3-ad1c9502279a', 0),
(999, 303, '65ea185608eccca7931858e132a2c64fdf0671b4abe7625896f4a12b32b9052d', '84b4b9dc-c103-4687-9f24-8fbd543a712b', 0),
(1000, 304, 'e6a66f14d58ae9a0a0488ee477f64c2ed52ab629302afdf157200b226e1f3cb8', '17bd467b-768a-4bb2-9f5b-8b437be10756', 0),
(1001, 305, 'APA91bG7-N9DqP7swOdgHfIlQoVXE4KLwxptRvTANcTAeEd8beKEjS-Z5NCtv1IhAp6nLHbUXt6KOuBmNn4g4qFmajbVNoTQPn1luwBKkjiwc-nV5ccmS1zjcTnChIoUlJxwh-l2HsP6', 'b9cc26765216df9e', 1),
(1002, 306, 'APA91bHF9kCPr2mVgb5BuyesnWGf9qRxYt6V9gEYUPt0MV9o3nER7INhN9MUpXbPUQpfKnJalYcLktNgPMksK_-8OmrshaeqowHBXAbCGaM9Y3UkfHIP9oWhiBS0KVylIumPLN-kL5j6', '13c9b0590f0e30bf', 1),
(1003, 307, 'APA91bFNjYk7rePH2FT5gBUQr8oZ8R5H9-ANaY6_wV1dr8CabsxaE65iVqCrcpFUqK_3ms22iCTkoFolVNAx5B2oZjhoZ1VHH5tpAsVKeXqaIUp6qWyBCV26GDYSrYdDlHGdRXkvQn-5', 'ace9abc202b1c246', 1),
(1004, 12, '520d5f0f3e0ff5a44ceea14d45aa09dd97689352b5b887bd7c8ca00e0813de4d', '31747172-19a7-4b0c-a545-08afbf75bc45', 0),
(1005, 308, 'a5ee45d82d5461e0cce85bfde0c716a370198ccdb6063ebfdfdc9a31e5a185ad', '0027d3e5-0b97-499b-a2c6-a437a4801ccd', 0),
(1007, 309, 'push_disabled_by_user', '6d01e789-d03c-4cc5-9de9-57680af29b0e', 0),
(1008, 310, '', '81d09c72-cb53-45f2-851c-86840b41fea4', 0),
(1009, 311, '2d063015082ff53face92e7de90d0950355a20210dca38fd7578b3772d18f9a8', 'd5262675-2586-4d71-9453-a399c6f9ba9a', 0),
(1012, 312, 'APA91bE1zZvamuOz8dOqlleZ2duLHO_qcuR8la6sGyM_srF7Y0CSYmiaj0qLAZcZf3679zajRCcmn7PbvlkxGJp6y7nBVj1tswQwoOpIczixk9XqINtxz9mxK1_eDnNXPNRe8Uz5YbLx', 'cf2ebc8be79697b1', 1),
(1013, 313, 'a8332b1127849d0af150b5a440b0c2995c04fb416675f305743cd9a1d2272058', '769b9093-0b95-4eb9-ac02-b755f554a123', 0),
(1014, 314, 'f84a89b04f67d2fd05c4d615e465300efd839bcdea3fdd2810b36480bb0c3802', '4c78384b-7e3e-4759-a599-284b22272281', 0),
(1015, 182, '8bfcf8b8b4a9b6593d1bb439a73caf8ffc4b785dfb0a00aa12fa82e2420eab30', 'bad8a98a-f566-4d9a-807f-fdce91ed29fd', 0),
(1016, 315, 'APA91bH9nvIjezaqIbt2pAfJgCtS7vxtplDYVkN4prJBh6DZuPRVLreypSOkkWvHWKbdV8ItRGCg9hV1xBrgBLEpR7bopppNZfR7PMTQCLyZPiFO6egL3VBSWOOhZLVYUSCCOrBT5RGw', 'eadebfd4b7c5b809', 1),
(1018, 317, '5f4e9c01fd12dc147b49df991ae79f440594aae16ffc70701b09ab236e0b007f', '1070074e-4897-4aa7-a7ef-1b4936e68448', 0),
(1019, 318, 'a431d25c1f6978bc0202b87dc687ed0fb4f7b197fe77819c85640e2e0fb03247', 'ccb84859-9a29-4ba9-9425-55d91bbd2379', 0),
(1020, 165, '886d7891f9cc2adb60f9cfc44513b9fa6a8580e05600072b21e3428451dd5238', '548c7702-174d-4407-abdb-0fc01475cc7e', 0),
(1024, 262, '00608d963f46ab00ed79af0980298298c2465f1fc626e1d670efa739909d87f3', '03197900-1ee6-4402-91a5-0af898af8355', 0),
(1026, 316, 'push_disabled_by_user', '6f222a64-3c18-4d6d-83be-1b6f003a3470', 0),
(1027, 319, 'APA91bHtQ5yusWqpAkefYaesG0o8-H8FH634JeHc-WOITh-PEVtVuF39RRnYXEoK4ViaVbH7jFEXSvVPuRtIBgk6grByKWvqmcW_LQbhf8m6qg9SaXY7rEKOq2ArhIENoj0TqUqNk2h3', 'e717ee5c312a3c4a', 1),
(1029, 182, 'ff4e403181d519ba2bfa427286c30b00075ca038d434e5c3a6c2bc4ecf09fc03', 'fc851fa5-3f89-4244-a90f-faeee29c98b3', 0),
(1030, 320, 'bcb825e0d6f36c8b2f850f5d8a975adca723822fe6ec65c6d8e56a4c10ce0b11', '6f1e8277-f025-411b-bb1b-659d054be3ce', 0),
(1031, 321, '449832cfd4536ba6eebe28b3b7b9149a1b6c5bf1750c8037d6318821e5db6cdb', '33d2b8db-ff96-42c3-828c-10dd392b2d06', 0),
(1032, 322, 'f99a6d2f4ff9da4435cebac70c6e2ebb0a1b1a2a5b0f95728b40816da10968e0', 'e7cb8da8-0097-4a17-825b-8718687969cd', 0),
(1033, 323, 'c08faff16a4cca663a1db146ca85d32963a4faaf7e9d847d6c1c9df9aef7761a', 'dcc598c2-f8fe-46a9-ad48-b88c0dd345ec', 0),
(1034, 324, '4679c7ec254873c66b64930421b9b574cf0835c190986b63b70ca467998f176a', '36aa5f86-9f24-4ba9-9938-e0344dfa0af9', 0),
(1037, 326, '8a2a6f5db88b72160e2b22673f76ae8564188a55c96070a56e5f17b34ec101a9', '6c22820f-75b0-4daa-b1d2-93c6af62dcea', 0),
(1038, 327, '6d7e5fc9de6dbd56d7e237749c4f0aecf068d550b41e57d22fe0100eaa1cb3b0', '44784864-98ca-492e-9ce6-8dcab98cc715', 0),
(1039, 328, 'ff96adcf0b1d2a1021970252e6e9eff45719e72c7a4df2d0c0b7b03f8f4a3c43', 'e6c5adfb-7b68-4071-80d6-3fce25504793', 0),
(1040, 329, '1a5f930af8ab7356a7eb37224d7d0d13e288fd1089f08ea1518657d0acf82631', '53a3cbdd-5087-416a-a8d1-a0618dfd0a5a', 0),
(1041, 330, 'adcd6ec15fd1817b6a9eafd318115eb9655697725a3c2360666665dfe6fd0dfa', '09e558ac-f853-44d3-a692-b25c5d4d7beb', 0),
(1042, 331, '53b1259c67c45e5b067654b17a33b81550db1cde3a80f299f43b3ebd27a4825e', '4672ae97-ad94-4b7e-a11b-969478c5a8a3', 0),
(1043, 332, 'e14283e063bb292bec1c8e605a9bbf682fb69920e6c1d9f7688dc484167d0c90', '21d4e14f-d48f-4786-a09f-26be4bbb2a56', 0),
(1044, 333, 'dde424b1c8f762f4ae66749fefd8ed9b8a397fb70dd2672960c89e23892d4bfc', '78c89ac5-7f75-4989-8fcc-c09dad03c33d', 0),
(1045, 334, 'ba6ae50c6a4fb7775cbb8dc760d8345c1d9c3dae5ca6f7d28723b2b439a5c186', '20549bde-a6fd-40d0-9b30-bbc0883d5bb1', 0),
(1046, 335, '63be2816ad06674cdbb513ff96e5bd0dae1c5c655693872168fef23c3bf70510', '1f55c233-d824-495c-9cd4-93916f67804a', 0),
(1047, 336, '2a3ea5cdd62f9216b98e6faac173b92bc87f15cd302ca7606f4ded37784bde81', '18497d31-84b4-461f-bd7d-4d3dda1163a2', 0),
(1048, 337, 'b4cb4a1cf7a92d40e4c83445c9efae72076e64bbc43a5eac21651631ab2d4ddb', '203e12ed-1d8f-4fc6-953c-1734452daff6', 0),
(1049, 338, 'a25a46e7216f17b318becfe349efd7aec87e9dc656da227deca8d6f36f9ba91a', '246cfb67-6e16-4944-9c9b-df11405ae523', 0),
(1050, 325, 'push_disabled_by_user', 'cdb4882f-918d-4cc7-a767-8dff9b188958', 0),
(1051, 339, '4822862b47702d53a8cf9a46d11e7786c79dda980fa936b7b60ec5e203666947', 'f362cf51-4b8f-47dd-8685-33806f973243', 0),
(1052, 340, 'cafae19c91dad038ed7bb6632ce10d7c3ca2403c73bd37e653686ca309c06c45', 'a6eed8b7-cec2-4913-a3fb-675f0ac0131e', 0),
(1053, 341, 'bb8af7bb0d556a899d2f515e65d4df82488a9f34636afa5510c888de79b1c8f1', '7079f4aa-c5b8-4b19-b47d-6fa26737dce4', 0),
(1054, 342, 'APA91bE2AoTjYrNNthnajzw6wxZnsSLUHoHdq9ILON5mucWz57P04LOFi3LauUM96NCcUqDtbHHl097LMWS-oxvqFte_usfTsb4o41MGLCX53YMjlKvvmzl-DDqCVGTYe_UxFmT05k2l', '59fb6d25482eee21', 1),
(1055, 343, 'c7b75cd8e1e3b9b0e329af52bdcc4a350fa10310fda0b7aabbd442e2021a7105', 'b6f6d6f4-cbc3-4327-8f1d-f941a2c4f871', 0),
(1056, 344, 'ddb8433382fe7edcd77867f6ee183d58f0fb03168fa75f63624a346ea5c66a80', 'c60b77b7-bac7-4bc3-930b-7e7b406b174c', 0),
(1057, 345, '27b278509eb7f5b513b17ab218ce34413b57042d5f2d2845513c9e486011d94f', '9411f20f-a0e1-41ed-8f8d-dbe01db29df2', 0),
(1062, 347, '2afb2657098ae3a749f4ae460730f7d8e41c4062977386608fc13efbb42ef46f', '8a5077ab-c400-47eb-a937-0fafb1c2bf4a', 0),
(1063, 348, 'e35a53e5d7ea02cb6bfcc44927fbf4226985fc02741b17b4e55b07c9c05eeafa', '6a8d15c4-1c49-4418-ad41-478d71db7f8f', 0),
(1064, 349, 'a8471512ec353675e7482667f2b1a8eeffd61152ba3033f3a31bd2a9450adeec', 'ff7bf5c8-71d3-4c22-b968-17b27e1e3c3a', 0),
(1065, 350, '7fc0620b2de83fed0b147e9831343a9296ae48f3912938d81cff0148b419498d', '75c883a2-0856-44a6-bdf9-4e23d02fd93c', 0),
(1066, 351, 'c8da36dbba76d9cba0788a9f61b8865bcd65abf3f2300927f28f4c48cb61881b', '9be791a4-1893-4ff5-8de0-7c48899ef63d', 0),
(1067, 352, '7e1cf3d2d38cde32f03226994f124441db1c5ef0660daaad5c45abaf1e738b87', 'b09f9132-0027-48e7-bbc4-785067a706a2', 0),
(1068, 353, '9c47bd6c6f64f23a4492528d7a33c694a4e31217626db6409485c941f53eb675', '45205a7a-2cd0-47dc-8ff5-aaeb952db67f', 0),
(1069, 354, '1b300f0a24862088a7c6bf1330095c1769c11008851bc8fe3750ad67a9d4b29e', 'c3fa0eeb-ec09-4a41-b264-0a6e116bab17', 0),
(1070, 355, 'e01e3eb8a908b146cda945dd8f73b5ba70984d19da28bf6fee75e95b39065986', '5de67144-b629-4a75-9ab6-84fea5636bc4', 0),
(1072, 357, 'ee4172769a7c3bb3237206926c5771adbdbf8d8e55919efe9e9caa6c3f458348', 'dabde62d-01f1-4815-884b-1a9aaaf28294', 0),
(1073, 358, 'b320aa018f8c5a11e406707240397d082221dea189f7e0e48390350e32452dac', '67bd1c56-b20b-40e6-85a1-512ede21f524', 0),
(1074, 359, 'APA91bGcSbh5-0rmP4Qw7oHqQWoiZmj7iPlgMWhsIIvuLDauQNQohGzVIG6LUuKKAyWOF-NvmDv5lK9kW5umZfzpVaCVuDT67yJkGgDszRDxKP6Kboa6SbdrkokWSZmP4L1kTBrCtjJF', 'da04c328a2b22967', 1),
(1076, 360, 'fee784fa852e63d4d4f64cda6473dccd62d1d34035448998cd097211261f4d76', '28adcb72-3240-4dfb-bd7a-2130ac3b8931', 0),
(1077, 361, 'a61a378a0ef68cd09725dc2249f64d0280f2fc58dff2dcd902fcf7f9ec498634', '13b13828-3d74-466a-8c10-6dbe612c7070', 0),
(1078, 362, 'aa648b1738d0e9fd801956e907b1c7b754c5e4e8a873ad793d7e562a9047c6ba', '764117c9-e1e8-4471-939e-2f7cbea2e6d7', 0),
(1079, 363, '6d68cc04626ad2e6e11e34e3be3d3455a80696c4b673307f58b6756ae87325c5', 'd7f841c9-c3f5-471b-abaf-d26e82ba01c3', 0),
(1080, 364, '127185d769324d1a7e2167f7977e7438418f8d3bfeb375caf5edbd14894b8302', '84a58c9d-3a60-480b-a336-d863c77c0a30', 0),
(1081, 365, '', '7a16fe23-afbf-400a-9fd9-2f9bda2fc7a8', 0),
(1082, 366, 'c6d337c699ec37ad91a9836cddfb02c771c5f2bc0014d227da11fa472647635b', 'be490ad8-c324-46e1-a078-e512e31facd4', 0),
(1083, 367, '9c26adeb9cadc4d5dfead466809c54feadc2277876a8aff6876e4898a6fa0c8a', '9f9d56a6-ed76-4763-a5c0-b2122799d3e1', 0),
(1084, 368, 'APA91bE3zGfUaR3iuoDVyYG149jI1BNjXuS-4jiMH5YqfZq4ChOyzssMzwCYivSdcQzaIFtGor_0K72daNfdEXeVr5pgHG2QHnoLINJxOFtSCzivgyKYxCuyFgJ9cqshZw4iHPJl0qSg', '437df75d5eb79aa8', 1),
(1085, 369, '22ef027499b74e8666624e587a05295e099376c175dfc37e775f4d1e0a5f47b4', '8688adbd-40e3-4f25-bae8-1b4b9028c561', 0),
(1086, 356, 'push_disabled_by_user', '74418724-efbe-42d7-8b65-3e1aede7485d', 0),
(1087, 370, 'APA91bFcabZ6lyV6d48or3KI5eJlfxCiqZ2CX5e5f_gcrDOIM-h7JmcYbzTzWdHERbkjaNpraKa0ZdTZhPXu2vBEZtVaybwGMbF-rSqqy4pV7R5HQK1h92jZ8G7ktu2SsIOwfg812jEP', 'b119349280ee000b', 1),
(1088, 371, '94dc095e05e630659c9c41e1feee08965d17110b0adf1c7a0f35e39fa327c83e', 'd410e4eb-a2d6-4957-9a7e-276678f7309e', 0),
(1090, 346, 'push_disabled_by_user', 'bb12593a-0aa6-46a0-b370-c7d6f068ee94', 0),
(1091, 372, 'e5101de36d085f680e2a4b6e49d52881fca8dd75f1324be1c7ceec694ef07771', '17a05bec-faef-45d8-af8f-30a447bd146d', 0),
(1094, 375, 'b2fd5ac9675df778d530646f1d540af683bf7c0eb4dd521837c8eb97b2bab735', '524252e8-3dd4-48c5-b7a7-5cacdb2c564d', 0),
(1095, 376, 'e2fd6f658a306d06274fb43222b12dae39a2e863d9ae360560e7150a42834e64', '0392fd02-4465-4af3-959a-e1300f81a214', 0),
(1096, 377, '657789682d47057e09a8f1937fd927f84eefd6b3d5c548084ac0615479e334cb', 'a29a2d82-e609-431b-aac6-a27ad3ef416c', 0),
(1097, 378, '1aa0747862dbd7c2cf57d67e3c7acfdbbb9a7bae070db4fc99a51f7cf566047e', 'f246d42b-ca49-435f-bb79-b2bff1ecaa4b', 0),
(1098, 379, '5130c0d1402ff6fc7f5a98306a7ed5aa3f99ff0176670adebc0f92ab25a2c8cc', '3ca5354c-4b3f-41d6-bb12-a36e6d2e537a', 0),
(1099, 380, '8039ceb609cda723ac7d35ee56311435066ad21bf2e39946e6088f10a9464d1b', '0580c2a2-1f07-4493-867c-4cc75df518c9', 0),
(1100, 381, 'acf678deb6082e7b9022bb9319b38ce73919e1f88862f2d82a9efa3c5d5217bc', '3c581a83-bc80-4b3a-accd-49c3e7b59c06', 0),
(1101, 382, '59bb8007a0463eaa048d3d495bc8f6fc346afbc3db22d2e929d30db340105837', '937d4960-cb1a-40d3-8cfa-1f21e66982c9', 0),
(1102, 383, 'b65ddfb8208f2b44ad942b02a791bdef9b4f0fbe6e0269567de74b21b204a0db', 'c34e1bff-2f86-466c-b630-a22dfd9d178d', 0),
(1103, 384, 'fec8a8352fa70643bad3fa176ddca936f5e91350ce898a6d2e3f67db5c082780', 'f8087598-d46d-4be8-9493-3d932569a78c', 0),
(1104, 385, 'f8942459b7452d5dae0f65c76f6084340f4479f66d650e50f6706ebcc717cc9e', 'dff0cdac-ec70-46ba-b1d9-3ad71daaf835', 0),
(1105, 386, '6cb8fe09b62b6b0ac0beb0da1ac6ec71804358d7148148be48be7d551d99f4d5', 'f847ffc3-2970-4f12-b6ec-3a60774b6322', 0),
(1106, 374, 'push_disabled_by_user', 'f83cd442-d283-4701-8255-c4e8823dd2cc', 0),
(1107, 387, '51501c007938382c5910ea81ca534e880ddd10637a8b3ab9c55b8c5131163fbe', 'bed92578-776b-419c-9b4e-91603020b77d', 0),
(1108, 388, 'f8677e9a7d404ebb7113fb92f845388bef54d57315245f30cbd0d1ce65d101a9', 'b3be9638-1343-41bb-b402-4c95d5a67bff', 0),
(1109, 389, 'ff0a276e9f51845dceebda23b19b2216292cf580d1e4cf4f2b33c0eb43a7c102', 'e2ba794e-6e0a-4af5-9550-866bef69e8de', 0),
(1110, 373, 'push_disabled_by_user', '0d95f8f7-0448-42a4-a0ca-4b05b7cacc98', 0),
(1111, 384, 'dab4eee483c9658f4e12ce535b84cffc4d720b6b07b125e0b259ec4c7eda55b6', '73a5cbf7-909a-4eb4-859e-19bf7c982ad2', 0),
(1113, 390, 'push_disabled_by_user', '40284378-0074-4e9f-b38b-d46c2f8302a7', 0),
(1114, 391, 'APA91bGClZZEM_3XiG1gbPAWZJmKVYkVIUt3j8t2NADeqCinOQN1MDYWk9-FRNMSN4duyQK8U2NG60Qlnv2RSR8u6n7U32wFd7GFYmcgjAf-jQDLUXSt8iF5zijGXPaD5TI_0h8Lxzxj', '658f6b4812380831', 1),
(1115, 392, '5202d5daa02c0891a30360adc7067f09c4e6d6a9cad5909a5de3d93ec2912f5d', 'f2d2c47e-590f-491f-a1bb-85bb6259b7d5', 0),
(1116, 393, 'c43c5d07086c5be861782bfea9eb25cb18dfe4c78313c760e73b1a060bf43fa3', '6a8f5e16-f8e8-4ca5-aedf-6fe43f050620', 0),
(1117, 394, '90d629e75bcacc04b5fa1946438305590951cf8bd87b423a37b249c1bb3ded5e', 'db1806e4-b0e8-472c-8a1f-823b48a4e924', 0),
(1118, 395, '8ba800c72eda5762db3e72824d31a750b99024b73beb2c5e3cd62f37312d56e5', '5f3e55f1-a570-4f1f-aa36-a1515b4dd064', 0),
(1119, 396, '6ab09f19c39cf30689abefef303d024b6de37a80ca7b9dee91e9a4a6c51f936d', '93320939-b2d4-405c-9038-d4362dd49c9d', 0),
(1120, 397, '248f6418c15d9a8f97955d63f249dd56afe2ce1e46d6968327ec09b8ba807306', '7319d744-4b3d-41b4-b627-8ef502ec6e10', 0),
(1121, 398, 'fb016274e036a65088a37d641282a75d2e70c4f01b13b6be1f626ba19dddd317', '347df24f-76fb-4a3c-bfa0-11003eeb8f37', 0),
(1123, 399, 'push_disabled_by_user', 'b3a77027-ada4-4432-888e-5da8433f2334', 0),
(1124, 400, '25ad8e074a4b76758bb33afed4f479dc24b1458e70874ceec8034823ce882c9a', '4fae1eaf-0a74-4bb1-8fad-8eafcdff7523', 0),
(1125, 401, '56c5bd232adcc0f13ce9f8a33026a7261a61109566eccd154cb314ed76621dc9', 'b6ce3222-b188-410e-86df-c7839f62a27a', 0),
(1126, 402, 'ee7887d56dfaca8f22901031ee763f509d9d5a2cb40f665f188ec81b9751945d', '33f102ac-828d-42c8-8bcd-8ca8d6153f2a', 0),
(1127, 403, '714973748f7e8ddf33470c8d2ef7f58a8381042a0ca2a369bb3c388eb20a9c9d', 'feee8b49-6c9a-4ae8-b0d9-44a53eeb0c0d', 0),
(1128, 404, '31ed2c896b67c31286c920d51e2d6a473adba4708417e5fea53f48409fd09648', '15a88ed1-dc92-4a10-a96d-f81317823f6d', 0),
(1129, 405, '08b3402919ee87509b9bce3f45cad78ce53f989ea1fcc077053fb8bbb6ebdb5d', 'bbb36f26-f35f-4e6c-94a3-09d5e61f68d1', 0),
(1130, 406, 'a0f6300ad2db1ad08c094bb4533db0ea93b5b6175c67f75248915c0a0c453f29', 'c85b2464-ff3e-4879-a018-e7f1cddda406', 0),
(1131, 407, 'ce9bc377ddfbc067019ea7d72d9e5142e3112a179bb0f3068728202085cb1092', 'd2325a7d-80f4-4e2b-a54f-ccf288be79c6', 0),
(1132, 408, '71536bdfb9b5f13b69815adddd9484bfea9167010375d1f24498d9509b06d51d', '583f9cd2-ae8d-46be-a587-a3800f939888', 0),
(1133, 409, '56196503354e9d521d56dbbf5642002fb738b4a549cb839a48a2876a39ca6ab0', '306c56f4-ba6b-458d-8f1a-ebb0628587ca', 0),
(1134, 410, 'cc53ea4bbbcc55a37b7187a836c926b9ba0c83af4a46eec753bd3fad1bac84b7', '204891c8-2620-4436-82f0-b539c114e162', 0),
(1135, 411, 'e0cf170fd9797c79caf014ca30aa1b2145a4bd5b6ee178f5bc6c9591dfce1c0e', '1d1709a7-87c4-4502-9863-b78652fb447d', 0),
(1136, 412, 'ff1bd1a4463fcac2e326a3ea0658d9cb475436ac01fb56d015efe2824ce0e5f6', '224f16b0-cd49-4b8a-8a5b-6fa1d66b98c6', 0),
(1137, 413, '1e7fa13fccf0dd6875e917a059f3f633462650329bea178aeab726002712ce3b', 'c45f334f-9d42-41f3-ae07-6d03d1b218c2', 0),
(1138, 414, '4d115a38fe7ed7b8a9b0f7cb254bb6e998406e1891618f24ed344cc07c45a1df', 'b4ae050f-cb5a-4f66-bbb7-fd4ec635b9be', 0),
(1139, 415, '0dd751a7b4f21059bf25deacc0b983a704da3959a26d0af58ccdc1bc750387a8', 'b19357b2-cc79-4584-a6ee-ffd6124185b6', 0),
(1140, 416, 'f6ae646a0e673f2ea4f0bba3fd1067dc03182fdc7f814914301570b42cfacf8e', 'f2a84eb1-2c70-401a-8477-c1ed0c5d4ea6', 0),
(1141, 417, '26ee4f807bef068bfe2b9af0cad6d9508dfe018093225dc1c497a263a02a75ca', 'ca71c83f-c8a4-4c49-9404-1ef6b5910825', 0),
(1142, 418, 'f97b8a1626d1f6c1b92cfb4518f23e3f0d78ddc1395124a75fcd6fd223df90e3', 'a44050ef-83a6-4cf9-a9f7-01af92c8d1e1', 0),
(1143, 419, 'd89260db8f98162d185559c3c56d94d15271a1926d29e12325728f81e132e0ac', 'cf4c3969-5d03-4c4b-a917-f3352f1bed70', 0);

-- --------------------------------------------------------

--
-- Struttura della tabella `push_messages`
--

CREATE TABLE IF NOT EXISTS `push_messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(128) NOT NULL,
  `text` varchar(256) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Dump dei dati per la tabella `push_messages`
--

INSERT INTO `push_messages` (`id`, `key`, `text`) VALUES
(2, 'NOTIFICATION_MESSAGE_FOLLOWERS_EVENT_CREATED', 'Someone you follow just created an event!'),
(3, 'NOTIFICATION_MESSAGE_PARTECIPANTS_EVENT_UPDATE', 'An event you joined has been updated'),
(4, 'NOTIFICATION_MESSAGE_EVENT_INVITATION', 'You have been invited to a new event!'),
(5, 'NOTIFICATION_MESSAGE_FRIENDSHIP_REQUEST', 'New friend request received!'),
(6, 'NOTIFICATION_MESSAGE_FRIENDSHIP_ACCEPTED', 'Your friend request has been accepted!'),
(7, 'NOTIFICATION_MESSAGE_BOOKING_REQUEST', 'Someone wants to partecipate to an event'),
(8, 'NOTIFICATION_MESSAGE_BOOKING_ACCEPT', 'Your partecipation has been approved!'),
(9, 'NOTIFICATION_MESSAGE_CHATMESSAGE_RECEIVED', ' sent you a new message');

-- --------------------------------------------------------

--
-- Struttura della tabella `requestlogs`
--

CREATE TABLE IF NOT EXISTS `requestlogs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(245) DEFAULT NULL COMMENT 'url della rest',
  `json` text NOT NULL,
  `creation_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Log delle richieste inviate al server dai clients' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struttura della tabella `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `iduser` int(11) NOT NULL AUTO_INCREMENT COMMENT '		',
  `email` varchar(256) DEFAULT NULL,
  `idgoogle` varchar(256) NOT NULL DEFAULT '-1',
  `idfacebook` varchar(256) NOT NULL DEFAULT '-1',
  `name` varchar(45) NOT NULL COMMENT '	',
  `surname` varchar(45) NOT NULL,
  `img` varchar(128) DEFAULT NULL,
  `birthdate` date NOT NULL DEFAULT '2000-01-01',
  `sex` varchar(45) NOT NULL,
  `job` int(3) DEFAULT NULL,
  `city` varchar(45) NOT NULL,
  `interests` varchar(255) DEFAULT NULL,
  `creationdate` datetime NOT NULL,
  `updatetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `password` varchar(45) DEFAULT NULL,
  `activated` int(1) NOT NULL DEFAULT '1' COMMENT '0: l''utente è stato bannato, 1:altrimenti',
  PRIMARY KEY (`iduser`),
  UNIQUE KEY `iduser` (`iduser`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=420 ;

--
-- Dump dei dati per la tabella `users`
--

INSERT INTO `users` (`iduser`, `email`, `idgoogle`, `idfacebook`, `name`, `surname`, `img`, `birthdate`, `sex`, `job`, `city`, `interests`, `creationdate`, `updatetime`, `password`, `activated`) VALUES
(1, 'gaetano.lenoci.rimuovere@gmail.com', '-1', '-1', 'Gaetano', 'Lenoci', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '1984-07-15', '0', 0, 'cerignola', 'Profumi e Matematica ', '2015-06-18 17:13:55', '2015-06-22 13:22:43', '714f32395b2159dbdf1410af5db5fb76', 1),
(4, 'fffg@gvcf.it', '-1', '-1', 'prpva', 'prpva', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '1996-03-13', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-22 13:22:43', 'abb1801fb07b4cc74861c871b90bf5a8', 1),
(5, 'gaggs@jjee.it', '-1', '-1', 'hshhs', 'hsbwb', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '1998-03-13', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-22 13:22:43', 'e2fb12a48119eed4d309f67c81d49d28', 1),
(6, 'giirgio@giorgio.com', '-1', '-1', 'Giorgio', 'Vanni', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '2014-03-13', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-22 13:22:43', 'e777a29bee9227c8a6a86e0bad61fc40', 1),
(7, 'giirgio2@giorgio.com', '-1', '-1', 'giorgio', 'giorgy', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '2009-03-13', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-22 13:22:43', 'e777a29bee9227c8a6a86e0bad61fc40', 1),
(8, 'amuro@gundam.com', '-1', '-1', 'Amuro', 'Ray', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '1979-05-11', '0', 0, 'Neo Osaka', 'Pilota di gundam', '2015-06-18 17:13:55', '2015-06-22 13:22:43', 'e777a29bee9227c8a6a86e0bad61fc40', 1),
(9, 'fstudioleaves.comff', '-1', '-1', 'John', 'McJohn', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '1978-11-01', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-22 13:22:43', '714f32395b2159dbdf1410af5db5fb76', 1),
(10, 'test.mic@gmail.com', '-1', '-1', 'Michele', 'Addante', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '1983-02-17', '0', 1, '-1', '-1', '2015-06-18 17:13:55', '2015-07-17 14:29:54', '58a223fda14e7745d1600af6a09ca21f', 1),
(12, 'cerialimobili@gmail.com', '104844521560288913150', '-1', 'CLAUDIO', 'CERIALI', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150729103223_9309.png', '1976-10-17', '0', 13, 'Conegliano', 'fotografia \n', '2015-06-18 17:13:55', '2015-07-29 08:32:23', '88fe99c0dcdbaca68a2d8798bec7fbd5', 1),
(13, 'christian.ruggiero.86@gmail.com', '115215884722690589116', '-1', 'Christian', 'Ruggiero', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '2009-11-09', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-22 13:22:43', 'a6160f504775ad2505783ab57ba5cff7', 1),
(14, 'fffrfrffr@hotmail.com', '-1', '-1', 'Massimiliano', 'De Santis', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '1978-07-25', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-22 13:22:43', '58d169814cf26ad7e7f5562abacc9ce1', 1),
(15, 'gagaga@ccc.it', '-1', '-1', 'bel', 'tipo', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '1997-04-17', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-22 13:22:43', '714f32395b2159dbdf1410af5db5fb76', 1),
(16, 'ggg@fgg.it', '-1', '-1', 'troppo', 'bello', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '2005-04-14', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-22 13:22:43', '77d8045444af604430b97cae66a78e52', 1),
(17, 'hshhs@bsbsv.it', '-1', '-1', 'taldei', 'tali', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '1998-04-18', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-22 13:22:43', '5cd92b35412f165d5d50a083510c9e1f', 1),
(18, 'ff@bbb.it', '-1', '-1', 'taldeitali', 'iola', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '1991-04-18', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-22 13:22:43', '8ab37411705301b47681d52e8679c582', 1),
(19, 'gigi@gigi.com', '-1', '-1', 'Kaneda', 'Matsui', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '2016-04-18', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-22 13:22:43', 'e777a29bee9227c8a6a86e0bad61fc40', 1),
(20, 'provaprova@prova.it', '-1', '-1', 'prpva', 'certo', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '1998-04-16', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-22 13:22:43', '714f32395b2159dbdf1410af5db5fb76', 1),
(21, 'ggsgs@jsjs.it', '-1', '-1', 'provattt', 'provauuu', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '1995-04-19', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-22 13:22:43', 'ee31c0bd4886fc06c59a02bbada9282c', 1),
(22, 'fff@bbhg.it', '-1', '-1', 'fff', 'gggg', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '1998-04-19', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-22 13:22:43', '2576d56ec477bc2385db19bab3d11faa', 1),
(23, 'gg@ff.it', '-1', '-1', 'tty', 'hhhhh', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '2000-04-19', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-22 13:22:43', '5ac1cf2433401eb6410f1a17af7d142c', 1),
(24, 'fggt@jjh.it', '-1', '-1', 'ghh', 'ghhh', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '1997-04-19', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-22 13:22:43', '62c3d39252cc21f44723739ce0188217', 1),
(25, 'gghh@cvfd.it', '-1', '-1', 'gguuuyt', 'gbnjj', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '1999-04-19', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-22 13:22:43', '0f351bedfcacb5c4d8947ead35bea310', 1),
(26, 'provatutto@tutto.it', '-1', '-1', 'Test', 'Emulatore', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '2002-04-20', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-22 13:22:43', '714f32395b2159dbdf1410af5db5fb76', 1),
(27, 'hideo@kojima.it', '-1', '-1', 'hideo', 'kojima', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '1994-04-24', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-22 13:22:43', '714f32395b2159dbdf1410af5db5fb76', 1),
(28, 'hdhshe@hehwhw.it', '-1', '-1', 'tutto', 'divertimento', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '2003-04-24', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-22 13:22:43', 'f98dea41dd7c06263108a083e841f656', 1),
(29, 'wua@hsd.com', '-1', '-1', 'pink', 'pall', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '2009-04-25', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-22 13:22:43', 'e777a29bee9227c8a6a86e0bad61fc40', 1),
(30, 'zbg@hdhd.com', '-1', '-1', 'shg', 'hsbz', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '2007-04-25', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-22 13:22:43', 'c29440757c9f767dcf6d8ad6cb817e1b', 1),
(31, 'ciao@ciao.net', '-1', '-1', 'jojo', 'kawaguchi', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '2006-11-02', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-22 13:22:43', 'e777a29bee9227c8a6a86e0bad61fc40', 1),
(32, 'ngg@gbvg.it', '-1', '-1', 'arte', 'anticha', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '1995-04-25', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-22 13:22:43', '43941d8fef993dbb7563147588be5b61', 1),
(33, 'turchi@turchi.it', '-1', '-1', 'mamma', 'Li Turchi', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '1992-04-26', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-22 13:22:43', '714f32395b2159dbdf1410af5db5fb76', 1),
(34, 'gshs@vbht.it', '-1', '-1', 'test', 'forte', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '2003-04-26', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-22 13:22:43', '714f32395b2159dbdf1410af5db5fb76', 1),
(35, 'turchi@turchiii.it', '-1', '-1', 'mamma', 'Li Turchi', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '1992-04-26', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-22 13:22:43', '714f32395b2159dbdf1410af5db5fb76', 1),
(36, 'jsjsh@bdhd.it', '-1', '-1', 'prova', 'indimenticabile', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '1996-04-26', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-22 13:22:43', '714f32395b2159dbdf1410af5db5fb76', 1),
(37, 'vbghh@cxf.it', '-1', '-1', 'yyyyy', 'hhjj', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '1994-04-26', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-22 13:22:43', '59abb0e142ab6d8e3842b815795cd1a8', 1),
(38, 'pietro@studioleaves.com', '-1', '-1', 'Marco', 'Menegoni', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '1991-04-27', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-22 13:22:43', 'e777a29bee9227c8a6a86e0bad61fc40', 1),
(40, 'email@gianfranco.com', '-1', '-1', 'michael ', 'shumacher', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '1986-04-27', '1', -1, '', '', '2015-06-18 17:13:55', '2015-06-22 13:22:43', 'e777a29bee9227c8a6a86e0bad61fc40', 1),
(41, 'seree@fgffh.com', '-1', '-1', 'giovanni', 'desgarbi', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '1997-04-24', '0', 0, '-1', 'puzza con gli spinaci', '2015-06-18 17:13:55', '2015-06-22 13:22:43', 'e777a29bee9227c8a6a86e0bad61fc40', 1),
(42, 'new@user.com', '-1', '-1', 'new', 'user', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '2007-04-27', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-22 13:22:43', 'e777a29bee9227c8a6a86e0bad61fc40', 1),
(43, 'vagagag@usyge.it', '-1', '-1', 'final', 'build', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '1994-04-27', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-22 13:22:43', '65428dd231a37136cf094a98ac33259d', 1),
(44, 'hshsb@hsb.it', '-1', '-1', 'test ', 'tutto', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '2000-04-27', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-22 13:22:43', '714f32395b2159dbdf1410af5db5fb76', 1),
(45, 'bnnh@vggfd.it', '-1', '-1', 'trollo', 'lollo', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '1998-04-27', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-22 13:22:43', 'cc087e703e3096142784dd1ce9e3b407', 1),
(46, 'bnnh@vgghjjjfd.it', '-1', '-1', 'trollo', 'lollo', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '1998-04-27', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-22 13:22:43', 'cc087e703e3096142784dd1ce9e3b407', 1),
(47, 'bnuuunh@vgghjjjfd.it', '-1', '-1', 'trollo', 'lollo', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '1998-04-27', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-22 13:22:43', 'cc087e703e3096142784dd1ce9e3b407', 1),
(48, 'bnuuuuunh@vgghjjjfd.it', '-1', '-1', 'trollouu', 'lollooo', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '1998-04-27', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-22 13:22:43', 'cc087e703e3096142784dd1ce9e3b407', 1),
(49, 'hjjj@bbbb.it', '-1', '-1', 'hhkk', 'jjhn', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '1997-04-27', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-22 13:22:43', 'b3cc3be8588368678b9b76954506edd2', 1),
(50, 'hdndhd@bdbd.it', '-1', '-1', 'jhsns', 'jdnd', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '1900-04-27', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-22 13:22:43', '65aab34c0cf3858f8e24efd38bba96ee', 1),
(51, 'hgffgh@fvvf.it', '-1', '-1', 'testtutto', 'oliu', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '1999-04-27', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-22 13:22:43', '667248c22f00e56a8acf00528a782814', 1),
(52, 'hdnsss@bdbd.it', '-1', '-1', 'jhsns', 'jdnd', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '1900-04-27', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-22 13:22:43', '65aab34c0cf3858f8e24efd38bba96ee', 1),
(53, 'hjjj@bbeb.it', '-1', '-1', 'hhkk', 'jjhn', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '1997-04-27', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-22 13:22:43', '4346d13c18c4509070bd1beaf39d703b', 1),
(54, 'cuaj@cisi.com', '-1', '-1', 'ciai', 'ciai', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '2004-04-27', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-22 13:22:43', 'e777a29bee9227c8a6a86e0bad61fc40', 1),
(55, 'gxgsg@hdhd.com', '-1', '-1', 'ciai', 'hdgdg', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '2010-04-27', '1', -1, '', '', '2015-06-18 17:13:55', '2015-06-22 13:22:43', 'faf2c9f5d419db3d77058e0c2f15dfcb', 1),
(56, 'hdhdh@jdjdh.it', '-1', '-1', 'hdhdhdhd', 'jdjdhdhd', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '2001-04-27', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-22 13:22:43', 'b35d862e1aaac15aa268a1111617aeb7', 1),
(57, 'jdjdj@bdbdb.it', '-1', '-1', 'nddh', 'ndndh', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '2002-04-27', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-22 13:22:43', '955fddfe42b01ed19f2bbdb82a856a58', 1),
(58, 'fffgf@ggg.com', '-1', '-1', 'fgggf', 'gghggg', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '2010-04-27', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-22 13:22:43', '3f917ae45d78e65c3456f0e7c6091d2f', 1),
(59, 'jdjdj@hdhd.it', '-1', '-1', 'hdhdh', 'jdjdh', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '2015-04-27', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-22 13:22:43', '7a7f29da645408d13f924d59858f68f3', 1),
(60, 'fgg@gghh.hb', '-1', '-1', 'cgbvh', 'chhbbv', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '2011-04-27', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-22 13:22:43', '6b95b979c971bf56bf38891073b1e3f9', 1),
(61, 'hshsh@bsb.it', '-1', '-1', 'haha', 'hhss', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '2001-04-27', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-22 13:22:43', '863f3ba05289c4a047b4abca2b4a1c39', 1),
(62, 'hshsffh@bsb.it', '-1', '-1', 'haha', 'hhss', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '2001-04-27', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-22 13:22:43', '943ad471c25f116466f316c9a16abba0', 1),
(63, 'hsddhsffh@bsb.it', '-1', '-1', 'haha', 'hhss', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '2001-04-27', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-22 13:22:43', '943ad471c25f116466f316c9a16abba0', 1),
(64, 'hsddeehsffh@bsb.it', '-1', '-1', 'haha', 'hhss', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '2001-04-27', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-22 13:22:43', '943ad471c25f116466f316c9a16abba0', 1),
(65, 'vgv@hbhh.com', '-1', '-1', 'ghhhhh', 'ghvgg', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '2011-04-27', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-22 13:22:43', '0654abbed134a648f77546441ea7f0ee', 1),
(66, 'hshs@bsbv.it', '-1', '-1', 'hshs', 'hshs', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '1995-04-27', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-22 13:22:43', '5d45e205e28dbeae917d8465dce5b686', 1),
(67, 'jgxdd@ccc.it', '-1', '-1', 'ugdgd', 'kggxgfmx', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '1999-04-27', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-22 13:22:43', 'c7d775c2124075f49ff92a522067f0db', 1),
(68, 'ggff@vvcd.it', '-1', '-1', 'hhh', 'hnnj', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '2002-04-27', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-22 13:22:43', '4f6ca1bcddc7b5dfd72d9739ea38dc78', 1),
(71, 'addanter.mic@gmail.com', '-1', '-1', 'Michele', 'Addante', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '2010-04-27', '0', -1, '', '', '2015-06-18 17:13:55', '2015-07-17 14:29:32', 'e7cf7a25378d9701f13f60f91601db40', 1),
(72, 'hhfhh@fvbgf.it', '-1', '-1', 'hhfyy', 'jjjbgt', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '2005-04-27', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-22 13:22:43', '762105189e77bcf1b35a615f36c89caa', 1),
(73, 'vhjvgf@gcdfgg.it', '-1', '-1', 'bghhh', 'hhhhb', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '2002-04-27', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-22 13:22:43', 'e47c27fb159c3b9fdc02f36540c7b79c', 1),
(74, 'miv@dfre.com', '-1', '-1', 'filippo', 'nanni', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '2007-04-27', '1', -1, '', '', '2015-06-18 17:13:55', '2015-06-22 13:22:43', 'e777a29bee9227c8a6a86e0bad61fc40', 1),
(75, 'test.miccccc@gmail.com', '-1', '-1', 'michele g', 'addante', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150327202053_426.png', '2011-04-27', '0', -1, '-1', '-1', '2015-06-18 17:13:55', '2015-06-18 15:13:55', 'e777a29bee9227c8a6a86e0bad61fc40', 1),
(76, 'super@mobile.it', '-1', '-1', 'super', 'mobile', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150330113729_7526.png', '1996-04-30', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-18 15:13:55', '714f32395b2159dbdf1410af5db5fb76', 1),
(77, 'superwww@mobile.it', '-1', '-1', 'super', 'mobile', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150330125653_4075.png', '1993-04-30', '0', 0, 'cerignola', 'Tutto', '2015-06-18 17:13:55', '2015-06-18 15:13:55', '714f32395b2159dbdf1410af5db5fb76', 1),
(78, 'natalino@btrrrui.com', '-1', '-1', 'Natalino', 'Palasso', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150330130714_3178.png', '1991-04-27', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-18 15:13:55', 'e777a29bee9227c8a6a86e0bad61fc40', 1),
(79, 'hshs@hsvs.it', '-1', '-1', 'tutto', 'niente', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150330154143_7042.png', '1998-04-30', '0', -1, '-1', '-1', '2015-06-18 17:13:55', '2015-06-18 15:13:55', '421324d779b353a3655ef4556def0a6c', 1),
(80, 'ggg@ccf.it', '-1', '-1', 'mamma', 'mia', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150331102616_1414.png', '2005-04-30', '0', -1, '-1', '-1', '2015-06-18 17:13:55', '2015-06-18 15:13:55', '0f16fbf9f9b3219c4cec746da7eabaa8', 1),
(81, 'hshshs.bheje@jsj.it', '-1', '-1', 'gaetano', 'nuovo', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150331104845_9525.png', '2004-04-30', '0', -1, '-1', '-1', '2015-06-18 17:13:55', '2015-06-18 15:13:55', '714f32395b2159dbdf1410af5db5fb76', 1),
(82, 'primo@ios.com', '-1', '-1', 'primo', 'ios', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '1983-02-17', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-22 13:22:43', 'e777a29bee9227c8a6a86e0bad61fc40', 1),
(83, 'gaetano@gaetano.it', '-1', '-1', 'gaetani', 'lenoci', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150331110710_4495.png', '2000-12-01', '0', -1, '-1', '-1', '2015-06-18 17:13:55', '2015-06-18 15:13:55', '714f32395b2159dbdf1410af5db5fb76', 1),
(84, 'second@ios.com', '-1', '-1', 'second', 'iOS', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '1983-02-17', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-22 13:22:43', 'e777a29bee9227c8a6a86e0bad61fc40', 1),
(85, 'gggf@cvfg.it', '-1', '-1', 'troppo', 'stroppia', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150331120315_5447.png', '2004-05-01', '0', -1, '-1', '-1', '2015-06-18 17:13:55', '2015-06-18 15:13:55', '92fd880f50ba2b8b4fe662086f51f113', 1),
(86, 'cvg@gg.com', '-1', '-1', 'ciao', 'bello', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150331112305_5309.jpeg', '1983-02-17', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-18 15:13:55', '3b9f4aad911ead41a2c1f3f33a777fe1', 1),
(87, 'test@t.it', '-1', '-1', 'test', 'test', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150331121054_53.png', '1992-05-01', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-18 15:13:55', '714f32395b2159dbdf1410af5db5fb76', 1),
(88, 'hdhsh@bshsy.it', '-1', '-1', 'push', 'test', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '2002-05-01', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-22 13:22:43', '714f32395b2159dbdf1410af5db5fb76', 1),
(89, 'fff@bvfy.it', '-1', '-1', 'ghh', 'hhh', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '2000-05-01', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-22 13:22:43', 'da1d01cb4d0ddc78553bda8b4009d7fb', 1),
(90, 'hdhd@bnns.it', '-1', '-1', 'tetetegg', 'uehevve', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '1996-05-01', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-22 13:22:43', '610356556e0432e4995cab5d0f108844', 1),
(91, 'maxdesantis@hotmail.com', '-1', '10153275495151654', 'Massimiliano', 'De Santis', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150624184037_6212.png', '1978-07-25', '0', 0, 'Milano', 'Fotografia, Cinema, Informatica.\n\nFounder @wherabout ;)', '2015-06-18 17:13:55', '2015-08-16 09:03:48', 'bddce2f861315ba319c3a7ffa40fd5ad', 1),
(92, 'gaetano2.lenoci@gmail.com', '-1', '-1', 'Gaetano', 'Lenoci', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150408173941_2216.png', '1984-07-15', '0', -1, '-1', '-1', '2015-06-18 17:13:55', '2015-06-18 15:13:55', '714f32395b2159dbdf1410af5db5fb76', 1),
(93, 'gaetanttto.lenoci@gmail.com', '-1', '-1', 'Gaetano', 'Lenoci', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150331134018_5425.png', '2000-05-01', '0', -1, '-1', '-1', '2015-06-18 17:13:55', '2015-06-18 15:13:55', '43941d8fef993dbb7563147588be5b61', 1),
(94, 'hdbds@bsbs.it', '-1', '-1', 'tabe', 'test', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150331134219_5008.png', '1998-02-01', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-18 15:13:55', '47921f8a7f20c8e02f3b996351211e63', 1),
(95, 'ffff@fghg.com', '-1', '-1', 'cvcxv', 'fcgfv', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '2015-03-31', '1', -1, '', '', '2015-06-18 17:13:55', '2015-06-22 13:22:43', 'c4030cb9cb03f4cd50aeebdd664e929b', 1),
(96, 'ggggy@ghgg.com', '-1', '-1', 'ucycyv', 'gggg', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '2015-03-31', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-22 13:22:43', 'dc38c83f28dd6bc6f1b3f928852a4232', 1),
(97, 'studioleavfesstaff.rimuovere2@gmail.com', '-1', '-1', 'Studio', 'Leaves', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150331161251_4681.png', '2001-05-01', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-18 15:13:55', '714f32395b2159dbdf1410af5db5fb76', 1),
(98, 'fff@studioleaves.com', '-1', '-1', 'John', 'McJohn', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150331161839_3954.png', '1999-05-01', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-18 15:13:55', '5fbffad41b64d819ca7f3a7c075532e9', 1),
(99, 'steve@jobs.com', '-1', '-1', 'Steve', 'Jobs', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150331164546_6114.jpeg', '1983-02-17', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-18 15:13:55', '481dad18bd5a975c01a89d133939a03e', 1),
(100, 'papa@castoro.com', '-1', '-1', 'Pappá', 'Castoro', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150401084556_1882.png', '2012-05-01', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-18 15:13:55', 'cf44b88b211b2d6e57f28adad73a2466', 1),
(101, 'polipo@style.com', '-1', '-1', 'polipo', 'style', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150401100151_3131.jpeg', '1980-04-01', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-18 15:13:55', 'e777a29bee9227c8a6a86e0bad61fc40', 1),
(102, 'pieo@stuioleaves.com', '-1', '-1', 'Marco', 'Menegoni', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '1991-04-27', '0', -1, '-1', '-1', '2015-06-18 17:13:55', '2015-06-22 13:22:43', 'e777a29bee9227c8a6a86e0bad61fc40', 1),
(103, 'hshsh@vsbs.it', '-1', '-1', 'hshs', 'jshs', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '2000-05-01', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-22 13:22:43', '863f3ba05289c4a047b4abca2b4a1c39', 1),
(104, 'gff@cdt.it', '-1', '-1', 'ggggg', 'hhh', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '2005-05-01', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-22 13:22:43', '2ad903949d649f63d03f630957858954', 1),
(105, 'test@stress.it', '-1', '-1', 'troppo', 'malinconia', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '2002-05-01', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-22 13:22:43', '80952cb50d61a3ec9517f860dc68fcb9', 1),
(106, 'ghhghh@cccc.it', '-1', '-1', 'ghhhg', 'hhj', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '2003-05-01', '0', -1, '-1', '-1', '2015-06-18 17:13:55', '2015-06-22 13:22:43', '8ab37411705301b47681d52e8679c582', 1),
(107, 'arnold@ditano.com', '-1', '-1', 'Arnold', 'DiaTano', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150406100036_1527.png', '2012-05-04', '0', -1, '-1', '-1', '2015-06-18 17:13:55', '2015-06-18 15:13:55', 'e777a29bee9227c8a6a86e0bad61fc40', 1),
(108, 'testpass@pass.it', '-1', '-1', 'testpass', 'password', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150407123350_1501.png', '2003-05-07', '0', -1, '-1', '-1', '2015-06-18 17:13:55', '2015-06-18 15:13:55', '714f32395b2159dbdf1410af5db5fb76', 1),
(109, 'ffds@xxx.it', '-1', '-1', 'ghhhjjj', 'rrrr', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '2005-05-07', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-22 13:22:43', 'a2899a469b8fc759813f07da793b89fe', 1),
(110, 'ryo@hazuki.it', '-1', '-1', 'ryo', 'hazuki', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '2000-05-08', '0', -1, '-1', '-1', '2015-06-18 17:13:55', '2015-06-22 13:22:43', 'e6e537d46540581aafa8d505896dee78', 1),
(111, 'ggyyy@ccxd.it', '-1', '-1', 'fffff', 'huuu', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '2002-05-08', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-22 13:22:43', '18596acbbb361e534ae5751d1b2c1064', 1),
(118, 'gsgsg@gggs.kt', '-1', '-1', 'eeee', 'yyy', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150409101950_202.png', '2005-05-09', '0', NULL, '', NULL, '2015-06-18 17:13:55', '2015-06-18 15:13:55', '1583b0a85aa92f2eb50cddb0f1376081', 1),
(119, 'fffg@ccdd.iy', '-1', '-1', 'vhhfd', 'hhytt', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150409121512_4081.png', '2003-05-09', '0', NULL, '', NULL, '2015-06-18 17:13:55', '2015-06-18 15:13:55', '7939a917a2b43f96eb358eb082c36aa2', 1),
(120, 'fff@xxx.it', '-1', '-1', 'yyyy', 'hhhh', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150409121635_3788.png', '2006-05-09', '0', NULL, '', NULL, '2015-06-18 17:13:55', '2015-06-18 15:13:55', 'b6860d5c02fe98a0d964f6c58404cf0c', 1),
(121, 'ffffdd@jhgg.it', '-1', '-1', 'yyyy', 'hhhh', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150409123054_1278.png', '2002-05-09', '0', 0, '-1', '-1', '2015-06-18 17:13:55', '2015-06-18 15:13:55', '933474b16f314ec4f1a5916b165898e9', 1),
(122, 'hshs@ccc.it', '-1', '-1', 'hwhehhsbsb', 'gsgsg', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150409130016_2506.png', '2004-05-09', '0', NULL, '', NULL, '2015-06-18 17:13:55', '2015-06-18 15:13:55', 'ce08af2d72a5cb6d63c90e0038572792', 1),
(123, 'ggg@xccc.it', '-1', '-1', 'hhhh', 'jjjj', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150409130933_888.png', '2007-05-09', '0', 0, 'cerignola', 'tutto', '2015-06-18 17:13:55', '2015-06-18 15:13:55', 'a337087687764175b4ed60083dfa78db', 1),
(124, 'ggg@dff.iy', '-1', '-1', 'ttttt', 'jjuuu', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150409142958_7239.png', '2004-01-09', '0', NULL, '', NULL, '2015-06-18 17:13:55', '2015-06-18 15:13:55', '825c89e9fe0b75c185bfa7c52cc0b6a3', 1),
(125, 'fffx@xxx.it', '-1', '-1', 'gggg', 'hhhhh', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150409143709_6028.png', '2008-05-09', '0', -1, '-1', '-1', '2015-06-18 17:13:55', '2015-06-18 15:13:55', '3635ad6eabcca07aa2a03576dfa11ad6', 1),
(126, 'tulio@tulio.it', '-1', '-1', 'tulio', 'iulio', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '2004-05-09', '0', NULL, '', NULL, '2015-06-18 17:13:55', '2015-06-22 13:22:43', '70873e8580c9900986939611618d7b1e', 1),
(127, 'ioio@ioio.it', '-1', '-1', 'ioio', 'tutu', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '2006-05-09', '0', NULL, '', NULL, '2015-06-18 17:13:55', '2015-06-22 13:22:43', '70873e8580c9900986939611618d7b1e', 1),
(128, 'asd@asd.it', '-1', '-1', 'tttt', 'hvffd', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150409150911_6641.png', '2005-05-09', '0', -1, '-1', '-1', '2015-06-18 17:13:55', '2015-06-18 15:13:55', '70873e8580c9900986939611618d7b1e', 1),
(129, 'ddssd@hvxx.it', '-1', '-1', 'gggg', 'jjjjj', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150409161346_8555.png', '2003-05-09', '0', NULL, '', NULL, '2015-06-18 17:13:55', '2015-06-18 15:13:55', '42632bc52cda3b1e5b71e095a3831b73', 1),
(130, 'ffff@hbb.it', '-1', '-1', 'yyyyuhh', 'hhhhg', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '2001-05-09', '0', -1, 'New York', 'New York', '2015-06-18 17:13:55', '2015-06-22 13:22:43', '70873e8580c9900986939611618d7b1e', 1),
(131, 'ioioio@io.it', '-1', '-1', 'ioioio', 'ioioio', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150803111759_7620.png', '2003-05-09', '0', 23, 'cerignola', 'Tutto basket', '2015-06-18 17:13:55', '2015-08-03 09:19:15', '696d29e0940a4957748fe3fc9efd22a3', 1),
(133, 'pedro.bit@hotmal.it', '-1', '-1', 'Pietro', 'La Grotta', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '1981-10-20', '0', NULL, '', NULL, '2015-06-18 17:13:55', '2015-07-02 16:50:11', '269e9bb6c60f4eb79c3c14ca240c4b6c', 1),
(134, 'sakura@sakura.it', '-1', '-1', 'Sakura', 'Gugiamoto', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '2013-05-10', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-22 13:22:43', 'e777a29bee9227c8a6a86e0bad61fc40', 1),
(135, 'gaetano.lenoci@live.com', '-1', '-1', 'gaetano', 'lenoci', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150415175137_7716.png', '1984-07-15', '0', 1, 'Cerignola', 'Basket, videogiochi. ', '2015-06-18 17:13:55', '2015-06-18 15:13:55', '696d29e0940a4957748fe3fc9efd22a3', 1),
(136, 'pippe@baude.com', '-1', '-1', 'Pippe', 'Baude', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '2015-05-10', '0', 0, '', '', '2015-06-18 17:13:55', '2015-06-22 13:22:43', 'b2c3bb7394dcc0307e6fb6119e416e44', 1),
(137, 'birra@birra.com', '-1', '-1', 'Signor', 'Birra', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150413124758_4405.jpeg', '1991-04-13', '0', -1, '-1', '-1', '2015-06-18 17:13:55', '2015-06-18 15:13:55', '882a6de7ac3540550a43c9990760bee9', 1),
(138, 'hdhdhdh@hhh.it', '-1', '-1', 'hdhdhd', 'hdhddj', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '2001-05-13', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-22 13:22:43', '29d03de8ec52197c90c40d2225bad0fe', 1),
(139, 'testo@zeta.com', '-1', '-1', 'test', 'user', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '2015-04-13', '0', -1, '-1', '-1', '2015-06-18 17:13:55', '2015-06-22 13:22:43', 'a7e608576c9c2c17f8c0599a10f29c40', 1),
(140, 'pchan@gmail.com', '-1', '-1', 'Ryoma', 'Saotome', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150507112530_8236.png', '2002-04-13', '0', 0, 'Neo Tokyo è', 'È un classico', '2015-06-18 17:13:55', '2015-06-18 15:13:55', '696d29e0940a4957748fe3fc9efd22a3', 1),
(141, 'email@email.com', '-1', '-1', 'nuovo', 'user', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '2015-04-14', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-22 13:22:43', '696d29e0940a4957748fe3fc9efd22a3', 1),
(142, 'pedro.bit@hotmail.it', '-1', '10205352862373709', 'Pietro', 'La Grotta', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150731145536_6189.png', '1981-10-20', '0', -1, '', '', '2015-06-18 17:13:55', '2015-07-31 12:55:36', '696d29e0940a4957748fe3fc9efd22a3', 1),
(143, 'ios4@mail.com', '-1', '-1', 'ios4s', 'device', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '2015-04-14', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-22 13:22:43', '696d29e0940a4957748fe3fc9efd22a3', 1),
(144, 'push@push.com', '-1', '-1', 'ios', 'push', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '2015-04-15', '1', -1, '', '', '2015-06-18 17:13:55', '2015-06-22 13:22:43', '696d29e0940a4957748fe3fc9efd22a3', 1),
(145, 'ffcc@bhh.it', '-1', '-1', 'tttfc', 'hhggf', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150415121931_6000.png', '2006-05-11', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-18 15:13:55', '957d9510998e58f261214f2ee2e1b63f', 1),
(146, 'fgg@uhh.com', '-1', '-1', 'push', 'negato', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '1983-06-16', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-22 13:22:43', '696d29e0940a4957748fe3fc9efd22a3', 1),
(147, 'hhhg@hhh.it', '-1', '-1', 'cup', 'knight', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150417154155_2757.png', '2000-05-17', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-18 15:13:55', '696d29e0940a4957748fe3fc9efd22a3', 1),
(148, 'ffrfrfr@studioleaves.com', '-1', '-1', 'John', 'McJohn', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150420151009_1486.jpeg', '2002-07-20', '0', -1, '-1', '-1', '2015-06-18 17:13:55', '2015-06-18 15:13:55', '696d29e0940a4957748fe3fc9efd22a3', 1),
(149, 'addante.mic.rimuover@gmail.com', '-1', '-1', 'Michele', 'Addante', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '1983-02-17', '0', -1, '', '', '2015-06-18 17:13:55', '2015-07-17 14:29:54', '696d29e0940a4957748fe3fc9efd22a3', 1),
(150, 'studioleavesstaff.rimuovere1@gmail.com', '-1', '-1', 'Studio', 'Leaves', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150420183702_2390.png', '2015-04-20', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-18 15:13:55', '696d29e0940a4957748fe3fc9efd22a3', 1),
(151, 'ciao@ggh.com', '-1', '-1', 'ennesimo', 'ios', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150421202833_2618.jpeg', '2015-04-21', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-18 15:13:55', '696d29e0940a4957748fe3fc9efd22a3', 1),
(152, 'ciau@ciauuuu.com', '-1', '-1', 'ciai', 'ciau', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '2011-02-17', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-22 13:22:43', '696d29e0940a4957748fe3fc9efd22a3', 1),
(153, 'ffggg@yhhhh.gg', '-1', '-1', 'ggggf', 'vvvvgv', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '2015-04-22', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-22 13:22:43', 'f7df5472ea6554c2852ea27f163e5b9c', 1),
(154, 'fggf@ggggy.gg', '-1', '-1', 'cfggff gg', 'cfggff', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '2015-04-22', '0', 1, 'vgvv', 'Hvgvvgvbg', '2015-06-18 17:13:55', '2015-06-22 13:22:43', 'd242ebd0470518950821f276743018c2', 1),
(155, 'zio@tizio.com', '-1', '-1', 'tizio', 'qualunque', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '1998-12-08', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-22 13:22:43', '696d29e0940a4957748fe3fc9efd22a3', 1),
(156, 'michael@jordan.it', '-1', '-1', 'michael', 'jordan', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150422093118_1126.png', '2005-05-22', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-18 15:13:55', '696d29e0940a4957748fe3fc9efd22a3', 1),
(157, 'gaetano.lenoci.rimuovere2@gmail.com', '-1', '-1', 'Gaetano', 'Lenoci', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150422111730_1868.png', '1984-07-15', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-18 15:13:55', '696d29e0940a4957748fe3fc9efd22a3', 1),
(158, 'johnmcjohn@studioleaves.com', '-1', '411121025723079', 'John', 'McJohn', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150422113704_4322.jpeg', '2015-04-22', '0', -1, '-1', '-1', '2015-06-18 17:13:55', '2015-06-18 15:13:55', '696d29e0940a4957748fe3fc9efd22a3', 1),
(159, 'gaetano.lenoci.rimuovere3@gmail.com', '-1', '-1', 'Gaetano', 'Lenoci', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150422115615_1693.png', '1984-07-15', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-18 15:13:55', '696d29e0940a4957748fe3fc9efd22a3', 1),
(160, 'gaetano.lenoci.rimuovere5@gmail.com', '-1', '-1', 'Gaetano', 'Lenoci', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150422123640_821.png', '1984-07-15', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-18 15:13:55', '696d29e0940a4957748fe3fc9efd22a3', 1),
(161, 'addante.mic.rimuovere@gmail.com', '-1', '-1', 'michele', 'addante', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150422123905_8343.jpeg', '1990-01-12', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-18 15:13:55', '696d29e0940a4957748fe3fc9efd22a3', 1),
(162, 'studioleavesstaffrimuovere2@gmail.com', '-1', '-1', 'Studio', 'Leaves', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150422123920_4639.png', '2008-05-22', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-18 15:13:55', '696d29e0940a4957748fe3fc9efd22a3', 1),
(163, 'gaetano.lenoci@gmail.com', '-1', '10153186369902520', 'Gaetano', 'Lenoci', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150720170246_5735.png', '1984-07-15', '0', 0, 'cerignola', 'Tutto', '2015-06-18 17:13:55', '2015-07-20 15:02:46', '696d29e0940a4957748fe3fc9efd22a3', 1),
(164, 'ciai@jdjdj.com', '-1', '-1', 'Michele', 'Android', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '2011-02-17', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-22 13:22:43', '696d29e0940a4957748fe3fc9efd22a3', 1),
(165, 'marti6976@yahoo.it', '-1', '10205754741775470', 'Martina', 'Malloggi', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150818213458_7983.png', '2015-05-28', '1', 5, 'Conegliano', 'viaggi ', '2015-06-18 17:13:55', '2015-08-18 19:34:58', 'ddd939661f423a403dec48a9ba78ce4a', 1),
(166, 'cerialiarredamenti@gmail.com', '-1', '-1', 'claudio', 'ceriali', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '2015-05-28', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-22 13:22:43', '88fe99c0dcdbaca68a2d8798bec7fbd5', 1),
(167, 'addante.mic3.rimuovere@gmail.com', '-1', '-1', 'Michele', 'Addante', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150506153557_4049.png', '1983-02-17', '0', NULL, '-1', '-1', '2015-06-18 17:13:55', '2015-06-30 16:12:32', '1063525fa992dc8a6aa6ed549515dbe8', 1),
(168, 'mods1125@yahoo.it', '-1', '10206187003221437', 'Mxg', 'Etto', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150506125032_6130.png', '1978-05-29', '0', -1, 'Conegliano', '', '2015-06-18 17:13:55', '2015-06-18 15:13:55', '2a2a8eafa0e8aa409894c4ab0441a643', 1),
(169, 'addanteffff.mic@gmail.com', '-1', '-1', 'Michele', 'Addante', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '1983-02-17', '0', 1, '-1', '-1', '2015-06-18 17:13:55', '2015-07-02 16:48:07', '969fc4473f2c0647dee8819c35fed602', 1),
(170, 'test@dff.com', '-1', '-1', 'test', 'test', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '2015-05-07', '0', -1, '-1', 'none', '2015-06-18 17:13:55', '2015-06-22 13:22:43', 'c9255768ad6d440da6d33daededc7950', 1),
(171, 'xxxx@dddd.com', '-1', '-1', 'cfgg', 'cfgg', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '2015-05-07', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-22 13:22:43', 'c96172a670bd226a2c4d3644df00b7ff', 1),
(172, 'dddd@ffff.com', '-1', '-1', 'cffffddd', 'ccccvc', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '2015-05-07', '0', 1, '-1', '-1', '2015-06-18 17:13:55', '2015-06-22 13:22:43', 'fb03e14bf35826e59f5ca437c67eae66', 1),
(173, 'hshah@hdhdhdhdhs.com', '-1', '-1', 'hdhdhdhdhs', 'bxh svdvfv', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '2015-05-07', '0', -1, '-1', '-1', '2015-06-18 17:13:55', '2015-06-22 13:22:43', 'b37bfbd9b5c0c400a8c9b4c4559b6b92', 1),
(174, 'fui@uhh.com', '-1', '-1', 'ggggf', 'ggggf', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '2015-05-07', '0', -1, '-1', 'none', '2015-06-18 17:13:55', '2015-06-22 13:22:43', 'a4948da28a64805b022edc91ede5bd80', 1),
(175, 'shhs@dissi.com', '-1', '-1', 'hshshd', 'bxhd ha', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '2015-05-07', '0', -1, '-1', 'none', '2015-06-18 17:13:55', '2015-06-22 13:22:43', '49183357f97d26b73dc69a699048ea58', 1),
(176, 'vshsh@hshhd.com', '-1', '-1', 'hdhdbdbd', 'gdbdbvdd', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150515163421_5832.png', '2015-06-07', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-18 15:13:55', '0156548c370c3fa6f9cbdf7a24eb7b30', 1),
(177, 'addante.old@gmail.com', '-1', '-1', 'Michele', 'Addante', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150507143526_5377.png', '1983-02-17', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-18 15:13:55', '696d29e0940a4957748fe3fc9efd22a3', 1),
(178, 'email@hghghg.com', '-1', '-1', 'utente', 'raw', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150507145432_2202.png', '2005-09-19', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-18 15:13:55', '696d29e0940a4957748fe3fc9efd22a3', 1),
(179, 'addagvhvvnte.mvvvic@gmail.com', '112316104162541714961', '-1', 'Michele', 'Addante', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150515171641_3331.png', '2015-06-15', '0', 16, 'Bari', 'Nulla Di Interesante', '2015-06-18 17:13:55', '2015-07-16 13:11:41', 'cdc0b75be770884e34637e7bdbe0f94f', 1),
(180, 'wherabout_vqvvuup_fake@tfbnw.net', '-1', '1389312264732700', 'wherabout', 'fake', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150519104225_9906.png', '1980-08-08', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-18 15:13:55', '696d29e0940a4957748fe3fc9efd22a3', 1),
(181, 'addante.oldold@gmail.com', '-1', '-1', 'Michele', 'Addante', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150522154407_1967.png', '1983-02-17', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-18 15:13:55', '696d29e0940a4957748fe3fc9efd22a3', 1),
(182, 'addante.mic@gmail.com', '-1', '1068468346502924', 'Michele', 'Addante', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150714103726_2451.png', '2015-06-22', '0', 4, 'Bari', '-1', '2015-06-18 17:13:55', '2015-08-03 13:46:11', '696d29e0940a4957748fe3fc9efd22a3', 1),
(183, 'user-a_exurgms_wherabout@tfbnw.net', '-1', '102834746721187', 'user-a', 'wherabout', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150526114051_1050.png', '2015-06-22', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-18 15:13:55', '696d29e0940a4957748fe3fc9efd22a3', 1),
(184, 'user-b_bptrrsl_wherabout@tfbnw.net', '-1', '105465669789328', 'user-b', 'wherabout', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150526115123_5578.png', '2015-06-22', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-18 15:13:55', '696d29e0940a4957748fe3fc9efd22a3', 1),
(185, 'testmichele@mic.com', '-1', '-1', 'testmichele', 'michele', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '2012-06-26', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-22 13:22:43', '696d29e0940a4957748fe3fc9efd22a3', 1),
(186, 'gaetano.lenoci2@gmail.com', '108647488673199071070', '-1', 'Gaetano', 'Lenoci', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150605153833_6202.png', '1987-07-05', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-18 15:13:55', '696d29e0940a4957748fe3fc9efd22a3', 1),
(187, 'jessicascapol@gmail.com', '105523849301773227416', '-1', 'Jessica', 'Enin', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150609193747_3302.png', '1992-10-30', '1', -1, '', '', '2015-06-18 17:13:55', '2015-06-18 15:13:55', 'b8a6976e40eb9389ac8429fa125accf6', 1),
(188, 'xguagli@inwind.it', '-1', '-1', 'Marcello', 'Guaglio', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150614120548_7079.png', '1981-03-08', '0', -1, '', '', '2015-06-18 17:13:55', '2015-06-18 15:13:55', 'dede022a9abf208544a644fe452cd46a', 1),
(189, 'test_ios@test.com', '-1', '-1', 'Test', 'user', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '2003-06-22', '0', -1, '', '', '2015-06-22 10:59:54', '2015-06-22 13:22:43', '696d29e0940a4957748fe3fc9efd22a3', 1),
(190, 'lino.pedulla@gmail.com', '-1', '-1', 'Lino', 'Tester', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '1967-07-11', '0', -1, '', '', '2015-06-22 13:01:24', '2015-06-22 13:22:43', 'fd3a52cbe1192cf830ad40e98d0d1946', 1),
(191, 'mauro.gambarotto@gmail.com', '103139887565517492770', '-1', 'Mauro', 'Gambarotto', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150622232121_7204.png', '1974-09-08', '0', -1, '', '', '2015-06-22 23:21:21', '2015-06-22 21:21:21', 'c80c122a03a50b6df498f5283eb9e3b4', 1),
(192, 'pippo@gianfranco.com', '-1', '-1', 'Pippo', 'Gianfranco', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '2015-07-25', '0', -1, '', '', '2015-06-25 10:19:49', '2015-06-25 08:19:49', '15fd54d4304c03055ceff04de78e4e77', 1),
(193, 'pippppo@gianfranco.com', '-1', '-1', 'Pippo', 'Gianfranco', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150625102542_2602.png', '2015-07-25', '1', -1, '', 'musica sport giardinaggio', '2015-06-25 10:25:42', '2015-06-29 16:30:57', '15fd54d4304c03055ceff04de78e4e77', 1),
(194, 'pix4notes@gmail.com', '-1', '10206978169923303', 'Duca', 'Dei Vizsla', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150728120456_5062.png', '1981-12-03', '0', 10, 'San Pietro di Feletto', 'Viaggiare, fotografare, cani, mostre d&#039;arte e fotografia, design, cucina, cinema, musica e festival, squash, biliardo.', '2015-06-26 17:41:37', '2015-07-31 11:37:38', '947e4634da9b0c0593bf882ff3be421e', 1),
(195, 'sil_damian@hotmail.com', '-1', '-1', 'Silvia', 'Damian', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150816133920_84.png', '1979-02-26', '1', 9, 'Milano', '-1', '2015-06-26 18:36:52', '2015-08-16 11:39:20', '512fff7186f8ec0622f896de971f7aed', 1),
(196, 'bucadellefate@yahoo.it', '-1', '10206130192147479', 'Gaetano', 'Staiano', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150626214449_801.png', '1979-05-03', '0', -1, '', '', '2015-06-26 21:44:49', '2015-06-26 19:44:49', 'cc596720ef1502e2783f0bc410448850', 1),
(197, 'testios@Michele.com', '-1', '-1', 'Testios', 'Michele', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150715151101_5376.png', '2006-06-29', '0', 8, 'Bari', 'Gfg ci\nHhh', '2015-06-29 10:01:09', '2015-07-15 13:11:01', '696d29e0940a4957748fe3fc9efd22a3', 1),
(198, 'testandroid@michele.com', '-1', '-1', 'Testandroid', 'Michele', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '2011-07-29', '0', -1, '', '', '2015-06-29 10:16:22', '2015-06-29 08:16:22', '696d29e0940a4957748fe3fc9efd22a3', 1),
(199, 'testipad@michele.com', '-1', '-1', 'Testipad', 'Michele', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150721155521_4079.png', '2004-06-29', '0', -1, '-1', '-1', '2015-06-29 11:34:58', '2015-07-21 13:55:21', '696d29e0940a4957748fe3fc9efd22a3', 1),
(200, 'testsimulator@michele.com', '-1', '-1', 'Simulator', 'Test', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150807170446_3726.png', '1996-06-30', '0', 2, '-1', '-1', '2015-06-30 10:11:50', '2015-08-07 15:04:46', '696d29e0940a4957748fe3fc9efd22a3', 1),
(202, 'testiphone@testiphone.it', '-1', '-1', 'Test', 'Test Tutto', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150701113556_1437.png', '2015-04-01', '0', -1, '', '', '2015-07-01 11:35:56', '2015-07-01 09:35:56', '696d29e0940a4957748fe3fc9efd22a3', 1);
INSERT INTO `users` (`iduser`, `email`, `idgoogle`, `idfacebook`, `name`, `surname`, `img`, `birthdate`, `sex`, `job`, `city`, `interests`, `creationdate`, `updatetime`, `password`, `activated`) VALUES
(203, 'testandroidpedro@michele.com', '-1', '-1', 'Testandroid', 'Pedro', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '2015-08-01', '0', -1, '', '', '2015-07-01 12:06:45', '2015-07-01 10:06:45', '696d29e0940a4957748fe3fc9efd22a3', 1),
(204, 'new_awshkbh_android@tfbnw.net', '-1', '100764983605723', 'New', 'Android', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150709093216_5805.png', '2015-08-09', '0', -1, '', '', '2015-07-09 09:32:16', '2015-07-09 07:32:16', '696d29e0940a4957748fe3fc9efd22a3', 1),
(205, 'matemare81@hotmail.com', '-1', '10206078511915259', 'Matteo', 'Maretto', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150728202831_1903.png', '1981-12-13', '0', 5, '-1', 'Photography\nWeb design\nStyle\nHoroscope \nDonuts\nSea\nMountain \nNature \nScience  ', '2015-07-10 13:47:17', '2015-07-28 18:28:31', 'a51899e5184f10fa88246d478720cc16', 1),
(206, 'pelser@tinoza.com', '-1', '-1', 'Onno', 'Pelser', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '1971-06-25', '0', -1, '', '', '2015-07-13 20:09:01', '2015-07-13 18:09:01', '9fcc498c85c2266e4f45b09b8cbdc269', 1),
(207, 'pierre@delabierre.it', '-1', '-1', 'Pierre', 'De La Bierre', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150715160648_3927.png', '1990-07-16', '0', -1, '-1', '-1', '2015-07-15 10:53:04', '2015-07-15 14:06:48', 'aa7c24a0d853c894ccf13405ccb987d6', 1),
(208, 'pierre@delevin.it', '-1', '-1', 'Pierre', 'De Le Vin', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '1994-08-15', '1', -1, '', '', '2015-07-15 10:55:05', '2015-07-15 08:55:05', 'c4aa2ce1ae825425d2c94ad021e0faa0', 1),
(209, 'carloceriali@tin.it', '-1', '-1', 'Carlo ', 'Ceriali ', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '1979-02-02', '0', -1, '', '', '2015-07-15 12:56:36', '2015-07-15 10:56:36', '540158dc8b6866a46fc7f3ec7bf79d49', 1),
(210, 'gptestfelicityl@gmail.com', '-1', '451527105008611', 'Fp', 'Lau', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150715191140_7028.png', '1992-03-13', '1', -1, '', '', '2015-07-15 19:11:40', '2015-07-15 17:11:40', '1eb3084fd30a560f3422a0f7549aed69', 1),
(211, 'test@hshah.com', '-1', '-1', 'User', 'Ban', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '2015-07-16', '0', NULL, '-1', '-1', '2015-07-16 13:23:15', '2015-07-16 12:10:18', '696d29e0940a4957748fe3fc9efd22a3', 0),
(212, 'testipod@Michele.com', '-1', '-1', 'Testios', 'Ios', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150721110658_7361.png', '2015-07-17', '0', -1, '-1', '-1', '2015-07-17 09:52:38', '2015-07-21 09:06:58', '696d29e0940a4957748fe3fc9efd22a3', 1),
(213, 'wherabout2@gmail.com', '-1', '-1', 'Alice', 'Zanchi', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150717101540_9655.png', '1987-06-21', '1', 10, 'Milano', 'Team Wherabout :)', '2015-07-17 10:11:26', '2015-07-17 08:15:40', '400d1efe1ea5b6beb2eb73efec347ee6', 1),
(214, 'wherabout3@gmail.com', '-1', '-1', 'Martaelisa', 'Varnier', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150717103229_6324.png', '1989-07-03', '1', 10, 'Milano', 'Team Wherabout  :-)', '2015-07-17 10:32:29', '2015-08-04 08:45:28', 'a2943c92ab68aa42f56296eba38f64f7', 1),
(215, 'ggg@hu.com', '-1', '-1', 'Ggg', 'Vgg', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '2015-08-17', '0', -1, '', '', '2015-07-17 18:04:28', '2015-07-17 16:04:28', 'f2f6fc2f6fd22462afb829a58dc59662', 1),
(216, 'cdcddcd@dedde.com', '-1', '-1', 'Cddcdccd', 'Cdcddcdc', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '2015-07-20', '0', -1, '', '', '2015-07-20 16:18:53', '2015-07-20 14:18:53', '6d53b6e08888a85462c3181583b5e32b', 1),
(217, 'matte2682@yahoo.it', '-1', '10205799256292331', 'Malloggi', 'Matteo', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150822190727_3352.png', '1982-06-02', '0', 2, 'Pieve di soligo ', '-1', '2015-07-23 16:38:02', '2015-08-22 17:07:27', '3b9211c48fcc95aa5aaec3b74ef0a462', 1),
(218, 'stefano.grassia@gmail.com', '114125281166952668389', '-1', 'Stefano', 'Grassia', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150725192308_7257.png', '1977-07-16', '0', -1, '', '', '2015-07-25 19:23:08', '2015-07-25 17:23:08', 'e3775890c40fcdbb08f2293a4b6e4420', 1),
(219, 'hdhdj@jsjs.com', '-1', '-1', 'Test', 'Memory', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '2015-08-27', '0', -1, '', '', '2015-07-27 13:40:54', '2015-07-27 11:40:54', '696d29e0940a4957748fe3fc9efd22a3', 1),
(220, 'hxgxgx@michdle.com', '-1', '-1', 'Mwmory', 'Test', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150727150045_8295.png', '2015-08-27', '0', -1, '', '', '2015-07-27 15:00:45', '2015-07-27 13:00:45', '696d29e0940a4957748fe3fc9efd22a3', 1),
(221, 'bdhsh@hdhdh.com', '-1', '-1', 'Memory', 'Test', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150727150420_1556.png', '2015-08-27', '0', -1, '', '', '2015-07-27 15:04:20', '2015-07-27 13:04:20', '3b9fdf97cd2f3d5b26063f8a41eb3929', 1),
(222, 'vggff@gvhh.com', '-1', '-1', 'Cggvvgg', 'Bhvvvv', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '2015-08-27', '0', -1, '', '', '2015-07-27 15:38:38', '2015-07-27 13:38:38', '1e6d7f7f846e9ed1f08013ce8810ccb7', 1),
(223, 'test_guhrwrw_memory@tfbnw.net', '-1', '123549127985709', 'Test', 'Memory', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150727160029_9940.png', '2015-08-27', '0', -1, '', '', '2015-07-27 16:00:29', '2015-07-27 14:00:29', '696d29e0940a4957748fe3fc9efd22a3', 1),
(224, 'pierino@super.com', '-1', '-1', 'Pierino', 'Super', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150727160510_2378.png', '2000-06-26', '0', -1, '', '', '2015-07-27 16:03:12', '2015-07-27 14:05:10', '3c282a5782a509f4630caed213971854', 1),
(225, 'testchat@test.it', '-1', '-1', 'Test', 'Chwt', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150727162432_5867.png', '2007-08-27', '0', -1, '', '', '2015-07-27 16:24:32', '2015-07-27 14:24:32', '696d29e0940a4957748fe3fc9efd22a3', 1),
(226, 'test@chat.it', '-1', '-1', 'Test', 'Chat', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150727162615_8838.png', '2002-08-27', '0', -1, '', '', '2015-07-27 16:26:15', '2015-07-27 14:26:15', '696d29e0940a4957748fe3fc9efd22a3', 1),
(227, 'hdhdh@jeje.com', '-1', '-1', 'Test', 'Michele2', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150727202835_8625.png', '2015-08-27', '0', -1, '', '', '2015-07-27 20:28:35', '2015-07-27 18:28:35', '696d29e0940a4957748fe3fc9efd22a3', 1),
(228, 'Private@jdjd.com', '-1', '10207129945117418', 'Francesco', 'Addante', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150727204936_6983.png', '2015-08-27', '0', -1, '', '', '2015-07-27 20:49:36', '2015-07-27 18:49:36', '696d29e0940a4957748fe3fc9efd22a3', 1),
(229, 'luisamotta76@gmail.com', '-1', '-1', 'Luisa', 'Motta', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '1976-07-14', '1', -1, '', '', '2015-07-27 21:18:40', '2015-07-27 19:18:40', 'd5a0c3a6ae8f9c32b440dae5bd3b3b67', 1),
(230, 'alice_arisu@hotmail.it', '-1', '10207280590803170', 'Alice', 'Zanchi', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150728095029_9165.png', '1987-06-21', '1', -1, '', '', '2015-07-28 09:50:29', '2015-07-28 07:50:29', '29523ea35ffa8ac4e105a4eb945744a2', 1),
(231, 'noraz9@libero.it', '-1', '10153038844658575', 'Eleonora', 'Canonico', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150728113053_3872.png', '1982-07-09', '1', -1, '', '', '2015-07-28 11:30:53', '2015-07-28 09:30:53', 'b366e504381da3a8d74f1ff03f86348a', 1),
(232, 'gaetano@lenoci.it', '-1', '-1', 'Gaetano', 'Lenoci', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150728114352_5682.png', '1995-08-28', '0', -1, '', '', '2015-07-28 11:43:52', '2015-07-28 09:43:52', '696d29e0940a4957748fe3fc9efd22a3', 1),
(233, 'maya3577@yahoo.it', '-1', '10153072535763100', 'Ale', 'Xina', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150728115722_7804.png', '1977-05-03', '1', 27, 'conegliano', 'vespa, cucina, musica, concerti, sport, ', '2015-07-28 11:57:22', '2015-07-29 13:49:17', '16adfd11735ae3ee6a3f2cb2c3c7f7a5', 1),
(234, 'elisa.baby@libero.it', '-1', '10207568918087967', 'Spinato', 'Elisa', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150728120758_7.png', '1987-04-05', '1', -1, 'conegliano', 'musica\n', '2015-07-28 12:07:58', '2015-08-08 19:35:26', 'ea56f1a5804162b91ad23c2bdc538cfa', 1),
(235, 'roberto.paronetto@gmail.com', '-1', '1662118780685604', 'Roberto', 'Paronetto', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150728121744_455.png', '1969-08-05', '0', -1, '', '', '2015-07-28 12:17:44', '2015-07-28 10:17:44', '58961f7e19b7377d6239d2cc758f19f3', 1),
(236, 'stefaniabrugnoni76@gmail.com', '108266017143153037310', '-1', 'Stefania', 'Brugnoni', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150728125249_1982.png', '1976-10-15', '1', -1, '', '', '2015-07-28 12:52:49', '2015-07-28 10:52:49', 'c85570718d464db0421b347c8f23e972', 1),
(237, 'mahannahcaguioa@yahoo.com', '-1', '147790922220329', 'Ma Hannah', 'Caguioa', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150728130343_1010.png', '1998-01-12', '1', -1, '', '', '2015-07-28 13:03:43', '2015-07-28 11:03:43', '4d33d20643eee7d1d8ff6d209a9fd6bf', 1),
(238, 'simonemenegon@gmail.com', '-1', '10155839964695557', 'Simone', 'Menegon', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150728131210_371.png', '1971-11-30', '0', -1, '', '', '2015-07-28 13:12:10', '2015-07-28 11:12:10', '6287954d62ac8b728161e324a330d610', 1),
(239, 'alessandro.bergnach@email.it', '-1', '-1', 'Alessandro', 'Bergnach', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150728135628_6865.png', '1981-09-13', '0', -1, '', '', '2015-07-28 13:27:12', '2015-07-28 11:56:28', '02067ed1e8a6685706c6ca631237dc8c', 1),
(240, 'giuliapezzullo@virgilio.it', '-1', '-1', 'Giulia', 'Pezzullo', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150728134215_9325.png', '1991-07-07', '1', -1, 'Conegliano', 'art, photography, music', '2015-07-28 13:42:15', '2015-07-28 11:44:50', 'c5cbf502dbdd4ad51923f532ebae2d6e', 1),
(241, 'andrearui2005@gmail.com', '111357501056916690463', '-1', 'Andrea', 'Rui', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150728134225_9491.png', '1976-08-28', '0', -1, '', '', '2015-07-28 13:42:25', '2015-07-28 11:42:25', '92ccbdbeaecf729c202d5552b47a7985', 1),
(242, 'nicoleta.capatina96@gmail.com', '-1', '592434067562188', 'Nicoleta', 'Capatina', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150728144356_5805.png', '1996-12-20', '1', -1, '', '', '2015-07-28 14:43:56', '2015-07-28 12:43:56', '12c8caf43608da924cb128c5441adb07', 1),
(243, 'marta.varnier@gmail.com', '-1', '-1', 'Martaelisa', 'Varnier', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150728145326_1779.png', '1989-07-03', '1', 10, 'Milano', 'Martials arts, comics, graphic novels, videogames, cosplay and dragons. And kitties!', '2015-07-28 14:53:26', '2015-08-12 07:40:39', 'db64ed64dff17a2ee3911584b7c621cc', 1),
(244, 'notifiche1@michele.com', '-1', '-1', 'Test', 'Notifiche', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '2015-08-28', '0', -1, '', '', '2015-07-28 14:57:55', '2015-07-28 12:57:55', '696d29e0940a4957748fe3fc9efd22a3', 1),
(245, 'notifiche2@michele.com', '-1', '-1', 'Test', 'Notifiche2', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '2015-08-28', '0', -1, '', '', '2015-07-28 14:59:04', '2015-07-28 12:59:04', '696d29e0940a4957748fe3fc9efd22a3', 1),
(246, 'santinpaolo14011979@gmail.com', '104156645642089523079', '-1', 'Paolo', 'Santin', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150728162528_6318.png', '1979-01-14', '0', -1, '', '', '2015-07-28 16:25:28', '2015-07-28 14:25:28', '3dea9a51a498e6ff2186cf7d3fd5dedf', 1),
(247, 'push@test.it', '-1', '-1', 'Testpush', 'Push', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '2001-08-28', '0', -1, '', '', '2015-07-28 16:39:25', '2015-07-28 14:39:26', '696d29e0940a4957748fe3fc9efd22a3', 1),
(248, 'michybressan@yahoo.it', '-1', '443531689159484', 'Michela', 'Bressan', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150728171053_1665.png', '1997-01-28', '1', -1, '', '', '2015-07-28 17:10:53', '2015-07-28 15:10:53', '0331d5bec8ab9b468bb0f71c0bf91e98', 1),
(249, 'michele.pampe@gmail.com', '-1', '10206396071533598', 'Michele', 'Modanese', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150728174145_6333.png', '1985-05-24', '0', -1, '', '', '2015-07-28 17:41:45', '2015-07-28 15:41:45', '619344d99c5762cc1e35650f17b04a50', 1),
(250, 'margherita.dalcin@gmail.com', '-1', '-1', 'Margherita', 'Dal Con', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150728183421_723.png', '1977-12-06', '1', -1, '', '', '2015-07-28 18:34:21', '2015-07-28 16:34:21', '45df2211280517922a035dff14b8785f', 1),
(251, 'linea_gotica@libero.it', '-1', '-1', 'Linea', 'Gotica', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '1973-10-04', '0', -1, '', '', '2015-07-28 18:41:39', '2015-07-28 16:41:39', '1c23805ec9f68aba5801d0eb97a86432', 1),
(252, 'roberta.pezzullo@gmail.com', '-1', '10206125284884034', 'Roberta', 'Pezzullo', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150728184719_5937.png', '1978-05-06', '1', -1, '', '', '2015-07-28 18:47:19', '2015-07-28 16:47:19', '5929c79abccfa3383e544661360c6267', 1),
(253, 'francysalv@gmail.com', '-1', '429516893900856', 'La', 'Francy', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150728194105_4207.png', '1984-05-23', '1', -1, '', '', '2015-07-28 19:41:05', '2015-07-28 17:41:05', 'd32c8076c622706940f40f2cef832096', 1),
(254, 'premtarisha@gmail.com', '-1', '10200782500362722', 'Paola', 'Pandolfo-prem Tarisha', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150728195656_2031.png', '1964-01-27', '1', -1, '', '', '2015-07-28 19:56:56', '2015-07-28 17:56:56', '85006dc7888ecc843cae83ba85d8452a', 1),
(255, 'francesca@michele.com', '-1', '10206822919240865', 'Francesca', 'Ferrara', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150728205721_1021.png', '2015-08-28', '1', -1, '', '', '2015-07-28 20:57:21', '2015-07-28 18:57:21', '696d29e0940a4957748fe3fc9efd22a3', 1),
(256, 'brightjulyerasmus@hotmail.com', '-1', '10153526892238748', 'Giulia', 'Mezzini', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150728210411_4277.png', '1982-04-01', '1', -1, '', '', '2015-07-28 21:04:11', '2015-07-28 19:04:11', '16277158113a97f77053945c10422084', 1),
(257, 'balda.mark@yahoo.it', '-1', '1052703211409405', 'Marco', 'Baldari', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150728213622_1303.png', '1995-03-15', '0', -1, '', '', '2015-07-28 21:36:22', '2015-07-28 19:36:22', 'd87dff371d0f32671a99c6cc3470759e', 1),
(258, 'robertozanchetta@gmail.com', '-1', '-1', 'Roberto', 'Zanchetta', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '1983-09-25', '0', 10, 'conegliano', '', '2015-07-28 22:13:32', '2015-07-28 20:17:04', '44164a7353183a9401da802cce5b0101', 1),
(259, 'da.stella@libero.it', '-1', '10154009401379338', 'Daniela', 'Spinazzè', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150729095507_4531.png', '1979-08-15', '1', -1, '', '', '2015-07-29 09:55:08', '2015-07-29 07:55:08', '3b2717567432c8a168839ed3e9a61b8d', 1),
(260, 'test_gwjbavw_memoryb@tfbnw.net', '-1', '116032712074601', 'Test', 'Memoryb', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150729112124_7401.png', '2015-12-29', '0', -1, '', '', '2015-07-29 11:21:24', '2015-07-29 09:21:24', '696d29e0940a4957748fe3fc9efd22a3', 1),
(261, 'test_vitttpw_memoryc@tfbnw.net', '-1', '125713537769112', 'Test', 'Memoryc', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150729124621_1618.png', '2015-08-29', '0', -1, '', '', '2015-07-29 12:46:21', '2015-07-29 10:46:21', '696d29e0940a4957748fe3fc9efd22a3', 1),
(262, 'marcovs2000@yahoo.it', '-1', '10206459869888966', 'Marco', 'Vecellio', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150729191143_3375.png', '1968-09-12', '0', -1, '', '', '2015-07-29 19:11:43', '2015-07-29 17:11:43', 'd48f94e1487d6ba54be0a6cd08185ffb', 1),
(263, 'test@not.it', '-1', '-1', 'Test', 'Notifiche', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150730122058_633.png', '2000-08-30', '0', -1, '', '', '2015-07-30 12:20:58', '2015-07-30 10:20:58', '696d29e0940a4957748fe3fc9efd22a3', 1),
(264, 'ciccio_zfkzprm_pasticcio@tfbnw.net', '-1', '1433180293679039', 'Ciccio', 'Pasticcio', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150730124337_3551.png', '1994-08-30', '0', -1, '', '', '2015-07-30 12:43:37', '2015-07-30 10:43:37', '696d29e0940a4957748fe3fc9efd22a3', 1),
(265, 'ciccio_rgcqqew_pasticcio@tfbnw.net', '-1', '1455484864776754', 'Ciccio', 'Pasticcio', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150730152537_8687.png', '1999-08-30', '0', -1, '', '', '2015-07-30 15:25:37', '2015-07-30 13:25:37', '696d29e0940a4957748fe3fc9efd22a3', 1),
(266, 'testtest@test.it', '-1', '-1', 'Test', 'Test', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '2005-08-30', '0', -1, '', '', '2015-07-30 16:35:21', '2015-07-30 14:35:21', '696d29e0940a4957748fe3fc9efd22a3', 1),
(267, 'tedandrea@libero.it', '-1', '1092426607453308', 'Andrea', 'Padovan', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150731120630_1637.png', '1964-10-30', '0', -1, '', '', '2015-07-31 12:06:30', '2015-07-31 10:06:30', '31d72fd8d01b1308e49c86b16546af7b', 1),
(268, 'andreamazz77@gmail.com', '114230049613145703782', '-1', 'Elwood', 'Butnotblues', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150731150017_517.png', '2015-08-31', '0', -1, '', '', '2015-07-31 15:00:17', '2015-07-31 13:00:17', '4e2ba7b307a56e8bfaf39ee4a9799547', 1),
(269, 'flavio.zinga@libero.it', '-1', '977054305678805', 'Flavio', 'Zeta', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150731180037_8388.png', '1982-11-10', '0', -1, '', '', '2015-07-31 18:00:37', '2015-07-31 16:00:37', '9c40a421c30d5e94c4e7dd81243f8021', 1),
(270, 'ciccio@pasticcio.it', '-1', '-1', 'Ciccio', 'Pasticcio', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150731184820_3414.png', '2003-08-31', '0', -1, '', '', '2015-07-31 18:48:20', '2015-07-31 16:48:20', '696d29e0940a4957748fe3fc9efd22a3', 1),
(271, 'rosa_rossignoli@yahoo.it', '-1', '1597609103839459', 'Rosy', 'Ross', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150801032816_4340.png', '1973-12-15', '1', -1, '', '', '2015-08-01 03:28:17', '2015-08-01 01:28:17', 'a587d4496515360cef4485905462fc54', 1),
(272, 'shine00720@gmail.com', '111729589962505318207', '-1', 'Alessandro', 'Del Pex', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150801222357_6129.png', '1980-05-10', '0', -1, '', '', '2015-08-01 21:52:23', '2015-08-01 20:23:57', '795971b5c9b0a4c6d0631bb84650c66e', 1),
(273, 'giuli.avon.milano@gmail.com', '-1', '-1', 'Giulia', 'B', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '1995-09-23', '1', -1, '', '', '2015-08-02 20:29:44', '2015-08-02 18:29:44', '887c24a7d17bf23c04caa1f33305b9f2', 1),
(274, 'test@avatar.com', '-1', '-1', 'Test', 'Cambio Avariar', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150803112959_1715.png', '2015-08-03', '0', 21, 'ghhhhhh', 'Ciao è ancora ciao', '2015-08-03 10:13:19', '2015-08-03 09:29:59', '696d29e0940a4957748fe3fc9efd22a3', 1),
(275, 'test@none.com', '-1', '-1', 'Test', 'None', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '2015-08-03', '0', -1, '', '', '2015-08-03 12:48:39', '2015-08-03 10:48:39', '696d29e0940a4957748fe3fc9efd22a3', 1),
(276, 'test@none2.com', '-1', '-1', 'Test', 'None2', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '2015-08-03', '0', 5, 'dddd', 'dddd', '2015-08-03 12:51:17', '2015-08-03 10:58:43', '696d29e0940a4957748fe3fc9efd22a3', 1),
(277, 'filippoghignone@gmail.com', '-1', '-1', 'Filippo', 'Ghignone', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150812135521_4467.png', '1981-04-27', '0', -1, '', '', '2015-08-05 16:19:53', '2015-08-12 11:55:21', '4ca48491f0f3770a241f730478d9cc21', 1),
(278, 'alla-luna@hotmail.it', '-1', '10207498743890381', 'Francesca', 'Ricchini', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150806204057_108.png', '1984-09-29', '1', -1, '', '', '2015-08-06 20:40:57', '2015-08-06 18:40:57', 'b2f62a1d109433bab693160f24465605', 1),
(279, 'alessandracastano@yahoo.it', '-1', '10207564818141435', 'Alessandra', 'Castano', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150806204812_4390.png', '1988-07-15', '1', -1, '', '', '2015-08-06 20:48:12', '2015-08-06 18:48:12', '26f7c37524a79863b76746d5f2304eb4', 1),
(280, 'marianovella.bugetti@gmail.com', '-1', '-1', 'Maria Novella', 'Bugetti', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '1980-07-19', '1', -1, '', '', '2015-08-07 15:26:07', '2015-08-07 13:26:07', '32af62d354b0d8b9653b1e636cd40738', 1),
(281, 'm.soonia@gmail.com', '-1', '1683406488539056', 'Sonia', 'Masoni', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150807215230_7202.png', '1968-01-21', '1', -1, '', '', '2015-08-07 21:52:30', '2015-08-07 19:52:30', 'b2a2671b0abe855dcac4742e16977fa8', 1),
(282, 'vivi1196@libero.it', '-1', '727796150659590', 'Viola', 'Gasparotti', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150809223624_315.png', '1996-04-11', '1', -1, '', '', '2015-08-09 22:36:24', '2015-08-09 20:36:24', '50bf73252e25477d837ff5061cc349a4', 1),
(283, 'ettore.griffoni@gmail.com', '113606320745706571399', '-1', 'Ettore', 'Griffoni', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150809233758_342.png', '1977-01-23', '0', -1, '', '', '2015-08-09 23:37:58', '2015-08-09 21:37:58', 'e6464d89a240e33eca0600c7bc79ddf0', 1),
(284, '31ottobre1991@gmail.com', '-1', '-1', 'Phantom', '99', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '1997-08-09', '1', -1, '', '', '2015-08-10 16:41:25', '2015-08-10 14:41:25', '17175ca30483b592fab6c2b7961a754e', 1),
(285, 'Zanchi.alice@gmail.com', '-1', '-1', 'Alice', 'Zanchi', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150812121449_6637.png', '1987-06-21', '1', 4, 'Trezzo Sull Adda', 'Viaggi, Cultura, Mercatini, Viaggi, Posticini Curiosi Dove Mangiare, Design, Ho Già Detto Viaggi?', '2015-08-12 09:42:26', '2015-08-12 10:53:12', 'f969fd217186b176e64a2ad4042b9253', 1),
(286, 'vaevictisasmadi@hotmail.com', '-1', '-1', 'Marco', 'Minelli', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '1985-01-28', '0', -1, '', '', '2015-08-12 10:15:57', '2015-08-12 08:15:57', '9dc670466f910b3a495ed15728343766', 1),
(287, 'Sanfilippo.fabri@gmail.com', '-1', '10207785392701137', 'Fabrizio', 'Sanfilippo', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150812101933_6177.png', '1989-01-28', '0', -1, '', '', '2015-08-12 10:19:33', '2015-08-12 08:19:33', '9301dfc6568f9064c025ad1e459f3cf4', 1),
(288, 'zweibeiner@gmail.com', '-1', '10207927581533884', 'Chiara', 'Galbusera', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150812104223_4715.png', '1982-11-10', '1', -1, '', '', '2015-08-12 10:42:23', '2015-08-12 08:42:23', 'd8f4c25ba94f69c49de8b2d1d94c9c58', 1),
(289, 'giulia.guazzi@gmail.com', '102216737481819527371', '-1', 'Giulia', 'Guazzi', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150812104729_5584.png', '1988-08-08', '1', -1, '', '', '2015-08-12 10:47:29', '2015-08-12 08:47:29', 'ffffaf5111a2700538104e4196d4fed5', 1),
(290, 'emanuele.canzonieri88@gmail.com', '-1', '10206697067921566', 'Emanuele', 'Canzonieri', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150812121455_5954.png', '1988-11-13', '0', -1, '', '', '2015-08-12 12:14:55', '2015-08-12 10:14:55', '7aa7406ea8bd9ff913c3e1ec8d303435', 1),
(291, 'kira-83@libero.it', '-1', '10206917578885233', 'Miria', 'Benotti', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150812132619_9876.png', '1983-09-22', '1', -1, '', '', '2015-08-12 13:26:19', '2015-08-12 11:26:19', 'd6f8895d6fac6ce604720c2b61ce19c2', 1),
(292, 'nimoi@hotmail.it', '-1', '-1', 'Riccardo', 'Caridi', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '1986-03-19', '0', -1, '', '', '2015-08-12 14:59:11', '2015-08-12 12:59:11', 'aab6dade736033c84c0236bb32c3818a', 1),
(293, 'florindoida@hotmail.it', '-1', '-1', 'Ida', 'Florindo', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '1966-09-05', '1', -1, '', '', '2015-08-12 15:04:09', '2015-08-12 13:04:09', '2fb01850deeaf8c57b9f67763626e086', 1),
(294, 'maltoita96@gmail.com', '112644733486967264755', '-1', 'Matteo', 'Salpietro', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150812152249_164.png', '1996-03-04', '0', -1, '', '', '2015-08-12 15:22:49', '2015-08-12 13:22:49', 'ff1ad44eeb6424b0d0d7fd48717f241a', 1),
(295, 'paola.petruzzelli90@gmail.com', '-1', '10206175272514406', 'Paola', 'Pi', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150812174320_8297.png', '1990-01-25', '1', -1, '', '', '2015-08-12 17:43:20', '2015-08-12 15:43:20', 'f3b35c5b79610d2025defcacabb9dd58', 1),
(296, 'sisto.sara@gmail.com', '107011089396126200781', '-1', 'Sara', 'Sisto', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150812175335_7601.png', '1985-07-24', '1', -1, '', '', '2015-08-12 17:53:35', '2015-08-12 15:53:35', '820229a5c4d62abf35121ea2a17bbf1a', 1),
(297, 'laluu@hotmail.it', '-1', '10203244119183855', 'Laura', 'Lalu Shinigami', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150812215347_8074.png', '1987-06-04', '1', -1, '', '', '2015-08-12 21:53:47', '2015-08-12 19:53:47', '2e03d0d1a39995a0854804034c86bc32', 1),
(298, 'roberto.dalzotto@gmail.com', '114105401777255619604', '-1', 'Roberto', 'Dal Zotto', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150813170312_1797.png', '1972-09-23', '0', -1, '', '', '2015-08-13 17:03:12', '2015-08-13 15:03:12', 'f91a8c593dc9ec99be0b4188001f7a25', 1),
(299, 'lios1929@hotmail.it', '-1', '-1', 'Dna', 'Netlabel', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150813174637_5960.png', '1988-05-27', '0', 16, 'Genova , Treviso', 'Music, theatre, art.\nOnline netlabel and musician&#039;s collective, based around Genova and Treviso. \nMembers: Merisi, Ego Drone, Alarm Pressure, Cenere Muto and many more.\nhttp:dnanetlabel.altervista.org', '2015-08-13 17:44:53', '2015-08-13 15:59:06', '09f4c22d847824865036a80c9777a9d9', 1),
(300, 'dgiannetti@libero.it', '-1', '-1', 'Donatella', 'Giannetti', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '1958-10-22', '1', -1, '', '', '2015-08-13 23:25:10', '2015-08-13 21:25:10', '595745a750791392c0346645fc2feaec', 1),
(301, 'zandemari@gmail.com', '115104794739798041825', '-1', 'Mariagrazia', 'Zandegiacomo', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150814210727_9223.png', '1987-12-15', '1', -1, '', '', '2015-08-14 21:07:27', '2015-08-14 19:07:27', '7b5a370188120d8191082d89bc45f42c', 1),
(302, 'gp@zandegiacomo.it', '-1', '-1', 'Gianpaolo', 'Zandegiacomo', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150815175008_6184.png', '1968-02-19', '0', 18, 'Zug', 'Curling, Music, Architectural, Drink &amp; Food, Healt &amp; Fittness, Interior Design, Life Style, Karate, Canoa, Art', '2015-08-15 17:29:58', '2015-08-15 17:11:08', '172618ccdea905e7613a378604918d5d', 1),
(303, 'rosapompanin@hotmail.com', '-1', '-1', 'Rosa', 'Pompanin ', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150815212703_2305.png', '1984-06-09', '1', 27, 'Zug ', 'Zumba fitness, Curling, good Bier, music, psychology ', '2015-08-15 21:27:03', '2015-08-15 19:30:00', '4c08a9d539dcc50c108fac2bbde170d0', 1),
(304, 'saretta-gucci@libero.it', '-1', '960956360631332', 'Sara', 'Damian', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150816090342_2815.png', '2016-08-21', '0', -1, '', '', '2015-08-16 09:03:42', '2015-08-16 07:03:42', '5299dc95451e32558736cad8e734570f', 1),
(305, 'elpapiro@alice.it', '-1', '10203456546454742', 'Eddy', 'Conti', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150816093508_8146.png', '1969-03-03', '0', -1, '', '', '2015-08-16 09:35:08', '2015-08-16 07:35:08', 'c27e3ba951cedf3068fc6eac85767716', 1),
(306, 'domeniconapoli93@gmail.com', '-1', '10205066641930676', 'Domenico', 'Napoli', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150817001555_2477.png', '1993-10-15', '0', -1, '', '', '2015-08-17 00:15:55', '2015-08-16 22:15:55', '6e409f9bea116137782b9ba87f249d9a', 1),
(307, 'stellaecila555@gmail.com', '-1', '-1', 'Alice', 'Romani', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150817102346_3512.png', '1989-01-31', '1', -1, '', '', '2015-08-17 10:23:46', '2015-08-17 08:23:46', 'a1c151bbd6b5262ea047201540cb7773', 1),
(308, 'delamothe@gmail.com', '-1', '10153184246145852', 'Hélène', 'De La Mothe', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150817160218_7764.png', '1980-03-28', '1', -1, '', '', '2015-08-17 16:02:18', '2015-08-17 14:02:18', 'ef832065001696ca1f0f4d5151055afb', 1),
(309, 'nicolettabelometti@gmail.com', '-1', '10207520510998815', 'Nicoletta', 'Belometti', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150817205652_7509.png', '1986-01-07', '1', -1, '', '', '2015-08-17 20:56:52', '2015-08-17 18:56:52', '7239e3bcdc06b7fe626e05bd0de068d9', 1),
(310, 'slash_marko@yahoo.it', '-1', '10206656391161082', 'Marco', 'Calignano', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150818100833_359.png', '1992-01-06', '0', -1, '', '', '2015-08-18 10:08:33', '2015-08-18 08:08:33', '11cb8ceaa9176e94edbddfa86fca049a', 1),
(311, 'andrea.zazzi@gmail.com', '-1', '10207770300443769', 'Andrea', 'Zazzi', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150818115144_8444.png', '1981-10-31', '0', -1, '', '', '2015-08-18 11:51:44', '2015-08-18 09:51:44', '2a77f0769d17c37883aeddd9a06aea18', 1),
(312, 'mattia97lazzari@libero.it', '-1', '1659206277692915', 'Meiko', 'Homna', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150818130237_3795.png', '1997-01-02', '0', -1, '', '', '2015-08-18 13:02:37', '2015-08-18 11:02:37', '0df1cefed467d82322659c651d9296ea', 1),
(313, 'margauxesposito@gmail.com', '-1', '10205759510365829', 'Margaux', 'Esposito', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150818143204_2562.png', '1989-08-25', '1', -1, '', '', '2015-08-18 14:32:04', '2015-08-18 12:32:04', '05f6a5412cfa1d2257684b7d0d9530aa', 1),
(314, 'lucabertazzon@yahoo.it', '-1', '10206395037266575', 'Luca', 'Bertazzon', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150818143324_7984.png', '1985-06-30', '0', 16, 'San Pietro di Feletto ', 'Cani,birra,cibo,calcio,giardinaggio\n', '2015-08-18 14:33:24', '2015-08-19 10:58:38', 'f56e8f4fcf2349dab9cf1a1a20ede7b3', 1),
(315, 'mastervlad@hotmail.it', '-1', '-1', 'Vladimir', 'Zecchini', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '1990-07-26', '0', -1, '', '', '2015-08-18 15:40:44', '2015-08-18 13:40:44', 'c5c196cfcf63eeca54a7ff1c80eccf9a', 1),
(316, 'giovanniraccuglia80@gmail.com', '-1', '10204375939509736', 'Giovanni', 'Raccuglia', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150818161458_1114.png', '1980-11-27', '0', -1, '', '', '2015-08-18 16:14:58', '2015-08-18 14:14:58', 'c16dfc2f1f836ab5791c097ec8fdb0ae', 1),
(317, 'hudhud@hotmail.fr', '-1', '10204797812331381', 'Larhrissi', 'Houda', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150818161753_1068.png', '1980-06-15', '1', -1, '', '', '2015-08-18 16:17:53', '2015-08-18 14:17:53', '0a329f1b47b235a120e32857e1542a11', 1),
(318, 'deborahluna.model@yahoo.it', '-1', '10207738727205898', 'Deborah Luna', 'Santarelli', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150818162633_736.png', '1987-06-08', '1', -1, '', '', '2015-08-18 16:26:33', '2015-08-18 14:26:33', '82ecc2fc1a0b4e9798f23350244aa547', 1),
(319, 'danzac64@gmail.com', '109333043341371987967', '-1', 'Daniele', 'Zacchetti', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150819213120_7294.png', '1964-11-09', '0', 27, 'milano', 'science, informatics, free software, linux,urban cycling', '2015-08-19 21:31:20', '2015-08-19 19:35:45', '9f5f456ec41b801198f0a1f4be02086d', 1),
(320, 'matteoandrioli2000@outlook.it', '-1', '1614359832152042', 'Matteo', 'Andrioli', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150820193749_944.png', '2000-12-12', '0', -1, 'Oria', '-1', '2015-08-20 19:37:49', '2015-08-20 17:38:16', 'f649c970528ba5c9440b9b5e2490b36a', 1),
(321, 'fufy26@hotmail.it', '-1', '10206336796652809', 'Funmilayo', 'Akhigbe', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150820203418_4568.png', '1991-11-26', '1', -1, '', '', '2015-08-20 20:34:18', '2015-08-20 18:34:18', 'ef5291094efb87e82b9bbd45a15a1846', 1),
(322, 'gin.panama92@gmail.com', '-1', '-1', 'Gin', 'Panama', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150820211758_9949.png', '1992-03-31', '1', -1, '', '', '2015-08-20 21:17:58', '2015-08-20 19:17:58', '067bf25023470ba59695ec437df71ed3', 1),
(323, 't.neri92@gmail.com', '109605007452063988017', '-1', 'Tommaso', 'Neri', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150820212637_7757.png', '1992-08-05', '0', -1, '', '', '2015-08-20 21:26:37', '2015-08-20 19:26:37', 'eef4429b5cde0e053ee9335a07737c10', 1),
(324, 'music1998@libero.it', '-1', '1511012365857136', 'Sara', 'Foresta', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150820214446_4965.png', '1996-12-18', '1', -1, '', '', '2015-08-20 21:44:46', '2015-08-20 19:44:46', '862197c76a96afa6b692f5896c0355d8', 1),
(325, 'oscar.colitti@virgilio.it', '-1', '-1', 'Oscar', 'Colitti', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '1980-02-21', '0', -1, '', '', '2015-08-21 01:16:13', '2015-08-20 23:16:13', '9ba11908e1ace37977a9713813e9b1ce', 1),
(326, 'aevangelisti@gmail.com', '-1', '10154110507303942', 'Alessandro', 'Evangelisti', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150821011815_4656.png', '1976-02-09', '0', -1, '', '', '2015-08-21 01:18:15', '2015-08-20 23:18:15', '7c6747ba0f477f064457d16b902adc46', 1),
(327, 'imalimitlessthing@gmail.com', '112406381983369008979', '-1', 'Autumŋ Gɾace', 'Laŋcasteɾ', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150821021514_8441.png', '2015-08-21', '1', -1, '', '', '2015-08-21 02:15:14', '2015-08-21 00:15:14', '68ea081caca76fa648829bc6eba730cb', 1),
(328, 'ziglioli.elisa@gmail.com', '-1', '10206270014482456', 'Elisa', 'Ziglioli', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150821022351_8467.png', '1994-12-21', '1', 13, '-1', '-1', '2015-08-21 02:23:51', '2015-08-21 00:27:14', '9ff5a921afdeba9e2b31853ce7a86b96', 1),
(329, 'alexregal@hotmail.it', '-1', '10205511361837566', 'Alessio', 'Regalbuto', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150821025007_1936.png', '1990-11-20', '0', -1, '', '', '2015-08-21 02:50:07', '2015-08-21 00:50:07', '126cf3494941cb2cce0807ecba75a1e4', 1),
(330, 'er_pupone.93@hotmail.it', '-1', '10206399209130625', 'Roberto', 'Carbone', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150821030516_9326.png', '1993-05-02', '0', -1, '', '', '2015-08-21 03:05:16', '2015-08-21 01:05:16', '42c6518d9ca09877ae7d6e143b3dd84c', 1),
(331, 'punkalone@gmail.com', '-1', '10207223977506588', 'Luca', 'Marino', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150821034912_9820.png', '1984-04-04', '0', -1, '', '', '2015-08-21 03:49:12', '2015-08-21 01:49:12', '91cd12526932cd27d2f250cd206bf24d', 1),
(332, 'christian_091@hotmail.it', '-1', '1629926477265238', 'Cristian', 'Benazzi', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150821072224_1764.png', '1991-12-29', '0', -1, '', '', '2015-08-21 07:22:24', '2015-08-21 05:22:24', 'b831b48d59e6b386fb52f2f4fc3df5ca', 1),
(333, 'desimone.padova@gmail.com', '-1', '-1', 'Antonio', 'De Simone', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '1978-04-08', '0', -1, '', '', '2015-08-21 07:56:54', '2015-08-21 05:56:54', 'd7d254e74f9a1289e4411d3239175c8b', 1),
(334, 'birbimirti@gmail.com', '-1', '-1', 'Barbara', 'De Laurentiis', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '1988-01-12', '1', -1, '', '', '2015-08-21 08:09:18', '2015-08-21 06:09:18', '7287665d3ed60a6ee3f55a3bd1e5b967', 1),
(335, 'simeonigi@libero.it', '-1', '-1', 'Stefano', 'Simeoni', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '1964-02-01', '0', -1, '', '', '2015-08-21 10:08:22', '2015-08-21 08:08:22', '76cbf0a30655059c64a48c01ce329438', 1),
(336, 'tsutano_ita@libero.it', '-1', '10207405717132029', 'Gaetano', 'Ottato', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150821103051_2157.png', '1989-03-04', '0', -1, '', '', '2015-08-21 10:30:51', '2015-08-21 08:30:51', 'a70b7a0c0969daefcfea5f41692012a6', 1),
(337, 'mkandrei16@yahoo.it', '-1', '772115356243919', 'Andrei ', 'Macovei', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150821103835_8634.png', '1997-01-18', '0', -1, '', '', '2015-08-21 10:38:35', '2015-08-21 08:38:35', 'eb321d26155c545394daa792f370c03e', 1),
(338, 'r.vizzolini@hotmail.it', '-1', '10207475814683897', 'Riccardo', 'Vizzolini', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150821123842_1465.png', '1995-05-27', '0', -1, '', '', '2015-08-21 12:38:42', '2015-08-21 10:38:42', '63d89995691a0d4fad0b87b3e6f7fa21', 1),
(339, 'sofia.r403@icloud.com', '-1', '-1', 'Sofia', 'R', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '1977-10-12', '1', -1, '', '', '2015-08-21 13:49:18', '2015-08-21 11:49:18', '84e179e5e9dfbd29fbf43b8c3d6b95aa', 1),
(340, 'bboozz@gmail.com', '-1', '-1', 'Bob', 'Z', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '1980-01-01', '0', -1, '', '', '2015-08-21 14:49:07', '2015-08-21 12:49:07', 'cc304abb5db908d2bae84ca3a898958d', 1),
(341, 'Riccardo_pachera@hotmail.it', '-1', '10153547090748351', 'Riccardo', 'Pachera', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150821145257_3277.png', '1981-10-15', '0', -1, '', '', '2015-08-21 14:52:57', '2015-08-21 12:52:57', '20d758b780e3f008d0250f8c66377445', 1),
(342, 'vecellio.wladimiro@gmail.com', '112494998037838738014', '-1', 'Wladimiro', 'Vecellio', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150821145725_1040.png', '2016-09-23', '0', -1, '', '', '2015-08-21 14:57:25', '2015-08-21 12:57:25', 'aa9af239231375a2577bc0536533670e', 1),
(343, 'mattonea@gmail.com', '-1', '10207818918260722', 'Simone', 'Piva', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150821172316_8708.png', '1979-04-12', '0', -1, '', '', '2015-08-21 17:23:16', '2015-08-21 15:23:16', '9da22d16b5fbf6bfd612b5afbbc75220', 1),
(344, 'luca.fabio@gmail.com', '-1', '10154132297899119', 'Luca', 'Fabio', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150821175130_4747.png', '1961-12-16', '0', -1, '', '', '2015-08-21 17:51:30', '2015-08-21 15:51:30', '43eea1fdd2c76ba4030abd278888d505', 1),
(345, 'michele.postiferi@me.com', '-1', '945075778907911', 'Michele', 'Postiferi', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150821183746_362.png', '1962-04-27', '0', -1, '', '', '2015-08-21 18:37:46', '2015-08-21 16:37:46', '5ec29010e340120afa3f118102b1bfea', 1),
(346, 'calzolari.luca95@gmail.com', '-1', '-1', 'Luca', 'Calzolari ', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '1995-03-04', '0', 25, 'Milano', '-1', '2015-08-21 19:29:41', '2015-08-21 17:32:42', '2a9fc8705a6946cb324bc4de11918f3a', 1),
(347, 'ottavia1997@hotmail.it', '-1', '864263446989843', 'Ottavia', 'Fioravanti', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150821201116_2303.png', '2015-04-25', '1', -1, '', '', '2015-08-21 20:11:16', '2015-08-21 18:11:16', 'b414c699decdf49030496e0c92a70b43', 1),
(348, 'luci.gloria@alice.it', '-1', '1007405779299011', 'Gloria', 'Ingrosso', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150821221931_4617.png', '1992-03-26', '1', -1, '', '', '2015-08-21 22:19:31', '2015-08-21 20:19:31', 'cc92eccff0b5b2ec2060b01a736c0e73', 1),
(349, 'la.reny@libero.it', '-1', '10207492920783449', 'Renata', 'Farina', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150822010056_1083.png', '1982-03-28', '1', -1, '', '', '2015-08-22 01:00:56', '2015-08-21 23:00:56', '6b547fb9e126ab0094bf2078f67ecd6f', 1),
(350, 'artbodnar@gmail.com', '-1', '1112284572132607', 'Артур', 'Боднар', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150822045955_889.png', '1986-11-12', '0', -1, '', '', '2015-08-22 04:59:55', '2015-08-22 02:59:55', '2bf7dbddbf4a9b48fc8be98c13e5573a', 1),
(351, 'stefanoprive@gmail.com', '110271493528602315999', '-1', 'Stefano', 'Liani', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150822061141_938.png', '1977-10-05', '0', -1, '', '', '2015-08-22 06:11:41', '2015-08-22 04:11:41', '6dff4c5363fe38b3685baeb891f6744d', 1),
(352, 'martina.migliari@studio.unibo.it', '-1', '10207437113716703', 'Martina', 'Migliari', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150822080626_4072.png', '1992-08-09', '1', -1, '', '', '2015-08-22 08:06:26', '2015-08-22 06:06:26', '5a72436b47eb51186129e4180789f766', 1),
(353, 'agnesefalanca@iCloud.com', '-1', '-1', 'Agnese', 'Falanca', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '1997-08-03', '1', -1, '', '', '2015-08-22 08:45:40', '2015-08-22 06:45:40', '4cb3181a91dcc94ed56b02c81411b1ed', 1),
(354, 'marc.caputo@tiscali.it', '-1', '1001595623218616', 'Marco', 'Caputo', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150822090456_3087.png', '1980-08-04', '0', 10, 'NAPLES ', '-1', '2015-08-22 09:04:56', '2015-08-22 07:22:05', '67a652e61659524e8f307f2fadb76242', 1),
(355, 'guerrierob@gmail.com', '-1', '-1', 'Bruno', 'Guerriero', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '1960-02-11', '0', -1, '', '', '2015-08-22 09:25:20', '2015-08-22 07:25:20', 'e043ec6c9cf31b64014c87599fb67b92', 1),
(356, 'bertazzon.ms@gmail.com', '-1', '10206442084163729', 'Maria Sole', 'Bertazzon', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150822094255_4849.png', '1995-08-14', '1', -1, '', '', '2015-08-22 09:42:55', '2015-08-22 07:42:55', '597f6d8ede97f468c6e516fcbb908535', 1),
(357, 'cristiana.ciunfrini@libero.it', '-1', '744143622377999', 'Cristiana', 'Ciunfrini', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150822102525_6772.png', '1992-07-07', '1', -1, '', '', '2015-08-22 10:25:25', '2015-08-22 08:25:25', 'ccbef87b57121e57b4057eecb2aca9b2', 1),
(358, 'giadabusco@gmail.com', '-1', '829789463808358', 'Giada', 'Busco', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150822103020_2630.png', '2000-03-20', '1', -1, '', '', '2015-08-22 10:30:20', '2015-08-22 08:30:20', 'd2d7c3bd8decf04f696084ff3671627d', 1),
(359, 'meggy.sky84@gmail.com', '-1', '-1', 'Tecla', 'Mazza', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '1984-11-21', '1', -1, '', '', '2015-08-22 10:31:51', '2015-08-22 08:31:51', '38743a6a0d6e5d68c3d64c4c35d2aa05', 1),
(360, 'sashy_92@hotmail.it', '-1', '10208322754894898', 'Satcha', 'Valentina', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150822112150_9266.png', '1992-09-04', '1', -1, '', '', '2015-08-22 11:21:50', '2015-08-22 09:21:50', '099ee5a4efa476d43acddd8eae5e9ed3', 1),
(361, 'gloria.caranzano@hotmail.it', '-1', '-1', 'Gloria ', 'Caranzano', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '1998-11-30', '1', -1, '', '', '2015-08-22 12:33:47', '2015-08-22 10:33:47', 'e75b4803f70a07c8244738fa9cad5186', 1),
(362, 'drumembas@Yahoo.it', '-1', '-1', 'Nyhamna', 'Aukra', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '2015-08-22', '0', -1, '', '', '2015-08-22 12:44:40', '2015-08-22 10:44:40', 'b8272dc28c90c2ea6c8360314bc62d4d', 1),
(363, 'luigi.cazzola@me.com', '-1', '10208089404340719', 'Luigi', 'Cazzola', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150822124909_4038.png', '1968-12-06', '0', -1, '', '', '2015-08-22 12:49:09', '2015-08-22 10:49:09', '3f37f16712f6965996074208ca885f7d', 1),
(364, 'robba12@gmail.com', '-1', '10152940872071386', 'Roberto', 'Porchera', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150822134258_8326.png', '1975-07-13', '0', -1, '', '', '2015-08-22 13:42:58', '2015-08-22 11:42:58', 'ba5229124617a37c37cbda03d9888290', 1),
(365, 'carlottina_titina@live.it', '-1', '1189767261049185', 'Carlotta', 'Pagliari', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150822141742_9679.png', '1996-07-23', '1', -1, '', '', '2015-08-22 14:17:42', '2015-08-22 12:17:42', '5032b3d1056143141b388ab6e0662a19', 1),
(366, 'davide21@live.it', '-1', '10207698871659196', 'Davide', 'Rocco', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150822142417_7279.png', '2010-09-09', '0', -1, '', '', '2015-08-22 14:24:17', '2015-08-22 12:24:17', '60808f04932e71332747d181fcd63934', 1),
(367, 'sonofigasoloio@Gmail.com', '-1', '-1', 'Vale', 'Vale', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '1988-08-22', '1', -1, '', '', '2015-08-22 15:49:55', '2015-08-22 13:49:55', '7f6e5300dbc582aaa43f96ccd0d023e2', 1),
(368, 'julie.dechappe09@gmail.com', '115767971331952080295', '-1', 'Julie', 'Dechappe', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150822164201_7996.png', '1986-10-29', '1', -1, '', '', '2015-08-22 16:42:01', '2015-08-22 14:42:01', '6c64b5ae4e6e134348f3566e48183052', 1),
(369, 'fed.arde@gmail.com', '-1', '10207640625636692', 'Federico', 'Ardemagni', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150822165233_6489.png', '1994-08-05', '0', -1, '', '', '2015-08-22 16:52:33', '2015-08-22 14:52:33', '45578fbdf8733a934383594dcb0d3e76', 1),
(370, 'shekespeare@hotmail.it', '-1', '10207118271977849', 'Giancarlo', 'De Marinis', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150822171949_5514.png', '1977-05-10', '0', -1, '', '', '2015-08-22 17:19:49', '2015-08-22 15:19:49', '61eaa5948c20d9f9549c8825ef5c75b5', 1),
(371, 'matteosantoro1998@live.it', '-1', '984567244898322', 'Matteo', 'Santoro', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150822175749_5392.png', '1998-11-23', '0', -1, '', '', '2015-08-22 17:57:49', '2015-08-22 15:57:49', '5f28f33da5d44fd1b1ed6832e7574a7c', 1),
(372, 'chiara_bettini@icloud.com', '-1', '-1', 'Chiara', 'Bettini', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '1989-07-28', '1', -1, '', '', '2015-08-22 19:13:27', '2015-08-22 17:13:27', '4ac00000e4b583093522cf5b1a11bbb8', 1),
(373, 'catanzaroalice@gmail.com', '-1', '770373026418935', 'Alice', 'Catanzaro', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150822195721_5961.png', '2000-02-04', '1', -1, '', '', '2015-08-22 19:57:21', '2015-08-22 17:57:21', '6dd18ba983e3b34241c876bdc32e4ab0', 1),
(374, 'robbyliberty@gmail.com', '-1', '1483433971950194', 'Roby Ed', 'Ely', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150822200739_5607.png', '1992-03-30', '1', -1, '', '', '2015-08-22 20:07:39', '2015-08-22 18:07:39', '52cd1ad89a515a523406d778cad5acca', 1),
(375, 'gauthama03@gmail.com', '100090233374816844787', '-1', 'Francesco', 'Gauthama', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150822212805_1875.png', '1970-07-03', '0', -1, '', '', '2015-08-22 21:28:05', '2015-08-22 19:28:05', '982d0b799e4c637c87f187963a43be61', 1),
(376, 'cianferotticlara1@gmail.com', '-1', '109122892773893', 'Clara', 'C', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150822221213_1624.png', '1989-08-07', '1', -1, '', '', '2015-08-22 22:12:13', '2015-08-22 20:12:13', 'd7ce94af44066f3e97490358c035124b', 1),
(377, 'luca.cerale@gmail.com', '104174861792353009809', '-1', 'Luca', 'Cerale', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150822221338_7611.png', '1994-03-30', '0', -1, '', '', '2015-08-22 22:13:38', '2015-08-22 20:13:38', 'd361ca2290cbe9e1ed78628ae1a59da3', 1);
INSERT INTO `users` (`iduser`, `email`, `idgoogle`, `idfacebook`, `name`, `surname`, `img`, `birthdate`, `sex`, `job`, `city`, `interests`, `creationdate`, `updatetime`, `password`, `activated`) VALUES
(378, 'ingrcarmando@Gmail.com', '-1', '10207537330179728', 'Remigio', 'Carmando', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150822221935_6929.png', '1978-06-28', '0', -1, '', '', '2015-08-22 22:19:35', '2015-08-22 20:19:35', 'cc40d6fd33c284f6ae63000faf32c686', 1),
(379, 'sfiume@tin.it', '-1', '10154123166178272', 'Valentina', 'Ruocco Fiume', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150822224315_1930.png', '1984-09-21', '1', -1, '', '', '2015-08-22 22:43:15', '2015-08-22 20:43:15', 'e105150712df2f584c2aa93dcb39b219', 1),
(380, 'cecchino2015@libero.it', '-1', '420827624789959', 'David', 'Themagic', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150822224837_4211.png', '2015-08-22', '0', -1, '', '', '2015-08-22 22:48:37', '2015-08-22 20:48:37', '85dba10f99c9b370a10924bfe00d478a', 1),
(381, 'matteo.done@gmail.com', '-1', '10153588434233000', 'Matteo', 'Donè', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150822225228_1983.png', '1979-12-19', '0', -1, '', '', '2015-08-22 22:52:28', '2015-08-22 20:52:28', '313ffbdf5cc680215b24e2f08989d16c', 1),
(382, 'camilla.paolessi0@gmail.com', '-1', '10206200183020791', 'Camilla', 'Paolessi', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150822235135_7935.png', '1996-08-13', '1', -1, '', '', '2015-08-22 23:51:35', '2015-08-22 21:51:35', '160b7272f03c92fd5563bf306e59b7ba', 1),
(383, 'olsi85@gmail.com', '-1', '-1', 'Olsi', 'Braho', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '1985-09-07', '0', -1, '', '', '2015-08-23 00:23:05', '2015-08-22 22:23:05', '00fc44acb7194b90b293e870bd03d13d', 1),
(384, 'frankaos@me.com', '-1', '-1', 'Jacopo ', 'Franchetti', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150823004958_4542.png', '1990-06-24', '0', -1, '', '', '2015-08-23 00:49:58', '2015-08-22 22:49:58', '41d5eea0ba696ff1ac0906ce6ddfc159', 1),
(385, 'Samanta.molla@gmail.com', '-1', '-1', 'Samanta ', 'Molla', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '1977-08-24', '1', -1, '', '', '2015-08-23 00:58:58', '2015-08-22 22:58:58', 'a91d222009a393b074618e874e49a484', 1),
(386, 'puccio9891@icloud.com', '-1', '10206163395755465', 'Puccio', 'Gabriele', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150823040521_8682.png', '1989-04-26', '0', -1, '', '', '2015-08-23 04:05:21', '2015-08-23 02:05:21', '5a9c10cdf518267bb4a9446f7e173bb7', 1),
(387, 'fadiabj@gmail.com', '-1', '-1', 'Fadia ', 'Bassmaji', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '1984-02-29', '1', -1, '', '', '2015-08-23 10:00:20', '2015-08-23 08:00:20', 'b0f2b5191ab94752cb6bc1483d61a597', 1),
(388, 'senese.nicola@yahoo.it', '104958819499732700295', '-1', 'Nicola', 'Senese', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150823101059_2843.png', '1982-03-15', '0', -1, '', '', '2015-08-23 10:10:59', '2015-08-23 08:10:59', 'f1ca84278d26051f39c8e88724ac84f6', 1),
(389, 'luka_77@hotmail.it', '-1', '10206361552269071', 'Luca', 'Talamo', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150823112604_1078.png', '1977-09-21', '0', -1, '', '', '2015-08-23 11:26:04', '2015-08-23 09:26:04', 'c5ff40d9c7379d6e415424af7afa11e3', 1),
(390, 'ade_radke@live.it', '-1', '10205106043189964', 'Adelina', 'Khafizova', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150823124858_1580.png', '2015-11-21', '1', -1, '', '', '2015-08-23 12:48:58', '2015-08-23 10:48:58', '9811e1f16e8d2622ef915bd0228912d3', 1),
(391, 'cristian.dipanfilo@gmail.com', '-1', '10154115904808154', 'Cristian', 'Panfilo', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150823131702_5378.png', '1983-05-13', '0', -1, '', '', '2015-08-23 13:17:02', '2015-08-23 11:17:02', 'f1778ec5dd7d4bbcc59f1d15d2a16fdf', 1),
(392, 'J_ago@hotmail.it', '-1', '-1', 'Simone', 'Di Shox', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150823135512_5284.png', '1984-07-13', '0', -1, '', '', '2015-08-23 13:55:12', '2015-08-23 11:55:12', 'e44e9e6f7e220d545e562e3c279b63b4', 1),
(393, 'fase9700@gmail.com', '-1', '-1', 'Fabi', 'Ang', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '1973-08-23', '1', -1, '', '', '2015-08-23 14:32:56', '2015-08-23 12:32:56', '361f41471e35649d7d93e3cca045caee', 1),
(394, 'carmine.cerrone@gmail.com', '-1', '10207445037104887', 'Carmine', 'Cerrone', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150823143405_8415.png', '1976-09-24', '0', -1, '', '', '2015-08-23 14:34:05', '2015-08-23 12:34:05', '84cc4a1aa0151102255537edf1b69338', 1),
(395, 'gaetano.dilascio@gmail.com', '116312028844940868298', '-1', 'Gaetano', 'Di Lascio', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150823144911_5348.png', '1986-12-18', '0', -1, '', '', '2015-08-23 14:49:11', '2015-08-23 12:49:11', '28dcd87a2b1bcffcaac4774a775a68aa', 1),
(396, 'alessandraazzena90@gmail.com', '116707503245768556077', '-1', 'Alessandra', 'Azzena', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150823160651_1487.png', '1990-11-11', '1', -1, '', '', '2015-08-23 16:06:51', '2015-08-23 14:06:51', 'b3ecde8cadb73eb076cc0df0213cef74', 1),
(397, 'betta.francy@hotmail.it', '-1', '10206823301522227', 'Francesco', 'Betta', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150823172825_5568.png', '1993-04-01', '0', -1, '', '', '2015-08-23 17:28:25', '2015-08-23 15:28:25', 'e8a3c2e04ab019c82676606c7e69239c', 1),
(398, 'juan_josemi@yahoo.com', '-1', '875760409177855', 'Cipry', 'Mihalcea', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150823182828_5124.png', '1986-10-10', '0', -1, '', '', '2015-08-23 18:28:28', '2015-08-23 16:28:28', 'c71729c3110927adad218ea38a302ba6', 1),
(399, 'shsnahashah@gmail.com', '-1', '-1', 'Ahaha', 'Hahaha', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '2015-08-23', '0', -1, '', '', '2015-08-23 18:57:15', '2015-08-23 16:57:15', '6c4b15c7f4804580fb967b1a70c1a3a3', 1),
(400, 'lorenzoaldrovandi@hotmail.com', '-1', '967430106650539', 'Lorenzo', 'Aldrovandi', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150823192116_3973.png', '1993-10-29', '0', -1, '', '', '2015-08-23 19:21:16', '2015-08-23 17:21:16', '352c42635365d8bf85235f23c4f4976a', 1),
(401, 'daniele.temp@gmail.com', '-1', '10153177212106656', 'Daniele', 'Iori', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150823193444_6953.png', '1977-01-01', '0', -1, '', '', '2015-08-23 19:34:45', '2015-08-23 17:34:45', '01989d17af841cee8079e87e8cf3d003', 1),
(402, 'rossanalocatelli@hotmail.com', '-1', '-1', 'Rossana', 'Locatelli', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '1978-05-30', '1', -1, '', '', '2015-08-23 19:40:57', '2015-08-23 17:40:57', '0aa9c4d93a335a1cb9e5f0f3af0f1635', 1),
(403, 'mbesiniciarini@gmail.com', '-1', '111782739174272', 'Mariateresa', 'Panzera', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150823194704_861.png', '1991-10-31', '1', -1, '', '', '2015-08-23 19:47:04', '2015-08-23 17:47:04', '13e6d176f6d922c22e14cd49208b5c42', 1),
(404, 'a.baricci@virgilio.it', '-1', '10206095032547981', 'Alessandro', 'Baricci', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150823195045_2096.png', '1962-04-10', '0', -1, '', '', '2015-08-23 19:50:45', '2015-08-23 17:50:45', 'a153de1cdfacbace561dd0701e406a6f', 1),
(405, 'alexcargnel@gmail.com', '-1', '10207821347037496', 'Alex', 'Cargnel', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150823195138_1439.png', '1988-10-08', '0', -1, '', '', '2015-08-23 19:51:38', '2015-08-23 17:51:38', 'e02f842730897d8c4d76be206b791d45', 1),
(406, 'salvatorenocera1910@hotmail.it', '-1', '-1', 'Salvatore', 'De Nardo', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150823200133_7810.png', '1988-08-02', '0', -1, '', '', '2015-08-23 20:01:33', '2015-08-23 18:01:33', '40f04bfb06fc46726397eb108dcb1699', 1),
(407, 'ga6ri3lla@gmail.com', '-1', '-1', 'Gabriella', 'B', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '1996-11-23', '1', -1, '', '', '2015-08-23 21:11:23', '2015-08-23 19:11:23', 'f23486ec15904608f9c32b08caaef213', 1),
(408, 'gabrielitoasr@hotmail.it', '-1', '856617081101988', 'Gabriele', 'Serani', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150823211543_562.png', '1997-05-30', '0', -1, '', '', '2015-08-23 21:15:43', '2015-08-23 19:15:43', '7037544d67efd584ec4bb786cb7a25bc', 1),
(409, 'valos.erline@gmail.com', '-1', '-1', 'Valos', 'Erline', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/avatardefault.png', '1969-03-05', '0', 6, 'Gorizia', 'Sports cultura feste\n', '2015-08-23 22:31:04', '2015-08-23 20:34:58', '19e392eba63dd75236eacd5eecc542e2', 1),
(410, 'joxhinarakipaj@gmail.com', '100973430729113245707', '-1', 'Joxhina', 'Rakipaj', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150823233509_493.png', '1989-09-05', '1', -1, '-1', '-1', '2015-08-23 23:32:12', '2015-08-23 21:35:09', '01e02ab624d77adf57c221d61b176a50', 1),
(411, 'beatrice.laurenti@hotmail.it', '-1', '749351095173562', 'Beatrice', 'Laurenti', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150824001254_1086.png', '1998-03-18', '1', -1, '', '', '2015-08-24 00:12:54', '2015-08-23 22:12:54', '60e52d4e0ff16f02af02c26806cf62b8', 1),
(412, 'marco.masucci@gmail.com', '-1', '10153766140207454', 'Marco', 'Masucci', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150824080953_8364.png', '1987-11-28', '0', -1, '', '', '2015-08-24 08:09:53', '2015-08-24 06:09:53', '5c5d2b63f87eb9287b497ff58fde8fd5', 1),
(413, 'andreasalini@googlemail.com', '-1', '10207006176900068', 'Andrea', 'Salini', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150824094114_9438.png', '1971-11-08', '0', -1, '', '', '2015-08-24 09:41:14', '2015-08-24 07:41:14', 'a63c843703ddca060b62951df25dbce1', 1),
(414, 'girlsilvia@live.it', '-1', '887618631345821', 'Silvia', 'Dal Pozzolo', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150824094220_8964.png', '1996-07-13', '1', -1, '', '', '2015-08-24 09:42:20', '2015-08-24 07:42:20', '6eac59b1c4af7c585bda3e2fb004376f', 1),
(415, 'malvasodalma@gmail.com', '-1', '10206982887551593', 'Dalma', 'Malvaso', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150824101022_9031.png', '1991-08-11', '1', -1, '', '', '2015-08-24 10:10:22', '2015-08-24 08:10:22', 'bf56724668841ad1bbc6b8b21faf7c67', 1),
(416, 'marco.tomasso.1991@gmail.com', '-1', '10207212065121056', 'Marco', 'Tomasso Di Palma', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150824101534_131.png', '1991-10-30', '0', -1, '', '', '2015-08-24 10:15:34', '2015-08-24 08:15:34', '92244c0e14b2cfa32c4de09a03c990ab', 1),
(417, 'drgianlucacolombo@gmail.com', '102975248629837400771', '-1', 'Gianluca', 'Colombo', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150824103202_1825.png', '1985-10-22', '0', -1, '', '', '2015-08-24 10:32:02', '2015-08-24 08:32:02', '2efbcdf2eae0994c0dc3ccbf0dcb68ae', 1),
(418, 'gigli1@hotmail.it', '-1', '10206309984967242', 'Elena', 'Gozzi', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150824105421_5767.png', '1990-03-22', '1', -1, '', '', '2015-08-24 10:54:21', '2015-08-24 08:54:21', '2fc02c75edc2c3a9d72f03a8e04ce62d', 1),
(419, 'nisa56@libero.it', '-1', '10207239672976498', 'Graziella', 'Santaniello', 'http://www.join-me.it/web-service/dev/app/webroot/images/user/20150824105807_4841.png', '1983-08-11', '1', -1, '', '', '2015-08-24 10:58:07', '2015-08-24 08:58:08', '14bea76a0a97964d07109606f0a00640', 1);

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `user_accepted_follows`
--
CREATE TABLE IF NOT EXISTS `user_accepted_follows` (
`iduser` int(11)
,`idfollow` int(11)
,`name` varchar(45)
,`surname` varchar(45)
,`img` varchar(128)
);
-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `user_accepted_friends`
--
CREATE TABLE IF NOT EXISTS `user_accepted_friends` (
`iduser` int(11)
,`idfriend` int(11)
,`accepted` int(5)
,`name` varchar(45)
,`surname` varchar(45)
,`img` varchar(128)
);
-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `user_created_events`
--
CREATE TABLE IF NOT EXISTS `user_created_events` (
`idevent` int(11)
,`iduser` int(11)
,`sex` int(1)
,`sittotal` int(3)
,`sitcurrent` int(3)
,`idcategory` int(11)
,`title` varchar(100)
,`description` varchar(1000)
,`city` varchar(45)
,`address` varchar(45)
,`agemin` int(2)
,`agemax` int(2)
,`date` date
,`hour` time
,`scope` int(11)
,`latitude` varchar(10)
,`longitude` varchar(10)
,`img` varchar(128)
,`userimg` varchar(128)
,`job` int(3)
);
-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `user_event_notifications`
--
CREATE TABLE IF NOT EXISTS `user_event_notifications` (
`id` int(11)
,`date` timestamp
,`type` int(1)
,`idsender` int(11)
,`idreceiver` int(11)
,`idevent` int(11)
,`name` varchar(45)
,`surname` varchar(45)
,`img` varchar(128)
,`title` varchar(100)
);
-- --------------------------------------------------------

--
-- Struttura della tabella `user_follows`
--

CREATE TABLE IF NOT EXISTS `user_follows` (
  `iduser` int(11) NOT NULL,
  `idfollow` int(11) NOT NULL,
  PRIMARY KEY (`iduser`,`idfollow`),
  KEY `fk_users_has_follow_idfollow` (`idfollow`),
  KEY `fk_users_has_follow_iduser` (`iduser`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `user_follows`
--

INSERT INTO `user_follows` (`iduser`, `idfollow`) VALUES
(1, 10),
(1, 14),
(10, 1),
(10, 11),
(10, 12),
(10, 14),
(10, 16),
(10, 21),
(10, 28),
(11, 19),
(12, 1),
(12, 8),
(12, 10),
(12, 14),
(12, 19),
(12, 71),
(12, 91),
(12, 131),
(12, 142),
(12, 165),
(12, 168),
(12, 169),
(12, 177),
(12, 190),
(12, 194),
(12, 195),
(12, 205),
(12, 214),
(12, 239),
(12, 252),
(12, 287),
(12, 299),
(12, 314),
(12, 316),
(12, 392),
(13, 10),
(14, 1),
(14, 10),
(14, 11),
(14, 12),
(16, 10),
(16, 12),
(16, 14),
(16, 19),
(18, 10),
(24, 32),
(30, 10),
(66, 71),
(71, 79),
(71, 101),
(71, 140),
(80, 79),
(85, 71),
(91, 12),
(91, 71),
(91, 83),
(91, 131),
(91, 142),
(91, 163),
(91, 165),
(91, 168),
(91, 182),
(91, 188),
(91, 194),
(91, 195),
(91, 205),
(91, 213),
(91, 214),
(91, 243),
(91, 299),
(92, 71),
(92, 106),
(98, 94),
(101, 71),
(101, 94),
(101, 107),
(108, 92),
(131, 12),
(131, 90),
(131, 91),
(131, 94),
(131, 101),
(131, 135),
(131, 142),
(131, 163),
(131, 168),
(131, 177),
(131, 179),
(131, 180),
(131, 182),
(131, 213),
(131, 214),
(133, 101),
(134, 101),
(135, 106),
(135, 107),
(135, 109),
(135, 122),
(135, 128),
(135, 131),
(140, 71),
(140, 91),
(140, 101),
(140, 107),
(142, 179),
(144, 71),
(144, 140),
(158, 131),
(158, 164),
(163, 12),
(163, 91),
(163, 131),
(163, 135),
(163, 165),
(163, 177),
(163, 179),
(163, 182),
(163, 188),
(163, 190),
(163, 194),
(163, 195),
(163, 206),
(163, 208),
(163, 214),
(163, 272),
(165, 12),
(165, 91),
(165, 194),
(165, 195),
(165, 205),
(165, 206),
(165, 213),
(165, 214),
(165, 224),
(165, 239),
(165, 272),
(165, 287),
(165, 299),
(168, 12),
(168, 91),
(168, 131),
(168, 177),
(168, 198),
(168, 206),
(169, 12),
(169, 131),
(177, 142),
(179, 12),
(179, 142),
(179, 182),
(182, 91),
(182, 131),
(182, 163),
(182, 179),
(182, 187),
(182, 194),
(182, 198),
(182, 213),
(182, 214),
(182, 239),
(182, 260),
(188, 91),
(190, 91),
(194, 12),
(194, 91),
(194, 165),
(194, 217),
(194, 252),
(195, 12),
(195, 91),
(195, 165),
(197, 198),
(198, 212),
(199, 91),
(199, 197),
(205, 12),
(205, 206),
(213, 12),
(213, 214),
(214, 91),
(214, 165),
(217, 195),
(223, 182),
(223, 194),
(223, 245),
(228, 91),
(228, 182),
(228, 260),
(233, 91),
(233, 194),
(239, 12),
(239, 299),
(243, 285),
(243, 299),
(244, 245),
(246, 214),
(252, 12),
(252, 165),
(252, 194),
(252, 217),
(261, 228),
(266, 163),
(272, 12),
(277, 91),
(279, 278),
(287, 12),
(287, 214),
(292, 214),
(292, 243),
(295, 214),
(302, 91),
(309, 285),
(314, 12),
(314, 165),
(314, 194),
(314, 217),
(314, 252),
(317, 316),
(337, 195),
(337, 243),
(337, 299),
(342, 316),
(353, 194),
(358, 316);

-- --------------------------------------------------------

--
-- Struttura della tabella `user_friends`
--

CREATE TABLE IF NOT EXISTS `user_friends` (
  `iduser` int(11) NOT NULL,
  `idfriend` int(11) NOT NULL,
  `accepted` int(5) NOT NULL COMMENT '0:inviata ed in attesa di accettazione, 2: ricevuta e da accettare, 1: amicizia attiva',
  PRIMARY KEY (`iduser`,`idfriend`),
  KEY `fk_users_has_users_users4_idx` (`idfriend`),
  KEY `fk_users_has_users_users3_idx` (`iduser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `user_friends`
--

INSERT INTO `user_friends` (`iduser`, `idfriend`, `accepted`) VALUES
(1, 10, 1),
(1, 12, 1),
(1, 14, 1),
(8, 12, 1),
(10, 1, 1),
(10, 12, 1),
(10, 16, 1),
(10, 18, 1),
(10, 28, 1),
(10, 30, 1),
(10, 31, 1),
(12, 1, 1),
(12, 8, 1),
(12, 10, 1),
(12, 14, 1),
(12, 19, 0),
(12, 71, 1),
(12, 90, 1),
(12, 91, 1),
(12, 131, 1),
(12, 142, 1),
(12, 163, 1),
(12, 165, 1),
(12, 168, 1),
(12, 169, 1),
(12, 177, 1),
(12, 190, 0),
(12, 194, 1),
(12, 195, 1),
(12, 213, 1),
(12, 214, 1),
(12, 217, 1),
(12, 239, 1),
(12, 241, 1),
(12, 252, 1),
(12, 287, 1),
(12, 314, 1),
(12, 316, 0),
(14, 1, 1),
(14, 12, 1),
(16, 10, 1),
(16, 19, 1),
(18, 10, 1),
(19, 12, 2),
(19, 16, 1),
(24, 32, 1),
(28, 10, 1),
(30, 10, 1),
(31, 10, 1),
(32, 24, 1),
(44, 50, 1),
(50, 44, 1),
(66, 71, 1),
(71, 12, 1),
(71, 66, 1),
(71, 79, 1),
(71, 80, 1),
(71, 81, 1),
(71, 83, 1),
(71, 85, 1),
(71, 90, 1),
(71, 91, 1),
(71, 92, 1),
(71, 93, 0),
(71, 101, 2),
(71, 131, 1),
(71, 140, 1),
(71, 144, 1),
(79, 71, 1),
(79, 80, 1),
(80, 71, 1),
(80, 79, 1),
(80, 81, 1),
(80, 83, 1),
(81, 71, 1),
(81, 80, 1),
(83, 71, 1),
(83, 80, 1),
(83, 85, 1),
(83, 91, 2),
(85, 71, 1),
(85, 83, 1),
(85, 90, 1),
(90, 12, 1),
(90, 71, 1),
(90, 85, 1),
(91, 12, 1),
(91, 71, 1),
(91, 83, 0),
(91, 101, 1),
(91, 131, 0),
(91, 165, 1),
(91, 168, 1),
(91, 182, 1),
(91, 188, 1),
(91, 190, 1),
(91, 194, 1),
(91, 195, 1),
(91, 205, 1),
(91, 213, 1),
(91, 214, 1),
(91, 217, 1),
(91, 277, 1),
(91, 299, 1),
(91, 328, 0),
(92, 71, 1),
(92, 98, 1),
(92, 103, 1),
(92, 106, 1),
(92, 108, 1),
(93, 71, 2),
(93, 94, 1),
(94, 93, 1),
(94, 98, 1),
(94, 101, 2),
(98, 92, 1),
(98, 94, 1),
(101, 71, 0),
(101, 91, 1),
(101, 94, 0),
(101, 107, 1),
(101, 131, 1),
(101, 133, 1),
(101, 134, 1),
(101, 136, 1),
(103, 92, 1),
(106, 92, 1),
(107, 101, 1),
(108, 92, 1),
(131, 12, 1),
(131, 71, 1),
(131, 91, 2),
(131, 101, 1),
(131, 135, 1),
(131, 140, 1),
(131, 142, 1),
(131, 163, 1),
(131, 168, 1),
(131, 179, 1),
(131, 182, 1),
(131, 186, 1),
(131, 214, 1),
(131, 239, 1),
(133, 101, 1),
(134, 101, 1),
(135, 131, 1),
(136, 101, 1),
(140, 71, 1),
(140, 131, 1),
(142, 12, 1),
(142, 131, 1),
(142, 163, 0),
(142, 177, 1),
(142, 179, 1),
(142, 182, 1),
(142, 189, 1),
(142, 198, 1),
(144, 71, 1),
(158, 164, 1),
(163, 12, 1),
(163, 131, 1),
(163, 142, 2),
(163, 165, 1),
(163, 177, 0),
(163, 182, 1),
(163, 194, 1),
(163, 214, 1),
(163, 263, 1),
(163, 264, 1),
(163, 265, 0),
(163, 266, 1),
(164, 158, 1),
(165, 12, 1),
(165, 91, 1),
(165, 163, 1),
(165, 194, 1),
(165, 195, 1),
(165, 205, 1),
(165, 206, 1),
(165, 213, 1),
(165, 214, 1),
(165, 217, 1),
(165, 239, 1),
(165, 241, 1),
(165, 252, 1),
(165, 258, 1),
(165, 287, 1),
(165, 300, 0),
(165, 314, 1),
(168, 12, 1),
(168, 91, 1),
(168, 131, 1),
(168, 169, 0),
(168, 206, 1),
(169, 12, 1),
(169, 168, 2),
(169, 176, 1),
(176, 169, 1),
(176, 177, 1),
(177, 12, 1),
(177, 142, 1),
(177, 163, 2),
(177, 176, 1),
(179, 131, 1),
(179, 142, 1),
(179, 212, 2),
(182, 91, 1),
(182, 131, 1),
(182, 142, 1),
(182, 163, 1),
(182, 198, 1),
(182, 223, 1),
(182, 224, 1),
(182, 260, 1),
(186, 131, 1),
(188, 91, 1),
(189, 142, 1),
(190, 12, 2),
(190, 91, 1),
(194, 12, 1),
(194, 91, 1),
(194, 163, 1),
(194, 165, 1),
(194, 205, 0),
(194, 217, 1),
(194, 241, 1),
(194, 243, 1),
(194, 252, 1),
(194, 314, 1),
(195, 12, 1),
(195, 91, 1),
(195, 165, 1),
(195, 217, 1),
(195, 337, 2),
(197, 198, 2),
(197, 199, 2),
(198, 142, 1),
(198, 182, 1),
(198, 197, 0),
(198, 199, 1),
(198, 203, 1),
(199, 197, 0),
(199, 198, 1),
(199, 202, 2),
(199, 203, 2),
(202, 199, 0),
(203, 198, 1),
(203, 199, 0),
(205, 91, 1),
(205, 165, 1),
(205, 194, 2),
(206, 165, 1),
(206, 168, 1),
(212, 179, 0),
(213, 12, 1),
(213, 91, 1),
(213, 165, 1),
(213, 217, 1),
(214, 12, 1),
(214, 91, 1),
(214, 131, 1),
(214, 163, 1),
(214, 165, 1),
(214, 217, 1),
(214, 246, 1),
(217, 12, 1),
(217, 91, 1),
(217, 165, 1),
(217, 194, 1),
(217, 195, 1),
(217, 213, 1),
(217, 214, 1),
(217, 252, 1),
(217, 314, 1),
(223, 182, 1),
(224, 182, 1),
(225, 226, 1),
(226, 225, 1),
(228, 261, 1),
(239, 12, 1),
(239, 131, 1),
(239, 165, 1),
(239, 241, 1),
(241, 12, 1),
(241, 165, 1),
(241, 194, 1),
(241, 239, 1),
(243, 194, 1),
(243, 299, 1),
(243, 337, 2),
(244, 245, 1),
(245, 244, 1),
(246, 214, 1),
(252, 12, 1),
(252, 165, 1),
(252, 194, 1),
(252, 217, 1),
(252, 314, 1),
(258, 165, 1),
(260, 182, 1),
(261, 228, 1),
(263, 163, 1),
(264, 163, 1),
(265, 163, 2),
(266, 163, 1),
(272, 277, 1),
(277, 91, 1),
(277, 272, 1),
(278, 279, 1),
(279, 278, 1),
(285, 309, 1),
(287, 12, 1),
(287, 165, 1),
(299, 91, 1),
(299, 243, 1),
(300, 165, 2),
(309, 285, 1),
(314, 12, 1),
(314, 165, 1),
(314, 194, 1),
(314, 217, 1),
(314, 252, 1),
(316, 12, 2),
(316, 342, 2),
(328, 91, 2),
(337, 195, 0),
(337, 243, 0),
(342, 316, 0);

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `user_partecipate_events`
--
CREATE TABLE IF NOT EXISTS `user_partecipate_events` (
`idevent` int(11)
,`iduser` int(11)
,`sex` int(1)
,`sittotal` int(3)
,`sitcurrent` int(3)
,`idcategory` int(11)
,`title` varchar(100)
,`description` varchar(1000)
,`city` varchar(45)
,`address` varchar(45)
,`agemin` int(2)
,`agemax` int(2)
,`date` date
,`hour` time
,`scope` int(11)
,`latitude` varchar(10)
,`longitude` varchar(10)
,`userimg` varchar(128)
,`job` int(3)
,`iduserpartecipates` int(11)
);
-- --------------------------------------------------------

--
-- Struttura della tabella `zcp8_assets`
--

CREATE TABLE IF NOT EXISTS `zcp8_assets` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `parent_id` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set parent.',
  `lft` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set lft.',
  `rgt` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set rgt.',
  `level` int(10) unsigned NOT NULL COMMENT 'The cached level in the nested tree.',
  `name` varchar(50) NOT NULL COMMENT 'The unique name for the asset.\n',
  `title` varchar(100) NOT NULL COMMENT 'The descriptive title for the asset.',
  `rules` varchar(5120) NOT NULL COMMENT 'JSON encoded access control.',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_asset_name` (`name`),
  KEY `idx_lft_rgt` (`lft`,`rgt`),
  KEY `idx_parent_id` (`parent_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=60 ;

--
-- Dump dei dati per la tabella `zcp8_assets`
--

INSERT INTO `zcp8_assets` (`id`, `parent_id`, `lft`, `rgt`, `level`, `name`, `title`, `rules`) VALUES
(1, 0, 0, 117, 0, 'root.1', 'Root Asset', '{"core.login.site":{"6":1,"2":1},"core.login.admin":{"6":1},"core.login.offline":{"6":1},"core.admin":{"8":1},"core.manage":{"7":1},"core.create":{"6":1,"3":1},"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1},"core.edit.own":{"6":1,"3":1}}'),
(2, 1, 1, 2, 1, 'com_admin', 'com_admin', '{}'),
(3, 1, 3, 6, 1, 'com_banners', 'com_banners', '{"core.admin":{"7":1},"core.manage":{"6":1},"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(4, 1, 7, 8, 1, 'com_cache', 'com_cache', '{"core.admin":{"7":1},"core.manage":{"7":1}}'),
(5, 1, 9, 10, 1, 'com_checkin', 'com_checkin', '{"core.admin":{"7":1},"core.manage":{"7":1}}'),
(6, 1, 11, 12, 1, 'com_config', 'com_config', '{}'),
(7, 1, 13, 16, 1, 'com_contact', 'com_contact', '{"core.admin":{"7":1},"core.manage":{"6":1},"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[],"core.edit.own":[]}'),
(8, 1, 17, 24, 1, 'com_content', 'com_content', '{"core.admin":{"7":1},"core.manage":{"6":1},"core.create":{"3":1},"core.delete":[],"core.edit":{"4":1},"core.edit.state":{"5":1},"core.edit.own":[]}'),
(9, 1, 25, 26, 1, 'com_cpanel', 'com_cpanel', '{}'),
(10, 1, 27, 28, 1, 'com_installer', 'com_installer', '{"core.admin":[],"core.manage":{"7":0},"core.delete":{"7":0},"core.edit.state":{"7":0}}'),
(11, 1, 29, 30, 1, 'com_languages', 'com_languages', '{"core.admin":{"7":1},"core.manage":[],"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(12, 1, 31, 32, 1, 'com_login', 'com_login', '{}'),
(13, 1, 33, 34, 1, 'com_mailto', 'com_mailto', '{}'),
(14, 1, 35, 36, 1, 'com_massmail', 'com_massmail', '{}'),
(15, 1, 37, 38, 1, 'com_media', 'com_media', '{"core.admin":{"7":1},"core.manage":{"6":1},"core.create":{"3":1},"core.delete":{"5":1}}'),
(16, 1, 39, 40, 1, 'com_menus', 'com_menus', '{"core.admin":{"7":1},"core.manage":[],"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(17, 1, 41, 42, 1, 'com_messages', 'com_messages', '{"core.admin":{"7":1},"core.manage":{"7":1}}'),
(18, 1, 43, 80, 1, 'com_modules', 'com_modules', '{"core.admin":{"7":1},"core.manage":[],"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(19, 1, 81, 84, 1, 'com_newsfeeds', 'com_newsfeeds', '{"core.admin":{"7":1},"core.manage":{"6":1},"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[],"core.edit.own":[]}'),
(20, 1, 85, 86, 1, 'com_plugins', 'com_plugins', '{"core.admin":{"7":1},"core.manage":[],"core.edit":[],"core.edit.state":[]}'),
(21, 1, 87, 88, 1, 'com_redirect', 'com_redirect', '{"core.admin":{"7":1},"core.manage":[]}'),
(22, 1, 89, 90, 1, 'com_search', 'com_search', '{"core.admin":{"7":1},"core.manage":{"6":1}}'),
(23, 1, 91, 92, 1, 'com_templates', 'com_templates', '{"core.admin":{"7":1},"core.manage":[],"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(24, 1, 93, 96, 1, 'com_users', 'com_users', '{"core.admin":{"7":1},"core.manage":[],"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(25, 1, 97, 100, 1, 'com_weblinks', 'com_weblinks', '{"core.admin":{"7":1},"core.manage":{"6":1},"core.create":{"3":1},"core.delete":[],"core.edit":{"4":1},"core.edit.state":{"5":1},"core.edit.own":[]}'),
(26, 1, 101, 102, 1, 'com_wrapper', 'com_wrapper', '{}'),
(27, 8, 18, 23, 2, 'com_content.category.2', 'Uncategorised', '{"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[],"core.edit.own":[]}'),
(28, 3, 4, 5, 2, 'com_banners.category.3', 'Uncategorised', '{"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(29, 7, 14, 15, 2, 'com_contact.category.4', 'Uncategorised', '{"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[],"core.edit.own":[]}'),
(30, 19, 82, 83, 2, 'com_newsfeeds.category.5', 'Uncategorised', '{"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[],"core.edit.own":[]}'),
(31, 25, 98, 99, 2, 'com_weblinks.category.6', 'Uncategorised', '{"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[],"core.edit.own":[]}'),
(32, 24, 94, 95, 1, 'com_users.category.7', 'Uncategorised', '{"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(33, 1, 103, 104, 1, 'com_finder', 'com_finder', '{"core.admin":{"7":1},"core.manage":{"6":1}}'),
(34, 1, 105, 106, 1, 'com_joomlaupdate', 'com_joomlaupdate', '{"core.admin":[],"core.manage":[],"core.delete":[],"core.edit.state":[]}'),
(35, 1, 107, 108, 1, 'com_tags', 'com_tags', '{"core.admin":[],"core.manage":[],"core.manage":[],"core.delete":[],"core.edit.state":[]}'),
(36, 1, 109, 110, 1, 'com_contenthistory', 'com_contenthistory', '{}'),
(37, 1, 111, 112, 1, 'com_ajax', 'com_ajax', '{}'),
(38, 1, 113, 114, 1, 'com_postinstall', 'com_postinstall', '{}'),
(39, 18, 44, 45, 2, 'com_modules.module.1', 'Strumenti', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1}}'),
(40, 18, 46, 47, 2, 'com_modules.module.2', 'Login', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(41, 18, 48, 49, 2, 'com_modules.module.3', 'Popular Articles', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(42, 18, 50, 51, 2, 'com_modules.module.4', 'Recently Added Articles', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(43, 18, 52, 53, 2, 'com_modules.module.8', 'Toolbar', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(44, 18, 54, 55, 2, 'com_modules.module.9', 'Quick Icons', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(45, 18, 56, 57, 2, 'com_modules.module.10', 'Logged-in Users', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(46, 18, 58, 59, 2, 'com_modules.module.12', 'Admin Menu', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(47, 18, 60, 61, 2, 'com_modules.module.13', 'Admin Submenu', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(48, 18, 62, 63, 2, 'com_modules.module.14', 'User Status', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(49, 18, 64, 65, 2, 'com_modules.module.15', 'Title', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(50, 18, 66, 67, 2, 'com_modules.module.16', 'Login Form', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(51, 18, 68, 69, 2, 'com_modules.module.17', 'Breadcrumbs', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(52, 18, 70, 71, 2, 'com_modules.module.79', 'Multilanguage status', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(53, 18, 72, 73, 2, 'com_modules.module.86', 'Joomla Version', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(54, 27, 19, 20, 3, 'com_content.article.1', 'Gestione Eventi', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1}}'),
(55, 27, 21, 22, 3, 'com_content.article.2', 'Gestione Utenti', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1}}'),
(56, 18, 74, 75, 2, 'com_modules.module.87', 'Esci', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1}}'),
(57, 18, 76, 77, 2, 'com_modules.module.88', 'Social Share Buttons', ''),
(58, 1, 115, 116, 1, 'com_chronoforms5', 'com_chronoforms5', '{}'),
(59, 18, 78, 79, 2, 'com_modules.module.89', 'ChronoForms5', '');

-- --------------------------------------------------------

--
-- Struttura della tabella `zcp8_associations`
--

CREATE TABLE IF NOT EXISTS `zcp8_associations` (
  `id` int(11) NOT NULL COMMENT 'A reference to the associated item.',
  `context` varchar(50) NOT NULL COMMENT 'The context of the associated item.',
  `key` char(32) NOT NULL COMMENT 'The key for the association computed from an md5 on associated ids.',
  PRIMARY KEY (`context`,`id`),
  KEY `idx_key` (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `zcp8_banners`
--

CREATE TABLE IF NOT EXISTS `zcp8_banners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cid` int(11) NOT NULL DEFAULT '0',
  `type` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `imptotal` int(11) NOT NULL DEFAULT '0',
  `impmade` int(11) NOT NULL DEFAULT '0',
  `clicks` int(11) NOT NULL DEFAULT '0',
  `clickurl` varchar(200) NOT NULL DEFAULT '',
  `state` tinyint(3) NOT NULL DEFAULT '0',
  `catid` int(10) unsigned NOT NULL DEFAULT '0',
  `description` text NOT NULL,
  `custombannercode` varchar(2048) NOT NULL,
  `sticky` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `metakey` text NOT NULL,
  `params` text NOT NULL,
  `own_prefix` tinyint(1) NOT NULL DEFAULT '0',
  `metakey_prefix` varchar(255) NOT NULL DEFAULT '',
  `purchase_type` tinyint(4) NOT NULL DEFAULT '-1',
  `track_clicks` tinyint(4) NOT NULL DEFAULT '-1',
  `track_impressions` tinyint(4) NOT NULL DEFAULT '-1',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `reset` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `language` char(7) NOT NULL DEFAULT '',
  `created_by` int(10) unsigned NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) unsigned NOT NULL DEFAULT '0',
  `version` int(10) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `idx_state` (`state`),
  KEY `idx_own_prefix` (`own_prefix`),
  KEY `idx_metakey_prefix` (`metakey_prefix`),
  KEY `idx_banner_catid` (`catid`),
  KEY `idx_language` (`language`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struttura della tabella `zcp8_banner_clients`
--

CREATE TABLE IF NOT EXISTS `zcp8_banner_clients` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `contact` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `extrainfo` text NOT NULL,
  `state` tinyint(3) NOT NULL DEFAULT '0',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `metakey` text NOT NULL,
  `own_prefix` tinyint(4) NOT NULL DEFAULT '0',
  `metakey_prefix` varchar(255) NOT NULL DEFAULT '',
  `purchase_type` tinyint(4) NOT NULL DEFAULT '-1',
  `track_clicks` tinyint(4) NOT NULL DEFAULT '-1',
  `track_impressions` tinyint(4) NOT NULL DEFAULT '-1',
  PRIMARY KEY (`id`),
  KEY `idx_own_prefix` (`own_prefix`),
  KEY `idx_metakey_prefix` (`metakey_prefix`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struttura della tabella `zcp8_banner_tracks`
--

CREATE TABLE IF NOT EXISTS `zcp8_banner_tracks` (
  `track_date` datetime NOT NULL,
  `track_type` int(10) unsigned NOT NULL,
  `banner_id` int(10) unsigned NOT NULL,
  `count` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`track_date`,`track_type`,`banner_id`),
  KEY `idx_track_date` (`track_date`),
  KEY `idx_track_type` (`track_type`),
  KEY `idx_banner_id` (`banner_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `zcp8_categories`
--

CREATE TABLE IF NOT EXISTS `zcp8_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `asset_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'FK to the #__assets table.',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `lft` int(11) NOT NULL DEFAULT '0',
  `rgt` int(11) NOT NULL DEFAULT '0',
  `level` int(10) unsigned NOT NULL DEFAULT '0',
  `path` varchar(255) NOT NULL DEFAULT '',
  `extension` varchar(50) NOT NULL DEFAULT '',
  `title` varchar(255) NOT NULL,
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `note` varchar(255) NOT NULL DEFAULT '',
  `description` mediumtext NOT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(11) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `access` int(10) unsigned NOT NULL DEFAULT '0',
  `params` text NOT NULL,
  `metadesc` varchar(1024) NOT NULL COMMENT 'The meta description for the page.',
  `metakey` varchar(1024) NOT NULL COMMENT 'The meta keywords for the page.',
  `metadata` varchar(2048) NOT NULL COMMENT 'JSON encoded metadata properties.',
  `created_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `created_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `modified_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  `language` char(7) NOT NULL,
  `version` int(10) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `cat_idx` (`extension`,`published`,`access`),
  KEY `idx_access` (`access`),
  KEY `idx_checkout` (`checked_out`),
  KEY `idx_path` (`path`),
  KEY `idx_left_right` (`lft`,`rgt`),
  KEY `idx_alias` (`alias`),
  KEY `idx_language` (`language`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dump dei dati per la tabella `zcp8_categories`
--

INSERT INTO `zcp8_categories` (`id`, `asset_id`, `parent_id`, `lft`, `rgt`, `level`, `path`, `extension`, `title`, `alias`, `note`, `description`, `published`, `checked_out`, `checked_out_time`, `access`, `params`, `metadesc`, `metakey`, `metadata`, `created_user_id`, `created_time`, `modified_user_id`, `modified_time`, `hits`, `language`, `version`) VALUES
(1, 0, 0, 0, 13, 0, '', 'system', 'ROOT', 'root', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{}', '', '', '{}', 42, '2011-01-01 00:00:01', 0, '0000-00-00 00:00:00', 0, '*', 1),
(2, 27, 1, 1, 2, 1, 'uncategorised', 'com_content', 'Uncategorised', 'uncategorised', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{"category_layout":"","image":""}', '', '', '{"author":"","robots":""}', 42, '2011-01-01 00:00:01', 0, '0000-00-00 00:00:00', 0, '*', 1),
(3, 28, 1, 3, 4, 1, 'uncategorised', 'com_banners', 'Uncategorised', 'uncategorised', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{"category_layout":"","image":""}', '', '', '{"author":"","robots":""}', 42, '2011-01-01 00:00:01', 0, '0000-00-00 00:00:00', 0, '*', 1),
(4, 29, 1, 5, 6, 1, 'uncategorised', 'com_contact', 'Uncategorised', 'uncategorised', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{"category_layout":"","image":""}', '', '', '{"author":"","robots":""}', 42, '2011-01-01 00:00:01', 0, '0000-00-00 00:00:00', 0, '*', 1),
(5, 30, 1, 7, 8, 1, 'uncategorised', 'com_newsfeeds', 'Uncategorised', 'uncategorised', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{"category_layout":"","image":""}', '', '', '{"author":"","robots":""}', 42, '2011-01-01 00:00:01', 0, '0000-00-00 00:00:00', 0, '*', 1),
(6, 31, 1, 9, 10, 1, 'uncategorised', 'com_weblinks', 'Uncategorised', 'uncategorised', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{"category_layout":"","image":""}', '', '', '{"author":"","robots":""}', 42, '2011-01-01 00:00:01', 0, '0000-00-00 00:00:00', 0, '*', 1),
(7, 32, 1, 11, 12, 1, 'uncategorised', 'com_users', 'Uncategorised', 'uncategorised', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{"category_layout":"","image":""}', '', '', '{"author":"","robots":""}', 42, '2011-01-01 00:00:01', 0, '0000-00-00 00:00:00', 0, '*', 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `zcp8_chronoengine_chronoforms`
--

CREATE TABLE IF NOT EXISTS `zcp8_chronoengine_chronoforms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '',
  `params` text NOT NULL,
  `extras` longtext NOT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '1',
  `app` varchar(255) NOT NULL DEFAULT '',
  `form_type` int(1) NOT NULL DEFAULT '1',
  `content` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dump dei dati per la tabella `zcp8_chronoengine_chronoforms`
--

INSERT INTO `zcp8_chronoengine_chronoforms` (`id`, `title`, `params`, `extras`, `published`, `app`, `form_type`, `content`) VALUES
(1, 'event_update_old', '{"description":"Modifica degli attributi associati ad un evento","setup":"0","theme":"bootstrap3","tight_layout":"0","rtl_support":"0","labels_right_aligned":"0","labels_auto_width":"0","js_validation_language":""}', 'YTo0OntzOjY6ImZpZWxkcyI7YToyMDp7aTo4O2E6MjA6e3M6NDoibmFtZSI7czo2OiJzdGF0dXMiO3M6MjoiaWQiO3M6Njoic3RhdHVzIjtzOjc6Im9wdGlvbnMiO3M6Mjc6IjA9RGlzYWJpbGl0YXRvDQoxPUFiaWxpdGF0byI7czo1OiJlbXB0eSI7czowOiIiO3M6NjoidmFsdWVzIjtzOjA6IiI7czo1OiJsYWJlbCI7YToyOntzOjQ6InRleHQiO3M6NToiU3RhdG8iO3M6ODoicG9zaXRpb24iO3M6NDoibGVmdCI7fXM6ODoic3VibGFiZWwiO3M6OTI6IlNhcsOgIHBvc3NpYmlsZSBhY2NlZGVyZSBhbGwnZXZlbnRvIGxhdG8gY2xpZW50IHNvbG8gc2Ugw6ggc2VsZXppb25hdGEgbCdvcHppb25lICJBYmlsaXRhdG8iIjtzOjg6Im11bHRpcGxlIjtzOjE6IjAiO3M6NDoic2l6ZSI7czowOiIiO3M6NToiY2xhc3MiO3M6MDoiIjtzOjU6InRpdGxlIjtzOjA6IiI7czo1OiJzdHlsZSI7czowOiIiO3M6NjoicGFyYW1zIjtzOjA6IiI7czoxNjoiOmRhdGEtbG9hZC1zdGF0ZSI7czowOiIiO3M6MTM6IjpkYXRhLXRvb2x0aXAiO3M6MDoiIjtzOjQ6InR5cGUiO3M6ODoiZHJvcGRvd24iO3M6MTI6ImNvbnRhaW5lcl9pZCI7czoxOiIwIjtzOjEwOiJ2YWxpZGF0aW9uIjthOjM6e3M6ODoicmVxdWlyZWQiO3M6MDoiIjtzOjc6ImNvbmZpcm0iO3M6MDoiIjtzOjY6ImN1c3RvbSI7czowOiIiO31zOjEyOiJkeW5hbWljX2RhdGEiO2E6NDp7czo3OiJlbmFibGVkIjtzOjA6IiI7czo5OiJkYXRhX3BhdGgiO3M6MDoiIjtzOjk6InZhbHVlX2tleSI7czowOiIiO3M6ODoidGV4dF9rZXkiO3M6MDoiIjt9czo2OiJldmVudHMiO2E6MTp7aTowO2E6NTp7czo4OiJvcGVyYXRvciI7czoxOiI9IjtzOjU6InN0YXRlIjtzOjA6IiI7czo2OiJhY3Rpb24iO3M6MDoiIjtzOjY6InRhcmdldCI7czowOiIiO3M6Nzoib3B0aW9ucyI7czowOiIiO319fWk6MTthOjE4OntzOjQ6Im5hbWUiO3M6NToidGl0bGUiO3M6MjoiaWQiO3M6NToidGl0bGUiO3M6NToidmFsdWUiO3M6MDoiIjtzOjU6ImxhYmVsIjthOjI6e3M6NDoidGV4dCI7czoxNjoiTm9tZSBkZWxsJ2V2ZW50byI7czo4OiJwb3NpdGlvbiI7czo0OiJsZWZ0Ijt9czo4OiJzdWJsYWJlbCI7czozMDoiSW5zZXJpcmUgaWwgdGl0b2xvIGRlbGwnZXZlbnRvIjtzOjExOiJwbGFjZWhvbGRlciI7czoxODoiVGl0b2xvIGRlbGwnZXZlbnRvIjtzOjk6Im1heGxlbmd0aCI7czoyOiIzMCI7czo0OiJzaXplIjtzOjA6IiI7czo1OiJjbGFzcyI7czowOiIiO3M6NToidGl0bGUiO3M6MDoiIjtzOjU6InN0eWxlIjtzOjA6IiI7czoxNToiOmRhdGEtaW5wdXRtYXNrIjtzOjA6IiI7czo2OiJwYXJhbXMiO3M6MDoiIjtzOjE2OiI6ZGF0YS1sb2FkLXN0YXRlIjtzOjA6IiI7czoxMzoiOmRhdGEtdG9vbHRpcCI7czowOiIiO3M6NDoidHlwZSI7czo0OiJ0ZXh0IjtzOjEyOiJjb250YWluZXJfaWQiO3M6MToiMCI7czoxMDoidmFsaWRhdGlvbiI7YToxMzp7czo4OiJyZXF1aXJlZCI7czoxOiIxIjtzOjU6ImFscGhhIjtzOjA6IiI7czo4OiJhbHBoYW51bSI7czowOiIiO3M6NToiZGlnaXQiO3M6MDoiIjtzOjc6Im5vZGlnaXQiO3M6MDoiIjtzOjc6Im5vc3BhY2UiO3M6MDoiIjtzOjY6Im51bWJlciI7czowOiIiO3M6NToiZW1haWwiO3M6MDoiIjtzOjU6InBob25lIjtzOjA6IiI7czoxMToicGhvbmVfaW50ZXIiO3M6MDoiIjtzOjM6InVybCI7czowOiIiO3M6NzoiY29uZmlybSI7czowOiIiO3M6NjoiY3VzdG9tIjtzOjA6IiI7fX1pOjI7YToyMDp7czo0OiJuYW1lIjtzOjk6ImNhdGVnb3JpYSI7czoyOiJpZCI7czo5OiJjYXRlZ29yaWEiO3M6Nzoib3B0aW9ucyI7czowOiIiO3M6NToiZW1wdHkiO3M6MDoiIjtzOjY6InZhbHVlcyI7czowOiIiO3M6NToibGFiZWwiO2E6Mjp7czo0OiJ0ZXh0IjtzOjk6IkNhdGVnb3JpYSI7czo4OiJwb3NpdGlvbiI7czo0OiJsZWZ0Ijt9czo4OiJzdWJsYWJlbCI7czozNzoiQ2F0ZWdvcmlhIGRpIGFwcGFydGVuZW56YSBkZWxsJ2V2ZW50byI7czo4OiJtdWx0aXBsZSI7czoxOiIwIjtzOjQ6InNpemUiO3M6MDoiIjtzOjU6ImNsYXNzIjtzOjA6IiI7czo1OiJ0aXRsZSI7czowOiIiO3M6NToic3R5bGUiO3M6MDoiIjtzOjY6InBhcmFtcyI7czowOiIiO3M6MTY6IjpkYXRhLWxvYWQtc3RhdGUiO3M6MDoiIjtzOjEzOiI6ZGF0YS10b29sdGlwIjtzOjA6IiI7czo0OiJ0eXBlIjtzOjg6ImRyb3Bkb3duIjtzOjEyOiJjb250YWluZXJfaWQiO3M6MToiMCI7czoxMDoidmFsaWRhdGlvbiI7YTozOntzOjg6InJlcXVpcmVkIjtzOjA6IiI7czo3OiJjb25maXJtIjtzOjA6IiI7czo2OiJjdXN0b20iO3M6MDoiIjt9czoxMjoiZHluYW1pY19kYXRhIjthOjQ6e3M6NzoiZW5hYmxlZCI7czoxOiIxIjtzOjk6ImRhdGFfcGF0aCI7czoxNDoiY2F0ZWdvcmllc2xpc3QiO3M6OToidmFsdWVfa2V5IjtzOjI6ImlkIjtzOjg6InRleHRfa2V5IjtzOjQ6Im5hbWUiO31zOjY6ImV2ZW50cyI7YToxOntpOjA7YTo1OntzOjg6Im9wZXJhdG9yIjtzOjE6Ij0iO3M6NToic3RhdGUiO3M6MDoiIjtzOjY6ImFjdGlvbiI7czowOiIiO3M6NjoidGFyZ2V0IjtzOjA6IiI7czo3OiJvcHRpb25zIjtzOjA6IiI7fX19aToxNDthOjE4OntzOjQ6Im5hbWUiO3M6NDoiZGF0ZSI7czoyOiJpZCI7czo0OiJkYXRlIjtzOjU6InZhbHVlIjtzOjA6IiI7czo1OiJsYWJlbCI7YToyOntzOjQ6InRleHQiO3M6NDoiRGF0YSI7czo4OiJwb3NpdGlvbiI7czo0OiJsZWZ0Ijt9czo4OiJzdWJsYWJlbCI7czo2MToiSW5zZXJpcmUgbGEgZGF0YSBpbiBjdWkgc2kgc3ZvbGdlcsOgIGwnZXZlbnRvLiBlczogZ2ctbW0tYWFhYSI7czoxMToicGxhY2Vob2xkZXIiO3M6MDoiIjtzOjk6Im1heGxlbmd0aCI7czoyOiIzMCI7czo0OiJzaXplIjtzOjA6IiI7czo1OiJjbGFzcyI7czowOiIiO3M6NToidGl0bGUiO3M6MDoiIjtzOjU6InN0eWxlIjtzOjA6IiI7czoxNToiOmRhdGEtaW5wdXRtYXNrIjtzOjIyOiInYWxpYXMnIDogJ2RkLW1tLXl5eXknIjtzOjY6InBhcmFtcyI7czowOiIiO3M6MTY6IjpkYXRhLWxvYWQtc3RhdGUiO3M6MDoiIjtzOjEzOiI6ZGF0YS10b29sdGlwIjtzOjA6IiI7czo0OiJ0eXBlIjtzOjQ6InRleHQiO3M6MTI6ImNvbnRhaW5lcl9pZCI7czoxOiIwIjtzOjEwOiJ2YWxpZGF0aW9uIjthOjEzOntzOjg6InJlcXVpcmVkIjtzOjE6IjEiO3M6NToiYWxwaGEiO3M6MDoiIjtzOjg6ImFscGhhbnVtIjtzOjA6IiI7czo1OiJkaWdpdCI7czowOiIiO3M6Nzoibm9kaWdpdCI7czowOiIiO3M6Nzoibm9zcGFjZSI7czowOiIiO3M6NjoibnVtYmVyIjtzOjA6IiI7czo1OiJlbWFpbCI7czowOiIiO3M6NToicGhvbmUiO3M6MDoiIjtzOjExOiJwaG9uZV9pbnRlciI7czowOiIiO3M6MzoidXJsIjtzOjA6IiI7czo3OiJjb25maXJtIjtzOjA6IiI7czo2OiJjdXN0b20iO3M6MDoiIjt9fWk6MTU7YToxODp7czo0OiJuYW1lIjtzOjQ6ImhvdXIiO3M6MjoiaWQiO3M6NDoiaG91ciI7czo1OiJ2YWx1ZSI7czowOiIiO3M6NToibGFiZWwiO2E6Mjp7czo0OiJ0ZXh0IjtzOjEwOiJPcmEgaW5pemlvIjtzOjg6InBvc2l0aW9uIjtzOjQ6ImxlZnQiO31zOjg6InN1YmxhYmVsIjtzOjUxOiJJbnNlcmlyZSBsJ29yYXJpbyBkaSBpbml6aW8gZGVsbCdldmVudG8uIGVzOiBoaDptbSAiO3M6MTE6InBsYWNlaG9sZGVyIjtzOjA6IiI7czo5OiJtYXhsZW5ndGgiO3M6MjoiMzAiO3M6NDoic2l6ZSI7czowOiIiO3M6NToiY2xhc3MiO3M6MDoiIjtzOjU6InRpdGxlIjtzOjA6IiI7czo1OiJzdHlsZSI7czowOiIiO3M6MTU6IjpkYXRhLWlucHV0bWFzayI7czoxNzoiJ2FsaWFzJyA6ICdoaDptbSciO3M6NjoicGFyYW1zIjtzOjA6IiI7czoxNjoiOmRhdGEtbG9hZC1zdGF0ZSI7czowOiIiO3M6MTM6IjpkYXRhLXRvb2x0aXAiO3M6MDoiIjtzOjQ6InR5cGUiO3M6NDoidGV4dCI7czoxMjoiY29udGFpbmVyX2lkIjtzOjE6IjAiO3M6MTA6InZhbGlkYXRpb24iO2E6MTM6e3M6ODoicmVxdWlyZWQiO3M6MToiMSI7czo1OiJhbHBoYSI7czowOiIiO3M6ODoiYWxwaGFudW0iO3M6MDoiIjtzOjU6ImRpZ2l0IjtzOjA6IiI7czo3OiJub2RpZ2l0IjtzOjA6IiI7czo3OiJub3NwYWNlIjtzOjA6IiI7czo2OiJudW1iZXIiO3M6MDoiIjtzOjU6ImVtYWlsIjtzOjA6IiI7czo1OiJwaG9uZSI7czowOiIiO3M6MTE6InBob25lX2ludGVyIjtzOjA6IiI7czozOiJ1cmwiO3M6MDoiIjtzOjc6ImNvbmZpcm0iO3M6MDoiIjtzOjY6ImN1c3RvbSI7czowOiIiO319aToxNzthOjIwOntzOjQ6Im5hbWUiO3M6Mzoic2V4IjtzOjI6ImlkIjtzOjM6InNleCI7czo3OiJvcHRpb25zIjtzOjQ1OiIwPVNvbG8gVW9taW5pDQoxPVNvbG8gRG9ubmUNCjM9RG9ubmUgZSBVb21pbmkiO3M6NToiZW1wdHkiO3M6MDoiIjtzOjY6InZhbHVlcyI7czowOiIiO3M6NToibGFiZWwiO2E6Mjp7czo0OiJ0ZXh0IjtzOjU6IlNlc3NvIjtzOjg6InBvc2l0aW9uIjtzOjQ6ImxlZnQiO31zOjg6InN1YmxhYmVsIjtzOjM5OiJHZW5lcmUgcGVyIGN1aSB1biBldmVudG8gw6ggcGlhbmlmaWNhdG8iO3M6ODoibXVsdGlwbGUiO3M6MToiMCI7czo0OiJzaXplIjtzOjA6IiI7czo1OiJjbGFzcyI7czowOiIiO3M6NToidGl0bGUiO3M6MDoiIjtzOjU6InN0eWxlIjtzOjA6IiI7czo2OiJwYXJhbXMiO3M6MDoiIjtzOjE2OiI6ZGF0YS1sb2FkLXN0YXRlIjtzOjA6IiI7czoxMzoiOmRhdGEtdG9vbHRpcCI7czowOiIiO3M6NDoidHlwZSI7czo4OiJkcm9wZG93biI7czoxMjoiY29udGFpbmVyX2lkIjtzOjE6IjAiO3M6MTA6InZhbGlkYXRpb24iO2E6Mzp7czo4OiJyZXF1aXJlZCI7czowOiIiO3M6NzoiY29uZmlybSI7czowOiIiO3M6NjoiY3VzdG9tIjtzOjA6IiI7fXM6MTI6ImR5bmFtaWNfZGF0YSI7YTo0OntzOjc6ImVuYWJsZWQiO3M6MDoiIjtzOjk6ImRhdGFfcGF0aCI7czowOiIiO3M6OToidmFsdWVfa2V5IjtzOjA6IiI7czo4OiJ0ZXh0X2tleSI7czowOiIiO31zOjY6ImV2ZW50cyI7YToxOntpOjA7YTo1OntzOjg6Im9wZXJhdG9yIjtzOjE6Ij0iO3M6NToic3RhdGUiO3M6MDoiIjtzOjY6ImFjdGlvbiI7czowOiIiO3M6NjoidGFyZ2V0IjtzOjA6IiI7czo3OiJvcHRpb25zIjtzOjA6IiI7fX19aTo3O2E6MjA6e3M6NDoibmFtZSI7czo1OiJzY29wZSI7czoyOiJpZCI7czo1OiJzY29wZSI7czo3OiJvcHRpb25zIjtzOjM3OiIwPVB1YmJsaWNvDQoxPVByaXZhdG8NCjI9RnJpZW5kcyBPbmx5IjtzOjU6ImVtcHR5IjtzOjA6IiI7czo2OiJ2YWx1ZXMiO3M6MDoiIjtzOjU6ImxhYmVsIjthOjI6e3M6NDoidGV4dCI7czo3OiJBY2Nlc3NvIjtzOjg6InBvc2l0aW9uIjtzOjQ6ImxlZnQiO31zOjg6InN1YmxhYmVsIjtzOjM0OiJTZWxlemlvbmFyZSBsYSB0aXBvbG9naWEgZGkgZXZlbnRvIjtzOjg6Im11bHRpcGxlIjtzOjE6IjAiO3M6NDoic2l6ZSI7czowOiIiO3M6NToiY2xhc3MiO3M6MDoiIjtzOjU6InRpdGxlIjtzOjA6IiI7czo1OiJzdHlsZSI7czowOiIiO3M6NjoicGFyYW1zIjtzOjA6IiI7czoxNjoiOmRhdGEtbG9hZC1zdGF0ZSI7czowOiIiO3M6MTM6IjpkYXRhLXRvb2x0aXAiO3M6MDoiIjtzOjQ6InR5cGUiO3M6ODoiZHJvcGRvd24iO3M6MTI6ImNvbnRhaW5lcl9pZCI7czoxOiIwIjtzOjEwOiJ2YWxpZGF0aW9uIjthOjM6e3M6ODoicmVxdWlyZWQiO3M6MDoiIjtzOjc6ImNvbmZpcm0iO3M6MDoiIjtzOjY6ImN1c3RvbSI7czowOiIiO31zOjEyOiJkeW5hbWljX2RhdGEiO2E6NDp7czo3OiJlbmFibGVkIjtzOjA6IiI7czo5OiJkYXRhX3BhdGgiO3M6MDoiIjtzOjk6InZhbHVlX2tleSI7czowOiIiO3M6ODoidGV4dF9rZXkiO3M6MDoiIjt9czo2OiJldmVudHMiO2E6MTp7aTowO2E6NTp7czo4OiJvcGVyYXRvciI7czoxOiI9IjtzOjU6InN0YXRlIjtzOjA6IiI7czo2OiJhY3Rpb24iO3M6MDoiIjtzOjY6InRhcmdldCI7czowOiIiO3M6Nzoib3B0aW9ucyI7czowOiIiO319fWk6MzthOjE4OntzOjQ6Im5hbWUiO3M6MTE6ImRlc2NyaXB0aW9uIjtzOjI6ImlkIjtzOjExOiJkZXNjcmlwdGlvbiI7czo1OiJ2YWx1ZSI7czowOiIiO3M6NToibGFiZWwiO2E6Mjp7czo0OiJ0ZXh0IjtzOjExOiJEZXNjcml6aW9uZSI7czo4OiJwb3NpdGlvbiI7czo0OiJsZWZ0Ijt9czo4OiJzdWJsYWJlbCI7czozMjoiRGVzY3JpemlvbmUgaW5zZXJpdGEgZGFsIHBsYW5uZXIiO3M6MTE6InBsYWNlaG9sZGVyIjtzOjIzOiJEZXNjcml6aW9uZSBkZWxsJ2V2ZW50byI7czo0OiJyb3dzIjtzOjE6IjUiO3M6NDoiY29scyI7czoyOiI0MCI7czo1OiJjbGFzcyI7czowOiIiO3M6NToidGl0bGUiO3M6MDoiIjtzOjU6InN0eWxlIjtzOjA6IiI7czoxMzoiOmRhdGEtd3lzaXd5ZyI7czoxOiIwIjtzOjY6InBhcmFtcyI7czowOiIiO3M6MTY6IjpkYXRhLWxvYWQtc3RhdGUiO3M6MDoiIjtzOjEzOiI6ZGF0YS10b29sdGlwIjtzOjA6IiI7czo0OiJ0eXBlIjtzOjg6InRleHRhcmVhIjtzOjEyOiJjb250YWluZXJfaWQiO3M6MToiMCI7czoxMDoidmFsaWRhdGlvbiI7YTo4OntzOjg6InJlcXVpcmVkIjtzOjE6IjEiO3M6NToiYWxwaGEiO3M6MDoiIjtzOjg6ImFscGhhbnVtIjtzOjA6IiI7czo1OiJkaWdpdCI7czowOiIiO3M6Nzoibm9kaWdpdCI7czowOiIiO3M6NjoibnVtYmVyIjtzOjA6IiI7czo3OiJjb25maXJtIjtzOjA6IiI7czo2OiJjdXN0b20iO3M6MDoiIjt9fWk6NDthOjE2OntzOjQ6Im5hbWUiO3M6NToiaW1hZ2UiO3M6MjoiaWQiO3M6NToiaW1hZ2UiO3M6NToibGFiZWwiO2E6Mjp7czo0OiJ0ZXh0IjtzOjk6IkNvcGVydGluYSI7czo4OiJwb3NpdGlvbiI7czo0OiJsZWZ0Ijt9czo4OiJzdWJsYWJlbCI7czoxMzM6IkltbWFnaW5lIGRpIGNvcGVydGluYTxicj5UaXBpIGRpIGZpbGU6IGpwZWcsIGpwZywgZ2lmLCBwbmc8YnI+TGFyZ2hlenphIGUgQWx0ZXp6YSBlc2F0dGU6IDcyMHB4IHggNDI2cHg8YnI+RGltZW5zaW9uaSBtYXNzaW1lOiA1MDAgS0IiO3M6NToiY2xhc3MiO3M6MDoiIjtzOjU6InRpdGxlIjtzOjA6IiI7czo1OiJzdHlsZSI7czowOiIiO3M6OToiOm11bHRpcGxlIjtzOjE6IjAiO3M6NjoicGFyYW1zIjtzOjA6IiI7czoxNjoiOmRhdGEtbG9hZC1zdGF0ZSI7czowOiIiO3M6MTM6IjpkYXRhLXRvb2x0aXAiO3M6MDoiIjtzOjU6Imdob3N0IjtzOjE6IjAiO3M6MTE6Imdob3N0X3ZhbHVlIjtzOjA6IiI7czo0OiJ0eXBlIjtzOjQ6ImZpbGUiO3M6MTI6ImNvbnRhaW5lcl9pZCI7czoxOiIwIjtzOjEwOiJ2YWxpZGF0aW9uIjthOjM6e3M6ODoicmVxdWlyZWQiO3M6MDoiIjtzOjU6ImltYWdlIjtzOjA6IiI7czo2OiJjdXN0b20iO3M6MDoiIjt9fWk6NTthOjE4OntzOjQ6Im5hbWUiO3M6NDoidG93biI7czoyOiJpZCI7czo0OiJ0b3duIjtzOjU6InZhbHVlIjtzOjA6IiI7czo1OiJsYWJlbCI7YToyOntzOjQ6InRleHQiO3M6NjoiQ2l0dMOgIjtzOjg6InBvc2l0aW9uIjtzOjQ6ImxlZnQiO31zOjg6InN1YmxhYmVsIjtzOjU4OiJJbnNlcmlyZSBpbCBub21lIGRlbGxhIGNpdHTDoCBpbiBjdWkgc2kgc3ZvbGdlcsOgIGwnZXZlbnRvIjtzOjExOiJwbGFjZWhvbGRlciI7czo2OiJDaXR0w6AiO3M6OToibWF4bGVuZ3RoIjtzOjI6IjQwIjtzOjQ6InNpemUiO3M6MDoiIjtzOjU6ImNsYXNzIjtzOjA6IiI7czo1OiJ0aXRsZSI7czowOiIiO3M6NToic3R5bGUiO3M6MDoiIjtzOjE1OiI6ZGF0YS1pbnB1dG1hc2siO3M6MDoiIjtzOjY6InBhcmFtcyI7czowOiIiO3M6MTY6IjpkYXRhLWxvYWQtc3RhdGUiO3M6MDoiIjtzOjEzOiI6ZGF0YS10b29sdGlwIjtzOjA6IiI7czo0OiJ0eXBlIjtzOjQ6InRleHQiO3M6MTI6ImNvbnRhaW5lcl9pZCI7czoxOiIwIjtzOjEwOiJ2YWxpZGF0aW9uIjthOjEzOntzOjg6InJlcXVpcmVkIjtzOjE6IjEiO3M6NToiYWxwaGEiO3M6MDoiIjtzOjg6ImFscGhhbnVtIjtzOjA6IiI7czo1OiJkaWdpdCI7czowOiIiO3M6Nzoibm9kaWdpdCI7czowOiIiO3M6Nzoibm9zcGFjZSI7czowOiIiO3M6NjoibnVtYmVyIjtzOjA6IiI7czo1OiJlbWFpbCI7czowOiIiO3M6NToicGhvbmUiO3M6MDoiIjtzOjExOiJwaG9uZV9pbnRlciI7czowOiIiO3M6MzoidXJsIjtzOjA6IiI7czo3OiJjb25maXJtIjtzOjA6IiI7czo2OiJjdXN0b20iO3M6MDoiIjt9fWk6OTthOjE4OntzOjQ6Im5hbWUiO3M6NzoiYWRkcmVzcyI7czoyOiJpZCI7czo3OiJhZGRyZXNzIjtzOjU6InZhbHVlIjtzOjA6IiI7czo1OiJsYWJlbCI7YToyOntzOjQ6InRleHQiO3M6OToiSW5kaXJpenpvIjtzOjg6InBvc2l0aW9uIjtzOjQ6ImxlZnQiO31zOjg6InN1YmxhYmVsIjtzOjYyOiJJbmRpcml6em8gZSBudW1lcm8gY2l2aWNvIGRlbCBsdW9nbyBkb3ZlIHNpIHN2b2xnZXLDoCBsJ2V2ZW50byI7czoxMToicGxhY2Vob2xkZXIiO3M6MDoiIjtzOjk6Im1heGxlbmd0aCI7czowOiIiO3M6NDoic2l6ZSI7czowOiIiO3M6NToiY2xhc3MiO3M6MDoiIjtzOjU6InRpdGxlIjtzOjA6IiI7czo1OiJzdHlsZSI7czowOiIiO3M6MTU6IjpkYXRhLWlucHV0bWFzayI7czowOiIiO3M6NjoicGFyYW1zIjtzOjA6IiI7czoxNjoiOmRhdGEtbG9hZC1zdGF0ZSI7czowOiIiO3M6MTM6IjpkYXRhLXRvb2x0aXAiO3M6MDoiIjtzOjQ6InR5cGUiO3M6NDoidGV4dCI7czoxMjoiY29udGFpbmVyX2lkIjtzOjE6IjAiO3M6MTA6InZhbGlkYXRpb24iO2E6MTM6e3M6ODoicmVxdWlyZWQiO3M6MDoiIjtzOjU6ImFscGhhIjtzOjA6IiI7czo4OiJhbHBoYW51bSI7czowOiIiO3M6NToiZGlnaXQiO3M6MDoiIjtzOjc6Im5vZGlnaXQiO3M6MDoiIjtzOjc6Im5vc3BhY2UiO3M6MDoiIjtzOjY6Im51bWJlciI7czowOiIiO3M6NToiZW1haWwiO3M6MDoiIjtzOjU6InBob25lIjtzOjA6IiI7czoxMToicGhvbmVfaW50ZXIiO3M6MDoiIjtzOjM6InVybCI7czowOiIiO3M6NzoiY29uZmlybSI7czowOiIiO3M6NjoiY3VzdG9tIjtzOjA6IiI7fX1pOjE5O2E6Nzp7czo1OiJsYWJlbCI7czoxMToiR29vZ2xlIE1hcHMiO3M6OToicHVyZV9jb2RlIjtzOjE6IjEiO3M6NDoiY29kZSI7czozOTk6IjxzY3JpcHQ+DQpnb29nbGUubWFwcy5ldmVudC5hZGREb21MaXN0ZW5lcih3aW5kb3csICdsb2FkJywgaW5pdGlhbGl6ZV9tYXBwYSk7DQo8L3NjcmlwdD4NCjxzdHlsZT4NCiNtYXAtY2FudmFzeyANCiAgaGVpZ2h0OiA0MDBweDsNCiAgd2lkdGg6IDk1MHB4Ow0KICAvKiBtYXJnaW4tYm90dG9tOiAzMHB4OyAqLw0KfQ0KPC9zdHlsZT4NCjxkaXYgaWQ9Im1hcC1jYW52YXMiPjwvZGl2Pg0KPHNwYW4gY2xhc3M9ImhlbHAtYmxvY2siPlBvc2l6aW9uYXJzaSBjb24gaWwgY3Vyc29yZSBkZWwgbW91c2Ugc3VsIG1hcmNhdG9yZSBlIHNwb3N0YXJsbyBzdWxsYSBtYXBwYSB0ZW5lbmRvIHByZW11dG8gaWwgdGFzdG8gc2luaXN0cm8gcGVyIG1vZGlmaWNhcmUgbGEgcG9zaXppb25lLjwvc3Bhbj48YnI+PGJyPiI7czo0OiJuYW1lIjtzOjY6ImN1c3RvbSI7czoxMToicmVuZGVyX3R5cGUiO3M6NjoiY3VzdG9tIjtzOjQ6InR5cGUiO3M6NjoiY3VzdG9tIjtzOjEyOiJjb250YWluZXJfaWQiO3M6MToiMCI7fWk6MTA7YToxODp7czo0OiJuYW1lIjtzOjY6ImFnZW1pbiI7czoyOiJpZCI7czo2OiJhZ2VtaW4iO3M6NToidmFsdWUiO3M6MDoiIjtzOjU6ImxhYmVsIjthOjI6e3M6NDoidGV4dCI7czoxMToiRXTDoCBtaW5pbWEiO3M6ODoicG9zaXRpb24iO3M6NDoibGVmdCI7fXM6ODoic3VibGFiZWwiO3M6Nzk6IkV0w6AgbWluaW1hIHBlciBwb3RlciBwYXJ0ZWNpcGFyZSBhbGwnZXZlbnRvIChOdW1lcm8gaW50ZXJvIHBvc2l0aXZvIHJpY2hpZXN0bykiO3M6MTE6InBsYWNlaG9sZGVyIjtzOjI6IjIwIjtzOjk6Im1heGxlbmd0aCI7czowOiIiO3M6NDoic2l6ZSI7czowOiIiO3M6NToiY2xhc3MiO3M6MDoiIjtzOjU6InRpdGxlIjtzOjA6IiI7czo1OiJzdHlsZSI7czowOiIiO3M6MTU6IjpkYXRhLWlucHV0bWFzayI7czoxOToiJ2FsaWFzJyA6ICdpbnRlZ2VyJyI7czo2OiJwYXJhbXMiO3M6MDoiIjtzOjE2OiI6ZGF0YS1sb2FkLXN0YXRlIjtzOjA6IiI7czoxMzoiOmRhdGEtdG9vbHRpcCI7czowOiIiO3M6NDoidHlwZSI7czo0OiJ0ZXh0IjtzOjEyOiJjb250YWluZXJfaWQiO3M6MToiMCI7czoxMDoidmFsaWRhdGlvbiI7YToxMzp7czo4OiJyZXF1aXJlZCI7czoxOiIxIjtzOjU6ImFscGhhIjtzOjA6IiI7czo4OiJhbHBoYW51bSI7czowOiIiO3M6NToiZGlnaXQiO3M6MDoiIjtzOjc6Im5vZGlnaXQiO3M6MDoiIjtzOjc6Im5vc3BhY2UiO3M6MDoiIjtzOjY6Im51bWJlciI7czoxOiIxIjtzOjU6ImVtYWlsIjtzOjA6IiI7czo1OiJwaG9uZSI7czowOiIiO3M6MTE6InBob25lX2ludGVyIjtzOjA6IiI7czozOiJ1cmwiO3M6MDoiIjtzOjc6ImNvbmZpcm0iO3M6MDoiIjtzOjY6ImN1c3RvbSI7czowOiIiO319aToxMTthOjE4OntzOjQ6Im5hbWUiO3M6NjoiYWdlbWF4IjtzOjI6ImlkIjtzOjY6ImFnZW1heCI7czo1OiJ2YWx1ZSI7czowOiIiO3M6NToibGFiZWwiO2E6Mjp7czo0OiJ0ZXh0IjtzOjEyOiJFdMOgIG1hc3NpbWEiO3M6ODoicG9zaXRpb24iO3M6NDoibGVmdCI7fXM6ODoic3VibGFiZWwiO3M6ODA6IkV0w6AgbWFzc2ltYSBwZXIgcG90ZXIgcGFydGVjaXBhcmUgYWxsJ2V2ZW50byAoTnVtZXJvIGludGVybyBwb3NpdGl2byByaWNoaWVzdG8pIjtzOjExOiJwbGFjZWhvbGRlciI7czoyOiI0MCI7czo5OiJtYXhsZW5ndGgiO3M6MDoiIjtzOjQ6InNpemUiO3M6MDoiIjtzOjU6ImNsYXNzIjtzOjA6IiI7czo1OiJ0aXRsZSI7czowOiIiO3M6NToic3R5bGUiO3M6MDoiIjtzOjE1OiI6ZGF0YS1pbnB1dG1hc2siO3M6MTk6IidhbGlhcycgOiAnaW50ZWdlciciO3M6NjoicGFyYW1zIjtzOjA6IiI7czoxNjoiOmRhdGEtbG9hZC1zdGF0ZSI7czowOiIiO3M6MTM6IjpkYXRhLXRvb2x0aXAiO3M6MDoiIjtzOjQ6InR5cGUiO3M6NDoidGV4dCI7czoxMjoiY29udGFpbmVyX2lkIjtzOjE6IjAiO3M6MTA6InZhbGlkYXRpb24iO2E6MTM6e3M6ODoicmVxdWlyZWQiO3M6MToiMSI7czo1OiJhbHBoYSI7czowOiIiO3M6ODoiYWxwaGFudW0iO3M6MDoiIjtzOjU6ImRpZ2l0IjtzOjA6IiI7czo3OiJub2RpZ2l0IjtzOjA6IiI7czo3OiJub3NwYWNlIjtzOjA6IiI7czo2OiJudW1iZXIiO3M6MToiMSI7czo1OiJlbWFpbCI7czowOiIiO3M6NToicGhvbmUiO3M6MDoiIjtzOjExOiJwaG9uZV9pbnRlciI7czowOiIiO3M6MzoidXJsIjtzOjA6IiI7czo3OiJjb25maXJtIjtzOjA6IiI7czo2OiJjdXN0b20iO3M6MDoiIjt9fWk6MTI7YToxODp7czo0OiJuYW1lIjtzOjEwOiJzaXRjdXJyZW50IjtzOjI6ImlkIjtzOjEwOiJzaXRjdXJyZW50IjtzOjU6InZhbHVlIjtzOjA6IiI7czo1OiJsYWJlbCI7YToyOntzOjQ6InRleHQiO3M6MTI6IlBhcnRlY2lwYW50aSI7czo4OiJwb3NpdGlvbiI7czo0OiJsZWZ0Ijt9czo4OiJzdWJsYWJlbCI7czo1OToiTnVtZXJvIGRpIHBhcnRlY2lwYW50aSBjaGUgaGFubm8gZWZmZXR0dWF0byBsYSBwcmVub3RhemlvbmUiO3M6MTE6InBsYWNlaG9sZGVyIjtzOjI6IjE4IjtzOjk6Im1heGxlbmd0aCI7czowOiIiO3M6NDoic2l6ZSI7czowOiIiO3M6NToiY2xhc3MiO3M6MDoiIjtzOjU6InRpdGxlIjtzOjA6IiI7czo1OiJzdHlsZSI7czowOiIiO3M6MTU6IjpkYXRhLWlucHV0bWFzayI7czoxOToiJ2FsaWFzJyA6ICdpbnRlZ2VyJyI7czo2OiJwYXJhbXMiO3M6MDoiIjtzOjE2OiI6ZGF0YS1sb2FkLXN0YXRlIjtzOjg6ImRpc2FibGVkIjtzOjEzOiI6ZGF0YS10b29sdGlwIjtzOjA6IiI7czo0OiJ0eXBlIjtzOjQ6InRleHQiO3M6MTI6ImNvbnRhaW5lcl9pZCI7czoxOiIwIjtzOjEwOiJ2YWxpZGF0aW9uIjthOjEzOntzOjg6InJlcXVpcmVkIjtzOjE6IjEiO3M6NToiYWxwaGEiO3M6MDoiIjtzOjg6ImFscGhhbnVtIjtzOjA6IiI7czo1OiJkaWdpdCI7czowOiIiO3M6Nzoibm9kaWdpdCI7czowOiIiO3M6Nzoibm9zcGFjZSI7czowOiIiO3M6NjoibnVtYmVyIjtzOjE6IjEiO3M6NToiZW1haWwiO3M6MDoiIjtzOjU6InBob25lIjtzOjA6IiI7czoxMToicGhvbmVfaW50ZXIiO3M6MDoiIjtzOjM6InVybCI7czowOiIiO3M6NzoiY29uZmlybSI7czowOiIiO3M6NjoiY3VzdG9tIjtzOjA6IiI7fX1pOjEzO2E6MTg6e3M6NDoibmFtZSI7czo4OiJzaXR0b3RhbCI7czoyOiJpZCI7czo4OiJzaXR0b3RhbCI7czo1OiJ2YWx1ZSI7czowOiIiO3M6NToibGFiZWwiO2E6Mjp7czo0OiJ0ZXh0IjtzOjE3OiJQb3N0aSBkaXNwb25pYmlsaSI7czo4OiJwb3NpdGlvbiI7czo0OiJsZWZ0Ijt9czo4OiJzdWJsYWJlbCI7czoyNzoiUG9zdGkgZGlzcG9uaWJpbGkgaW4gdG90YWxlIjtzOjExOiJwbGFjZWhvbGRlciI7czoyOiI1MCI7czo5OiJtYXhsZW5ndGgiO3M6MDoiIjtzOjQ6InNpemUiO3M6MDoiIjtzOjU6ImNsYXNzIjtzOjA6IiI7czo1OiJ0aXRsZSI7czowOiIiO3M6NToic3R5bGUiO3M6MDoiIjtzOjE1OiI6ZGF0YS1pbnB1dG1hc2siO3M6MTk6IidhbGlhcycgOiAnaW50ZWdlciciO3M6NjoicGFyYW1zIjtzOjA6IiI7czoxNjoiOmRhdGEtbG9hZC1zdGF0ZSI7czowOiIiO3M6MTM6IjpkYXRhLXRvb2x0aXAiO3M6MDoiIjtzOjQ6InR5cGUiO3M6NDoidGV4dCI7czoxMjoiY29udGFpbmVyX2lkIjtzOjE6IjAiO3M6MTA6InZhbGlkYXRpb24iO2E6MTM6e3M6ODoicmVxdWlyZWQiO3M6MToiMSI7czo1OiJhbHBoYSI7czowOiIiO3M6ODoiYWxwaGFudW0iO3M6MDoiIjtzOjU6ImRpZ2l0IjtzOjA6IiI7czo3OiJub2RpZ2l0IjtzOjA6IiI7czo3OiJub3NwYWNlIjtzOjA6IiI7czo2OiJudW1iZXIiO3M6MToiMSI7czo1OiJlbWFpbCI7czowOiIiO3M6NToicGhvbmUiO3M6MDoiIjtzOjExOiJwaG9uZV9pbnRlciI7czowOiIiO3M6MzoidXJsIjtzOjA6IiI7czo3OiJjb25maXJtIjtzOjA6IiI7czo2OiJjdXN0b20iO3M6MDoiIjt9fWk6MTY7YToxMDp7czoxMToicmVuZGVyX3R5cGUiO3M6Njoic3VibWl0IjtzOjQ6Im5hbWUiO3M6ODoiYnV0dG9uMTYiO3M6MjoiaWQiO3M6ODoiYnV0dG9uMTYiO3M6NDoidHlwZSI7czo2OiJzdWJtaXQiO3M6NToidmFsdWUiO3M6NToiU2FsdmEiO3M6NToiY2xhc3MiO3M6MTU6ImJ0biBidG4tZGVmYXVsdCI7czo1OiJzdHlsZSI7czowOiIiO3M6NjoicGFyYW1zIjtzOjA6IiI7czoxNjoiOmRhdGEtbG9hZC1zdGF0ZSI7czowOiIiO3M6MTI6ImNvbnRhaW5lcl9pZCI7czoxOiIwIjt9aToxODthOjc6e3M6NDoibmFtZSI7czoxNToic2l0Y3VycmVudGdob3N0IjtzOjI6ImlkIjtzOjE1OiJzaXRjdXJyZW50Z2hvc3QiO3M6NToidmFsdWUiO3M6MDoiIjtzOjU6ImxhYmVsIjtzOjEyOiJIaWRkZW4gTGFiZWwiO3M6NjoicGFyYW1zIjtzOjA6IiI7czo0OiJ0eXBlIjtzOjY6ImhpZGRlbiI7czoxMjoiY29udGFpbmVyX2lkIjtzOjE6IjAiO31pOjIwO2E6Nzp7czo0OiJuYW1lIjtzOjg6ImxhdGl0dWRlIjtzOjI6ImlkIjtzOjg6ImxhdGl0dWRlIjtzOjU6InZhbHVlIjtzOjA6IiI7czo1OiJsYWJlbCI7czoxMjoiSGlkZGVuIExhYmVsIjtzOjY6InBhcmFtcyI7czowOiIiO3M6NDoidHlwZSI7czo2OiJoaWRkZW4iO3M6MTI6ImNvbnRhaW5lcl9pZCI7czoxOiIwIjt9aToyMTthOjc6e3M6NDoibmFtZSI7czo5OiJsb25naXR1ZGUiO3M6MjoiaWQiO3M6OToibG9uZ2l0dWRlIjtzOjU6InZhbHVlIjtzOjA6IiI7czo1OiJsYWJlbCI7czoxMjoiSGlkZGVuIExhYmVsIjtzOjY6InBhcmFtcyI7czowOiIiO3M6NDoidHlwZSI7czo2OiJoaWRkZW4iO3M6MTI6ImNvbnRhaW5lcl9pZCI7czoxOiIwIjt9fXM6MzoiRE5BIjthOjI6e3M6NDoibG9hZCI7YTo0OntzOjk6ImRiX3JlYWRfMSI7YToyOntzOjU6ImZvdW5kIjthOjE6e3M6MTM6ImN1c3RvbV9jb2RlXzQiO3M6MDoiIjt9czo5OiJub3RfZm91bmQiO3M6MDoiIjt9czoxMzoiY3VzdG9tX2NvZGVfMiI7czowOiIiO3M6NjoiaHRtbF8wIjtzOjA6IiI7czoxMzoiY3VzdG9tX2NvZGVfNSI7czowOiIiO31zOjY6InN1Ym1pdCI7YToxOntzOjEzOiJjdXN0b21fY29kZV8zIjtzOjA6IiI7fX1zOjE0OiJhY3Rpb25zX2NvbmZpZyI7YTo2OntpOjQ7YToyOntzOjEyOiJhY3Rpb25fbGFiZWwiO3M6NTc6Ik1vZGVsbGF6aW9uZSBkZWxsZSBvcHRpb25zIGNvbiBjYXRlZ29yaWUgZSBzb3R0b2NhdGVnb3JpZSI7czo3OiJjb250ZW50IjtzOjEzMjoiPD9waHANCiRlbSA9IG5ldyBFdmVudHNNb2RlbCgpOw0KJGVtLT5jYXRlZ29yeWxpc3QoJGZvcm0tPmRhdGFbJ2NhdGVnb3JpZXNsaXN0J10pOw0KLy92YXJfZHVtcCgkZm9ybS0+ZGF0YVsnY2F0ZWdvcmllc2xpc3QnXSk7DQo/Pg0KIjt9aToxO2E6MTk6e3M6MTI6ImFjdGlvbl9sYWJlbCI7czoyNzoiQ2FyaWNhbWVudG8gZGVsbGUgY2F0ZWdvcmllIjtzOjc6ImVuYWJsZWQiO3M6MToiMSI7czo5OiJ0YWJsZW5hbWUiO3M6MTA6ImNhdGVnb3JpZXMiO3M6MTA6Im11bHRpX3JlYWQiO3M6MToiMSI7czoxODoibG9hZF91bmRlcl9tb2RlbGlkIjtzOjE6IjEiO3M6ODoibW9kZWxfaWQiO3M6MTQ6ImNhdGVnb3JpZXNsaXN0IjtzOjY6ImZpZWxkcyI7czozMjoiaWQsbmFtZSxpZHN1cGVyY2F0ZWdvcnksbGFuZ3VhZ2UiO3M6NToib3JkZXIiO3M6MjoiaWQiO3M6NToiZ3JvdXAiO3M6MDoiIjtzOjEwOiJjb25kaXRpb25zIjtzOjA6IiI7czoxNjoiZW5hYmxlX3JlbGF0aW9ucyI7czoxOiIwIjtzOjk6InJlbGF0aW9ucyI7YToxOntpOjA7YTo0OntzOjU6Im1vZGVsIjtzOjA6IiI7czo5OiJ0YWJsZW5hbWUiO3M6MDoiIjtzOjQ6InR5cGUiO3M6NjoiaGFzT25lIjtzOjQ6ImZrZXkiO3M6MDoiIjt9fXM6MTA6Im5kYl9lbmFibGUiO3M6MToiMCI7czoxMDoibmRiX2RyaXZlciI7czo1OiJteXNxbCI7czo4OiJuZGJfaG9zdCI7czo5OiJsb2NhbGhvc3QiO3M6MTI6Im5kYl9kYXRhYmFzZSI7czowOiIiO3M6ODoibmRiX3VzZXIiO3M6MDoiIjtzOjEyOiJuZGJfcGFzc3dvcmQiO3M6MDoiIjtzOjEwOiJuZGJfcHJlZml4IjtzOjQ6Impvc18iO31pOjI7YToyOntzOjEyOiJhY3Rpb25fbGFiZWwiO3M6MjA6IkNhcmljYW1lbnRvIGRlaSBkYXRpIjtzOjc6ImNvbnRlbnQiO3M6MTA0OiI8P3BocA0KJGVjID1UcmF2ZWw6OmdldEluc3RhbmNlKCktPmdldCgnZXZlbnRDb250cm9sbGVyJyk7DQokZWMtPmluaXRHcHMoKTsNCiRlYy0+aW5pdEZvcm0oJGZvcm0pOw0KPz4NCiI7fWk6MDthOjExOntzOjQ6InBhZ2UiO3M6MToiMSI7czoxMjoic3VibWl0X2V2ZW50IjtzOjY6InN1Ym1pdCI7czoxMToiZm9ybV9tZXRob2QiO3M6NDoiZmlsZSI7czoxMDoiYWN0aW9uX3VybCI7czowOiIiO3M6MTA6ImZvcm1fY2xhc3MiO3M6MTA6ImNocm9ub2Zvcm0iO3M6MTU6ImZvcm1fdGFnX2F0dGFjaCI7czowOiIiO3M6MjQ6InJlcXVpcmVkX2xhYmVsc19pZGVudGlmeSI7czoxOiIxIjtzOjEyOiJyZWxhdGl2ZV91cmwiO3M6MToiMSI7czoxMToiYWpheF9zdWJtaXQiO3M6MToiMCI7czoxMzoiYWRkX2Zvcm1fdGFncyI7czoxOiIxIjtzOjk6InhodG1sX3VybCI7czoxOiIwIjt9aTo1O2E6Mjp7czoxMjoiYWN0aW9uX2xhYmVsIjtzOjMxOiJNb2RpZmljYSBrZXlzIGNhdGVnb3J5IHNlbGVjdG9yIjtzOjc6ImNvbnRlbnQiO3M6MjQ2OiI8c2NyaXB0Pg0KalF1ZXJ5KCJvcHRpb25bdmFsdWUqPSdzdXBjXyddIikuZWFjaChmdW5jdGlvbiggaW5kZXggKSB7DQogICBqUXVlcnkodGhpcykuY3NzKCdiYWNrZ3JvdW5kLWNvbG9yJywgJyM5OTknKTsNCiAgIGpRdWVyeSh0aGlzKS5jc3MoJ2NvbG9yJywgJ2JsYWNrJyk7DQogICBqUXVlcnkodGhpcykuYXR0cigndmFsdWUnLCBqUXVlcnkodGhpcykuYXR0cigndmFsdWUnKS5zdWJzdHIoNSkpOyAgIA0KfSk7DQo8L3NjcmlwdD4iO31pOjM7YToyOntzOjEyOiJhY3Rpb25fbGFiZWwiO3M6MjI6IlNhbHZhdGFnZ2lvIGUgcmVkaXJlY3QiO3M6NzoiY29udGVudCI7czoyNzI6Ijw/cGhwDQoNCkpMb2FkZXI6OnJlZ2lzdGVyKCdFdmVudHNNb2RlbCcsICd2ZW5kb3IvbW9kZWwvRXZlbnRzTW9kZWwucGhwJyk7DQokbW9kZWwgPSBuZXcgRXZlbnRzTW9kZWwoKTsNCiRtb2RlbC0+c2F2ZSgpOw0KDQpKRmFjdG9yeTo6Z2V0QXBwbGljYXRpb24oKS0+cmVkaXJlY3QoSlVyaTo6YmFzZSgpLic/c2hvdz1lZGl0Jml0ZW09Jy5KUmVxdWVzdDo6Z2V0VmFyKCdpdGVtJyksICdTYWx2YXRhZ2dpbyBtb2RpZmljaGUgZWZmZXR0dWF0ZScsICdzdWNjZXNzJyk7DQoNCj8+Ijt9fXM6MTQ6ImRiX2ZpZWxkc19saXN0IjthOjE6e3M6MTA6ImNhdGVnb3JpZXMiO3M6MDoiIjt9fQ==', 1, 'Eventi', 1, '<div class="form-group gcore-form-row" id="form-row-status"><label for="status" class="control-label gcore-label-left">Stato</label>\n<div class="gcore-input gcore-display-table" id="fin-status"><select name="status" id="status" size="" class="form-control A" title="" style="" data-load-state="" data-tooltip="">\n<option value="0">Disabilitato</option>\n<option value="1">Abilitato</option>\n</select><span class="help-block">Sarà possibile accedere all''evento lato client solo se è selezionata l''opzione "Abilitato"</span></div></div><div class="form-group gcore-form-row" id="form-row-title"><label for="title" class="control-label gcore-label-left">Nome dell''evento</label>\n<div class="gcore-input gcore-display-table" id="fin-title"><input name="title" id="title" value="" placeholder="Titolo dell&#039;evento" maxlength="30" size="" class="validate[&#039;required&#039;] form-control A" title="" style="" data-inputmask="" data-load-state="" data-tooltip="" type="text" /><span class="help-block">Inserire il titolo dell''evento</span></div></div><?php\n$keys = \\GCore\\Libs\\Arr::getVal(\\GCore\\Libs\\Arr::getVal($form->data, explode(".", "categorieslist")), explode(".", "[n].id"));\n$values = \\GCore\\Libs\\Arr::getVal(\\GCore\\Libs\\Arr::getVal($form->data, explode(".", "categorieslist")), explode(".", "[n].name"));\n$options = array_combine($keys, $values);\n$field = array (\n  ''name'' => ''categoria'',\n  ''id'' => ''categoria'',\n  ''options'' => \n  array (\n  ),\n  ''empty'' => '''',\n  ''values'' => \n  array (\n  ),\n  ''label'' => \n  array (\n    ''text'' => ''Categoria'',\n    ''position'' => ''left'',\n  ),\n  ''sublabel'' => ''Categoria di appartenenza dell\\''evento'',\n  ''multiple'' => ''0'',\n  ''size'' => '''',\n  ''class'' => '''',\n  ''title'' => '''',\n  ''style'' => '''',\n  ''params'' => '''',\n  '':data-load-state'' => '''',\n  '':data-tooltip'' => '''',\n  ''type'' => ''dropdown'',\n  ''container_id'' => ''0'',\n);\n$field["options"] = $options;\necho \\GCore\\Helpers\\Html::formLine($field["name"], $field);\n?><div class="form-group gcore-form-row" id="form-row-date"><label for="date" class="control-label gcore-label-left">Data</label>\n<div class="gcore-input gcore-display-table" id="fin-date"><input name="date" id="date" value="" placeholder="" maxlength="30" size="" class="validate[&#039;required&#039;] form-control A" title="" style="" data-inputmask="&#039;alias&#039; : &#039;dd-mm-yyyy&#039;" data-load-state="" data-tooltip="" type="text" /><span class="help-block">Inserire la data in cui si svolgerà l''evento. es: gg-mm-aaaa</span></div></div><div class="form-group gcore-form-row" id="form-row-hour"><label for="hour" class="control-label gcore-label-left">Ora inizio</label>\n<div class="gcore-input gcore-display-table" id="fin-hour"><input name="hour" id="hour" value="" placeholder="" maxlength="30" size="" class="validate[&#039;required&#039;] form-control A" title="" style="" data-inputmask="&#039;alias&#039; : &#039;hh:mm&#039;" data-load-state="" data-tooltip="" type="text" /><span class="help-block">Inserire l''orario di inizio dell''evento. es: hh:mm </span></div></div><div class="form-group gcore-form-row" id="form-row-sex"><label for="sex" class="control-label gcore-label-left">Sesso</label>\n<div class="gcore-input gcore-display-table" id="fin-sex"><select name="sex" id="sex" size="" class="form-control A" title="" style="" data-load-state="" data-tooltip="">\n<option value="0">Solo Uomini</option>\n<option value="1">Solo Donne</option>\n<option value="3">Donne e Uomini</option>\n</select><span class="help-block">Genere per cui un evento è pianificato</span></div></div><div class="form-group gcore-form-row" id="form-row-scope"><label for="scope" class="control-label gcore-label-left">Accesso</label>\n<div class="gcore-input gcore-display-table" id="fin-scope"><select name="scope" id="scope" size="" class="form-control A" title="" style="" data-load-state="" data-tooltip="">\n<option value="0">Pubblico</option>\n<option value="1">Privato</option>\n<option value="2">Friends Only</option>\n</select><span class="help-block">Selezionare la tipologia di evento</span></div></div><div class="form-group gcore-form-row" id="form-row-description"><label for="description" class="control-label gcore-label-left">Descrizione</label>\n<div class="gcore-input gcore-display-table" id="fin-description"><textarea name="description" id="description" placeholder="Descrizione dell&#039;evento" rows="5" cols="40" class="validate[&#039;required&#039;] form-control A" title="" style="" data-wysiwyg="0" data-load-state="" data-tooltip=""></textarea><span class="help-block">Descrizione inserita dal planner</span></div></div><div class="form-group gcore-form-row" id="form-row-image"><label for="image" class="control-label gcore-label-left">Copertina</label>\n<div class="gcore-input gcore-display-table" id="fin-image"><input name="image" id="image" class="form-control gcore-height-auto A" title="" style="" multiple="0" data-load-state="" data-tooltip="" type="file" /><span class="help-block">Immagine di copertina<br>Tipi di file: jpeg, jpg, gif, png<br>Larghezza e Altezza esatte: 720px x 426px<br>Dimensioni massime: 500 KB</span></div></div><div class="form-group gcore-form-row" id="form-row-town"><label for="town" class="control-label gcore-label-left">Città</label>\n<div class="gcore-input gcore-display-table" id="fin-town"><input name="town" id="town" value="" placeholder="Città" maxlength="40" size="" class="validate[&#039;required&#039;] form-control A" title="" style="" data-inputmask="" data-load-state="" data-tooltip="" type="text" /><span class="help-block">Inserire il nome della città in cui si svolgerà l''evento</span></div></div><div class="form-group gcore-form-row" id="form-row-address"><label for="address" class="control-label gcore-label-left">Indirizzo</label>\n<div class="gcore-input gcore-display-table" id="fin-address"><input name="address" id="address" value="" placeholder="" maxlength="" size="" class="form-control A" title="" style="" data-inputmask="" data-load-state="" data-tooltip="" type="text" /><span class="help-block">Indirizzo e numero civico del luogo dove si svolgerà l''evento</span></div></div><script>\r\ngoogle.maps.event.addDomListener(window, ''load'', initialize_mappa);\r\n</script>\r\n<style>\r\n#map-canvas{ \r\n  height: 400px;\r\n  width: 950px;\r\n  /* margin-bottom: 30px; */\r\n}\r\n</style>\r\n<div id="map-canvas"></div>\r\n<span class="help-block">Posizionarsi con il cursore del mouse sul marcatore e spostarlo sulla mappa tenendo premuto il tasto sinistro per modificare la posizione.</span><br><br><div class="form-group gcore-form-row" id="form-row-agemin"><label for="agemin" class="control-label gcore-label-left">Età minima</label>\n<div class="gcore-input gcore-display-table" id="fin-agemin"><input name="agemin" id="agemin" value="" placeholder="20" maxlength="" size="" class="validate[&#039;required&#039;,&#039;number&#039;] form-control A" title="" style="" data-inputmask="&#039;alias&#039; : &#039;integer&#039;" data-load-state="" data-tooltip="" type="text" /><span class="help-block">Età minima per poter partecipare all''evento (Numero intero positivo richiesto)</span></div></div><div class="form-group gcore-form-row" id="form-row-agemax"><label for="agemax" class="control-label gcore-label-left">Età massima</label>\n<div class="gcore-input gcore-display-table" id="fin-agemax"><input name="agemax" id="agemax" value="" placeholder="40" maxlength="" size="" class="validate[&#039;required&#039;,&#039;number&#039;] form-control A" title="" style="" data-inputmask="&#039;alias&#039; : &#039;integer&#039;" data-load-state="" data-tooltip="" type="text" /><span class="help-block">Età massima per poter partecipare all''evento (Numero intero positivo richiesto)</span></div></div><div class="form-group gcore-form-row" id="form-row-sitcurrent"><label for="sitcurrent" class="control-label gcore-label-left">Partecipanti</label>\n<div class="gcore-input gcore-display-table" id="fin-sitcurrent"><input name="sitcurrent" id="sitcurrent" value="" placeholder="18" maxlength="" size="" class="validate[&#039;required&#039;,&#039;number&#039;] form-control A" title="" style="" data-inputmask="&#039;alias&#039; : &#039;integer&#039;" data-load-state="disabled" data-tooltip="" type="text" /><span class="help-block">Numero di partecipanti che hanno effettuato la prenotazione</span></div></div><div class="form-group gcore-form-row" id="form-row-sittotal"><label for="sittotal" class="control-label gcore-label-left">Posti disponibili</label>\n<div class="gcore-input gcore-display-table" id="fin-sittotal"><input name="sittotal" id="sittotal" value="" placeholder="50" maxlength="" size="" class="validate[&#039;required&#039;,&#039;number&#039;] form-control A" title="" style="" data-inputmask="&#039;alias&#039; : &#039;integer&#039;" data-load-state="" data-tooltip="" type="text" /><span class="help-block">Posti disponibili in totale</span></div></div><div class="form-group gcore-form-row" id="form-row-button16"><div class="gcore-input gcore-display-table" id="fin-button16"><input name="button16" id="button16" type="submit" value="Salva" class="btn btn-default form-control A" style="" data-load-state="" /></div></div><input name="sitcurrentghost" id="sitcurrentghost" value="" type="hidden" class="form-control A" /><input name="latitude" id="latitude" value="" type="hidden" class="form-control A" /><input name="longitude" id="longitude" value="" type="hidden" class="form-control A" />');
INSERT INTO `zcp8_chronoengine_chronoforms` (`id`, `title`, `params`, `extras`, `published`, `app`, `form_type`, `content`) VALUES
(2, 'event_search_old', '{"description":"Impostazione dei filtri da applicare alla lista degli eventi","setup":"0","theme":"bootstrap3","tight_layout":"0","rtl_support":"0","labels_right_aligned":"0","labels_auto_width":"0","js_validation_language":""}', 'YTo0OntzOjY6ImZpZWxkcyI7YToxODp7aTo4O2E6MjA6e3M6NDoibmFtZSI7czo2OiJzdGF0dXMiO3M6MjoiaWQiO3M6Njoic3RhdHVzIjtzOjc6Im9wdGlvbnMiO3M6Mjc6IjA9RGlzYWJpbGl0YXRvDQoxPUFiaWxpdGF0byI7czo1OiJlbXB0eSI7czoxMzoiTm9uIEltcG9zdGF0byI7czo2OiJ2YWx1ZXMiO3M6MDoiIjtzOjU6ImxhYmVsIjthOjI6e3M6NDoidGV4dCI7czo1OiJTdGF0byI7czo4OiJwb3NpdGlvbiI7czo0OiJsZWZ0Ijt9czo4OiJzdWJsYWJlbCI7czo5MjoiU2Fyw6AgcG9zc2liaWxlIGFjY2VkZXJlIGFsbCdldmVudG8gbGF0byBjbGllbnQgc29sbyBzZSDDqCBzZWxlemlvbmF0YSBsJ29wemlvbmUgIkFiaWxpdGF0byIiO3M6ODoibXVsdGlwbGUiO3M6MToiMCI7czo0OiJzaXplIjtzOjA6IiI7czo1OiJjbGFzcyI7czowOiIiO3M6NToidGl0bGUiO3M6MDoiIjtzOjU6InN0eWxlIjtzOjA6IiI7czo2OiJwYXJhbXMiO3M6MDoiIjtzOjE2OiI6ZGF0YS1sb2FkLXN0YXRlIjtzOjA6IiI7czoxMzoiOmRhdGEtdG9vbHRpcCI7czowOiIiO3M6NDoidHlwZSI7czo4OiJkcm9wZG93biI7czoxMjoiY29udGFpbmVyX2lkIjtzOjE6IjAiO3M6MTA6InZhbGlkYXRpb24iO2E6Mzp7czo4OiJyZXF1aXJlZCI7czowOiIiO3M6NzoiY29uZmlybSI7czowOiIiO3M6NjoiY3VzdG9tIjtzOjA6IiI7fXM6MTI6ImR5bmFtaWNfZGF0YSI7YTo0OntzOjc6ImVuYWJsZWQiO3M6MDoiIjtzOjk6ImRhdGFfcGF0aCI7czowOiIiO3M6OToidmFsdWVfa2V5IjtzOjA6IiI7czo4OiJ0ZXh0X2tleSI7czowOiIiO31zOjY6ImV2ZW50cyI7YToxOntpOjA7YTo1OntzOjg6Im9wZXJhdG9yIjtzOjE6Ij0iO3M6NToic3RhdGUiO3M6MDoiIjtzOjY6ImFjdGlvbiI7czowOiIiO3M6NjoidGFyZ2V0IjtzOjA6IiI7czo3OiJvcHRpb25zIjtzOjA6IiI7fX19aToxO2E6MTg6e3M6NDoibmFtZSI7czo1OiJ0aXRsZSI7czoyOiJpZCI7czo1OiJ0aXRsZSI7czo1OiJ2YWx1ZSI7czowOiIiO3M6NToibGFiZWwiO2E6Mjp7czo0OiJ0ZXh0IjtzOjE2OiJOb21lIGRlbGwnZXZlbnRvIjtzOjg6InBvc2l0aW9uIjtzOjQ6ImxlZnQiO31zOjg6InN1YmxhYmVsIjtzOjMwOiJJbnNlcmlyZSBpbCB0aXRvbG8gZGVsbCdldmVudG8iO3M6MTE6InBsYWNlaG9sZGVyIjtzOjE4OiJUaXRvbG8gZGVsbCdldmVudG8iO3M6OToibWF4bGVuZ3RoIjtzOjI6IjMwIjtzOjQ6InNpemUiO3M6MDoiIjtzOjU6ImNsYXNzIjtzOjA6IiI7czo1OiJ0aXRsZSI7czowOiIiO3M6NToic3R5bGUiO3M6MDoiIjtzOjE1OiI6ZGF0YS1pbnB1dG1hc2siO3M6MDoiIjtzOjY6InBhcmFtcyI7czowOiIiO3M6MTY6IjpkYXRhLWxvYWQtc3RhdGUiO3M6MDoiIjtzOjEzOiI6ZGF0YS10b29sdGlwIjtzOjA6IiI7czo0OiJ0eXBlIjtzOjQ6InRleHQiO3M6MTI6ImNvbnRhaW5lcl9pZCI7czoxOiIwIjtzOjEwOiJ2YWxpZGF0aW9uIjthOjEzOntzOjg6InJlcXVpcmVkIjtzOjA6IiI7czo1OiJhbHBoYSI7czowOiIiO3M6ODoiYWxwaGFudW0iO3M6MDoiIjtzOjU6ImRpZ2l0IjtzOjA6IiI7czo3OiJub2RpZ2l0IjtzOjA6IiI7czo3OiJub3NwYWNlIjtzOjA6IiI7czo2OiJudW1iZXIiO3M6MDoiIjtzOjU6ImVtYWlsIjtzOjA6IiI7czo1OiJwaG9uZSI7czowOiIiO3M6MTE6InBob25lX2ludGVyIjtzOjA6IiI7czozOiJ1cmwiO3M6MDoiIjtzOjc6ImNvbmZpcm0iO3M6MDoiIjtzOjY6ImN1c3RvbSI7czowOiIiO319aToyO2E6MjA6e3M6NDoibmFtZSI7czo5OiJjYXRlZ29yaWEiO3M6MjoiaWQiO3M6OToiY2F0ZWdvcmlhIjtzOjc6Im9wdGlvbnMiO3M6MDoiIjtzOjU6ImVtcHR5IjtzOjEzOiJOb24gSW1wb3N0YXRvIjtzOjY6InZhbHVlcyI7czowOiIiO3M6NToibGFiZWwiO2E6Mjp7czo0OiJ0ZXh0IjtzOjk6IkNhdGVnb3JpYSI7czo4OiJwb3NpdGlvbiI7czo0OiJsZWZ0Ijt9czo4OiJzdWJsYWJlbCI7czozNzoiQ2F0ZWdvcmlhIGRpIGFwcGFydGVuZW56YSBkZWxsJ2V2ZW50byI7czo4OiJtdWx0aXBsZSI7czoxOiIwIjtzOjQ6InNpemUiO3M6MDoiIjtzOjU6ImNsYXNzIjtzOjA6IiI7czo1OiJ0aXRsZSI7czowOiIiO3M6NToic3R5bGUiO3M6MDoiIjtzOjY6InBhcmFtcyI7czowOiIiO3M6MTY6IjpkYXRhLWxvYWQtc3RhdGUiO3M6MDoiIjtzOjEzOiI6ZGF0YS10b29sdGlwIjtzOjA6IiI7czo0OiJ0eXBlIjtzOjg6ImRyb3Bkb3duIjtzOjEyOiJjb250YWluZXJfaWQiO3M6MToiMCI7czoxMDoidmFsaWRhdGlvbiI7YTozOntzOjg6InJlcXVpcmVkIjtzOjA6IiI7czo3OiJjb25maXJtIjtzOjA6IiI7czo2OiJjdXN0b20iO3M6MDoiIjt9czoxMjoiZHluYW1pY19kYXRhIjthOjQ6e3M6NzoiZW5hYmxlZCI7czoxOiIxIjtzOjk6ImRhdGFfcGF0aCI7czoxNDoiY2F0ZWdvcmllc2xpc3QiO3M6OToidmFsdWVfa2V5IjtzOjI6ImlkIjtzOjg6InRleHRfa2V5IjtzOjQ6Im5hbWUiO31zOjY6ImV2ZW50cyI7YToxOntpOjA7YTo1OntzOjg6Im9wZXJhdG9yIjtzOjE6Ij0iO3M6NToic3RhdGUiO3M6MDoiIjtzOjY6ImFjdGlvbiI7czowOiIiO3M6NjoidGFyZ2V0IjtzOjA6IiI7czo3OiJvcHRpb25zIjtzOjA6IiI7fX19aToxNDthOjE4OntzOjQ6Im5hbWUiO3M6NzoiZGF0ZWluZiI7czoyOiJpZCI7czo3OiJkYXRlaW5mIjtzOjU6InZhbHVlIjtzOjA6IiI7czo1OiJsYWJlbCI7YToyOntzOjQ6InRleHQiO3M6MTA6IkRhbGxhIGRhdGEiO3M6ODoicG9zaXRpb24iO3M6NDoibGVmdCI7fXM6ODoic3VibGFiZWwiO3M6NTY6IkxpbWl0ZSBpbmZlcmlvcmUgZGVsbCdpbnRlcnZhbGxvIGRpIHRlbXBvIGRhIHNlbGV6aW9uYXJlIjtzOjExOiJwbGFjZWhvbGRlciI7czowOiIiO3M6OToibWF4bGVuZ3RoIjtzOjI6IjMwIjtzOjQ6InNpemUiO3M6MDoiIjtzOjU6ImNsYXNzIjtzOjA6IiI7czo1OiJ0aXRsZSI7czowOiIiO3M6NToic3R5bGUiO3M6MDoiIjtzOjE1OiI6ZGF0YS1pbnB1dG1hc2siO3M6MjI6IidhbGlhcycgOiAnZGQtbW0teXl5eSciO3M6NjoicGFyYW1zIjtzOjA6IiI7czoxNjoiOmRhdGEtbG9hZC1zdGF0ZSI7czowOiIiO3M6MTM6IjpkYXRhLXRvb2x0aXAiO3M6MDoiIjtzOjQ6InR5cGUiO3M6NDoidGV4dCI7czoxMjoiY29udGFpbmVyX2lkIjtzOjE6IjAiO3M6MTA6InZhbGlkYXRpb24iO2E6MTM6e3M6ODoicmVxdWlyZWQiO3M6MDoiIjtzOjU6ImFscGhhIjtzOjA6IiI7czo4OiJhbHBoYW51bSI7czowOiIiO3M6NToiZGlnaXQiO3M6MDoiIjtzOjc6Im5vZGlnaXQiO3M6MDoiIjtzOjc6Im5vc3BhY2UiO3M6MDoiIjtzOjY6Im51bWJlciI7czowOiIiO3M6NToiZW1haWwiO3M6MDoiIjtzOjU6InBob25lIjtzOjA6IiI7czoxMToicGhvbmVfaW50ZXIiO3M6MDoiIjtzOjM6InVybCI7czowOiIiO3M6NzoiY29uZmlybSI7czowOiIiO3M6NjoiY3VzdG9tIjtzOjA6IiI7fX1pOjIyO2E6MTg6e3M6NDoibmFtZSI7czo3OiJkYXRlc3VwIjtzOjI6ImlkIjtzOjc6ImRhdGVzdXAiO3M6NToidmFsdWUiO3M6MDoiIjtzOjU6ImxhYmVsIjthOjI6e3M6NDoidGV4dCI7czo5OiJBbGxhIGRhdGEiO3M6ODoicG9zaXRpb24iO3M6NDoibGVmdCI7fXM6ODoic3VibGFiZWwiO3M6NTY6IkxpbWl0ZSBzdXBlcmlvcmUgZGVsbCdpbnRlcnZhbGxvIGRpIHRlbXBvIGRhIGNvbnNpZGVyYXJlIjtzOjExOiJwbGFjZWhvbGRlciI7czowOiIiO3M6OToibWF4bGVuZ3RoIjtzOjI6IjMwIjtzOjQ6InNpemUiO3M6MDoiIjtzOjU6ImNsYXNzIjtzOjA6IiI7czo1OiJ0aXRsZSI7czowOiIiO3M6NToic3R5bGUiO3M6MDoiIjtzOjE1OiI6ZGF0YS1pbnB1dG1hc2siO3M6MjI6IidhbGlhcycgOiAnZGQtbW0teXl5eSciO3M6NjoicGFyYW1zIjtzOjA6IiI7czoxNjoiOmRhdGEtbG9hZC1zdGF0ZSI7czowOiIiO3M6MTM6IjpkYXRhLXRvb2x0aXAiO3M6MDoiIjtzOjQ6InR5cGUiO3M6NDoidGV4dCI7czoxMjoiY29udGFpbmVyX2lkIjtzOjE6IjAiO3M6MTA6InZhbGlkYXRpb24iO2E6MTM6e3M6ODoicmVxdWlyZWQiO3M6MDoiIjtzOjU6ImFscGhhIjtzOjA6IiI7czo4OiJhbHBoYW51bSI7czowOiIiO3M6NToiZGlnaXQiO3M6MDoiIjtzOjc6Im5vZGlnaXQiO3M6MDoiIjtzOjc6Im5vc3BhY2UiO3M6MDoiIjtzOjY6Im51bWJlciI7czowOiIiO3M6NToiZW1haWwiO3M6MDoiIjtzOjU6InBob25lIjtzOjA6IiI7czoxMToicGhvbmVfaW50ZXIiO3M6MDoiIjtzOjM6InVybCI7czowOiIiO3M6NzoiY29uZmlybSI7czowOiIiO3M6NjoiY3VzdG9tIjtzOjA6IiI7fX1pOjE3O2E6MjA6e3M6NDoibmFtZSI7czozOiJzZXgiO3M6MjoiaWQiO3M6Mzoic2V4IjtzOjc6Im9wdGlvbnMiO3M6NDU6IjA9U29sbyBVb21pbmkNCjE9U29sbyBEb25uZQ0KMz1Eb25uZSBlIFVvbWluaSI7czo1OiJlbXB0eSI7czoxMzoiTm9uIEltcG9zdGF0byI7czo2OiJ2YWx1ZXMiO3M6MDoiIjtzOjU6ImxhYmVsIjthOjI6e3M6NDoidGV4dCI7czo1OiJTZXNzbyI7czo4OiJwb3NpdGlvbiI7czo0OiJsZWZ0Ijt9czo4OiJzdWJsYWJlbCI7czozOToiR2VuZXJlIHBlciBjdWkgdW4gZXZlbnRvIMOoIHBpYW5pZmljYXRvIjtzOjg6Im11bHRpcGxlIjtzOjE6IjAiO3M6NDoic2l6ZSI7czowOiIiO3M6NToiY2xhc3MiO3M6MDoiIjtzOjU6InRpdGxlIjtzOjA6IiI7czo1OiJzdHlsZSI7czowOiIiO3M6NjoicGFyYW1zIjtzOjA6IiI7czoxNjoiOmRhdGEtbG9hZC1zdGF0ZSI7czowOiIiO3M6MTM6IjpkYXRhLXRvb2x0aXAiO3M6MDoiIjtzOjQ6InR5cGUiO3M6ODoiZHJvcGRvd24iO3M6MTI6ImNvbnRhaW5lcl9pZCI7czoxOiIwIjtzOjEwOiJ2YWxpZGF0aW9uIjthOjM6e3M6ODoicmVxdWlyZWQiO3M6MDoiIjtzOjc6ImNvbmZpcm0iO3M6MDoiIjtzOjY6ImN1c3RvbSI7czowOiIiO31zOjEyOiJkeW5hbWljX2RhdGEiO2E6NDp7czo3OiJlbmFibGVkIjtzOjA6IiI7czo5OiJkYXRhX3BhdGgiO3M6MDoiIjtzOjk6InZhbHVlX2tleSI7czowOiIiO3M6ODoidGV4dF9rZXkiO3M6MDoiIjt9czo2OiJldmVudHMiO2E6MTp7aTowO2E6NTp7czo4OiJvcGVyYXRvciI7czoxOiI9IjtzOjU6InN0YXRlIjtzOjA6IiI7czo2OiJhY3Rpb24iO3M6MDoiIjtzOjY6InRhcmdldCI7czowOiIiO3M6Nzoib3B0aW9ucyI7czowOiIiO319fWk6NzthOjIwOntzOjQ6Im5hbWUiO3M6NToic2NvcGUiO3M6MjoiaWQiO3M6NToic2NvcGUiO3M6Nzoib3B0aW9ucyI7czozNzoiMD1QdWJibGljbw0KMT1Qcml2YXRvDQoyPUZyaWVuZHMgT25seSI7czo1OiJlbXB0eSI7czoxMzoiTm9uIEltcG9zdGF0byI7czo2OiJ2YWx1ZXMiO3M6MDoiIjtzOjU6ImxhYmVsIjthOjI6e3M6NDoidGV4dCI7czo3OiJBY2Nlc3NvIjtzOjg6InBvc2l0aW9uIjtzOjQ6ImxlZnQiO31zOjg6InN1YmxhYmVsIjtzOjM0OiJTZWxlemlvbmFyZSBsYSB0aXBvbG9naWEgZGkgZXZlbnRvIjtzOjg6Im11bHRpcGxlIjtzOjE6IjAiO3M6NDoic2l6ZSI7czowOiIiO3M6NToiY2xhc3MiO3M6MDoiIjtzOjU6InRpdGxlIjtzOjA6IiI7czo1OiJzdHlsZSI7czowOiIiO3M6NjoicGFyYW1zIjtzOjA6IiI7czoxNjoiOmRhdGEtbG9hZC1zdGF0ZSI7czowOiIiO3M6MTM6IjpkYXRhLXRvb2x0aXAiO3M6MDoiIjtzOjQ6InR5cGUiO3M6ODoiZHJvcGRvd24iO3M6MTI6ImNvbnRhaW5lcl9pZCI7czoxOiIwIjtzOjEwOiJ2YWxpZGF0aW9uIjthOjM6e3M6ODoicmVxdWlyZWQiO3M6MDoiIjtzOjc6ImNvbmZpcm0iO3M6MDoiIjtzOjY6ImN1c3RvbSI7czowOiIiO31zOjEyOiJkeW5hbWljX2RhdGEiO2E6NDp7czo3OiJlbmFibGVkIjtzOjA6IiI7czo5OiJkYXRhX3BhdGgiO3M6MDoiIjtzOjk6InZhbHVlX2tleSI7czowOiIiO3M6ODoidGV4dF9rZXkiO3M6MDoiIjt9czo2OiJldmVudHMiO2E6MTp7aTowO2E6NTp7czo4OiJvcGVyYXRvciI7czoxOiI9IjtzOjU6InN0YXRlIjtzOjA6IiI7czo2OiJhY3Rpb24iO3M6MDoiIjtzOjY6InRhcmdldCI7czowOiIiO3M6Nzoib3B0aW9ucyI7czowOiIiO319fWk6MzthOjE4OntzOjQ6Im5hbWUiO3M6MTE6ImRlc2NyaXB0aW9uIjtzOjI6ImlkIjtzOjExOiJkZXNjcmlwdGlvbiI7czo1OiJ2YWx1ZSI7czowOiIiO3M6NToibGFiZWwiO2E6Mjp7czo0OiJ0ZXh0IjtzOjExOiJEZXNjcml6aW9uZSI7czo4OiJwb3NpdGlvbiI7czo0OiJsZWZ0Ijt9czo4OiJzdWJsYWJlbCI7czo5MjoiU2VxdWVuemEgZGkgdG9rZW4gY2hlIHBvc3Nvbm8gdHJvdmFyc2kgbmVsbGEgZGVzY3JpemlvbmUgZGVsbCdldmVudG8gc2VwYXJhdGkgZGEgdW5hIHZpcmdvbGEiO3M6MTE6InBsYWNlaG9sZGVyIjtzOjQ0OiJFczogTG9yZW0gaXBzdW0sIGxvcmVtLCBpcHN1bSwgdG9rZW4xLCB0b2tlbiI7czo0OiJyb3dzIjtzOjE6IjUiO3M6NDoiY29scyI7czoyOiI0MCI7czo1OiJjbGFzcyI7czowOiIiO3M6NToidGl0bGUiO3M6MDoiIjtzOjU6InN0eWxlIjtzOjA6IiI7czoxMzoiOmRhdGEtd3lzaXd5ZyI7czoxOiIwIjtzOjY6InBhcmFtcyI7czowOiIiO3M6MTY6IjpkYXRhLWxvYWQtc3RhdGUiO3M6MDoiIjtzOjEzOiI6ZGF0YS10b29sdGlwIjtzOjA6IiI7czo0OiJ0eXBlIjtzOjg6InRleHRhcmVhIjtzOjEyOiJjb250YWluZXJfaWQiO3M6MToiMCI7czoxMDoidmFsaWRhdGlvbiI7YTo4OntzOjg6InJlcXVpcmVkIjtzOjA6IiI7czo1OiJhbHBoYSI7czowOiIiO3M6ODoiYWxwaGFudW0iO3M6MDoiIjtzOjU6ImRpZ2l0IjtzOjA6IiI7czo3OiJub2RpZ2l0IjtzOjA6IiI7czo2OiJudW1iZXIiO3M6MDoiIjtzOjc6ImNvbmZpcm0iO3M6MDoiIjtzOjY6ImN1c3RvbSI7czowOiIiO319aTo1O2E6MTg6e3M6NDoibmFtZSI7czo0OiJ0b3duIjtzOjI6ImlkIjtzOjQ6InRvd24iO3M6NToidmFsdWUiO3M6MDoiIjtzOjU6ImxhYmVsIjthOjI6e3M6NDoidGV4dCI7czo2OiJDaXR0w6AiO3M6ODoicG9zaXRpb24iO3M6NDoibGVmdCI7fXM6ODoic3VibGFiZWwiO3M6NTg6Ikluc2VyaXJlIGlsIG5vbWUgZGVsbGEgY2l0dMOgIGluIGN1aSBzaSBzdm9sZ2Vyw6AgbCdldmVudG8iO3M6MTE6InBsYWNlaG9sZGVyIjtzOjY6IkNpdHTDoCI7czo5OiJtYXhsZW5ndGgiO3M6MjoiNDAiO3M6NDoic2l6ZSI7czowOiIiO3M6NToiY2xhc3MiO3M6MDoiIjtzOjU6InRpdGxlIjtzOjA6IiI7czo1OiJzdHlsZSI7czowOiIiO3M6MTU6IjpkYXRhLWlucHV0bWFzayI7czowOiIiO3M6NjoicGFyYW1zIjtzOjA6IiI7czoxNjoiOmRhdGEtbG9hZC1zdGF0ZSI7czowOiIiO3M6MTM6IjpkYXRhLXRvb2x0aXAiO3M6MDoiIjtzOjQ6InR5cGUiO3M6NDoidGV4dCI7czoxMjoiY29udGFpbmVyX2lkIjtzOjE6IjAiO3M6MTA6InZhbGlkYXRpb24iO2E6MTM6e3M6ODoicmVxdWlyZWQiO3M6MDoiIjtzOjU6ImFscGhhIjtzOjA6IiI7czo4OiJhbHBoYW51bSI7czowOiIiO3M6NToiZGlnaXQiO3M6MDoiIjtzOjc6Im5vZGlnaXQiO3M6MDoiIjtzOjc6Im5vc3BhY2UiO3M6MDoiIjtzOjY6Im51bWJlciI7czowOiIiO3M6NToiZW1haWwiO3M6MDoiIjtzOjU6InBob25lIjtzOjA6IiI7czoxMToicGhvbmVfaW50ZXIiO3M6MDoiIjtzOjM6InVybCI7czowOiIiO3M6NzoiY29uZmlybSI7czowOiIiO3M6NjoiY3VzdG9tIjtzOjA6IiI7fX1pOjIzO2E6MTg6e3M6NDoibmFtZSI7czoxNDoibWlucGFydGVjaXBhbnQiO3M6MjoiaWQiO3M6MTQ6Im1pbnBhcnRlY2lwYW50IjtzOjU6InZhbHVlIjtzOjA6IiI7czo1OiJsYWJlbCI7YToyOntzOjQ6InRleHQiO3M6Mjk6IkxpbWl0ZSBpbmZlcmlvcmUgcGFydGVjaXBhbnRpIjtzOjg6InBvc2l0aW9uIjtzOjQ6ImxlZnQiO31zOjg6InN1YmxhYmVsIjtzOjUxOiJOdW1lcm8gZGkgcGFydGVjaXBhbnRpIHN1cGVyaW9yZSBhbCB2YWxvcmUgaW5zZXJpdG8iO3M6MTE6InBsYWNlaG9sZGVyIjtzOjA6IiI7czo5OiJtYXhsZW5ndGgiO3M6MToiMyI7czo0OiJzaXplIjtzOjA6IiI7czo1OiJjbGFzcyI7czowOiIiO3M6NToidGl0bGUiO3M6MDoiIjtzOjU6InN0eWxlIjtzOjA6IiI7czoxNToiOmRhdGEtaW5wdXRtYXNrIjtzOjE5OiInYWxpYXMnIDogJ2ludGVnZXInIjtzOjY6InBhcmFtcyI7czowOiIiO3M6MTY6IjpkYXRhLWxvYWQtc3RhdGUiO3M6MDoiIjtzOjEzOiI6ZGF0YS10b29sdGlwIjtzOjA6IiI7czo0OiJ0eXBlIjtzOjQ6InRleHQiO3M6MTI6ImNvbnRhaW5lcl9pZCI7czoxOiIwIjtzOjEwOiJ2YWxpZGF0aW9uIjthOjEzOntzOjg6InJlcXVpcmVkIjtzOjA6IiI7czo1OiJhbHBoYSI7czowOiIiO3M6ODoiYWxwaGFudW0iO3M6MDoiIjtzOjU6ImRpZ2l0IjtzOjA6IiI7czo3OiJub2RpZ2l0IjtzOjA6IiI7czo3OiJub3NwYWNlIjtzOjA6IiI7czo2OiJudW1iZXIiO3M6MToiMSI7czo1OiJlbWFpbCI7czowOiIiO3M6NToicGhvbmUiO3M6MDoiIjtzOjExOiJwaG9uZV9pbnRlciI7czowOiIiO3M6MzoidXJsIjtzOjA6IiI7czo3OiJjb25maXJtIjtzOjA6IiI7czo2OiJjdXN0b20iO3M6MDoiIjt9fWk6MjQ7YToxODp7czo0OiJuYW1lIjtzOjE0OiJtYXhwYXJ0ZWNpcGFudCI7czoyOiJpZCI7czoxNDoibWF4cGFydGVjaXBhbnQiO3M6NToidmFsdWUiO3M6MDoiIjtzOjU6ImxhYmVsIjthOjI6e3M6NDoidGV4dCI7czoyOToiTGltaXRlIHN1cGVyaW9yZSBwYXJ0ZWNpcGFudGkiO3M6ODoicG9zaXRpb24iO3M6NDoibGVmdCI7fXM6ODoic3VibGFiZWwiO3M6NTE6Ik51bWVybyBkaSBwYXJ0ZWNpcGFudGkgaW5mZXJpb3JlIGFsIHZhbG9yZSBpbnNlcml0byI7czoxMToicGxhY2Vob2xkZXIiO3M6MDoiIjtzOjk6Im1heGxlbmd0aCI7czoxOiIzIjtzOjQ6InNpemUiO3M6MDoiIjtzOjU6ImNsYXNzIjtzOjA6IiI7czo1OiJ0aXRsZSI7czowOiIiO3M6NToic3R5bGUiO3M6MDoiIjtzOjE1OiI6ZGF0YS1pbnB1dG1hc2siO3M6MTk6IidhbGlhcycgOiAnaW50ZWdlciciO3M6NjoicGFyYW1zIjtzOjA6IiI7czoxNjoiOmRhdGEtbG9hZC1zdGF0ZSI7czowOiIiO3M6MTM6IjpkYXRhLXRvb2x0aXAiO3M6MDoiIjtzOjQ6InR5cGUiO3M6NDoidGV4dCI7czoxMjoiY29udGFpbmVyX2lkIjtzOjE6IjAiO3M6MTA6InZhbGlkYXRpb24iO2E6MTM6e3M6ODoicmVxdWlyZWQiO3M6MDoiIjtzOjU6ImFscGhhIjtzOjA6IiI7czo4OiJhbHBoYW51bSI7czowOiIiO3M6NToiZGlnaXQiO3M6MDoiIjtzOjc6Im5vZGlnaXQiO3M6MDoiIjtzOjc6Im5vc3BhY2UiO3M6MDoiIjtzOjY6Im51bWJlciI7czoxOiIxIjtzOjU6ImVtYWlsIjtzOjA6IiI7czo1OiJwaG9uZSI7czowOiIiO3M6MTE6InBob25lX2ludGVyIjtzOjA6IiI7czozOiJ1cmwiO3M6MDoiIjtzOjc6ImNvbmZpcm0iO3M6MDoiIjtzOjY6ImN1c3RvbSI7czowOiIiO319aToyNTthOjE4OntzOjQ6Im5hbWUiO3M6ODoibWlucGxhY2UiO3M6MjoiaWQiO3M6ODoibWlucGxhY2UiO3M6NToidmFsdWUiO3M6MDoiIjtzOjU6ImxhYmVsIjthOjI6e3M6NDoidGV4dCI7czozNDoiTGltaXRlIGluZmVyaW9yZSBwb3N0aSBkaXNwb25pYmlsaSI7czo4OiJwb3NpdGlvbiI7czo0OiJsZWZ0Ijt9czo4OiJzdWJsYWJlbCI7czo1NjoiTnVtZXJvIGRpIHBvc3RpIGRpc3BvbmliaWxpIHN1cGVyaW9yZSBhbCB2YWxvcmUgaW5zZXJpdG8iO3M6MTE6InBsYWNlaG9sZGVyIjtzOjA6IiI7czo5OiJtYXhsZW5ndGgiO3M6MToiMyI7czo0OiJzaXplIjtzOjA6IiI7czo1OiJjbGFzcyI7czowOiIiO3M6NToidGl0bGUiO3M6MDoiIjtzOjU6InN0eWxlIjtzOjA6IiI7czoxNToiOmRhdGEtaW5wdXRtYXNrIjtzOjE5OiInYWxpYXMnIDogJ2ludGVnZXInIjtzOjY6InBhcmFtcyI7czowOiIiO3M6MTY6IjpkYXRhLWxvYWQtc3RhdGUiO3M6MDoiIjtzOjEzOiI6ZGF0YS10b29sdGlwIjtzOjA6IiI7czo0OiJ0eXBlIjtzOjQ6InRleHQiO3M6MTI6ImNvbnRhaW5lcl9pZCI7czoxOiIwIjtzOjEwOiJ2YWxpZGF0aW9uIjthOjEzOntzOjg6InJlcXVpcmVkIjtzOjA6IiI7czo1OiJhbHBoYSI7czowOiIiO3M6ODoiYWxwaGFudW0iO3M6MDoiIjtzOjU6ImRpZ2l0IjtzOjA6IiI7czo3OiJub2RpZ2l0IjtzOjA6IiI7czo3OiJub3NwYWNlIjtzOjA6IiI7czo2OiJudW1iZXIiO3M6MToiMSI7czo1OiJlbWFpbCI7czowOiIiO3M6NToicGhvbmUiO3M6MDoiIjtzOjExOiJwaG9uZV9pbnRlciI7czowOiIiO3M6MzoidXJsIjtzOjA6IiI7czo3OiJjb25maXJtIjtzOjA6IiI7czo2OiJjdXN0b20iO3M6MDoiIjt9fWk6MjY7YToxODp7czo0OiJuYW1lIjtzOjg6Im1heHBsYWNlIjtzOjI6ImlkIjtzOjg6Im1heHBsYWNlIjtzOjU6InZhbHVlIjtzOjA6IiI7czo1OiJsYWJlbCI7YToyOntzOjQ6InRleHQiO3M6MzQ6IkxpbWl0ZSBzdXBlcmlvcmUgcG9zdGkgZGlzcG9uaWJpbGkiO3M6ODoicG9zaXRpb24iO3M6NDoibGVmdCI7fXM6ODoic3VibGFiZWwiO3M6NTY6Ik51bWVybyBkaSBwb3N0aSBkaXNwb25pYmlsaSBpbmZlcmlvcmUgYWwgdmFsb3JlIGluc2VyaXRvIjtzOjExOiJwbGFjZWhvbGRlciI7czowOiIiO3M6OToibWF4bGVuZ3RoIjtzOjE6IjMiO3M6NDoic2l6ZSI7czowOiIiO3M6NToiY2xhc3MiO3M6MDoiIjtzOjU6InRpdGxlIjtzOjA6IiI7czo1OiJzdHlsZSI7czowOiIiO3M6MTU6IjpkYXRhLWlucHV0bWFzayI7czoxOToiJ2FsaWFzJyA6ICdpbnRlZ2VyJyI7czo2OiJwYXJhbXMiO3M6MDoiIjtzOjE2OiI6ZGF0YS1sb2FkLXN0YXRlIjtzOjA6IiI7czoxMzoiOmRhdGEtdG9vbHRpcCI7czowOiIiO3M6NDoidHlwZSI7czo0OiJ0ZXh0IjtzOjEyOiJjb250YWluZXJfaWQiO3M6MToiMCI7czoxMDoidmFsaWRhdGlvbiI7YToxMzp7czo4OiJyZXF1aXJlZCI7czowOiIiO3M6NToiYWxwaGEiO3M6MDoiIjtzOjg6ImFscGhhbnVtIjtzOjA6IiI7czo1OiJkaWdpdCI7czowOiIiO3M6Nzoibm9kaWdpdCI7czowOiIiO3M6Nzoibm9zcGFjZSI7czowOiIiO3M6NjoibnVtYmVyIjtzOjE6IjEiO3M6NToiZW1haWwiO3M6MDoiIjtzOjU6InBob25lIjtzOjA6IiI7czoxMToicGhvbmVfaW50ZXIiO3M6MDoiIjtzOjM6InVybCI7czowOiIiO3M6NzoiY29uZmlybSI7czowOiIiO3M6NjoiY3VzdG9tIjtzOjA6IiI7fX1pOjI3O2E6MTg6e3M6NDoibmFtZSI7czoxMDoibWluYWdlZnJvbSI7czoyOiJpZCI7czoxMDoibWluYWdlZnJvbSI7czo1OiJ2YWx1ZSI7czowOiIiO3M6NToibGFiZWwiO2E6Mjp7czo0OiJ0ZXh0IjtzOjI4OiJMaW1pdGUgaW5mZXJpb3JlIGV0w6AgbWluaW1hIjtzOjg6InBvc2l0aW9uIjtzOjQ6ImxlZnQiO31zOjg6InN1YmxhYmVsIjtzOjQwOiJFdMOgIG1pbmltYSBzdXBlcmlvcmUgYWwgdmFsb3JlIGluc2VyaXRvIjtzOjExOiJwbGFjZWhvbGRlciI7czowOiIiO3M6OToibWF4bGVuZ3RoIjtzOjE6IjMiO3M6NDoic2l6ZSI7czowOiIiO3M6NToiY2xhc3MiO3M6MDoiIjtzOjU6InRpdGxlIjtzOjA6IiI7czo1OiJzdHlsZSI7czowOiIiO3M6MTU6IjpkYXRhLWlucHV0bWFzayI7czoxOToiJ2FsaWFzJyA6ICdpbnRlZ2VyJyI7czo2OiJwYXJhbXMiO3M6MDoiIjtzOjE2OiI6ZGF0YS1sb2FkLXN0YXRlIjtzOjA6IiI7czoxMzoiOmRhdGEtdG9vbHRpcCI7czowOiIiO3M6NDoidHlwZSI7czo0OiJ0ZXh0IjtzOjEyOiJjb250YWluZXJfaWQiO3M6MToiMCI7czoxMDoidmFsaWRhdGlvbiI7YToxMzp7czo4OiJyZXF1aXJlZCI7czowOiIiO3M6NToiYWxwaGEiO3M6MDoiIjtzOjg6ImFscGhhbnVtIjtzOjA6IiI7czo1OiJkaWdpdCI7czowOiIiO3M6Nzoibm9kaWdpdCI7czowOiIiO3M6Nzoibm9zcGFjZSI7czowOiIiO3M6NjoibnVtYmVyIjtzOjE6IjEiO3M6NToiZW1haWwiO3M6MDoiIjtzOjU6InBob25lIjtzOjA6IiI7czoxMToicGhvbmVfaW50ZXIiO3M6MDoiIjtzOjM6InVybCI7czowOiIiO3M6NzoiY29uZmlybSI7czowOiIiO3M6NjoiY3VzdG9tIjtzOjA6IiI7fX1pOjI4O2E6MTg6e3M6NDoibmFtZSI7czoxMDoibWF4YWdlZnJvbSI7czoyOiJpZCI7czoxMDoibWF4YWdlZnJvbSI7czo1OiJ2YWx1ZSI7czowOiIiO3M6NToibGFiZWwiO2E6Mjp7czo0OiJ0ZXh0IjtzOjI4OiJMaW1pdGUgc3VwZXJpb3JlIGV0w6AgbWluaW1hIjtzOjg6InBvc2l0aW9uIjtzOjQ6ImxlZnQiO31zOjg6InN1YmxhYmVsIjtzOjQwOiJFdMOgIG1pbmltYSBzdXBlcmlvcmUgYWwgdmFsb3JlIGluc2VyaXRvIjtzOjExOiJwbGFjZWhvbGRlciI7czowOiIiO3M6OToibWF4bGVuZ3RoIjtzOjE6IjMiO3M6NDoic2l6ZSI7czowOiIiO3M6NToiY2xhc3MiO3M6MDoiIjtzOjU6InRpdGxlIjtzOjA6IiI7czo1OiJzdHlsZSI7czowOiIiO3M6MTU6IjpkYXRhLWlucHV0bWFzayI7czoxOToiJ2FsaWFzJyA6ICdpbnRlZ2VyJyI7czo2OiJwYXJhbXMiO3M6MDoiIjtzOjE2OiI6ZGF0YS1sb2FkLXN0YXRlIjtzOjA6IiI7czoxMzoiOmRhdGEtdG9vbHRpcCI7czowOiIiO3M6NDoidHlwZSI7czo0OiJ0ZXh0IjtzOjEyOiJjb250YWluZXJfaWQiO3M6MToiMCI7czoxMDoidmFsaWRhdGlvbiI7YToxMzp7czo4OiJyZXF1aXJlZCI7czowOiIiO3M6NToiYWxwaGEiO3M6MDoiIjtzOjg6ImFscGhhbnVtIjtzOjA6IiI7czo1OiJkaWdpdCI7czowOiIiO3M6Nzoibm9kaWdpdCI7czowOiIiO3M6Nzoibm9zcGFjZSI7czowOiIiO3M6NjoibnVtYmVyIjtzOjE6IjEiO3M6NToiZW1haWwiO3M6MDoiIjtzOjU6InBob25lIjtzOjA6IiI7czoxMToicGhvbmVfaW50ZXIiO3M6MDoiIjtzOjM6InVybCI7czowOiIiO3M6NzoiY29uZmlybSI7czowOiIiO3M6NjoiY3VzdG9tIjtzOjA6IiI7fX1pOjI5O2E6MTg6e3M6NDoibmFtZSI7czo4OiJtaW5hZ2V0byI7czoyOiJpZCI7czo4OiJtaW5hZ2V0byI7czo1OiJ2YWx1ZSI7czowOiIiO3M6NToibGFiZWwiO2E6Mjp7czo0OiJ0ZXh0IjtzOjI5OiJMaW1pdGUgaW5mZXJpb3JlIGV0w6AgbWFzc2ltYSI7czo4OiJwb3NpdGlvbiI7czo0OiJsZWZ0Ijt9czo4OiJzdWJsYWJlbCI7czo0MToiRXTDoCBtYXNzaW1hIHN1cGVyaW9yZSBhbCB2YWxvcmUgaW5zZXJpdG8iO3M6MTE6InBsYWNlaG9sZGVyIjtzOjA6IiI7czo5OiJtYXhsZW5ndGgiO3M6MToiMyI7czo0OiJzaXplIjtzOjA6IiI7czo1OiJjbGFzcyI7czowOiIiO3M6NToidGl0bGUiO3M6MDoiIjtzOjU6InN0eWxlIjtzOjA6IiI7czoxNToiOmRhdGEtaW5wdXRtYXNrIjtzOjE5OiInYWxpYXMnIDogJ2ludGVnZXInIjtzOjY6InBhcmFtcyI7czowOiIiO3M6MTY6IjpkYXRhLWxvYWQtc3RhdGUiO3M6MDoiIjtzOjEzOiI6ZGF0YS10b29sdGlwIjtzOjA6IiI7czo0OiJ0eXBlIjtzOjQ6InRleHQiO3M6MTI6ImNvbnRhaW5lcl9pZCI7czoxOiIwIjtzOjEwOiJ2YWxpZGF0aW9uIjthOjEzOntzOjg6InJlcXVpcmVkIjtzOjA6IiI7czo1OiJhbHBoYSI7czowOiIiO3M6ODoiYWxwaGFudW0iO3M6MDoiIjtzOjU6ImRpZ2l0IjtzOjA6IiI7czo3OiJub2RpZ2l0IjtzOjA6IiI7czo3OiJub3NwYWNlIjtzOjA6IiI7czo2OiJudW1iZXIiO3M6MToiMSI7czo1OiJlbWFpbCI7czowOiIiO3M6NToicGhvbmUiO3M6MDoiIjtzOjExOiJwaG9uZV9pbnRlciI7czowOiIiO3M6MzoidXJsIjtzOjA6IiI7czo3OiJjb25maXJtIjtzOjA6IiI7czo2OiJjdXN0b20iO3M6MDoiIjt9fWk6MzA7YToxODp7czo0OiJuYW1lIjtzOjg6Im1heGFnZXRvIjtzOjI6ImlkIjtzOjg6Im1heGFnZXRvIjtzOjU6InZhbHVlIjtzOjA6IiI7czo1OiJsYWJlbCI7YToyOntzOjQ6InRleHQiO3M6Mjk6IkxpbWl0ZSBzdXBlcmlvcmUgZXTDoCBtYXNzaW1hIjtzOjg6InBvc2l0aW9uIjtzOjQ6ImxlZnQiO31zOjg6InN1YmxhYmVsIjtzOjQxOiJFdMOgIG1hc3NpbWEgaW5mZXJpb3JlIGFsIHZhbG9yZSBpbnNlcml0byI7czoxMToicGxhY2Vob2xkZXIiO3M6MDoiIjtzOjk6Im1heGxlbmd0aCI7czoxOiIzIjtzOjQ6InNpemUiO3M6MDoiIjtzOjU6ImNsYXNzIjtzOjA6IiI7czo1OiJ0aXRsZSI7czowOiIiO3M6NToic3R5bGUiO3M6MDoiIjtzOjE1OiI6ZGF0YS1pbnB1dG1hc2siO3M6MTk6IidhbGlhcycgOiAnaW50ZWdlciciO3M6NjoicGFyYW1zIjtzOjA6IiI7czoxNjoiOmRhdGEtbG9hZC1zdGF0ZSI7czowOiIiO3M6MTM6IjpkYXRhLXRvb2x0aXAiO3M6MDoiIjtzOjQ6InR5cGUiO3M6NDoidGV4dCI7czoxMjoiY29udGFpbmVyX2lkIjtzOjE6IjAiO3M6MTA6InZhbGlkYXRpb24iO2E6MTM6e3M6ODoicmVxdWlyZWQiO3M6MDoiIjtzOjU6ImFscGhhIjtzOjA6IiI7czo4OiJhbHBoYW51bSI7czowOiIiO3M6NToiZGlnaXQiO3M6MDoiIjtzOjc6Im5vZGlnaXQiO3M6MDoiIjtzOjc6Im5vc3BhY2UiO3M6MDoiIjtzOjY6Im51bWJlciI7czoxOiIxIjtzOjU6ImVtYWlsIjtzOjA6IiI7czo1OiJwaG9uZSI7czowOiIiO3M6MTE6InBob25lX2ludGVyIjtzOjA6IiI7czozOiJ1cmwiO3M6MDoiIjtzOjc6ImNvbmZpcm0iO3M6MDoiIjtzOjY6ImN1c3RvbSI7czowOiIiO319aToxNjthOjEwOntzOjExOiJyZW5kZXJfdHlwZSI7czo2OiJzdWJtaXQiO3M6NDoibmFtZSI7czo4OiJidXR0b24xNiI7czoyOiJpZCI7czo4OiJidXR0b24xNiI7czo0OiJ0eXBlIjtzOjY6InN1Ym1pdCI7czo1OiJ2YWx1ZSI7czo1OiJDZXJjYSI7czo1OiJjbGFzcyI7czoxNToiYnRuIGJ0bi1kZWZhdWx0IjtzOjU6InN0eWxlIjtzOjA6IiI7czo2OiJwYXJhbXMiO3M6MDoiIjtzOjE2OiI6ZGF0YS1sb2FkLXN0YXRlIjtzOjA6IiI7czoxMjoiY29udGFpbmVyX2lkIjtzOjE6IjAiO319czozOiJETkEiO2E6Mjp7czo0OiJsb2FkIjthOjQ6e3M6OToiZGJfcmVhZF8xIjthOjI6e3M6NToiZm91bmQiO2E6MTp7czoxMzoiY3VzdG9tX2NvZGVfNCI7czowOiIiO31zOjk6Im5vdF9mb3VuZCI7czowOiIiO31zOjEzOiJjdXN0b21fY29kZV8yIjtzOjA6IiI7czo2OiJodG1sXzAiO3M6MDoiIjtzOjEzOiJjdXN0b21fY29kZV81IjtzOjA6IiI7fXM6Njoic3VibWl0IjthOjE6e3M6MTM6ImN1c3RvbV9jb2RlXzMiO3M6MDoiIjt9fXM6MTQ6ImFjdGlvbnNfY29uZmlnIjthOjY6e2k6NDthOjI6e3M6MTI6ImFjdGlvbl9sYWJlbCI7czo1NzoiTW9kZWxsYXppb25lIGRlbGxlIG9wdGlvbnMgY29uIGNhdGVnb3JpZSBlIHNvdHRvY2F0ZWdvcmllIjtzOjc6ImNvbnRlbnQiO3M6ODg6Ijw/cGhwDQokZW0gPSBuZXcgRXZlbnRzTW9kZWwoKTsNCiRlbS0+Y2F0ZWdvcnlsaXN0KCRmb3JtLT5kYXRhWydjYXRlZ29yaWVzbGlzdCddKTsNCj8+DQoiO31pOjE7YToxOTp7czoxMjoiYWN0aW9uX2xhYmVsIjtzOjI3OiJDYXJpY2FtZW50byBkZWxsZSBjYXRlZ29yaWUiO3M6NzoiZW5hYmxlZCI7czoxOiIxIjtzOjk6InRhYmxlbmFtZSI7czoxMDoiY2F0ZWdvcmllcyI7czoxMDoibXVsdGlfcmVhZCI7czoxOiIxIjtzOjE4OiJsb2FkX3VuZGVyX21vZGVsaWQiO3M6MToiMSI7czo4OiJtb2RlbF9pZCI7czoxNDoiY2F0ZWdvcmllc2xpc3QiO3M6NjoiZmllbGRzIjtzOjMyOiJpZCxuYW1lLGlkc3VwZXJjYXRlZ29yeSxsYW5ndWFnZSI7czo1OiJvcmRlciI7czoyOiJpZCI7czo1OiJncm91cCI7czowOiIiO3M6MTA6ImNvbmRpdGlvbnMiO3M6MDoiIjtzOjE2OiJlbmFibGVfcmVsYXRpb25zIjtzOjE6IjAiO3M6OToicmVsYXRpb25zIjthOjE6e2k6MDthOjQ6e3M6NToibW9kZWwiO3M6MDoiIjtzOjk6InRhYmxlbmFtZSI7czowOiIiO3M6NDoidHlwZSI7czo2OiJoYXNPbmUiO3M6NDoiZmtleSI7czowOiIiO319czoxMDoibmRiX2VuYWJsZSI7czoxOiIwIjtzOjEwOiJuZGJfZHJpdmVyIjtzOjU6Im15c3FsIjtzOjg6Im5kYl9ob3N0IjtzOjk6ImxvY2FsaG9zdCI7czoxMjoibmRiX2RhdGFiYXNlIjtzOjA6IiI7czo4OiJuZGJfdXNlciI7czowOiIiO3M6MTI6Im5kYl9wYXNzd29yZCI7czowOiIiO3M6MTA6Im5kYl9wcmVmaXgiO3M6NDoiam9zXyI7fWk6MjthOjI6e3M6MTI6ImFjdGlvbl9sYWJlbCI7czoyMDoiQ2FyaWNhbWVudG8gZGVpIGRhdGkiO3M6NzoiY29udGVudCI7czo5MzoiPD9waHANCiRlYyA9VHJhdmVsOjpnZXRJbnN0YW5jZSgpLT5nZXQoJ2V2ZW50Q29udHJvbGxlcicpOw0KJGVjLT5pbml0U2VhcmNoRm9ybSgkZm9ybSk7DQo/Pg0KIjt9aTowO2E6MTE6e3M6NDoicGFnZSI7czoxOiIxIjtzOjEyOiJzdWJtaXRfZXZlbnQiO3M6Njoic3VibWl0IjtzOjExOiJmb3JtX21ldGhvZCI7czo0OiJmaWxlIjtzOjEwOiJhY3Rpb25fdXJsIjtzOjA6IiI7czoxMDoiZm9ybV9jbGFzcyI7czoxMDoiY2hyb25vZm9ybSI7czoxNToiZm9ybV90YWdfYXR0YWNoIjtzOjA6IiI7czoyNDoicmVxdWlyZWRfbGFiZWxzX2lkZW50aWZ5IjtzOjE6IjEiO3M6MTI6InJlbGF0aXZlX3VybCI7czoxOiIxIjtzOjExOiJhamF4X3N1Ym1pdCI7czoxOiIwIjtzOjEzOiJhZGRfZm9ybV90YWdzIjtzOjE6IjEiO3M6OToieGh0bWxfdXJsIjtzOjE6IjAiO31pOjU7YToyOntzOjEyOiJhY3Rpb25fbGFiZWwiO3M6MzE6Ik1vZGlmaWNhIGtleXMgY2F0ZWdvcnkgc2VsZWN0b3IiO3M6NzoiY29udGVudCI7czoyNDk6IjxzY3JpcHQ+DQpqUXVlcnkoIm9wdGlvblt2YWx1ZSo9J3N1cGNfJ10iKS5lYWNoKGZ1bmN0aW9uKCBpbmRleCApIHsNCiAgIGpRdWVyeSh0aGlzKS5jc3MoJ2JhY2tncm91bmQtY29sb3InLCAnIzk5OScpOw0KICAgalF1ZXJ5KHRoaXMpLmNzcygnY29sb3InLCAnYmxhY2snKTsNCiAgLy8gIGpRdWVyeSh0aGlzKS5hdHRyKCd2YWx1ZScsIGpRdWVyeSh0aGlzKS5hdHRyKCd2YWx1ZScpLnN1YnN0cig1KSk7ICAgDQp9KTsNCjwvc2NyaXB0PiI7fWk6MzthOjI6e3M6MTI6ImFjdGlvbl9sYWJlbCI7czoyMjoiU2FsdmF0YWdnaW8gZSByZWRpcmVjdCI7czo3OiJjb250ZW50IjtzOjE4ODoiPD9waHANCi8vdmFyX2R1bXAoJF9QT1NUKTsgZGllKCk7DQpKTG9hZGVyOjpyZWdpc3RlcignRXZlbnRzQ29udHJvbGxlcicsICd2ZW5kb3IvY29udHJvbC9FdmVudHNDb250cm9sbGVyLnBocCcpOw0KJGNvbnRyb2xsZXIgPSBuZXcgRXZlbnRzQ29udHJvbGxlcigpOw0KJGNvbnRyb2xsZXItPnNhdmVTZWFyY2hQYXJhbSgpOw0KPz4iO319czoxNDoiZGJfZmllbGRzX2xpc3QiO2E6MTp7czoxMDoiY2F0ZWdvcmllcyI7czowOiIiO319', 1, 'Eventi', 1, '<div class="form-group gcore-form-row" id="form-row-status"><label for="status" class="control-label gcore-label-left">Stato</label>\n<div class="gcore-input gcore-display-table" id="fin-status"><select name="status" id="status" size="" class="form-control A" title="" style="" data-load-state="" data-tooltip="">\n<option value="">Non Impostato</option>\n<option value="0">Disabilitato</option>\n<option value="1">Abilitato</option>\n</select><span class="help-block">Sarà possibile accedere all''evento lato client solo se è selezionata l''opzione "Abilitato"</span></div></div><div class="form-group gcore-form-row" id="form-row-title"><label for="title" class="control-label gcore-label-left">Nome dell''evento</label>\n<div class="gcore-input gcore-display-table" id="fin-title"><input name="title" id="title" value="" placeholder="Titolo dell&#039;evento" maxlength="30" size="" class="form-control A" title="" style="" data-inputmask="" data-load-state="" data-tooltip="" type="text" /><span class="help-block">Inserire il titolo dell''evento</span></div></div><?php\n$keys = \\GCore\\Libs\\Arr::getVal(\\GCore\\Libs\\Arr::getVal($form->data, explode(".", "categorieslist")), explode(".", "[n].id"));\n$values = \\GCore\\Libs\\Arr::getVal(\\GCore\\Libs\\Arr::getVal($form->data, explode(".", "categorieslist")), explode(".", "[n].name"));\n$options = array_combine($keys, $values);\n$field = array (\n  ''name'' => ''categoria'',\n  ''id'' => ''categoria'',\n  ''options'' => \n  array (\n  ),\n  ''empty'' => ''Non Impostato'',\n  ''values'' => \n  array (\n  ),\n  ''label'' => \n  array (\n    ''text'' => ''Categoria'',\n    ''position'' => ''left'',\n  ),\n  ''sublabel'' => ''Categoria di appartenenza dell\\''evento'',\n  ''multiple'' => ''0'',\n  ''size'' => '''',\n  ''class'' => '''',\n  ''title'' => '''',\n  ''style'' => '''',\n  ''params'' => '''',\n  '':data-load-state'' => '''',\n  '':data-tooltip'' => '''',\n  ''type'' => ''dropdown'',\n  ''container_id'' => ''0'',\n);\n$field["options"] = $options;\necho \\GCore\\Helpers\\Html::formLine($field["name"], $field);\n?><div class="form-group gcore-form-row" id="form-row-dateinf"><label for="dateinf" class="control-label gcore-label-left">Dalla data</label>\n<div class="gcore-input gcore-display-table" id="fin-dateinf"><input name="dateinf" id="dateinf" value="" placeholder="" maxlength="30" size="" class="form-control A" title="" style="" data-inputmask="&#039;alias&#039; : &#039;dd-mm-yyyy&#039;" data-load-state="" data-tooltip="" type="text" /><span class="help-block">Limite inferiore dell''intervallo di tempo da selezionare</span></div></div><div class="form-group gcore-form-row" id="form-row-datesup"><label for="datesup" class="control-label gcore-label-left">Alla data</label>\n<div class="gcore-input gcore-display-table" id="fin-datesup"><input name="datesup" id="datesup" value="" placeholder="" maxlength="30" size="" class="form-control A" title="" style="" data-inputmask="&#039;alias&#039; : &#039;dd-mm-yyyy&#039;" data-load-state="" data-tooltip="" type="text" /><span class="help-block">Limite superiore dell''intervallo di tempo da considerare</span></div></div><div class="form-group gcore-form-row" id="form-row-sex"><label for="sex" class="control-label gcore-label-left">Sesso</label>\n<div class="gcore-input gcore-display-table" id="fin-sex"><select name="sex" id="sex" size="" class="form-control A" title="" style="" data-load-state="" data-tooltip="">\n<option value="">Non Impostato</option>\n<option value="0">Solo Uomini</option>\n<option value="1">Solo Donne</option>\n<option value="3">Donne e Uomini</option>\n</select><span class="help-block">Genere per cui un evento è pianificato</span></div></div><div class="form-group gcore-form-row" id="form-row-scope"><label for="scope" class="control-label gcore-label-left">Accesso</label>\n<div class="gcore-input gcore-display-table" id="fin-scope"><select name="scope" id="scope" size="" class="form-control A" title="" style="" data-load-state="" data-tooltip="">\n<option value="">Non Impostato</option>\n<option value="0">Pubblico</option>\n<option value="1">Privato</option>\n<option value="2">Friends Only</option>\n</select><span class="help-block">Selezionare la tipologia di evento</span></div></div><div class="form-group gcore-form-row" id="form-row-description"><label for="description" class="control-label gcore-label-left">Descrizione</label>\n<div class="gcore-input gcore-display-table" id="fin-description"><textarea name="description" id="description" placeholder="Es: Lorem ipsum, lorem, ipsum, token1, token" rows="5" cols="40" class="form-control A" title="" style="" data-wysiwyg="0" data-load-state="" data-tooltip=""></textarea><span class="help-block">Sequenza di token che possono trovarsi nella descrizione dell''evento separati da una virgola</span></div></div><div class="form-group gcore-form-row" id="form-row-town"><label for="town" class="control-label gcore-label-left">Città</label>\n<div class="gcore-input gcore-display-table" id="fin-town"><input name="town" id="town" value="" placeholder="Città" maxlength="40" size="" class="form-control A" title="" style="" data-inputmask="" data-load-state="" data-tooltip="" type="text" /><span class="help-block">Inserire il nome della città in cui si svolgerà l''evento</span></div></div><div class="form-group gcore-form-row" id="form-row-minpartecipant"><label for="minpartecipant" class="control-label gcore-label-left">Limite inferiore partecipanti</label>\n<div class="gcore-input gcore-display-table" id="fin-minpartecipant"><input name="minpartecipant" id="minpartecipant" value="" placeholder="" maxlength="3" size="" class="validate[&#039;number&#039;] form-control A" title="" style="" data-inputmask="&#039;alias&#039; : &#039;integer&#039;" data-load-state="" data-tooltip="" type="text" /><span class="help-block">Numero di partecipanti superiore al valore inserito</span></div></div><div class="form-group gcore-form-row" id="form-row-maxpartecipant"><label for="maxpartecipant" class="control-label gcore-label-left">Limite superiore partecipanti</label>\n<div class="gcore-input gcore-display-table" id="fin-maxpartecipant"><input name="maxpartecipant" id="maxpartecipant" value="" placeholder="" maxlength="3" size="" class="validate[&#039;number&#039;] form-control A" title="" style="" data-inputmask="&#039;alias&#039; : &#039;integer&#039;" data-load-state="" data-tooltip="" type="text" /><span class="help-block">Numero di partecipanti inferiore al valore inserito</span></div></div><div class="form-group gcore-form-row" id="form-row-minplace"><label for="minplace" class="control-label gcore-label-left">Limite inferiore posti disponibili</label>\n<div class="gcore-input gcore-display-table" id="fin-minplace"><input name="minplace" id="minplace" value="" placeholder="" maxlength="3" size="" class="validate[&#039;number&#039;] form-control A" title="" style="" data-inputmask="&#039;alias&#039; : &#039;integer&#039;" data-load-state="" data-tooltip="" type="text" /><span class="help-block">Numero di posti disponibili superiore al valore inserito</span></div></div><div class="form-group gcore-form-row" id="form-row-maxplace"><label for="maxplace" class="control-label gcore-label-left">Limite superiore posti disponibili</label>\n<div class="gcore-input gcore-display-table" id="fin-maxplace"><input name="maxplace" id="maxplace" value="" placeholder="" maxlength="3" size="" class="validate[&#039;number&#039;] form-control A" title="" style="" data-inputmask="&#039;alias&#039; : &#039;integer&#039;" data-load-state="" data-tooltip="" type="text" /><span class="help-block">Numero di posti disponibili inferiore al valore inserito</span></div></div><div class="form-group gcore-form-row" id="form-row-minagefrom"><label for="minagefrom" class="control-label gcore-label-left">Limite inferiore età minima</label>\n<div class="gcore-input gcore-display-table" id="fin-minagefrom"><input name="minagefrom" id="minagefrom" value="" placeholder="" maxlength="3" size="" class="validate[&#039;number&#039;] form-control A" title="" style="" data-inputmask="&#039;alias&#039; : &#039;integer&#039;" data-load-state="" data-tooltip="" type="text" /><span class="help-block">Età minima superiore al valore inserito</span></div></div><div class="form-group gcore-form-row" id="form-row-maxagefrom"><label for="maxagefrom" class="control-label gcore-label-left">Limite superiore età minima</label>\n<div class="gcore-input gcore-display-table" id="fin-maxagefrom"><input name="maxagefrom" id="maxagefrom" value="" placeholder="" maxlength="3" size="" class="validate[&#039;number&#039;] form-control A" title="" style="" data-inputmask="&#039;alias&#039; : &#039;integer&#039;" data-load-state="" data-tooltip="" type="text" /><span class="help-block">Età minima superiore al valore inserito</span></div></div><div class="form-group gcore-form-row" id="form-row-minageto"><label for="minageto" class="control-label gcore-label-left">Limite inferiore età massima</label>\n<div class="gcore-input gcore-display-table" id="fin-minageto"><input name="minageto" id="minageto" value="" placeholder="" maxlength="3" size="" class="validate[&#039;number&#039;] form-control A" title="" style="" data-inputmask="&#039;alias&#039; : &#039;integer&#039;" data-load-state="" data-tooltip="" type="text" /><span class="help-block">Età massima superiore al valore inserito</span></div></div><div class="form-group gcore-form-row" id="form-row-maxageto"><label for="maxageto" class="control-label gcore-label-left">Limite superiore età massima</label>\n<div class="gcore-input gcore-display-table" id="fin-maxageto"><input name="maxageto" id="maxageto" value="" placeholder="" maxlength="3" size="" class="validate[&#039;number&#039;] form-control A" title="" style="" data-inputmask="&#039;alias&#039; : &#039;integer&#039;" data-load-state="" data-tooltip="" type="text" /><span class="help-block">Età massima inferiore al valore inserito</span></div></div><div class="form-group gcore-form-row" id="form-row-button16"><div class="gcore-input gcore-display-table" id="fin-button16"><input name="button16" id="button16" type="submit" value="Cerca" class="btn btn-default form-control A" style="" data-load-state="" /></div></div>');
INSERT INTO `zcp8_chronoengine_chronoforms` (`id`, `title`, `params`, `extras`, `published`, `app`, `form_type`, `content`) VALUES
(3, 'user_update', '{"description":"Form per la modifica dei dati anagrafici associati ad un utente","setup":"0","theme":"bootstrap3","tight_layout":"0","rtl_support":"0","labels_right_aligned":"0","labels_auto_width":"0","js_validation_language":""}', 'YTo0OntzOjY6ImZpZWxkcyI7YToxMDp7aToxO2E6MjA6e3M6NDoibmFtZSI7czo2OiJzdGF0dXMiO3M6MjoiaWQiO3M6Njoic3RhdHVzIjtzOjc6Im9wdGlvbnMiO3M6MTk6IjA9QmFubmF0bw0KMT1BdHRpdm8iO3M6NToiZW1wdHkiO3M6MDoiIjtzOjY6InZhbHVlcyI7czowOiIiO3M6NToibGFiZWwiO2E6Mjp7czo0OiJ0ZXh0IjtzOjE3OiJTdGF0byBkZWxsJ3V0ZW50ZSI7czo4OiJwb3NpdGlvbiI7czo0OiJsZWZ0Ijt9czo4OiJzdWJsYWJlbCI7czo2NToiR2xpIHV0ZW50aSBiYW5uYXRpIG5vbiBwb3Nzb25vIHVzYXJlIGkgc2Vydml6aSBvZmZlcnRpIGRhaSBjbGllbnQiO3M6ODoibXVsdGlwbGUiO3M6MToiMCI7czo0OiJzaXplIjtzOjA6IiI7czo1OiJjbGFzcyI7czowOiIiO3M6NToidGl0bGUiO3M6MDoiIjtzOjU6InN0eWxlIjtzOjA6IiI7czo2OiJwYXJhbXMiO3M6MDoiIjtzOjE2OiI6ZGF0YS1sb2FkLXN0YXRlIjtzOjA6IiI7czoxMzoiOmRhdGEtdG9vbHRpcCI7czowOiIiO3M6NDoidHlwZSI7czo4OiJkcm9wZG93biI7czoxMjoiY29udGFpbmVyX2lkIjtzOjE6IjAiO3M6MTA6InZhbGlkYXRpb24iO2E6Mzp7czo4OiJyZXF1aXJlZCI7czowOiIiO3M6NzoiY29uZmlybSI7czowOiIiO3M6NjoiY3VzdG9tIjtzOjA6IiI7fXM6MTI6ImR5bmFtaWNfZGF0YSI7YTo0OntzOjc6ImVuYWJsZWQiO3M6MDoiIjtzOjk6ImRhdGFfcGF0aCI7czowOiIiO3M6OToidmFsdWVfa2V5IjtzOjA6IiI7czo4OiJ0ZXh0X2tleSI7czowOiIiO31zOjY6ImV2ZW50cyI7YToxOntpOjA7YTo1OntzOjg6Im9wZXJhdG9yIjtzOjE6Ij0iO3M6NToic3RhdGUiO3M6MDoiIjtzOjY6ImFjdGlvbiI7czowOiIiO3M6NjoidGFyZ2V0IjtzOjA6IiI7czo3OiJvcHRpb25zIjtzOjA6IiI7fX19aToyO2E6MTg6e3M6NDoibmFtZSI7czo0OiJuYW1lIjtzOjI6ImlkIjtzOjQ6Im5hbWUiO3M6NToidmFsdWUiO3M6MDoiIjtzOjU6ImxhYmVsIjthOjI6e3M6NDoidGV4dCI7czo0OiJOb21lIjtzOjg6InBvc2l0aW9uIjtzOjQ6ImxlZnQiO31zOjg6InN1YmxhYmVsIjtzOjI4OiJOb21lIGNoZSBjb21wYXJlIG5lbCBwcm9maWxvIjtzOjExOiJwbGFjZWhvbGRlciI7czoxNToiUGFvbG8gRnJhbmNlc2NvIjtzOjk6Im1heGxlbmd0aCI7czoyOiI0MCI7czo0OiJzaXplIjtzOjA6IiI7czo1OiJjbGFzcyI7czowOiIiO3M6NToidGl0bGUiO3M6MDoiIjtzOjU6InN0eWxlIjtzOjA6IiI7czoxNToiOmRhdGEtaW5wdXRtYXNrIjtzOjA6IiI7czo2OiJwYXJhbXMiO3M6MDoiIjtzOjE2OiI6ZGF0YS1sb2FkLXN0YXRlIjtzOjA6IiI7czoxMzoiOmRhdGEtdG9vbHRpcCI7czowOiIiO3M6NDoidHlwZSI7czo0OiJ0ZXh0IjtzOjEyOiJjb250YWluZXJfaWQiO3M6MToiMCI7czoxMDoidmFsaWRhdGlvbiI7YToxMzp7czo4OiJyZXF1aXJlZCI7czoxOiIxIjtzOjU6ImFscGhhIjtzOjA6IiI7czo4OiJhbHBoYW51bSI7czoxOiIxIjtzOjU6ImRpZ2l0IjtzOjA6IiI7czo3OiJub2RpZ2l0IjtzOjA6IiI7czo3OiJub3NwYWNlIjtzOjA6IiI7czo2OiJudW1iZXIiO3M6MDoiIjtzOjU6ImVtYWlsIjtzOjA6IiI7czo1OiJwaG9uZSI7czowOiIiO3M6MTE6InBob25lX2ludGVyIjtzOjA6IiI7czozOiJ1cmwiO3M6MDoiIjtzOjc6ImNvbmZpcm0iO3M6MDoiIjtzOjY6ImN1c3RvbSI7czowOiIiO319aTozO2E6MTg6e3M6NDoibmFtZSI7czo3OiJzdXJuYW1lIjtzOjI6ImlkIjtzOjc6InN1cm5hbWUiO3M6NToidmFsdWUiO3M6MDoiIjtzOjU6ImxhYmVsIjthOjI6e3M6NDoidGV4dCI7czo3OiJDb2dub21lIjtzOjg6InBvc2l0aW9uIjtzOjQ6ImxlZnQiO31zOjg6InN1YmxhYmVsIjtzOjMxOiJDb2dub21lIGNoZSBjb21wYXJlIG5lbCBwcm9maWxvIjtzOjExOiJwbGFjZWhvbGRlciI7czo4OiJEZSBSb3NzaSI7czo5OiJtYXhsZW5ndGgiO3M6MjoiNDAiO3M6NDoic2l6ZSI7czowOiIiO3M6NToiY2xhc3MiO3M6MDoiIjtzOjU6InRpdGxlIjtzOjA6IiI7czo1OiJzdHlsZSI7czowOiIiO3M6MTU6IjpkYXRhLWlucHV0bWFzayI7czowOiIiO3M6NjoicGFyYW1zIjtzOjA6IiI7czoxNjoiOmRhdGEtbG9hZC1zdGF0ZSI7czowOiIiO3M6MTM6IjpkYXRhLXRvb2x0aXAiO3M6MDoiIjtzOjQ6InR5cGUiO3M6NDoidGV4dCI7czoxMjoiY29udGFpbmVyX2lkIjtzOjE6IjAiO3M6MTA6InZhbGlkYXRpb24iO2E6MTM6e3M6ODoicmVxdWlyZWQiO3M6MToiMSI7czo1OiJhbHBoYSI7czowOiIiO3M6ODoiYWxwaGFudW0iO3M6MToiMSI7czo1OiJkaWdpdCI7czowOiIiO3M6Nzoibm9kaWdpdCI7czowOiIiO3M6Nzoibm9zcGFjZSI7czowOiIiO3M6NjoibnVtYmVyIjtzOjA6IiI7czo1OiJlbWFpbCI7czowOiIiO3M6NToicGhvbmUiO3M6MDoiIjtzOjExOiJwaG9uZV9pbnRlciI7czowOiIiO3M6MzoidXJsIjtzOjA6IiI7czo3OiJjb25maXJtIjtzOjA6IiI7czo2OiJjdXN0b20iO3M6MDoiIjt9fWk6MTI7YToxODp7czo0OiJuYW1lIjtzOjU6ImVtYWlsIjtzOjI6ImlkIjtzOjU6ImVtYWlsIjtzOjU6InZhbHVlIjtzOjA6IiI7czo1OiJsYWJlbCI7YToyOntzOjQ6InRleHQiO3M6NToiRW1haWwiO3M6ODoicG9zaXRpb24iO3M6NDoibGVmdCI7fXM6ODoic3VibGFiZWwiO3M6MzI6IkluZGlyaXp6byBlbWFpbCBkaSByZWdpc3RyYXppb25lIjtzOjExOiJwbGFjZWhvbGRlciI7czowOiIiO3M6OToibWF4bGVuZ3RoIjtzOjA6IiI7czo0OiJzaXplIjtzOjA6IiI7czo1OiJjbGFzcyI7czowOiIiO3M6NToidGl0bGUiO3M6MDoiIjtzOjU6InN0eWxlIjtzOjA6IiI7czoxNToiOmRhdGEtaW5wdXRtYXNrIjtzOjA6IiI7czo2OiJwYXJhbXMiO3M6MDoiIjtzOjE2OiI6ZGF0YS1sb2FkLXN0YXRlIjtzOjA6IiI7czoxMzoiOmRhdGEtdG9vbHRpcCI7czowOiIiO3M6NDoidHlwZSI7czo0OiJ0ZXh0IjtzOjEyOiJjb250YWluZXJfaWQiO3M6MToiMCI7czoxMDoidmFsaWRhdGlvbiI7YToxMzp7czo4OiJyZXF1aXJlZCI7czoxOiIxIjtzOjU6ImFscGhhIjtzOjA6IiI7czo4OiJhbHBoYW51bSI7czowOiIiO3M6NToiZGlnaXQiO3M6MDoiIjtzOjc6Im5vZGlnaXQiO3M6MDoiIjtzOjc6Im5vc3BhY2UiO3M6MDoiIjtzOjY6Im51bWJlciI7czowOiIiO3M6NToiZW1haWwiO3M6MToiMSI7czo1OiJwaG9uZSI7czowOiIiO3M6MTE6InBob25lX2ludGVyIjtzOjA6IiI7czozOiJ1cmwiO3M6MDoiIjtzOjc6ImNvbmZpcm0iO3M6MDoiIjtzOjY6ImN1c3RvbSI7czowOiIiO319aTo1O2E6MTg6e3M6NDoibmFtZSI7czo5OiJiaXJ0aGRhdGUiO3M6MjoiaWQiO3M6OToiYmlydGhkYXRlIjtzOjU6InZhbHVlIjtzOjA6IiI7czo1OiJsYWJlbCI7YToyOntzOjQ6InRleHQiO3M6MTU6IkRhdGEgZGkgbmFzY2l0YSI7czo4OiJwb3NpdGlvbiI7czo0OiJsZWZ0Ijt9czo4OiJzdWJsYWJlbCI7czoyNzoiRGF0YSBkaSBuYXNjaXRhIGRlbGwndXRlbnRlIjtzOjExOiJwbGFjZWhvbGRlciI7czowOiIiO3M6OToibWF4bGVuZ3RoIjtzOjA6IiI7czo0OiJzaXplIjtzOjA6IiI7czo1OiJjbGFzcyI7czowOiIiO3M6NToidGl0bGUiO3M6MDoiIjtzOjU6InN0eWxlIjtzOjA6IiI7czoxNToiOmRhdGEtaW5wdXRtYXNrIjtzOjIyOiInYWxpYXMnIDogJ2RkLW1tLXl5eXknIjtzOjY6InBhcmFtcyI7czowOiIiO3M6MTY6IjpkYXRhLWxvYWQtc3RhdGUiO3M6MDoiIjtzOjEzOiI6ZGF0YS10b29sdGlwIjtzOjA6IiI7czo0OiJ0eXBlIjtzOjQ6InRleHQiO3M6MTI6ImNvbnRhaW5lcl9pZCI7czoxOiIwIjtzOjEwOiJ2YWxpZGF0aW9uIjthOjEzOntzOjg6InJlcXVpcmVkIjtzOjE6IjEiO3M6NToiYWxwaGEiO3M6MDoiIjtzOjg6ImFscGhhbnVtIjtzOjA6IiI7czo1OiJkaWdpdCI7czowOiIiO3M6Nzoibm9kaWdpdCI7czowOiIiO3M6Nzoibm9zcGFjZSI7czowOiIiO3M6NjoibnVtYmVyIjtzOjA6IiI7czo1OiJlbWFpbCI7czowOiIiO3M6NToicGhvbmUiO3M6MDoiIjtzOjExOiJwaG9uZV9pbnRlciI7czowOiIiO3M6MzoidXJsIjtzOjA6IiI7czo3OiJjb25maXJtIjtzOjA6IiI7czo2OiJjdXN0b20iO3M6MDoiIjt9fWk6NjthOjIwOntzOjQ6Im5hbWUiO3M6Mzoic2V4IjtzOjI6ImlkIjtzOjM6InNleCI7czo3OiJvcHRpb25zIjtzOjE1OiIwPVVvbW8NCjE9RG9ubmEiO3M6NToiZW1wdHkiO3M6MDoiIjtzOjY6InZhbHVlcyI7czowOiIiO3M6NToibGFiZWwiO2E6Mjp7czo0OiJ0ZXh0IjtzOjU6IlNlc3NvIjtzOjg6InBvc2l0aW9uIjtzOjQ6ImxlZnQiO31zOjg6InN1YmxhYmVsIjtzOjI5OiJJbnNlcmlyZSBpbCBzZXNzbyBkZWxsJ3V0ZW50ZSI7czo4OiJtdWx0aXBsZSI7czoxOiIwIjtzOjQ6InNpemUiO3M6MDoiIjtzOjU6ImNsYXNzIjtzOjA6IiI7czo1OiJ0aXRsZSI7czowOiIiO3M6NToic3R5bGUiO3M6MDoiIjtzOjY6InBhcmFtcyI7czowOiIiO3M6MTY6IjpkYXRhLWxvYWQtc3RhdGUiO3M6MDoiIjtzOjEzOiI6ZGF0YS10b29sdGlwIjtzOjA6IiI7czo0OiJ0eXBlIjtzOjg6ImRyb3Bkb3duIjtzOjEyOiJjb250YWluZXJfaWQiO3M6MToiMCI7czoxMDoidmFsaWRhdGlvbiI7YTozOntzOjg6InJlcXVpcmVkIjtzOjA6IiI7czo3OiJjb25maXJtIjtzOjA6IiI7czo2OiJjdXN0b20iO3M6MDoiIjt9czoxMjoiZHluYW1pY19kYXRhIjthOjQ6e3M6NzoiZW5hYmxlZCI7czowOiIiO3M6OToiZGF0YV9wYXRoIjtzOjA6IiI7czo5OiJ2YWx1ZV9rZXkiO3M6MDoiIjtzOjg6InRleHRfa2V5IjtzOjA6IiI7fXM6NjoiZXZlbnRzIjthOjE6e2k6MDthOjU6e3M6ODoib3BlcmF0b3IiO3M6MToiPSI7czo1OiJzdGF0ZSI7czowOiIiO3M6NjoiYWN0aW9uIjtzOjA6IiI7czo2OiJ0YXJnZXQiO3M6MDoiIjtzOjc6Im9wdGlvbnMiO3M6MDoiIjt9fX1pOjc7YToxODp7czo0OiJuYW1lIjtzOjQ6ImNpdHkiO3M6MjoiaWQiO3M6NDoiY2l0eSI7czo1OiJ2YWx1ZSI7czowOiIiO3M6NToibGFiZWwiO2E6Mjp7czo0OiJ0ZXh0IjtzOjY6IkNpdHTDoCI7czo4OiJwb3NpdGlvbiI7czo0OiJsZWZ0Ijt9czo4OiJzdWJsYWJlbCI7czo2MDoiQ2l0dMOgIGluIGN1aSB2aXZlIGwndXRlbnRlPGJyPigtMSkgSW5kaWNhIHVuYSBzdHJpbmdhIHZ1b3RhIjtzOjExOiJwbGFjZWhvbGRlciI7czo2OiJNaWxhbm8iO3M6OToibWF4bGVuZ3RoIjtzOjI6IjQwIjtzOjQ6InNpemUiO3M6MDoiIjtzOjU6ImNsYXNzIjtzOjA6IiI7czo1OiJ0aXRsZSI7czowOiIiO3M6NToic3R5bGUiO3M6MDoiIjtzOjE1OiI6ZGF0YS1pbnB1dG1hc2siO3M6MDoiIjtzOjY6InBhcmFtcyI7czowOiIiO3M6MTY6IjpkYXRhLWxvYWQtc3RhdGUiO3M6MDoiIjtzOjEzOiI6ZGF0YS10b29sdGlwIjtzOjA6IiI7czo0OiJ0eXBlIjtzOjQ6InRleHQiO3M6MTI6ImNvbnRhaW5lcl9pZCI7czoxOiIwIjtzOjEwOiJ2YWxpZGF0aW9uIjthOjEzOntzOjg6InJlcXVpcmVkIjtzOjA6IiI7czo1OiJhbHBoYSI7czowOiIiO3M6ODoiYWxwaGFudW0iO3M6MToiMSI7czo1OiJkaWdpdCI7czowOiIiO3M6Nzoibm9kaWdpdCI7czowOiIiO3M6Nzoibm9zcGFjZSI7czowOiIiO3M6NjoibnVtYmVyIjtzOjA6IiI7czo1OiJlbWFpbCI7czowOiIiO3M6NToicGhvbmUiO3M6MDoiIjtzOjExOiJwaG9uZV9pbnRlciI7czowOiIiO3M6MzoidXJsIjtzOjA6IiI7czo3OiJjb25maXJtIjtzOjA6IiI7czo2OiJjdXN0b20iO3M6MDoiIjt9fWk6ODthOjE4OntzOjQ6Im5hbWUiO3M6OToiaW50ZXJlc3RzIjtzOjI6ImlkIjtzOjk6ImludGVyZXN0cyI7czo1OiJ2YWx1ZSI7czowOiIiO3M6NToibGFiZWwiO2E6Mjp7czo0OiJ0ZXh0IjtzOjk6IkludGVyZXNzaSI7czo4OiJwb3NpdGlvbiI7czo0OiJsZWZ0Ijt9czo4OiJzdWJsYWJlbCI7czo4MToiUGFyb2xlIGNoaWF2ZSByaWd1YXJkbyBnbGkgaW50ZXJlc3NpIGRlbGwndXRlbnRlPGJyPigtMSkgSW5kaWNhIHVuYSBzdHJpbmdhIHZ1b3RhIjtzOjExOiJwbGFjZWhvbGRlciI7czoyNzoiTXVzaWNhLCBTcG9ydCwgR2lhcmRpbmFnZ2lvIjtzOjQ6InJvd3MiO3M6MToiMyI7czo0OiJjb2xzIjtzOjI6IjQwIjtzOjU6ImNsYXNzIjtzOjA6IiI7czo1OiJ0aXRsZSI7czowOiIiO3M6NToic3R5bGUiO3M6MDoiIjtzOjEzOiI6ZGF0YS13eXNpd3lnIjtzOjE6IjAiO3M6NjoicGFyYW1zIjtzOjA6IiI7czoxNjoiOmRhdGEtbG9hZC1zdGF0ZSI7czowOiIiO3M6MTM6IjpkYXRhLXRvb2x0aXAiO3M6MDoiIjtzOjQ6InR5cGUiO3M6ODoidGV4dGFyZWEiO3M6MTI6ImNvbnRhaW5lcl9pZCI7czoxOiIwIjtzOjEwOiJ2YWxpZGF0aW9uIjthOjg6e3M6ODoicmVxdWlyZWQiO3M6MDoiIjtzOjU6ImFscGhhIjtzOjA6IiI7czo4OiJhbHBoYW51bSI7czowOiIiO3M6NToiZGlnaXQiO3M6MDoiIjtzOjc6Im5vZGlnaXQiO3M6MDoiIjtzOjY6Im51bWJlciI7czowOiIiO3M6NzoiY29uZmlybSI7czowOiIiO3M6NjoiY3VzdG9tIjtzOjA6IiI7fX1pOjk7YToyMDp7czo0OiJuYW1lIjtzOjM6ImpvYiI7czoyOiJpZCI7czozOiJqb2IiO3M6Nzoib3B0aW9ucyI7czowOiIiO3M6NToiZW1wdHkiO3M6MTM6Ik5vbiBJbXBvc3RhdG8iO3M6NjoidmFsdWVzIjtzOjA6IiI7czo1OiJsYWJlbCI7YToyOntzOjQ6InRleHQiO3M6MzoiSm9iIjtzOjg6InBvc2l0aW9uIjtzOjQ6ImxlZnQiO31zOjg6InN1YmxhYmVsIjtzOjMzOiJTZWxlemlvbmFyZSB1biBsYXZvcm8gZGFsbGEgbGlzdGEiO3M6ODoibXVsdGlwbGUiO3M6MToiMCI7czo0OiJzaXplIjtzOjA6IiI7czo1OiJjbGFzcyI7czowOiIiO3M6NToidGl0bGUiO3M6MDoiIjtzOjU6InN0eWxlIjtzOjA6IiI7czo2OiJwYXJhbXMiO3M6MDoiIjtzOjE2OiI6ZGF0YS1sb2FkLXN0YXRlIjtzOjA6IiI7czoxMzoiOmRhdGEtdG9vbHRpcCI7czowOiIiO3M6NDoidHlwZSI7czo4OiJkcm9wZG93biI7czoxMjoiY29udGFpbmVyX2lkIjtzOjE6IjAiO3M6MTA6InZhbGlkYXRpb24iO2E6Mzp7czo4OiJyZXF1aXJlZCI7czowOiIiO3M6NzoiY29uZmlybSI7czowOiIiO3M6NjoiY3VzdG9tIjtzOjA6IiI7fXM6MTI6ImR5bmFtaWNfZGF0YSI7YTo0OntzOjc6ImVuYWJsZWQiO3M6MToiMSI7czo5OiJkYXRhX3BhdGgiO3M6ODoiam9ic2xpc3QiO3M6OToidmFsdWVfa2V5IjtzOjI6ImlkIjtzOjg6InRleHRfa2V5IjtzOjQ6Im5hbWUiO31zOjY6ImV2ZW50cyI7YToxOntpOjA7YTo1OntzOjg6Im9wZXJhdG9yIjtzOjE6Ij0iO3M6NToic3RhdGUiO3M6MDoiIjtzOjY6ImFjdGlvbiI7czowOiIiO3M6NjoidGFyZ2V0IjtzOjA6IiI7czo3OiJvcHRpb25zIjtzOjA6IiI7fX19aToxMDthOjEwOntzOjExOiJyZW5kZXJfdHlwZSI7czo2OiJzdWJtaXQiO3M6NDoibmFtZSI7czo4OiJidXR0b24xMCI7czoyOiJpZCI7czo4OiJidXR0b24xMCI7czo0OiJ0eXBlIjtzOjY6InN1Ym1pdCI7czo1OiJ2YWx1ZSI7czo1OiJTYWx2YSI7czo1OiJjbGFzcyI7czoxNToiYnRuIGJ0bi1kZWZhdWx0IjtzOjU6InN0eWxlIjtzOjA6IiI7czo2OiJwYXJhbXMiO3M6MDoiIjtzOjE2OiI6ZGF0YS1sb2FkLXN0YXRlIjtzOjA6IiI7czoxMjoiY29udGFpbmVyX2lkIjtzOjE6IjAiO319czozOiJETkEiO2E6Mjp7czo0OiJsb2FkIjthOjU6e3M6OToiZGJfcmVhZF8xIjthOjI6e3M6NToiZm91bmQiO3M6MDoiIjtzOjk6Im5vdF9mb3VuZCI7czowOiIiO31zOjEzOiJjdXN0b21fY29kZV8zIjtzOjA6IiI7czo2OiJodG1sXzAiO3M6MDoiIjtzOjEzOiJjdXN0b21fY29kZV80IjtzOjA6IiI7czoxMzoiY3VzdG9tX2NvZGVfNiI7czowOiIiO31zOjY6InN1Ym1pdCI7YToxOntzOjEzOiJjdXN0b21fY29kZV81IjtzOjA6IiI7fX1zOjE0OiJhY3Rpb25zX2NvbmZpZyI7YTo2OntpOjE7YToxOTp7czoxMjoiYWN0aW9uX2xhYmVsIjtzOjIwOiJDYXJpY21lbnRvIGxpc3RhIGpvYiI7czo3OiJlbmFibGVkIjtzOjE6IjEiO3M6OToidGFibGVuYW1lIjtzOjQ6ImpvYnMiO3M6MTA6Im11bHRpX3JlYWQiO3M6MToiMSI7czoxODoibG9hZF91bmRlcl9tb2RlbGlkIjtzOjE6IjEiO3M6ODoibW9kZWxfaWQiO3M6ODoiam9ic2xpc3QiO3M6NjoiZmllbGRzIjtzOjE4OiJpZCwgbmFtZSwgbGFuZ3VhZ2UiO3M6NToib3JkZXIiO3M6NDoibmFtZSI7czo1OiJncm91cCI7czowOiIiO3M6MTA6ImNvbmRpdGlvbnMiO3M6MDoiIjtzOjE2OiJlbmFibGVfcmVsYXRpb25zIjtzOjE6IjAiO3M6OToicmVsYXRpb25zIjthOjE6e2k6MDthOjQ6e3M6NToibW9kZWwiO3M6MDoiIjtzOjk6InRhYmxlbmFtZSI7czowOiIiO3M6NDoidHlwZSI7czo2OiJoYXNPbmUiO3M6NDoiZmtleSI7czowOiIiO319czoxMDoibmRiX2VuYWJsZSI7czoxOiIwIjtzOjEwOiJuZGJfZHJpdmVyIjtzOjU6Im15c3FsIjtzOjg6Im5kYl9ob3N0IjtzOjk6ImxvY2FsaG9zdCI7czoxMjoibmRiX2RhdGFiYXNlIjtzOjA6IiI7czo4OiJuZGJfdXNlciI7czowOiIiO3M6MTI6Im5kYl9wYXNzd29yZCI7czowOiIiO3M6MTA6Im5kYl9wcmVmaXgiO3M6NDoiam9zXyI7fWk6MzthOjI6e3M6MTI6ImFjdGlvbl9sYWJlbCI7czoyMDoiQ2FyaWNhbWVudG8gZGVpIGRhdGkiO3M6NzoiY29udGVudCI7czo4NzoiPD9waHANCiR1YyA9VHJhdmVsOjpnZXRJbnN0YW5jZSgpLT5nZXQoJ3VzZXJzQ29udHJvbGxlcicpOw0KJHVjLT5pbml0Rm9ybSgkZm9ybSk7DQo/Pg0KIjt9aTowO2E6MTE6e3M6NDoicGFnZSI7czoxOiIxIjtzOjEyOiJzdWJtaXRfZXZlbnQiO3M6Njoic3VibWl0IjtzOjExOiJmb3JtX21ldGhvZCI7czo0OiJmaWxlIjtzOjEwOiJhY3Rpb25fdXJsIjtzOjA6IiI7czoxMDoiZm9ybV9jbGFzcyI7czoxMDoiY2hyb25vZm9ybSI7czoxNToiZm9ybV90YWdfYXR0YWNoIjtzOjA6IiI7czoyNDoicmVxdWlyZWRfbGFiZWxzX2lkZW50aWZ5IjtzOjE6IjEiO3M6MTI6InJlbGF0aXZlX3VybCI7czoxOiIxIjtzOjExOiJhamF4X3N1Ym1pdCI7czoxOiIwIjtzOjEzOiJhZGRfZm9ybV90YWdzIjtzOjE6IjEiO3M6OToieGh0bWxfdXJsIjtzOjE6IjAiO31pOjQ7YToyOntzOjEyOiJhY3Rpb25fbGFiZWwiO3M6NDY6InNvc3RpdHV6aW9uZSBkZWkgY2FyYXR0ZXJpIHNwZWNpYWxpIG5lbGxhIG1haWwiO3M6NzoiY29udGVudCI7czo4ODoiPHNjcmlwdD4NCmpRdWVyeSgnI2VtYWlsJykudmFsKGpRdWVyeSgnI2VtYWlsJykudmFsKCkucmVwbGFjZSgnJiM2NDsnLCAnQCcpKTsNCjwvc2NyaXB0PiI7fWk6NjthOjI6e3M6MTI6ImFjdGlvbl9sYWJlbCI7czozMToiTW9kaWZpY2hlIGFsbCdodG1sIGF1dG9nZW5lcmF0byI7czo3OiJjb250ZW50IjtzOjEzOToiPD9waHAgDQokYSA9ICd2ZW5kb3IvanMvdXNlcmRldGFpbGZvcm0uanMnOw0KZWNobyAnPHNjcmlwdCBzcmM9IicuSnVyaTo6YmFzZSgpIC4gJGEuJyI+PC9zY3JpcHQ+JzsNCj8+DQoNCjxzY3JpcHQ+DQphc3Rlcml4KCk7DQo8L3NjcmlwdD4NCiI7fWk6NTthOjI6e3M6MTI6ImFjdGlvbl9sYWJlbCI7czoyMjoiU2FsdmF0YWdnaW8gZSByZWRpcmVjdCI7czo3OiJjb250ZW50IjtzOjI5MToiPD9waHANCkpMb2FkZXI6OnJlZ2lzdGVyKCdVc2Vyc01vZGVsJywgJ3ZlbmRvci9tb2RlbC9Vc2Vyc01vZGVsLnBocCcpOw0KJHVzZXJNb2RlbCA9IG5ldyBVc2Vyc01vZGVsKCk7DQokdXNlck1vZGVsLT5zYXZlKCk7DQoNCkpGYWN0b3J5OjpnZXRBcHBsaWNhdGlvbigpLT5yZWRpcmVjdChKVXJpOjpiYXNlKCkuJ2luZGV4LnBocC91dGVudGk/c2hvdz1lZGl0Jml0ZW09Jy5KUmVxdWVzdDo6Z2V0VmFyKCdpdGVtJyksICdTYWx2YXRhZ2dpbyBtb2RpZmljaGUgZWZmZXR0dWF0ZScsICdzdWNjZXNzJyk7DQoNCj8+Ijt9fXM6MTQ6ImRiX2ZpZWxkc19saXN0IjthOjE6e3M6NDoiam9icyI7czowOiIiO319', 1, 'user', 1, '<div class="form-group gcore-form-row" id="form-row-status"><label for="status" class="control-label gcore-label-left">Stato dell''utente</label>\n<div class="gcore-input gcore-display-table" id="fin-status"><select name="status" id="status" size="" class="form-control A" title="" style="" data-load-state="" data-tooltip="">\n<option value="0">Bannato</option>\n<option value="1">Attivo</option>\n</select><span class="help-block">Gli utenti bannati non possono usare i servizi offerti dai client</span></div></div><div class="form-group gcore-form-row" id="form-row-name"><label for="name" class="control-label gcore-label-left">Nome</label>\n<div class="gcore-input gcore-display-table" id="fin-name"><input name="name" id="name" value="" placeholder="Paolo Francesco" maxlength="40" size="" class="validate[&#039;required&#039;,&#039;alphanum&#039;] form-control A" title="" style="" data-inputmask="" data-load-state="" data-tooltip="" type="text" /><span class="help-block">Nome che compare nel profilo</span></div></div><div class="form-group gcore-form-row" id="form-row-surname"><label for="surname" class="control-label gcore-label-left">Cognome</label>\n<div class="gcore-input gcore-display-table" id="fin-surname"><input name="surname" id="surname" value="" placeholder="De Rossi" maxlength="40" size="" class="validate[&#039;required&#039;,&#039;alphanum&#039;] form-control A" title="" style="" data-inputmask="" data-load-state="" data-tooltip="" type="text" /><span class="help-block">Cognome che compare nel profilo</span></div></div><div class="form-group gcore-form-row" id="form-row-email"><label for="email" class="control-label gcore-label-left">Email</label>\n<div class="gcore-input gcore-display-table" id="fin-email"><input name="email" id="email" value="" placeholder="" maxlength="" size="" class="validate[&#039;required&#039;,&#039;email&#039;] form-control A" title="" style="" data-inputmask="" data-load-state="" data-tooltip="" type="text" /><span class="help-block">Indirizzo email di registrazione</span></div></div><div class="form-group gcore-form-row" id="form-row-birthdate"><label for="birthdate" class="control-label gcore-label-left">Data di nascita</label>\n<div class="gcore-input gcore-display-table" id="fin-birthdate"><input name="birthdate" id="birthdate" value="" placeholder="" maxlength="" size="" class="validate[&#039;required&#039;] form-control A" title="" style="" data-inputmask="&#039;alias&#039; : &#039;dd-mm-yyyy&#039;" data-load-state="" data-tooltip="" type="text" /><span class="help-block">Data di nascita dell''utente</span></div></div><div class="form-group gcore-form-row" id="form-row-sex"><label for="sex" class="control-label gcore-label-left">Sesso</label>\n<div class="gcore-input gcore-display-table" id="fin-sex"><select name="sex" id="sex" size="" class="form-control A" title="" style="" data-load-state="" data-tooltip="">\n<option value="0">Uomo</option>\n<option value="1">Donna</option>\n</select><span class="help-block">Inserire il sesso dell''utente</span></div></div><div class="form-group gcore-form-row" id="form-row-city"><label for="city" class="control-label gcore-label-left">Città</label>\n<div class="gcore-input gcore-display-table" id="fin-city"><input name="city" id="city" value="" placeholder="Milano" maxlength="40" size="" class="validate[&#039;alphanum&#039;] form-control A" title="" style="" data-inputmask="" data-load-state="" data-tooltip="" type="text" /><span class="help-block">Città in cui vive l''utente<br>(-1) Indica una stringa vuota</span></div></div><div class="form-group gcore-form-row" id="form-row-interests"><label for="interests" class="control-label gcore-label-left">Interessi</label>\n<div class="gcore-input gcore-display-table" id="fin-interests"><textarea name="interests" id="interests" placeholder="Musica, Sport, Giardinaggio" rows="3" cols="40" class="form-control A" title="" style="" data-wysiwyg="0" data-load-state="" data-tooltip=""></textarea><span class="help-block">Parole chiave riguardo gli interessi dell''utente<br>(-1) Indica una stringa vuota</span></div></div><?php\n$keys = \\GCore\\Libs\\Arr::getVal(\\GCore\\Libs\\Arr::getVal($form->data, explode(".", "jobslist")), explode(".", "[n].id"));\n$values = \\GCore\\Libs\\Arr::getVal(\\GCore\\Libs\\Arr::getVal($form->data, explode(".", "jobslist")), explode(".", "[n].name"));\n$options = array_combine($keys, $values);\n$field = array (\n  ''name'' => ''job'',\n  ''id'' => ''job'',\n  ''options'' => \n  array (\n  ),\n  ''empty'' => ''Non Impostato'',\n  ''values'' => \n  array (\n  ),\n  ''label'' => \n  array (\n    ''text'' => ''Job'',\n    ''position'' => ''left'',\n  ),\n  ''sublabel'' => ''Selezionare un lavoro dalla lista'',\n  ''multiple'' => ''0'',\n  ''size'' => '''',\n  ''class'' => '''',\n  ''title'' => '''',\n  ''style'' => '''',\n  ''params'' => '''',\n  '':data-load-state'' => '''',\n  '':data-tooltip'' => '''',\n  ''type'' => ''dropdown'',\n  ''container_id'' => ''0'',\n);\n$field["options"] = $options;\necho \\GCore\\Helpers\\Html::formLine($field["name"], $field);\n?><div class="form-group gcore-form-row" id="form-row-button10"><div class="gcore-input gcore-display-table" id="fin-button10"><input name="button10" id="button10" type="submit" value="Salva" class="btn btn-default form-control A" style="" data-load-state="" /></div></div>'),
(4, 'user_search', '{"description":"Filtri per la ricerca sulla lista degli utenti","setup":"0","theme":"bootstrap3","tight_layout":"0","rtl_support":"0","labels_right_aligned":"0","labels_auto_width":"0","js_validation_language":""}', 'YTo0OntzOjY6ImZpZWxkcyI7YToxNzp7aToxO2E6MjA6e3M6NDoibmFtZSI7czo2OiJzdGF0dXMiO3M6MjoiaWQiO3M6Njoic3RhdHVzIjtzOjc6Im9wdGlvbnMiO3M6MTk6IjE9QXR0aXZpDQowPUJhbm5hdGkiO3M6NToiZW1wdHkiO3M6MTM6Ik5vbiBJbXBvc3RhdG8iO3M6NjoidmFsdWVzIjtzOjA6IiI7czo1OiJsYWJlbCI7YToyOntzOjQ6InRleHQiO3M6MTg6IlN0YXRvIGRlZ2xpIHV0ZW50aSI7czo4OiJwb3NpdGlvbiI7czo0OiJsZWZ0Ijt9czo4OiJzdWJsYWJlbCI7czo1NjoiRmlsdHJvIHBlciBsYSBzZWxlemlvbmUgZGVpIHNvbGkgdXRlbnRpIGF0dGl2aSBvIGJhbm5hdGkiO3M6ODoibXVsdGlwbGUiO3M6MToiMCI7czo0OiJzaXplIjtzOjA6IiI7czo1OiJjbGFzcyI7czowOiIiO3M6NToidGl0bGUiO3M6MDoiIjtzOjU6InN0eWxlIjtzOjA6IiI7czo2OiJwYXJhbXMiO3M6MDoiIjtzOjE2OiI6ZGF0YS1sb2FkLXN0YXRlIjtzOjA6IiI7czoxMzoiOmRhdGEtdG9vbHRpcCI7czowOiIiO3M6NDoidHlwZSI7czo4OiJkcm9wZG93biI7czoxMjoiY29udGFpbmVyX2lkIjtzOjE6IjAiO3M6MTA6InZhbGlkYXRpb24iO2E6Mzp7czo4OiJyZXF1aXJlZCI7czowOiIiO3M6NzoiY29uZmlybSI7czowOiIiO3M6NjoiY3VzdG9tIjtzOjA6IiI7fXM6MTI6ImR5bmFtaWNfZGF0YSI7YTo0OntzOjc6ImVuYWJsZWQiO3M6MDoiIjtzOjk6ImRhdGFfcGF0aCI7czowOiIiO3M6OToidmFsdWVfa2V5IjtzOjA6IiI7czo4OiJ0ZXh0X2tleSI7czowOiIiO31zOjY6ImV2ZW50cyI7YToxOntpOjA7YTo1OntzOjg6Im9wZXJhdG9yIjtzOjE6Ij0iO3M6NToic3RhdGUiO3M6MDoiIjtzOjY6ImFjdGlvbiI7czowOiIiO3M6NjoidGFyZ2V0IjtzOjA6IiI7czo3OiJvcHRpb25zIjtzOjA6IiI7fX19aToyO2E6MTg6e3M6NDoibmFtZSI7czo1OiJlbWFpbCI7czoyOiJpZCI7czo1OiJlbWFpbCI7czo1OiJ2YWx1ZSI7czowOiIiO3M6NToibGFiZWwiO2E6Mjp7czo0OiJ0ZXh0IjtzOjU6IkVtYWlsIjtzOjg6InBvc2l0aW9uIjtzOjQ6ImxlZnQiO31zOjg6InN1YmxhYmVsIjtzOjU4OiJTb3R0b3N0cmluZ2EgY29udGVudXRhIG5lbGwnaW5kaXJpenpvIGRpIHBvc3RhIGVsZXR0cm9uaWNhIjtzOjExOiJwbGFjZWhvbGRlciI7czowOiIiO3M6OToibWF4bGVuZ3RoIjtzOjA6IiI7czo0OiJzaXplIjtzOjA6IiI7czo1OiJjbGFzcyI7czowOiIiO3M6NToidGl0bGUiO3M6MDoiIjtzOjU6InN0eWxlIjtzOjA6IiI7czoxNToiOmRhdGEtaW5wdXRtYXNrIjtzOjA6IiI7czo2OiJwYXJhbXMiO3M6MDoiIjtzOjE2OiI6ZGF0YS1sb2FkLXN0YXRlIjtzOjA6IiI7czoxMzoiOmRhdGEtdG9vbHRpcCI7czowOiIiO3M6NDoidHlwZSI7czo0OiJ0ZXh0IjtzOjEyOiJjb250YWluZXJfaWQiO3M6MToiMCI7czoxMDoidmFsaWRhdGlvbiI7YToxMzp7czo4OiJyZXF1aXJlZCI7czowOiIiO3M6NToiYWxwaGEiO3M6MDoiIjtzOjg6ImFscGhhbnVtIjtzOjA6IiI7czo1OiJkaWdpdCI7czowOiIiO3M6Nzoibm9kaWdpdCI7czowOiIiO3M6Nzoibm9zcGFjZSI7czowOiIiO3M6NjoibnVtYmVyIjtzOjA6IiI7czo1OiJlbWFpbCI7czowOiIiO3M6NToicGhvbmUiO3M6MDoiIjtzOjExOiJwaG9uZV9pbnRlciI7czowOiIiO3M6MzoidXJsIjtzOjA6IiI7czo3OiJjb25maXJtIjtzOjA6IiI7czo2OiJjdXN0b20iO3M6MDoiIjt9fWk6MzthOjE4OntzOjQ6Im5hbWUiO3M6NDoibmFtZSI7czoyOiJpZCI7czo0OiJuYW1lIjtzOjU6InZhbHVlIjtzOjA6IiI7czo1OiJsYWJlbCI7YToyOntzOjQ6InRleHQiO3M6MTE6Ik5vbWUgdXRlbnRlIjtzOjg6InBvc2l0aW9uIjtzOjQ6ImxlZnQiO31zOjg6InN1YmxhYmVsIjtzOjI4OiJSaWNlcmNhIG5lbCBub21lIGRlbGwndXRlbnRlIjtzOjExOiJwbGFjZWhvbGRlciI7czowOiIiO3M6OToibWF4bGVuZ3RoIjtzOjA6IiI7czo0OiJzaXplIjtzOjA6IiI7czo1OiJjbGFzcyI7czowOiIiO3M6NToidGl0bGUiO3M6MDoiIjtzOjU6InN0eWxlIjtzOjA6IiI7czoxNToiOmRhdGEtaW5wdXRtYXNrIjtzOjA6IiI7czo2OiJwYXJhbXMiO3M6MDoiIjtzOjE2OiI6ZGF0YS1sb2FkLXN0YXRlIjtzOjA6IiI7czoxMzoiOmRhdGEtdG9vbHRpcCI7czowOiIiO3M6NDoidHlwZSI7czo0OiJ0ZXh0IjtzOjEyOiJjb250YWluZXJfaWQiO3M6MToiMCI7czoxMDoidmFsaWRhdGlvbiI7YToxMzp7czo4OiJyZXF1aXJlZCI7czowOiIiO3M6NToiYWxwaGEiO3M6MDoiIjtzOjg6ImFscGhhbnVtIjtzOjA6IiI7czo1OiJkaWdpdCI7czowOiIiO3M6Nzoibm9kaWdpdCI7czowOiIiO3M6Nzoibm9zcGFjZSI7czowOiIiO3M6NjoibnVtYmVyIjtzOjA6IiI7czo1OiJlbWFpbCI7czowOiIiO3M6NToicGhvbmUiO3M6MDoiIjtzOjExOiJwaG9uZV9pbnRlciI7czowOiIiO3M6MzoidXJsIjtzOjA6IiI7czo3OiJjb25maXJtIjtzOjA6IiI7czo2OiJjdXN0b20iO3M6MDoiIjt9fWk6NDthOjE4OntzOjQ6Im5hbWUiO3M6Nzoic3VybmFtZSI7czoyOiJpZCI7czo3OiJzdXJuYW1lIjtzOjU6InZhbHVlIjtzOjA6IiI7czo1OiJsYWJlbCI7YToyOntzOjQ6InRleHQiO3M6MTQ6IkNvZ25vbWUgdXRlbnRlIjtzOjg6InBvc2l0aW9uIjtzOjQ6ImxlZnQiO31zOjg6InN1YmxhYmVsIjtzOjMxOiJSaWNlcmNhIG5lbCBjb2dub21lIGRlbGwndXRlbnRlIjtzOjExOiJwbGFjZWhvbGRlciI7czowOiIiO3M6OToibWF4bGVuZ3RoIjtzOjA6IiI7czo0OiJzaXplIjtzOjA6IiI7czo1OiJjbGFzcyI7czowOiIiO3M6NToidGl0bGUiO3M6MDoiIjtzOjU6InN0eWxlIjtzOjA6IiI7czoxNToiOmRhdGEtaW5wdXRtYXNrIjtzOjA6IiI7czo2OiJwYXJhbXMiO3M6MDoiIjtzOjE2OiI6ZGF0YS1sb2FkLXN0YXRlIjtzOjA6IiI7czoxMzoiOmRhdGEtdG9vbHRpcCI7czowOiIiO3M6NDoidHlwZSI7czo0OiJ0ZXh0IjtzOjEyOiJjb250YWluZXJfaWQiO3M6MToiMCI7czoxMDoidmFsaWRhdGlvbiI7YToxMzp7czo4OiJyZXF1aXJlZCI7czowOiIiO3M6NToiYWxwaGEiO3M6MDoiIjtzOjg6ImFscGhhbnVtIjtzOjA6IiI7czo1OiJkaWdpdCI7czowOiIiO3M6Nzoibm9kaWdpdCI7czowOiIiO3M6Nzoibm9zcGFjZSI7czowOiIiO3M6NjoibnVtYmVyIjtzOjA6IiI7czo1OiJlbWFpbCI7czowOiIiO3M6NToicGhvbmUiO3M6MDoiIjtzOjExOiJwaG9uZV9pbnRlciI7czowOiIiO3M6MzoidXJsIjtzOjA6IiI7czo3OiJjb25maXJtIjtzOjA6IiI7czo2OiJjdXN0b20iO3M6MDoiIjt9fWk6NTthOjE4OntzOjQ6Im5hbWUiO3M6MTM6ImJpcnRoZGF0ZWZyb20iO3M6MjoiaWQiO3M6MTM6ImJpcnRoZGF0ZWZyb20iO3M6NToidmFsdWUiO3M6MDoiIjtzOjU6ImxhYmVsIjthOjI6e3M6NDoidGV4dCI7czoxODoiTmF0byBkb3BvIGxhIGRhdGEgIjtzOjg6InBvc2l0aW9uIjtzOjQ6ImxlZnQiO31zOjg6InN1YmxhYmVsIjtzOjY5OiJMaW1pdGUgaW5mZXJpb3JlIGRlbGwnaW50ZXJ2YWxsbyBkZWxsZSBkYXRlIGRpIG5hc2NpdGEgZGEgY29uc2lkZXJhcmUiO3M6MTE6InBsYWNlaG9sZGVyIjtzOjA6IiI7czo5OiJtYXhsZW5ndGgiO3M6MDoiIjtzOjQ6InNpemUiO3M6MDoiIjtzOjU6ImNsYXNzIjtzOjA6IiI7czo1OiJ0aXRsZSI7czowOiIiO3M6NToic3R5bGUiO3M6MDoiIjtzOjE1OiI6ZGF0YS1pbnB1dG1hc2siO3M6MjI6IidhbGlhcycgOiAnZGQtbW0teXl5eSciO3M6NjoicGFyYW1zIjtzOjA6IiI7czoxNjoiOmRhdGEtbG9hZC1zdGF0ZSI7czowOiIiO3M6MTM6IjpkYXRhLXRvb2x0aXAiO3M6MDoiIjtzOjQ6InR5cGUiO3M6NDoidGV4dCI7czoxMjoiY29udGFpbmVyX2lkIjtzOjE6IjAiO3M6MTA6InZhbGlkYXRpb24iO2E6MTM6e3M6ODoicmVxdWlyZWQiO3M6MDoiIjtzOjU6ImFscGhhIjtzOjA6IiI7czo4OiJhbHBoYW51bSI7czowOiIiO3M6NToiZGlnaXQiO3M6MDoiIjtzOjc6Im5vZGlnaXQiO3M6MDoiIjtzOjc6Im5vc3BhY2UiO3M6MDoiIjtzOjY6Im51bWJlciI7czowOiIiO3M6NToiZW1haWwiO3M6MDoiIjtzOjU6InBob25lIjtzOjA6IiI7czoxMToicGhvbmVfaW50ZXIiO3M6MDoiIjtzOjM6InVybCI7czowOiIiO3M6NzoiY29uZmlybSI7czowOiIiO3M6NjoiY3VzdG9tIjtzOjA6IiI7fX1pOjY7YToxODp7czo0OiJuYW1lIjtzOjExOiJiaXJ0aGRhdGV0byI7czoyOiJpZCI7czoxMToiYmlydGhkYXRldG8iO3M6NToidmFsdWUiO3M6MDoiIjtzOjU6ImxhYmVsIjthOjI6e3M6NDoidGV4dCI7czoyMToiTmF0byBwcmltYSBkZWxsYSBkYXRhIjtzOjg6InBvc2l0aW9uIjtzOjQ6ImxlZnQiO31zOjg6InN1YmxhYmVsIjtzOjY5OiJMaW1pdGUgc3VwZXJpb3JlIGRlbGwnaW50ZXJ2YWxsbyBkZWxsZSBkYXRlIGRpIG5hc2NpdGEgZGEgY29uc2lkZXJhcmUiO3M6MTE6InBsYWNlaG9sZGVyIjtzOjA6IiI7czo5OiJtYXhsZW5ndGgiO3M6MDoiIjtzOjQ6InNpemUiO3M6MDoiIjtzOjU6ImNsYXNzIjtzOjA6IiI7czo1OiJ0aXRsZSI7czowOiIiO3M6NToic3R5bGUiO3M6MDoiIjtzOjE1OiI6ZGF0YS1pbnB1dG1hc2siO3M6MjI6IidhbGlhcycgOiAnZGQtbW0teXl5eSciO3M6NjoicGFyYW1zIjtzOjA6IiI7czoxNjoiOmRhdGEtbG9hZC1zdGF0ZSI7czowOiIiO3M6MTM6IjpkYXRhLXRvb2x0aXAiO3M6MDoiIjtzOjQ6InR5cGUiO3M6NDoidGV4dCI7czoxMjoiY29udGFpbmVyX2lkIjtzOjE6IjAiO3M6MTA6InZhbGlkYXRpb24iO2E6MTM6e3M6ODoicmVxdWlyZWQiO3M6MDoiIjtzOjU6ImFscGhhIjtzOjA6IiI7czo4OiJhbHBoYW51bSI7czowOiIiO3M6NToiZGlnaXQiO3M6MDoiIjtzOjc6Im5vZGlnaXQiO3M6MDoiIjtzOjc6Im5vc3BhY2UiO3M6MDoiIjtzOjY6Im51bWJlciI7czowOiIiO3M6NToiZW1haWwiO3M6MDoiIjtzOjU6InBob25lIjtzOjA6IiI7czoxMToicGhvbmVfaW50ZXIiO3M6MDoiIjtzOjM6InVybCI7czowOiIiO3M6NzoiY29uZmlybSI7czowOiIiO3M6NjoiY3VzdG9tIjtzOjA6IiI7fX1pOjc7YToyMDp7czo0OiJuYW1lIjtzOjM6InNleCI7czoyOiJpZCI7czozOiJzZXgiO3M6Nzoib3B0aW9ucyI7czoxNToiMD1Vb21vDQoxPURvbm5hIjtzOjU6ImVtcHR5IjtzOjEzOiJOb24gSW1wb3N0YXRvIjtzOjY6InZhbHVlcyI7czowOiIiO3M6NToibGFiZWwiO2E6Mjp7czo0OiJ0ZXh0IjtzOjM6IlNleCI7czo4OiJwb3NpdGlvbiI7czo0OiJsZWZ0Ijt9czo4OiJzdWJsYWJlbCI7czozOToiRmlsdHJvIGRpIHJpY2VyY2Egc3VsIHNlc3NvIGRlbGwndXRlbnRlIjtzOjg6Im11bHRpcGxlIjtzOjE6IjAiO3M6NDoic2l6ZSI7czowOiIiO3M6NToiY2xhc3MiO3M6MDoiIjtzOjU6InRpdGxlIjtzOjA6IiI7czo1OiJzdHlsZSI7czowOiIiO3M6NjoicGFyYW1zIjtzOjA6IiI7czoxNjoiOmRhdGEtbG9hZC1zdGF0ZSI7czowOiIiO3M6MTM6IjpkYXRhLXRvb2x0aXAiO3M6MDoiIjtzOjQ6InR5cGUiO3M6ODoiZHJvcGRvd24iO3M6MTI6ImNvbnRhaW5lcl9pZCI7czoxOiIwIjtzOjEwOiJ2YWxpZGF0aW9uIjthOjM6e3M6ODoicmVxdWlyZWQiO3M6MDoiIjtzOjc6ImNvbmZpcm0iO3M6MDoiIjtzOjY6ImN1c3RvbSI7czowOiIiO31zOjEyOiJkeW5hbWljX2RhdGEiO2E6NDp7czo3OiJlbmFibGVkIjtzOjA6IiI7czo5OiJkYXRhX3BhdGgiO3M6MDoiIjtzOjk6InZhbHVlX2tleSI7czowOiIiO3M6ODoidGV4dF9rZXkiO3M6MDoiIjt9czo2OiJldmVudHMiO2E6MTp7aTowO2E6NTp7czo4OiJvcGVyYXRvciI7czoxOiI9IjtzOjU6InN0YXRlIjtzOjA6IiI7czo2OiJhY3Rpb24iO3M6MDoiIjtzOjY6InRhcmdldCI7czowOiIiO3M6Nzoib3B0aW9ucyI7czowOiIiO319fWk6ODthOjIwOntzOjQ6Im5hbWUiO3M6Mzoiam9iIjtzOjI6ImlkIjtzOjM6ImpvYiI7czo3OiJvcHRpb25zIjtzOjA6IiI7czo1OiJlbXB0eSI7czoxMzoiTm9uIEltcG9zdGF0byI7czo2OiJ2YWx1ZXMiO3M6MDoiIjtzOjU6ImxhYmVsIjthOjI6e3M6NDoidGV4dCI7czozOiJKb2IiO3M6ODoicG9zaXRpb24iO3M6NDoibGVmdCI7fXM6ODoic3VibGFiZWwiO3M6NDc6IkZpbHRybyBkaSByaWNlcmNhIHN1bGxhIHByb2Zlc3Npb25lIGRlbGwndXRlbnRlIjtzOjg6Im11bHRpcGxlIjtzOjE6IjAiO3M6NDoic2l6ZSI7czowOiIiO3M6NToiY2xhc3MiO3M6MDoiIjtzOjU6InRpdGxlIjtzOjA6IiI7czo1OiJzdHlsZSI7czowOiIiO3M6NjoicGFyYW1zIjtzOjA6IiI7czoxNjoiOmRhdGEtbG9hZC1zdGF0ZSI7czowOiIiO3M6MTM6IjpkYXRhLXRvb2x0aXAiO3M6MDoiIjtzOjQ6InR5cGUiO3M6ODoiZHJvcGRvd24iO3M6MTI6ImNvbnRhaW5lcl9pZCI7czoxOiIwIjtzOjEwOiJ2YWxpZGF0aW9uIjthOjM6e3M6ODoicmVxdWlyZWQiO3M6MDoiIjtzOjc6ImNvbmZpcm0iO3M6MDoiIjtzOjY6ImN1c3RvbSI7czowOiIiO31zOjEyOiJkeW5hbWljX2RhdGEiO2E6NDp7czo3OiJlbmFibGVkIjtzOjE6IjEiO3M6OToiZGF0YV9wYXRoIjtzOjg6ImpvYnNsaXN0IjtzOjk6InZhbHVlX2tleSI7czoyOiJpZCI7czo4OiJ0ZXh0X2tleSI7czo0OiJuYW1lIjt9czo2OiJldmVudHMiO2E6MTp7aTowO2E6NTp7czo4OiJvcGVyYXRvciI7czoxOiI9IjtzOjU6InN0YXRlIjtzOjA6IiI7czo2OiJhY3Rpb24iO3M6MDoiIjtzOjY6InRhcmdldCI7czowOiIiO3M6Nzoib3B0aW9ucyI7czowOiIiO319fWk6OTthOjE4OntzOjQ6Im5hbWUiO3M6NDoiY2l0eSI7czoyOiJpZCI7czo0OiJjaXR5IjtzOjU6InZhbHVlIjtzOjA6IiI7czo1OiJsYWJlbCI7YToyOntzOjQ6InRleHQiO3M6NjoiQ2l0dMOgIjtzOjg6InBvc2l0aW9uIjtzOjQ6ImxlZnQiO31zOjg6InN1YmxhYmVsIjtzOjUwOiJSaWNlcmNhIG5lbCBub21lIGRlbGxhIGNpdHTDoCBpbnNlcml0YSBkYWxsJ3V0ZW50ZSI7czoxMToicGxhY2Vob2xkZXIiO3M6MDoiIjtzOjk6Im1heGxlbmd0aCI7czowOiIiO3M6NDoic2l6ZSI7czowOiIiO3M6NToiY2xhc3MiO3M6MDoiIjtzOjU6InRpdGxlIjtzOjA6IiI7czo1OiJzdHlsZSI7czowOiIiO3M6MTU6IjpkYXRhLWlucHV0bWFzayI7czowOiIiO3M6NjoicGFyYW1zIjtzOjA6IiI7czoxNjoiOmRhdGEtbG9hZC1zdGF0ZSI7czowOiIiO3M6MTM6IjpkYXRhLXRvb2x0aXAiO3M6MDoiIjtzOjQ6InR5cGUiO3M6NDoidGV4dCI7czoxMjoiY29udGFpbmVyX2lkIjtzOjE6IjAiO3M6MTA6InZhbGlkYXRpb24iO2E6MTM6e3M6ODoicmVxdWlyZWQiO3M6MDoiIjtzOjU6ImFscGhhIjtzOjA6IiI7czo4OiJhbHBoYW51bSI7czowOiIiO3M6NToiZGlnaXQiO3M6MDoiIjtzOjc6Im5vZGlnaXQiO3M6MDoiIjtzOjc6Im5vc3BhY2UiO3M6MDoiIjtzOjY6Im51bWJlciI7czowOiIiO3M6NToiZW1haWwiO3M6MDoiIjtzOjU6InBob25lIjtzOjA6IiI7czoxMToicGhvbmVfaW50ZXIiO3M6MDoiIjtzOjM6InVybCI7czowOiIiO3M6NzoiY29uZmlybSI7czowOiIiO3M6NjoiY3VzdG9tIjtzOjA6IiI7fX1pOjEwO2E6MTg6e3M6NDoibmFtZSI7czo5OiJpbnRlcmVzdHMiO3M6MjoiaWQiO3M6OToiaW50ZXJlc3RzIjtzOjU6InZhbHVlIjtzOjA6IiI7czo1OiJsYWJlbCI7YToyOntzOjQ6InRleHQiO3M6OToiSW50ZXJlc3NpIjtzOjg6InBvc2l0aW9uIjtzOjQ6ImxlZnQiO31zOjg6InN1YmxhYmVsIjtzOjkyOiJTZXF1ZW56YSBkaSB0b2tlbiBjaGUgcG9zc29ubyB0cm92YXJzaSB0cmEgZ2xpIGludGVyZXNzaSBkZWxsJ3V0ZW50ZSBzZXBhcmF0aSBkYSB1bmEgdmlyZ29sYSI7czoxMToicGxhY2Vob2xkZXIiO3M6MDoiIjtzOjQ6InJvd3MiO3M6MToiMyI7czo0OiJjb2xzIjtzOjI6IjQwIjtzOjU6ImNsYXNzIjtzOjA6IiI7czo1OiJ0aXRsZSI7czowOiIiO3M6NToic3R5bGUiO3M6MDoiIjtzOjEzOiI6ZGF0YS13eXNpd3lnIjtzOjE6IjAiO3M6NjoicGFyYW1zIjtzOjA6IiI7czoxNjoiOmRhdGEtbG9hZC1zdGF0ZSI7czowOiIiO3M6MTM6IjpkYXRhLXRvb2x0aXAiO3M6MDoiIjtzOjQ6InR5cGUiO3M6ODoidGV4dGFyZWEiO3M6MTI6ImNvbnRhaW5lcl9pZCI7czoxOiIwIjtzOjEwOiJ2YWxpZGF0aW9uIjthOjg6e3M6ODoicmVxdWlyZWQiO3M6MDoiIjtzOjU6ImFscGhhIjtzOjA6IiI7czo4OiJhbHBoYW51bSI7czowOiIiO3M6NToiZGlnaXQiO3M6MDoiIjtzOjc6Im5vZGlnaXQiO3M6MDoiIjtzOjY6Im51bWJlciI7czowOiIiO3M6NzoiY29uZmlybSI7czowOiIiO3M6NjoiY3VzdG9tIjtzOjA6IiI7fX1pOjExO2E6MTg6e3M6NDoibmFtZSI7czoyMDoicmVnaXN0cmF0aW9udGltZWZyb20iO3M6MjoiaWQiO3M6MTg6InJlZ2lzdHJhdGlvbnRpbWV0byI7czo1OiJ2YWx1ZSI7czowOiIiO3M6NToibGFiZWwiO2E6Mjp7czo0OiJ0ZXh0IjtzOjMwOiJVdGVudGUgcmVnaXN0cmF0byBkb3BvIGxhIGRhdGEiO3M6ODoicG9zaXRpb24iO3M6NDoibGVmdCI7fXM6ODoic3VibGFiZWwiO3M6Nzg6IkxpbWl0ZSBpbmZlcmlvcmUgZGVsbCdpbnRlcnZhbGxvIGRlbGxlIGRhdGUgZGkgZGkgcmVnaXN0cmF6aW9uZSBkYSBjb25zaWRlcmFyZSI7czoxMToicGxhY2Vob2xkZXIiO3M6MDoiIjtzOjk6Im1heGxlbmd0aCI7czowOiIiO3M6NDoic2l6ZSI7czowOiIiO3M6NToiY2xhc3MiO3M6MDoiIjtzOjU6InRpdGxlIjtzOjA6IiI7czo1OiJzdHlsZSI7czowOiIiO3M6MTU6IjpkYXRhLWlucHV0bWFzayI7czoyMjoiJ2FsaWFzJyA6ICdkZC1tbS15eXl5JyI7czo2OiJwYXJhbXMiO3M6MDoiIjtzOjE2OiI6ZGF0YS1sb2FkLXN0YXRlIjtzOjA6IiI7czoxMzoiOmRhdGEtdG9vbHRpcCI7czowOiIiO3M6NDoidHlwZSI7czo0OiJ0ZXh0IjtzOjEyOiJjb250YWluZXJfaWQiO3M6MToiMCI7czoxMDoidmFsaWRhdGlvbiI7YToxMzp7czo4OiJyZXF1aXJlZCI7czowOiIiO3M6NToiYWxwaGEiO3M6MDoiIjtzOjg6ImFscGhhbnVtIjtzOjA6IiI7czo1OiJkaWdpdCI7czowOiIiO3M6Nzoibm9kaWdpdCI7czowOiIiO3M6Nzoibm9zcGFjZSI7czowOiIiO3M6NjoibnVtYmVyIjtzOjA6IiI7czo1OiJlbWFpbCI7czowOiIiO3M6NToicGhvbmUiO3M6MDoiIjtzOjExOiJwaG9uZV9pbnRlciI7czowOiIiO3M6MzoidXJsIjtzOjA6IiI7czo3OiJjb25maXJtIjtzOjA6IiI7czo2OiJjdXN0b20iO3M6MDoiIjt9fWk6MTI7YToxODp7czo0OiJuYW1lIjtzOjE4OiJyZWdpc3RyYXRpb25kYXRldG8iO3M6MjoiaWQiO3M6MTg6InJlZ2lzdHJhdGlvbmRhdGV0byI7czo1OiJ2YWx1ZSI7czowOiIiO3M6NToibGFiZWwiO2E6Mjp7czo0OiJ0ZXh0IjtzOjM0OiJVdGVudGUgcmVnaXN0cmF0byBwcmltYSBkZWxsYSBkYXRhIjtzOjg6InBvc2l0aW9uIjtzOjQ6ImxlZnQiO31zOjg6InN1YmxhYmVsIjtzOjc1OiJMaW1pdGUgc3VwZXJpb3JlIGRlbGwnaW50ZXJ2YWxsbyBkZWxsZSBkYXRlIGRpIHJlZ2lzdHJhemlvbmUgZGEgY29uc2lkZXJhcmUiO3M6MTE6InBsYWNlaG9sZGVyIjtzOjA6IiI7czo5OiJtYXhsZW5ndGgiO3M6MDoiIjtzOjQ6InNpemUiO3M6MDoiIjtzOjU6ImNsYXNzIjtzOjA6IiI7czo1OiJ0aXRsZSI7czowOiIiO3M6NToic3R5bGUiO3M6MDoiIjtzOjE1OiI6ZGF0YS1pbnB1dG1hc2siO3M6MjI6IidhbGlhcycgOiAnZGQtbW0teXl5eSciO3M6NjoicGFyYW1zIjtzOjA6IiI7czoxNjoiOmRhdGEtbG9hZC1zdGF0ZSI7czowOiIiO3M6MTM6IjpkYXRhLXRvb2x0aXAiO3M6MDoiIjtzOjQ6InR5cGUiO3M6NDoidGV4dCI7czoxMjoiY29udGFpbmVyX2lkIjtzOjE6IjAiO3M6MTA6InZhbGlkYXRpb24iO2E6MTM6e3M6ODoicmVxdWlyZWQiO3M6MDoiIjtzOjU6ImFscGhhIjtzOjA6IiI7czo4OiJhbHBoYW51bSI7czowOiIiO3M6NToiZGlnaXQiO3M6MDoiIjtzOjc6Im5vZGlnaXQiO3M6MDoiIjtzOjc6Im5vc3BhY2UiO3M6MDoiIjtzOjY6Im51bWJlciI7czowOiIiO3M6NToiZW1haWwiO3M6MDoiIjtzOjU6InBob25lIjtzOjA6IiI7czoxMToicGhvbmVfaW50ZXIiO3M6MDoiIjtzOjM6InVybCI7czowOiIiO3M6NzoiY29uZmlybSI7czowOiIiO3M6NjoiY3VzdG9tIjtzOjA6IiI7fX1pOjEzO2E6MTg6e3M6NDoibmFtZSI7czoxMDoiY3JlYXRlZGluZiI7czoyOiJpZCI7czoxMDoiY3JlYXRlZGluZiI7czo1OiJ2YWx1ZSI7czowOiIiO3M6NToibGFiZWwiO2E6Mjp7czo0OiJ0ZXh0IjtzOjMwOiJMaW1pdGUgaW5mZXJpb3JlIGV2ZW50aSBjcmVhdGkiO3M6ODoicG9zaXRpb24iO3M6NDoibGVmdCI7fXM6ODoic3VibGFiZWwiO3M6NTc6IkxpbWl0ZSBpbmZlcmlvcmUgc3VsIG51bWVybyBkaSBldmVudGkgY3JlYXRpIGRhZ2xpIHV0ZW50ZSI7czoxMToicGxhY2Vob2xkZXIiO3M6MDoiIjtzOjk6Im1heGxlbmd0aCI7czowOiIiO3M6NDoic2l6ZSI7czowOiIiO3M6NToiY2xhc3MiO3M6MDoiIjtzOjU6InRpdGxlIjtzOjA6IiI7czo1OiJzdHlsZSI7czowOiIiO3M6MTU6IjpkYXRhLWlucHV0bWFzayI7czowOiIiO3M6NjoicGFyYW1zIjtzOjA6IiI7czoxNjoiOmRhdGEtbG9hZC1zdGF0ZSI7czowOiIiO3M6MTM6IjpkYXRhLXRvb2x0aXAiO3M6MDoiIjtzOjQ6InR5cGUiO3M6NDoidGV4dCI7czoxMjoiY29udGFpbmVyX2lkIjtzOjE6IjAiO3M6MTA6InZhbGlkYXRpb24iO2E6MTM6e3M6ODoicmVxdWlyZWQiO3M6MDoiIjtzOjU6ImFscGhhIjtzOjA6IiI7czo4OiJhbHBoYW51bSI7czowOiIiO3M6NToiZGlnaXQiO3M6MDoiIjtzOjc6Im5vZGlnaXQiO3M6MDoiIjtzOjc6Im5vc3BhY2UiO3M6MDoiIjtzOjY6Im51bWJlciI7czoxOiIxIjtzOjU6ImVtYWlsIjtzOjA6IiI7czo1OiJwaG9uZSI7czowOiIiO3M6MTE6InBob25lX2ludGVyIjtzOjA6IiI7czozOiJ1cmwiO3M6MDoiIjtzOjc6ImNvbmZpcm0iO3M6MDoiIjtzOjY6ImN1c3RvbSI7czowOiIiO319aToxNDthOjE4OntzOjQ6Im5hbWUiO3M6MTA6ImNyZWF0ZWRzdXAiO3M6MjoiaWQiO3M6MTA6ImNyZWF0ZWRzdXAiO3M6NToidmFsdWUiO3M6MDoiIjtzOjU6ImxhYmVsIjthOjI6e3M6NDoidGV4dCI7czozMDoiTGltaXRlIHN1cGVyaW9yZSBldmVudGkgY3JlYXRpIjtzOjg6InBvc2l0aW9uIjtzOjQ6ImxlZnQiO31zOjg6InN1YmxhYmVsIjtzOjU3OiJMaW1pdGUgc3VwZXJpb3JlIHN1bCBudW1lcm8gZGkgZXZlbnRpIGNyZWF0aSBkYWdsaSB1dGVudGkiO3M6MTE6InBsYWNlaG9sZGVyIjtzOjA6IiI7czo5OiJtYXhsZW5ndGgiO3M6MDoiIjtzOjQ6InNpemUiO3M6MDoiIjtzOjU6ImNsYXNzIjtzOjA6IiI7czo1OiJ0aXRsZSI7czowOiIiO3M6NToic3R5bGUiO3M6MDoiIjtzOjE1OiI6ZGF0YS1pbnB1dG1hc2siO3M6MDoiIjtzOjY6InBhcmFtcyI7czowOiIiO3M6MTY6IjpkYXRhLWxvYWQtc3RhdGUiO3M6MDoiIjtzOjEzOiI6ZGF0YS10b29sdGlwIjtzOjA6IiI7czo0OiJ0eXBlIjtzOjQ6InRleHQiO3M6MTI6ImNvbnRhaW5lcl9pZCI7czoxOiIwIjtzOjEwOiJ2YWxpZGF0aW9uIjthOjEzOntzOjg6InJlcXVpcmVkIjtzOjA6IiI7czo1OiJhbHBoYSI7czowOiIiO3M6ODoiYWxwaGFudW0iO3M6MDoiIjtzOjU6ImRpZ2l0IjtzOjA6IiI7czo3OiJub2RpZ2l0IjtzOjA6IiI7czo3OiJub3NwYWNlIjtzOjA6IiI7czo2OiJudW1iZXIiO3M6MToiMSI7czo1OiJlbWFpbCI7czowOiIiO3M6NToicGhvbmUiO3M6MDoiIjtzOjExOiJwaG9uZV9pbnRlciI7czowOiIiO3M6MzoidXJsIjtzOjA6IiI7czo3OiJjb25maXJtIjtzOjA6IiI7czo2OiJjdXN0b20iO3M6MDoiIjt9fWk6MTU7YToxODp7czo0OiJuYW1lIjtzOjk6ImJvb2tlZGluZiI7czoyOiJpZCI7czo5OiJib29rZWRpbmYiO3M6NToidmFsdWUiO3M6MDoiIjtzOjU6ImxhYmVsIjthOjI6e3M6NDoidGV4dCI7czozMzoiTGltaXRlIGluZmVyaW9yZSBldmVudGkgcHJlbm90YXRpIjtzOjg6InBvc2l0aW9uIjtzOjQ6ImxlZnQiO31zOjg6InN1YmxhYmVsIjtzOjEwMjoiTGltaXRlIGluZmVyaW9yZSBzdWwgbnVtZXJvIGRpIGV2ZW50aSBwZXIgY3VpIGdsaSB1dGVudGkgaGFubm8gaW52aWF0byB1bmEgcmljaGllc3RhIGRpIHBhcnRlY2lwYXppb25lIjtzOjExOiJwbGFjZWhvbGRlciI7czowOiIiO3M6OToibWF4bGVuZ3RoIjtzOjA6IiI7czo0OiJzaXplIjtzOjA6IiI7czo1OiJjbGFzcyI7czowOiIiO3M6NToidGl0bGUiO3M6MDoiIjtzOjU6InN0eWxlIjtzOjA6IiI7czoxNToiOmRhdGEtaW5wdXRtYXNrIjtzOjA6IiI7czo2OiJwYXJhbXMiO3M6MDoiIjtzOjE2OiI6ZGF0YS1sb2FkLXN0YXRlIjtzOjA6IiI7czoxMzoiOmRhdGEtdG9vbHRpcCI7czowOiIiO3M6NDoidHlwZSI7czo0OiJ0ZXh0IjtzOjEyOiJjb250YWluZXJfaWQiO3M6MToiMCI7czoxMDoidmFsaWRhdGlvbiI7YToxMzp7czo4OiJyZXF1aXJlZCI7czowOiIiO3M6NToiYWxwaGEiO3M6MDoiIjtzOjg6ImFscGhhbnVtIjtzOjA6IiI7czo1OiJkaWdpdCI7czowOiIiO3M6Nzoibm9kaWdpdCI7czowOiIiO3M6Nzoibm9zcGFjZSI7czowOiIiO3M6NjoibnVtYmVyIjtzOjE6IjEiO3M6NToiZW1haWwiO3M6MDoiIjtzOjU6InBob25lIjtzOjA6IiI7czoxMToicGhvbmVfaW50ZXIiO3M6MDoiIjtzOjM6InVybCI7czowOiIiO3M6NzoiY29uZmlybSI7czowOiIiO3M6NjoiY3VzdG9tIjtzOjA6IiI7fX1pOjE2O2E6MTg6e3M6NDoibmFtZSI7czo5OiJib29rZWRzdXAiO3M6MjoiaWQiO3M6OToiYm9va2Vkc3VwIjtzOjU6InZhbHVlIjtzOjA6IiI7czo1OiJsYWJlbCI7YToyOntzOjQ6InRleHQiO3M6MzM6IkxpbWl0ZSBzdXBlcmlvcmUgZXZlbnRpIHByZW5vdGF0aSI7czo4OiJwb3NpdGlvbiI7czo0OiJsZWZ0Ijt9czo4OiJzdWJsYWJlbCI7czoxMDI6IkxpbWl0ZSBzdXBlcmlvcmUgc3VsIG51bWVybyBkaSBldmVudGkgcGVyIGN1aSBnbGkgdXRlbnRpIGhhbm5vIGludmlhdG8gdW5hIHJpY2hpZXN0YSBkaSBwYXJ0ZWNpcGF6aW9uZSI7czoxMToicGxhY2Vob2xkZXIiO3M6MDoiIjtzOjk6Im1heGxlbmd0aCI7czowOiIiO3M6NDoic2l6ZSI7czowOiIiO3M6NToiY2xhc3MiO3M6MDoiIjtzOjU6InRpdGxlIjtzOjA6IiI7czo1OiJzdHlsZSI7czowOiIiO3M6MTU6IjpkYXRhLWlucHV0bWFzayI7czowOiIiO3M6NjoicGFyYW1zIjtzOjA6IiI7czoxNjoiOmRhdGEtbG9hZC1zdGF0ZSI7czowOiIiO3M6MTM6IjpkYXRhLXRvb2x0aXAiO3M6MDoiIjtzOjQ6InR5cGUiO3M6NDoidGV4dCI7czoxMjoiY29udGFpbmVyX2lkIjtzOjE6IjAiO3M6MTA6InZhbGlkYXRpb24iO2E6MTM6e3M6ODoicmVxdWlyZWQiO3M6MDoiIjtzOjU6ImFscGhhIjtzOjA6IiI7czo4OiJhbHBoYW51bSI7czowOiIiO3M6NToiZGlnaXQiO3M6MDoiIjtzOjc6Im5vZGlnaXQiO3M6MDoiIjtzOjc6Im5vc3BhY2UiO3M6MDoiIjtzOjY6Im51bWJlciI7czoxOiIxIjtzOjU6ImVtYWlsIjtzOjA6IiI7czo1OiJwaG9uZSI7czowOiIiO3M6MTE6InBob25lX2ludGVyIjtzOjA6IiI7czozOiJ1cmwiO3M6MDoiIjtzOjc6ImNvbmZpcm0iO3M6MDoiIjtzOjY6ImN1c3RvbSI7czowOiIiO319aToxNzthOjEwOntzOjExOiJyZW5kZXJfdHlwZSI7czo2OiJzdWJtaXQiO3M6NDoibmFtZSI7czo4OiJidXR0b24xNyI7czoyOiJpZCI7czo4OiJidXR0b24xNyI7czo0OiJ0eXBlIjtzOjY6InN1Ym1pdCI7czo1OiJ2YWx1ZSI7czo1OiJDZXJjYSI7czo1OiJjbGFzcyI7czoxNToiYnRuIGJ0bi1kZWZhdWx0IjtzOjU6InN0eWxlIjtzOjA6IiI7czo2OiJwYXJhbXMiO3M6MDoiIjtzOjE2OiI6ZGF0YS1sb2FkLXN0YXRlIjtzOjA6IiI7czoxMjoiY29udGFpbmVyX2lkIjtzOjE6IjAiO319czozOiJETkEiO2E6Mjp7czo0OiJsb2FkIjthOjQ6e3M6OToiZGJfcmVhZF8xIjthOjI6e3M6NToiZm91bmQiO3M6MDoiIjtzOjk6Im5vdF9mb3VuZCI7czowOiIiO31zOjE3OiJzZXNzaW9uX3RvX2RhdGFfMiI7czowOiIiO3M6NjoiaHRtbF8wIjtzOjA6IiI7czoxMzoiY3VzdG9tX2NvZGVfNiI7czowOiIiO31zOjY6InN1Ym1pdCI7YTozOntzOjEzOiJjdXN0b21fY29kZV81IjtzOjA6IiI7czoxNzoiZGF0YV90b19zZXNzaW9uXzMiO3M6MDoiIjtzOjEwOiJyZWRpcmVjdF80IjtzOjA6IiI7fX1zOjE0OiJhY3Rpb25zX2NvbmZpZyI7YTo3OntpOjE7YToxOTp7czoxMjoiYWN0aW9uX2xhYmVsIjtzOjMyOiJDYXJpY2FtZW50byBkZWxsYSBsaXN0YSBkZWkgam9icyI7czo3OiJlbmFibGVkIjtzOjE6IjEiO3M6OToidGFibGVuYW1lIjtzOjQ6ImpvYnMiO3M6MTA6Im11bHRpX3JlYWQiO3M6MToiMSI7czoxODoibG9hZF91bmRlcl9tb2RlbGlkIjtzOjE6IjEiO3M6ODoibW9kZWxfaWQiO3M6ODoiam9ic2xpc3QiO3M6NjoiZmllbGRzIjtzOjg6ImlkLCBuYW1lIjtzOjU6Im9yZGVyIjtzOjQ6Im5hbWUiO3M6NToiZ3JvdXAiO3M6MDoiIjtzOjEwOiJjb25kaXRpb25zIjtzOjA6IiI7czoxNjoiZW5hYmxlX3JlbGF0aW9ucyI7czoxOiIwIjtzOjk6InJlbGF0aW9ucyI7YToxOntpOjA7YTo0OntzOjU6Im1vZGVsIjtzOjA6IiI7czo5OiJ0YWJsZW5hbWUiO3M6MDoiIjtzOjQ6InR5cGUiO3M6NjoiaGFzT25lIjtzOjQ6ImZrZXkiO3M6MDoiIjt9fXM6MTA6Im5kYl9lbmFibGUiO3M6MToiMCI7czoxMDoibmRiX2RyaXZlciI7czo1OiJteXNxbCI7czo4OiJuZGJfaG9zdCI7czo5OiJsb2NhbGhvc3QiO3M6MTI6Im5kYl9kYXRhYmFzZSI7czowOiIiO3M6ODoibmRiX3VzZXIiO3M6MDoiIjtzOjEyOiJuZGJfcGFzc3dvcmQiO3M6MDoiIjtzOjEwOiJuZGJfcHJlZml4IjtzOjQ6Impvc18iO31pOjI7YTozOntzOjU6ImNsZWFyIjtzOjE6IjAiO3M6Mzoia2V5IjtzOjE4OiJ1c2VyX3NlYXJjaF9wYXJhbXMiO3M6MTg6Im92ZXJ3cml0ZV9vbGRfZGF0YSI7czoxOiIwIjt9aTowO2E6MTE6e3M6NDoicGFnZSI7czoxOiIxIjtzOjEyOiJzdWJtaXRfZXZlbnQiO3M6Njoic3VibWl0IjtzOjExOiJmb3JtX21ldGhvZCI7czo0OiJmaWxlIjtzOjEwOiJhY3Rpb25fdXJsIjtzOjA6IiI7czoxMDoiZm9ybV9jbGFzcyI7czoxMDoiY2hyb25vZm9ybSI7czoxNToiZm9ybV90YWdfYXR0YWNoIjtzOjA6IiI7czoyNDoicmVxdWlyZWRfbGFiZWxzX2lkZW50aWZ5IjtzOjE6IjEiO3M6MTI6InJlbGF0aXZlX3VybCI7czoxOiIxIjtzOjExOiJhamF4X3N1Ym1pdCI7czoxOiIwIjtzOjEzOiJhZGRfZm9ybV90YWdzIjtzOjE6IjEiO3M6OToieGh0bWxfdXJsIjtzOjE6IjAiO31pOjY7YToyOntzOjEyOiJhY3Rpb25fbGFiZWwiO3M6MTk6IkZvcm1hdHRhemlvbmUgZW1haWwiO3M6NzoiY29udGVudCI7czo4ODoiPHNjcmlwdD4NCmpRdWVyeSgnI2VtYWlsJykudmFsKGpRdWVyeSgnI2VtYWlsJykudmFsKCkucmVwbGFjZSgnJiM2NDsnLCAnQCcpKTsNCjwvc2NyaXB0PiI7fWk6NTthOjI6e3M6MTI6ImFjdGlvbl9sYWJlbCI7czoxOToiRm9ybWF0dGF6aW9uZSBlbWFpbCI7czo3OiJjb250ZW50IjtzOjg1OiI8P3BocA0KICAgJGZvcm0tPmRhdGFbJ2VtYWlsJ10gPSBzdHJfcmVwbGFjZSgiQCIsICImIzY0OyIsICRmb3JtLT5kYXRhWydlbWFpbCddKTsNCj8+Ijt9aTozO2E6Mjp7czo1OiJtZXJnZSI7czoxOiIwIjtzOjM6ImtleSI7czoxODoidXNlcl9zZWFyY2hfcGFyYW1zIjt9aTo0O2E6Mjp7czozOiJ1cmwiO3M6MTY6ImluZGV4LnBocC91dGVudGkiO3M6MTI6ImV4dHJhX3BhcmFtcyI7czowOiIiO319czoxNDoiZGJfZmllbGRzX2xpc3QiO2E6MTp7czo0OiJqb2JzIjtzOjA6IiI7fX0=', 1, 'users', 1, '<div class="form-group gcore-form-row" id="form-row-status"><label for="status" class="control-label gcore-label-left">Stato degli utenti</label>\n<div class="gcore-input gcore-display-table" id="fin-status"><select name="status" id="status" size="" class="form-control A" title="" style="" data-load-state="" data-tooltip="">\n<option value="">Non Impostato</option>\n<option value="1">Attivi</option>\n<option value="0">Bannati</option>\n</select><span class="help-block">Filtro per la selezione dei soli utenti attivi o bannati</span></div></div><div class="form-group gcore-form-row" id="form-row-email"><label for="email" class="control-label gcore-label-left">Email</label>\n<div class="gcore-input gcore-display-table" id="fin-email"><input name="email" id="email" value="" placeholder="" maxlength="" size="" class="form-control A" title="" style="" data-inputmask="" data-load-state="" data-tooltip="" type="text" /><span class="help-block">Sottostringa contenuta nell''indirizzo di posta elettronica</span></div></div><div class="form-group gcore-form-row" id="form-row-name"><label for="name" class="control-label gcore-label-left">Nome utente</label>\n<div class="gcore-input gcore-display-table" id="fin-name"><input name="name" id="name" value="" placeholder="" maxlength="" size="" class="form-control A" title="" style="" data-inputmask="" data-load-state="" data-tooltip="" type="text" /><span class="help-block">Ricerca nel nome dell''utente</span></div></div><div class="form-group gcore-form-row" id="form-row-surname"><label for="surname" class="control-label gcore-label-left">Cognome utente</label>\n<div class="gcore-input gcore-display-table" id="fin-surname"><input name="surname" id="surname" value="" placeholder="" maxlength="" size="" class="form-control A" title="" style="" data-inputmask="" data-load-state="" data-tooltip="" type="text" /><span class="help-block">Ricerca nel cognome dell''utente</span></div></div><div class="form-group gcore-form-row" id="form-row-birthdatefrom"><label for="birthdatefrom" class="control-label gcore-label-left">Nato dopo la data </label>\n<div class="gcore-input gcore-display-table" id="fin-birthdatefrom"><input name="birthdatefrom" id="birthdatefrom" value="" placeholder="" maxlength="" size="" class="form-control A" title="" style="" data-inputmask="&#039;alias&#039; : &#039;dd-mm-yyyy&#039;" data-load-state="" data-tooltip="" type="text" /><span class="help-block">Limite inferiore dell''intervallo delle date di nascita da considerare</span></div></div><div class="form-group gcore-form-row" id="form-row-birthdateto"><label for="birthdateto" class="control-label gcore-label-left">Nato prima della data</label>\n<div class="gcore-input gcore-display-table" id="fin-birthdateto"><input name="birthdateto" id="birthdateto" value="" placeholder="" maxlength="" size="" class="form-control A" title="" style="" data-inputmask="&#039;alias&#039; : &#039;dd-mm-yyyy&#039;" data-load-state="" data-tooltip="" type="text" /><span class="help-block">Limite superiore dell''intervallo delle date di nascita da considerare</span></div></div><div class="form-group gcore-form-row" id="form-row-sex"><label for="sex" class="control-label gcore-label-left">Sex</label>\n<div class="gcore-input gcore-display-table" id="fin-sex"><select name="sex" id="sex" size="" class="form-control A" title="" style="" data-load-state="" data-tooltip="">\n<option value="">Non Impostato</option>\n<option value="0">Uomo</option>\n<option value="1">Donna</option>\n</select><span class="help-block">Filtro di ricerca sul sesso dell''utente</span></div></div><?php\n$keys = \\GCore\\Libs\\Arr::getVal(\\GCore\\Libs\\Arr::getVal($form->data, explode(".", "jobslist")), explode(".", "[n].id"));\n$values = \\GCore\\Libs\\Arr::getVal(\\GCore\\Libs\\Arr::getVal($form->data, explode(".", "jobslist")), explode(".", "[n].name"));\n$options = array_combine($keys, $values);\n$field = array (\n  ''name'' => ''job'',\n  ''id'' => ''job'',\n  ''options'' => \n  array (\n  ),\n  ''empty'' => ''Non Impostato'',\n  ''values'' => \n  array (\n  ),\n  ''label'' => \n  array (\n    ''text'' => ''Job'',\n    ''position'' => ''left'',\n  ),\n  ''sublabel'' => ''Filtro di ricerca sulla professione dell\\''utente'',\n  ''multiple'' => ''0'',\n  ''size'' => '''',\n  ''class'' => '''',\n  ''title'' => '''',\n  ''style'' => '''',\n  ''params'' => '''',\n  '':data-load-state'' => '''',\n  '':data-tooltip'' => '''',\n  ''type'' => ''dropdown'',\n  ''container_id'' => ''0'',\n);\n$field["options"] = $options;\necho \\GCore\\Helpers\\Html::formLine($field["name"], $field);\n?><div class="form-group gcore-form-row" id="form-row-city"><label for="city" class="control-label gcore-label-left">Città</label>\n<div class="gcore-input gcore-display-table" id="fin-city"><input name="city" id="city" value="" placeholder="" maxlength="" size="" class="form-control A" title="" style="" data-inputmask="" data-load-state="" data-tooltip="" type="text" /><span class="help-block">Ricerca nel nome della città inserita dall''utente</span></div></div><div class="form-group gcore-form-row" id="form-row-interests"><label for="interests" class="control-label gcore-label-left">Interessi</label>\n<div class="gcore-input gcore-display-table" id="fin-interests"><textarea name="interests" id="interests" placeholder="" rows="3" cols="40" class="form-control A" title="" style="" data-wysiwyg="0" data-load-state="" data-tooltip=""></textarea><span class="help-block">Sequenza di token che possono trovarsi tra gli interessi dell''utente separati da una virgola</span></div></div><div class="form-group gcore-form-row" id="form-row-registrationtimeto"><label for="registrationtimeto" class="control-label gcore-label-left">Utente registrato dopo la data</label>\n<div class="gcore-input gcore-display-table" id="fin-registrationtimeto"><input name="registrationtimefrom" id="registrationtimeto" value="" placeholder="" maxlength="" size="" class="form-control A" title="" style="" data-inputmask="&#039;alias&#039; : &#039;dd-mm-yyyy&#039;" data-load-state="" data-tooltip="" type="text" /><span class="help-block">Limite inferiore dell''intervallo delle date di di registrazione da considerare</span></div></div><div class="form-group gcore-form-row" id="form-row-registrationdateto"><label for="registrationdateto" class="control-label gcore-label-left">Utente registrato prima della data</label>\n<div class="gcore-input gcore-display-table" id="fin-registrationdateto"><input name="registrationdateto" id="registrationdateto" value="" placeholder="" maxlength="" size="" class="form-control A" title="" style="" data-inputmask="&#039;alias&#039; : &#039;dd-mm-yyyy&#039;" data-load-state="" data-tooltip="" type="text" /><span class="help-block">Limite superiore dell''intervallo delle date di registrazione da considerare</span></div></div><div class="form-group gcore-form-row" id="form-row-createdinf"><label for="createdinf" class="control-label gcore-label-left">Limite inferiore eventi creati</label>\n<div class="gcore-input gcore-display-table" id="fin-createdinf"><input name="createdinf" id="createdinf" value="" placeholder="" maxlength="" size="" class="validate[&#039;number&#039;] form-control A" title="" style="" data-inputmask="" data-load-state="" data-tooltip="" type="text" /><span class="help-block">Limite inferiore sul numero di eventi creati dagli utente</span></div></div><div class="form-group gcore-form-row" id="form-row-createdsup"><label for="createdsup" class="control-label gcore-label-left">Limite superiore eventi creati</label>\n<div class="gcore-input gcore-display-table" id="fin-createdsup"><input name="createdsup" id="createdsup" value="" placeholder="" maxlength="" size="" class="validate[&#039;number&#039;] form-control A" title="" style="" data-inputmask="" data-load-state="" data-tooltip="" type="text" /><span class="help-block">Limite superiore sul numero di eventi creati dagli utenti</span></div></div><div class="form-group gcore-form-row" id="form-row-bookedinf"><label for="bookedinf" class="control-label gcore-label-left">Limite inferiore eventi prenotati</label>\n<div class="gcore-input gcore-display-table" id="fin-bookedinf"><input name="bookedinf" id="bookedinf" value="" placeholder="" maxlength="" size="" class="validate[&#039;number&#039;] form-control A" title="" style="" data-inputmask="" data-load-state="" data-tooltip="" type="text" /><span class="help-block">Limite inferiore sul numero di eventi per cui gli utenti hanno inviato una richiesta di partecipazione</span></div></div><div class="form-group gcore-form-row" id="form-row-bookedsup"><label for="bookedsup" class="control-label gcore-label-left">Limite superiore eventi prenotati</label>\n<div class="gcore-input gcore-display-table" id="fin-bookedsup"><input name="bookedsup" id="bookedsup" value="" placeholder="" maxlength="" size="" class="validate[&#039;number&#039;] form-control A" title="" style="" data-inputmask="" data-load-state="" data-tooltip="" type="text" /><span class="help-block">Limite superiore sul numero di eventi per cui gli utenti hanno inviato una richiesta di partecipazione</span></div></div><div class="form-group gcore-form-row" id="form-row-button17"><div class="gcore-input gcore-display-table" id="fin-button17"><input name="button17" id="button17" type="submit" value="Cerca" class="btn btn-default form-control A" style="" data-load-state="" /></div></div>');
INSERT INTO `zcp8_chronoengine_chronoforms` (`id`, `title`, `params`, `extras`, `published`, `app`, `form_type`, `content`) VALUES
(5, 'event_update', '{"description":"Modifica degli attributi associati ad un evento","setup":"0","theme":"bootstrap3","tight_layout":"0","rtl_support":"0","labels_right_aligned":"0","labels_auto_width":"0","js_validation_language":""}', 'YTo0OntzOjY6ImZpZWxkcyI7YToyMTp7aTo4O2E6MjA6e3M6NDoibmFtZSI7czo2OiJzdGF0dXMiO3M6MjoiaWQiO3M6Njoic3RhdHVzIjtzOjc6Im9wdGlvbnMiO3M6Mjc6IjA9RGlzYWJpbGl0YXRvDQoxPUFiaWxpdGF0byI7czo1OiJlbXB0eSI7czowOiIiO3M6NjoidmFsdWVzIjtzOjA6IiI7czo1OiJsYWJlbCI7YToyOntzOjQ6InRleHQiO3M6NToiU3RhdG8iO3M6ODoicG9zaXRpb24iO3M6NDoibGVmdCI7fXM6ODoic3VibGFiZWwiO3M6OTI6IlNhcsOgIHBvc3NpYmlsZSBhY2NlZGVyZSBhbGwnZXZlbnRvIGxhdG8gY2xpZW50IHNvbG8gc2Ugw6ggc2VsZXppb25hdGEgbCdvcHppb25lICJBYmlsaXRhdG8iIjtzOjg6Im11bHRpcGxlIjtzOjE6IjAiO3M6NDoic2l6ZSI7czowOiIiO3M6NToiY2xhc3MiO3M6MDoiIjtzOjU6InRpdGxlIjtzOjA6IiI7czo1OiJzdHlsZSI7czowOiIiO3M6NjoicGFyYW1zIjtzOjA6IiI7czoxNjoiOmRhdGEtbG9hZC1zdGF0ZSI7czowOiIiO3M6MTM6IjpkYXRhLXRvb2x0aXAiO3M6MDoiIjtzOjQ6InR5cGUiO3M6ODoiZHJvcGRvd24iO3M6MTI6ImNvbnRhaW5lcl9pZCI7czoxOiIwIjtzOjEwOiJ2YWxpZGF0aW9uIjthOjM6e3M6ODoicmVxdWlyZWQiO3M6MDoiIjtzOjc6ImNvbmZpcm0iO3M6MDoiIjtzOjY6ImN1c3RvbSI7czowOiIiO31zOjEyOiJkeW5hbWljX2RhdGEiO2E6NDp7czo3OiJlbmFibGVkIjtzOjA6IiI7czo5OiJkYXRhX3BhdGgiO3M6MDoiIjtzOjk6InZhbHVlX2tleSI7czowOiIiO3M6ODoidGV4dF9rZXkiO3M6MDoiIjt9czo2OiJldmVudHMiO2E6MTp7aTowO2E6NTp7czo4OiJvcGVyYXRvciI7czoxOiI9IjtzOjU6InN0YXRlIjtzOjA6IiI7czo2OiJhY3Rpb24iO3M6MDoiIjtzOjY6InRhcmdldCI7czowOiIiO3M6Nzoib3B0aW9ucyI7czowOiIiO319fWk6MTthOjE4OntzOjQ6Im5hbWUiO3M6NToidGl0bGUiO3M6MjoiaWQiO3M6NToidGl0bGUiO3M6NToidmFsdWUiO3M6MDoiIjtzOjU6ImxhYmVsIjthOjI6e3M6NDoidGV4dCI7czoxNjoiTm9tZSBkZWxsJ2V2ZW50byI7czo4OiJwb3NpdGlvbiI7czo0OiJsZWZ0Ijt9czo4OiJzdWJsYWJlbCI7czozMDoiSW5zZXJpcmUgaWwgdGl0b2xvIGRlbGwnZXZlbnRvIjtzOjExOiJwbGFjZWhvbGRlciI7czoxODoiVGl0b2xvIGRlbGwnZXZlbnRvIjtzOjk6Im1heGxlbmd0aCI7czoyOiIzMCI7czo0OiJzaXplIjtzOjA6IiI7czo1OiJjbGFzcyI7czowOiIiO3M6NToidGl0bGUiO3M6MDoiIjtzOjU6InN0eWxlIjtzOjA6IiI7czoxNToiOmRhdGEtaW5wdXRtYXNrIjtzOjA6IiI7czo2OiJwYXJhbXMiO3M6MDoiIjtzOjE2OiI6ZGF0YS1sb2FkLXN0YXRlIjtzOjA6IiI7czoxMzoiOmRhdGEtdG9vbHRpcCI7czowOiIiO3M6NDoidHlwZSI7czo0OiJ0ZXh0IjtzOjEyOiJjb250YWluZXJfaWQiO3M6MToiMCI7czoxMDoidmFsaWRhdGlvbiI7YToxMzp7czo4OiJyZXF1aXJlZCI7czoxOiIxIjtzOjU6ImFscGhhIjtzOjA6IiI7czo4OiJhbHBoYW51bSI7czowOiIiO3M6NToiZGlnaXQiO3M6MDoiIjtzOjc6Im5vZGlnaXQiO3M6MDoiIjtzOjc6Im5vc3BhY2UiO3M6MDoiIjtzOjY6Im51bWJlciI7czowOiIiO3M6NToiZW1haWwiO3M6MDoiIjtzOjU6InBob25lIjtzOjA6IiI7czoxMToicGhvbmVfaW50ZXIiO3M6MDoiIjtzOjM6InVybCI7czowOiIiO3M6NzoiY29uZmlybSI7czowOiIiO3M6NjoiY3VzdG9tIjtzOjA6IiI7fX1pOjI7YToyMDp7czo0OiJuYW1lIjtzOjE0OiJzdXBlcmNhdGVnb3JpYSI7czoyOiJpZCI7czoxNDoic3VwZXJjYXRlZ29yaWEiO3M6Nzoib3B0aW9ucyI7czowOiIiO3M6NToiZW1wdHkiO3M6MDoiIjtzOjY6InZhbHVlcyI7czowOiIiO3M6NToibGFiZWwiO2E6Mjp7czo0OiJ0ZXh0IjtzOjE1OiJTdXBlciBDYXRlZ29yaWEiO3M6ODoicG9zaXRpb24iO3M6NDoibGVmdCI7fXM6ODoic3VibGFiZWwiO3M6Njk6IlNlbGV6aW9uYXJlIHVuYSBmYW1pZ2xpYSBkaSBDYXRlZ29yaWUgYSBjdWkgbCdldmVudG8gZGV2ZSBhcHBhcnRlbmVyZSI7czo4OiJtdWx0aXBsZSI7czoxOiIwIjtzOjQ6InNpemUiO3M6MDoiIjtzOjU6ImNsYXNzIjtzOjA6IiI7czo1OiJ0aXRsZSI7czowOiIiO3M6NToic3R5bGUiO3M6MDoiIjtzOjY6InBhcmFtcyI7czowOiIiO3M6MTY6IjpkYXRhLWxvYWQtc3RhdGUiO3M6MDoiIjtzOjEzOiI6ZGF0YS10b29sdGlwIjtzOjA6IiI7czo0OiJ0eXBlIjtzOjg6ImRyb3Bkb3duIjtzOjEyOiJjb250YWluZXJfaWQiO3M6MToiMCI7czoxMDoidmFsaWRhdGlvbiI7YTozOntzOjg6InJlcXVpcmVkIjtzOjA6IiI7czo3OiJjb25maXJtIjtzOjA6IiI7czo2OiJjdXN0b20iO3M6MDoiIjt9czoxMjoiZHluYW1pY19kYXRhIjthOjQ6e3M6NzoiZW5hYmxlZCI7czoxOiIxIjtzOjk6ImRhdGFfcGF0aCI7czoxOToic3VwZXJjYXRlZ29yaWVzbGlzdCI7czo5OiJ2YWx1ZV9rZXkiO3M6MjoiaWQiO3M6ODoidGV4dF9rZXkiO3M6NDoibmFtZSI7fXM6NjoiZXZlbnRzIjthOjE6e2k6MDthOjU6e3M6ODoib3BlcmF0b3IiO3M6MToiPSI7czo1OiJzdGF0ZSI7czowOiIiO3M6NjoiYWN0aW9uIjtzOjA6IiI7czo2OiJ0YXJnZXQiO3M6MDoiIjtzOjc6Im9wdGlvbnMiO3M6MDoiIjt9fX1pOjIyO2E6MjA6e3M6NDoibmFtZSI7czo5OiJjYXRlZ29yaWEiO3M6MjoiaWQiO3M6OToiY2F0ZWdvcmlhIjtzOjc6Im9wdGlvbnMiO3M6MDoiIjtzOjU6ImVtcHR5IjtzOjA6IiI7czo2OiJ2YWx1ZXMiO3M6MDoiIjtzOjU6ImxhYmVsIjthOjI6e3M6NDoidGV4dCI7czoxNToiU290dG8gQ2F0ZWdvcmlhIjtzOjg6InBvc2l0aW9uIjtzOjQ6ImxlZnQiO31zOjg6InN1YmxhYmVsIjtzOjY4OiJTZWxlemlvbmFyZSB1bmEgZGVsbGUgc290dG9jYXRlZ29yaWUgYSBjdWkgbCdldmVudG8gZGV2ZSBhcHBhcnRlbmVyZSI7czo4OiJtdWx0aXBsZSI7czoxOiIwIjtzOjQ6InNpemUiO3M6MDoiIjtzOjU6ImNsYXNzIjtzOjA6IiI7czo1OiJ0aXRsZSI7czowOiIiO3M6NToic3R5bGUiO3M6MDoiIjtzOjY6InBhcmFtcyI7czowOiIiO3M6MTY6IjpkYXRhLWxvYWQtc3RhdGUiO3M6MDoiIjtzOjEzOiI6ZGF0YS10b29sdGlwIjtzOjA6IiI7czo0OiJ0eXBlIjtzOjg6ImRyb3Bkb3duIjtzOjEyOiJjb250YWluZXJfaWQiO3M6MToiMCI7czoxMDoidmFsaWRhdGlvbiI7YTozOntzOjg6InJlcXVpcmVkIjtzOjA6IiI7czo3OiJjb25maXJtIjtzOjA6IiI7czo2OiJjdXN0b20iO3M6MDoiIjt9czoxMjoiZHluYW1pY19kYXRhIjthOjQ6e3M6NzoiZW5hYmxlZCI7czoxOiIxIjtzOjk6ImRhdGFfcGF0aCI7czoxNDoiY2F0ZWdvcmllc2xpc3QiO3M6OToidmFsdWVfa2V5IjtzOjI6ImlkIjtzOjg6InRleHRfa2V5IjtzOjQ6Im5hbWUiO31zOjY6ImV2ZW50cyI7YToxOntpOjA7YTo1OntzOjg6Im9wZXJhdG9yIjtzOjE6Ij0iO3M6NToic3RhdGUiO3M6MDoiIjtzOjY6ImFjdGlvbiI7czowOiIiO3M6NjoidGFyZ2V0IjtzOjA6IiI7czo3OiJvcHRpb25zIjtzOjA6IiI7fX19aToxNDthOjE4OntzOjQ6Im5hbWUiO3M6NDoiZGF0ZSI7czoyOiJpZCI7czo0OiJkYXRlIjtzOjU6InZhbHVlIjtzOjA6IiI7czo1OiJsYWJlbCI7YToyOntzOjQ6InRleHQiO3M6NDoiRGF0YSI7czo4OiJwb3NpdGlvbiI7czo0OiJsZWZ0Ijt9czo4OiJzdWJsYWJlbCI7czo2MToiSW5zZXJpcmUgbGEgZGF0YSBpbiBjdWkgc2kgc3ZvbGdlcsOgIGwnZXZlbnRvLiBlczogZ2ctbW0tYWFhYSI7czoxMToicGxhY2Vob2xkZXIiO3M6MDoiIjtzOjk6Im1heGxlbmd0aCI7czoyOiIzMCI7czo0OiJzaXplIjtzOjA6IiI7czo1OiJjbGFzcyI7czowOiIiO3M6NToidGl0bGUiO3M6MDoiIjtzOjU6InN0eWxlIjtzOjA6IiI7czoxNToiOmRhdGEtaW5wdXRtYXNrIjtzOjIyOiInYWxpYXMnIDogJ2RkLW1tLXl5eXknIjtzOjY6InBhcmFtcyI7czowOiIiO3M6MTY6IjpkYXRhLWxvYWQtc3RhdGUiO3M6MDoiIjtzOjEzOiI6ZGF0YS10b29sdGlwIjtzOjA6IiI7czo0OiJ0eXBlIjtzOjQ6InRleHQiO3M6MTI6ImNvbnRhaW5lcl9pZCI7czoxOiIwIjtzOjEwOiJ2YWxpZGF0aW9uIjthOjEzOntzOjg6InJlcXVpcmVkIjtzOjE6IjEiO3M6NToiYWxwaGEiO3M6MDoiIjtzOjg6ImFscGhhbnVtIjtzOjA6IiI7czo1OiJkaWdpdCI7czowOiIiO3M6Nzoibm9kaWdpdCI7czowOiIiO3M6Nzoibm9zcGFjZSI7czowOiIiO3M6NjoibnVtYmVyIjtzOjA6IiI7czo1OiJlbWFpbCI7czowOiIiO3M6NToicGhvbmUiO3M6MDoiIjtzOjExOiJwaG9uZV9pbnRlciI7czowOiIiO3M6MzoidXJsIjtzOjA6IiI7czo3OiJjb25maXJtIjtzOjA6IiI7czo2OiJjdXN0b20iO3M6MDoiIjt9fWk6MTU7YToxODp7czo0OiJuYW1lIjtzOjQ6ImhvdXIiO3M6MjoiaWQiO3M6NDoiaG91ciI7czo1OiJ2YWx1ZSI7czowOiIiO3M6NToibGFiZWwiO2E6Mjp7czo0OiJ0ZXh0IjtzOjEwOiJPcmEgaW5pemlvIjtzOjg6InBvc2l0aW9uIjtzOjQ6ImxlZnQiO31zOjg6InN1YmxhYmVsIjtzOjUxOiJJbnNlcmlyZSBsJ29yYXJpbyBkaSBpbml6aW8gZGVsbCdldmVudG8uIGVzOiBoaDptbSAiO3M6MTE6InBsYWNlaG9sZGVyIjtzOjA6IiI7czo5OiJtYXhsZW5ndGgiO3M6MjoiMzAiO3M6NDoic2l6ZSI7czowOiIiO3M6NToiY2xhc3MiO3M6MDoiIjtzOjU6InRpdGxlIjtzOjA6IiI7czo1OiJzdHlsZSI7czowOiIiO3M6MTU6IjpkYXRhLWlucHV0bWFzayI7czoxNzoiJ2FsaWFzJyA6ICdoaDptbSciO3M6NjoicGFyYW1zIjtzOjA6IiI7czoxNjoiOmRhdGEtbG9hZC1zdGF0ZSI7czowOiIiO3M6MTM6IjpkYXRhLXRvb2x0aXAiO3M6MDoiIjtzOjQ6InR5cGUiO3M6NDoidGV4dCI7czoxMjoiY29udGFpbmVyX2lkIjtzOjE6IjAiO3M6MTA6InZhbGlkYXRpb24iO2E6MTM6e3M6ODoicmVxdWlyZWQiO3M6MToiMSI7czo1OiJhbHBoYSI7czowOiIiO3M6ODoiYWxwaGFudW0iO3M6MDoiIjtzOjU6ImRpZ2l0IjtzOjA6IiI7czo3OiJub2RpZ2l0IjtzOjA6IiI7czo3OiJub3NwYWNlIjtzOjA6IiI7czo2OiJudW1iZXIiO3M6MDoiIjtzOjU6ImVtYWlsIjtzOjA6IiI7czo1OiJwaG9uZSI7czowOiIiO3M6MTE6InBob25lX2ludGVyIjtzOjA6IiI7czozOiJ1cmwiO3M6MDoiIjtzOjc6ImNvbmZpcm0iO3M6MDoiIjtzOjY6ImN1c3RvbSI7czowOiIiO319aToxNzthOjIwOntzOjQ6Im5hbWUiO3M6Mzoic2V4IjtzOjI6ImlkIjtzOjM6InNleCI7czo3OiJvcHRpb25zIjtzOjQ1OiIwPVNvbG8gVW9taW5pDQoxPVNvbG8gRG9ubmUNCjM9RG9ubmUgZSBVb21pbmkiO3M6NToiZW1wdHkiO3M6MDoiIjtzOjY6InZhbHVlcyI7czowOiIiO3M6NToibGFiZWwiO2E6Mjp7czo0OiJ0ZXh0IjtzOjU6IlNlc3NvIjtzOjg6InBvc2l0aW9uIjtzOjQ6ImxlZnQiO31zOjg6InN1YmxhYmVsIjtzOjM5OiJHZW5lcmUgcGVyIGN1aSB1biBldmVudG8gw6ggcGlhbmlmaWNhdG8iO3M6ODoibXVsdGlwbGUiO3M6MToiMCI7czo0OiJzaXplIjtzOjA6IiI7czo1OiJjbGFzcyI7czowOiIiO3M6NToidGl0bGUiO3M6MDoiIjtzOjU6InN0eWxlIjtzOjA6IiI7czo2OiJwYXJhbXMiO3M6MDoiIjtzOjE2OiI6ZGF0YS1sb2FkLXN0YXRlIjtzOjA6IiI7czoxMzoiOmRhdGEtdG9vbHRpcCI7czowOiIiO3M6NDoidHlwZSI7czo4OiJkcm9wZG93biI7czoxMjoiY29udGFpbmVyX2lkIjtzOjE6IjAiO3M6MTA6InZhbGlkYXRpb24iO2E6Mzp7czo4OiJyZXF1aXJlZCI7czowOiIiO3M6NzoiY29uZmlybSI7czowOiIiO3M6NjoiY3VzdG9tIjtzOjA6IiI7fXM6MTI6ImR5bmFtaWNfZGF0YSI7YTo0OntzOjc6ImVuYWJsZWQiO3M6MDoiIjtzOjk6ImRhdGFfcGF0aCI7czowOiIiO3M6OToidmFsdWVfa2V5IjtzOjA6IiI7czo4OiJ0ZXh0X2tleSI7czowOiIiO31zOjY6ImV2ZW50cyI7YToxOntpOjA7YTo1OntzOjg6Im9wZXJhdG9yIjtzOjE6Ij0iO3M6NToic3RhdGUiO3M6MDoiIjtzOjY6ImFjdGlvbiI7czowOiIiO3M6NjoidGFyZ2V0IjtzOjA6IiI7czo3OiJvcHRpb25zIjtzOjA6IiI7fX19aTo3O2E6MjA6e3M6NDoibmFtZSI7czo1OiJzY29wZSI7czoyOiJpZCI7czo1OiJzY29wZSI7czo3OiJvcHRpb25zIjtzOjM3OiIwPVB1YmJsaWNvDQoxPVByaXZhdG8NCjI9RnJpZW5kcyBPbmx5IjtzOjU6ImVtcHR5IjtzOjA6IiI7czo2OiJ2YWx1ZXMiO3M6MDoiIjtzOjU6ImxhYmVsIjthOjI6e3M6NDoidGV4dCI7czo3OiJBY2Nlc3NvIjtzOjg6InBvc2l0aW9uIjtzOjQ6ImxlZnQiO31zOjg6InN1YmxhYmVsIjtzOjM0OiJTZWxlemlvbmFyZSBsYSB0aXBvbG9naWEgZGkgZXZlbnRvIjtzOjg6Im11bHRpcGxlIjtzOjE6IjAiO3M6NDoic2l6ZSI7czowOiIiO3M6NToiY2xhc3MiO3M6MDoiIjtzOjU6InRpdGxlIjtzOjA6IiI7czo1OiJzdHlsZSI7czowOiIiO3M6NjoicGFyYW1zIjtzOjA6IiI7czoxNjoiOmRhdGEtbG9hZC1zdGF0ZSI7czowOiIiO3M6MTM6IjpkYXRhLXRvb2x0aXAiO3M6MDoiIjtzOjQ6InR5cGUiO3M6ODoiZHJvcGRvd24iO3M6MTI6ImNvbnRhaW5lcl9pZCI7czoxOiIwIjtzOjEwOiJ2YWxpZGF0aW9uIjthOjM6e3M6ODoicmVxdWlyZWQiO3M6MDoiIjtzOjc6ImNvbmZpcm0iO3M6MDoiIjtzOjY6ImN1c3RvbSI7czowOiIiO31zOjEyOiJkeW5hbWljX2RhdGEiO2E6NDp7czo3OiJlbmFibGVkIjtzOjA6IiI7czo5OiJkYXRhX3BhdGgiO3M6MDoiIjtzOjk6InZhbHVlX2tleSI7czowOiIiO3M6ODoidGV4dF9rZXkiO3M6MDoiIjt9czo2OiJldmVudHMiO2E6MTp7aTowO2E6NTp7czo4OiJvcGVyYXRvciI7czoxOiI9IjtzOjU6InN0YXRlIjtzOjA6IiI7czo2OiJhY3Rpb24iO3M6MDoiIjtzOjY6InRhcmdldCI7czowOiIiO3M6Nzoib3B0aW9ucyI7czowOiIiO319fWk6MzthOjE4OntzOjQ6Im5hbWUiO3M6MTE6ImRlc2NyaXB0aW9uIjtzOjI6ImlkIjtzOjExOiJkZXNjcmlwdGlvbiI7czo1OiJ2YWx1ZSI7czowOiIiO3M6NToibGFiZWwiO2E6Mjp7czo0OiJ0ZXh0IjtzOjExOiJEZXNjcml6aW9uZSI7czo4OiJwb3NpdGlvbiI7czo0OiJsZWZ0Ijt9czo4OiJzdWJsYWJlbCI7czozMjoiRGVzY3JpemlvbmUgaW5zZXJpdGEgZGFsIHBsYW5uZXIiO3M6MTE6InBsYWNlaG9sZGVyIjtzOjIzOiJEZXNjcml6aW9uZSBkZWxsJ2V2ZW50byI7czo0OiJyb3dzIjtzOjE6IjUiO3M6NDoiY29scyI7czoyOiI0MCI7czo1OiJjbGFzcyI7czowOiIiO3M6NToidGl0bGUiO3M6MDoiIjtzOjU6InN0eWxlIjtzOjA6IiI7czoxMzoiOmRhdGEtd3lzaXd5ZyI7czoxOiIwIjtzOjY6InBhcmFtcyI7czowOiIiO3M6MTY6IjpkYXRhLWxvYWQtc3RhdGUiO3M6MDoiIjtzOjEzOiI6ZGF0YS10b29sdGlwIjtzOjA6IiI7czo0OiJ0eXBlIjtzOjg6InRleHRhcmVhIjtzOjEyOiJjb250YWluZXJfaWQiO3M6MToiMCI7czoxMDoidmFsaWRhdGlvbiI7YTo4OntzOjg6InJlcXVpcmVkIjtzOjE6IjEiO3M6NToiYWxwaGEiO3M6MDoiIjtzOjg6ImFscGhhbnVtIjtzOjA6IiI7czo1OiJkaWdpdCI7czowOiIiO3M6Nzoibm9kaWdpdCI7czowOiIiO3M6NjoibnVtYmVyIjtzOjA6IiI7czo3OiJjb25maXJtIjtzOjA6IiI7czo2OiJjdXN0b20iO3M6MDoiIjt9fWk6NDthOjE2OntzOjQ6Im5hbWUiO3M6NToiaW1hZ2UiO3M6MjoiaWQiO3M6NToiaW1hZ2UiO3M6NToibGFiZWwiO2E6Mjp7czo0OiJ0ZXh0IjtzOjk6IkNvcGVydGluYSI7czo4OiJwb3NpdGlvbiI7czo0OiJsZWZ0Ijt9czo4OiJzdWJsYWJlbCI7czoxMzM6IkltbWFnaW5lIGRpIGNvcGVydGluYTxicj5UaXBpIGRpIGZpbGU6IGpwZWcsIGpwZywgZ2lmLCBwbmc8YnI+TGFyZ2hlenphIGUgQWx0ZXp6YSBlc2F0dGU6IDcyMHB4IHggNDI2cHg8YnI+RGltZW5zaW9uaSBtYXNzaW1lOiA1MDAgS0IiO3M6NToiY2xhc3MiO3M6MDoiIjtzOjU6InRpdGxlIjtzOjA6IiI7czo1OiJzdHlsZSI7czowOiIiO3M6OToiOm11bHRpcGxlIjtzOjE6IjAiO3M6NjoicGFyYW1zIjtzOjA6IiI7czoxNjoiOmRhdGEtbG9hZC1zdGF0ZSI7czowOiIiO3M6MTM6IjpkYXRhLXRvb2x0aXAiO3M6MDoiIjtzOjU6Imdob3N0IjtzOjE6IjAiO3M6MTE6Imdob3N0X3ZhbHVlIjtzOjA6IiI7czo0OiJ0eXBlIjtzOjQ6ImZpbGUiO3M6MTI6ImNvbnRhaW5lcl9pZCI7czoxOiIwIjtzOjEwOiJ2YWxpZGF0aW9uIjthOjM6e3M6ODoicmVxdWlyZWQiO3M6MDoiIjtzOjU6ImltYWdlIjtzOjA6IiI7czo2OiJjdXN0b20iO3M6MDoiIjt9fWk6NTthOjE4OntzOjQ6Im5hbWUiO3M6NDoidG93biI7czoyOiJpZCI7czo0OiJ0b3duIjtzOjU6InZhbHVlIjtzOjA6IiI7czo1OiJsYWJlbCI7YToyOntzOjQ6InRleHQiO3M6NjoiQ2l0dMOgIjtzOjg6InBvc2l0aW9uIjtzOjQ6ImxlZnQiO31zOjg6InN1YmxhYmVsIjtzOjU4OiJJbnNlcmlyZSBpbCBub21lIGRlbGxhIGNpdHTDoCBpbiBjdWkgc2kgc3ZvbGdlcsOgIGwnZXZlbnRvIjtzOjExOiJwbGFjZWhvbGRlciI7czo2OiJDaXR0w6AiO3M6OToibWF4bGVuZ3RoIjtzOjI6IjQwIjtzOjQ6InNpemUiO3M6MDoiIjtzOjU6ImNsYXNzIjtzOjA6IiI7czo1OiJ0aXRsZSI7czowOiIiO3M6NToic3R5bGUiO3M6MDoiIjtzOjE1OiI6ZGF0YS1pbnB1dG1hc2siO3M6MDoiIjtzOjY6InBhcmFtcyI7czowOiIiO3M6MTY6IjpkYXRhLWxvYWQtc3RhdGUiO3M6MDoiIjtzOjEzOiI6ZGF0YS10b29sdGlwIjtzOjA6IiI7czo0OiJ0eXBlIjtzOjQ6InRleHQiO3M6MTI6ImNvbnRhaW5lcl9pZCI7czoxOiIwIjtzOjEwOiJ2YWxpZGF0aW9uIjthOjEzOntzOjg6InJlcXVpcmVkIjtzOjE6IjEiO3M6NToiYWxwaGEiO3M6MDoiIjtzOjg6ImFscGhhbnVtIjtzOjA6IiI7czo1OiJkaWdpdCI7czowOiIiO3M6Nzoibm9kaWdpdCI7czowOiIiO3M6Nzoibm9zcGFjZSI7czowOiIiO3M6NjoibnVtYmVyIjtzOjA6IiI7czo1OiJlbWFpbCI7czowOiIiO3M6NToicGhvbmUiO3M6MDoiIjtzOjExOiJwaG9uZV9pbnRlciI7czowOiIiO3M6MzoidXJsIjtzOjA6IiI7czo3OiJjb25maXJtIjtzOjA6IiI7czo2OiJjdXN0b20iO3M6MDoiIjt9fWk6OTthOjE4OntzOjQ6Im5hbWUiO3M6NzoiYWRkcmVzcyI7czoyOiJpZCI7czo3OiJhZGRyZXNzIjtzOjU6InZhbHVlIjtzOjA6IiI7czo1OiJsYWJlbCI7YToyOntzOjQ6InRleHQiO3M6OToiSW5kaXJpenpvIjtzOjg6InBvc2l0aW9uIjtzOjQ6ImxlZnQiO31zOjg6InN1YmxhYmVsIjtzOjYyOiJJbmRpcml6em8gZSBudW1lcm8gY2l2aWNvIGRlbCBsdW9nbyBkb3ZlIHNpIHN2b2xnZXLDoCBsJ2V2ZW50byI7czoxMToicGxhY2Vob2xkZXIiO3M6MDoiIjtzOjk6Im1heGxlbmd0aCI7czowOiIiO3M6NDoic2l6ZSI7czowOiIiO3M6NToiY2xhc3MiO3M6MDoiIjtzOjU6InRpdGxlIjtzOjA6IiI7czo1OiJzdHlsZSI7czowOiIiO3M6MTU6IjpkYXRhLWlucHV0bWFzayI7czowOiIiO3M6NjoicGFyYW1zIjtzOjA6IiI7czoxNjoiOmRhdGEtbG9hZC1zdGF0ZSI7czowOiIiO3M6MTM6IjpkYXRhLXRvb2x0aXAiO3M6MDoiIjtzOjQ6InR5cGUiO3M6NDoidGV4dCI7czoxMjoiY29udGFpbmVyX2lkIjtzOjE6IjAiO3M6MTA6InZhbGlkYXRpb24iO2E6MTM6e3M6ODoicmVxdWlyZWQiO3M6MDoiIjtzOjU6ImFscGhhIjtzOjA6IiI7czo4OiJhbHBoYW51bSI7czowOiIiO3M6NToiZGlnaXQiO3M6MDoiIjtzOjc6Im5vZGlnaXQiO3M6MDoiIjtzOjc6Im5vc3BhY2UiO3M6MDoiIjtzOjY6Im51bWJlciI7czowOiIiO3M6NToiZW1haWwiO3M6MDoiIjtzOjU6InBob25lIjtzOjA6IiI7czoxMToicGhvbmVfaW50ZXIiO3M6MDoiIjtzOjM6InVybCI7czowOiIiO3M6NzoiY29uZmlybSI7czowOiIiO3M6NjoiY3VzdG9tIjtzOjA6IiI7fX1pOjE5O2E6Nzp7czo1OiJsYWJlbCI7czoxMToiR29vZ2xlIE1hcHMiO3M6OToicHVyZV9jb2RlIjtzOjE6IjEiO3M6NDoiY29kZSI7czo0MDE6IjxzY3JpcHQ+DQpnb29nbGUubWFwcy5ldmVudC5hZGREb21MaXN0ZW5lcih3aW5kb3csICdsb2FkJywgaW5pdGlhbGl6ZV9tYXBwYSk7DQo8L3NjcmlwdD4NCjxzdHlsZT4NCiNtYXAtY2FudmFzeyANCiAgaGVpZ2h0OiA0MDBweDsNCiAgd2lkdGg6IDk1MHB4Ow0KICAvKiBtYXJnaW4tYm90dG9tOiAzMHB4OyAqLw0KfQ0KPC9zdHlsZT4NCjxkaXYgaWQ9Im1hcC1jYW52YXMiPjwvZGl2Pg0KPHNwYW4gY2xhc3M9ImhlbHAtYmxvY2siPlBlciBtb2RpZmljYXJlIGxhIHBvc2l6aW9uZSBkZWxsJ2V2ZW50bywgcG9zaXppb25hcnNpIGNvbiBpbCBjdXJzb3JlIGRlbCBtb3VzZSBzdWwgbWFyY2F0b3JlIGUgdHJhc2NpbmFybG8gdGVuZW5kbyBwcmVtdXRvIGlsIHRhc3RvIHNpbmlzdHJvPC9zcGFuPjxicj48YnI+IjtzOjQ6Im5hbWUiO3M6NjoiY3VzdG9tIjtzOjExOiJyZW5kZXJfdHlwZSI7czo2OiJjdXN0b20iO3M6NDoidHlwZSI7czo2OiJjdXN0b20iO3M6MTI6ImNvbnRhaW5lcl9pZCI7czoxOiIwIjt9aToxMDthOjE4OntzOjQ6Im5hbWUiO3M6NjoiYWdlbWluIjtzOjI6ImlkIjtzOjY6ImFnZW1pbiI7czo1OiJ2YWx1ZSI7czowOiIiO3M6NToibGFiZWwiO2E6Mjp7czo0OiJ0ZXh0IjtzOjExOiJFdMOgIG1pbmltYSI7czo4OiJwb3NpdGlvbiI7czo0OiJsZWZ0Ijt9czo4OiJzdWJsYWJlbCI7czo3OToiRXTDoCBtaW5pbWEgcGVyIHBvdGVyIHBhcnRlY2lwYXJlIGFsbCdldmVudG8gKE51bWVybyBpbnRlcm8gcG9zaXRpdm8gcmljaGllc3RvKSI7czoxMToicGxhY2Vob2xkZXIiO3M6MjoiMjAiO3M6OToibWF4bGVuZ3RoIjtzOjA6IiI7czo0OiJzaXplIjtzOjA6IiI7czo1OiJjbGFzcyI7czowOiIiO3M6NToidGl0bGUiO3M6MDoiIjtzOjU6InN0eWxlIjtzOjA6IiI7czoxNToiOmRhdGEtaW5wdXRtYXNrIjtzOjE5OiInYWxpYXMnIDogJ2ludGVnZXInIjtzOjY6InBhcmFtcyI7czowOiIiO3M6MTY6IjpkYXRhLWxvYWQtc3RhdGUiO3M6MDoiIjtzOjEzOiI6ZGF0YS10b29sdGlwIjtzOjA6IiI7czo0OiJ0eXBlIjtzOjQ6InRleHQiO3M6MTI6ImNvbnRhaW5lcl9pZCI7czoxOiIwIjtzOjEwOiJ2YWxpZGF0aW9uIjthOjEzOntzOjg6InJlcXVpcmVkIjtzOjE6IjEiO3M6NToiYWxwaGEiO3M6MDoiIjtzOjg6ImFscGhhbnVtIjtzOjA6IiI7czo1OiJkaWdpdCI7czowOiIiO3M6Nzoibm9kaWdpdCI7czowOiIiO3M6Nzoibm9zcGFjZSI7czowOiIiO3M6NjoibnVtYmVyIjtzOjE6IjEiO3M6NToiZW1haWwiO3M6MDoiIjtzOjU6InBob25lIjtzOjA6IiI7czoxMToicGhvbmVfaW50ZXIiO3M6MDoiIjtzOjM6InVybCI7czowOiIiO3M6NzoiY29uZmlybSI7czowOiIiO3M6NjoiY3VzdG9tIjtzOjA6IiI7fX1pOjExO2E6MTg6e3M6NDoibmFtZSI7czo2OiJhZ2VtYXgiO3M6MjoiaWQiO3M6NjoiYWdlbWF4IjtzOjU6InZhbHVlIjtzOjA6IiI7czo1OiJsYWJlbCI7YToyOntzOjQ6InRleHQiO3M6MTI6IkV0w6AgbWFzc2ltYSI7czo4OiJwb3NpdGlvbiI7czo0OiJsZWZ0Ijt9czo4OiJzdWJsYWJlbCI7czo4MDoiRXTDoCBtYXNzaW1hIHBlciBwb3RlciBwYXJ0ZWNpcGFyZSBhbGwnZXZlbnRvIChOdW1lcm8gaW50ZXJvIHBvc2l0aXZvIHJpY2hpZXN0bykiO3M6MTE6InBsYWNlaG9sZGVyIjtzOjI6IjQwIjtzOjk6Im1heGxlbmd0aCI7czowOiIiO3M6NDoic2l6ZSI7czowOiIiO3M6NToiY2xhc3MiO3M6MDoiIjtzOjU6InRpdGxlIjtzOjA6IiI7czo1OiJzdHlsZSI7czowOiIiO3M6MTU6IjpkYXRhLWlucHV0bWFzayI7czoxOToiJ2FsaWFzJyA6ICdpbnRlZ2VyJyI7czo2OiJwYXJhbXMiO3M6MDoiIjtzOjE2OiI6ZGF0YS1sb2FkLXN0YXRlIjtzOjA6IiI7czoxMzoiOmRhdGEtdG9vbHRpcCI7czowOiIiO3M6NDoidHlwZSI7czo0OiJ0ZXh0IjtzOjEyOiJjb250YWluZXJfaWQiO3M6MToiMCI7czoxMDoidmFsaWRhdGlvbiI7YToxMzp7czo4OiJyZXF1aXJlZCI7czoxOiIxIjtzOjU6ImFscGhhIjtzOjA6IiI7czo4OiJhbHBoYW51bSI7czowOiIiO3M6NToiZGlnaXQiO3M6MDoiIjtzOjc6Im5vZGlnaXQiO3M6MDoiIjtzOjc6Im5vc3BhY2UiO3M6MDoiIjtzOjY6Im51bWJlciI7czoxOiIxIjtzOjU6ImVtYWlsIjtzOjA6IiI7czo1OiJwaG9uZSI7czowOiIiO3M6MTE6InBob25lX2ludGVyIjtzOjA6IiI7czozOiJ1cmwiO3M6MDoiIjtzOjc6ImNvbmZpcm0iO3M6MDoiIjtzOjY6ImN1c3RvbSI7czowOiIiO319aToxMjthOjE4OntzOjQ6Im5hbWUiO3M6MTA6InNpdGN1cnJlbnQiO3M6MjoiaWQiO3M6MTA6InNpdGN1cnJlbnQiO3M6NToidmFsdWUiO3M6MDoiIjtzOjU6ImxhYmVsIjthOjI6e3M6NDoidGV4dCI7czoxMjoiUGFydGVjaXBhbnRpIjtzOjg6InBvc2l0aW9uIjtzOjQ6ImxlZnQiO31zOjg6InN1YmxhYmVsIjtzOjU5OiJOdW1lcm8gZGkgcGFydGVjaXBhbnRpIGNoZSBoYW5ubyBlZmZldHR1YXRvIGxhIHByZW5vdGF6aW9uZSI7czoxMToicGxhY2Vob2xkZXIiO3M6MjoiMTgiO3M6OToibWF4bGVuZ3RoIjtzOjA6IiI7czo0OiJzaXplIjtzOjA6IiI7czo1OiJjbGFzcyI7czowOiIiO3M6NToidGl0bGUiO3M6MDoiIjtzOjU6InN0eWxlIjtzOjA6IiI7czoxNToiOmRhdGEtaW5wdXRtYXNrIjtzOjE5OiInYWxpYXMnIDogJ2ludGVnZXInIjtzOjY6InBhcmFtcyI7czowOiIiO3M6MTY6IjpkYXRhLWxvYWQtc3RhdGUiO3M6ODoiZGlzYWJsZWQiO3M6MTM6IjpkYXRhLXRvb2x0aXAiO3M6MDoiIjtzOjQ6InR5cGUiO3M6NDoidGV4dCI7czoxMjoiY29udGFpbmVyX2lkIjtzOjE6IjAiO3M6MTA6InZhbGlkYXRpb24iO2E6MTM6e3M6ODoicmVxdWlyZWQiO3M6MToiMSI7czo1OiJhbHBoYSI7czowOiIiO3M6ODoiYWxwaGFudW0iO3M6MDoiIjtzOjU6ImRpZ2l0IjtzOjA6IiI7czo3OiJub2RpZ2l0IjtzOjA6IiI7czo3OiJub3NwYWNlIjtzOjA6IiI7czo2OiJudW1iZXIiO3M6MToiMSI7czo1OiJlbWFpbCI7czowOiIiO3M6NToicGhvbmUiO3M6MDoiIjtzOjExOiJwaG9uZV9pbnRlciI7czowOiIiO3M6MzoidXJsIjtzOjA6IiI7czo3OiJjb25maXJtIjtzOjA6IiI7czo2OiJjdXN0b20iO3M6MDoiIjt9fWk6MTM7YToxODp7czo0OiJuYW1lIjtzOjg6InNpdHRvdGFsIjtzOjI6ImlkIjtzOjg6InNpdHRvdGFsIjtzOjU6InZhbHVlIjtzOjA6IiI7czo1OiJsYWJlbCI7YToyOntzOjQ6InRleHQiO3M6MTc6IlBvc3RpIGRpc3BvbmliaWxpIjtzOjg6InBvc2l0aW9uIjtzOjQ6ImxlZnQiO31zOjg6InN1YmxhYmVsIjtzOjI3OiJQb3N0aSBkaXNwb25pYmlsaSBpbiB0b3RhbGUiO3M6MTE6InBsYWNlaG9sZGVyIjtzOjI6IjUwIjtzOjk6Im1heGxlbmd0aCI7czowOiIiO3M6NDoic2l6ZSI7czowOiIiO3M6NToiY2xhc3MiO3M6MDoiIjtzOjU6InRpdGxlIjtzOjA6IiI7czo1OiJzdHlsZSI7czowOiIiO3M6MTU6IjpkYXRhLWlucHV0bWFzayI7czoxOToiJ2FsaWFzJyA6ICdpbnRlZ2VyJyI7czo2OiJwYXJhbXMiO3M6MDoiIjtzOjE2OiI6ZGF0YS1sb2FkLXN0YXRlIjtzOjA6IiI7czoxMzoiOmRhdGEtdG9vbHRpcCI7czowOiIiO3M6NDoidHlwZSI7czo0OiJ0ZXh0IjtzOjEyOiJjb250YWluZXJfaWQiO3M6MToiMCI7czoxMDoidmFsaWRhdGlvbiI7YToxMzp7czo4OiJyZXF1aXJlZCI7czoxOiIxIjtzOjU6ImFscGhhIjtzOjA6IiI7czo4OiJhbHBoYW51bSI7czowOiIiO3M6NToiZGlnaXQiO3M6MDoiIjtzOjc6Im5vZGlnaXQiO3M6MDoiIjtzOjc6Im5vc3BhY2UiO3M6MDoiIjtzOjY6Im51bWJlciI7czoxOiIxIjtzOjU6ImVtYWlsIjtzOjA6IiI7czo1OiJwaG9uZSI7czowOiIiO3M6MTE6InBob25lX2ludGVyIjtzOjA6IiI7czozOiJ1cmwiO3M6MDoiIjtzOjc6ImNvbmZpcm0iO3M6MDoiIjtzOjY6ImN1c3RvbSI7czowOiIiO319aToxNjthOjEwOntzOjExOiJyZW5kZXJfdHlwZSI7czo2OiJzdWJtaXQiO3M6NDoibmFtZSI7czo4OiJidXR0b24xNiI7czoyOiJpZCI7czo4OiJidXR0b24xNiI7czo0OiJ0eXBlIjtzOjY6InN1Ym1pdCI7czo1OiJ2YWx1ZSI7czo1OiJTYWx2YSI7czo1OiJjbGFzcyI7czoxNToiYnRuIGJ0bi1kZWZhdWx0IjtzOjU6InN0eWxlIjtzOjA6IiI7czo2OiJwYXJhbXMiO3M6MDoiIjtzOjE2OiI6ZGF0YS1sb2FkLXN0YXRlIjtzOjA6IiI7czoxMjoiY29udGFpbmVyX2lkIjtzOjE6IjAiO31pOjE4O2E6Nzp7czo0OiJuYW1lIjtzOjE1OiJzaXRjdXJyZW50Z2hvc3QiO3M6MjoiaWQiO3M6MTU6InNpdGN1cnJlbnRnaG9zdCI7czo1OiJ2YWx1ZSI7czowOiIiO3M6NToibGFiZWwiO3M6MTI6IkhpZGRlbiBMYWJlbCI7czo2OiJwYXJhbXMiO3M6MDoiIjtzOjQ6InR5cGUiO3M6NjoiaGlkZGVuIjtzOjEyOiJjb250YWluZXJfaWQiO3M6MToiMCI7fWk6MjA7YTo3OntzOjQ6Im5hbWUiO3M6ODoibGF0aXR1ZGUiO3M6MjoiaWQiO3M6ODoibGF0aXR1ZGUiO3M6NToidmFsdWUiO3M6MDoiIjtzOjU6ImxhYmVsIjtzOjEyOiJIaWRkZW4gTGFiZWwiO3M6NjoicGFyYW1zIjtzOjA6IiI7czo0OiJ0eXBlIjtzOjY6ImhpZGRlbiI7czoxMjoiY29udGFpbmVyX2lkIjtzOjE6IjAiO31pOjIxO2E6Nzp7czo0OiJuYW1lIjtzOjk6ImxvbmdpdHVkZSI7czoyOiJpZCI7czo5OiJsb25naXR1ZGUiO3M6NToidmFsdWUiO3M6MDoiIjtzOjU6ImxhYmVsIjtzOjEyOiJIaWRkZW4gTGFiZWwiO3M6NjoicGFyYW1zIjtzOjA6IiI7czo0OiJ0eXBlIjtzOjY6ImhpZGRlbiI7czoxMjoiY29udGFpbmVyX2lkIjtzOjE6IjAiO319czozOiJETkEiO2E6Mjp7czo0OiJsb2FkIjthOjY6e3M6OToiZGJfcmVhZF8xIjthOjI6e3M6NToiZm91bmQiO3M6MDoiIjtzOjk6Im5vdF9mb3VuZCI7czowOiIiO31zOjk6ImRiX3JlYWRfNyI7YToyOntzOjU6ImZvdW5kIjthOjE6e3M6MTM6ImN1c3RvbV9jb2RlXzgiO3M6MDoiIjt9czo5OiJub3RfZm91bmQiO3M6MDoiIjt9czoxMzoiY3VzdG9tX2NvZGVfMiI7czowOiIiO3M6MTM6ImN1c3RvbV9jb2RlXzYiO3M6MDoiIjtzOjY6Imh0bWxfMCI7czowOiIiO3M6MTM6ImN1c3RvbV9jb2RlXzUiO3M6MDoiIjt9czo2OiJzdWJtaXQiO2E6MTp7czoxMzoiY3VzdG9tX2NvZGVfMyI7czowOiIiO319czoxNDoiYWN0aW9uc19jb25maWciO2E6ODp7aToxO2E6MTk6e3M6MTI6ImFjdGlvbl9sYWJlbCI7czoyNzoiQ2FyaWNhbWVudG8gZGVsbGUgY2F0ZWdvcmllIjtzOjc6ImVuYWJsZWQiO3M6MToiMSI7czo5OiJ0YWJsZW5hbWUiO3M6MTA6ImNhdGVnb3JpZXMiO3M6MTA6Im11bHRpX3JlYWQiO3M6MToiMSI7czoxODoibG9hZF91bmRlcl9tb2RlbGlkIjtzOjE6IjEiO3M6ODoibW9kZWxfaWQiO3M6MTk6InN1cGVyY2F0ZWdvcmllc2xpc3QiO3M6NjoiZmllbGRzIjtzOjE2OiJpZCxuYW1lLGxhbmd1YWdlIjtzOjU6Im9yZGVyIjtzOjI6ImlkIjtzOjU6Imdyb3VwIjtzOjA6IiI7czoxMDoiY29uZGl0aW9ucyI7czo0OToiPD9waHAgcmV0dXJuIGFycmF5KCdpZHN1cGVyY2F0ZWdvcnknID0+IE5VTEwpOyA/PiI7czoxNjoiZW5hYmxlX3JlbGF0aW9ucyI7czoxOiIwIjtzOjk6InJlbGF0aW9ucyI7YToxOntpOjA7YTo0OntzOjU6Im1vZGVsIjtzOjA6IiI7czo5OiJ0YWJsZW5hbWUiO3M6MDoiIjtzOjQ6InR5cGUiO3M6NjoiaGFzT25lIjtzOjQ6ImZrZXkiO3M6MDoiIjt9fXM6MTA6Im5kYl9lbmFibGUiO3M6MToiMCI7czoxMDoibmRiX2RyaXZlciI7czo1OiJteXNxbCI7czo4OiJuZGJfaG9zdCI7czo5OiJsb2NhbGhvc3QiO3M6MTI6Im5kYl9kYXRhYmFzZSI7czowOiIiO3M6ODoibmRiX3VzZXIiO3M6MDoiIjtzOjEyOiJuZGJfcGFzc3dvcmQiO3M6MDoiIjtzOjEwOiJuZGJfcHJlZml4IjtzOjQ6Impvc18iO31pOjg7YToyOntzOjEyOiJhY3Rpb25fbGFiZWwiO3M6NDQ6IkZvcm1hdHRhemlvbmUgZGVpIGRhdGkgcGVyIGxlIHNvdHRvY2F0ZWdvcmllIjtzOjc6ImNvbnRlbnQiO3M6ODg6Ijw/cGhwDQokZW0gPSBuZXcgRXZlbnRzTW9kZWwoKTsNCiRlbS0+Y2F0ZWdvcnlsaXN0KCRmb3JtLT5kYXRhWydjYXRlZ29yaWVzbGlzdCddKTsNCj8+DQoiO31pOjc7YToxOTp7czoxMjoiYWN0aW9uX2xhYmVsIjtzOjMyOiJDYXJpY2FtZW50byBkZWxsZSBzb3R0b2NhdGVnb3JpZSI7czo3OiJlbmFibGVkIjtzOjE6IjEiO3M6OToidGFibGVuYW1lIjtzOjEwOiJjYXRlZ29yaWVzIjtzOjEwOiJtdWx0aV9yZWFkIjtzOjE6IjEiO3M6MTg6ImxvYWRfdW5kZXJfbW9kZWxpZCI7czoxOiIxIjtzOjg6Im1vZGVsX2lkIjtzOjE0OiJjYXRlZ29yaWVzbGlzdCI7czo2OiJmaWVsZHMiO3M6MzI6ImlkLG5hbWUsbGFuZ3VhZ2UsaWRzdXBlcmNhdGVnb3J5IjtzOjU6Im9yZGVyIjtzOjI6ImlkIjtzOjU6Imdyb3VwIjtzOjA6IiI7czoxMDoiY29uZGl0aW9ucyI7czowOiIiO3M6MTY6ImVuYWJsZV9yZWxhdGlvbnMiO3M6MToiMCI7czo5OiJyZWxhdGlvbnMiO2E6MTp7aTowO2E6NDp7czo1OiJtb2RlbCI7czowOiIiO3M6OToidGFibGVuYW1lIjtzOjA6IiI7czo0OiJ0eXBlIjtzOjY6Imhhc09uZSI7czo0OiJma2V5IjtzOjA6IiI7fX1zOjEwOiJuZGJfZW5hYmxlIjtzOjE6IjAiO3M6MTA6Im5kYl9kcml2ZXIiO3M6NToibXlzcWwiO3M6ODoibmRiX2hvc3QiO3M6OToibG9jYWxob3N0IjtzOjEyOiJuZGJfZGF0YWJhc2UiO3M6MDoiIjtzOjg6Im5kYl91c2VyIjtzOjA6IiI7czoxMjoibmRiX3Bhc3N3b3JkIjtzOjA6IiI7czoxMDoibmRiX3ByZWZpeCI7czo0OiJqb3NfIjt9aToyO2E6Mjp7czoxMjoiYWN0aW9uX2xhYmVsIjtzOjIwOiJDYXJpY2FtZW50byBkZWkgZGF0aSI7czo3OiJjb250ZW50IjtzOjEwNDoiPD9waHANCiRlYyA9VHJhdmVsOjpnZXRJbnN0YW5jZSgpLT5nZXQoJ2V2ZW50Q29udHJvbGxlcicpOw0KJGVjLT5pbml0R3BzKCk7DQokZWMtPmluaXRGb3JtKCRmb3JtKTsNCj8+DQoiO31pOjY7YToyOntzOjEyOiJhY3Rpb25fbGFiZWwiO3M6MTc6IlF1ZXJpZXMgZGVidWdnZXIgIjtzOjc6ImNvbnRlbnQiO3M6Nzk6Ijw/cGhwIA0KLy8gcHIoXEdDb3JlXE1vZGVsc1xzdXBlcmNhdGVnb3JpZXNsaXN0OjpnZXRJbnN0YW5jZSgpLT5kYm8tPmxvZyk7IA0KPz4iO31pOjA7YToxMTp7czo0OiJwYWdlIjtzOjE6IjEiO3M6MTI6InN1Ym1pdF9ldmVudCI7czo2OiJzdWJtaXQiO3M6MTE6ImZvcm1fbWV0aG9kIjtzOjQ6ImZpbGUiO3M6MTA6ImFjdGlvbl91cmwiO3M6MDoiIjtzOjEwOiJmb3JtX2NsYXNzIjtzOjEwOiJjaHJvbm9mb3JtIjtzOjE1OiJmb3JtX3RhZ19hdHRhY2giO3M6MDoiIjtzOjI0OiJyZXF1aXJlZF9sYWJlbHNfaWRlbnRpZnkiO3M6MToiMSI7czoxMjoicmVsYXRpdmVfdXJsIjtzOjE6IjEiO3M6MTE6ImFqYXhfc3VibWl0IjtzOjE6IjAiO3M6MTM6ImFkZF9mb3JtX3RhZ3MiO3M6MToiMSI7czo5OiJ4aHRtbF91cmwiO3M6MToiMCI7fWk6NTthOjI6e3M6MTI6ImFjdGlvbl9sYWJlbCI7czo1MDoiRm9ybWF0dGF6aW9uZSBzZWxldHRvcmkgY2F0ZWdvcmlhIGUgc290dG9jYXRlZ29yaWEiO3M6NzoiY29udGVudCI7czoyMjI6Ijw/cGhwIA0KJGEgPSAndmVuZG9yL2pzL2V2ZW50ZGV0YWlsZm9ybS5qcyc7DQplY2hvICc8c2NyaXB0IHNyYz0iJy5KdXJpOjpiYXNlKCkgLiAkYS4nIj48L3NjcmlwdD4nOw0KPz4NCg0KPHNjcmlwdD4NCmluaXRfc3ViX2NhdGVnb3JpZXMoIDw/cGhwIGVjaG8gVHJhdmVsOjpnZXRJbnN0YW5jZSgpLT5nZXQoJ2V2ZW50RGV0YWlsJyktPmlkY2F0ZWdvcnk7ID8+ICk7DQo8L3NjcmlwdD4NCiI7fWk6MzthOjI6e3M6MTI6ImFjdGlvbl9sYWJlbCI7czoyMjoiU2FsdmF0YWdnaW8gZSByZWRpcmVjdCI7czo3OiJjb250ZW50IjtzOjI3MjoiPD9waHANCg0KSkxvYWRlcjo6cmVnaXN0ZXIoJ0V2ZW50c01vZGVsJywgJ3ZlbmRvci9tb2RlbC9FdmVudHNNb2RlbC5waHAnKTsNCiRtb2RlbCA9IG5ldyBFdmVudHNNb2RlbCgpOw0KJG1vZGVsLT5zYXZlKCk7DQoNCkpGYWN0b3J5OjpnZXRBcHBsaWNhdGlvbigpLT5yZWRpcmVjdChKVXJpOjpiYXNlKCkuJz9zaG93PWVkaXQmaXRlbT0nLkpSZXF1ZXN0OjpnZXRWYXIoJ2l0ZW0nKSwgJ1NhbHZhdGFnZ2lvIG1vZGlmaWNoZSBlZmZldHR1YXRlJywgJ3N1Y2Nlc3MnKTsNCg0KPz4iO319czoxNDoiZGJfZmllbGRzX2xpc3QiO2E6MTp7czoxMDoiY2F0ZWdvcmllcyI7czowOiIiO319', 1, 'Eventi', 1, '<div class="form-group gcore-form-row" id="form-row-status"><label for="status" class="control-label gcore-label-left">Stato</label>\n<div class="gcore-input gcore-display-table" id="fin-status"><select name="status" id="status" size="" class="form-control A" title="" style="" data-load-state="" data-tooltip="">\n<option value="0">Disabilitato</option>\n<option value="1">Abilitato</option>\n</select><span class="help-block">Sarà possibile accedere all''evento lato client solo se è selezionata l''opzione "Abilitato"</span></div></div><div class="form-group gcore-form-row" id="form-row-title"><label for="title" class="control-label gcore-label-left">Nome dell''evento</label>\n<div class="gcore-input gcore-display-table" id="fin-title"><input name="title" id="title" value="" placeholder="Titolo dell&#039;evento" maxlength="30" size="" class="validate[&#039;required&#039;] form-control A" title="" style="" data-inputmask="" data-load-state="" data-tooltip="" type="text" /><span class="help-block">Inserire il titolo dell''evento</span></div></div><?php\n$keys = \\GCore\\Libs\\Arr::getVal(\\GCore\\Libs\\Arr::getVal($form->data, explode(".", "supercategorieslist")), explode(".", "[n].id"));\n$values = \\GCore\\Libs\\Arr::getVal(\\GCore\\Libs\\Arr::getVal($form->data, explode(".", "supercategorieslist")), explode(".", "[n].name"));\n$options = array_combine($keys, $values);\n$field = array (\n  ''name'' => ''supercategoria'',\n  ''id'' => ''supercategoria'',\n  ''options'' => \n  array (\n  ),\n  ''empty'' => '''',\n  ''values'' => \n  array (\n  ),\n  ''label'' => \n  array (\n    ''text'' => ''Super Categoria'',\n    ''position'' => ''left'',\n  ),\n  ''sublabel'' => ''Selezionare una famiglia di Categorie a cui l\\''evento deve appartenere'',\n  ''multiple'' => ''0'',\n  ''size'' => '''',\n  ''class'' => '''',\n  ''title'' => '''',\n  ''style'' => '''',\n  ''params'' => '''',\n  '':data-load-state'' => '''',\n  '':data-tooltip'' => '''',\n  ''type'' => ''dropdown'',\n  ''container_id'' => ''0'',\n);\n$field["options"] = $options;\necho \\GCore\\Helpers\\Html::formLine($field["name"], $field);\n?><?php\n$keys = \\GCore\\Libs\\Arr::getVal(\\GCore\\Libs\\Arr::getVal($form->data, explode(".", "categorieslist")), explode(".", "[n].id"));\n$values = \\GCore\\Libs\\Arr::getVal(\\GCore\\Libs\\Arr::getVal($form->data, explode(".", "categorieslist")), explode(".", "[n].name"));\n$options = array_combine($keys, $values);\n$field = array (\n  ''name'' => ''categoria'',\n  ''id'' => ''categoria'',\n  ''options'' => \n  array (\n  ),\n  ''empty'' => '''',\n  ''values'' => \n  array (\n  ),\n  ''label'' => \n  array (\n    ''text'' => ''Sotto Categoria'',\n    ''position'' => ''left'',\n  ),\n  ''sublabel'' => ''Selezionare una delle sottocategorie a cui l\\''evento deve appartenere'',\n  ''multiple'' => ''0'',\n  ''size'' => '''',\n  ''class'' => '''',\n  ''title'' => '''',\n  ''style'' => '''',\n  ''params'' => '''',\n  '':data-load-state'' => '''',\n  '':data-tooltip'' => '''',\n  ''type'' => ''dropdown'',\n  ''container_id'' => ''0'',\n);\n$field["options"] = $options;\necho \\GCore\\Helpers\\Html::formLine($field["name"], $field);\n?><div class="form-group gcore-form-row" id="form-row-date"><label for="date" class="control-label gcore-label-left">Data</label>\n<div class="gcore-input gcore-display-table" id="fin-date"><input name="date" id="date" value="" placeholder="" maxlength="30" size="" class="validate[&#039;required&#039;] form-control A" title="" style="" data-inputmask="&#039;alias&#039; : &#039;dd-mm-yyyy&#039;" data-load-state="" data-tooltip="" type="text" /><span class="help-block">Inserire la data in cui si svolgerà l''evento. es: gg-mm-aaaa</span></div></div><div class="form-group gcore-form-row" id="form-row-hour"><label for="hour" class="control-label gcore-label-left">Ora inizio</label>\n<div class="gcore-input gcore-display-table" id="fin-hour"><input name="hour" id="hour" value="" placeholder="" maxlength="30" size="" class="validate[&#039;required&#039;] form-control A" title="" style="" data-inputmask="&#039;alias&#039; : &#039;hh:mm&#039;" data-load-state="" data-tooltip="" type="text" /><span class="help-block">Inserire l''orario di inizio dell''evento. es: hh:mm </span></div></div><div class="form-group gcore-form-row" id="form-row-sex"><label for="sex" class="control-label gcore-label-left">Sesso</label>\n<div class="gcore-input gcore-display-table" id="fin-sex"><select name="sex" id="sex" size="" class="form-control A" title="" style="" data-load-state="" data-tooltip="">\n<option value="0">Solo Uomini</option>\n<option value="1">Solo Donne</option>\n<option value="3">Donne e Uomini</option>\n</select><span class="help-block">Genere per cui un evento è pianificato</span></div></div><div class="form-group gcore-form-row" id="form-row-scope"><label for="scope" class="control-label gcore-label-left">Accesso</label>\n<div class="gcore-input gcore-display-table" id="fin-scope"><select name="scope" id="scope" size="" class="form-control A" title="" style="" data-load-state="" data-tooltip="">\n<option value="0">Pubblico</option>\n<option value="1">Privato</option>\n<option value="2">Friends Only</option>\n</select><span class="help-block">Selezionare la tipologia di evento</span></div></div><div class="form-group gcore-form-row" id="form-row-description"><label for="description" class="control-label gcore-label-left">Descrizione</label>\n<div class="gcore-input gcore-display-table" id="fin-description"><textarea name="description" id="description" placeholder="Descrizione dell&#039;evento" rows="5" cols="40" class="validate[&#039;required&#039;] form-control A" title="" style="" data-wysiwyg="0" data-load-state="" data-tooltip=""></textarea><span class="help-block">Descrizione inserita dal planner</span></div></div><div class="form-group gcore-form-row" id="form-row-image"><label for="image" class="control-label gcore-label-left">Copertina</label>\n<div class="gcore-input gcore-display-table" id="fin-image"><input name="image" id="image" class="form-control gcore-height-auto A" title="" style="" multiple="0" data-load-state="" data-tooltip="" type="file" /><span class="help-block">Immagine di copertina<br>Tipi di file: jpeg, jpg, gif, png<br>Larghezza e Altezza esatte: 720px x 426px<br>Dimensioni massime: 500 KB</span></div></div><div class="form-group gcore-form-row" id="form-row-town"><label for="town" class="control-label gcore-label-left">Città</label>\n<div class="gcore-input gcore-display-table" id="fin-town"><input name="town" id="town" value="" placeholder="Città" maxlength="40" size="" class="validate[&#039;required&#039;] form-control A" title="" style="" data-inputmask="" data-load-state="" data-tooltip="" type="text" /><span class="help-block">Inserire il nome della città in cui si svolgerà l''evento</span></div></div><div class="form-group gcore-form-row" id="form-row-address"><label for="address" class="control-label gcore-label-left">Indirizzo</label>\n<div class="gcore-input gcore-display-table" id="fin-address"><input name="address" id="address" value="" placeholder="" maxlength="" size="" class="form-control A" title="" style="" data-inputmask="" data-load-state="" data-tooltip="" type="text" /><span class="help-block">Indirizzo e numero civico del luogo dove si svolgerà l''evento</span></div></div><script>\r\ngoogle.maps.event.addDomListener(window, ''load'', initialize_mappa);\r\n</script>\r\n<style>\r\n#map-canvas{ \r\n  height: 400px;\r\n  width: 950px;\r\n  /* margin-bottom: 30px; */\r\n}\r\n</style>\r\n<div id="map-canvas"></div>\r\n<span class="help-block">Per modificare la posizione dell''evento, posizionarsi con il cursore del mouse sul marcatore e trascinarlo tenendo premuto il tasto sinistro</span><br><br><div class="form-group gcore-form-row" id="form-row-agemin"><label for="agemin" class="control-label gcore-label-left">Età minima</label>\n<div class="gcore-input gcore-display-table" id="fin-agemin"><input name="agemin" id="agemin" value="" placeholder="20" maxlength="" size="" class="validate[&#039;required&#039;,&#039;number&#039;] form-control A" title="" style="" data-inputmask="&#039;alias&#039; : &#039;integer&#039;" data-load-state="" data-tooltip="" type="text" /><span class="help-block">Età minima per poter partecipare all''evento (Numero intero positivo richiesto)</span></div></div><div class="form-group gcore-form-row" id="form-row-agemax"><label for="agemax" class="control-label gcore-label-left">Età massima</label>\n<div class="gcore-input gcore-display-table" id="fin-agemax"><input name="agemax" id="agemax" value="" placeholder="40" maxlength="" size="" class="validate[&#039;required&#039;,&#039;number&#039;] form-control A" title="" style="" data-inputmask="&#039;alias&#039; : &#039;integer&#039;" data-load-state="" data-tooltip="" type="text" /><span class="help-block">Età massima per poter partecipare all''evento (Numero intero positivo richiesto)</span></div></div><div class="form-group gcore-form-row" id="form-row-sitcurrent"><label for="sitcurrent" class="control-label gcore-label-left">Partecipanti</label>\n<div class="gcore-input gcore-display-table" id="fin-sitcurrent"><input name="sitcurrent" id="sitcurrent" value="" placeholder="18" maxlength="" size="" class="validate[&#039;required&#039;,&#039;number&#039;] form-control A" title="" style="" data-inputmask="&#039;alias&#039; : &#039;integer&#039;" data-load-state="disabled" data-tooltip="" type="text" /><span class="help-block">Numero di partecipanti che hanno effettuato la prenotazione</span></div></div><div class="form-group gcore-form-row" id="form-row-sittotal"><label for="sittotal" class="control-label gcore-label-left">Posti disponibili</label>\n<div class="gcore-input gcore-display-table" id="fin-sittotal"><input name="sittotal" id="sittotal" value="" placeholder="50" maxlength="" size="" class="validate[&#039;required&#039;,&#039;number&#039;] form-control A" title="" style="" data-inputmask="&#039;alias&#039; : &#039;integer&#039;" data-load-state="" data-tooltip="" type="text" /><span class="help-block">Posti disponibili in totale</span></div></div><div class="form-group gcore-form-row" id="form-row-button16"><div class="gcore-input gcore-display-table" id="fin-button16"><input name="button16" id="button16" type="submit" value="Salva" class="btn btn-default form-control A" style="" data-load-state="" /></div></div><input name="sitcurrentghost" id="sitcurrentghost" value="" type="hidden" class="form-control A" /><input name="latitude" id="latitude" value="" type="hidden" class="form-control A" /><input name="longitude" id="longitude" value="" type="hidden" class="form-control A" />');
INSERT INTO `zcp8_chronoengine_chronoforms` (`id`, `title`, `params`, `extras`, `published`, `app`, `form_type`, `content`) VALUES
(6, 'event_search', '{"description":"Impostazione dei filtri da applicare alla lista degli eventi","setup":"0","theme":"bootstrap3","tight_layout":"0","rtl_support":"0","labels_right_aligned":"0","labels_auto_width":"0","js_validation_language":""}', 'YTo0OntzOjY6ImZpZWxkcyI7YToxOTp7aTo4O2E6MjA6e3M6NDoibmFtZSI7czo2OiJzdGF0dXMiO3M6MjoiaWQiO3M6Njoic3RhdHVzIjtzOjc6Im9wdGlvbnMiO3M6Mjc6IjA9RGlzYWJpbGl0YXRvDQoxPUFiaWxpdGF0byI7czo1OiJlbXB0eSI7czoxMzoiTm9uIEltcG9zdGF0byI7czo2OiJ2YWx1ZXMiO3M6MDoiIjtzOjU6ImxhYmVsIjthOjI6e3M6NDoidGV4dCI7czo1OiJTdGF0byI7czo4OiJwb3NpdGlvbiI7czo0OiJsZWZ0Ijt9czo4OiJzdWJsYWJlbCI7czo5MjoiU2Fyw6AgcG9zc2liaWxlIGFjY2VkZXJlIGFsbCdldmVudG8gbGF0byBjbGllbnQgc29sbyBzZSDDqCBzZWxlemlvbmF0YSBsJ29wemlvbmUgIkFiaWxpdGF0byIiO3M6ODoibXVsdGlwbGUiO3M6MToiMCI7czo0OiJzaXplIjtzOjA6IiI7czo1OiJjbGFzcyI7czowOiIiO3M6NToidGl0bGUiO3M6MDoiIjtzOjU6InN0eWxlIjtzOjA6IiI7czo2OiJwYXJhbXMiO3M6MDoiIjtzOjE2OiI6ZGF0YS1sb2FkLXN0YXRlIjtzOjA6IiI7czoxMzoiOmRhdGEtdG9vbHRpcCI7czowOiIiO3M6NDoidHlwZSI7czo4OiJkcm9wZG93biI7czoxMjoiY29udGFpbmVyX2lkIjtzOjE6IjAiO3M6MTA6InZhbGlkYXRpb24iO2E6Mzp7czo4OiJyZXF1aXJlZCI7czowOiIiO3M6NzoiY29uZmlybSI7czowOiIiO3M6NjoiY3VzdG9tIjtzOjA6IiI7fXM6MTI6ImR5bmFtaWNfZGF0YSI7YTo0OntzOjc6ImVuYWJsZWQiO3M6MDoiIjtzOjk6ImRhdGFfcGF0aCI7czowOiIiO3M6OToidmFsdWVfa2V5IjtzOjA6IiI7czo4OiJ0ZXh0X2tleSI7czowOiIiO31zOjY6ImV2ZW50cyI7YToxOntpOjA7YTo1OntzOjg6Im9wZXJhdG9yIjtzOjE6Ij0iO3M6NToic3RhdGUiO3M6MDoiIjtzOjY6ImFjdGlvbiI7czowOiIiO3M6NjoidGFyZ2V0IjtzOjA6IiI7czo3OiJvcHRpb25zIjtzOjA6IiI7fX19aToxO2E6MTg6e3M6NDoibmFtZSI7czo1OiJ0aXRsZSI7czoyOiJpZCI7czo1OiJ0aXRsZSI7czo1OiJ2YWx1ZSI7czowOiIiO3M6NToibGFiZWwiO2E6Mjp7czo0OiJ0ZXh0IjtzOjE2OiJOb21lIGRlbGwnZXZlbnRvIjtzOjg6InBvc2l0aW9uIjtzOjQ6ImxlZnQiO31zOjg6InN1YmxhYmVsIjtzOjMwOiJJbnNlcmlyZSBpbCB0aXRvbG8gZGVsbCdldmVudG8iO3M6MTE6InBsYWNlaG9sZGVyIjtzOjE4OiJUaXRvbG8gZGVsbCdldmVudG8iO3M6OToibWF4bGVuZ3RoIjtzOjI6IjMwIjtzOjQ6InNpemUiO3M6MDoiIjtzOjU6ImNsYXNzIjtzOjA6IiI7czo1OiJ0aXRsZSI7czowOiIiO3M6NToic3R5bGUiO3M6MDoiIjtzOjE1OiI6ZGF0YS1pbnB1dG1hc2siO3M6MDoiIjtzOjY6InBhcmFtcyI7czowOiIiO3M6MTY6IjpkYXRhLWxvYWQtc3RhdGUiO3M6MDoiIjtzOjEzOiI6ZGF0YS10b29sdGlwIjtzOjA6IiI7czo0OiJ0eXBlIjtzOjQ6InRleHQiO3M6MTI6ImNvbnRhaW5lcl9pZCI7czoxOiIwIjtzOjEwOiJ2YWxpZGF0aW9uIjthOjEzOntzOjg6InJlcXVpcmVkIjtzOjA6IiI7czo1OiJhbHBoYSI7czowOiIiO3M6ODoiYWxwaGFudW0iO3M6MDoiIjtzOjU6ImRpZ2l0IjtzOjA6IiI7czo3OiJub2RpZ2l0IjtzOjA6IiI7czo3OiJub3NwYWNlIjtzOjA6IiI7czo2OiJudW1iZXIiO3M6MDoiIjtzOjU6ImVtYWlsIjtzOjA6IiI7czo1OiJwaG9uZSI7czowOiIiO3M6MTE6InBob25lX2ludGVyIjtzOjA6IiI7czozOiJ1cmwiO3M6MDoiIjtzOjc6ImNvbmZpcm0iO3M6MDoiIjtzOjY6ImN1c3RvbSI7czowOiIiO319aToyO2E6MjA6e3M6NDoibmFtZSI7czoxNDoic3VwZXJjYXRlZ29yaWEiO3M6MjoiaWQiO3M6MTQ6InN1cGVyY2F0ZWdvcmlhIjtzOjc6Im9wdGlvbnMiO3M6MDoiIjtzOjU6ImVtcHR5IjtzOjEzOiJOb24gSW1wb3N0YXRvIjtzOjY6InZhbHVlcyI7czowOiIiO3M6NToibGFiZWwiO2E6Mjp7czo0OiJ0ZXh0IjtzOjE1OiJTdXBlciBDYXRlZ29yaWEiO3M6ODoicG9zaXRpb24iO3M6NDoibGVmdCI7fXM6ODoic3VibGFiZWwiO3M6NTU6IkZpbHRybyBzdWxsYSBzdXBlciBjYXRlZ29yaWUgZSBzdWxsZSBzdWUgc290dG9jYXRlZ29yaWUiO3M6ODoibXVsdGlwbGUiO3M6MToiMCI7czo0OiJzaXplIjtzOjA6IiI7czo1OiJjbGFzcyI7czowOiIiO3M6NToidGl0bGUiO3M6MDoiIjtzOjU6InN0eWxlIjtzOjA6IiI7czo2OiJwYXJhbXMiO3M6MDoiIjtzOjE2OiI6ZGF0YS1sb2FkLXN0YXRlIjtzOjA6IiI7czoxMzoiOmRhdGEtdG9vbHRpcCI7czowOiIiO3M6NDoidHlwZSI7czo4OiJkcm9wZG93biI7czoxMjoiY29udGFpbmVyX2lkIjtzOjE6IjAiO3M6MTA6InZhbGlkYXRpb24iO2E6Mzp7czo4OiJyZXF1aXJlZCI7czowOiIiO3M6NzoiY29uZmlybSI7czowOiIiO3M6NjoiY3VzdG9tIjtzOjA6IiI7fXM6MTI6ImR5bmFtaWNfZGF0YSI7YTo0OntzOjc6ImVuYWJsZWQiO3M6MToiMSI7czo5OiJkYXRhX3BhdGgiO3M6MTk6InN1cGVyY2F0ZWdvcmllc2xpc3QiO3M6OToidmFsdWVfa2V5IjtzOjI6ImlkIjtzOjg6InRleHRfa2V5IjtzOjQ6Im5hbWUiO31zOjY6ImV2ZW50cyI7YToxOntpOjA7YTo1OntzOjg6Im9wZXJhdG9yIjtzOjE6Ij0iO3M6NToic3RhdGUiO3M6MDoiIjtzOjY6ImFjdGlvbiI7czowOiIiO3M6NjoidGFyZ2V0IjtzOjA6IiI7czo3OiJvcHRpb25zIjtzOjA6IiI7fX19aTozMTthOjIwOntzOjQ6Im5hbWUiO3M6OToiY2F0ZWdvcmlhIjtzOjI6ImlkIjtzOjk6ImNhdGVnb3JpYSI7czo3OiJvcHRpb25zIjtzOjA6IiI7czo1OiJlbXB0eSI7czowOiIiO3M6NjoidmFsdWVzIjtzOjA6IiI7czo1OiJsYWJlbCI7YToyOntzOjQ6InRleHQiO3M6MTU6IlNvdHRvIENhdGVnb3JpYSI7czo4OiJwb3NpdGlvbiI7czo0OiJsZWZ0Ijt9czo4OiJzdWJsYWJlbCI7czoxMjM6IlNlIG5vbiB2aWVuZSBzZWxlemlvbmF0YSB1bmEgc290dG9jYXRlZ29yaWEgbGEgcmljZXJjYSB2YWxlIHBlciB0dXR0ZSBsZSBzb3R0b2NhdGVnb3JpZSBkZWxsYSBzdXBlcmNhdGVnb3JpYSBzZSBzZWxlemlvbmF0YSI7czo4OiJtdWx0aXBsZSI7czoxOiIwIjtzOjQ6InNpemUiO3M6MDoiIjtzOjU6ImNsYXNzIjtzOjA6IiI7czo1OiJ0aXRsZSI7czowOiIiO3M6NToic3R5bGUiO3M6MDoiIjtzOjY6InBhcmFtcyI7czowOiIiO3M6MTY6IjpkYXRhLWxvYWQtc3RhdGUiO3M6MDoiIjtzOjEzOiI6ZGF0YS10b29sdGlwIjtzOjA6IiI7czo0OiJ0eXBlIjtzOjg6ImRyb3Bkb3duIjtzOjEyOiJjb250YWluZXJfaWQiO3M6MToiMCI7czoxMDoidmFsaWRhdGlvbiI7YTozOntzOjg6InJlcXVpcmVkIjtzOjA6IiI7czo3OiJjb25maXJtIjtzOjA6IiI7czo2OiJjdXN0b20iO3M6MDoiIjt9czoxMjoiZHluYW1pY19kYXRhIjthOjQ6e3M6NzoiZW5hYmxlZCI7czoxOiIxIjtzOjk6ImRhdGFfcGF0aCI7czoxNDoiY2F0ZWdvcmllc2xpc3QiO3M6OToidmFsdWVfa2V5IjtzOjI6ImlkIjtzOjg6InRleHRfa2V5IjtzOjQ6Im5hbWUiO31zOjY6ImV2ZW50cyI7YToxOntpOjA7YTo1OntzOjg6Im9wZXJhdG9yIjtzOjE6Ij0iO3M6NToic3RhdGUiO3M6MDoiIjtzOjY6ImFjdGlvbiI7czowOiIiO3M6NjoidGFyZ2V0IjtzOjA6IiI7czo3OiJvcHRpb25zIjtzOjA6IiI7fX19aToxNDthOjE4OntzOjQ6Im5hbWUiO3M6NzoiZGF0ZWluZiI7czoyOiJpZCI7czo3OiJkYXRlaW5mIjtzOjU6InZhbHVlIjtzOjA6IiI7czo1OiJsYWJlbCI7YToyOntzOjQ6InRleHQiO3M6MTA6IkRhbGxhIGRhdGEiO3M6ODoicG9zaXRpb24iO3M6NDoibGVmdCI7fXM6ODoic3VibGFiZWwiO3M6NTY6IkxpbWl0ZSBpbmZlcmlvcmUgZGVsbCdpbnRlcnZhbGxvIGRpIHRlbXBvIGRhIHNlbGV6aW9uYXJlIjtzOjExOiJwbGFjZWhvbGRlciI7czowOiIiO3M6OToibWF4bGVuZ3RoIjtzOjI6IjMwIjtzOjQ6InNpemUiO3M6MDoiIjtzOjU6ImNsYXNzIjtzOjA6IiI7czo1OiJ0aXRsZSI7czowOiIiO3M6NToic3R5bGUiO3M6MDoiIjtzOjE1OiI6ZGF0YS1pbnB1dG1hc2siO3M6MjI6IidhbGlhcycgOiAnZGQtbW0teXl5eSciO3M6NjoicGFyYW1zIjtzOjA6IiI7czoxNjoiOmRhdGEtbG9hZC1zdGF0ZSI7czowOiIiO3M6MTM6IjpkYXRhLXRvb2x0aXAiO3M6MDoiIjtzOjQ6InR5cGUiO3M6NDoidGV4dCI7czoxMjoiY29udGFpbmVyX2lkIjtzOjE6IjAiO3M6MTA6InZhbGlkYXRpb24iO2E6MTM6e3M6ODoicmVxdWlyZWQiO3M6MDoiIjtzOjU6ImFscGhhIjtzOjA6IiI7czo4OiJhbHBoYW51bSI7czowOiIiO3M6NToiZGlnaXQiO3M6MDoiIjtzOjc6Im5vZGlnaXQiO3M6MDoiIjtzOjc6Im5vc3BhY2UiO3M6MDoiIjtzOjY6Im51bWJlciI7czowOiIiO3M6NToiZW1haWwiO3M6MDoiIjtzOjU6InBob25lIjtzOjA6IiI7czoxMToicGhvbmVfaW50ZXIiO3M6MDoiIjtzOjM6InVybCI7czowOiIiO3M6NzoiY29uZmlybSI7czowOiIiO3M6NjoiY3VzdG9tIjtzOjA6IiI7fX1pOjIyO2E6MTg6e3M6NDoibmFtZSI7czo3OiJkYXRlc3VwIjtzOjI6ImlkIjtzOjc6ImRhdGVzdXAiO3M6NToidmFsdWUiO3M6MDoiIjtzOjU6ImxhYmVsIjthOjI6e3M6NDoidGV4dCI7czo5OiJBbGxhIGRhdGEiO3M6ODoicG9zaXRpb24iO3M6NDoibGVmdCI7fXM6ODoic3VibGFiZWwiO3M6NTY6IkxpbWl0ZSBzdXBlcmlvcmUgZGVsbCdpbnRlcnZhbGxvIGRpIHRlbXBvIGRhIGNvbnNpZGVyYXJlIjtzOjExOiJwbGFjZWhvbGRlciI7czowOiIiO3M6OToibWF4bGVuZ3RoIjtzOjI6IjMwIjtzOjQ6InNpemUiO3M6MDoiIjtzOjU6ImNsYXNzIjtzOjA6IiI7czo1OiJ0aXRsZSI7czowOiIiO3M6NToic3R5bGUiO3M6MDoiIjtzOjE1OiI6ZGF0YS1pbnB1dG1hc2siO3M6MjI6IidhbGlhcycgOiAnZGQtbW0teXl5eSciO3M6NjoicGFyYW1zIjtzOjA6IiI7czoxNjoiOmRhdGEtbG9hZC1zdGF0ZSI7czowOiIiO3M6MTM6IjpkYXRhLXRvb2x0aXAiO3M6MDoiIjtzOjQ6InR5cGUiO3M6NDoidGV4dCI7czoxMjoiY29udGFpbmVyX2lkIjtzOjE6IjAiO3M6MTA6InZhbGlkYXRpb24iO2E6MTM6e3M6ODoicmVxdWlyZWQiO3M6MDoiIjtzOjU6ImFscGhhIjtzOjA6IiI7czo4OiJhbHBoYW51bSI7czowOiIiO3M6NToiZGlnaXQiO3M6MDoiIjtzOjc6Im5vZGlnaXQiO3M6MDoiIjtzOjc6Im5vc3BhY2UiO3M6MDoiIjtzOjY6Im51bWJlciI7czowOiIiO3M6NToiZW1haWwiO3M6MDoiIjtzOjU6InBob25lIjtzOjA6IiI7czoxMToicGhvbmVfaW50ZXIiO3M6MDoiIjtzOjM6InVybCI7czowOiIiO3M6NzoiY29uZmlybSI7czowOiIiO3M6NjoiY3VzdG9tIjtzOjA6IiI7fX1pOjE3O2E6MjA6e3M6NDoibmFtZSI7czozOiJzZXgiO3M6MjoiaWQiO3M6Mzoic2V4IjtzOjc6Im9wdGlvbnMiO3M6NDU6IjA9U29sbyBVb21pbmkNCjE9U29sbyBEb25uZQ0KMz1Eb25uZSBlIFVvbWluaSI7czo1OiJlbXB0eSI7czoxMzoiTm9uIEltcG9zdGF0byI7czo2OiJ2YWx1ZXMiO3M6MDoiIjtzOjU6ImxhYmVsIjthOjI6e3M6NDoidGV4dCI7czo1OiJTZXNzbyI7czo4OiJwb3NpdGlvbiI7czo0OiJsZWZ0Ijt9czo4OiJzdWJsYWJlbCI7czozOToiR2VuZXJlIHBlciBjdWkgdW4gZXZlbnRvIMOoIHBpYW5pZmljYXRvIjtzOjg6Im11bHRpcGxlIjtzOjE6IjAiO3M6NDoic2l6ZSI7czowOiIiO3M6NToiY2xhc3MiO3M6MDoiIjtzOjU6InRpdGxlIjtzOjA6IiI7czo1OiJzdHlsZSI7czowOiIiO3M6NjoicGFyYW1zIjtzOjA6IiI7czoxNjoiOmRhdGEtbG9hZC1zdGF0ZSI7czowOiIiO3M6MTM6IjpkYXRhLXRvb2x0aXAiO3M6MDoiIjtzOjQ6InR5cGUiO3M6ODoiZHJvcGRvd24iO3M6MTI6ImNvbnRhaW5lcl9pZCI7czoxOiIwIjtzOjEwOiJ2YWxpZGF0aW9uIjthOjM6e3M6ODoicmVxdWlyZWQiO3M6MDoiIjtzOjc6ImNvbmZpcm0iO3M6MDoiIjtzOjY6ImN1c3RvbSI7czowOiIiO31zOjEyOiJkeW5hbWljX2RhdGEiO2E6NDp7czo3OiJlbmFibGVkIjtzOjA6IiI7czo5OiJkYXRhX3BhdGgiO3M6MDoiIjtzOjk6InZhbHVlX2tleSI7czowOiIiO3M6ODoidGV4dF9rZXkiO3M6MDoiIjt9czo2OiJldmVudHMiO2E6MTp7aTowO2E6NTp7czo4OiJvcGVyYXRvciI7czoxOiI9IjtzOjU6InN0YXRlIjtzOjA6IiI7czo2OiJhY3Rpb24iO3M6MDoiIjtzOjY6InRhcmdldCI7czowOiIiO3M6Nzoib3B0aW9ucyI7czowOiIiO319fWk6NzthOjIwOntzOjQ6Im5hbWUiO3M6NToic2NvcGUiO3M6MjoiaWQiO3M6NToic2NvcGUiO3M6Nzoib3B0aW9ucyI7czozNzoiMD1QdWJibGljbw0KMT1Qcml2YXRvDQoyPUZyaWVuZHMgT25seSI7czo1OiJlbXB0eSI7czoxMzoiTm9uIEltcG9zdGF0byI7czo2OiJ2YWx1ZXMiO3M6MDoiIjtzOjU6ImxhYmVsIjthOjI6e3M6NDoidGV4dCI7czo3OiJBY2Nlc3NvIjtzOjg6InBvc2l0aW9uIjtzOjQ6ImxlZnQiO31zOjg6InN1YmxhYmVsIjtzOjM0OiJTZWxlemlvbmFyZSBsYSB0aXBvbG9naWEgZGkgZXZlbnRvIjtzOjg6Im11bHRpcGxlIjtzOjE6IjAiO3M6NDoic2l6ZSI7czowOiIiO3M6NToiY2xhc3MiO3M6MDoiIjtzOjU6InRpdGxlIjtzOjA6IiI7czo1OiJzdHlsZSI7czowOiIiO3M6NjoicGFyYW1zIjtzOjA6IiI7czoxNjoiOmRhdGEtbG9hZC1zdGF0ZSI7czowOiIiO3M6MTM6IjpkYXRhLXRvb2x0aXAiO3M6MDoiIjtzOjQ6InR5cGUiO3M6ODoiZHJvcGRvd24iO3M6MTI6ImNvbnRhaW5lcl9pZCI7czoxOiIwIjtzOjEwOiJ2YWxpZGF0aW9uIjthOjM6e3M6ODoicmVxdWlyZWQiO3M6MDoiIjtzOjc6ImNvbmZpcm0iO3M6MDoiIjtzOjY6ImN1c3RvbSI7czowOiIiO31zOjEyOiJkeW5hbWljX2RhdGEiO2E6NDp7czo3OiJlbmFibGVkIjtzOjA6IiI7czo5OiJkYXRhX3BhdGgiO3M6MDoiIjtzOjk6InZhbHVlX2tleSI7czowOiIiO3M6ODoidGV4dF9rZXkiO3M6MDoiIjt9czo2OiJldmVudHMiO2E6MTp7aTowO2E6NTp7czo4OiJvcGVyYXRvciI7czoxOiI9IjtzOjU6InN0YXRlIjtzOjA6IiI7czo2OiJhY3Rpb24iO3M6MDoiIjtzOjY6InRhcmdldCI7czowOiIiO3M6Nzoib3B0aW9ucyI7czowOiIiO319fWk6MzthOjE4OntzOjQ6Im5hbWUiO3M6MTE6ImRlc2NyaXB0aW9uIjtzOjI6ImlkIjtzOjExOiJkZXNjcmlwdGlvbiI7czo1OiJ2YWx1ZSI7czowOiIiO3M6NToibGFiZWwiO2E6Mjp7czo0OiJ0ZXh0IjtzOjExOiJEZXNjcml6aW9uZSI7czo4OiJwb3NpdGlvbiI7czo0OiJsZWZ0Ijt9czo4OiJzdWJsYWJlbCI7czo5MjoiU2VxdWVuemEgZGkgdG9rZW4gY2hlIHBvc3Nvbm8gdHJvdmFyc2kgbmVsbGEgZGVzY3JpemlvbmUgZGVsbCdldmVudG8gc2VwYXJhdGkgZGEgdW5hIHZpcmdvbGEiO3M6MTE6InBsYWNlaG9sZGVyIjtzOjQ0OiJFczogTG9yZW0gaXBzdW0sIGxvcmVtLCBpcHN1bSwgdG9rZW4xLCB0b2tlbiI7czo0OiJyb3dzIjtzOjE6IjUiO3M6NDoiY29scyI7czoyOiI0MCI7czo1OiJjbGFzcyI7czowOiIiO3M6NToidGl0bGUiO3M6MDoiIjtzOjU6InN0eWxlIjtzOjA6IiI7czoxMzoiOmRhdGEtd3lzaXd5ZyI7czoxOiIwIjtzOjY6InBhcmFtcyI7czowOiIiO3M6MTY6IjpkYXRhLWxvYWQtc3RhdGUiO3M6MDoiIjtzOjEzOiI6ZGF0YS10b29sdGlwIjtzOjA6IiI7czo0OiJ0eXBlIjtzOjg6InRleHRhcmVhIjtzOjEyOiJjb250YWluZXJfaWQiO3M6MToiMCI7czoxMDoidmFsaWRhdGlvbiI7YTo4OntzOjg6InJlcXVpcmVkIjtzOjA6IiI7czo1OiJhbHBoYSI7czowOiIiO3M6ODoiYWxwaGFudW0iO3M6MDoiIjtzOjU6ImRpZ2l0IjtzOjA6IiI7czo3OiJub2RpZ2l0IjtzOjA6IiI7czo2OiJudW1iZXIiO3M6MDoiIjtzOjc6ImNvbmZpcm0iO3M6MDoiIjtzOjY6ImN1c3RvbSI7czowOiIiO319aTo1O2E6MTg6e3M6NDoibmFtZSI7czo0OiJ0b3duIjtzOjI6ImlkIjtzOjQ6InRvd24iO3M6NToidmFsdWUiO3M6MDoiIjtzOjU6ImxhYmVsIjthOjI6e3M6NDoidGV4dCI7czo2OiJDaXR0w6AiO3M6ODoicG9zaXRpb24iO3M6NDoibGVmdCI7fXM6ODoic3VibGFiZWwiO3M6NTg6Ikluc2VyaXJlIGlsIG5vbWUgZGVsbGEgY2l0dMOgIGluIGN1aSBzaSBzdm9sZ2Vyw6AgbCdldmVudG8iO3M6MTE6InBsYWNlaG9sZGVyIjtzOjY6IkNpdHTDoCI7czo5OiJtYXhsZW5ndGgiO3M6MjoiNDAiO3M6NDoic2l6ZSI7czowOiIiO3M6NToiY2xhc3MiO3M6MDoiIjtzOjU6InRpdGxlIjtzOjA6IiI7czo1OiJzdHlsZSI7czowOiIiO3M6MTU6IjpkYXRhLWlucHV0bWFzayI7czowOiIiO3M6NjoicGFyYW1zIjtzOjA6IiI7czoxNjoiOmRhdGEtbG9hZC1zdGF0ZSI7czowOiIiO3M6MTM6IjpkYXRhLXRvb2x0aXAiO3M6MDoiIjtzOjQ6InR5cGUiO3M6NDoidGV4dCI7czoxMjoiY29udGFpbmVyX2lkIjtzOjE6IjAiO3M6MTA6InZhbGlkYXRpb24iO2E6MTM6e3M6ODoicmVxdWlyZWQiO3M6MDoiIjtzOjU6ImFscGhhIjtzOjA6IiI7czo4OiJhbHBoYW51bSI7czowOiIiO3M6NToiZGlnaXQiO3M6MDoiIjtzOjc6Im5vZGlnaXQiO3M6MDoiIjtzOjc6Im5vc3BhY2UiO3M6MDoiIjtzOjY6Im51bWJlciI7czowOiIiO3M6NToiZW1haWwiO3M6MDoiIjtzOjU6InBob25lIjtzOjA6IiI7czoxMToicGhvbmVfaW50ZXIiO3M6MDoiIjtzOjM6InVybCI7czowOiIiO3M6NzoiY29uZmlybSI7czowOiIiO3M6NjoiY3VzdG9tIjtzOjA6IiI7fX1pOjIzO2E6MTg6e3M6NDoibmFtZSI7czoxNDoibWlucGFydGVjaXBhbnQiO3M6MjoiaWQiO3M6MTQ6Im1pbnBhcnRlY2lwYW50IjtzOjU6InZhbHVlIjtzOjA6IiI7czo1OiJsYWJlbCI7YToyOntzOjQ6InRleHQiO3M6Mjk6IkxpbWl0ZSBpbmZlcmlvcmUgcGFydGVjaXBhbnRpIjtzOjg6InBvc2l0aW9uIjtzOjQ6ImxlZnQiO31zOjg6InN1YmxhYmVsIjtzOjUxOiJOdW1lcm8gZGkgcGFydGVjaXBhbnRpIHN1cGVyaW9yZSBhbCB2YWxvcmUgaW5zZXJpdG8iO3M6MTE6InBsYWNlaG9sZGVyIjtzOjA6IiI7czo5OiJtYXhsZW5ndGgiO3M6MToiMyI7czo0OiJzaXplIjtzOjA6IiI7czo1OiJjbGFzcyI7czowOiIiO3M6NToidGl0bGUiO3M6MDoiIjtzOjU6InN0eWxlIjtzOjA6IiI7czoxNToiOmRhdGEtaW5wdXRtYXNrIjtzOjE5OiInYWxpYXMnIDogJ2ludGVnZXInIjtzOjY6InBhcmFtcyI7czowOiIiO3M6MTY6IjpkYXRhLWxvYWQtc3RhdGUiO3M6MDoiIjtzOjEzOiI6ZGF0YS10b29sdGlwIjtzOjA6IiI7czo0OiJ0eXBlIjtzOjQ6InRleHQiO3M6MTI6ImNvbnRhaW5lcl9pZCI7czoxOiIwIjtzOjEwOiJ2YWxpZGF0aW9uIjthOjEzOntzOjg6InJlcXVpcmVkIjtzOjA6IiI7czo1OiJhbHBoYSI7czowOiIiO3M6ODoiYWxwaGFudW0iO3M6MDoiIjtzOjU6ImRpZ2l0IjtzOjA6IiI7czo3OiJub2RpZ2l0IjtzOjA6IiI7czo3OiJub3NwYWNlIjtzOjA6IiI7czo2OiJudW1iZXIiO3M6MToiMSI7czo1OiJlbWFpbCI7czowOiIiO3M6NToicGhvbmUiO3M6MDoiIjtzOjExOiJwaG9uZV9pbnRlciI7czowOiIiO3M6MzoidXJsIjtzOjA6IiI7czo3OiJjb25maXJtIjtzOjA6IiI7czo2OiJjdXN0b20iO3M6MDoiIjt9fWk6MjQ7YToxODp7czo0OiJuYW1lIjtzOjE0OiJtYXhwYXJ0ZWNpcGFudCI7czoyOiJpZCI7czoxNDoibWF4cGFydGVjaXBhbnQiO3M6NToidmFsdWUiO3M6MDoiIjtzOjU6ImxhYmVsIjthOjI6e3M6NDoidGV4dCI7czoyOToiTGltaXRlIHN1cGVyaW9yZSBwYXJ0ZWNpcGFudGkiO3M6ODoicG9zaXRpb24iO3M6NDoibGVmdCI7fXM6ODoic3VibGFiZWwiO3M6NTE6Ik51bWVybyBkaSBwYXJ0ZWNpcGFudGkgaW5mZXJpb3JlIGFsIHZhbG9yZSBpbnNlcml0byI7czoxMToicGxhY2Vob2xkZXIiO3M6MDoiIjtzOjk6Im1heGxlbmd0aCI7czoxOiIzIjtzOjQ6InNpemUiO3M6MDoiIjtzOjU6ImNsYXNzIjtzOjA6IiI7czo1OiJ0aXRsZSI7czowOiIiO3M6NToic3R5bGUiO3M6MDoiIjtzOjE1OiI6ZGF0YS1pbnB1dG1hc2siO3M6MTk6IidhbGlhcycgOiAnaW50ZWdlciciO3M6NjoicGFyYW1zIjtzOjA6IiI7czoxNjoiOmRhdGEtbG9hZC1zdGF0ZSI7czowOiIiO3M6MTM6IjpkYXRhLXRvb2x0aXAiO3M6MDoiIjtzOjQ6InR5cGUiO3M6NDoidGV4dCI7czoxMjoiY29udGFpbmVyX2lkIjtzOjE6IjAiO3M6MTA6InZhbGlkYXRpb24iO2E6MTM6e3M6ODoicmVxdWlyZWQiO3M6MDoiIjtzOjU6ImFscGhhIjtzOjA6IiI7czo4OiJhbHBoYW51bSI7czowOiIiO3M6NToiZGlnaXQiO3M6MDoiIjtzOjc6Im5vZGlnaXQiO3M6MDoiIjtzOjc6Im5vc3BhY2UiO3M6MDoiIjtzOjY6Im51bWJlciI7czoxOiIxIjtzOjU6ImVtYWlsIjtzOjA6IiI7czo1OiJwaG9uZSI7czowOiIiO3M6MTE6InBob25lX2ludGVyIjtzOjA6IiI7czozOiJ1cmwiO3M6MDoiIjtzOjc6ImNvbmZpcm0iO3M6MDoiIjtzOjY6ImN1c3RvbSI7czowOiIiO319aToyNTthOjE4OntzOjQ6Im5hbWUiO3M6ODoibWlucGxhY2UiO3M6MjoiaWQiO3M6ODoibWlucGxhY2UiO3M6NToidmFsdWUiO3M6MDoiIjtzOjU6ImxhYmVsIjthOjI6e3M6NDoidGV4dCI7czozNDoiTGltaXRlIGluZmVyaW9yZSBwb3N0aSBkaXNwb25pYmlsaSI7czo4OiJwb3NpdGlvbiI7czo0OiJsZWZ0Ijt9czo4OiJzdWJsYWJlbCI7czo1NjoiTnVtZXJvIGRpIHBvc3RpIGRpc3BvbmliaWxpIHN1cGVyaW9yZSBhbCB2YWxvcmUgaW5zZXJpdG8iO3M6MTE6InBsYWNlaG9sZGVyIjtzOjA6IiI7czo5OiJtYXhsZW5ndGgiO3M6MToiMyI7czo0OiJzaXplIjtzOjA6IiI7czo1OiJjbGFzcyI7czowOiIiO3M6NToidGl0bGUiO3M6MDoiIjtzOjU6InN0eWxlIjtzOjA6IiI7czoxNToiOmRhdGEtaW5wdXRtYXNrIjtzOjE5OiInYWxpYXMnIDogJ2ludGVnZXInIjtzOjY6InBhcmFtcyI7czowOiIiO3M6MTY6IjpkYXRhLWxvYWQtc3RhdGUiO3M6MDoiIjtzOjEzOiI6ZGF0YS10b29sdGlwIjtzOjA6IiI7czo0OiJ0eXBlIjtzOjQ6InRleHQiO3M6MTI6ImNvbnRhaW5lcl9pZCI7czoxOiIwIjtzOjEwOiJ2YWxpZGF0aW9uIjthOjEzOntzOjg6InJlcXVpcmVkIjtzOjA6IiI7czo1OiJhbHBoYSI7czowOiIiO3M6ODoiYWxwaGFudW0iO3M6MDoiIjtzOjU6ImRpZ2l0IjtzOjA6IiI7czo3OiJub2RpZ2l0IjtzOjA6IiI7czo3OiJub3NwYWNlIjtzOjA6IiI7czo2OiJudW1iZXIiO3M6MToiMSI7czo1OiJlbWFpbCI7czowOiIiO3M6NToicGhvbmUiO3M6MDoiIjtzOjExOiJwaG9uZV9pbnRlciI7czowOiIiO3M6MzoidXJsIjtzOjA6IiI7czo3OiJjb25maXJtIjtzOjA6IiI7czo2OiJjdXN0b20iO3M6MDoiIjt9fWk6MjY7YToxODp7czo0OiJuYW1lIjtzOjg6Im1heHBsYWNlIjtzOjI6ImlkIjtzOjg6Im1heHBsYWNlIjtzOjU6InZhbHVlIjtzOjA6IiI7czo1OiJsYWJlbCI7YToyOntzOjQ6InRleHQiO3M6MzQ6IkxpbWl0ZSBzdXBlcmlvcmUgcG9zdGkgZGlzcG9uaWJpbGkiO3M6ODoicG9zaXRpb24iO3M6NDoibGVmdCI7fXM6ODoic3VibGFiZWwiO3M6NTY6Ik51bWVybyBkaSBwb3N0aSBkaXNwb25pYmlsaSBpbmZlcmlvcmUgYWwgdmFsb3JlIGluc2VyaXRvIjtzOjExOiJwbGFjZWhvbGRlciI7czowOiIiO3M6OToibWF4bGVuZ3RoIjtzOjE6IjMiO3M6NDoic2l6ZSI7czowOiIiO3M6NToiY2xhc3MiO3M6MDoiIjtzOjU6InRpdGxlIjtzOjA6IiI7czo1OiJzdHlsZSI7czowOiIiO3M6MTU6IjpkYXRhLWlucHV0bWFzayI7czoxOToiJ2FsaWFzJyA6ICdpbnRlZ2VyJyI7czo2OiJwYXJhbXMiO3M6MDoiIjtzOjE2OiI6ZGF0YS1sb2FkLXN0YXRlIjtzOjA6IiI7czoxMzoiOmRhdGEtdG9vbHRpcCI7czowOiIiO3M6NDoidHlwZSI7czo0OiJ0ZXh0IjtzOjEyOiJjb250YWluZXJfaWQiO3M6MToiMCI7czoxMDoidmFsaWRhdGlvbiI7YToxMzp7czo4OiJyZXF1aXJlZCI7czowOiIiO3M6NToiYWxwaGEiO3M6MDoiIjtzOjg6ImFscGhhbnVtIjtzOjA6IiI7czo1OiJkaWdpdCI7czowOiIiO3M6Nzoibm9kaWdpdCI7czowOiIiO3M6Nzoibm9zcGFjZSI7czowOiIiO3M6NjoibnVtYmVyIjtzOjE6IjEiO3M6NToiZW1haWwiO3M6MDoiIjtzOjU6InBob25lIjtzOjA6IiI7czoxMToicGhvbmVfaW50ZXIiO3M6MDoiIjtzOjM6InVybCI7czowOiIiO3M6NzoiY29uZmlybSI7czowOiIiO3M6NjoiY3VzdG9tIjtzOjA6IiI7fX1pOjI3O2E6MTg6e3M6NDoibmFtZSI7czoxMDoibWluYWdlZnJvbSI7czoyOiJpZCI7czoxMDoibWluYWdlZnJvbSI7czo1OiJ2YWx1ZSI7czowOiIiO3M6NToibGFiZWwiO2E6Mjp7czo0OiJ0ZXh0IjtzOjI4OiJMaW1pdGUgaW5mZXJpb3JlIGV0w6AgbWluaW1hIjtzOjg6InBvc2l0aW9uIjtzOjQ6ImxlZnQiO31zOjg6InN1YmxhYmVsIjtzOjQwOiJFdMOgIG1pbmltYSBzdXBlcmlvcmUgYWwgdmFsb3JlIGluc2VyaXRvIjtzOjExOiJwbGFjZWhvbGRlciI7czowOiIiO3M6OToibWF4bGVuZ3RoIjtzOjE6IjMiO3M6NDoic2l6ZSI7czowOiIiO3M6NToiY2xhc3MiO3M6MDoiIjtzOjU6InRpdGxlIjtzOjA6IiI7czo1OiJzdHlsZSI7czowOiIiO3M6MTU6IjpkYXRhLWlucHV0bWFzayI7czoxOToiJ2FsaWFzJyA6ICdpbnRlZ2VyJyI7czo2OiJwYXJhbXMiO3M6MDoiIjtzOjE2OiI6ZGF0YS1sb2FkLXN0YXRlIjtzOjA6IiI7czoxMzoiOmRhdGEtdG9vbHRpcCI7czowOiIiO3M6NDoidHlwZSI7czo0OiJ0ZXh0IjtzOjEyOiJjb250YWluZXJfaWQiO3M6MToiMCI7czoxMDoidmFsaWRhdGlvbiI7YToxMzp7czo4OiJyZXF1aXJlZCI7czowOiIiO3M6NToiYWxwaGEiO3M6MDoiIjtzOjg6ImFscGhhbnVtIjtzOjA6IiI7czo1OiJkaWdpdCI7czowOiIiO3M6Nzoibm9kaWdpdCI7czowOiIiO3M6Nzoibm9zcGFjZSI7czowOiIiO3M6NjoibnVtYmVyIjtzOjE6IjEiO3M6NToiZW1haWwiO3M6MDoiIjtzOjU6InBob25lIjtzOjA6IiI7czoxMToicGhvbmVfaW50ZXIiO3M6MDoiIjtzOjM6InVybCI7czowOiIiO3M6NzoiY29uZmlybSI7czowOiIiO3M6NjoiY3VzdG9tIjtzOjA6IiI7fX1pOjI4O2E6MTg6e3M6NDoibmFtZSI7czoxMDoibWF4YWdlZnJvbSI7czoyOiJpZCI7czoxMDoibWF4YWdlZnJvbSI7czo1OiJ2YWx1ZSI7czowOiIiO3M6NToibGFiZWwiO2E6Mjp7czo0OiJ0ZXh0IjtzOjI4OiJMaW1pdGUgc3VwZXJpb3JlIGV0w6AgbWluaW1hIjtzOjg6InBvc2l0aW9uIjtzOjQ6ImxlZnQiO31zOjg6InN1YmxhYmVsIjtzOjQwOiJFdMOgIG1pbmltYSBzdXBlcmlvcmUgYWwgdmFsb3JlIGluc2VyaXRvIjtzOjExOiJwbGFjZWhvbGRlciI7czowOiIiO3M6OToibWF4bGVuZ3RoIjtzOjE6IjMiO3M6NDoic2l6ZSI7czowOiIiO3M6NToiY2xhc3MiO3M6MDoiIjtzOjU6InRpdGxlIjtzOjA6IiI7czo1OiJzdHlsZSI7czowOiIiO3M6MTU6IjpkYXRhLWlucHV0bWFzayI7czoxOToiJ2FsaWFzJyA6ICdpbnRlZ2VyJyI7czo2OiJwYXJhbXMiO3M6MDoiIjtzOjE2OiI6ZGF0YS1sb2FkLXN0YXRlIjtzOjA6IiI7czoxMzoiOmRhdGEtdG9vbHRpcCI7czowOiIiO3M6NDoidHlwZSI7czo0OiJ0ZXh0IjtzOjEyOiJjb250YWluZXJfaWQiO3M6MToiMCI7czoxMDoidmFsaWRhdGlvbiI7YToxMzp7czo4OiJyZXF1aXJlZCI7czowOiIiO3M6NToiYWxwaGEiO3M6MDoiIjtzOjg6ImFscGhhbnVtIjtzOjA6IiI7czo1OiJkaWdpdCI7czowOiIiO3M6Nzoibm9kaWdpdCI7czowOiIiO3M6Nzoibm9zcGFjZSI7czowOiIiO3M6NjoibnVtYmVyIjtzOjE6IjEiO3M6NToiZW1haWwiO3M6MDoiIjtzOjU6InBob25lIjtzOjA6IiI7czoxMToicGhvbmVfaW50ZXIiO3M6MDoiIjtzOjM6InVybCI7czowOiIiO3M6NzoiY29uZmlybSI7czowOiIiO3M6NjoiY3VzdG9tIjtzOjA6IiI7fX1pOjI5O2E6MTg6e3M6NDoibmFtZSI7czo4OiJtaW5hZ2V0byI7czoyOiJpZCI7czo4OiJtaW5hZ2V0byI7czo1OiJ2YWx1ZSI7czowOiIiO3M6NToibGFiZWwiO2E6Mjp7czo0OiJ0ZXh0IjtzOjI5OiJMaW1pdGUgaW5mZXJpb3JlIGV0w6AgbWFzc2ltYSI7czo4OiJwb3NpdGlvbiI7czo0OiJsZWZ0Ijt9czo4OiJzdWJsYWJlbCI7czo0MToiRXTDoCBtYXNzaW1hIHN1cGVyaW9yZSBhbCB2YWxvcmUgaW5zZXJpdG8iO3M6MTE6InBsYWNlaG9sZGVyIjtzOjA6IiI7czo5OiJtYXhsZW5ndGgiO3M6MToiMyI7czo0OiJzaXplIjtzOjA6IiI7czo1OiJjbGFzcyI7czowOiIiO3M6NToidGl0bGUiO3M6MDoiIjtzOjU6InN0eWxlIjtzOjA6IiI7czoxNToiOmRhdGEtaW5wdXRtYXNrIjtzOjE5OiInYWxpYXMnIDogJ2ludGVnZXInIjtzOjY6InBhcmFtcyI7czowOiIiO3M6MTY6IjpkYXRhLWxvYWQtc3RhdGUiO3M6MDoiIjtzOjEzOiI6ZGF0YS10b29sdGlwIjtzOjA6IiI7czo0OiJ0eXBlIjtzOjQ6InRleHQiO3M6MTI6ImNvbnRhaW5lcl9pZCI7czoxOiIwIjtzOjEwOiJ2YWxpZGF0aW9uIjthOjEzOntzOjg6InJlcXVpcmVkIjtzOjA6IiI7czo1OiJhbHBoYSI7czowOiIiO3M6ODoiYWxwaGFudW0iO3M6MDoiIjtzOjU6ImRpZ2l0IjtzOjA6IiI7czo3OiJub2RpZ2l0IjtzOjA6IiI7czo3OiJub3NwYWNlIjtzOjA6IiI7czo2OiJudW1iZXIiO3M6MToiMSI7czo1OiJlbWFpbCI7czowOiIiO3M6NToicGhvbmUiO3M6MDoiIjtzOjExOiJwaG9uZV9pbnRlciI7czowOiIiO3M6MzoidXJsIjtzOjA6IiI7czo3OiJjb25maXJtIjtzOjA6IiI7czo2OiJjdXN0b20iO3M6MDoiIjt9fWk6MzA7YToxODp7czo0OiJuYW1lIjtzOjg6Im1heGFnZXRvIjtzOjI6ImlkIjtzOjg6Im1heGFnZXRvIjtzOjU6InZhbHVlIjtzOjA6IiI7czo1OiJsYWJlbCI7YToyOntzOjQ6InRleHQiO3M6Mjk6IkxpbWl0ZSBzdXBlcmlvcmUgZXTDoCBtYXNzaW1hIjtzOjg6InBvc2l0aW9uIjtzOjQ6ImxlZnQiO31zOjg6InN1YmxhYmVsIjtzOjQxOiJFdMOgIG1hc3NpbWEgaW5mZXJpb3JlIGFsIHZhbG9yZSBpbnNlcml0byI7czoxMToicGxhY2Vob2xkZXIiO3M6MDoiIjtzOjk6Im1heGxlbmd0aCI7czoxOiIzIjtzOjQ6InNpemUiO3M6MDoiIjtzOjU6ImNsYXNzIjtzOjA6IiI7czo1OiJ0aXRsZSI7czowOiIiO3M6NToic3R5bGUiO3M6MDoiIjtzOjE1OiI6ZGF0YS1pbnB1dG1hc2siO3M6MTk6IidhbGlhcycgOiAnaW50ZWdlciciO3M6NjoicGFyYW1zIjtzOjA6IiI7czoxNjoiOmRhdGEtbG9hZC1zdGF0ZSI7czowOiIiO3M6MTM6IjpkYXRhLXRvb2x0aXAiO3M6MDoiIjtzOjQ6InR5cGUiO3M6NDoidGV4dCI7czoxMjoiY29udGFpbmVyX2lkIjtzOjE6IjAiO3M6MTA6InZhbGlkYXRpb24iO2E6MTM6e3M6ODoicmVxdWlyZWQiO3M6MDoiIjtzOjU6ImFscGhhIjtzOjA6IiI7czo4OiJhbHBoYW51bSI7czowOiIiO3M6NToiZGlnaXQiO3M6MDoiIjtzOjc6Im5vZGlnaXQiO3M6MDoiIjtzOjc6Im5vc3BhY2UiO3M6MDoiIjtzOjY6Im51bWJlciI7czoxOiIxIjtzOjU6ImVtYWlsIjtzOjA6IiI7czo1OiJwaG9uZSI7czowOiIiO3M6MTE6InBob25lX2ludGVyIjtzOjA6IiI7czozOiJ1cmwiO3M6MDoiIjtzOjc6ImNvbmZpcm0iO3M6MDoiIjtzOjY6ImN1c3RvbSI7czowOiIiO319aToxNjthOjEwOntzOjExOiJyZW5kZXJfdHlwZSI7czo2OiJzdWJtaXQiO3M6NDoibmFtZSI7czo4OiJidXR0b24xNiI7czoyOiJpZCI7czo4OiJidXR0b24xNiI7czo0OiJ0eXBlIjtzOjY6InN1Ym1pdCI7czo1OiJ2YWx1ZSI7czo1OiJDZXJjYSI7czo1OiJjbGFzcyI7czoxNToiYnRuIGJ0bi1kZWZhdWx0IjtzOjU6InN0eWxlIjtzOjA6IiI7czo2OiJwYXJhbXMiO3M6MDoiIjtzOjE2OiI6ZGF0YS1sb2FkLXN0YXRlIjtzOjA6IiI7czoxMjoiY29udGFpbmVyX2lkIjtzOjE6IjAiO319czozOiJETkEiO2E6Mjp7czo0OiJsb2FkIjthOjU6e3M6OToiZGJfcmVhZF8xIjthOjI6e3M6NToiZm91bmQiO3M6MDoiIjtzOjk6Im5vdF9mb3VuZCI7czowOiIiO31zOjk6ImRiX3JlYWRfNiI7YToyOntzOjU6ImZvdW5kIjthOjE6e3M6MTM6ImN1c3RvbV9jb2RlXzciO3M6MDoiIjt9czo5OiJub3RfZm91bmQiO3M6MDoiIjt9czoxMzoiY3VzdG9tX2NvZGVfMiI7czowOiIiO3M6NjoiaHRtbF8wIjtzOjA6IiI7czoxMzoiY3VzdG9tX2NvZGVfNSI7czowOiIiO31zOjY6InN1Ym1pdCI7YToxOntzOjEzOiJjdXN0b21fY29kZV8zIjtzOjA6IiI7fX1zOjE0OiJhY3Rpb25zX2NvbmZpZyI7YTo3OntpOjE7YToxOTp7czoxMjoiYWN0aW9uX2xhYmVsIjtzOjI3OiJDYXJpY2FtZW50byBkZWxsZSBjYXRlZ29yaWUiO3M6NzoiZW5hYmxlZCI7czoxOiIxIjtzOjk6InRhYmxlbmFtZSI7czoxMDoiY2F0ZWdvcmllcyI7czoxMDoibXVsdGlfcmVhZCI7czoxOiIxIjtzOjE4OiJsb2FkX3VuZGVyX21vZGVsaWQiO3M6MToiMSI7czo4OiJtb2RlbF9pZCI7czoxOToic3VwZXJjYXRlZ29yaWVzbGlzdCI7czo2OiJmaWVsZHMiO3M6MTY6ImlkLG5hbWUsbGFuZ3VhZ2UiO3M6NToib3JkZXIiO3M6MjoiaWQiO3M6NToiZ3JvdXAiO3M6MDoiIjtzOjEwOiJjb25kaXRpb25zIjtzOjQ5OiI8P3BocCByZXR1cm4gYXJyYXkoJ2lkc3VwZXJjYXRlZ29yeScgPT4gTlVMTCk7ID8+IjtzOjE2OiJlbmFibGVfcmVsYXRpb25zIjtzOjE6IjAiO3M6OToicmVsYXRpb25zIjthOjE6e2k6MDthOjQ6e3M6NToibW9kZWwiO3M6MDoiIjtzOjk6InRhYmxlbmFtZSI7czowOiIiO3M6NDoidHlwZSI7czo2OiJoYXNPbmUiO3M6NDoiZmtleSI7czowOiIiO319czoxMDoibmRiX2VuYWJsZSI7czoxOiIwIjtzOjEwOiJuZGJfZHJpdmVyIjtzOjU6Im15c3FsIjtzOjg6Im5kYl9ob3N0IjtzOjk6ImxvY2FsaG9zdCI7czoxMjoibmRiX2RhdGFiYXNlIjtzOjA6IiI7czo4OiJuZGJfdXNlciI7czowOiIiO3M6MTI6Im5kYl9wYXNzd29yZCI7czowOiIiO3M6MTA6Im5kYl9wcmVmaXgiO3M6NDoiam9zXyI7fWk6NzthOjI6e3M6MTI6ImFjdGlvbl9sYWJlbCI7czo0NDoiRm9ybWF0dGF6aW9uZSBkZWkgZGF0aSBwZXIgbGUgc290dG9jYXRlZ29yaWUiO3M6NzoiY29udGVudCI7czo4ODoiPD9waHANCiRlbSA9IG5ldyBFdmVudHNNb2RlbCgpOw0KJGVtLT5jYXRlZ29yeWxpc3QoJGZvcm0tPmRhdGFbJ2NhdGVnb3JpZXNsaXN0J10pOw0KPz4NCiI7fWk6NjthOjE5OntzOjEyOiJhY3Rpb25fbGFiZWwiO3M6MzI6IkNhcmljYW1lbnRvIGRlbGxlIHNvdHRvY2F0ZWdvcmllIjtzOjc6ImVuYWJsZWQiO3M6MToiMSI7czo5OiJ0YWJsZW5hbWUiO3M6MTA6ImNhdGVnb3JpZXMiO3M6MTA6Im11bHRpX3JlYWQiO3M6MToiMSI7czoxODoibG9hZF91bmRlcl9tb2RlbGlkIjtzOjE6IjEiO3M6ODoibW9kZWxfaWQiO3M6MTQ6ImNhdGVnb3JpZXNsaXN0IjtzOjY6ImZpZWxkcyI7czozMjoiaWQsbmFtZSxsYW5ndWFnZSxpZHN1cGVyY2F0ZWdvcnkiO3M6NToib3JkZXIiO3M6MjoiaWQiO3M6NToiZ3JvdXAiO3M6MDoiIjtzOjEwOiJjb25kaXRpb25zIjtzOjA6IiI7czoxNjoiZW5hYmxlX3JlbGF0aW9ucyI7czoxOiIwIjtzOjk6InJlbGF0aW9ucyI7YToxOntpOjA7YTo0OntzOjU6Im1vZGVsIjtzOjA6IiI7czo5OiJ0YWJsZW5hbWUiO3M6MDoiIjtzOjQ6InR5cGUiO3M6NjoiaGFzT25lIjtzOjQ6ImZrZXkiO3M6MDoiIjt9fXM6MTA6Im5kYl9lbmFibGUiO3M6MToiMCI7czoxMDoibmRiX2RyaXZlciI7czo1OiJteXNxbCI7czo4OiJuZGJfaG9zdCI7czo5OiJsb2NhbGhvc3QiO3M6MTI6Im5kYl9kYXRhYmFzZSI7czowOiIiO3M6ODoibmRiX3VzZXIiO3M6MDoiIjtzOjEyOiJuZGJfcGFzc3dvcmQiO3M6MDoiIjtzOjEwOiJuZGJfcHJlZml4IjtzOjQ6Impvc18iO31pOjI7YToyOntzOjEyOiJhY3Rpb25fbGFiZWwiO3M6MjA6IkNhcmljYW1lbnRvIGRlaSBkYXRpIjtzOjc6ImNvbnRlbnQiO3M6OTM6Ijw/cGhwDQokZWMgPVRyYXZlbDo6Z2V0SW5zdGFuY2UoKS0+Z2V0KCdldmVudENvbnRyb2xsZXInKTsNCiRlYy0+aW5pdFNlYXJjaEZvcm0oJGZvcm0pOw0KPz4NCiI7fWk6MDthOjExOntzOjQ6InBhZ2UiO3M6MToiMSI7czoxMjoic3VibWl0X2V2ZW50IjtzOjY6InN1Ym1pdCI7czoxMToiZm9ybV9tZXRob2QiO3M6NDoiZmlsZSI7czoxMDoiYWN0aW9uX3VybCI7czowOiIiO3M6MTA6ImZvcm1fY2xhc3MiO3M6MTA6ImNocm9ub2Zvcm0iO3M6MTU6ImZvcm1fdGFnX2F0dGFjaCI7czowOiIiO3M6MjQ6InJlcXVpcmVkX2xhYmVsc19pZGVudGlmeSI7czoxOiIxIjtzOjEyOiJyZWxhdGl2ZV91cmwiO3M6MToiMSI7czoxMToiYWpheF9zdWJtaXQiO3M6MToiMCI7czoxMzoiYWRkX2Zvcm1fdGFncyI7czoxOiIxIjtzOjk6InhodG1sX3VybCI7czoxOiIwIjt9aTo1O2E6Mjp7czoxMjoiYWN0aW9uX2xhYmVsIjtzOjUwOiJGb3JtYXR0YXppb25lIHNlbGV0dG9yaSBjYXRlZ29yaWEgZSBzb3R0b2NhdGVnb3JpYSI7czo3OiJjb250ZW50IjtzOjI1ODoiPD9waHAgDQokYSA9ICd2ZW5kb3IvanMvZXZlbnRzZWFyaGZvcm0uanMnOw0KZWNobyAnPHNjcmlwdCBzcmM9IicuSnVyaTo6YmFzZSgpIC4gJGEuJyI+PC9zY3JpcHQ+JzsNCg0KJGNhdGlkID0gKGlzc2V0KCRmb3JtLT5kYXRhWydjYXRlZ29yaWEnXSkpID8gJGZvcm0tPmRhdGFbJ2NhdGVnb3JpYSddIDogLTE7DQogICANCj8+DQoNCjxzY3JpcHQ+DQppbml0X3N1Yl9jYXRlZ29yaWVzKCA8P3BocCBlY2hvICRjYXRpZDsgPz4gKTsNCjwvc2NyaXB0Pg0KIjt9aTozO2E6Mjp7czoxMjoiYWN0aW9uX2xhYmVsIjtzOjIyOiJTYWx2YXRhZ2dpbyBlIHJlZGlyZWN0IjtzOjc6ImNvbnRlbnQiO3M6MTg4OiI8P3BocA0KLy92YXJfZHVtcCgkX1BPU1QpOyBkaWUoKTsNCkpMb2FkZXI6OnJlZ2lzdGVyKCdFdmVudHNDb250cm9sbGVyJywgJ3ZlbmRvci9jb250cm9sL0V2ZW50c0NvbnRyb2xsZXIucGhwJyk7DQokY29udHJvbGxlciA9IG5ldyBFdmVudHNDb250cm9sbGVyKCk7DQokY29udHJvbGxlci0+c2F2ZVNlYXJjaFBhcmFtKCk7DQo/PiI7fX1zOjE0OiJkYl9maWVsZHNfbGlzdCI7YToxOntzOjEwOiJjYXRlZ29yaWVzIjtzOjA6IiI7fX0=', 1, 'Eventi', 1, '<div class="form-group gcore-form-row" id="form-row-status"><label for="status" class="control-label gcore-label-left">Stato</label>\n<div class="gcore-input gcore-display-table" id="fin-status"><select name="status" id="status" size="" class="form-control A" title="" style="" data-load-state="" data-tooltip="">\n<option value="">Non Impostato</option>\n<option value="0">Disabilitato</option>\n<option value="1">Abilitato</option>\n</select><span class="help-block">Sarà possibile accedere all''evento lato client solo se è selezionata l''opzione "Abilitato"</span></div></div><div class="form-group gcore-form-row" id="form-row-title"><label for="title" class="control-label gcore-label-left">Nome dell''evento</label>\n<div class="gcore-input gcore-display-table" id="fin-title"><input name="title" id="title" value="" placeholder="Titolo dell&#039;evento" maxlength="30" size="" class="form-control A" title="" style="" data-inputmask="" data-load-state="" data-tooltip="" type="text" /><span class="help-block">Inserire il titolo dell''evento</span></div></div><?php\n$keys = \\GCore\\Libs\\Arr::getVal(\\GCore\\Libs\\Arr::getVal($form->data, explode(".", "supercategorieslist")), explode(".", "[n].id"));\n$values = \\GCore\\Libs\\Arr::getVal(\\GCore\\Libs\\Arr::getVal($form->data, explode(".", "supercategorieslist")), explode(".", "[n].name"));\n$options = array_combine($keys, $values);\n$field = array (\n  ''name'' => ''supercategoria'',\n  ''id'' => ''supercategoria'',\n  ''options'' => \n  array (\n  ),\n  ''empty'' => ''Non Impostato'',\n  ''values'' => \n  array (\n  ),\n  ''label'' => \n  array (\n    ''text'' => ''Super Categoria'',\n    ''position'' => ''left'',\n  ),\n  ''sublabel'' => ''Filtro sulla super categorie e sulle sue sottocategorie'',\n  ''multiple'' => ''0'',\n  ''size'' => '''',\n  ''class'' => '''',\n  ''title'' => '''',\n  ''style'' => '''',\n  ''params'' => '''',\n  '':data-load-state'' => '''',\n  '':data-tooltip'' => '''',\n  ''type'' => ''dropdown'',\n  ''container_id'' => ''0'',\n);\n$field["options"] = $options;\necho \\GCore\\Helpers\\Html::formLine($field["name"], $field);\n?><?php\n$keys = \\GCore\\Libs\\Arr::getVal(\\GCore\\Libs\\Arr::getVal($form->data, explode(".", "categorieslist")), explode(".", "[n].id"));\n$values = \\GCore\\Libs\\Arr::getVal(\\GCore\\Libs\\Arr::getVal($form->data, explode(".", "categorieslist")), explode(".", "[n].name"));\n$options = array_combine($keys, $values);\n$field = array (\n  ''name'' => ''categoria'',\n  ''id'' => ''categoria'',\n  ''options'' => \n  array (\n  ),\n  ''empty'' => '''',\n  ''values'' => \n  array (\n  ),\n  ''label'' => \n  array (\n    ''text'' => ''Sotto Categoria'',\n    ''position'' => ''left'',\n  ),\n  ''sublabel'' => ''Se non viene selezionata una sottocategoria la ricerca vale per tutte le sottocategorie della supercategoria se selezionata'',\n  ''multiple'' => ''0'',\n  ''size'' => '''',\n  ''class'' => '''',\n  ''title'' => '''',\n  ''style'' => '''',\n  ''params'' => '''',\n  '':data-load-state'' => '''',\n  '':data-tooltip'' => '''',\n  ''type'' => ''dropdown'',\n  ''container_id'' => ''0'',\n);\n$field["options"] = $options;\necho \\GCore\\Helpers\\Html::formLine($field["name"], $field);\n?><div class="form-group gcore-form-row" id="form-row-dateinf"><label for="dateinf" class="control-label gcore-label-left">Dalla data</label>\n<div class="gcore-input gcore-display-table" id="fin-dateinf"><input name="dateinf" id="dateinf" value="" placeholder="" maxlength="30" size="" class="form-control A" title="" style="" data-inputmask="&#039;alias&#039; : &#039;dd-mm-yyyy&#039;" data-load-state="" data-tooltip="" type="text" /><span class="help-block">Limite inferiore dell''intervallo di tempo da selezionare</span></div></div><div class="form-group gcore-form-row" id="form-row-datesup"><label for="datesup" class="control-label gcore-label-left">Alla data</label>\n<div class="gcore-input gcore-display-table" id="fin-datesup"><input name="datesup" id="datesup" value="" placeholder="" maxlength="30" size="" class="form-control A" title="" style="" data-inputmask="&#039;alias&#039; : &#039;dd-mm-yyyy&#039;" data-load-state="" data-tooltip="" type="text" /><span class="help-block">Limite superiore dell''intervallo di tempo da considerare</span></div></div><div class="form-group gcore-form-row" id="form-row-sex"><label for="sex" class="control-label gcore-label-left">Sesso</label>\n<div class="gcore-input gcore-display-table" id="fin-sex"><select name="sex" id="sex" size="" class="form-control A" title="" style="" data-load-state="" data-tooltip="">\n<option value="">Non Impostato</option>\n<option value="0">Solo Uomini</option>\n<option value="1">Solo Donne</option>\n<option value="3">Donne e Uomini</option>\n</select><span class="help-block">Genere per cui un evento è pianificato</span></div></div><div class="form-group gcore-form-row" id="form-row-scope"><label for="scope" class="control-label gcore-label-left">Accesso</label>\n<div class="gcore-input gcore-display-table" id="fin-scope"><select name="scope" id="scope" size="" class="form-control A" title="" style="" data-load-state="" data-tooltip="">\n<option value="">Non Impostato</option>\n<option value="0">Pubblico</option>\n<option value="1">Privato</option>\n<option value="2">Friends Only</option>\n</select><span class="help-block">Selezionare la tipologia di evento</span></div></div><div class="form-group gcore-form-row" id="form-row-description"><label for="description" class="control-label gcore-label-left">Descrizione</label>\n<div class="gcore-input gcore-display-table" id="fin-description"><textarea name="description" id="description" placeholder="Es: Lorem ipsum, lorem, ipsum, token1, token" rows="5" cols="40" class="form-control A" title="" style="" data-wysiwyg="0" data-load-state="" data-tooltip=""></textarea><span class="help-block">Sequenza di token che possono trovarsi nella descrizione dell''evento separati da una virgola</span></div></div><div class="form-group gcore-form-row" id="form-row-town"><label for="town" class="control-label gcore-label-left">Città</label>\n<div class="gcore-input gcore-display-table" id="fin-town"><input name="town" id="town" value="" placeholder="Città" maxlength="40" size="" class="form-control A" title="" style="" data-inputmask="" data-load-state="" data-tooltip="" type="text" /><span class="help-block">Inserire il nome della città in cui si svolgerà l''evento</span></div></div><div class="form-group gcore-form-row" id="form-row-minpartecipant"><label for="minpartecipant" class="control-label gcore-label-left">Limite inferiore partecipanti</label>\n<div class="gcore-input gcore-display-table" id="fin-minpartecipant"><input name="minpartecipant" id="minpartecipant" value="" placeholder="" maxlength="3" size="" class="validate[&#039;number&#039;] form-control A" title="" style="" data-inputmask="&#039;alias&#039; : &#039;integer&#039;" data-load-state="" data-tooltip="" type="text" /><span class="help-block">Numero di partecipanti superiore al valore inserito</span></div></div><div class="form-group gcore-form-row" id="form-row-maxpartecipant"><label for="maxpartecipant" class="control-label gcore-label-left">Limite superiore partecipanti</label>\n<div class="gcore-input gcore-display-table" id="fin-maxpartecipant"><input name="maxpartecipant" id="maxpartecipant" value="" placeholder="" maxlength="3" size="" class="validate[&#039;number&#039;] form-control A" title="" style="" data-inputmask="&#039;alias&#039; : &#039;integer&#039;" data-load-state="" data-tooltip="" type="text" /><span class="help-block">Numero di partecipanti inferiore al valore inserito</span></div></div><div class="form-group gcore-form-row" id="form-row-minplace"><label for="minplace" class="control-label gcore-label-left">Limite inferiore posti disponibili</label>\n<div class="gcore-input gcore-display-table" id="fin-minplace"><input name="minplace" id="minplace" value="" placeholder="" maxlength="3" size="" class="validate[&#039;number&#039;] form-control A" title="" style="" data-inputmask="&#039;alias&#039; : &#039;integer&#039;" data-load-state="" data-tooltip="" type="text" /><span class="help-block">Numero di posti disponibili superiore al valore inserito</span></div></div><div class="form-group gcore-form-row" id="form-row-maxplace"><label for="maxplace" class="control-label gcore-label-left">Limite superiore posti disponibili</label>\n<div class="gcore-input gcore-display-table" id="fin-maxplace"><input name="maxplace" id="maxplace" value="" placeholder="" maxlength="3" size="" class="validate[&#039;number&#039;] form-control A" title="" style="" data-inputmask="&#039;alias&#039; : &#039;integer&#039;" data-load-state="" data-tooltip="" type="text" /><span class="help-block">Numero di posti disponibili inferiore al valore inserito</span></div></div><div class="form-group gcore-form-row" id="form-row-minagefrom"><label for="minagefrom" class="control-label gcore-label-left">Limite inferiore età minima</label>\n<div class="gcore-input gcore-display-table" id="fin-minagefrom"><input name="minagefrom" id="minagefrom" value="" placeholder="" maxlength="3" size="" class="validate[&#039;number&#039;] form-control A" title="" style="" data-inputmask="&#039;alias&#039; : &#039;integer&#039;" data-load-state="" data-tooltip="" type="text" /><span class="help-block">Età minima superiore al valore inserito</span></div></div><div class="form-group gcore-form-row" id="form-row-maxagefrom"><label for="maxagefrom" class="control-label gcore-label-left">Limite superiore età minima</label>\n<div class="gcore-input gcore-display-table" id="fin-maxagefrom"><input name="maxagefrom" id="maxagefrom" value="" placeholder="" maxlength="3" size="" class="validate[&#039;number&#039;] form-control A" title="" style="" data-inputmask="&#039;alias&#039; : &#039;integer&#039;" data-load-state="" data-tooltip="" type="text" /><span class="help-block">Età minima superiore al valore inserito</span></div></div><div class="form-group gcore-form-row" id="form-row-minageto"><label for="minageto" class="control-label gcore-label-left">Limite inferiore età massima</label>\n<div class="gcore-input gcore-display-table" id="fin-minageto"><input name="minageto" id="minageto" value="" placeholder="" maxlength="3" size="" class="validate[&#039;number&#039;] form-control A" title="" style="" data-inputmask="&#039;alias&#039; : &#039;integer&#039;" data-load-state="" data-tooltip="" type="text" /><span class="help-block">Età massima superiore al valore inserito</span></div></div><div class="form-group gcore-form-row" id="form-row-maxageto"><label for="maxageto" class="control-label gcore-label-left">Limite superiore età massima</label>\n<div class="gcore-input gcore-display-table" id="fin-maxageto"><input name="maxageto" id="maxageto" value="" placeholder="" maxlength="3" size="" class="validate[&#039;number&#039;] form-control A" title="" style="" data-inputmask="&#039;alias&#039; : &#039;integer&#039;" data-load-state="" data-tooltip="" type="text" /><span class="help-block">Età massima inferiore al valore inserito</span></div></div><div class="form-group gcore-form-row" id="form-row-button16"><div class="gcore-input gcore-display-table" id="fin-button16"><input name="button16" id="button16" type="submit" value="Cerca" class="btn btn-default form-control A" style="" data-load-state="" /></div></div>');

-- --------------------------------------------------------

--
-- Struttura della tabella `zcp8_chronoengine_extensions`
--

CREATE TABLE IF NOT EXISTS `zcp8_chronoengine_extensions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `addon_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `ordering` int(4) NOT NULL,
  `settings` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struttura della tabella `zcp8_contact_details`
--

CREATE TABLE IF NOT EXISTS `zcp8_contact_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `con_position` varchar(255) DEFAULT NULL,
  `address` text,
  `suburb` varchar(100) DEFAULT NULL,
  `state` varchar(100) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `postcode` varchar(100) DEFAULT NULL,
  `telephone` varchar(255) DEFAULT NULL,
  `fax` varchar(255) DEFAULT NULL,
  `misc` mediumtext,
  `image` varchar(255) DEFAULT NULL,
  `email_to` varchar(255) DEFAULT NULL,
  `default_con` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `params` text NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `catid` int(11) NOT NULL DEFAULT '0',
  `access` int(10) unsigned NOT NULL DEFAULT '0',
  `mobile` varchar(255) NOT NULL DEFAULT '',
  `webpage` varchar(255) NOT NULL DEFAULT '',
  `sortname1` varchar(255) NOT NULL,
  `sortname2` varchar(255) NOT NULL,
  `sortname3` varchar(255) NOT NULL,
  `language` char(7) NOT NULL,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) unsigned NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) unsigned NOT NULL DEFAULT '0',
  `metakey` text NOT NULL,
  `metadesc` text NOT NULL,
  `metadata` text NOT NULL,
  `featured` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'Set if article is featured.',
  `xreference` varchar(50) NOT NULL COMMENT 'A reference to enable linkages to external data sets.',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `version` int(10) unsigned NOT NULL DEFAULT '1',
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_access` (`access`),
  KEY `idx_checkout` (`checked_out`),
  KEY `idx_state` (`published`),
  KEY `idx_catid` (`catid`),
  KEY `idx_createdby` (`created_by`),
  KEY `idx_featured_catid` (`featured`,`catid`),
  KEY `idx_language` (`language`),
  KEY `idx_xreference` (`xreference`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struttura della tabella `zcp8_content`
--

CREATE TABLE IF NOT EXISTS `zcp8_content` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `asset_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'FK to the #__assets table.',
  `title` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `introtext` mediumtext NOT NULL,
  `fulltext` mediumtext NOT NULL,
  `state` tinyint(3) NOT NULL DEFAULT '0',
  `catid` int(10) unsigned NOT NULL DEFAULT '0',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) unsigned NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `images` text NOT NULL,
  `urls` text NOT NULL,
  `attribs` varchar(5120) NOT NULL,
  `version` int(10) unsigned NOT NULL DEFAULT '1',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `metakey` text NOT NULL,
  `metadesc` text NOT NULL,
  `access` int(10) unsigned NOT NULL DEFAULT '0',
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  `metadata` text NOT NULL,
  `featured` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'Set if article is featured.',
  `language` char(7) NOT NULL COMMENT 'The language code for the article.',
  `xreference` varchar(50) NOT NULL COMMENT 'A reference to enable linkages to external data sets.',
  PRIMARY KEY (`id`),
  KEY `idx_access` (`access`),
  KEY `idx_checkout` (`checked_out`),
  KEY `idx_state` (`state`),
  KEY `idx_catid` (`catid`),
  KEY `idx_createdby` (`created_by`),
  KEY `idx_featured_catid` (`featured`,`catid`),
  KEY `idx_language` (`language`),
  KEY `idx_xreference` (`xreference`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dump dei dati per la tabella `zcp8_content`
--

INSERT INTO `zcp8_content` (`id`, `asset_id`, `title`, `alias`, `introtext`, `fulltext`, `state`, `catid`, `created`, `created_by`, `created_by_alias`, `modified`, `modified_by`, `checked_out`, `checked_out_time`, `publish_up`, `publish_down`, `images`, `urls`, `attribs`, `version`, `ordering`, `metakey`, `metadesc`, `access`, `hits`, `metadata`, `featured`, `language`, `xreference`) VALUES
(1, 54, 'Gestione Eventi', 'gestione-eventi', '{source}\r\n<?php\r\n\r\nJLoader::register(''EventsController'', ''vendor/control/EventsController.php'');\r\n$eventsController = new EventsController();\r\n$eventsModel = new EventsModel();  \r\n\r\nswitch($eventsController->getShow()){\r\n  case ''list'':\r\n  \r\n    $eventsController->performListActions();\r\n  \r\n  	echo EventsView::listTitle();\r\n    echo EventsView::listInstruments();\r\n  \r\n    $eventsModel->loadFilter();\r\n    EventsView::$list = $eventsModel->select();\r\n  \r\n    echo EventsView::showlist();\r\n    break;\r\n  \r\n  case ''search'':\r\n  \r\n  	// passaggio del controller chronoforms \r\n    Travel::getInstance()->set(''eventController'', $eventsController);\r\n  \r\n  	echo EventsView::searchEventTitle();\r\n    echo EventsView::backToEventList();\r\n  \r\n    ?> {chronoforms5}event_search{/chronoforms5} <?php\r\n    \r\n    echo EventsView::backToEventList();\r\n    \r\n    break;\r\n  \r\n  case ''edit'':\r\n    $eventsController->loadMaps();\r\n  	$eventDetail = $eventsModel->getById(JRequest::getVar(''item''));\r\n  \r\n    // passaggio del controller e della model al chronoforms \r\n    Travel::getInstance()->set(''eventDetail'', $eventDetail);\r\n    Travel::getInstance()->set(''eventController'', $eventsController);\r\n    \r\n    echo EventsView::mainDetails($eventDetail);\r\n    echo EventsView::updateEventTitle();\r\n	\r\n  	?> {chronoforms5}event_update{/chronoforms5} <?php\r\n    \r\n    break;\r\n  \r\n  case ''partecipants'':\r\n    $idEvent = JRequest::getVar(''item'');\r\n    $eventsController->bokingListActions();\r\n  \r\n    echo EventsView::partecipateTitle($idEvent, $eventsModel->getById($idEvent)->title);\r\n    echo EventsView::partecipantsInstruments();\r\n\r\n    EventsView::$list = $eventsModel->partecipates($idEvent);\r\n    echo EventsView::showPartecipants();\r\n    break;\r\n  \r\n  default:\r\n  	JFactory::getApplication()->enqueueMessage(JText::_(''SOME_ERROR_OCCURRED''), ''error'');\r\n    break;\r\n}\r\n\r\n?>\r\n{/source}', '', 1, 2, '2015-05-19 15:55:29', 286, '', '2015-06-18 14:23:15', 286, 286, '2015-07-17 16:07:46', '2015-05-19 15:55:29', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":false,"urlatext":"","targeta":"","urlb":false,"urlbtext":"","targetb":"","urlc":false,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_tags":"","show_intro":"","info_block_position":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 56, 1, '', '', 1, 915, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(2, 55, 'Gestione Utenti', 'gestione-utenti', '{source}\r\n<?php\r\n\r\nJLoader::register(''UsersController'', ''vendor/control/UsersController.php'');\r\n$usersController = new UsersController();\r\n$usersModel = new UsersModel();  \r\n\r\nswitch($usersController->getShow()){\r\n  case ''list'':\r\n     \r\n    $usersController->performListActions();\r\n    $usersModel->loadFilter();\r\n    UsersView::$list = $usersModel->select();  \r\n  \r\n    echo UsersView::listTitle();\r\n    echo UsersView::listInstruments();\r\n    echo UsersView::showlist();\r\n  \r\n    break;\r\n  \r\n  case ''search'':\r\n   \r\n    // passaggio del controller chronoforms \r\n    Travel::getInstance()->set(''usersController'', $usersController);\r\n    \r\n    echo UsersView::searchUsersTitle();\r\n    echo UsersView::backToUsersList();\r\n  \r\n    if ($usersController->getAction() == ''reset''){\r\n    	$usersController->clearSearchParam();\r\n    }\r\n  \r\n    ?> {chronoforms5}user_search{/chronoforms5} <?php\r\n  \r\n    echo UsersView::backToUsersList();\r\n  \r\n    break;\r\n  \r\n  case ''edit'':\r\n     \r\n    $iduser = JRequest::getVar(''item'');\r\n  	$userDetail = $usersModel->getById($iduser);\r\n    Travel::getInstance()->set(''userDetail'', $userDetail);\r\n    Travel::getInstance()->set(''usersController'', $usersController);\r\n  \r\n  	if ($usersController->getAction() == ''defaultavatar''){\r\n      $usersController->setDefaultAvatar($iduser, $userDetail->img);\r\n      JFactory::getApplication()->redirect(JUri::base().''index.php/utenti?show=edit&item=''.$iduser, ''Impostato Avatar di default'', ''success'');\r\n    }\r\n  \r\n    echo UsersView::mainDetails($userDetail);\r\n    echo UsersView::updateUserTitle();\r\n  \r\n    ?> {chronoforms5}user_update{/chronoforms5} <?php\r\n    \r\n    break;\r\n  \r\n  case ''partecipates'':\r\n     \r\n    $iduser = JRequest::getVar(''item'');\r\n  	$userDetail = $usersModel->getById($iduser); \r\n    echo UsersView::bookingTitle($userDetail);\r\n    echo UsersView::bookingInstruments();\r\n  \r\n    $usersController->performBookingActions();\r\n    UsersView::$list = $usersModel->getUserBookings($iduser);\r\n    echo UsersView::showBooked();  \r\n  \r\n    break;\r\n  \r\n  case ''created'':\r\n    $iduser = JRequest::getVar(''item'');\r\n  	$userDetail = $usersModel->getById($iduser); \r\n    echo UsersView::createdTitle($userDetail);\r\n  \r\n  // prendiamo in prestito la lista degli eventi\r\n    JLoader::register(''EventsView'', ''vendor/view/events/EventsView.php'');\r\n    EventsView::$list = $usersModel->getEventsByPlanner($iduser);\r\n  	echo EventsView::showlist();	\r\n  \r\n    break;\r\n  \r\n  default:\r\n  	JFactory::getApplication()->enqueueMessage(JText::_(''SOME_ERROR_OCCURRED''), ''error'');\r\n    break;\r\n}\r\n\r\n?>\r\n{/source}', '', 1, 2, '2015-05-19 15:55:57', 286, '', '2015-07-09 15:45:01', 286, 0, '0000-00-00 00:00:00', '2015-05-19 15:55:57', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":false,"urlatext":"","targeta":"","urlb":false,"urlbtext":"","targetb":"","urlc":false,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_tags":"","show_intro":"","info_block_position":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 37, 0, '', '', 1, 474, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', '');

-- --------------------------------------------------------

--
-- Struttura della tabella `zcp8_contentitem_tag_map`
--

CREATE TABLE IF NOT EXISTS `zcp8_contentitem_tag_map` (
  `type_alias` varchar(255) NOT NULL DEFAULT '',
  `core_content_id` int(10) unsigned NOT NULL COMMENT 'PK from the core content table',
  `content_item_id` int(11) NOT NULL COMMENT 'PK from the content type table',
  `tag_id` int(10) unsigned NOT NULL COMMENT 'PK from the tag table',
  `tag_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Date of most recent save for this tag-item',
  `type_id` mediumint(8) NOT NULL COMMENT 'PK from the content_type table',
  UNIQUE KEY `uc_ItemnameTagid` (`type_id`,`content_item_id`,`tag_id`),
  KEY `idx_tag_type` (`tag_id`,`type_id`),
  KEY `idx_date_id` (`tag_date`,`tag_id`),
  KEY `idx_tag` (`tag_id`),
  KEY `idx_type` (`type_id`),
  KEY `idx_core_content_id` (`core_content_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Maps items from content tables to tags';

-- --------------------------------------------------------

--
-- Struttura della tabella `zcp8_content_frontpage`
--

CREATE TABLE IF NOT EXISTS `zcp8_content_frontpage` (
  `content_id` int(11) NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`content_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `zcp8_content_rating`
--

CREATE TABLE IF NOT EXISTS `zcp8_content_rating` (
  `content_id` int(11) NOT NULL DEFAULT '0',
  `rating_sum` int(10) unsigned NOT NULL DEFAULT '0',
  `rating_count` int(10) unsigned NOT NULL DEFAULT '0',
  `lastip` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`content_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `zcp8_content_types`
--

CREATE TABLE IF NOT EXISTS `zcp8_content_types` (
  `type_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type_title` varchar(255) NOT NULL DEFAULT '',
  `type_alias` varchar(255) NOT NULL DEFAULT '',
  `table` varchar(255) NOT NULL DEFAULT '',
  `rules` text NOT NULL,
  `field_mappings` text NOT NULL,
  `router` varchar(255) NOT NULL DEFAULT '',
  `content_history_options` varchar(5120) DEFAULT NULL COMMENT 'JSON string for com_contenthistory options',
  PRIMARY KEY (`type_id`),
  KEY `idx_alias` (`type_alias`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

--
-- Dump dei dati per la tabella `zcp8_content_types`
--

INSERT INTO `zcp8_content_types` (`type_id`, `type_title`, `type_alias`, `table`, `rules`, `field_mappings`, `router`, `content_history_options`) VALUES
(1, 'Article', 'com_content.article', '{"special":{"dbtable":"#__content","key":"id","type":"Content","prefix":"JTable","config":"array()"},"common":{"dbtable":"#__ucm_content","key":"ucm_id","type":"Corecontent","prefix":"JTable","config":"array()"}}', '', '{"common":{"core_content_item_id":"id","core_title":"title","core_state":"state","core_alias":"alias","core_created_time":"created","core_modified_time":"modified","core_body":"introtext", "core_hits":"hits","core_publish_up":"publish_up","core_publish_down":"publish_down","core_access":"access", "core_params":"attribs", "core_featured":"featured", "core_metadata":"metadata", "core_language":"language", "core_images":"images", "core_urls":"urls", "core_version":"version", "core_ordering":"ordering", "core_metakey":"metakey", "core_metadesc":"metadesc", "core_catid":"catid", "core_xreference":"xreference", "asset_id":"asset_id"}, "special":{"fulltext":"fulltext"}}', 'ContentHelperRoute::getArticleRoute', '{"formFile":"administrator\\/components\\/com_content\\/models\\/forms\\/article.xml", "hideFields":["asset_id","checked_out","checked_out_time","version"],"ignoreChanges":["modified_by", "modified", "checked_out", "checked_out_time", "version", "hits"],"convertToInt":["publish_up", "publish_down", "featured", "ordering"],"displayLookup":[{"sourceColumn":"catid","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"created_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"} ]}'),
(2, 'Weblink', 'com_weblinks.weblink', '{"special":{"dbtable":"#__weblinks","key":"id","type":"Weblink","prefix":"WeblinksTable","config":"array()"},"common":{"dbtable":"#__ucm_content","key":"ucm_id","type":"Corecontent","prefix":"JTable","config":"array()"}}', '', '{"common":{"core_content_item_id":"id","core_title":"title","core_state":"state","core_alias":"alias","core_created_time":"created","core_modified_time":"modified","core_body":"description", "core_hits":"hits","core_publish_up":"publish_up","core_publish_down":"publish_down","core_access":"access", "core_params":"params", "core_featured":"featured", "core_metadata":"metadata", "core_language":"language", "core_images":"images", "core_urls":"urls", "core_version":"version", "core_ordering":"ordering", "core_metakey":"metakey", "core_metadesc":"metadesc", "core_catid":"catid", "core_xreference":"xreference", "asset_id":"null"}, "special":{}}', 'WeblinksHelperRoute::getWeblinkRoute', '{"formFile":"administrator\\/components\\/com_weblinks\\/models\\/forms\\/weblink.xml", "hideFields":["asset_id","checked_out","checked_out_time","version","featured","images"], "ignoreChanges":["modified_by", "modified", "checked_out", "checked_out_time", "version", "hits"], "convertToInt":["publish_up", "publish_down", "featured", "ordering"], "displayLookup":[{"sourceColumn":"catid","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"created_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"} ]}'),
(3, 'Contact', 'com_contact.contact', '{"special":{"dbtable":"#__contact_details","key":"id","type":"Contact","prefix":"ContactTable","config":"array()"},"common":{"dbtable":"#__ucm_content","key":"ucm_id","type":"Corecontent","prefix":"JTable","config":"array()"}}', '', '{"common":{"core_content_item_id":"id","core_title":"name","core_state":"published","core_alias":"alias","core_created_time":"created","core_modified_time":"modified","core_body":"address", "core_hits":"hits","core_publish_up":"publish_up","core_publish_down":"publish_down","core_access":"access", "core_params":"params", "core_featured":"featured", "core_metadata":"metadata", "core_language":"language", "core_images":"image", "core_urls":"webpage", "core_version":"version", "core_ordering":"ordering", "core_metakey":"metakey", "core_metadesc":"metadesc", "core_catid":"catid", "core_xreference":"xreference", "asset_id":"null"}, "special":{"con_position":"con_position","suburb":"suburb","state":"state","country":"country","postcode":"postcode","telephone":"telephone","fax":"fax","misc":"misc","email_to":"email_to","default_con":"default_con","user_id":"user_id","mobile":"mobile","sortname1":"sortname1","sortname2":"sortname2","sortname3":"sortname3"}}', 'ContactHelperRoute::getContactRoute', '{"formFile":"administrator\\/components\\/com_contact\\/models\\/forms\\/contact.xml","hideFields":["default_con","checked_out","checked_out_time","version","xreference"],"ignoreChanges":["modified_by", "modified", "checked_out", "checked_out_time", "version", "hits"],"convertToInt":["publish_up", "publish_down", "featured", "ordering"], "displayLookup":[ {"sourceColumn":"created_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"catid","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"} ] }'),
(4, 'Newsfeed', 'com_newsfeeds.newsfeed', '{"special":{"dbtable":"#__newsfeeds","key":"id","type":"Newsfeed","prefix":"NewsfeedsTable","config":"array()"},"common":{"dbtable":"#__ucm_content","key":"ucm_id","type":"Corecontent","prefix":"JTable","config":"array()"}}', '', '{"common":{"core_content_item_id":"id","core_title":"name","core_state":"published","core_alias":"alias","core_created_time":"created","core_modified_time":"modified","core_body":"description", "core_hits":"hits","core_publish_up":"publish_up","core_publish_down":"publish_down","core_access":"access", "core_params":"params", "core_featured":"featured", "core_metadata":"metadata", "core_language":"language", "core_images":"images", "core_urls":"link", "core_version":"version", "core_ordering":"ordering", "core_metakey":"metakey", "core_metadesc":"metadesc", "core_catid":"catid", "core_xreference":"xreference", "asset_id":"null"}, "special":{"numarticles":"numarticles","cache_time":"cache_time","rtl":"rtl"}}', 'NewsfeedsHelperRoute::getNewsfeedRoute', '{"formFile":"administrator\\/components\\/com_newsfeeds\\/models\\/forms\\/newsfeed.xml","hideFields":["asset_id","checked_out","checked_out_time","version"],"ignoreChanges":["modified_by", "modified", "checked_out", "checked_out_time", "version", "hits"],"convertToInt":["publish_up", "publish_down", "featured", "ordering"],"displayLookup":[{"sourceColumn":"catid","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"created_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"} ]}'),
(5, 'User', 'com_users.user', '{"special":{"dbtable":"#__users","key":"id","type":"User","prefix":"JTable","config":"array()"},"common":{"dbtable":"#__ucm_content","key":"ucm_id","type":"Corecontent","prefix":"JTable","config":"array()"}}', '', '{"common":{"core_content_item_id":"id","core_title":"name","core_state":"null","core_alias":"username","core_created_time":"registerdate","core_modified_time":"lastvisitDate","core_body":"null", "core_hits":"null","core_publish_up":"null","core_publish_down":"null","access":"null", "core_params":"params", "core_featured":"null", "core_metadata":"null", "core_language":"null", "core_images":"null", "core_urls":"null", "core_version":"null", "core_ordering":"null", "core_metakey":"null", "core_metadesc":"null", "core_catid":"null", "core_xreference":"null", "asset_id":"null"}, "special":{}}', 'UsersHelperRoute::getUserRoute', ''),
(6, 'Article Category', 'com_content.category', '{"special":{"dbtable":"#__categories","key":"id","type":"Category","prefix":"JTable","config":"array()"},"common":{"dbtable":"#__ucm_content","key":"ucm_id","type":"Corecontent","prefix":"JTable","config":"array()"}}', '', '{"common":{"core_content_item_id":"id","core_title":"title","core_state":"published","core_alias":"alias","core_created_time":"created_time","core_modified_time":"modified_time","core_body":"description", "core_hits":"hits","core_publish_up":"null","core_publish_down":"null","core_access":"access", "core_params":"params", "core_featured":"null", "core_metadata":"metadata", "core_language":"language", "core_images":"null", "core_urls":"null", "core_version":"version", "core_ordering":"null", "core_metakey":"metakey", "core_metadesc":"metadesc", "core_catid":"parent_id", "core_xreference":"null", "asset_id":"asset_id"}, "special":{"parent_id":"parent_id","lft":"lft","rgt":"rgt","level":"level","path":"path","extension":"extension","note":"note"}}', 'ContentHelperRoute::getCategoryRoute', '{"formFile":"administrator\\/components\\/com_categories\\/models\\/forms\\/category.xml", "hideFields":["asset_id","checked_out","checked_out_time","version","lft","rgt","level","path","extension"], "ignoreChanges":["modified_user_id", "modified_time", "checked_out", "checked_out_time", "version", "hits", "path"],"convertToInt":["publish_up", "publish_down"], "displayLookup":[{"sourceColumn":"created_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"parent_id","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"}]}'),
(7, 'Contact Category', 'com_contact.category', '{"special":{"dbtable":"#__categories","key":"id","type":"Category","prefix":"JTable","config":"array()"},"common":{"dbtable":"#__ucm_content","key":"ucm_id","type":"Corecontent","prefix":"JTable","config":"array()"}}', '', '{"common":{"core_content_item_id":"id","core_title":"title","core_state":"published","core_alias":"alias","core_created_time":"created_time","core_modified_time":"modified_time","core_body":"description", "core_hits":"hits","core_publish_up":"null","core_publish_down":"null","core_access":"access", "core_params":"params", "core_featured":"null", "core_metadata":"metadata", "core_language":"language", "core_images":"null", "core_urls":"null", "core_version":"version", "core_ordering":"null", "core_metakey":"metakey", "core_metadesc":"metadesc", "core_catid":"parent_id", "core_xreference":"null", "asset_id":"asset_id"}, "special":{"parent_id":"parent_id","lft":"lft","rgt":"rgt","level":"level","path":"path","extension":"extension","note":"note"}}', 'ContactHelperRoute::getCategoryRoute', '{"formFile":"administrator\\/components\\/com_categories\\/models\\/forms\\/category.xml", "hideFields":["asset_id","checked_out","checked_out_time","version","lft","rgt","level","path","extension"], "ignoreChanges":["modified_user_id", "modified_time", "checked_out", "checked_out_time", "version", "hits", "path"],"convertToInt":["publish_up", "publish_down"], "displayLookup":[{"sourceColumn":"created_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"parent_id","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"}]}'),
(8, 'Newsfeeds Category', 'com_newsfeeds.category', '{"special":{"dbtable":"#__categories","key":"id","type":"Category","prefix":"JTable","config":"array()"},"common":{"dbtable":"#__ucm_content","key":"ucm_id","type":"Corecontent","prefix":"JTable","config":"array()"}}', '', '{"common":{"core_content_item_id":"id","core_title":"title","core_state":"published","core_alias":"alias","core_created_time":"created_time","core_modified_time":"modified_time","core_body":"description", "core_hits":"hits","core_publish_up":"null","core_publish_down":"null","core_access":"access", "core_params":"params", "core_featured":"null", "core_metadata":"metadata", "core_language":"language", "core_images":"null", "core_urls":"null", "core_version":"version", "core_ordering":"null", "core_metakey":"metakey", "core_metadesc":"metadesc", "core_catid":"parent_id", "core_xreference":"null", "asset_id":"asset_id"}, "special":{"parent_id":"parent_id","lft":"lft","rgt":"rgt","level":"level","path":"path","extension":"extension","note":"note"}}', 'NewsfeedsHelperRoute::getCategoryRoute', '{"formFile":"administrator\\/components\\/com_categories\\/models\\/forms\\/category.xml", "hideFields":["asset_id","checked_out","checked_out_time","version","lft","rgt","level","path","extension"], "ignoreChanges":["modified_user_id", "modified_time", "checked_out", "checked_out_time", "version", "hits", "path"],"convertToInt":["publish_up", "publish_down"], "displayLookup":[{"sourceColumn":"created_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"parent_id","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"}]}'),
(9, 'Weblinks Category', 'com_weblinks.category', '{"special":{"dbtable":"#__categories","key":"id","type":"Category","prefix":"JTable","config":"array()"},"common":{"dbtable":"#__ucm_content","key":"ucm_id","type":"Corecontent","prefix":"JTable","config":"array()"}}', '', '{"common":{"core_content_item_id":"id","core_title":"title","core_state":"published","core_alias":"alias","core_created_time":"created_time","core_modified_time":"modified_time","core_body":"description", "core_hits":"hits","core_publish_up":"null","core_publish_down":"null","core_access":"access", "core_params":"params", "core_featured":"null", "core_metadata":"metadata", "core_language":"language", "core_images":"null", "core_urls":"null", "core_version":"version", "core_ordering":"null", "core_metakey":"metakey", "core_metadesc":"metadesc", "core_catid":"parent_id", "core_xreference":"null", "asset_id":"asset_id"}, "special":{"parent_id":"parent_id","lft":"lft","rgt":"rgt","level":"level","path":"path","extension":"extension","note":"note"}}', 'WeblinksHelperRoute::getCategoryRoute', '{"formFile":"administrator\\/components\\/com_categories\\/models\\/forms\\/category.xml", "hideFields":["asset_id","checked_out","checked_out_time","version","lft","rgt","level","path","extension"], "ignoreChanges":["modified_user_id", "modified_time", "checked_out", "checked_out_time", "version", "hits", "path"],"convertToInt":["publish_up", "publish_down"], "displayLookup":[{"sourceColumn":"created_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"parent_id","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"}]}'),
(10, 'Tag', 'com_tags.tag', '{"special":{"dbtable":"#__tags","key":"tag_id","type":"Tag","prefix":"TagsTable","config":"array()"},"common":{"dbtable":"#__ucm_content","key":"ucm_id","type":"Corecontent","prefix":"JTable","config":"array()"}}', '', '{"common":{"core_content_item_id":"id","core_title":"title","core_state":"published","core_alias":"alias","core_created_time":"created_time","core_modified_time":"modified_time","core_body":"description", "core_hits":"hits","core_publish_up":"null","core_publish_down":"null","core_access":"access", "core_params":"params", "core_featured":"featured", "core_metadata":"metadata", "core_language":"language", "core_images":"images", "core_urls":"urls", "core_version":"version", "core_ordering":"null", "core_metakey":"metakey", "core_metadesc":"metadesc", "core_catid":"null", "core_xreference":"null", "asset_id":"null"}, "special":{"parent_id":"parent_id","lft":"lft","rgt":"rgt","level":"level","path":"path"}}', 'TagsHelperRoute::getTagRoute', '{"formFile":"administrator\\/components\\/com_tags\\/models\\/forms\\/tag.xml", "hideFields":["checked_out","checked_out_time","version", "lft", "rgt", "level", "path", "urls", "publish_up", "publish_down"],"ignoreChanges":["modified_user_id", "modified_time", "checked_out", "checked_out_time", "version", "hits", "path"],"convertToInt":["publish_up", "publish_down"], "displayLookup":[{"sourceColumn":"created_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"}, {"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"}, {"sourceColumn":"modified_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"}]}'),
(11, 'Banner', 'com_banners.banner', '{"special":{"dbtable":"#__banners","key":"id","type":"Banner","prefix":"BannersTable","config":"array()"},"common":{"dbtable":"#__ucm_content","key":"ucm_id","type":"Corecontent","prefix":"JTable","config":"array()"}}', '', '{"common":{"core_content_item_id":"id","core_title":"name","core_state":"published","core_alias":"alias","core_created_time":"created","core_modified_time":"modified","core_body":"description", "core_hits":"null","core_publish_up":"publish_up","core_publish_down":"publish_down","core_access":"access", "core_params":"params", "core_featured":"null", "core_metadata":"metadata", "core_language":"language", "core_images":"images", "core_urls":"link", "core_version":"version", "core_ordering":"ordering", "core_metakey":"metakey", "core_metadesc":"metadesc", "core_catid":"catid", "core_xreference":"null", "asset_id":"null"}, "special":{"imptotal":"imptotal", "impmade":"impmade", "clicks":"clicks", "clickurl":"clickurl", "custombannercode":"custombannercode", "cid":"cid", "purchase_type":"purchase_type", "track_impressions":"track_impressions", "track_clicks":"track_clicks"}}', '', '{"formFile":"administrator\\/components\\/com_banners\\/models\\/forms\\/banner.xml", "hideFields":["checked_out","checked_out_time","version", "reset"],"ignoreChanges":["modified_by", "modified", "checked_out", "checked_out_time", "version", "imptotal", "impmade", "reset"], "convertToInt":["publish_up", "publish_down", "ordering"], "displayLookup":[{"sourceColumn":"catid","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"}, {"sourceColumn":"cid","targetTable":"#__banner_clients","targetColumn":"id","displayColumn":"name"}, {"sourceColumn":"created_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"modified_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"} ]}'),
(12, 'Banners Category', 'com_banners.category', '{"special":{"dbtable":"#__categories","key":"id","type":"Category","prefix":"JTable","config":"array()"},"common":{"dbtable":"#__ucm_content","key":"ucm_id","type":"Corecontent","prefix":"JTable","config":"array()"}}', '', '{"common":{"core_content_item_id":"id","core_title":"title","core_state":"published","core_alias":"alias","core_created_time":"created_time","core_modified_time":"modified_time","core_body":"description", "core_hits":"hits","core_publish_up":"null","core_publish_down":"null","core_access":"access", "core_params":"params", "core_featured":"null", "core_metadata":"metadata", "core_language":"language", "core_images":"null", "core_urls":"null", "core_version":"version", "core_ordering":"null", "core_metakey":"metakey", "core_metadesc":"metadesc", "core_catid":"parent_id", "core_xreference":"null", "asset_id":"asset_id"}, "special": {"parent_id":"parent_id","lft":"lft","rgt":"rgt","level":"level","path":"path","extension":"extension","note":"note"}}', '', '{"formFile":"administrator\\/components\\/com_categories\\/models\\/forms\\/category.xml", "hideFields":["asset_id","checked_out","checked_out_time","version","lft","rgt","level","path","extension"], "ignoreChanges":["modified_user_id", "modified_time", "checked_out", "checked_out_time", "version", "hits", "path"], "convertToInt":["publish_up", "publish_down"], "displayLookup":[{"sourceColumn":"created_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"parent_id","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"}]}'),
(13, 'Banner Client', 'com_banners.client', '{"special":{"dbtable":"#__banner_clients","key":"id","type":"Client","prefix":"BannersTable"}}', '', '', '', '{"formFile":"administrator\\/components\\/com_banners\\/models\\/forms\\/client.xml", "hideFields":["checked_out","checked_out_time"], "ignoreChanges":["checked_out", "checked_out_time"], "convertToInt":[], "displayLookup":[]}'),
(14, 'User Notes', 'com_users.note', '{"special":{"dbtable":"#__user_notes","key":"id","type":"Note","prefix":"UsersTable"}}', '', '', '', '{"formFile":"administrator\\/components\\/com_users\\/models\\/forms\\/note.xml", "hideFields":["checked_out","checked_out_time", "publish_up", "publish_down"],"ignoreChanges":["modified_user_id", "modified_time", "checked_out", "checked_out_time"], "convertToInt":["publish_up", "publish_down"],"displayLookup":[{"sourceColumn":"catid","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"}, {"sourceColumn":"created_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"}, {"sourceColumn":"user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"}, {"sourceColumn":"modified_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"}]}'),
(15, 'User Notes Category', 'com_users.category', '{"special":{"dbtable":"#__categories","key":"id","type":"Category","prefix":"JTable","config":"array()"},"common":{"dbtable":"#__ucm_content","key":"ucm_id","type":"Corecontent","prefix":"JTable","config":"array()"}}', '', '{"common":{"core_content_item_id":"id","core_title":"title","core_state":"published","core_alias":"alias","core_created_time":"created_time","core_modified_time":"modified_time","core_body":"description", "core_hits":"hits","core_publish_up":"null","core_publish_down":"null","core_access":"access", "core_params":"params", "core_featured":"null", "core_metadata":"metadata", "core_language":"language", "core_images":"null", "core_urls":"null", "core_version":"version", "core_ordering":"null", "core_metakey":"metakey", "core_metadesc":"metadesc", "core_catid":"parent_id", "core_xreference":"null", "asset_id":"asset_id"}, "special":{"parent_id":"parent_id","lft":"lft","rgt":"rgt","level":"level","path":"path","extension":"extension","note":"note"}}', '', '{"formFile":"administrator\\/components\\/com_categories\\/models\\/forms\\/category.xml", "hideFields":["checked_out","checked_out_time","version","lft","rgt","level","path","extension"], "ignoreChanges":["modified_user_id", "modified_time", "checked_out", "checked_out_time", "version", "hits", "path"], "convertToInt":["publish_up", "publish_down"], "displayLookup":[{"sourceColumn":"created_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"}, {"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"parent_id","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"}]}');

-- --------------------------------------------------------

--
-- Struttura della tabella `zcp8_core_log_searches`
--

CREATE TABLE IF NOT EXISTS `zcp8_core_log_searches` (
  `search_term` varchar(128) NOT NULL DEFAULT '',
  `hits` int(10) unsigned NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `zcp8_extensions`
--

CREATE TABLE IF NOT EXISTS `zcp8_extensions` (
  `extension_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `type` varchar(20) NOT NULL,
  `element` varchar(100) NOT NULL,
  `folder` varchar(100) NOT NULL,
  `client_id` tinyint(3) NOT NULL,
  `enabled` tinyint(3) NOT NULL DEFAULT '1',
  `access` int(10) unsigned NOT NULL DEFAULT '1',
  `protected` tinyint(3) NOT NULL DEFAULT '0',
  `manifest_cache` text NOT NULL,
  `params` text NOT NULL,
  `custom_data` text NOT NULL,
  `system_data` text NOT NULL,
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) DEFAULT '0',
  `state` int(11) DEFAULT '0',
  PRIMARY KEY (`extension_id`),
  KEY `element_clientid` (`element`,`client_id`),
  KEY `element_folder_clientid` (`element`,`folder`,`client_id`),
  KEY `extension` (`type`,`element`,`folder`,`client_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10010 ;

--
-- Dump dei dati per la tabella `zcp8_extensions`
--

INSERT INTO `zcp8_extensions` (`extension_id`, `name`, `type`, `element`, `folder`, `client_id`, `enabled`, `access`, `protected`, `manifest_cache`, `params`, `custom_data`, `system_data`, `checked_out`, `checked_out_time`, `ordering`, `state`) VALUES
(1, 'com_mailto', 'component', 'com_mailto', '', 0, 1, 1, 1, '{"name":"com_mailto","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2014 Open Source Matters. All rights reserved.\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_MAILTO_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(2, 'com_wrapper', 'component', 'com_wrapper', '', 0, 1, 1, 1, '{"name":"com_wrapper","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2014 Open Source Matters. All rights reserved.\\n\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_WRAPPER_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(3, 'com_admin', 'component', 'com_admin', '', 1, 1, 1, 1, '{"name":"com_admin","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2014 Open Source Matters. All rights reserved.\\n\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_ADMIN_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(4, 'com_banners', 'component', 'com_banners', '', 1, 1, 1, 0, '{"name":"com_banners","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2014 Open Source Matters. All rights reserved.\\n\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_BANNERS_XML_DESCRIPTION","group":""}', '{"purchase_type":"3","track_impressions":"0","track_clicks":"0","metakey_prefix":"","save_history":"1","history_limit":10}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(5, 'com_cache', 'component', 'com_cache', '', 1, 1, 1, 1, '{"name":"com_cache","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_CACHE_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(6, 'com_categories', 'component', 'com_categories', '', 1, 1, 1, 1, '{"name":"com_categories","type":"component","creationDate":"December 2007","author":"Joomla! Project","copyright":"(C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_CATEGORIES_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(7, 'com_checkin', 'component', 'com_checkin', '', 1, 1, 1, 1, '{"name":"com_checkin","type":"component","creationDate":"Unknown","author":"Joomla! Project","copyright":"(C) 2005 - 2008 Open Source Matters. All rights reserved.\\n\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_CHECKIN_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(8, 'com_contact', 'component', 'com_contact', '', 1, 1, 1, 0, '{"name":"com_contact","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2014 Open Source Matters. All rights reserved.\\n\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_CONTACT_XML_DESCRIPTION","group":""}', '{"show_contact_category":"hide","save_history":"1","history_limit":10,"show_contact_list":"0","presentation_style":"sliders","show_name":"1","show_position":"1","show_email":"0","show_street_address":"1","show_suburb":"1","show_state":"1","show_postcode":"1","show_country":"1","show_telephone":"1","show_mobile":"1","show_fax":"1","show_webpage":"1","show_misc":"1","show_image":"1","image":"","allow_vcard":"0","show_articles":"0","show_profile":"0","show_links":"0","linka_name":"","linkb_name":"","linkc_name":"","linkd_name":"","linke_name":"","contact_icons":"0","icon_address":"","icon_email":"","icon_telephone":"","icon_mobile":"","icon_fax":"","icon_misc":"","show_headings":"1","show_position_headings":"1","show_email_headings":"0","show_telephone_headings":"1","show_mobile_headings":"0","show_fax_headings":"0","allow_vcard_headings":"0","show_suburb_headings":"1","show_state_headings":"1","show_country_headings":"1","show_email_form":"1","show_email_copy":"1","banned_email":"","banned_subject":"","banned_text":"","validate_session":"1","custom_reply":"0","redirect":"","show_category_crumb":"0","metakey":"","metadesc":"","robots":"","author":"","rights":"","xreference":""}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(9, 'com_cpanel', 'component', 'com_cpanel', '', 1, 1, 1, 1, '{"name":"com_cpanel","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_CPANEL_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10, 'com_installer', 'component', 'com_installer', '', 1, 1, 1, 1, '{"name":"com_installer","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2014 Open Source Matters. All rights reserved.\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_INSTALLER_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(11, 'com_languages', 'component', 'com_languages', '', 1, 1, 1, 1, '{"name":"com_languages","type":"component","creationDate":"2006","author":"Joomla! Project","copyright":"(C) 2005 - 2014 Open Source Matters. All rights reserved.\\n\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_LANGUAGES_XML_DESCRIPTION","group":""}', '{"administrator":"en-GB","site":"en-GB"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(12, 'com_login', 'component', 'com_login', '', 1, 1, 1, 1, '{"name":"com_login","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2014 Open Source Matters. All rights reserved.\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_LOGIN_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(13, 'com_media', 'component', 'com_media', '', 1, 1, 0, 1, '{"name":"com_media","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2014 Open Source Matters. All rights reserved.\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_MEDIA_XML_DESCRIPTION","group":""}', '{"upload_extensions":"bmp,csv,doc,gif,ico,jpg,jpeg,odg,odp,ods,odt,pdf,png,ppt,swf,txt,xcf,xls,BMP,CSV,DOC,GIF,ICO,JPG,JPEG,ODG,ODP,ODS,ODT,PDF,PNG,PPT,SWF,TXT,XCF,XLS","upload_maxsize":"10","file_path":"images","image_path":"images","restrict_uploads":"1","allowed_media_usergroup":"3","check_mime":"1","image_extensions":"bmp,gif,jpg,png","ignore_extensions":"","upload_mime":"image\\/jpeg,image\\/gif,image\\/png,image\\/bmp,application\\/x-shockwave-flash,application\\/msword,application\\/excel,application\\/pdf,application\\/powerpoint,text\\/plain,application\\/x-zip","upload_mime_illegal":"text\\/html"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(14, 'com_menus', 'component', 'com_menus', '', 1, 1, 1, 1, '{"name":"com_menus","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2014 Open Source Matters. All rights reserved.\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_MENUS_XML_DESCRIPTION","group":""}', '{"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":""}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(15, 'com_messages', 'component', 'com_messages', '', 1, 1, 1, 1, '{"name":"com_messages","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2014 Open Source Matters. All rights reserved.\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_MESSAGES_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(16, 'com_modules', 'component', 'com_modules', '', 1, 1, 1, 1, '{"name":"com_modules","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2014 Open Source Matters. All rights reserved.\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_MODULES_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(17, 'com_newsfeeds', 'component', 'com_newsfeeds', '', 1, 1, 1, 0, '{"name":"com_newsfeeds","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2014 Open Source Matters. All rights reserved.\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_NEWSFEEDS_XML_DESCRIPTION","group":""}', '{"newsfeed_layout":"_:default","save_history":"1","history_limit":5,"show_feed_image":"1","show_feed_description":"1","show_item_description":"1","feed_character_count":"0","feed_display_order":"des","float_first":"right","float_second":"right","show_tags":"1","category_layout":"_:default","show_category_title":"1","show_description":"1","show_description_image":"1","maxLevel":"-1","show_empty_categories":"0","show_subcat_desc":"1","show_cat_items":"1","show_cat_tags":"1","show_base_description":"1","maxLevelcat":"-1","show_empty_categories_cat":"0","show_subcat_desc_cat":"1","show_cat_items_cat":"1","filter_field":"1","show_pagination_limit":"1","show_headings":"1","show_articles":"0","show_link":"1","show_pagination":"1","show_pagination_results":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(18, 'com_plugins', 'component', 'com_plugins', '', 1, 1, 1, 1, '{"name":"com_plugins","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2014 Open Source Matters. All rights reserved.\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_PLUGINS_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(19, 'com_search', 'component', 'com_search', '', 1, 1, 1, 0, '{"name":"com_search","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2014 Open Source Matters. All rights reserved.\\n\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_SEARCH_XML_DESCRIPTION","group":""}', '{"enabled":"0","show_date":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(20, 'com_templates', 'component', 'com_templates', '', 1, 1, 1, 1, '{"name":"com_templates","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2014 Open Source Matters. All rights reserved.\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_TEMPLATES_XML_DESCRIPTION","group":""}', '{"template_positions_display":"0","upload_limit":"2","image_formats":"gif,bmp,jpg,jpeg,png","source_formats":"txt,less,ini,xml,js,php,css","font_formats":"woff,ttf,otf","compressed_formats":"zip"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(21, 'com_weblinks', 'component', 'com_weblinks', '', 1, 1, 1, 0, '{"name":"com_weblinks","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2014 Open Source Matters. All rights reserved.\\n\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_WEBLINKS_XML_DESCRIPTION","group":""}', '{"target":"0","save_history":"1","history_limit":5,"count_clicks":"1","icons":1,"link_icons":"","float_first":"right","float_second":"right","show_tags":"1","category_layout":"_:default","show_category_title":"1","show_description":"1","show_description_image":"1","maxLevel":"-1","show_empty_categories":"0","show_subcat_desc":"1","show_cat_num_links":"1","show_cat_tags":"1","show_base_description":"1","maxLevelcat":"-1","show_empty_categories_cat":"0","show_subcat_desc_cat":"1","show_cat_num_links_cat":"1","filter_field":"1","show_pagination_limit":"1","show_headings":"0","show_link_description":"1","show_link_hits":"1","show_pagination":"2","show_pagination_results":"1","show_feed_link":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(22, 'com_content', 'component', 'com_content', '', 1, 1, 0, 1, '{"name":"com_content","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2014 Open Source Matters. All rights reserved.\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_CONTENT_XML_DESCRIPTION","group":""}', '{"article_layout":"_:default","show_title":"1","link_titles":"0","show_intro":"0","info_block_position":"0","show_category":"0","link_category":"0","show_parent_category":"0","link_parent_category":"0","show_author":"0","link_author":"0","show_create_date":"0","show_modify_date":"0","show_publish_date":"0","show_item_navigation":"0","show_vote":"0","show_readmore":"0","show_readmore_title":"0","readmore_limit":"100","show_tags":"0","show_icons":"0","show_print_icon":"0","show_email_icon":"0","show_hits":"0","show_noauth":"0","urls_position":"0","show_publishing_options":"1","show_article_options":"1","save_history":"1","history_limit":10,"show_urls_images_frontend":"0","show_urls_images_backend":"1","targeta":0,"targetb":0,"targetc":0,"float_intro":"left","float_fulltext":"left","category_layout":"_:blog","show_category_heading_title_text":"1","show_category_title":"0","show_description":"0","show_description_image":"0","maxLevel":"1","show_empty_categories":"0","show_no_articles":"1","show_subcat_desc":"1","show_cat_num_articles":"0","show_base_description":"1","maxLevelcat":"-1","show_empty_categories_cat":"0","show_subcat_desc_cat":"1","show_cat_num_articles_cat":"1","num_leading_articles":"1","num_intro_articles":"4","num_columns":"2","num_links":"4","multi_column_order":"0","show_subcategory_content":"0","show_pagination_limit":"1","filter_field":"hide","show_headings":"1","list_show_date":"0","date_format":"","list_show_hits":"1","list_show_author":"1","orderby_pri":"order","orderby_sec":"rdate","order_date":"published","show_pagination":"2","show_pagination_results":"1","show_feed_link":"1","feed_summary":"0","feed_show_readmore":"0"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(23, 'com_config', 'component', 'com_config', '', 1, 1, 0, 1, '{"name":"com_config","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_CONFIG_XML_DESCRIPTION","group":""}', '{"filters":{"1":{"filter_type":"NH","filter_tags":"","filter_attributes":""},"9":{"filter_type":"BL","filter_tags":"","filter_attributes":""},"6":{"filter_type":"BL","filter_tags":"","filter_attributes":""},"7":{"filter_type":"NONE","filter_tags":"","filter_attributes":""},"2":{"filter_type":"NH","filter_tags":"","filter_attributes":""},"3":{"filter_type":"BL","filter_tags":"","filter_attributes":""},"4":{"filter_type":"BL","filter_tags":"","filter_attributes":""},"5":{"filter_type":"BL","filter_tags":"","filter_attributes":""},"8":{"filter_type":"NONE","filter_tags":"","filter_attributes":""}}}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(24, 'com_redirect', 'component', 'com_redirect', '', 1, 1, 0, 1, '{"name":"com_redirect","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2014 Open Source Matters. All rights reserved.\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_REDIRECT_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(25, 'com_users', 'component', 'com_users', '', 1, 1, 0, 1, '{"name":"com_users","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2014 Open Source Matters. All rights reserved.\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_USERS_XML_DESCRIPTION","group":""}', '{"allowUserRegistration":"0","new_usertype":"2","guest_usergroup":"9","sendpassword":"1","useractivation":"1","mail_to_admin":"1","captcha":"","frontend_userparams":"1","site_language":"0","change_login_name":"0","reset_count":"10","reset_time":"1","minimum_length":"4","minimum_integers":"0","minimum_symbols":"0","minimum_uppercase":"0","save_history":"1","history_limit":5,"mailSubjectPrefix":"","mailBodySuffix":""}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(27, 'com_finder', 'component', 'com_finder', '', 1, 1, 0, 0, '{"name":"com_finder","type":"component","creationDate":"August 2011","author":"Joomla! Project","copyright":"(C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_FINDER_XML_DESCRIPTION","group":""}', '{"show_description":"1","description_length":255,"allow_empty_query":"0","show_url":"1","show_advanced":"1","expand_advanced":"0","show_date_filters":"0","highlight_terms":"1","opensearch_name":"","opensearch_description":"","batch_size":"50","memory_table_limit":30000,"title_multiplier":"1.7","text_multiplier":"0.7","meta_multiplier":"1.2","path_multiplier":"2.0","misc_multiplier":"0.3","stemmer":"snowball"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(28, 'com_joomlaupdate', 'component', 'com_joomlaupdate', '', 1, 1, 0, 1, '{"name":"com_joomlaupdate","type":"component","creationDate":"February 2012","author":"Joomla! Project","copyright":"(C) 2005 - 2014 Open Source Matters. All rights reserved.\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_JOOMLAUPDATE_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(29, 'com_tags', 'component', 'com_tags', '', 1, 1, 1, 1, '{"name":"com_tags","type":"component","creationDate":"December 2013","author":"Joomla! Project","copyright":"(C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.1.0","description":"COM_TAGS_XML_DESCRIPTION","group":""}', '{"tag_layout":"_:default","save_history":"1","history_limit":5,"show_tag_title":"0","tag_list_show_tag_image":"0","tag_list_show_tag_description":"0","tag_list_image":"","show_tag_num_items":"0","tag_list_orderby":"title","tag_list_orderby_direction":"ASC","show_headings":"0","tag_list_show_date":"0","tag_list_show_item_image":"0","tag_list_show_item_description":"0","tag_list_item_maximum_characters":0,"return_any_or_all":"1","include_children":"0","maximum":200,"tag_list_language_filter":"all","tags_layout":"_:default","all_tags_orderby":"title","all_tags_orderby_direction":"ASC","all_tags_show_tag_image":"0","all_tags_show_tag_descripion":"0","all_tags_tag_maximum_characters":20,"all_tags_show_tag_hits":"0","filter_field":"1","show_pagination_limit":"1","show_pagination":"2","show_pagination_results":"1","tag_field_ajax_mode":"1","show_feed_link":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(30, 'com_contenthistory', 'component', 'com_contenthistory', '', 1, 1, 1, 0, '{"name":"com_contenthistory","type":"component","creationDate":"May 2013","author":"Joomla! Project","copyright":"(C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.2.0","description":"COM_CONTENTHISTORY_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(31, 'com_ajax', 'component', 'com_ajax', '', 1, 1, 1, 0, '{"name":"com_ajax","type":"component","creationDate":"August 2013","author":"Joomla! Project","copyright":"(C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.2.0","description":"COM_AJAX_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(32, 'com_postinstall', 'component', 'com_postinstall', '', 1, 1, 1, 1, '{"name":"com_postinstall","type":"component","creationDate":"September 2013","author":"Joomla! Project","copyright":"(C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.2.0","description":"COM_POSTINSTALL_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(100, 'PHPMailer', 'library', 'phpmailer', '', 0, 1, 1, 1, '{"name":"PHPMailer","type":"library","creationDate":"2001","author":"PHPMailer","copyright":"(c) 2001-2003, Brent R. Matzelle, (c) 2004-2009, Andy Prevost. All Rights Reserved., (c) 2010-2013, Jim Jagielski. All Rights Reserved.","authorEmail":"jimjag@gmail.com","authorUrl":"https:\\/\\/github.com\\/PHPMailer\\/PHPMailer","version":"5.2.6","description":"LIB_PHPMAILER_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(101, 'SimplePie', 'library', 'simplepie', '', 0, 1, 1, 1, '{"name":"SimplePie","type":"library","creationDate":"2004","author":"SimplePie","copyright":"Copyright (c) 2004-2009, Ryan Parman and Geoffrey Sneddon","authorEmail":"","authorUrl":"http:\\/\\/simplepie.org\\/","version":"1.2","description":"LIB_SIMPLEPIE_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(102, 'phputf8', 'library', 'phputf8', '', 0, 1, 1, 1, '{"name":"phputf8","type":"library","creationDate":"2006","author":"Harry Fuecks","copyright":"Copyright various authors","authorEmail":"hfuecks@gmail.com","authorUrl":"http:\\/\\/sourceforge.net\\/projects\\/phputf8","version":"0.5","description":"LIB_PHPUTF8_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(103, 'Joomla! Platform', 'library', 'joomla', '', 0, 1, 1, 1, '{"name":"Joomla! Platform","type":"library","creationDate":"2008","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"http:\\/\\/www.joomla.org","version":"13.1","description":"LIB_JOOMLA_XML_DESCRIPTION","group":""}', '{"mediaversion":"27e01bd83e7b9249949ef8189daf3058"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(104, 'IDNA Convert', 'library', 'idna_convert', '', 0, 1, 1, 1, '{"name":"IDNA Convert","type":"library","creationDate":"2004","author":"phlyLabs","copyright":"2004-2011 phlyLabs Berlin, http:\\/\\/phlylabs.de","authorEmail":"phlymail@phlylabs.de","authorUrl":"http:\\/\\/phlylabs.de","version":"0.8.0","description":"LIB_IDNA_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(105, 'FOF', 'library', 'fof', '', 0, 1, 1, 1, '{"name":"FOF","type":"library","creationDate":"2014-03-09 12:54:48","author":"Nicholas K. Dionysopoulos \\/ Akeeba Ltd","copyright":"(C)2011-2014 Nicholas K. Dionysopoulos","authorEmail":"nicholas@akeebabackup.com","authorUrl":"https:\\/\\/www.akeebabackup.com","version":"2.2.1","description":"LIB_FOF_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(106, 'PHPass', 'library', 'phpass', '', 0, 1, 1, 1, '{"name":"PHPass","type":"library","creationDate":"2004-2006","author":"Solar Designer","copyright":"","authorEmail":"solar@openwall.com","authorUrl":"http:\\/\\/www.openwall.com\\/phpass\\/","version":"0.3","description":"LIB_PHPASS_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(200, 'mod_articles_archive', 'module', 'mod_articles_archive', '', 0, 1, 1, 0, '{"name":"mod_articles_archive","type":"module","creationDate":"July 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters.\\n\\t\\tAll rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_ARTICLES_ARCHIVE_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(201, 'mod_articles_latest', 'module', 'mod_articles_latest', '', 0, 1, 1, 0, '{"name":"mod_articles_latest","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_LATEST_NEWS_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(202, 'mod_articles_popular', 'module', 'mod_articles_popular', '', 0, 1, 1, 0, '{"name":"mod_articles_popular","type":"module","creationDate":"July 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_POPULAR_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(203, 'mod_banners', 'module', 'mod_banners', '', 0, 1, 1, 0, '{"name":"mod_banners","type":"module","creationDate":"July 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_BANNERS_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(204, 'mod_breadcrumbs', 'module', 'mod_breadcrumbs', '', 0, 1, 1, 1, '{"name":"mod_breadcrumbs","type":"module","creationDate":"July 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_BREADCRUMBS_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(205, 'mod_custom', 'module', 'mod_custom', '', 0, 1, 1, 1, '{"name":"mod_custom","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_CUSTOM_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(206, 'mod_feed', 'module', 'mod_feed', '', 0, 1, 1, 0, '{"name":"mod_feed","type":"module","creationDate":"July 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_FEED_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(207, 'mod_footer', 'module', 'mod_footer', '', 0, 1, 1, 0, '{"name":"mod_footer","type":"module","creationDate":"July 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_FOOTER_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(208, 'mod_login', 'module', 'mod_login', '', 0, 1, 1, 1, '{"name":"mod_login","type":"module","creationDate":"July 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_LOGIN_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(209, 'mod_menu', 'module', 'mod_menu', '', 0, 1, 1, 1, '{"name":"mod_menu","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_MENU_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(210, 'mod_articles_news', 'module', 'mod_articles_news', '', 0, 1, 1, 0, '{"name":"mod_articles_news","type":"module","creationDate":"July 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_ARTICLES_NEWS_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(211, 'mod_random_image', 'module', 'mod_random_image', '', 0, 1, 1, 0, '{"name":"mod_random_image","type":"module","creationDate":"July 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_RANDOM_IMAGE_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(212, 'mod_related_items', 'module', 'mod_related_items', '', 0, 1, 1, 0, '{"name":"mod_related_items","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_RELATED_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(213, 'mod_search', 'module', 'mod_search', '', 0, 1, 1, 0, '{"name":"mod_search","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_SEARCH_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(214, 'mod_stats', 'module', 'mod_stats', '', 0, 1, 1, 0, '{"name":"mod_stats","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_STATS_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(215, 'mod_syndicate', 'module', 'mod_syndicate', '', 0, 1, 1, 1, '{"name":"mod_syndicate","type":"module","creationDate":"May 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_SYNDICATE_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(216, 'mod_users_latest', 'module', 'mod_users_latest', '', 0, 1, 1, 0, '{"name":"mod_users_latest","type":"module","creationDate":"December 2009","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_USERS_LATEST_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(217, 'mod_weblinks', 'module', 'mod_weblinks', '', 0, 1, 1, 0, '{"name":"mod_weblinks","type":"module","creationDate":"July 2009","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_WEBLINKS_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(218, 'mod_whosonline', 'module', 'mod_whosonline', '', 0, 1, 1, 0, '{"name":"mod_whosonline","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_WHOSONLINE_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(219, 'mod_wrapper', 'module', 'mod_wrapper', '', 0, 1, 1, 0, '{"name":"mod_wrapper","type":"module","creationDate":"October 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_WRAPPER_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(220, 'mod_articles_category', 'module', 'mod_articles_category', '', 0, 1, 1, 0, '{"name":"mod_articles_category","type":"module","creationDate":"February 2010","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_ARTICLES_CATEGORY_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(221, 'mod_articles_categories', 'module', 'mod_articles_categories', '', 0, 1, 1, 0, '{"name":"mod_articles_categories","type":"module","creationDate":"February 2010","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_ARTICLES_CATEGORIES_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(222, 'mod_languages', 'module', 'mod_languages', '', 0, 1, 1, 1, '{"name":"mod_languages","type":"module","creationDate":"February 2010","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_LANGUAGES_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(223, 'mod_finder', 'module', 'mod_finder', '', 0, 1, 0, 0, '{"name":"mod_finder","type":"module","creationDate":"August 2011","author":"Joomla! Project","copyright":"(C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_FINDER_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(300, 'mod_custom', 'module', 'mod_custom', '', 1, 1, 1, 1, '{"name":"mod_custom","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_CUSTOM_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(301, 'mod_feed', 'module', 'mod_feed', '', 1, 1, 1, 0, '{"name":"mod_feed","type":"module","creationDate":"July 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_FEED_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(302, 'mod_latest', 'module', 'mod_latest', '', 1, 1, 1, 0, '{"name":"mod_latest","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_LATEST_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(303, 'mod_logged', 'module', 'mod_logged', '', 1, 1, 1, 0, '{"name":"mod_logged","type":"module","creationDate":"January 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_LOGGED_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(304, 'mod_login', 'module', 'mod_login', '', 1, 1, 1, 1, '{"name":"mod_login","type":"module","creationDate":"March 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_LOGIN_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(305, 'mod_menu', 'module', 'mod_menu', '', 1, 1, 1, 0, '{"name":"mod_menu","type":"module","creationDate":"March 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_MENU_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(307, 'mod_popular', 'module', 'mod_popular', '', 1, 1, 1, 0, '{"name":"mod_popular","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_POPULAR_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(308, 'mod_quickicon', 'module', 'mod_quickicon', '', 1, 1, 1, 1, '{"name":"mod_quickicon","type":"module","creationDate":"Nov 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_QUICKICON_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(309, 'mod_status', 'module', 'mod_status', '', 1, 1, 1, 0, '{"name":"mod_status","type":"module","creationDate":"Feb 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_STATUS_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(310, 'mod_submenu', 'module', 'mod_submenu', '', 1, 1, 1, 0, '{"name":"mod_submenu","type":"module","creationDate":"Feb 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_SUBMENU_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(311, 'mod_title', 'module', 'mod_title', '', 1, 1, 1, 0, '{"name":"mod_title","type":"module","creationDate":"Nov 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_TITLE_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(312, 'mod_toolbar', 'module', 'mod_toolbar', '', 1, 1, 1, 1, '{"name":"mod_toolbar","type":"module","creationDate":"Nov 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_TOOLBAR_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(313, 'mod_multilangstatus', 'module', 'mod_multilangstatus', '', 1, 1, 1, 0, '{"name":"mod_multilangstatus","type":"module","creationDate":"September 2011","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_MULTILANGSTATUS_XML_DESCRIPTION","group":""}', '{"cache":"0"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(314, 'mod_version', 'module', 'mod_version', '', 1, 1, 1, 0, '{"name":"mod_version","type":"module","creationDate":"January 2012","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_VERSION_XML_DESCRIPTION","group":""}', '{"format":"short","product":"1","cache":"0"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(315, 'mod_stats_admin', 'module', 'mod_stats_admin', '', 1, 1, 1, 0, '{"name":"mod_stats_admin","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_STATS_XML_DESCRIPTION","group":""}', '{"serverinfo":"0","siteinfo":"0","counter":"0","increase":"0","cache":"1","cache_time":"900","cachemode":"static"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(316, 'mod_tags_popular', 'module', 'mod_tags_popular', '', 0, 1, 1, 0, '{"name":"mod_tags_popular","type":"module","creationDate":"January 2013","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.1.0","description":"MOD_TAGS_POPULAR_XML_DESCRIPTION","group":""}', '{"maximum":"5","timeframe":"alltime","owncache":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(317, 'mod_tags_similar', 'module', 'mod_tags_similar', '', 0, 1, 1, 0, '{"name":"mod_tags_similar","type":"module","creationDate":"January 2013","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.1.0","description":"MOD_TAGS_SIMILAR_XML_DESCRIPTION","group":""}', '{"maximum":"5","matchtype":"any","owncache":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(400, 'plg_authentication_gmail', 'plugin', 'gmail', 'authentication', 0, 0, 1, 0, '{"name":"plg_authentication_gmail","type":"plugin","creationDate":"February 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_GMAIL_XML_DESCRIPTION","group":""}', '{"applysuffix":"0","suffix":"","verifypeer":"1","user_blacklist":""}', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(401, 'plg_authentication_joomla', 'plugin', 'joomla', 'authentication', 0, 1, 1, 1, '{"name":"plg_authentication_joomla","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_AUTH_JOOMLA_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(402, 'plg_authentication_ldap', 'plugin', 'ldap', 'authentication', 0, 0, 1, 0, '{"name":"plg_authentication_ldap","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_LDAP_XML_DESCRIPTION","group":""}', '{"host":"","port":"389","use_ldapV3":"0","negotiate_tls":"0","no_referrals":"0","auth_method":"bind","base_dn":"","search_string":"","users_dn":"","username":"admin","password":"bobby7","ldap_fullname":"fullName","ldap_email":"mail","ldap_uid":"uid"}', '', '', 0, '0000-00-00 00:00:00', 3, 0),
(403, 'plg_content_contact', 'plugin', 'contact', 'content', 0, 1, 1, 0, '{"name":"plg_content_contact","type":"plugin","creationDate":"January 2014","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.2.2","description":"PLG_CONTENT_CONTACT_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(404, 'plg_content_emailcloak', 'plugin', 'emailcloak', 'content', 0, 1, 1, 0, '{"name":"plg_content_emailcloak","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_CONTENT_EMAILCLOAK_XML_DESCRIPTION","group":""}', '{"mode":"1"}', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(406, 'plg_content_loadmodule', 'plugin', 'loadmodule', 'content', 0, 1, 1, 0, '{"name":"plg_content_loadmodule","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_LOADMODULE_XML_DESCRIPTION","group":""}', '{"style":"xhtml"}', '', '', 0, '2011-09-18 15:22:50', 0, 0),
(407, 'plg_content_pagebreak', 'plugin', 'pagebreak', 'content', 0, 1, 1, 0, '{"name":"plg_content_pagebreak","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_CONTENT_PAGEBREAK_XML_DESCRIPTION","group":""}', '{"title":"1","multipage_toc":"1","showall":"1"}', '', '', 0, '0000-00-00 00:00:00', 4, 0),
(408, 'plg_content_pagenavigation', 'plugin', 'pagenavigation', 'content', 0, 1, 1, 0, '{"name":"plg_content_pagenavigation","type":"plugin","creationDate":"January 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_PAGENAVIGATION_XML_DESCRIPTION","group":""}', '{"position":"1"}', '', '', 0, '0000-00-00 00:00:00', 5, 0),
(409, 'plg_content_vote', 'plugin', 'vote', 'content', 0, 1, 1, 0, '{"name":"plg_content_vote","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_VOTE_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 6, 0),
(410, 'plg_editors_codemirror', 'plugin', 'codemirror', 'editors', 0, 1, 1, 1, '{"name":"plg_editors_codemirror","type":"plugin","creationDate":"28 March 2011","author":"Marijn Haverbeke","copyright":"","authorEmail":"N\\/A","authorUrl":"","version":"3.15","description":"PLG_CODEMIRROR_XML_DESCRIPTION","group":""}', '{"lineNumbers":"1","lineWrapping":"1","matchTags":"1","matchBrackets":"1","marker-gutter":"1","autoCloseTags":"1","autoCloseBrackets":"1","autoFocus":"1","theme":"default","tabmode":"indent"}', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(411, 'plg_editors_none', 'plugin', 'none', 'editors', 0, 1, 1, 1, '{"name":"plg_editors_none","type":"plugin","creationDate":"August 2004","author":"Unknown","copyright":"","authorEmail":"N\\/A","authorUrl":"","version":"3.0.0","description":"PLG_NONE_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 2, 0),
(412, 'plg_editors_tinymce', 'plugin', 'tinymce', 'editors', 0, 1, 1, 0, '{"name":"plg_editors_tinymce","type":"plugin","creationDate":"2005-2013","author":"Moxiecode Systems AB","copyright":"Moxiecode Systems AB","authorEmail":"N\\/A","authorUrl":"tinymce.moxiecode.com","version":"4.0.22","description":"PLG_TINY_XML_DESCRIPTION","group":""}', '{"mode":"1","skin":"0","mobile":"0","entity_encoding":"raw","lang_mode":"1","text_direction":"ltr","content_css":"1","content_css_custom":"","relative_urls":"1","newlines":"0","invalid_elements":"script,applet,iframe","extended_elements":"","html_height":"550","html_width":"750","resizing":"1","element_path":"1","fonts":"1","paste":"1","searchreplace":"1","insertdate":"1","colors":"1","table":"1","smilies":"1","hr":"1","link":"1","media":"1","print":"1","directionality":"1","fullscreen":"1","alignment":"1","visualchars":"1","visualblocks":"1","nonbreaking":"1","template":"1","blockquote":"1","wordcount":"1","advlist":"1","autosave":"1","contextmenu":"1","inlinepopups":"1","custom_plugin":"","custom_button":""}', '', '', 0, '0000-00-00 00:00:00', 3, 0),
(413, 'plg_editors-xtd_article', 'plugin', 'article', 'editors-xtd', 0, 1, 1, 1, '{"name":"plg_editors-xtd_article","type":"plugin","creationDate":"October 2009","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_ARTICLE_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(414, 'plg_editors-xtd_image', 'plugin', 'image', 'editors-xtd', 0, 1, 1, 0, '{"name":"plg_editors-xtd_image","type":"plugin","creationDate":"August 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_IMAGE_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 2, 0),
(415, 'plg_editors-xtd_pagebreak', 'plugin', 'pagebreak', 'editors-xtd', 0, 1, 1, 0, '{"name":"plg_editors-xtd_pagebreak","type":"plugin","creationDate":"August 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_EDITORSXTD_PAGEBREAK_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 3, 0),
(416, 'plg_editors-xtd_readmore', 'plugin', 'readmore', 'editors-xtd', 0, 1, 1, 0, '{"name":"plg_editors-xtd_readmore","type":"plugin","creationDate":"March 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_READMORE_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 4, 0);
INSERT INTO `zcp8_extensions` (`extension_id`, `name`, `type`, `element`, `folder`, `client_id`, `enabled`, `access`, `protected`, `manifest_cache`, `params`, `custom_data`, `system_data`, `checked_out`, `checked_out_time`, `ordering`, `state`) VALUES
(417, 'plg_search_categories', 'plugin', 'categories', 'search', 0, 1, 1, 0, '{"name":"plg_search_categories","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_SEARCH_CATEGORIES_XML_DESCRIPTION","group":""}', '{"search_limit":"50","search_content":"1","search_archived":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(418, 'plg_search_contacts', 'plugin', 'contacts', 'search', 0, 1, 1, 0, '{"name":"plg_search_contacts","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_SEARCH_CONTACTS_XML_DESCRIPTION","group":""}', '{"search_limit":"50","search_content":"1","search_archived":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(419, 'plg_search_content', 'plugin', 'content', 'search', 0, 1, 1, 0, '{"name":"plg_search_content","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_SEARCH_CONTENT_XML_DESCRIPTION","group":""}', '{"search_limit":"50","search_content":"1","search_archived":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(420, 'plg_search_newsfeeds', 'plugin', 'newsfeeds', 'search', 0, 1, 1, 0, '{"name":"plg_search_newsfeeds","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_SEARCH_NEWSFEEDS_XML_DESCRIPTION","group":""}', '{"search_limit":"50","search_content":"1","search_archived":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(421, 'plg_search_weblinks', 'plugin', 'weblinks', 'search', 0, 1, 1, 0, '{"name":"plg_search_weblinks","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_SEARCH_WEBLINKS_XML_DESCRIPTION","group":""}', '{"search_limit":"50","search_content":"1","search_archived":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(422, 'plg_system_languagefilter', 'plugin', 'languagefilter', 'system', 0, 0, 1, 1, '{"name":"plg_system_languagefilter","type":"plugin","creationDate":"July 2010","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_SYSTEM_LANGUAGEFILTER_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(423, 'plg_system_p3p', 'plugin', 'p3p', 'system', 0, 1, 1, 0, '{"name":"plg_system_p3p","type":"plugin","creationDate":"September 2010","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_P3P_XML_DESCRIPTION","group":""}', '{"headers":"NOI ADM DEV PSAi COM NAV OUR OTRo STP IND DEM"}', '', '', 0, '0000-00-00 00:00:00', 2, 0),
(424, 'plg_system_cache', 'plugin', 'cache', 'system', 0, 0, 1, 1, '{"name":"plg_system_cache","type":"plugin","creationDate":"February 2007","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_CACHE_XML_DESCRIPTION","group":""}', '{"browsercache":"0","cachetime":"15"}', '', '', 0, '0000-00-00 00:00:00', 9, 0),
(425, 'plg_system_debug', 'plugin', 'debug', 'system', 0, 1, 1, 0, '{"name":"plg_system_debug","type":"plugin","creationDate":"December 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_DEBUG_XML_DESCRIPTION","group":""}', '{"profile":"1","queries":"1","memory":"1","language_files":"1","language_strings":"1","strip-first":"1","strip-prefix":"","strip-suffix":""}', '', '', 0, '0000-00-00 00:00:00', 4, 0),
(426, 'plg_system_log', 'plugin', 'log', 'system', 0, 1, 1, 1, '{"name":"plg_system_log","type":"plugin","creationDate":"April 2007","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_LOG_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 5, 0),
(427, 'plg_system_redirect', 'plugin', 'redirect', 'system', 0, 0, 1, 1, '{"name":"plg_system_redirect","type":"plugin","creationDate":"April 2009","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_REDIRECT_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 6, 0),
(428, 'plg_system_remember', 'plugin', 'remember', 'system', 0, 1, 1, 1, '{"name":"plg_system_remember","type":"plugin","creationDate":"April 2007","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_REMEMBER_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 7, 0),
(429, 'plg_system_sef', 'plugin', 'sef', 'system', 0, 1, 1, 0, '{"name":"plg_system_sef","type":"plugin","creationDate":"December 2007","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_SEF_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 8, 0),
(430, 'plg_system_logout', 'plugin', 'logout', 'system', 0, 1, 1, 1, '{"name":"plg_system_logout","type":"plugin","creationDate":"April 2009","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_SYSTEM_LOGOUT_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 3, 0),
(431, 'plg_user_contactcreator', 'plugin', 'contactcreator', 'user', 0, 0, 1, 0, '{"name":"plg_user_contactcreator","type":"plugin","creationDate":"August 2009","author":"Joomla! Project","copyright":"(C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_CONTACTCREATOR_XML_DESCRIPTION","group":""}', '{"autowebpage":"","category":"34","autopublish":"0"}', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(432, 'plg_user_joomla', 'plugin', 'joomla', 'user', 0, 1, 1, 0, '{"name":"plg_user_joomla","type":"plugin","creationDate":"December 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2009 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_USER_JOOMLA_XML_DESCRIPTION","group":""}', '{"strong_passwords":"1","autoregister":"1"}', '', '', 0, '0000-00-00 00:00:00', 2, 0),
(433, 'plg_user_profile', 'plugin', 'profile', 'user', 0, 0, 1, 0, '{"name":"plg_user_profile","type":"plugin","creationDate":"January 2008","author":"Joomla! Project","copyright":"(C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_USER_PROFILE_XML_DESCRIPTION","group":""}', '{"register-require_address1":"1","register-require_address2":"1","register-require_city":"1","register-require_region":"1","register-require_country":"1","register-require_postal_code":"1","register-require_phone":"1","register-require_website":"1","register-require_favoritebook":"1","register-require_aboutme":"1","register-require_tos":"1","register-require_dob":"1","profile-require_address1":"1","profile-require_address2":"1","profile-require_city":"1","profile-require_region":"1","profile-require_country":"1","profile-require_postal_code":"1","profile-require_phone":"1","profile-require_website":"1","profile-require_favoritebook":"1","profile-require_aboutme":"1","profile-require_tos":"1","profile-require_dob":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(434, 'plg_extension_joomla', 'plugin', 'joomla', 'extension', 0, 1, 1, 1, '{"name":"plg_extension_joomla","type":"plugin","creationDate":"May 2010","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_EXTENSION_JOOMLA_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(435, 'plg_content_joomla', 'plugin', 'joomla', 'content', 0, 1, 1, 0, '{"name":"plg_content_joomla","type":"plugin","creationDate":"November 2010","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_CONTENT_JOOMLA_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(436, 'plg_system_languagecode', 'plugin', 'languagecode', 'system', 0, 0, 1, 0, '{"name":"plg_system_languagecode","type":"plugin","creationDate":"November 2011","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_SYSTEM_LANGUAGECODE_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 10, 0),
(437, 'plg_quickicon_joomlaupdate', 'plugin', 'joomlaupdate', 'quickicon', 0, 1, 1, 1, '{"name":"plg_quickicon_joomlaupdate","type":"plugin","creationDate":"August 2011","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_QUICKICON_JOOMLAUPDATE_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(438, 'plg_quickicon_extensionupdate', 'plugin', 'extensionupdate', 'quickicon', 0, 1, 1, 1, '{"name":"plg_quickicon_extensionupdate","type":"plugin","creationDate":"August 2011","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_QUICKICON_EXTENSIONUPDATE_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(439, 'plg_captcha_recaptcha', 'plugin', 'recaptcha', 'captcha', 0, 0, 1, 0, '{"name":"plg_captcha_recaptcha","type":"plugin","creationDate":"December 2011","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_CAPTCHA_RECAPTCHA_XML_DESCRIPTION","group":""}', '{"public_key":"","private_key":"","theme":"clean"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(440, 'plg_system_highlight', 'plugin', 'highlight', 'system', 0, 1, 1, 0, '{"name":"plg_system_highlight","type":"plugin","creationDate":"August 2011","author":"Joomla! Project","copyright":"(C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_SYSTEM_HIGHLIGHT_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 7, 0),
(441, 'plg_content_finder', 'plugin', 'finder', 'content', 0, 0, 1, 0, '{"name":"plg_content_finder","type":"plugin","creationDate":"December 2011","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_CONTENT_FINDER_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(442, 'plg_finder_categories', 'plugin', 'categories', 'finder', 0, 1, 1, 0, '{"name":"plg_finder_categories","type":"plugin","creationDate":"August 2011","author":"Joomla! Project","copyright":"(C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_FINDER_CATEGORIES_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(443, 'plg_finder_contacts', 'plugin', 'contacts', 'finder', 0, 1, 1, 0, '{"name":"plg_finder_contacts","type":"plugin","creationDate":"August 2011","author":"Joomla! Project","copyright":"(C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_FINDER_CONTACTS_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 2, 0),
(444, 'plg_finder_content', 'plugin', 'content', 'finder', 0, 1, 1, 0, '{"name":"plg_finder_content","type":"plugin","creationDate":"August 2011","author":"Joomla! Project","copyright":"(C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_FINDER_CONTENT_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 3, 0),
(445, 'plg_finder_newsfeeds', 'plugin', 'newsfeeds', 'finder', 0, 1, 1, 0, '{"name":"plg_finder_newsfeeds","type":"plugin","creationDate":"August 2011","author":"Joomla! Project","copyright":"(C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_FINDER_NEWSFEEDS_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 4, 0),
(446, 'plg_finder_weblinks', 'plugin', 'weblinks', 'finder', 0, 1, 1, 0, '{"name":"plg_finder_weblinks","type":"plugin","creationDate":"August 2011","author":"Joomla! Project","copyright":"(C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_FINDER_WEBLINKS_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 5, 0),
(447, 'plg_finder_tags', 'plugin', 'tags', 'finder', 0, 1, 1, 0, '{"name":"plg_finder_tags","type":"plugin","creationDate":"February 2013","author":"Joomla! Project","copyright":"(C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_FINDER_TAGS_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(448, 'plg_twofactorauth_totp', 'plugin', 'totp', 'twofactorauth', 0, 0, 1, 0, '{"name":"plg_twofactorauth_totp","type":"plugin","creationDate":"August 2013","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.2.0","description":"PLG_TWOFACTORAUTH_TOTP_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(449, 'plg_authentication_cookie', 'plugin', 'cookie', 'authentication', 0, 1, 1, 0, '{"name":"plg_authentication_cookie","type":"plugin","creationDate":"July 2013","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_AUTH_COOKIE_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(450, 'plg_twofactorauth_yubikey', 'plugin', 'yubikey', 'twofactorauth', 0, 0, 1, 0, '{"name":"plg_twofactorauth_yubikey","type":"plugin","creationDate":"September 2013","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.2.0","description":"PLG_TWOFACTORAUTH_YUBIKEY_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(503, 'beez3', 'template', 'beez3', '', 0, 1, 1, 0, '{"name":"beez3","type":"template","creationDate":"25 November 2009","author":"Angie Radtke","copyright":"Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.","authorEmail":"a.radtke@derauftritt.de","authorUrl":"http:\\/\\/www.der-auftritt.de","version":"3.1.0","description":"TPL_BEEZ3_XML_DESCRIPTION","group":""}', '{"wrapperSmall":"53","wrapperLarge":"72","sitetitle":"","sitedescription":"","navposition":"center","templatecolor":"nature"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(504, 'hathor', 'template', 'hathor', '', 1, 1, 1, 0, '{"name":"hathor","type":"template","creationDate":"May 2010","author":"Andrea Tarr","copyright":"Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.","authorEmail":"hathor@tarrconsulting.com","authorUrl":"http:\\/\\/www.tarrconsulting.com","version":"3.0.0","description":"TPL_HATHOR_XML_DESCRIPTION","group":""}', '{"showSiteName":"0","colourChoice":"0","boldText":"0"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(506, 'protostar', 'template', 'protostar', '', 0, 1, 1, 0, '{"name":"protostar","type":"template","creationDate":"4\\/30\\/2012","author":"Kyle Ledbetter","copyright":"Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"","version":"1.0","description":"TPL_PROTOSTAR_XML_DESCRIPTION","group":""}', '{"templateColor":"","logoFile":"","googleFont":"1","googleFontName":"Open+Sans","fluidContainer":"0"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(507, 'isis', 'template', 'isis', '', 1, 1, 1, 0, '{"name":"isis","type":"template","creationDate":"3\\/30\\/2012","author":"Kyle Ledbetter","copyright":"Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"","version":"1.0","description":"TPL_ISIS_XML_DESCRIPTION","group":""}', '{"templateColor":"","logoFile":""}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(600, 'English (United Kingdom)', 'language', 'en-GB', '', 0, 1, 1, 1, '{"name":"English (United Kingdom)","type":"language","creationDate":"2013-03-07","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.2.3","description":"en-GB site language","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(601, 'English (United Kingdom)', 'language', 'en-GB', '', 1, 1, 1, 1, '{"name":"English (United Kingdom)","type":"language","creationDate":"2013-03-07","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.2.3","description":"en-GB administrator language","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(700, 'files_joomla', 'file', 'joomla', '', 0, 1, 1, 1, '{"name":"files_joomla","type":"file","creationDate":"April 2014","author":"Joomla! Project","copyright":"(C) 2005 - 2014 Open Source Matters. All rights reserved","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.2.4","description":"FILES_JOOMLA_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10000, 'plg_installer_webinstaller', 'plugin', 'webinstaller', 'installer', 0, 1, 1, 0, '{"name":"plg_installer_webinstaller","type":"plugin","creationDate":"18 December 2013","author":"Joomla! Project","copyright":"Copyright (C) 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"1.0.5","description":"PLG_INSTALLER_WEBINSTALLER_XML_DESCRIPTION","group":""}', '{"tab_position":"0"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10001, 'PLG_SYSTEM_NNFRAMEWORK', 'plugin', 'nnframework', 'system', 0, 1, 1, 0, '{"name":"PLG_SYSTEM_NNFRAMEWORK","type":"plugin","creationDate":"November 2014","author":"NoNumber (Peter van Westen)","copyright":"Copyright \\u00a9 2014 NoNumber All Rights Reserved","authorEmail":"peter@nonumber.nl","authorUrl":"http:\\/\\/www.nonumber.nl","version":"14.11.8","description":"PLG_SYSTEM_NNFRAMEWORK_DESC","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10002, 'PLG_SYSTEM_SOURCERER', 'plugin', 'sourcerer', 'system', 0, 1, 1, 0, '{"name":"PLG_SYSTEM_SOURCERER","type":"plugin","creationDate":"November 2014","author":"NoNumber (Peter van Westen)","copyright":"Copyright \\u00a9 2014 NoNumber All Rights Reserved","authorEmail":"peter@nonumber.nl","authorUrl":"http:\\/\\/www.nonumber.nl","version":"4.4.7FREE","description":"PLG_SYSTEM_SOURCERER_DESC","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10003, 'PLG_EDITORS-XTD_SOURCERER', 'plugin', 'sourcerer', 'editors-xtd', 0, 1, 1, 0, '{"name":"PLG_EDITORS-XTD_SOURCERER","type":"plugin","creationDate":"November 2014","author":"NoNumber (Peter van Westen)","copyright":"Copyright \\u00a9 2014 NoNumber All Rights Reserved","authorEmail":"peter@nonumber.nl","authorUrl":"http:\\/\\/www.nonumber.nl","version":"4.4.7FREE","description":"PLG_EDITORS-XTD_SOURCERER_DESC","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10004, 'Social Share Buttons', 'module', 'mod_socialsharebuttons', '', 0, 1, 0, 0, '{"name":"Social Share Buttons","type":"module","creationDate":"Aug 2013","author":"E-max","copyright":"Copyright (C) 2008 - 2013 e-max.it. All rights reserved.","authorEmail":"webmaster@e-max.it","authorUrl":"http:\\/\\/www.e-max.it","version":"1.2.2","description":"MOD_SOCIALSHAREBUTTONS_DESCRIPTION","group":""}', '{"facebookLikeButton":"1","facebookLikeRenderer":"1","facebookLikeAction":"like","facebookLikeType":"button_count","facebookLikeFaces":"0","facebookLikeColor":"light","facebookLikeFont":"","facebookLikeWidth":"100","fbDynamicLocale":"0","fbLocale":"en_US","twitterButton":"1","twitterName":"","twitterCounter":"horizontal","twitterLanguage":"0","retweetmeButton":"1","retweetmeType":"compact","plusButton":"1","plusType":"medium","plusLocale":"en","linkedInButton":"1","linkedInType":"right","buzzButton":"1","buzzType":"small-count","buzzLocale":"en","diggButton":"1","diggType":"DiggCompact","stumbleButton":"1","stumbleType":"1","tumblrButton":"1","tumblrType":"1","redditButton":"1","redditType":"1","pinterestButton":"1","pinterestStaticImage":"0","pinterestImage":"","pinterestStaticDesc":"0","pinterestDesc":"","pinterestType":"1","bufferappButton":"1","bufferappType":"1","facebookShareMeButton":"0","facebookShareMeType":"small","facebookShareMeBadgeText":"FFFFFF","facebookShareMeBadge":"3B5998","owncache":"1","cache_time":"900","facebookLikeAppId":"","facebookLikeSend":"1","facebookRootDiv":"1","facebookLoadJsLib":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10005, 'CEGCore', 'library', 'cegcore', '', 0, 1, 1, 0, '{"name":"CEGCore","type":"library","creationDate":"01.2014","author":"ChronoEngine.com","copyright":"Copyright (C) 2013","authorEmail":"webmaster@chronoengine.com","authorUrl":"www.chronoengine.com","version":"1.0","description":"The GCore framework classes!","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10006, 'com_chronoforms5', 'component', 'com_chronoforms5', '', 1, 1, 0, 0, '{"name":"com_chronoforms5","type":"component","creationDate":"16.March.2015","author":"ChronoEngine.com","copyright":"ChronoEngine.com 2015","authorEmail":"webmaster@chronoengine.com","authorUrl":"www.chronoengine.com","version":"5.0.9","description":"The ChronoForms V5 component is the ultimate tool for Joomla forms creation, power and simplicity.","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10007, 'chronoforms5', 'package', 'pkg_chronoforms5', '', 0, 1, 1, 0, '{"name":"ChronoForms5 Package","type":"package","creationDate":"2015","author":"ChronoEngine.com Team","copyright":"","authorEmail":"","authorUrl":"","version":"V5.0","description":"ChronoForms5 extension package (Component + CEGCore library)","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10008, 'Chronoforms5', 'plugin', 'chronoforms5', 'content', 0, 1, 1, 0, '{"name":"Chronoforms5","type":"plugin","creationDate":"08.Dec.2013","author":"ChronoEngine.com","copyright":"(C) ChronoEngine.com","authorEmail":"webmaster@chronoengine.com","authorUrl":"www.chronoengine.com","version":"V5.0","description":"\\n\\tThis plugin requires the ChronoForms V5 component.\\n\\tYou just need to put the name of the form you want to show between : {chronoforms5}&{\\/chronoforms5}, Example: {chronoforms5}my_form_name{\\/chronoforms5}\\n\\t","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10009, 'ChronoForms5', 'module', 'mod_chronoforms5', '', 0, 1, 0, 0, '{"name":"ChronoForms5","type":"module","creationDate":"December 2013","author":"ChronoEngine.com","copyright":"Copyright (C) 2006 - 2013 ChronoEngine.com. All rights reserved.","authorEmail":"webmaster@chronoengine.com","authorUrl":"www.chronoengine.com","version":"V5 RC1.2","description":"Show a Chronoform V5","group":""}', '{"cache":"0","chronoform":"","moduleclass_sfx":""}', '', '', 0, '0000-00-00 00:00:00', 0, 0);

-- --------------------------------------------------------

--
-- Struttura della tabella `zcp8_finder_filters`
--

CREATE TABLE IF NOT EXISTS `zcp8_finder_filters` (
  `filter_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) unsigned NOT NULL,
  `created_by_alias` varchar(255) NOT NULL,
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `map_count` int(10) unsigned NOT NULL DEFAULT '0',
  `data` text NOT NULL,
  `params` mediumtext,
  PRIMARY KEY (`filter_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struttura della tabella `zcp8_finder_links`
--

CREATE TABLE IF NOT EXISTS `zcp8_finder_links` (
  `link_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `url` varchar(255) NOT NULL,
  `route` varchar(255) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `indexdate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `md5sum` varchar(32) DEFAULT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '1',
  `state` int(5) DEFAULT '1',
  `access` int(5) DEFAULT '0',
  `language` varchar(8) NOT NULL,
  `publish_start_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_end_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `start_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `end_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `list_price` double unsigned NOT NULL DEFAULT '0',
  `sale_price` double unsigned NOT NULL DEFAULT '0',
  `type_id` int(11) NOT NULL,
  `object` mediumblob NOT NULL,
  PRIMARY KEY (`link_id`),
  KEY `idx_type` (`type_id`),
  KEY `idx_title` (`title`),
  KEY `idx_md5` (`md5sum`),
  KEY `idx_url` (`url`(75)),
  KEY `idx_published_list` (`published`,`state`,`access`,`publish_start_date`,`publish_end_date`,`list_price`),
  KEY `idx_published_sale` (`published`,`state`,`access`,`publish_start_date`,`publish_end_date`,`sale_price`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struttura della tabella `zcp8_finder_links_terms0`
--

CREATE TABLE IF NOT EXISTS `zcp8_finder_links_terms0` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `zcp8_finder_links_terms1`
--

CREATE TABLE IF NOT EXISTS `zcp8_finder_links_terms1` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `zcp8_finder_links_terms2`
--

CREATE TABLE IF NOT EXISTS `zcp8_finder_links_terms2` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `zcp8_finder_links_terms3`
--

CREATE TABLE IF NOT EXISTS `zcp8_finder_links_terms3` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `zcp8_finder_links_terms4`
--

CREATE TABLE IF NOT EXISTS `zcp8_finder_links_terms4` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `zcp8_finder_links_terms5`
--

CREATE TABLE IF NOT EXISTS `zcp8_finder_links_terms5` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `zcp8_finder_links_terms6`
--

CREATE TABLE IF NOT EXISTS `zcp8_finder_links_terms6` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `zcp8_finder_links_terms7`
--

CREATE TABLE IF NOT EXISTS `zcp8_finder_links_terms7` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `zcp8_finder_links_terms8`
--

CREATE TABLE IF NOT EXISTS `zcp8_finder_links_terms8` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `zcp8_finder_links_terms9`
--

CREATE TABLE IF NOT EXISTS `zcp8_finder_links_terms9` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `zcp8_finder_links_termsa`
--

CREATE TABLE IF NOT EXISTS `zcp8_finder_links_termsa` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `zcp8_finder_links_termsb`
--

CREATE TABLE IF NOT EXISTS `zcp8_finder_links_termsb` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `zcp8_finder_links_termsc`
--

CREATE TABLE IF NOT EXISTS `zcp8_finder_links_termsc` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `zcp8_finder_links_termsd`
--

CREATE TABLE IF NOT EXISTS `zcp8_finder_links_termsd` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `zcp8_finder_links_termse`
--

CREATE TABLE IF NOT EXISTS `zcp8_finder_links_termse` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `zcp8_finder_links_termsf`
--

CREATE TABLE IF NOT EXISTS `zcp8_finder_links_termsf` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `zcp8_finder_taxonomy`
--

CREATE TABLE IF NOT EXISTS `zcp8_finder_taxonomy` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL,
  `state` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `access` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ordering` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `parent_id` (`parent_id`),
  KEY `state` (`state`),
  KEY `ordering` (`ordering`),
  KEY `access` (`access`),
  KEY `idx_parent_published` (`parent_id`,`state`,`access`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dump dei dati per la tabella `zcp8_finder_taxonomy`
--

INSERT INTO `zcp8_finder_taxonomy` (`id`, `parent_id`, `title`, `state`, `access`, `ordering`) VALUES
(1, 0, 'ROOT', 0, 0, 0);

-- --------------------------------------------------------

--
-- Struttura della tabella `zcp8_finder_taxonomy_map`
--

CREATE TABLE IF NOT EXISTS `zcp8_finder_taxonomy_map` (
  `link_id` int(10) unsigned NOT NULL,
  `node_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`node_id`),
  KEY `link_id` (`link_id`),
  KEY `node_id` (`node_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `zcp8_finder_terms`
--

CREATE TABLE IF NOT EXISTS `zcp8_finder_terms` (
  `term_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `term` varchar(75) NOT NULL,
  `stem` varchar(75) NOT NULL,
  `common` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `phrase` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `weight` float unsigned NOT NULL DEFAULT '0',
  `soundex` varchar(75) NOT NULL,
  `links` int(10) NOT NULL DEFAULT '0',
  `language` char(3) NOT NULL DEFAULT '',
  PRIMARY KEY (`term_id`),
  UNIQUE KEY `idx_term` (`term`),
  KEY `idx_term_phrase` (`term`,`phrase`),
  KEY `idx_stem_phrase` (`stem`,`phrase`),
  KEY `idx_soundex_phrase` (`soundex`,`phrase`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struttura della tabella `zcp8_finder_terms_common`
--

CREATE TABLE IF NOT EXISTS `zcp8_finder_terms_common` (
  `term` varchar(75) NOT NULL,
  `language` varchar(3) NOT NULL,
  KEY `idx_word_lang` (`term`,`language`),
  KEY `idx_lang` (`language`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `zcp8_finder_terms_common`
--

INSERT INTO `zcp8_finder_terms_common` (`term`, `language`) VALUES
('a', 'en'),
('about', 'en'),
('after', 'en'),
('ago', 'en'),
('all', 'en'),
('am', 'en'),
('an', 'en'),
('and', 'en'),
('ani', 'en'),
('any', 'en'),
('are', 'en'),
('aren''t', 'en'),
('as', 'en'),
('at', 'en'),
('be', 'en'),
('but', 'en'),
('by', 'en'),
('for', 'en'),
('from', 'en'),
('get', 'en'),
('go', 'en'),
('how', 'en'),
('if', 'en'),
('in', 'en'),
('into', 'en'),
('is', 'en'),
('isn''t', 'en'),
('it', 'en'),
('its', 'en'),
('me', 'en'),
('more', 'en'),
('most', 'en'),
('must', 'en'),
('my', 'en'),
('new', 'en'),
('no', 'en'),
('none', 'en'),
('not', 'en'),
('noth', 'en'),
('nothing', 'en'),
('of', 'en'),
('off', 'en'),
('often', 'en'),
('old', 'en'),
('on', 'en'),
('onc', 'en'),
('once', 'en'),
('onli', 'en'),
('only', 'en'),
('or', 'en'),
('other', 'en'),
('our', 'en'),
('ours', 'en'),
('out', 'en'),
('over', 'en'),
('page', 'en'),
('she', 'en'),
('should', 'en'),
('small', 'en'),
('so', 'en'),
('some', 'en'),
('than', 'en'),
('thank', 'en'),
('that', 'en'),
('the', 'en'),
('their', 'en'),
('theirs', 'en'),
('them', 'en'),
('then', 'en'),
('there', 'en'),
('these', 'en'),
('they', 'en'),
('this', 'en'),
('those', 'en'),
('thus', 'en'),
('time', 'en'),
('times', 'en'),
('to', 'en'),
('too', 'en'),
('true', 'en'),
('under', 'en'),
('until', 'en'),
('up', 'en'),
('upon', 'en'),
('use', 'en'),
('user', 'en'),
('users', 'en'),
('veri', 'en'),
('version', 'en'),
('very', 'en'),
('via', 'en'),
('want', 'en'),
('was', 'en'),
('way', 'en'),
('were', 'en'),
('what', 'en'),
('when', 'en'),
('where', 'en'),
('whi', 'en'),
('which', 'en'),
('who', 'en'),
('whom', 'en'),
('whose', 'en'),
('why', 'en'),
('wide', 'en'),
('will', 'en'),
('with', 'en'),
('within', 'en'),
('without', 'en'),
('would', 'en'),
('yes', 'en'),
('yet', 'en'),
('you', 'en'),
('your', 'en'),
('yours', 'en');

-- --------------------------------------------------------

--
-- Struttura della tabella `zcp8_finder_tokens`
--

CREATE TABLE IF NOT EXISTS `zcp8_finder_tokens` (
  `term` varchar(75) NOT NULL,
  `stem` varchar(75) NOT NULL,
  `common` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `phrase` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `weight` float unsigned NOT NULL DEFAULT '1',
  `context` tinyint(1) unsigned NOT NULL DEFAULT '2',
  `language` char(3) NOT NULL DEFAULT '',
  KEY `idx_word` (`term`),
  KEY `idx_context` (`context`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `zcp8_finder_tokens_aggregate`
--

CREATE TABLE IF NOT EXISTS `zcp8_finder_tokens_aggregate` (
  `term_id` int(10) unsigned NOT NULL,
  `map_suffix` char(1) NOT NULL,
  `term` varchar(75) NOT NULL,
  `stem` varchar(75) NOT NULL,
  `common` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `phrase` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `term_weight` float unsigned NOT NULL,
  `context` tinyint(1) unsigned NOT NULL DEFAULT '2',
  `context_weight` float unsigned NOT NULL,
  `total_weight` float unsigned NOT NULL,
  `language` char(3) NOT NULL DEFAULT '',
  KEY `token` (`term`),
  KEY `keyword_id` (`term_id`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `zcp8_finder_types`
--

CREATE TABLE IF NOT EXISTS `zcp8_finder_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `mime` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `title` (`title`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struttura della tabella `zcp8_languages`
--

CREATE TABLE IF NOT EXISTS `zcp8_languages` (
  `lang_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `lang_code` char(7) NOT NULL,
  `title` varchar(50) NOT NULL,
  `title_native` varchar(50) NOT NULL,
  `sef` varchar(50) NOT NULL,
  `image` varchar(50) NOT NULL,
  `description` varchar(512) NOT NULL,
  `metakey` text NOT NULL,
  `metadesc` text NOT NULL,
  `sitename` varchar(1024) NOT NULL DEFAULT '',
  `published` int(11) NOT NULL DEFAULT '0',
  `access` int(10) unsigned NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`lang_id`),
  UNIQUE KEY `idx_sef` (`sef`),
  UNIQUE KEY `idx_image` (`image`),
  UNIQUE KEY `idx_langcode` (`lang_code`),
  KEY `idx_access` (`access`),
  KEY `idx_ordering` (`ordering`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dump dei dati per la tabella `zcp8_languages`
--

INSERT INTO `zcp8_languages` (`lang_id`, `lang_code`, `title`, `title_native`, `sef`, `image`, `description`, `metakey`, `metadesc`, `sitename`, `published`, `access`, `ordering`) VALUES
(1, 'en-GB', 'English (UK)', 'English (UK)', 'en', 'en', '', '', '', '', 1, 1, 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `zcp8_menu`
--

CREATE TABLE IF NOT EXISTS `zcp8_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menutype` varchar(24) NOT NULL COMMENT 'The type of menu this item belongs to. FK to #__menu_types.menutype',
  `title` varchar(255) NOT NULL COMMENT 'The display title of the menu item.',
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'The SEF alias of the menu item.',
  `note` varchar(255) NOT NULL DEFAULT '',
  `path` varchar(1024) NOT NULL COMMENT 'The computed path of the menu item based on the alias field.',
  `link` varchar(1024) NOT NULL COMMENT 'The actually link the menu item refers to.',
  `type` varchar(16) NOT NULL COMMENT 'The type of link: Component, URL, Alias, Separator',
  `published` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'The published state of the menu link.',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '1' COMMENT 'The parent menu item in the menu tree.',
  `level` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The relative level in the tree.',
  `component_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'FK to #__extensions.id',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'FK to #__users.id',
  `checked_out_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'The time the menu item was checked out.',
  `browserNav` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'The click behaviour of the link.',
  `access` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The access level required to view the menu item.',
  `img` varchar(255) NOT NULL COMMENT 'The image of the menu item.',
  `template_style_id` int(10) unsigned NOT NULL DEFAULT '0',
  `params` text NOT NULL COMMENT 'JSON encoded data for the menu item.',
  `lft` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set lft.',
  `rgt` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set rgt.',
  `home` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'Indicates if this menu item is the home or default page.',
  `language` char(7) NOT NULL DEFAULT '',
  `client_id` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_client_id_parent_id_alias_language` (`client_id`,`parent_id`,`alias`,`language`),
  KEY `idx_componentid` (`component_id`,`menutype`,`published`,`access`),
  KEY `idx_menutype` (`menutype`),
  KEY `idx_left_right` (`lft`,`rgt`),
  KEY `idx_alias` (`alias`),
  KEY `idx_path` (`path`(255)),
  KEY `idx_language` (`language`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=108 ;

--
-- Dump dei dati per la tabella `zcp8_menu`
--

INSERT INTO `zcp8_menu` (`id`, `menutype`, `title`, `alias`, `note`, `path`, `link`, `type`, `published`, `parent_id`, `level`, `component_id`, `checked_out`, `checked_out_time`, `browserNav`, `access`, `img`, `template_style_id`, `params`, `lft`, `rgt`, `home`, `language`, `client_id`) VALUES
(1, '', 'Menu_Item_Root', 'root', '', '', '', '', 1, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, 0, '', 0, '', 0, 61, 0, '*', 0),
(2, 'menu', 'com_banners', 'Banners', '', 'Banners', 'index.php?option=com_banners', 'component', 0, 1, 1, 4, 0, '0000-00-00 00:00:00', 0, 0, 'class:banners', 0, '', 1, 10, 0, '*', 1),
(3, 'menu', 'com_banners', 'Banners', '', 'Banners/Banners', 'index.php?option=com_banners', 'component', 0, 2, 2, 4, 0, '0000-00-00 00:00:00', 0, 0, 'class:banners', 0, '', 2, 3, 0, '*', 1),
(4, 'menu', 'com_banners_categories', 'Categories', '', 'Banners/Categories', 'index.php?option=com_categories&extension=com_banners', 'component', 0, 2, 2, 6, 0, '0000-00-00 00:00:00', 0, 0, 'class:banners-cat', 0, '', 4, 5, 0, '*', 1),
(5, 'menu', 'com_banners_clients', 'Clients', '', 'Banners/Clients', 'index.php?option=com_banners&view=clients', 'component', 0, 2, 2, 4, 0, '0000-00-00 00:00:00', 0, 0, 'class:banners-clients', 0, '', 6, 7, 0, '*', 1),
(6, 'menu', 'com_banners_tracks', 'Tracks', '', 'Banners/Tracks', 'index.php?option=com_banners&view=tracks', 'component', 0, 2, 2, 4, 0, '0000-00-00 00:00:00', 0, 0, 'class:banners-tracks', 0, '', 8, 9, 0, '*', 1),
(7, 'menu', 'com_contact', 'Contacts', '', 'Contacts', 'index.php?option=com_contact', 'component', 0, 1, 1, 8, 0, '0000-00-00 00:00:00', 0, 0, 'class:contact', 0, '', 11, 16, 0, '*', 1),
(8, 'menu', 'com_contact', 'Contacts', '', 'Contacts/Contacts', 'index.php?option=com_contact', 'component', 0, 7, 2, 8, 0, '0000-00-00 00:00:00', 0, 0, 'class:contact', 0, '', 12, 13, 0, '*', 1),
(9, 'menu', 'com_contact_categories', 'Categories', '', 'Contacts/Categories', 'index.php?option=com_categories&extension=com_contact', 'component', 0, 7, 2, 6, 0, '0000-00-00 00:00:00', 0, 0, 'class:contact-cat', 0, '', 14, 15, 0, '*', 1),
(10, 'menu', 'com_messages', 'Messaging', '', 'Messaging', 'index.php?option=com_messages', 'component', 0, 1, 1, 15, 0, '0000-00-00 00:00:00', 0, 0, 'class:messages', 0, '', 17, 22, 0, '*', 1),
(11, 'menu', 'com_messages_add', 'New Private Message', '', 'Messaging/New Private Message', 'index.php?option=com_messages&task=message.add', 'component', 0, 10, 2, 15, 0, '0000-00-00 00:00:00', 0, 0, 'class:messages-add', 0, '', 18, 19, 0, '*', 1),
(12, 'menu', 'com_messages_read', 'Read Private Message', '', 'Messaging/Read Private Message', 'index.php?option=com_messages', 'component', 0, 10, 2, 15, 0, '0000-00-00 00:00:00', 0, 0, 'class:messages-read', 0, '', 20, 21, 0, '*', 1),
(13, 'menu', 'com_newsfeeds', 'News Feeds', '', 'News Feeds', 'index.php?option=com_newsfeeds', 'component', 0, 1, 1, 17, 0, '0000-00-00 00:00:00', 0, 0, 'class:newsfeeds', 0, '', 23, 28, 0, '*', 1),
(14, 'menu', 'com_newsfeeds_feeds', 'Feeds', '', 'News Feeds/Feeds', 'index.php?option=com_newsfeeds', 'component', 0, 13, 2, 17, 0, '0000-00-00 00:00:00', 0, 0, 'class:newsfeeds', 0, '', 24, 25, 0, '*', 1),
(15, 'menu', 'com_newsfeeds_categories', 'Categories', '', 'News Feeds/Categories', 'index.php?option=com_categories&extension=com_newsfeeds', 'component', 0, 13, 2, 6, 0, '0000-00-00 00:00:00', 0, 0, 'class:newsfeeds-cat', 0, '', 26, 27, 0, '*', 1),
(16, 'menu', 'com_redirect', 'Redirect', '', 'Redirect', 'index.php?option=com_redirect', 'component', 0, 1, 1, 24, 0, '0000-00-00 00:00:00', 0, 0, 'class:redirect', 0, '', 29, 30, 0, '*', 1),
(17, 'menu', 'com_search', 'Basic Search', '', 'Basic Search', 'index.php?option=com_search', 'component', 0, 1, 1, 19, 0, '0000-00-00 00:00:00', 0, 0, 'class:search', 0, '', 31, 32, 0, '*', 1),
(18, 'menu', 'com_weblinks', 'Weblinks', '', 'Weblinks', 'index.php?option=com_weblinks', 'component', 0, 1, 1, 21, 0, '0000-00-00 00:00:00', 0, 0, 'class:weblinks', 0, '', 33, 38, 0, '*', 1),
(19, 'menu', 'com_weblinks_links', 'Links', '', 'Weblinks/Links', 'index.php?option=com_weblinks', 'component', 0, 18, 2, 21, 0, '0000-00-00 00:00:00', 0, 0, 'class:weblinks', 0, '', 34, 35, 0, '*', 1),
(20, 'menu', 'com_weblinks_categories', 'Categories', '', 'Weblinks/Categories', 'index.php?option=com_categories&extension=com_weblinks', 'component', 0, 18, 2, 6, 0, '0000-00-00 00:00:00', 0, 0, 'class:weblinks-cat', 0, '', 36, 37, 0, '*', 1),
(21, 'menu', 'com_finder', 'Smart Search', '', 'Smart Search', 'index.php?option=com_finder', 'component', 0, 1, 1, 27, 0, '0000-00-00 00:00:00', 0, 0, 'class:finder', 0, '', 39, 40, 0, '*', 1),
(22, 'menu', 'com_joomlaupdate', 'Joomla! Update', '', 'Joomla! Update', 'index.php?option=com_joomlaupdate', 'component', 1, 1, 1, 28, 0, '0000-00-00 00:00:00', 0, 0, 'class:joomlaupdate', 0, '', 41, 42, 0, '*', 1),
(23, 'main', 'com_tags', 'Tags', '', 'Tags', 'index.php?option=com_tags', 'component', 0, 1, 1, 29, 0, '0000-00-00 00:00:00', 0, 1, 'class:tags', 0, '', 43, 44, 0, '', 1),
(24, 'main', 'com_postinstall', 'Post-installation messages', '', 'Post-installation messages', 'index.php?option=com_postinstall', 'component', 0, 1, 1, 32, 0, '0000-00-00 00:00:00', 0, 1, 'class:postinstall', 0, '', 45, 46, 0, '*', 1),
(101, 'mainmenu', 'Eventi', 'eventi', '', 'eventi', 'index.php?option=com_content&view=article&id=1', 'component', 1, 1, 1, 22, 0, '0000-00-00 00:00:00', 0, 2, '', 0, '{"show_title":"","link_titles":"","show_intro":"","info_block_position":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_tags":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 47, 48, 1, '*', 0),
(102, 'mainmenu', 'Utenti', 'utenti', '', 'utenti', 'index.php?option=com_content&view=article&id=2', 'component', 1, 1, 1, 22, 0, '0000-00-00 00:00:00', 0, 2, '', 0, '{"show_title":"","link_titles":"","show_intro":"","info_block_position":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_tags":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 49, 50, 0, '*', 0),
(103, 'mainmenu', 'Profilo', 'profilo', '', 'profilo', 'index.php?option=com_users&view=profile&layout=edit', 'component', 1, 1, 1, 25, 0, '0000-00-00 00:00:00', 0, 2, '', 0, '{"menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 51, 52, 0, '*', 0),
(104, 'mainmenu', 'Esci', 'esci', '', 'esci', 'index.php?option=com_users&view=login', 'component', 1, 1, 1, 25, 0, '0000-00-00 00:00:00', 0, 2, '', 0, '{"login_redirect_url":"index.php","logindescription_show":"0","login_description":"","login_image":"","logout_redirect_url":"index.php","logoutdescription_show":"1","logout_description":"","logout_image":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 53, 54, 0, '*', 0),
(105, 'main', 'COM_CHRONOFORMS5', 'com-chronoforms5', '', 'com-chronoforms5', 'index.php?option=com_chronoforms5', 'component', 0, 1, 1, 10006, 0, '0000-00-00 00:00:00', 0, 2, 'components/com_chronoforms5/CF.png', 0, '', 55, 60, 0, '', 1),
(106, 'main', 'COM_CHRONOFORMS5', 'com-chronoforms5', '', 'com-chronoforms5/com-chronoforms5', 'index.php?option=com_chronoforms5', 'component', 0, 105, 2, 10006, 0, '0000-00-00 00:00:00', 0, 2, 'class:component', 0, '', 56, 57, 0, '', 1),
(107, 'main', 'COM_CHRONOFORMS5_VALIDATE', 'com-chronoforms5-validate', '', 'com-chronoforms5/com-chronoforms5-validate', 'index.php?option=com_chronoforms5&act=validateinstall', 'component', 0, 105, 2, 10006, 0, '0000-00-00 00:00:00', 0, 2, 'class:component', 0, '', 58, 59, 0, '', 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `zcp8_menu_types`
--

CREATE TABLE IF NOT EXISTS `zcp8_menu_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menutype` varchar(24) NOT NULL,
  `title` varchar(48) NOT NULL,
  `description` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_menutype` (`menutype`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dump dei dati per la tabella `zcp8_menu_types`
--

INSERT INTO `zcp8_menu_types` (`id`, `menutype`, `title`, `description`) VALUES
(1, 'mainmenu', 'Main Menu', 'The main menu for the site');

-- --------------------------------------------------------

--
-- Struttura della tabella `zcp8_messages`
--

CREATE TABLE IF NOT EXISTS `zcp8_messages` (
  `message_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id_from` int(10) unsigned NOT NULL DEFAULT '0',
  `user_id_to` int(10) unsigned NOT NULL DEFAULT '0',
  `folder_id` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `date_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `state` tinyint(1) NOT NULL DEFAULT '0',
  `priority` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `subject` varchar(255) NOT NULL DEFAULT '',
  `message` text NOT NULL,
  PRIMARY KEY (`message_id`),
  KEY `useridto_state` (`user_id_to`,`state`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struttura della tabella `zcp8_messages_cfg`
--

CREATE TABLE IF NOT EXISTS `zcp8_messages_cfg` (
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `cfg_name` varchar(100) NOT NULL DEFAULT '',
  `cfg_value` varchar(255) NOT NULL DEFAULT '',
  UNIQUE KEY `idx_user_var_name` (`user_id`,`cfg_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `zcp8_modules`
--

CREATE TABLE IF NOT EXISTS `zcp8_modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `asset_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'FK to the #__assets table.',
  `title` varchar(100) NOT NULL DEFAULT '',
  `note` varchar(255) NOT NULL DEFAULT '',
  `content` text NOT NULL,
  `ordering` int(11) NOT NULL DEFAULT '0',
  `position` varchar(50) NOT NULL DEFAULT '',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `module` varchar(50) DEFAULT NULL,
  `access` int(10) unsigned NOT NULL DEFAULT '0',
  `showtitle` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `params` text NOT NULL,
  `client_id` tinyint(4) NOT NULL DEFAULT '0',
  `language` char(7) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `published` (`published`,`access`),
  KEY `newsfeeds` (`module`,`published`),
  KEY `idx_language` (`language`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=90 ;

--
-- Dump dei dati per la tabella `zcp8_modules`
--

INSERT INTO `zcp8_modules` (`id`, `asset_id`, `title`, `note`, `content`, `ordering`, `position`, `checked_out`, `checked_out_time`, `publish_up`, `publish_down`, `published`, `module`, `access`, `showtitle`, `params`, `client_id`, `language`) VALUES
(1, 39, 'Strumenti', '', '', 1, 'position-8', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_menu', 1, 1, '{"menutype":"mainmenu","base":"","startLevel":"1","endLevel":"0","showAllChildren":"0","tag_id":"","class_sfx":"","window_open":"","layout":"_:default","moduleclass_sfx":"_menu","cache":"1","cache_time":"900","cachemode":"itemid","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(2, 56, 'Login', '', '', 1, 'login', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_login', 1, 1, '', 1, '*'),
(3, 57, 'Popular Articles', '', '', 3, 'cpanel', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_popular', 3, 1, '{"count":"5","catid":"","user_id":"0","layout":"_:default","moduleclass_sfx":"","cache":"0","automatic_title":"1"}', 1, '*'),
(4, 58, 'Recently Added Articles', '', '', 4, 'cpanel', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_latest', 3, 1, '{"count":"5","ordering":"c_dsc","catid":"","user_id":"0","layout":"_:default","moduleclass_sfx":"","cache":"0","automatic_title":"1"}', 1, '*'),
(8, 59, 'Toolbar', '', '', 1, 'toolbar', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_toolbar', 3, 1, '', 1, '*'),
(9, 60, 'Quick Icons', '', '', 1, 'icon', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_quickicon', 3, 1, '', 1, '*'),
(10, 61, 'Logged-in Users', '', '', 2, 'cpanel', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_logged', 3, 1, '{"count":"5","name":"1","layout":"_:default","moduleclass_sfx":"","cache":"0","automatic_title":"1"}', 1, '*'),
(12, 62, 'Admin Menu', '', '', 1, 'menu', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_menu', 3, 1, '{"layout":"","moduleclass_sfx":"","shownew":"1","showhelp":"1","cache":"0"}', 1, '*'),
(13, 63, 'Admin Submenu', '', '', 1, 'submenu', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_submenu', 3, 1, '', 1, '*'),
(14, 64, 'User Status', '', '', 2, 'status', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_status', 3, 1, '', 1, '*'),
(15, 65, 'Title', '', '', 1, 'title', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_title', 3, 1, '', 1, '*'),
(16, 66, 'Login Form', '', '', 7, 'position-7', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_login', 1, 1, '{"greeting":"1","name":"0"}', 0, '*'),
(17, 67, 'Breadcrumbs', '', '', 1, 'position-2', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_breadcrumbs', 1, 1, '{"moduleclass_sfx":"","showHome":"1","homeText":"","showComponent":"1","separator":"","cache":"1","cache_time":"900","cachemode":"itemid"}', 0, '*'),
(79, 68, 'Multilanguage status', '', '', 1, 'status', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_multilangstatus', 3, 1, '{"layout":"_:default","moduleclass_sfx":"","cache":"0"}', 1, '*'),
(86, 69, 'Joomla Version', '', '', 1, 'footer', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_version', 3, 1, '{"format":"short","product":"1","layout":"_:default","moduleclass_sfx":"","cache":"0"}', 1, '*'),
(87, 56, 'Esci', '', '<p> </p>\r\n<p><strong>Premere il tasto Logout se si è certi di voler terminare l''attuale sessione di lavoro. Sarà nuovamente possibile effettuare l''accesso al pannello tramite autenticazione.</strong></p>\r\n<p> </p>\r\n<p> </p>', 1, 'position-3', 286, '2015-05-20 10:26:07', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 2, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(88, 57, 'Social Share Buttons', '', '', 0, '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_socialsharebuttons', 1, 1, '', 0, '*'),
(89, 59, 'ChronoForms5', '', '', 0, '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_chronoforms5', 1, 1, '', 0, '*');

-- --------------------------------------------------------

--
-- Struttura della tabella `zcp8_modules_menu`
--

CREATE TABLE IF NOT EXISTS `zcp8_modules_menu` (
  `moduleid` int(11) NOT NULL DEFAULT '0',
  `menuid` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`moduleid`,`menuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `zcp8_modules_menu`
--

INSERT INTO `zcp8_modules_menu` (`moduleid`, `menuid`) VALUES
(1, 0),
(2, 0),
(3, 0),
(4, 0),
(6, 0),
(7, 0),
(8, 0),
(9, 0),
(10, 0),
(12, 0),
(13, 0),
(14, 0),
(15, 0),
(16, 0),
(17, 0),
(79, 0),
(86, 0),
(87, 104);

-- --------------------------------------------------------

--
-- Struttura della tabella `zcp8_newsfeeds`
--

CREATE TABLE IF NOT EXISTS `zcp8_newsfeeds` (
  `catid` int(11) NOT NULL DEFAULT '0',
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL DEFAULT '',
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `link` varchar(200) NOT NULL DEFAULT '',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `numarticles` int(10) unsigned NOT NULL DEFAULT '1',
  `cache_time` int(10) unsigned NOT NULL DEFAULT '3600',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `rtl` tinyint(4) NOT NULL DEFAULT '0',
  `access` int(10) unsigned NOT NULL DEFAULT '0',
  `language` char(7) NOT NULL DEFAULT '',
  `params` text NOT NULL,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) unsigned NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) unsigned NOT NULL DEFAULT '0',
  `metakey` text NOT NULL,
  `metadesc` text NOT NULL,
  `metadata` text NOT NULL,
  `xreference` varchar(50) NOT NULL COMMENT 'A reference to enable linkages to external data sets.',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `description` text NOT NULL,
  `version` int(10) unsigned NOT NULL DEFAULT '1',
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  `images` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_access` (`access`),
  KEY `idx_checkout` (`checked_out`),
  KEY `idx_state` (`published`),
  KEY `idx_catid` (`catid`),
  KEY `idx_createdby` (`created_by`),
  KEY `idx_language` (`language`),
  KEY `idx_xreference` (`xreference`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struttura della tabella `zcp8_overrider`
--

CREATE TABLE IF NOT EXISTS `zcp8_overrider` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `constant` varchar(255) NOT NULL,
  `string` text NOT NULL,
  `file` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struttura della tabella `zcp8_postinstall_messages`
--

CREATE TABLE IF NOT EXISTS `zcp8_postinstall_messages` (
  `postinstall_message_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `extension_id` bigint(20) NOT NULL DEFAULT '700' COMMENT 'FK to #__extensions',
  `title_key` varchar(255) NOT NULL DEFAULT '' COMMENT 'Lang key for the title',
  `description_key` varchar(255) NOT NULL DEFAULT '' COMMENT 'Lang key for description',
  `action_key` varchar(255) NOT NULL DEFAULT '',
  `language_extension` varchar(255) NOT NULL DEFAULT 'com_postinstall' COMMENT 'Extension holding lang keys',
  `language_client_id` tinyint(3) NOT NULL DEFAULT '1',
  `type` varchar(10) NOT NULL DEFAULT 'link' COMMENT 'Message type - message, link, action',
  `action_file` varchar(255) DEFAULT '' COMMENT 'RAD URI to the PHP file containing action method',
  `action` varchar(255) DEFAULT '' COMMENT 'Action method name or URL',
  `condition_file` varchar(255) DEFAULT NULL COMMENT 'RAD URI to file holding display condition method',
  `condition_method` varchar(255) DEFAULT NULL COMMENT 'Display condition method, must return boolean',
  `version_introduced` varchar(50) NOT NULL DEFAULT '3.2.0' COMMENT 'Version when this message was introduced',
  `enabled` tinyint(3) NOT NULL DEFAULT '1',
  PRIMARY KEY (`postinstall_message_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dump dei dati per la tabella `zcp8_postinstall_messages`
--

INSERT INTO `zcp8_postinstall_messages` (`postinstall_message_id`, `extension_id`, `title_key`, `description_key`, `action_key`, `language_extension`, `language_client_id`, `type`, `action_file`, `action`, `condition_file`, `condition_method`, `version_introduced`, `enabled`) VALUES
(1, 700, 'PLG_TWOFACTORAUTH_TOTP_POSTINSTALL_TITLE', 'PLG_TWOFACTORAUTH_TOTP_POSTINSTALL_BODY', 'PLG_TWOFACTORAUTH_TOTP_POSTINSTALL_ACTION', 'plg_twofactorauth_totp', 1, 'action', 'site://plugins/twofactorauth/totp/postinstall/actions.php', 'twofactorauth_postinstall_action', 'site://plugins/twofactorauth/totp/postinstall/actions.php', 'twofactorauth_postinstall_condition', '3.2.0', 1),
(2, 700, 'COM_CPANEL_MSG_EACCELERATOR_TITLE', 'COM_CPANEL_MSG_EACCELERATOR_BODY', 'COM_CPANEL_MSG_EACCELERATOR_BUTTON', 'com_cpanel', 1, 'action', 'admin://components/com_admin/postinstall/eaccelerator.php', 'admin_postinstall_eaccelerator_action', 'admin://components/com_admin/postinstall/eaccelerator.php', 'admin_postinstall_eaccelerator_condition', '3.2.0', 1),
(3, 700, 'COM_CPANEL_WELCOME_BEGINNERS_TITLE', 'COM_CPANEL_WELCOME_BEGINNERS_MESSAGE', '', 'com_cpanel', 1, 'message', '', '', '', '', '3.2.0', 1),
(4, 700, 'COM_CPANEL_MSG_PHPVERSION_TITLE', 'COM_CPANEL_MSG_PHPVERSION_BODY', '', 'com_cpanel', 1, 'message', '', '', 'admin://components/com_admin/postinstall/phpversion.php', 'admin_postinstall_phpversion_condition', '3.2.2', 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `zcp8_redirect_links`
--

CREATE TABLE IF NOT EXISTS `zcp8_redirect_links` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `old_url` varchar(255) NOT NULL,
  `new_url` varchar(255) NOT NULL,
  `referer` varchar(150) NOT NULL,
  `comment` varchar(255) NOT NULL,
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  `published` tinyint(4) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_link_old` (`old_url`),
  KEY `idx_link_modifed` (`modified_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struttura della tabella `zcp8_schemas`
--

CREATE TABLE IF NOT EXISTS `zcp8_schemas` (
  `extension_id` int(11) NOT NULL,
  `version_id` varchar(20) NOT NULL,
  PRIMARY KEY (`extension_id`,`version_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `zcp8_schemas`
--

INSERT INTO `zcp8_schemas` (`extension_id`, `version_id`) VALUES
(700, '3.2.3-2014-02-20');

-- --------------------------------------------------------

--
-- Struttura della tabella `zcp8_session`
--

CREATE TABLE IF NOT EXISTS `zcp8_session` (
  `session_id` varchar(200) NOT NULL DEFAULT '',
  `client_id` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `guest` tinyint(4) unsigned DEFAULT '1',
  `time` varchar(14) DEFAULT '',
  `data` mediumtext,
  `userid` int(11) DEFAULT '0',
  `username` varchar(150) DEFAULT '',
  PRIMARY KEY (`session_id`),
  KEY `userid` (`userid`),
  KEY `time` (`time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `zcp8_session`
--

INSERT INTO `zcp8_session` (`session_id`, `client_id`, `guest`, `time`, `data`, `userid`, `username`) VALUES
('qf22kiqt4l3kfjbkcf2k0t93p0', 0, 1, '1440394119', 'v2wPwDMYRtnIJ2fPgHcHeP4B-CyJNV399QCkgPiyLaMZxZtjjlAPjfH2F9J34-ijNgBOCw_TYwc1kMohYkNnQsKKZDQD66TNzbEv-bxyMp8NImKOx-L8doECfne6kCWYWL64Mrn99gu72souhHWTAe1xDy3gwwZXMPBiYrnfGznkb5ryHe15rQfUWIsIpACBNLOEtdcaZn269ZsMgUxFNuWT4en8Zm_Ep9cBm9wJJvZTQ7ozChbGDlD18iVslLVikY-zfGgKVCN6XVRE7b64VGDt9mezo1RL8OTZWr434brjM8tmISFx3L1bfdPrqALtMGWFeQz7bnPiUngo7ZUKfUOYoALMx0_dMD5sYVSKuhA5A6kUc7MA5LAo3zfpjfif1iy4IC2YmSte7H7874wR20Gr1IZlpRQisyS-BH_sSWe_mk0KHxwu510kr2gYndH7EXltPm2jbKGFSa0wr2Utz3ZzNKUC3uPQh-tvBLX6PHtzeyNtEq6AqJy8x4n_is5JbJiKLK9q73Qt_njy0RVtNJ_G3ueTVgZgrPd7XiVC2u3Zvy-P5EOCyNQrDqViwpS3rC8phvLvueo5cjncBsd691ja43Zj0I1AR4rLcspMeA2FHm_8-Llgr3yIxQ6OomI8dfPkd81MJAqNfl2pkUGZb9vctT8FCR8Hjwol3ZTJpvMAMBcmIRXllgr3meMPJvk2V4aqWzCPAUh6_KX85j2nzqOX6EZcCJ6U1LQbUA0b9l-1V978U_apdGcH3JtpDBHDu2czZWCmmXnkd6-FVNSXirM2fVJn2XkliyqPczpMZTgBX0QQLMO_GcY2B67Ng0NwLFexhzlwu30PJZ3as6TloaqczJRofwMm-HghK5AfOfhcKyiYoZmix4J-G6rf9wEjhuAldn5my-k3r1S41iFZNWc1zy5-5cZWOa8xJwhzxAdj4v3rYEYmCu0R9KwLBLxa55R423_ovhej6MxUZC6OQprlMjelPrftKAz8Zl7kgbEv5c9_syoleB3-SZbAWeNdcI2A4nZibOes_FR3S1TD0odl2Z11jI15q96tBXFa9rwVOijxOnAq_7ACr1ZmNwzqumsbuJf9oLnq8FLyyX6xM2UKszv51-yzhNetPERR-2tmzMpznJYCVHzx2Km-goDZ67rL-Me1h94lCoe7OuqcINM932aXV4wl3_wBu-gkKtVBlXB2nG-W6pHmaXT6jpVUYVm_zoRcit4q1USVFjEPfBf_s71s_hjRaSquP8xfsLPtVKSSSIcoyF2fP64J4InSFODK6Xeg_BHlxpiJvUglc06Mt5lUayNC9VvMr56qKwRl3HKMyFznTlDscb3-3tCLPRzh2El-PKRUqHrZMGgdbphQHBdbdmnO4teZ01MsH46jH22cWrw65uxUr-sRXSncuLWbqXZBBR4t0w51UUcUFJpzTUXyq6-tgU3aNSdrW5HgF07Czx8RHZNxNG-SIAU7EaFwS_UG6A6mHWbkNSPm2oqbFz1TqcfIILetLpg8Dn9oU9chFwtX_WuMK8YkbZsjvxKLkj2eszUBMJTk4xKb2C7L2zvdYr32m5eYf26R6h--C4S6R6e-LlCicbGi0SXAfaw98GvQyvyGFNK2xCza78UK-oip13-zMukw0fl1TdYo8bivMhqNkWpmBjDGIMCo', 0, '');

-- --------------------------------------------------------

--
-- Struttura della tabella `zcp8_tags`
--

CREATE TABLE IF NOT EXISTS `zcp8_tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `lft` int(11) NOT NULL DEFAULT '0',
  `rgt` int(11) NOT NULL DEFAULT '0',
  `level` int(10) unsigned NOT NULL DEFAULT '0',
  `path` varchar(255) NOT NULL DEFAULT '',
  `title` varchar(255) NOT NULL,
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `note` varchar(255) NOT NULL DEFAULT '',
  `description` mediumtext NOT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(11) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `access` int(10) unsigned NOT NULL DEFAULT '0',
  `params` text NOT NULL,
  `metadesc` varchar(1024) NOT NULL COMMENT 'The meta description for the page.',
  `metakey` varchar(1024) NOT NULL COMMENT 'The meta keywords for the page.',
  `metadata` varchar(2048) NOT NULL COMMENT 'JSON encoded metadata properties.',
  `created_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `created_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by_alias` varchar(255) NOT NULL DEFAULT '',
  `modified_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `modified_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `images` text NOT NULL,
  `urls` text NOT NULL,
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  `language` char(7) NOT NULL,
  `version` int(10) unsigned NOT NULL DEFAULT '1',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `tag_idx` (`published`,`access`),
  KEY `idx_access` (`access`),
  KEY `idx_checkout` (`checked_out`),
  KEY `idx_path` (`path`),
  KEY `idx_left_right` (`lft`,`rgt`),
  KEY `idx_alias` (`alias`),
  KEY `idx_language` (`language`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dump dei dati per la tabella `zcp8_tags`
--

INSERT INTO `zcp8_tags` (`id`, `parent_id`, `lft`, `rgt`, `level`, `path`, `title`, `alias`, `note`, `description`, `published`, `checked_out`, `checked_out_time`, `access`, `params`, `metadesc`, `metakey`, `metadata`, `created_user_id`, `created_time`, `created_by_alias`, `modified_user_id`, `modified_time`, `images`, `urls`, `hits`, `language`, `version`, `publish_up`, `publish_down`) VALUES
(1, 0, 0, 1, 0, '', 'ROOT', 'root', '', '', 1, 0, '0000-00-00 00:00:00', 1, '', '', '', '', 0, '2011-01-01 00:00:01', '', 0, '0000-00-00 00:00:00', '', '', 0, '*', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Struttura della tabella `zcp8_template_styles`
--

CREATE TABLE IF NOT EXISTS `zcp8_template_styles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `template` varchar(50) NOT NULL DEFAULT '',
  `client_id` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `home` char(7) NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  `params` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_template` (`template`),
  KEY `idx_home` (`home`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dump dei dati per la tabella `zcp8_template_styles`
--

INSERT INTO `zcp8_template_styles` (`id`, `template`, `client_id`, `home`, `title`, `params`) VALUES
(4, 'beez3', 0, '0', 'Beez3 - Default', '{"wrapperSmall":"53","wrapperLarge":"72","logo":"images\\/joomla_black.gif","sitetitle":"Joomla!","sitedescription":"Open Source Content Management","navposition":"left","templatecolor":"personal","html5":"0"}'),
(5, 'hathor', 1, '0', 'Hathor - Default', '{"showSiteName":"0","colourChoice":"","boldText":"0"}'),
(7, 'protostar', 0, '1', 'protostar - Default', '{"templateColor":"","logoFile":"","googleFont":"1","googleFontName":"Open+Sans","fluidContainer":"0"}'),
(8, 'isis', 1, '1', 'isis - Default', '{"templateColor":"","logoFile":""}');

-- --------------------------------------------------------

--
-- Struttura della tabella `zcp8_ucm_base`
--

CREATE TABLE IF NOT EXISTS `zcp8_ucm_base` (
  `ucm_id` int(10) unsigned NOT NULL,
  `ucm_item_id` int(10) NOT NULL,
  `ucm_type_id` int(11) NOT NULL,
  `ucm_language_id` int(11) NOT NULL,
  PRIMARY KEY (`ucm_id`),
  KEY `idx_ucm_item_id` (`ucm_item_id`),
  KEY `idx_ucm_type_id` (`ucm_type_id`),
  KEY `idx_ucm_language_id` (`ucm_language_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `zcp8_ucm_content`
--

CREATE TABLE IF NOT EXISTS `zcp8_ucm_content` (
  `core_content_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `core_type_alias` varchar(255) NOT NULL DEFAULT '' COMMENT 'FK to the content types table',
  `core_title` varchar(255) NOT NULL,
  `core_alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `core_body` mediumtext NOT NULL,
  `core_state` tinyint(1) NOT NULL DEFAULT '0',
  `core_checked_out_time` varchar(255) NOT NULL DEFAULT '',
  `core_checked_out_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `core_access` int(10) unsigned NOT NULL DEFAULT '0',
  `core_params` text NOT NULL,
  `core_featured` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `core_metadata` varchar(2048) NOT NULL COMMENT 'JSON encoded metadata properties.',
  `core_created_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `core_created_by_alias` varchar(255) NOT NULL DEFAULT '',
  `core_created_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `core_modified_user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Most recent user that modified',
  `core_modified_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `core_language` char(7) NOT NULL,
  `core_publish_up` datetime NOT NULL,
  `core_publish_down` datetime NOT NULL,
  `core_content_item_id` int(10) unsigned DEFAULT NULL COMMENT 'ID from the individual type table',
  `asset_id` int(10) unsigned DEFAULT NULL COMMENT 'FK to the #__assets table.',
  `core_images` text NOT NULL,
  `core_urls` text NOT NULL,
  `core_hits` int(10) unsigned NOT NULL DEFAULT '0',
  `core_version` int(10) unsigned NOT NULL DEFAULT '1',
  `core_ordering` int(11) NOT NULL DEFAULT '0',
  `core_metakey` text NOT NULL,
  `core_metadesc` text NOT NULL,
  `core_catid` int(10) unsigned NOT NULL DEFAULT '0',
  `core_xreference` varchar(50) NOT NULL COMMENT 'A reference to enable linkages to external data sets.',
  `core_type_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`core_content_id`),
  KEY `tag_idx` (`core_state`,`core_access`),
  KEY `idx_access` (`core_access`),
  KEY `idx_alias` (`core_alias`),
  KEY `idx_language` (`core_language`),
  KEY `idx_title` (`core_title`),
  KEY `idx_modified_time` (`core_modified_time`),
  KEY `idx_created_time` (`core_created_time`),
  KEY `idx_content_type` (`core_type_alias`),
  KEY `idx_core_modified_user_id` (`core_modified_user_id`),
  KEY `idx_core_checked_out_user_id` (`core_checked_out_user_id`),
  KEY `idx_core_created_user_id` (`core_created_user_id`),
  KEY `idx_core_type_id` (`core_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Contains core content data in name spaced fields' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struttura della tabella `zcp8_ucm_history`
--

CREATE TABLE IF NOT EXISTS `zcp8_ucm_history` (
  `version_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ucm_item_id` int(10) unsigned NOT NULL,
  `ucm_type_id` int(10) unsigned NOT NULL,
  `version_note` varchar(255) NOT NULL DEFAULT '' COMMENT 'Optional version name',
  `save_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `editor_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `character_count` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Number of characters in this version.',
  `sha1_hash` varchar(50) NOT NULL DEFAULT '' COMMENT 'SHA1 hash of the version_data column.',
  `version_data` mediumtext NOT NULL COMMENT 'json-encoded string of version data',
  `keep_forever` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=auto delete; 1=keep',
  PRIMARY KEY (`version_id`),
  KEY `idx_ucm_item_id` (`ucm_type_id`,`ucm_item_id`),
  KEY `idx_save_date` (`save_date`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=75 ;

--
-- Dump dei dati per la tabella `zcp8_ucm_history`
--

INSERT INTO `zcp8_ucm_history` (`version_id`, `ucm_item_id`, `ucm_type_id`, `version_note`, `save_date`, `editor_user_id`, `character_count`, `sha1_hash`, `version_data`, `keep_forever`) VALUES
(36, 1, 1, '', '2015-06-17 14:12:27', 286, 3357, '64d3305a00f247f0d63ff1f351ab26ed0db70a73', '{"id":1,"asset_id":"54","title":"Gestione Eventi","alias":"gestione-eventi","introtext":"{source}\\r\\n<?php\\r\\n\\r\\nJLoader::register(''EventsController'', ''vendor\\/control\\/EventsController.php'');\\r\\n$eventsController = new EventsController();\\r\\n$eventsModel = new EventsModel();  \\r\\n\\r\\nswitch($eventsController->getShow()){\\r\\n  case ''list'':\\r\\n  \\techo EventsView::listTitle();\\r\\n    echo EventsView::listInstruments();\\r\\n  \\r\\n    $eventsModel->loadFilter();\\r\\n    EventsView::$list = $eventsModel->select();\\r\\n  \\r\\n    echo EventsView::showlist();\\r\\n    break;\\r\\n  \\r\\n  case ''search'':\\r\\n  \\r\\n  \\t\\/\\/ passaggio del controller chronoforms \\r\\n    Travel::getInstance()->set(''eventController'', $eventsController);\\r\\n  \\r\\n  \\techo EventsView::searchEventTitle();\\r\\n    echo EventsView::backToEventList();\\r\\n  \\r\\n    ?> {chronoforms5}event_search{\\/chronoforms5} <?php\\r\\n    \\r\\n    echo EventsView::backToEventList();\\r\\n    \\r\\n    break;\\r\\n  \\r\\n  case ''edit'':\\r\\n    $eventsController->loadMaps();\\r\\n  \\t$eventDetail = $eventsModel->getById(JRequest::getVar(''item''));\\r\\n  \\r\\n    \\/\\/ passaggio del controller e della model al chronoforms \\r\\n    Travel::getInstance()->set(''eventDetail'', $eventDetail);\\r\\n    Travel::getInstance()->set(''eventController'', $eventsController);\\r\\n    \\r\\n    echo EventsView::mainDetails($eventDetail);\\r\\n    echo EventsView::updateEventTitle();\\r\\n\\t\\r\\n  \\t?> {chronoforms5}event_update{\\/chronoforms5} <?php\\r\\n    \\r\\n    break;\\r\\n  \\r\\n  case ''partecipants'':\\r\\n  \\techo ''Lista degli utenti che partecipano ad un evento'';\\r\\n    break;\\r\\n  \\r\\n  default:\\r\\n  \\tJFactory::getApplication()->enqueueMessage(JText::_(''SOME_ERROR_OCCURRED''), ''error'');\\r\\n    break;\\r\\n}\\r\\n\\r\\n?>\\r\\n{\\/source}","fulltext":"","state":1,"catid":"2","created":"2015-05-19 15:55:29","created_by":"286","created_by_alias":"","modified":"2015-06-17 14:12:27","modified_by":"286","checked_out":"286","checked_out_time":"2015-06-17 14:11:49","publish_up":"2015-05-19 15:55:29","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":46,"ordering":"1","metakey":"","metadesc":"","access":"1","hits":"342","metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0),
(37, 1, 1, '', '2015-06-17 14:47:42', 286, 3377, 'febcf466ea5a72a283d29636d2d6c684f56278e4', '{"id":1,"asset_id":"54","title":"Gestione Eventi","alias":"gestione-eventi","introtext":"{source}\\r\\n<?php\\r\\n\\r\\nJLoader::register(''EventsController'', ''vendor\\/control\\/EventsController.php'');\\r\\n$eventsController = new EventsController();\\r\\n$eventsModel = new EventsModel();  \\r\\n\\r\\nswitch($eventsController->getShow()){\\r\\n  case ''list'':\\r\\n  \\r\\n    \\r\\n  \\r\\n  \\techo EventsView::listTitle();\\r\\n    echo EventsView::listInstruments();\\r\\n  \\r\\n    $eventsModel->loadFilter();\\r\\n    EventsView::$list = $eventsModel->select();\\r\\n  \\r\\n    echo EventsView::showlist();\\r\\n    break;\\r\\n  \\r\\n  case ''search'':\\r\\n  \\r\\n  \\t\\/\\/ passaggio del controller chronoforms \\r\\n    Travel::getInstance()->set(''eventController'', $eventsController);\\r\\n  \\r\\n  \\techo EventsView::searchEventTitle();\\r\\n    echo EventsView::backToEventList();\\r\\n  \\r\\n    ?> {chronoforms5}event_search{\\/chronoforms5} <?php\\r\\n    \\r\\n    echo EventsView::backToEventList();\\r\\n    \\r\\n    break;\\r\\n  \\r\\n  case ''edit'':\\r\\n    $eventsController->loadMaps();\\r\\n  \\t$eventDetail = $eventsModel->getById(JRequest::getVar(''item''));\\r\\n  \\r\\n    \\/\\/ passaggio del controller e della model al chronoforms \\r\\n    Travel::getInstance()->set(''eventDetail'', $eventDetail);\\r\\n    Travel::getInstance()->set(''eventController'', $eventsController);\\r\\n    \\r\\n    echo EventsView::mainDetails($eventDetail);\\r\\n    echo EventsView::updateEventTitle();\\r\\n\\t\\r\\n  \\t?> {chronoforms5}event_update{\\/chronoforms5} <?php\\r\\n    \\r\\n    break;\\r\\n  \\r\\n  case ''partecipants'':\\r\\n  \\techo ''Lista degli utenti che partecipano ad un evento'';\\r\\n    break;\\r\\n  \\r\\n  default:\\r\\n  \\tJFactory::getApplication()->enqueueMessage(JText::_(''SOME_ERROR_OCCURRED''), ''error'');\\r\\n    break;\\r\\n}\\r\\n\\r\\n?>\\r\\n{\\/source}","fulltext":"","state":1,"catid":"2","created":"2015-05-19 15:55:29","created_by":"286","created_by_alias":"","modified":"2015-06-17 14:47:42","modified_by":"286","checked_out":"286","checked_out_time":"2015-06-17 14:12:27","publish_up":"2015-05-19 15:55:29","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":47,"ordering":"1","metakey":"","metadesc":"","access":"1","hits":"352","metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0),
(38, 1, 1, '', '2015-06-17 15:21:55', 286, 3417, '79fb52042134d0a131c4b55a14a96bb2caacff68', '{"id":1,"asset_id":"54","title":"Gestione Eventi","alias":"gestione-eventi","introtext":"{source}\\r\\n<?php\\r\\n\\r\\nJLoader::register(''EventsController'', ''vendor\\/control\\/EventsController.php'');\\r\\n$eventsController = new EventsController();\\r\\n$eventsModel = new EventsModel();  \\r\\n\\r\\nswitch($eventsController->getShow()){\\r\\n  case ''list'':\\r\\n  \\r\\n    $eventsController->performListActions();\\r\\n  \\r\\n  \\techo EventsView::listTitle();\\r\\n    echo EventsView::listInstruments();\\r\\n  \\r\\n    $eventsModel->loadFilter();\\r\\n    EventsView::$list = $eventsModel->select();\\r\\n  \\r\\n    echo EventsView::showlist();\\r\\n    break;\\r\\n  \\r\\n  case ''search'':\\r\\n  \\r\\n  \\t\\/\\/ passaggio del controller chronoforms \\r\\n    Travel::getInstance()->set(''eventController'', $eventsController);\\r\\n  \\r\\n  \\techo EventsView::searchEventTitle();\\r\\n    echo EventsView::backToEventList();\\r\\n  \\r\\n    ?> {chronoforms5}event_search{\\/chronoforms5} <?php\\r\\n    \\r\\n    echo EventsView::backToEventList();\\r\\n    \\r\\n    break;\\r\\n  \\r\\n  case ''edit'':\\r\\n    $eventsController->loadMaps();\\r\\n  \\t$eventDetail = $eventsModel->getById(JRequest::getVar(''item''));\\r\\n  \\r\\n    \\/\\/ passaggio del controller e della model al chronoforms \\r\\n    Travel::getInstance()->set(''eventDetail'', $eventDetail);\\r\\n    Travel::getInstance()->set(''eventController'', $eventsController);\\r\\n    \\r\\n    echo EventsView::mainDetails($eventDetail);\\r\\n    echo EventsView::updateEventTitle();\\r\\n\\t\\r\\n  \\t?> {chronoforms5}event_update{\\/chronoforms5} <?php\\r\\n    \\r\\n    break;\\r\\n  \\r\\n  case ''partecipants'':\\r\\n  \\techo ''Lista degli utenti che partecipano ad un evento'';\\r\\n    break;\\r\\n  \\r\\n  default:\\r\\n  \\tJFactory::getApplication()->enqueueMessage(JText::_(''SOME_ERROR_OCCURRED''), ''error'');\\r\\n    break;\\r\\n}\\r\\n\\r\\n?>\\r\\n{\\/source}","fulltext":"","state":1,"catid":"2","created":"2015-05-19 15:55:29","created_by":"286","created_by_alias":"","modified":"2015-06-17 15:21:55","modified_by":"286","checked_out":"286","checked_out_time":"2015-06-17 14:47:42","publish_up":"2015-05-19 15:55:29","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":48,"ordering":"1","metakey":"","metadesc":"","access":"1","hits":"362","metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0),
(39, 1, 1, '', '2015-06-18 10:57:52', 286, 3608, '8d9081a458c6b76df35cb978c51d3a823fada1a3', '{"id":1,"asset_id":"54","title":"Gestione Eventi","alias":"gestione-eventi","introtext":"{source}\\r\\n<?php\\r\\n\\r\\nJLoader::register(''EventsController'', ''vendor\\/control\\/EventsController.php'');\\r\\n$eventsController = new EventsController();\\r\\n$eventsModel = new EventsModel();  \\r\\n\\r\\nswitch($eventsController->getShow()){\\r\\n  case ''list'':\\r\\n  \\r\\n    $eventsController->performListActions();\\r\\n  \\r\\n  \\techo EventsView::listTitle();\\r\\n    echo EventsView::listInstruments();\\r\\n  \\r\\n    $eventsModel->loadFilter();\\r\\n    EventsView::$list = $eventsModel->select();\\r\\n  \\r\\n    echo EventsView::showlist();\\r\\n    break;\\r\\n  \\r\\n  case ''search'':\\r\\n  \\r\\n  \\t\\/\\/ passaggio del controller chronoforms \\r\\n    Travel::getInstance()->set(''eventController'', $eventsController);\\r\\n  \\r\\n  \\techo EventsView::searchEventTitle();\\r\\n    echo EventsView::backToEventList();\\r\\n  \\r\\n    ?> {chronoforms5}event_search{\\/chronoforms5} <?php\\r\\n    \\r\\n    echo EventsView::backToEventList();\\r\\n    \\r\\n    break;\\r\\n  \\r\\n  case ''edit'':\\r\\n    $eventsController->loadMaps();\\r\\n  \\t$eventDetail = $eventsModel->getById(JRequest::getVar(''item''));\\r\\n  \\r\\n    \\/\\/ passaggio del controller e della model al chronoforms \\r\\n    Travel::getInstance()->set(''eventDetail'', $eventDetail);\\r\\n    Travel::getInstance()->set(''eventController'', $eventsController);\\r\\n    \\r\\n    echo EventsView::mainDetails($eventDetail);\\r\\n    echo EventsView::updateEventTitle();\\r\\n\\t\\r\\n  \\t?> {chronoforms5}event_update{\\/chronoforms5} <?php\\r\\n    \\r\\n    break;\\r\\n  \\r\\n  case ''partecipants'':\\r\\n  \\/\\/$eventsController->performListActions();\\r\\n  \\r\\n  echo EventsView::partecipateTitle();\\r\\n  \\/\\/echo EventsView::listInstruments();\\r\\n\\r\\n    EventsView::$list = $eventsModel->partecipates();\\r\\n  \\r\\n    echo EventsView::showPartecipants();\\r\\n    break;\\r\\n  \\r\\n  default:\\r\\n  \\tJFactory::getApplication()->enqueueMessage(JText::_(''SOME_ERROR_OCCURRED''), ''error'');\\r\\n    break;\\r\\n}\\r\\n\\r\\n?>\\r\\n{\\/source}","fulltext":"","state":1,"catid":"2","created":"2015-05-19 15:55:29","created_by":"286","created_by_alias":"","modified":"2015-06-18 10:57:52","modified_by":"286","checked_out":"286","checked_out_time":"2015-06-18 10:27:39","publish_up":"2015-05-19 15:55:29","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":49,"ordering":"1","metakey":"","metadesc":"","access":"1","hits":"378","metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0),
(40, 1, 1, '', '2015-06-18 10:59:14', 286, 3632, 'e1913e34e9d0a1eb87166aab4da3a62bf53ecd45', '{"id":1,"asset_id":"54","title":"Gestione Eventi","alias":"gestione-eventi","introtext":"{source}\\r\\n<?php\\r\\n\\r\\nJLoader::register(''EventsController'', ''vendor\\/control\\/EventsController.php'');\\r\\n$eventsController = new EventsController();\\r\\n$eventsModel = new EventsModel();  \\r\\n\\r\\nswitch($eventsController->getShow()){\\r\\n  case ''list'':\\r\\n  \\r\\n    $eventsController->performListActions();\\r\\n  \\r\\n  \\techo EventsView::listTitle();\\r\\n    echo EventsView::listInstruments();\\r\\n  \\r\\n    $eventsModel->loadFilter();\\r\\n    EventsView::$list = $eventsModel->select();\\r\\n  \\r\\n    echo EventsView::showlist();\\r\\n    break;\\r\\n  \\r\\n  case ''search'':\\r\\n  \\r\\n  \\t\\/\\/ passaggio del controller chronoforms \\r\\n    Travel::getInstance()->set(''eventController'', $eventsController);\\r\\n  \\r\\n  \\techo EventsView::searchEventTitle();\\r\\n    echo EventsView::backToEventList();\\r\\n  \\r\\n    ?> {chronoforms5}event_search{\\/chronoforms5} <?php\\r\\n    \\r\\n    echo EventsView::backToEventList();\\r\\n    \\r\\n    break;\\r\\n  \\r\\n  case ''edit'':\\r\\n    $eventsController->loadMaps();\\r\\n  \\t$eventDetail = $eventsModel->getById(JRequest::getVar(''item''));\\r\\n  \\r\\n    \\/\\/ passaggio del controller e della model al chronoforms \\r\\n    Travel::getInstance()->set(''eventDetail'', $eventDetail);\\r\\n    Travel::getInstance()->set(''eventController'', $eventsController);\\r\\n    \\r\\n    echo EventsView::mainDetails($eventDetail);\\r\\n    echo EventsView::updateEventTitle();\\r\\n\\t\\r\\n  \\t?> {chronoforms5}event_update{\\/chronoforms5} <?php\\r\\n    \\r\\n    break;\\r\\n  \\r\\n  case ''partecipants'':\\r\\n  \\/\\/$eventsController->performListActions();\\r\\n  \\r\\n  echo EventsView::partecipateTitle();\\r\\n  \\/\\/echo EventsView::listInstruments();\\r\\n\\r\\n    EventsView::$list = $eventsModel->partecipates(JRequest::getVar(''item''));\\r\\n  \\r\\n    echo EventsView::showPartecipants();\\r\\n    break;\\r\\n  \\r\\n  default:\\r\\n  \\tJFactory::getApplication()->enqueueMessage(JText::_(''SOME_ERROR_OCCURRED''), ''error'');\\r\\n    break;\\r\\n}\\r\\n\\r\\n?>\\r\\n{\\/source}","fulltext":"","state":1,"catid":"2","created":"2015-05-19 15:55:29","created_by":"286","created_by_alias":"","modified":"2015-06-18 10:59:14","modified_by":"286","checked_out":"286","checked_out_time":"2015-06-18 10:57:52","publish_up":"2015-05-19 15:55:29","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":50,"ordering":"1","metakey":"","metadesc":"","access":"1","hits":"379","metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0),
(41, 1, 1, '', '2015-06-18 12:11:45', 286, 3693, 'da0ea4df2016b52374403858145942d160392ad1', '{"id":1,"asset_id":"54","title":"Gestione Eventi","alias":"gestione-eventi","introtext":"{source}\\r\\n<?php\\r\\n\\r\\nJLoader::register(''EventsController'', ''vendor\\/control\\/EventsController.php'');\\r\\n$eventsController = new EventsController();\\r\\n$eventsModel = new EventsModel();  \\r\\n\\r\\nswitch($eventsController->getShow()){\\r\\n  case ''list'':\\r\\n  \\r\\n    $eventsController->performListActions();\\r\\n  \\r\\n  \\techo EventsView::listTitle();\\r\\n    echo EventsView::listInstruments();\\r\\n  \\r\\n    $eventsModel->loadFilter();\\r\\n    EventsView::$list = $eventsModel->select();\\r\\n  \\r\\n    echo EventsView::showlist();\\r\\n    break;\\r\\n  \\r\\n  case ''search'':\\r\\n  \\r\\n  \\t\\/\\/ passaggio del controller chronoforms \\r\\n    Travel::getInstance()->set(''eventController'', $eventsController);\\r\\n  \\r\\n  \\techo EventsView::searchEventTitle();\\r\\n    echo EventsView::backToEventList();\\r\\n  \\r\\n    ?> {chronoforms5}event_search{\\/chronoforms5} <?php\\r\\n    \\r\\n    echo EventsView::backToEventList();\\r\\n    \\r\\n    break;\\r\\n  \\r\\n  case ''edit'':\\r\\n    $eventsController->loadMaps();\\r\\n  \\t$eventDetail = $eventsModel->getById(JRequest::getVar(''item''));\\r\\n  \\r\\n    \\/\\/ passaggio del controller e della model al chronoforms \\r\\n    Travel::getInstance()->set(''eventDetail'', $eventDetail);\\r\\n    Travel::getInstance()->set(''eventController'', $eventsController);\\r\\n    \\r\\n    echo EventsView::mainDetails($eventDetail);\\r\\n    echo EventsView::updateEventTitle();\\r\\n\\t\\r\\n  \\t?> {chronoforms5}event_update{\\/chronoforms5} <?php\\r\\n    \\r\\n    break;\\r\\n  \\r\\n  case ''partecipants'':\\r\\n    $idEvent = JRequest::getVar(''item'');\\r\\n  \\/\\/$eventsController->performListActions();\\r\\n  \\r\\n    echo EventsView::partecipateTitle($eventModel->getById($idEvent)->title);\\r\\n  \\/\\/echo EventsView::listInstruments();\\r\\n\\r\\n    EventsView::$list = $eventsModel->partecipates($idEvent);\\r\\n    echo EventsView::showPartecipants();\\r\\n    break;\\r\\n  \\r\\n  default:\\r\\n  \\tJFactory::getApplication()->enqueueMessage(JText::_(''SOME_ERROR_OCCURRED''), ''error'');\\r\\n    break;\\r\\n}\\r\\n\\r\\n?>\\r\\n{\\/source}","fulltext":"","state":1,"catid":"2","created":"2015-05-19 15:55:29","created_by":"286","created_by_alias":"","modified":"2015-06-18 12:11:45","modified_by":"286","checked_out":"286","checked_out_time":"2015-06-18 12:09:08","publish_up":"2015-05-19 15:55:29","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":51,"ordering":"1","metakey":"","metadesc":"","access":"1","hits":"384","metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0),
(42, 1, 1, '', '2015-06-18 12:12:31', 286, 3694, '3c12b764ef0266be863fdedd14f1bf89929d7460', '{"id":1,"asset_id":"54","title":"Gestione Eventi","alias":"gestione-eventi","introtext":"{source}\\r\\n<?php\\r\\n\\r\\nJLoader::register(''EventsController'', ''vendor\\/control\\/EventsController.php'');\\r\\n$eventsController = new EventsController();\\r\\n$eventsModel = new EventsModel();  \\r\\n\\r\\nswitch($eventsController->getShow()){\\r\\n  case ''list'':\\r\\n  \\r\\n    $eventsController->performListActions();\\r\\n  \\r\\n  \\techo EventsView::listTitle();\\r\\n    echo EventsView::listInstruments();\\r\\n  \\r\\n    $eventsModel->loadFilter();\\r\\n    EventsView::$list = $eventsModel->select();\\r\\n  \\r\\n    echo EventsView::showlist();\\r\\n    break;\\r\\n  \\r\\n  case ''search'':\\r\\n  \\r\\n  \\t\\/\\/ passaggio del controller chronoforms \\r\\n    Travel::getInstance()->set(''eventController'', $eventsController);\\r\\n  \\r\\n  \\techo EventsView::searchEventTitle();\\r\\n    echo EventsView::backToEventList();\\r\\n  \\r\\n    ?> {chronoforms5}event_search{\\/chronoforms5} <?php\\r\\n    \\r\\n    echo EventsView::backToEventList();\\r\\n    \\r\\n    break;\\r\\n  \\r\\n  case ''edit'':\\r\\n    $eventsController->loadMaps();\\r\\n  \\t$eventDetail = $eventsModel->getById(JRequest::getVar(''item''));\\r\\n  \\r\\n    \\/\\/ passaggio del controller e della model al chronoforms \\r\\n    Travel::getInstance()->set(''eventDetail'', $eventDetail);\\r\\n    Travel::getInstance()->set(''eventController'', $eventsController);\\r\\n    \\r\\n    echo EventsView::mainDetails($eventDetail);\\r\\n    echo EventsView::updateEventTitle();\\r\\n\\t\\r\\n  \\t?> {chronoforms5}event_update{\\/chronoforms5} <?php\\r\\n    \\r\\n    break;\\r\\n  \\r\\n  case ''partecipants'':\\r\\n    $idEvent = JRequest::getVar(''item'');\\r\\n  \\/\\/$eventsController->performListActions();\\r\\n  \\r\\n    echo EventsView::partecipateTitle($eventsModel->getById($idEvent)->title);\\r\\n  \\/\\/echo EventsView::listInstruments();\\r\\n\\r\\n    EventsView::$list = $eventsModel->partecipates($idEvent);\\r\\n    echo EventsView::showPartecipants();\\r\\n    break;\\r\\n  \\r\\n  default:\\r\\n  \\tJFactory::getApplication()->enqueueMessage(JText::_(''SOME_ERROR_OCCURRED''), ''error'');\\r\\n    break;\\r\\n}\\r\\n\\r\\n?>\\r\\n{\\/source}","fulltext":"","state":1,"catid":"2","created":"2015-05-19 15:55:29","created_by":"286","created_by_alias":"","modified":"2015-06-18 12:12:31","modified_by":"286","checked_out":"286","checked_out_time":"2015-06-18 12:11:45","publish_up":"2015-05-19 15:55:29","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":52,"ordering":"1","metakey":"","metadesc":"","access":"1","hits":"384","metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0),
(43, 1, 1, '', '2015-06-18 12:19:53', 286, 3704, '898fc3b19ccd7399b251adf9c99e14157628d907', '{"id":1,"asset_id":"54","title":"Gestione Eventi","alias":"gestione-eventi","introtext":"{source}\\r\\n<?php\\r\\n\\r\\nJLoader::register(''EventsController'', ''vendor\\/control\\/EventsController.php'');\\r\\n$eventsController = new EventsController();\\r\\n$eventsModel = new EventsModel();  \\r\\n\\r\\nswitch($eventsController->getShow()){\\r\\n  case ''list'':\\r\\n  \\r\\n    $eventsController->performListActions();\\r\\n  \\r\\n  \\techo EventsView::listTitle();\\r\\n    echo EventsView::listInstruments();\\r\\n  \\r\\n    $eventsModel->loadFilter();\\r\\n    EventsView::$list = $eventsModel->select();\\r\\n  \\r\\n    echo EventsView::showlist();\\r\\n    break;\\r\\n  \\r\\n  case ''search'':\\r\\n  \\r\\n  \\t\\/\\/ passaggio del controller chronoforms \\r\\n    Travel::getInstance()->set(''eventController'', $eventsController);\\r\\n  \\r\\n  \\techo EventsView::searchEventTitle();\\r\\n    echo EventsView::backToEventList();\\r\\n  \\r\\n    ?> {chronoforms5}event_search{\\/chronoforms5} <?php\\r\\n    \\r\\n    echo EventsView::backToEventList();\\r\\n    \\r\\n    break;\\r\\n  \\r\\n  case ''edit'':\\r\\n    $eventsController->loadMaps();\\r\\n  \\t$eventDetail = $eventsModel->getById(JRequest::getVar(''item''));\\r\\n  \\r\\n    \\/\\/ passaggio del controller e della model al chronoforms \\r\\n    Travel::getInstance()->set(''eventDetail'', $eventDetail);\\r\\n    Travel::getInstance()->set(''eventController'', $eventsController);\\r\\n    \\r\\n    echo EventsView::mainDetails($eventDetail);\\r\\n    echo EventsView::updateEventTitle();\\r\\n\\t\\r\\n  \\t?> {chronoforms5}event_update{\\/chronoforms5} <?php\\r\\n    \\r\\n    break;\\r\\n  \\r\\n  case ''partecipants'':\\r\\n    $idEvent = JRequest::getVar(''item'');\\r\\n  \\/\\/$eventsController->performListActions();\\r\\n  \\r\\n    echo EventsView::partecipateTitle($idEvent, $eventsModel->getById($idEvent)->title);\\r\\n  \\/\\/echo EventsView::listInstruments();\\r\\n\\r\\n    EventsView::$list = $eventsModel->partecipates($idEvent);\\r\\n    echo EventsView::showPartecipants();\\r\\n    break;\\r\\n  \\r\\n  default:\\r\\n  \\tJFactory::getApplication()->enqueueMessage(JText::_(''SOME_ERROR_OCCURRED''), ''error'');\\r\\n    break;\\r\\n}\\r\\n\\r\\n?>\\r\\n{\\/source}","fulltext":"","state":1,"catid":"2","created":"2015-05-19 15:55:29","created_by":"286","created_by_alias":"","modified":"2015-06-18 12:19:53","modified_by":"286","checked_out":"286","checked_out_time":"2015-06-18 12:12:31","publish_up":"2015-05-19 15:55:29","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":53,"ordering":"1","metakey":"","metadesc":"","access":"1","hits":"386","metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0),
(44, 1, 1, '', '2015-06-18 12:59:25', 286, 3710, 'ceb2beb775ef71aafee1070a8a6dea743611bbcf', '{"id":1,"asset_id":"54","title":"Gestione Eventi","alias":"gestione-eventi","introtext":"{source}\\r\\n<?php\\r\\n\\r\\nJLoader::register(''EventsController'', ''vendor\\/control\\/EventsController.php'');\\r\\n$eventsController = new EventsController();\\r\\n$eventsModel = new EventsModel();  \\r\\n\\r\\nswitch($eventsController->getShow()){\\r\\n  case ''list'':\\r\\n  \\r\\n    $eventsController->performListActions();\\r\\n  \\r\\n  \\techo EventsView::listTitle();\\r\\n    echo EventsView::listInstruments();\\r\\n  \\r\\n    $eventsModel->loadFilter();\\r\\n    EventsView::$list = $eventsModel->select();\\r\\n  \\r\\n    echo EventsView::showlist();\\r\\n    break;\\r\\n  \\r\\n  case ''search'':\\r\\n  \\r\\n  \\t\\/\\/ passaggio del controller chronoforms \\r\\n    Travel::getInstance()->set(''eventController'', $eventsController);\\r\\n  \\r\\n  \\techo EventsView::searchEventTitle();\\r\\n    echo EventsView::backToEventList();\\r\\n  \\r\\n    ?> {chronoforms5}event_search{\\/chronoforms5} <?php\\r\\n    \\r\\n    echo EventsView::backToEventList();\\r\\n    \\r\\n    break;\\r\\n  \\r\\n  case ''edit'':\\r\\n    $eventsController->loadMaps();\\r\\n  \\t$eventDetail = $eventsModel->getById(JRequest::getVar(''item''));\\r\\n  \\r\\n    \\/\\/ passaggio del controller e della model al chronoforms \\r\\n    Travel::getInstance()->set(''eventDetail'', $eventDetail);\\r\\n    Travel::getInstance()->set(''eventController'', $eventsController);\\r\\n    \\r\\n    echo EventsView::mainDetails($eventDetail);\\r\\n    echo EventsView::updateEventTitle();\\r\\n\\t\\r\\n  \\t?> {chronoforms5}event_update{\\/chronoforms5} <?php\\r\\n    \\r\\n    break;\\r\\n  \\r\\n  case ''partecipants'':\\r\\n    $idEvent = JRequest::getVar(''item'');\\r\\n  \\/\\/$eventsController->performListActions();\\r\\n  \\r\\n    echo EventsView::partecipateTitle($idEvent, $eventsModel->getById($idEvent)->title);\\r\\n    echo EventsView::partecipantsInstruments();\\r\\n\\r\\n    EventsView::$list = $eventsModel->partecipates($idEvent);\\r\\n    echo EventsView::showPartecipants();\\r\\n    break;\\r\\n  \\r\\n  default:\\r\\n  \\tJFactory::getApplication()->enqueueMessage(JText::_(''SOME_ERROR_OCCURRED''), ''error'');\\r\\n    break;\\r\\n}\\r\\n\\r\\n?>\\r\\n{\\/source}","fulltext":"","state":1,"catid":"2","created":"2015-05-19 15:55:29","created_by":"286","created_by_alias":"","modified":"2015-06-18 12:59:25","modified_by":"286","checked_out":"286","checked_out_time":"2015-06-18 12:59:01","publish_up":"2015-05-19 15:55:29","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":54,"ordering":"1","metakey":"","metadesc":"","access":"1","hits":"393","metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0),
(45, 1, 1, '', '2015-06-18 14:22:58', 286, 3707, '6cf0fb597d50a440981d14fbb003cab9028af7b5', '{"id":1,"asset_id":"54","title":"Gestione Eventi","alias":"gestione-eventi","introtext":"{source}\\r\\n<?php\\r\\n\\r\\nJLoader::register(''EventsController'', ''vendor\\/control\\/EventsController.php'');\\r\\n$eventsController = new EventsController();\\r\\n$eventsModel = new EventsModel();  \\r\\n\\r\\nswitch($eventsController->getShow()){\\r\\n  case ''list'':\\r\\n  \\r\\n    $eventsController->performListActions();\\r\\n  \\r\\n  \\techo EventsView::listTitle();\\r\\n    echo EventsView::listInstruments();\\r\\n  \\r\\n    $eventsModel->loadFilter();\\r\\n    EventsView::$list = $eventsModel->select();\\r\\n  \\r\\n    echo EventsView::showlist();\\r\\n    break;\\r\\n  \\r\\n  case ''search'':\\r\\n  \\r\\n  \\t\\/\\/ passaggio del controller chronoforms \\r\\n    Travel::getInstance()->set(''eventController'', $eventsController);\\r\\n  \\r\\n  \\techo EventsView::searchEventTitle();\\r\\n    echo EventsView::backToEventList();\\r\\n  \\r\\n    ?> {chronoforms5}event_search{\\/chronoforms5} <?php\\r\\n    \\r\\n    echo EventsView::backToEventList();\\r\\n    \\r\\n    break;\\r\\n  \\r\\n  case ''edit'':\\r\\n    $eventsController->loadMaps();\\r\\n  \\t$eventDetail = $eventsModel->getById(JRequest::getVar(''item''));\\r\\n  \\r\\n    \\/\\/ passaggio del controller e della model al chronoforms \\r\\n    Travel::getInstance()->set(''eventDetail'', $eventDetail);\\r\\n    Travel::getInstance()->set(''eventController'', $eventsController);\\r\\n    \\r\\n    echo EventsView::mainDetails($eventDetail);\\r\\n    echo EventsView::updateEventTitle();\\r\\n\\t\\r\\n  \\t?> {chronoforms5}event_update{\\/chronoforms5} <?php\\r\\n    \\r\\n    break;\\r\\n  \\r\\n  case ''partecipants'':\\r\\n    $idEvent = JRequest::getVar(''item'');\\r\\n    $eventsController->bokingListActions();\\r\\n  \\r\\n    echo EventsView::partecipateTitle($idEvent, $eventsModel->getById($idEvent)->title);\\r\\n    echo EventsView::partecipantsInstruments();\\r\\n\\r\\n    EventsView::$list = $eventsModel->partecipates($idEvent);\\r\\n    echo EventsView::showPartecipants();\\r\\n    break;\\r\\n  \\r\\n  default:\\r\\n  \\tJFactory::getApplication()->enqueueMessage(JText::_(''SOME_ERROR_OCCURRED''), ''error'');\\r\\n    break;\\r\\n}\\r\\n\\r\\n?>\\r\\n{\\/source}","fulltext":"","state":1,"catid":"2","created":"2015-05-19 15:55:29","created_by":"286","created_by_alias":"","modified":"2015-06-18 14:22:58","modified_by":"286","checked_out":"286","checked_out_time":"2015-06-18 14:22:36","publish_up":"2015-05-19 15:55:29","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":55,"ordering":"1","metakey":"","metadesc":"","access":"1","hits":"398","metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0),
(65, 2, 1, '', '2015-07-08 16:08:45', 286, 3826, '064564cb7f6e1fe0aa62f406568dd09c1859d2cc', '{"id":2,"asset_id":"55","title":"Gestione Utenti","alias":"gestione-utenti","introtext":"{source}\\r\\n<?php\\r\\n\\r\\nJLoader::register(''UsersController'', ''vendor\\/control\\/UsersController.php'');\\r\\n$usersController = new UsersController();\\r\\n$usersModel = new UsersModel();  \\r\\n\\r\\nswitch($usersController->getShow()){\\r\\n  case ''list'':\\r\\n     \\r\\n    $usersController->performListActions();\\r\\n    $usersModel->loadFilter();\\r\\n    UsersView::$list = $usersModel->select();  \\r\\n  \\r\\n    echo UsersView::listTitle();\\r\\n    echo UsersView::listInstruments();\\r\\n    echo UsersView::showlist();\\r\\n  \\r\\n    break;\\r\\n  \\r\\n  case ''search'':\\r\\n   \\r\\n    \\/\\/ passaggio del controller chronoforms \\r\\n    Travel::getInstance()->set(''usersController'', $usersController);\\r\\n    \\r\\n    echo UsersView::searchUsersTitle();\\r\\n    echo UsersView::backToUsersList();\\r\\n  \\r\\n    if ($usersController->getAction() == ''reset''){\\r\\n    \\t$usersController->clearSearchParam();\\r\\n    }\\r\\n  \\r\\n    ?> {chronoforms5}user_search{\\/chronoforms5} <?php\\r\\n  \\r\\n    echo UsersView::backToUsersList();\\r\\n  \\r\\n    break;\\r\\n  \\r\\n  case ''edit'':\\r\\n     \\r\\n    $iduser = JRequest::getVar(''item'');\\r\\n  \\t$userDetail = $usersModel->getById($iduser);\\r\\n    Travel::getInstance()->set(''userDetail'', $userDetail);\\r\\n    Travel::getInstance()->set(''usersController'', $usersController);\\r\\n  \\r\\n  \\tif ($usersController->getAction() == ''defaultavatar''){\\r\\n      $usersController->setDefaultAvatar($iduser, $userDetail->img);\\r\\n      \\/\\/JFactory::getApplication()->redirect(JUri::base().''index.php\\/utenti?show=edit&item=''.$iduser, ''Impostato Avatar di default'', ''success'');\\r\\n    }\\r\\n  \\r\\n    echo UsersView::mainDetails($userDetail);\\r\\n    echo UsersView::updateUserTitle();\\r\\n  \\r\\n    ?> {chronoforms5}user_update{\\/chronoforms5} <?php\\r\\n    \\r\\n    break;\\r\\n  \\r\\n  case ''partecipates'':\\r\\n     \\r\\n    echo ''Lista eventi a cui un utente partecipa'';\\r\\n    break;\\r\\n  \\r\\n  case ''created'':\\r\\n     \\r\\n    echo ''Lista degli eventi creati da un utente'';\\r\\n    break;\\r\\n  \\r\\n  default:\\r\\n  \\tJFactory::getApplication()->enqueueMessage(JText::_(''SOME_ERROR_OCCURRED''), ''error'');\\r\\n    break;\\r\\n}\\r\\n\\r\\n?>\\r\\n{\\/source}","fulltext":"","state":1,"catid":"2","created":"2015-05-19 15:55:57","created_by":"286","created_by_alias":"","modified":"2015-07-08 16:08:45","modified_by":"286","checked_out":"0","checked_out_time":"0000-00-00 00:00:00","publish_up":"2015-05-19 15:55:57","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":28,"ordering":"0","metakey":"","metadesc":"","access":"1","hits":"289","metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0),
(66, 2, 1, '', '2015-07-09 09:23:15', 286, 3821, '3706f57ef1f58bc7c782bd2e91359df0dfc36a31', '{"id":2,"asset_id":"55","title":"Gestione Utenti","alias":"gestione-utenti","introtext":"{source}\\r\\n<?php\\r\\n\\r\\nJLoader::register(''UsersController'', ''vendor\\/control\\/UsersController.php'');\\r\\n$usersController = new UsersController();\\r\\n$usersModel = new UsersModel();  \\r\\n\\r\\nswitch($usersController->getShow()){\\r\\n  case ''list'':\\r\\n     \\r\\n    $usersController->performListActions();\\r\\n    $usersModel->loadFilter();\\r\\n    UsersView::$list = $usersModel->select();  \\r\\n  \\r\\n    echo UsersView::listTitle();\\r\\n    echo UsersView::listInstruments();\\r\\n    echo UsersView::showlist();\\r\\n  \\r\\n    break;\\r\\n  \\r\\n  case ''search'':\\r\\n   \\r\\n    \\/\\/ passaggio del controller chronoforms \\r\\n    Travel::getInstance()->set(''usersController'', $usersController);\\r\\n    \\r\\n    echo UsersView::searchUsersTitle();\\r\\n    echo UsersView::backToUsersList();\\r\\n  \\r\\n    if ($usersController->getAction() == ''reset''){\\r\\n    \\t$usersController->clearSearchParam();\\r\\n    }\\r\\n  \\r\\n    ?> {chronoforms5}user_search{\\/chronoforms5} <?php\\r\\n  \\r\\n    echo UsersView::backToUsersList();\\r\\n  \\r\\n    break;\\r\\n  \\r\\n  case ''edit'':\\r\\n     \\r\\n    $iduser = JRequest::getVar(''item'');\\r\\n  \\t$userDetail = $usersModel->getById($iduser);\\r\\n    Travel::getInstance()->set(''userDetail'', $userDetail);\\r\\n    Travel::getInstance()->set(''usersController'', $usersController);\\r\\n  \\r\\n  \\tif ($usersController->getAction() == ''defaultavatar''){\\r\\n      $usersController->setDefaultAvatar($iduser, $userDetail->img);\\r\\n      \\/\\/JFactory::getApplication()->redirect(JUri::base().''index.php\\/utenti?show=edit&item=''.$iduser, ''Impostato Avatar di default'', ''success'');\\r\\n    }\\r\\n  \\r\\n    echo UsersView::mainDetails($userDetail);\\r\\n    echo UsersView::updateUserTitle();\\r\\n  \\r\\n    ?> {chronoforms5}user_update{\\/chronoforms5} <?php\\r\\n    \\r\\n    break;\\r\\n  \\r\\n  case ''partecipates'':\\r\\n     \\r\\n    echo ''Lista eventi a cui un utente partecipa'';\\r\\n    break;\\r\\n  \\r\\n  case ''created'':\\r\\n     \\r\\n    echo UsersView::createdTitle(12, ''Ciao'');\\r\\n    break;\\r\\n  \\r\\n  default:\\r\\n  \\tJFactory::getApplication()->enqueueMessage(JText::_(''SOME_ERROR_OCCURRED''), ''error'');\\r\\n    break;\\r\\n}\\r\\n\\r\\n?>\\r\\n{\\/source}","fulltext":"","state":1,"catid":"2","created":"2015-05-19 15:55:57","created_by":"286","created_by_alias":"","modified":"2015-07-09 09:23:15","modified_by":"286","checked_out":"0","checked_out_time":"0000-00-00 00:00:00","publish_up":"2015-05-19 15:55:57","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":29,"ordering":"0","metakey":"","metadesc":"","access":"1","hits":"336","metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0);
INSERT INTO `zcp8_ucm_history` (`version_id`, `ucm_item_id`, `ucm_type_id`, `version_note`, `save_date`, `editor_user_id`, `character_count`, `sha1_hash`, `version_data`, `keep_forever`) VALUES
(67, 2, 1, '', '2015-07-09 09:33:19', 286, 3911, '13ca8e988870ccb45540e83a0fb0db3207cd777b', '{"id":2,"asset_id":"55","title":"Gestione Utenti","alias":"gestione-utenti","introtext":"{source}\\r\\n<?php\\r\\n\\r\\nJLoader::register(''UsersController'', ''vendor\\/control\\/UsersController.php'');\\r\\n$usersController = new UsersController();\\r\\n$usersModel = new UsersModel();  \\r\\n\\r\\nswitch($usersController->getShow()){\\r\\n  case ''list'':\\r\\n     \\r\\n    $usersController->performListActions();\\r\\n    $usersModel->loadFilter();\\r\\n    UsersView::$list = $usersModel->select();  \\r\\n  \\r\\n    echo UsersView::listTitle();\\r\\n    echo UsersView::listInstruments();\\r\\n    echo UsersView::showlist();\\r\\n  \\r\\n    break;\\r\\n  \\r\\n  case ''search'':\\r\\n   \\r\\n    \\/\\/ passaggio del controller chronoforms \\r\\n    Travel::getInstance()->set(''usersController'', $usersController);\\r\\n    \\r\\n    echo UsersView::searchUsersTitle();\\r\\n    echo UsersView::backToUsersList();\\r\\n  \\r\\n    if ($usersController->getAction() == ''reset''){\\r\\n    \\t$usersController->clearSearchParam();\\r\\n    }\\r\\n  \\r\\n    ?> {chronoforms5}user_search{\\/chronoforms5} <?php\\r\\n  \\r\\n    echo UsersView::backToUsersList();\\r\\n  \\r\\n    break;\\r\\n  \\r\\n  case ''edit'':\\r\\n     \\r\\n    $iduser = JRequest::getVar(''item'');\\r\\n  \\t$userDetail = $usersModel->getById($iduser);\\r\\n    Travel::getInstance()->set(''userDetail'', $userDetail);\\r\\n    Travel::getInstance()->set(''usersController'', $usersController);\\r\\n  \\r\\n  \\tif ($usersController->getAction() == ''defaultavatar''){\\r\\n      $usersController->setDefaultAvatar($iduser, $userDetail->img);\\r\\n      \\/\\/JFactory::getApplication()->redirect(JUri::base().''index.php\\/utenti?show=edit&item=''.$iduser, ''Impostato Avatar di default'', ''success'');\\r\\n    }\\r\\n  \\r\\n    echo UsersView::mainDetails($userDetail);\\r\\n    echo UsersView::updateUserTitle();\\r\\n  \\r\\n    ?> {chronoforms5}user_update{\\/chronoforms5} <?php\\r\\n    \\r\\n    break;\\r\\n  \\r\\n  case ''partecipates'':\\r\\n     \\r\\n    echo ''Lista eventi a cui un utente partecipa'';\\r\\n    break;\\r\\n  \\r\\n  case ''created'':\\r\\n    $iduser = JRequest::getVar(''item'');\\r\\n  \\t$userDetail = $usersModel->getById($iduser); \\r\\n    echo UsersView::createdTitle($userDetail);\\r\\n    break;\\r\\n  \\r\\n  default:\\r\\n  \\tJFactory::getApplication()->enqueueMessage(JText::_(''SOME_ERROR_OCCURRED''), ''error'');\\r\\n    break;\\r\\n}\\r\\n\\r\\n?>\\r\\n{\\/source}","fulltext":"","state":1,"catid":"2","created":"2015-05-19 15:55:57","created_by":"286","created_by_alias":"","modified":"2015-07-09 09:33:19","modified_by":"286","checked_out":"286","checked_out_time":"2015-07-09 09:23:15","publish_up":"2015-05-19 15:55:57","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":30,"ordering":"0","metakey":"","metadesc":"","access":"1","hits":"337","metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0),
(68, 2, 1, '', '2015-07-09 10:10:59', 286, 4164, 'f64df1acf78601e089ecde0ddf1c800917f9a485', '{"id":2,"asset_id":"55","title":"Gestione Utenti","alias":"gestione-utenti","introtext":"{source}\\r\\n<?php\\r\\n\\r\\nJLoader::register(''UsersController'', ''vendor\\/control\\/UsersController.php'');\\r\\n$usersController = new UsersController();\\r\\n$usersModel = new UsersModel();  \\r\\n\\r\\nswitch($usersController->getShow()){\\r\\n  case ''list'':\\r\\n     \\r\\n    $usersController->performListActions();\\r\\n    $usersModel->loadFilter();\\r\\n    UsersView::$list = $usersModel->select();  \\r\\n  \\r\\n    echo UsersView::listTitle();\\r\\n    echo UsersView::listInstruments();\\r\\n    echo UsersView::showlist();\\r\\n  \\r\\n    break;\\r\\n  \\r\\n  case ''search'':\\r\\n   \\r\\n    \\/\\/ passaggio del controller chronoforms \\r\\n    Travel::getInstance()->set(''usersController'', $usersController);\\r\\n    \\r\\n    echo UsersView::searchUsersTitle();\\r\\n    echo UsersView::backToUsersList();\\r\\n  \\r\\n    if ($usersController->getAction() == ''reset''){\\r\\n    \\t$usersController->clearSearchParam();\\r\\n    }\\r\\n  \\r\\n    ?> {chronoforms5}user_search{\\/chronoforms5} <?php\\r\\n  \\r\\n    echo UsersView::backToUsersList();\\r\\n  \\r\\n    break;\\r\\n  \\r\\n  case ''edit'':\\r\\n     \\r\\n    $iduser = JRequest::getVar(''item'');\\r\\n  \\t$userDetail = $usersModel->getById($iduser);\\r\\n    Travel::getInstance()->set(''userDetail'', $userDetail);\\r\\n    Travel::getInstance()->set(''usersController'', $usersController);\\r\\n  \\r\\n  \\tif ($usersController->getAction() == ''defaultavatar''){\\r\\n      $usersController->setDefaultAvatar($iduser, $userDetail->img);\\r\\n      \\/\\/JFactory::getApplication()->redirect(JUri::base().''index.php\\/utenti?show=edit&item=''.$iduser, ''Impostato Avatar di default'', ''success'');\\r\\n    }\\r\\n  \\r\\n    echo UsersView::mainDetails($userDetail);\\r\\n    echo UsersView::updateUserTitle();\\r\\n  \\r\\n    ?> {chronoforms5}user_update{\\/chronoforms5} <?php\\r\\n    \\r\\n    break;\\r\\n  \\r\\n  case ''partecipates'':\\r\\n     \\r\\n    echo ''Lista eventi a cui un utente partecipa'';\\r\\n    break;\\r\\n  \\r\\n  case ''created'':\\r\\n    $iduser = JRequest::getVar(''item'');\\r\\n  \\t$userDetail = $usersModel->getById($iduser); \\r\\n    echo UsersView::createdTitle($userDetail);\\r\\n  \\r\\n  \\/\\/ prendiamo in prestito la lista degli eventi\\r\\n    JLoader::register(''EventsView'', ''vendor\\/view\\/events\\/EventsView.php'');\\r\\n    EventsView::$list = $usersModel->getEventsByPlanner($iduser);\\r\\n  \\techo EventsView::showlist();\\t\\r\\n  \\r\\n    break;\\r\\n  \\r\\n  default:\\r\\n  \\tJFactory::getApplication()->enqueueMessage(JText::_(''SOME_ERROR_OCCURRED''), ''error'');\\r\\n    break;\\r\\n}\\r\\n\\r\\n?>\\r\\n{\\/source}","fulltext":"","state":1,"catid":"2","created":"2015-05-19 15:55:57","created_by":"286","created_by_alias":"","modified":"2015-07-09 10:10:59","modified_by":"286","checked_out":"286","checked_out_time":"2015-07-09 09:33:19","publish_up":"2015-05-19 15:55:57","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":31,"ordering":"0","metakey":"","metadesc":"","access":"1","hits":"341","metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0),
(69, 2, 1, '', '2015-07-09 11:08:10', 286, 4164, '79af45722cbb2909ff1637b1516ab177a51acc91', '{"id":2,"asset_id":"55","title":"Gestione Utenti","alias":"gestione-utenti","introtext":"{source}\\r\\n<?php\\r\\n\\r\\nJLoader::register(''UsersController'', ''vendor\\/control\\/UsersController.php'');\\r\\n$usersController = new UsersController();\\r\\n$usersModel = new UsersModel();  \\r\\n\\r\\nswitch($usersController->getShow()){\\r\\n  case ''list'':\\r\\n     \\r\\n    $usersController->performListActions();\\r\\n    $usersModel->loadFilter();\\r\\n    UsersView::$list = $usersModel->select();  \\r\\n  \\r\\n    echo UsersView::listTitle();\\r\\n    echo UsersView::listInstruments();\\r\\n    echo UsersView::showlist();\\r\\n  \\r\\n    break;\\r\\n  \\r\\n  case ''search'':\\r\\n   \\r\\n    \\/\\/ passaggio del controller chronoforms \\r\\n    Travel::getInstance()->set(''usersController'', $usersController);\\r\\n    \\r\\n    echo UsersView::searchUsersTitle();\\r\\n    echo UsersView::backToUsersList();\\r\\n  \\r\\n    if ($usersController->getAction() == ''reset''){\\r\\n    \\t$usersController->clearSearchParam();\\r\\n    }\\r\\n  \\r\\n    ?> {chronoforms5}user_search{\\/chronoforms5} <?php\\r\\n  \\r\\n    echo UsersView::backToUsersList();\\r\\n  \\r\\n    break;\\r\\n  \\r\\n  case ''edit'':\\r\\n     \\r\\n    $iduser = JRequest::getVar(''item'');\\r\\n  \\t$userDetail = $usersModel->getById($iduser);\\r\\n    Travel::getInstance()->set(''userDetail'', $userDetail);\\r\\n    Travel::getInstance()->set(''usersController'', $usersController);\\r\\n  \\r\\n  \\tif ($usersController->getAction() == ''defaultavatar''){\\r\\n      $usersController->setDefaultAvatar($iduser, $userDetail->img);\\r\\n      \\/\\/JFactory::getApplication()->redirect(JUri::base().''index.php\\/utenti?show=edit&item=''.$iduser, ''Impostato Avatar di default'', ''success'');\\r\\n    }\\r\\n  \\r\\n    echo UsersView::mainDetails($userDetail);\\r\\n    echo UsersView::updateUserTitle();\\r\\n  \\r\\n    ?> {chronoforms5}user_update{\\/chronoforms5} <?php\\r\\n    \\r\\n    break;\\r\\n  \\r\\n  case ''partecipates'':\\r\\n     \\r\\n    echo ''Lista eventi a cui un utente partecipa'';\\r\\n    break;\\r\\n  \\r\\n  case ''created'':\\r\\n    $iduser = JRequest::getVar(''item'');\\r\\n  \\t$userDetail = $usersModel->getById($iduser); \\r\\n    echo UsersView::bookingTitle($userDetail);\\r\\n  \\r\\n  \\/\\/ prendiamo in prestito la lista degli eventi\\r\\n    JLoader::register(''EventsView'', ''vendor\\/view\\/events\\/EventsView.php'');\\r\\n    EventsView::$list = $usersModel->getEventsByPlanner($iduser);\\r\\n  \\techo EventsView::showlist();\\t\\r\\n  \\r\\n    break;\\r\\n  \\r\\n  default:\\r\\n  \\tJFactory::getApplication()->enqueueMessage(JText::_(''SOME_ERROR_OCCURRED''), ''error'');\\r\\n    break;\\r\\n}\\r\\n\\r\\n?>\\r\\n{\\/source}","fulltext":"","state":1,"catid":"2","created":"2015-05-19 15:55:57","created_by":"286","created_by_alias":"","modified":"2015-07-09 11:08:10","modified_by":"286","checked_out":"286","checked_out_time":"2015-07-09 10:10:59","publish_up":"2015-05-19 15:55:57","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":32,"ordering":"0","metakey":"","metadesc":"","access":"1","hits":"346","metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0),
(70, 2, 1, '', '2015-07-09 11:11:46', 286, 4262, '52ee23a222a1869d460d3e77b37570bd5ebacd55', '{"id":2,"asset_id":"55","title":"Gestione Utenti","alias":"gestione-utenti","introtext":"{source}\\r\\n<?php\\r\\n\\r\\nJLoader::register(''UsersController'', ''vendor\\/control\\/UsersController.php'');\\r\\n$usersController = new UsersController();\\r\\n$usersModel = new UsersModel();  \\r\\n\\r\\nswitch($usersController->getShow()){\\r\\n  case ''list'':\\r\\n     \\r\\n    $usersController->performListActions();\\r\\n    $usersModel->loadFilter();\\r\\n    UsersView::$list = $usersModel->select();  \\r\\n  \\r\\n    echo UsersView::listTitle();\\r\\n    echo UsersView::listInstruments();\\r\\n    echo UsersView::showlist();\\r\\n  \\r\\n    break;\\r\\n  \\r\\n  case ''search'':\\r\\n   \\r\\n    \\/\\/ passaggio del controller chronoforms \\r\\n    Travel::getInstance()->set(''usersController'', $usersController);\\r\\n    \\r\\n    echo UsersView::searchUsersTitle();\\r\\n    echo UsersView::backToUsersList();\\r\\n  \\r\\n    if ($usersController->getAction() == ''reset''){\\r\\n    \\t$usersController->clearSearchParam();\\r\\n    }\\r\\n  \\r\\n    ?> {chronoforms5}user_search{\\/chronoforms5} <?php\\r\\n  \\r\\n    echo UsersView::backToUsersList();\\r\\n  \\r\\n    break;\\r\\n  \\r\\n  case ''edit'':\\r\\n     \\r\\n    $iduser = JRequest::getVar(''item'');\\r\\n  \\t$userDetail = $usersModel->getById($iduser);\\r\\n    Travel::getInstance()->set(''userDetail'', $userDetail);\\r\\n    Travel::getInstance()->set(''usersController'', $usersController);\\r\\n  \\r\\n  \\tif ($usersController->getAction() == ''defaultavatar''){\\r\\n      $usersController->setDefaultAvatar($iduser, $userDetail->img);\\r\\n      \\/\\/JFactory::getApplication()->redirect(JUri::base().''index.php\\/utenti?show=edit&item=''.$iduser, ''Impostato Avatar di default'', ''success'');\\r\\n    }\\r\\n  \\r\\n    echo UsersView::mainDetails($userDetail);\\r\\n    echo UsersView::updateUserTitle();\\r\\n  \\r\\n    ?> {chronoforms5}user_update{\\/chronoforms5} <?php\\r\\n    \\r\\n    break;\\r\\n  \\r\\n  case ''partecipates'':\\r\\n     \\r\\n    $iduser = JRequest::getVar(''item'');\\r\\n  \\t$userDetail = $usersModel->getById($iduser); \\r\\n    echo UsersView::bookingTitle($userDetail);\\r\\n  \\r\\n    break;\\r\\n  \\r\\n  case ''created'':\\r\\n    $iduser = JRequest::getVar(''item'');\\r\\n  \\t$userDetail = $usersModel->getById($iduser); \\r\\n    echo UsersView::createdTitle($userDetail);\\r\\n  \\r\\n  \\/\\/ prendiamo in prestito la lista degli eventi\\r\\n    JLoader::register(''EventsView'', ''vendor\\/view\\/events\\/EventsView.php'');\\r\\n    EventsView::$list = $usersModel->getEventsByPlanner($iduser);\\r\\n  \\techo EventsView::showlist();\\t\\r\\n  \\r\\n    break;\\r\\n  \\r\\n  default:\\r\\n  \\tJFactory::getApplication()->enqueueMessage(JText::_(''SOME_ERROR_OCCURRED''), ''error'');\\r\\n    break;\\r\\n}\\r\\n\\r\\n?>\\r\\n{\\/source}","fulltext":"","state":1,"catid":"2","created":"2015-05-19 15:55:57","created_by":"286","created_by_alias":"","modified":"2015-07-09 11:11:46","modified_by":"286","checked_out":"286","checked_out_time":"2015-07-09 11:08:10","publish_up":"2015-05-19 15:55:57","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":33,"ordering":"0","metakey":"","metadesc":"","access":"1","hits":"346","metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0),
(71, 2, 1, '', '2015-07-09 12:49:23', 286, 4307, '67dc5cb7695195a8fc663d088300b47d5858481e', '{"id":2,"asset_id":"55","title":"Gestione Utenti","alias":"gestione-utenti","introtext":"{source}\\r\\n<?php\\r\\n\\r\\nJLoader::register(''UsersController'', ''vendor\\/control\\/UsersController.php'');\\r\\n$usersController = new UsersController();\\r\\n$usersModel = new UsersModel();  \\r\\n\\r\\nswitch($usersController->getShow()){\\r\\n  case ''list'':\\r\\n     \\r\\n    $usersController->performListActions();\\r\\n    $usersModel->loadFilter();\\r\\n    UsersView::$list = $usersModel->select();  \\r\\n  \\r\\n    echo UsersView::listTitle();\\r\\n    echo UsersView::listInstruments();\\r\\n    echo UsersView::showlist();\\r\\n  \\r\\n    break;\\r\\n  \\r\\n  case ''search'':\\r\\n   \\r\\n    \\/\\/ passaggio del controller chronoforms \\r\\n    Travel::getInstance()->set(''usersController'', $usersController);\\r\\n    \\r\\n    echo UsersView::searchUsersTitle();\\r\\n    echo UsersView::backToUsersList();\\r\\n  \\r\\n    if ($usersController->getAction() == ''reset''){\\r\\n    \\t$usersController->clearSearchParam();\\r\\n    }\\r\\n  \\r\\n    ?> {chronoforms5}user_search{\\/chronoforms5} <?php\\r\\n  \\r\\n    echo UsersView::backToUsersList();\\r\\n  \\r\\n    break;\\r\\n  \\r\\n  case ''edit'':\\r\\n     \\r\\n    $iduser = JRequest::getVar(''item'');\\r\\n  \\t$userDetail = $usersModel->getById($iduser);\\r\\n    Travel::getInstance()->set(''userDetail'', $userDetail);\\r\\n    Travel::getInstance()->set(''usersController'', $usersController);\\r\\n  \\r\\n  \\tif ($usersController->getAction() == ''defaultavatar''){\\r\\n      $usersController->setDefaultAvatar($iduser, $userDetail->img);\\r\\n      \\/\\/JFactory::getApplication()->redirect(JUri::base().''index.php\\/utenti?show=edit&item=''.$iduser, ''Impostato Avatar di default'', ''success'');\\r\\n    }\\r\\n  \\r\\n    echo UsersView::mainDetails($userDetail);\\r\\n    echo UsersView::updateUserTitle();\\r\\n  \\r\\n    ?> {chronoforms5}user_update{\\/chronoforms5} <?php\\r\\n    \\r\\n    break;\\r\\n  \\r\\n  case ''partecipates'':\\r\\n     \\r\\n    $iduser = JRequest::getVar(''item'');\\r\\n  \\t$userDetail = $usersModel->getById($iduser); \\r\\n    echo UsersView::bookingTitle($userDetail);\\r\\n    echo UsersView::bookingInstruments();\\r\\n  \\r\\n    break;\\r\\n  \\r\\n  case ''created'':\\r\\n    $iduser = JRequest::getVar(''item'');\\r\\n  \\t$userDetail = $usersModel->getById($iduser); \\r\\n    echo UsersView::createdTitle($userDetail);\\r\\n  \\r\\n  \\/\\/ prendiamo in prestito la lista degli eventi\\r\\n    JLoader::register(''EventsView'', ''vendor\\/view\\/events\\/EventsView.php'');\\r\\n    EventsView::$list = $usersModel->getEventsByPlanner($iduser);\\r\\n  \\techo EventsView::showlist();\\t\\r\\n  \\r\\n    break;\\r\\n  \\r\\n  default:\\r\\n  \\tJFactory::getApplication()->enqueueMessage(JText::_(''SOME_ERROR_OCCURRED''), ''error'');\\r\\n    break;\\r\\n}\\r\\n\\r\\n?>\\r\\n{\\/source}","fulltext":"","state":1,"catid":"2","created":"2015-05-19 15:55:57","created_by":"286","created_by_alias":"","modified":"2015-07-09 12:49:23","modified_by":"286","checked_out":"286","checked_out_time":"2015-07-09 12:47:49","publish_up":"2015-05-19 15:55:57","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":34,"ordering":"0","metakey":"","metadesc":"","access":"1","hits":"347","metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0),
(72, 2, 1, '', '2015-07-09 12:52:48', 286, 4462, '626c8d4f6995d73588619d64769b7ce3715bca3f', '{"id":2,"asset_id":"55","title":"Gestione Utenti","alias":"gestione-utenti","introtext":"{source}\\r\\n<?php\\r\\n\\r\\nJLoader::register(''UsersController'', ''vendor\\/control\\/UsersController.php'');\\r\\n$usersController = new UsersController();\\r\\n$usersModel = new UsersModel();  \\r\\n\\r\\nswitch($usersController->getShow()){\\r\\n  case ''list'':\\r\\n     \\r\\n    $usersController->performListActions();\\r\\n    $usersModel->loadFilter();\\r\\n    UsersView::$list = $usersModel->select();  \\r\\n  \\r\\n    echo UsersView::listTitle();\\r\\n    echo UsersView::listInstruments();\\r\\n    echo UsersView::showlist();\\r\\n  \\r\\n    break;\\r\\n  \\r\\n  case ''search'':\\r\\n   \\r\\n    \\/\\/ passaggio del controller chronoforms \\r\\n    Travel::getInstance()->set(''usersController'', $usersController);\\r\\n    \\r\\n    echo UsersView::searchUsersTitle();\\r\\n    echo UsersView::backToUsersList();\\r\\n  \\r\\n    if ($usersController->getAction() == ''reset''){\\r\\n    \\t$usersController->clearSearchParam();\\r\\n    }\\r\\n  \\r\\n    ?> {chronoforms5}user_search{\\/chronoforms5} <?php\\r\\n  \\r\\n    echo UsersView::backToUsersList();\\r\\n  \\r\\n    break;\\r\\n  \\r\\n  case ''edit'':\\r\\n     \\r\\n    $iduser = JRequest::getVar(''item'');\\r\\n  \\t$userDetail = $usersModel->getById($iduser);\\r\\n    Travel::getInstance()->set(''userDetail'', $userDetail);\\r\\n    Travel::getInstance()->set(''usersController'', $usersController);\\r\\n  \\r\\n  \\tif ($usersController->getAction() == ''defaultavatar''){\\r\\n      $usersController->setDefaultAvatar($iduser, $userDetail->img);\\r\\n      \\/\\/JFactory::getApplication()->redirect(JUri::base().''index.php\\/utenti?show=edit&item=''.$iduser, ''Impostato Avatar di default'', ''success'');\\r\\n    }\\r\\n  \\r\\n    echo UsersView::mainDetails($userDetail);\\r\\n    echo UsersView::updateUserTitle();\\r\\n  \\r\\n    ?> {chronoforms5}user_update{\\/chronoforms5} <?php\\r\\n    \\r\\n    break;\\r\\n  \\r\\n  case ''partecipates'':\\r\\n     \\r\\n    $iduser = JRequest::getVar(''item'');\\r\\n  \\t$userDetail = $usersModel->getById($iduser); \\r\\n    echo UsersView::bookingTitle($userDetail);\\r\\n    echo UsersView::bookingInstruments();\\r\\n  \\r\\n    $usersController->performListActions();\\r\\n    UsersView::$list = $usersModel->getUserBookings($iduser);\\r\\n    var_dump(UsersView::$list);  \\r\\n  \\r\\n    break;\\r\\n  \\r\\n  case ''created'':\\r\\n    $iduser = JRequest::getVar(''item'');\\r\\n  \\t$userDetail = $usersModel->getById($iduser); \\r\\n    echo UsersView::createdTitle($userDetail);\\r\\n  \\r\\n  \\/\\/ prendiamo in prestito la lista degli eventi\\r\\n    JLoader::register(''EventsView'', ''vendor\\/view\\/events\\/EventsView.php'');\\r\\n    EventsView::$list = $usersModel->getEventsByPlanner($iduser);\\r\\n  \\techo EventsView::showlist();\\t\\r\\n  \\r\\n    break;\\r\\n  \\r\\n  default:\\r\\n  \\tJFactory::getApplication()->enqueueMessage(JText::_(''SOME_ERROR_OCCURRED''), ''error'');\\r\\n    break;\\r\\n}\\r\\n\\r\\n?>\\r\\n{\\/source}","fulltext":"","state":1,"catid":"2","created":"2015-05-19 15:55:57","created_by":"286","created_by_alias":"","modified":"2015-07-09 12:52:48","modified_by":"286","checked_out":"286","checked_out_time":"2015-07-09 12:49:23","publish_up":"2015-05-19 15:55:57","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":35,"ordering":"0","metakey":"","metadesc":"","access":"1","hits":"348","metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0),
(73, 2, 1, '', '2015-07-09 14:32:32', 286, 4460, '57a950ed7ab67dec11280bd10f95423396fd38f0', '{"id":2,"asset_id":"55","title":"Gestione Utenti","alias":"gestione-utenti","introtext":"{source}\\r\\n<?php\\r\\n\\r\\nJLoader::register(''UsersController'', ''vendor\\/control\\/UsersController.php'');\\r\\n$usersController = new UsersController();\\r\\n$usersModel = new UsersModel();  \\r\\n\\r\\nswitch($usersController->getShow()){\\r\\n  case ''list'':\\r\\n     \\r\\n    $usersController->performListActions();\\r\\n    $usersModel->loadFilter();\\r\\n    UsersView::$list = $usersModel->select();  \\r\\n  \\r\\n    echo UsersView::listTitle();\\r\\n    echo UsersView::listInstruments();\\r\\n    echo UsersView::showlist();\\r\\n  \\r\\n    break;\\r\\n  \\r\\n  case ''search'':\\r\\n   \\r\\n    \\/\\/ passaggio del controller chronoforms \\r\\n    Travel::getInstance()->set(''usersController'', $usersController);\\r\\n    \\r\\n    echo UsersView::searchUsersTitle();\\r\\n    echo UsersView::backToUsersList();\\r\\n  \\r\\n    if ($usersController->getAction() == ''reset''){\\r\\n    \\t$usersController->clearSearchParam();\\r\\n    }\\r\\n  \\r\\n    ?> {chronoforms5}user_search{\\/chronoforms5} <?php\\r\\n  \\r\\n    echo UsersView::backToUsersList();\\r\\n  \\r\\n    break;\\r\\n  \\r\\n  case ''edit'':\\r\\n     \\r\\n    $iduser = JRequest::getVar(''item'');\\r\\n  \\t$userDetail = $usersModel->getById($iduser);\\r\\n    Travel::getInstance()->set(''userDetail'', $userDetail);\\r\\n    Travel::getInstance()->set(''usersController'', $usersController);\\r\\n  \\r\\n  \\tif ($usersController->getAction() == ''defaultavatar''){\\r\\n      $usersController->setDefaultAvatar($iduser, $userDetail->img);\\r\\n      JFactory::getApplication()->redirect(JUri::base().''index.php\\/utenti?show=edit&item=''.$iduser, ''Impostato Avatar di default'', ''success'');\\r\\n    }\\r\\n  \\r\\n    echo UsersView::mainDetails($userDetail);\\r\\n    echo UsersView::updateUserTitle();\\r\\n  \\r\\n    ?> {chronoforms5}user_update{\\/chronoforms5} <?php\\r\\n    \\r\\n    break;\\r\\n  \\r\\n  case ''partecipates'':\\r\\n     \\r\\n    $iduser = JRequest::getVar(''item'');\\r\\n  \\t$userDetail = $usersModel->getById($iduser); \\r\\n    echo UsersView::bookingTitle($userDetail);\\r\\n    echo UsersView::bookingInstruments();\\r\\n  \\r\\n    $usersController->performListActions();\\r\\n    UsersView::$list = $usersModel->getUserBookings($iduser);\\r\\n    echo UsersView::showBooked();  \\r\\n  \\r\\n    break;\\r\\n  \\r\\n  case ''created'':\\r\\n    $iduser = JRequest::getVar(''item'');\\r\\n  \\t$userDetail = $usersModel->getById($iduser); \\r\\n    echo UsersView::createdTitle($userDetail);\\r\\n  \\r\\n  \\/\\/ prendiamo in prestito la lista degli eventi\\r\\n    JLoader::register(''EventsView'', ''vendor\\/view\\/events\\/EventsView.php'');\\r\\n    EventsView::$list = $usersModel->getEventsByPlanner($iduser);\\r\\n  \\techo EventsView::showlist();\\t\\r\\n  \\r\\n    break;\\r\\n  \\r\\n  default:\\r\\n  \\tJFactory::getApplication()->enqueueMessage(JText::_(''SOME_ERROR_OCCURRED''), ''error'');\\r\\n    break;\\r\\n}\\r\\n\\r\\n?>\\r\\n{\\/source}","fulltext":"","state":1,"catid":"2","created":"2015-05-19 15:55:57","created_by":"286","created_by_alias":"","modified":"2015-07-09 14:32:32","modified_by":"286","checked_out":"286","checked_out_time":"2015-07-09 12:52:48","publish_up":"2015-05-19 15:55:57","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":36,"ordering":"0","metakey":"","metadesc":"","access":"1","hits":"353","metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0),
(74, 2, 1, '', '2015-07-09 15:45:01', 286, 4463, 'efd58e44e7b4ff95562d626f7110f383c5c40313', '{"id":2,"asset_id":"55","title":"Gestione Utenti","alias":"gestione-utenti","introtext":"{source}\\r\\n<?php\\r\\n\\r\\nJLoader::register(''UsersController'', ''vendor\\/control\\/UsersController.php'');\\r\\n$usersController = new UsersController();\\r\\n$usersModel = new UsersModel();  \\r\\n\\r\\nswitch($usersController->getShow()){\\r\\n  case ''list'':\\r\\n     \\r\\n    $usersController->performListActions();\\r\\n    $usersModel->loadFilter();\\r\\n    UsersView::$list = $usersModel->select();  \\r\\n  \\r\\n    echo UsersView::listTitle();\\r\\n    echo UsersView::listInstruments();\\r\\n    echo UsersView::showlist();\\r\\n  \\r\\n    break;\\r\\n  \\r\\n  case ''search'':\\r\\n   \\r\\n    \\/\\/ passaggio del controller chronoforms \\r\\n    Travel::getInstance()->set(''usersController'', $usersController);\\r\\n    \\r\\n    echo UsersView::searchUsersTitle();\\r\\n    echo UsersView::backToUsersList();\\r\\n  \\r\\n    if ($usersController->getAction() == ''reset''){\\r\\n    \\t$usersController->clearSearchParam();\\r\\n    }\\r\\n  \\r\\n    ?> {chronoforms5}user_search{\\/chronoforms5} <?php\\r\\n  \\r\\n    echo UsersView::backToUsersList();\\r\\n  \\r\\n    break;\\r\\n  \\r\\n  case ''edit'':\\r\\n     \\r\\n    $iduser = JRequest::getVar(''item'');\\r\\n  \\t$userDetail = $usersModel->getById($iduser);\\r\\n    Travel::getInstance()->set(''userDetail'', $userDetail);\\r\\n    Travel::getInstance()->set(''usersController'', $usersController);\\r\\n  \\r\\n  \\tif ($usersController->getAction() == ''defaultavatar''){\\r\\n      $usersController->setDefaultAvatar($iduser, $userDetail->img);\\r\\n      JFactory::getApplication()->redirect(JUri::base().''index.php\\/utenti?show=edit&item=''.$iduser, ''Impostato Avatar di default'', ''success'');\\r\\n    }\\r\\n  \\r\\n    echo UsersView::mainDetails($userDetail);\\r\\n    echo UsersView::updateUserTitle();\\r\\n  \\r\\n    ?> {chronoforms5}user_update{\\/chronoforms5} <?php\\r\\n    \\r\\n    break;\\r\\n  \\r\\n  case ''partecipates'':\\r\\n     \\r\\n    $iduser = JRequest::getVar(''item'');\\r\\n  \\t$userDetail = $usersModel->getById($iduser); \\r\\n    echo UsersView::bookingTitle($userDetail);\\r\\n    echo UsersView::bookingInstruments();\\r\\n  \\r\\n    $usersController->performBookingActions();\\r\\n    UsersView::$list = $usersModel->getUserBookings($iduser);\\r\\n    echo UsersView::showBooked();  \\r\\n  \\r\\n    break;\\r\\n  \\r\\n  case ''created'':\\r\\n    $iduser = JRequest::getVar(''item'');\\r\\n  \\t$userDetail = $usersModel->getById($iduser); \\r\\n    echo UsersView::createdTitle($userDetail);\\r\\n  \\r\\n  \\/\\/ prendiamo in prestito la lista degli eventi\\r\\n    JLoader::register(''EventsView'', ''vendor\\/view\\/events\\/EventsView.php'');\\r\\n    EventsView::$list = $usersModel->getEventsByPlanner($iduser);\\r\\n  \\techo EventsView::showlist();\\t\\r\\n  \\r\\n    break;\\r\\n  \\r\\n  default:\\r\\n  \\tJFactory::getApplication()->enqueueMessage(JText::_(''SOME_ERROR_OCCURRED''), ''error'');\\r\\n    break;\\r\\n}\\r\\n\\r\\n?>\\r\\n{\\/source}","fulltext":"","state":1,"catid":"2","created":"2015-05-19 15:55:57","created_by":"286","created_by_alias":"","modified":"2015-07-09 15:45:01","modified_by":"286","checked_out":"286","checked_out_time":"2015-07-09 14:32:32","publish_up":"2015-05-19 15:55:57","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":37,"ordering":"0","metakey":"","metadesc":"","access":"1","hits":"358","metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0);

-- --------------------------------------------------------

--
-- Struttura della tabella `zcp8_updates`
--

CREATE TABLE IF NOT EXISTS `zcp8_updates` (
  `update_id` int(11) NOT NULL AUTO_INCREMENT,
  `update_site_id` int(11) DEFAULT '0',
  `extension_id` int(11) DEFAULT '0',
  `name` varchar(100) DEFAULT '',
  `description` text NOT NULL,
  `element` varchar(100) DEFAULT '',
  `type` varchar(20) DEFAULT '',
  `folder` varchar(20) DEFAULT '',
  `client_id` tinyint(3) DEFAULT '0',
  `version` varchar(32) DEFAULT '',
  `data` text NOT NULL,
  `detailsurl` text NOT NULL,
  `infourl` text NOT NULL,
  `extra_query` varchar(1000) DEFAULT '',
  PRIMARY KEY (`update_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Available Updates' AUTO_INCREMENT=76 ;

--
-- Dump dei dati per la tabella `zcp8_updates`
--

INSERT INTO `zcp8_updates` (`update_id`, `update_site_id`, `extension_id`, `name`, `description`, `element`, `type`, `folder`, `client_id`, `version`, `data`, `detailsurl`, `infourl`, `extra_query`) VALUES
(1, 1, 700, 'Joomla', '', 'joomla', 'file', '', 0, '3.4.3', '', 'http://update.joomla.org/core/sts/extension_sts.xml', '', ''),
(2, 3, 0, 'Armenian', '', 'pkg_hy-AM', 'package', '', 0, '3.4.3.6', '', 'http://update.joomla.org/language/details3/hy-AM_details.xml', '', ''),
(3, 3, 0, 'Malay', '', 'pkg_ms-MY', 'package', '', 0, '3.4.1.2', '', 'http://update.joomla.org/language/details3/ms-MY_details.xml', '', ''),
(4, 3, 0, 'Romanian', '', 'pkg_ro-RO', 'package', '', 0, '3.4.3.1', '', 'http://update.joomla.org/language/details3/ro-RO_details.xml', '', ''),
(5, 3, 0, 'Flemish', '', 'pkg_nl-BE', 'package', '', 0, '3.4.3.1', '', 'http://update.joomla.org/language/details3/nl-BE_details.xml', '', ''),
(6, 3, 0, 'Chinese Traditional', '', 'pkg_zh-TW', 'package', '', 0, '3.4.2.1', '', 'http://update.joomla.org/language/details3/zh-TW_details.xml', '', ''),
(7, 3, 0, 'French', '', 'pkg_fr-FR', 'package', '', 0, '3.4.3.1', '', 'http://update.joomla.org/language/details3/fr-FR_details.xml', '', ''),
(8, 3, 0, 'Galician', '', 'pkg_gl-ES', 'package', '', 0, '3.3.1.2', '', 'http://update.joomla.org/language/details3/gl-ES_details.xml', '', ''),
(9, 3, 0, 'German', '', 'pkg_de-DE', 'package', '', 0, '3.4.3.1', '', 'http://update.joomla.org/language/details3/de-DE_details.xml', '', ''),
(10, 3, 0, 'Greek', '', 'pkg_el-GR', 'package', '', 0, '3.4.2.1', '', 'http://update.joomla.org/language/details3/el-GR_details.xml', '', ''),
(11, 3, 0, 'Japanese', '', 'pkg_ja-JP', 'package', '', 0, '3.4.3.1', '', 'http://update.joomla.org/language/details3/ja-JP_details.xml', '', ''),
(12, 3, 0, 'Hebrew', '', 'pkg_he-IL', 'package', '', 0, '3.1.1.1', '', 'http://update.joomla.org/language/details3/he-IL_details.xml', '', ''),
(13, 3, 0, 'EnglishAU', '', 'pkg_en-AU', 'package', '', 0, '3.3.1.1', '', 'http://update.joomla.org/language/details3/en-AU_details.xml', '', ''),
(14, 3, 0, 'EnglishUS', '', 'pkg_en-US', 'package', '', 0, '3.3.1.1', '', 'http://update.joomla.org/language/details3/en-US_details.xml', '', ''),
(15, 3, 0, 'Hungarian', '', 'pkg_hu-HU', 'package', '', 0, '3.4.3.1', '', 'http://update.joomla.org/language/details3/hu-HU_details.xml', '', ''),
(16, 3, 0, 'Afrikaans', '', 'pkg_af-ZA', 'package', '', 0, '3.4.3.1', '', 'http://update.joomla.org/language/details3/af-ZA_details.xml', '', ''),
(17, 3, 0, 'Arabic Unitag', '', 'pkg_ar-AA', 'package', '', 0, '3.4.3.1', '', 'http://update.joomla.org/language/details3/ar-AA_details.xml', '', ''),
(18, 3, 0, 'Belarusian', '', 'pkg_be-BY', 'package', '', 0, '3.2.1.1', '', 'http://update.joomla.org/language/details3/be-BY_details.xml', '', ''),
(19, 3, 0, 'Bulgarian', '', 'pkg_bg-BG', 'package', '', 0, '3.3.0.1', '', 'http://update.joomla.org/language/details3/bg-BG_details.xml', '', ''),
(20, 3, 0, 'Catalan', '', 'pkg_ca-ES', 'package', '', 0, '3.4.2.1', '', 'http://update.joomla.org/language/details3/ca-ES_details.xml', '', ''),
(21, 3, 0, 'Chinese Simplified', '', 'pkg_zh-CN', 'package', '', 0, '3.4.1.1', '', 'http://update.joomla.org/language/details3/zh-CN_details.xml', '', ''),
(22, 3, 0, 'Croatian', '', 'pkg_hr-HR', 'package', '', 0, '3.4.3.2', '', 'http://update.joomla.org/language/details3/hr-HR_details.xml', '', ''),
(23, 3, 0, 'Czech', '', 'pkg_cs-CZ', 'package', '', 0, '3.4.1.1', '', 'http://update.joomla.org/language/details3/cs-CZ_details.xml', '', ''),
(24, 3, 0, 'Danish', '', 'pkg_da-DK', 'package', '', 0, '3.4.2.2', '', 'http://update.joomla.org/language/details3/da-DK_details.xml', '', ''),
(25, 3, 0, 'Dutch', '', 'pkg_nl-NL', 'package', '', 0, '3.4.3.1', '', 'http://update.joomla.org/language/details3/nl-NL_details.xml', '', ''),
(26, 3, 0, 'Estonian', '', 'pkg_et-EE', 'package', '', 0, '3.4.2.1', '', 'http://update.joomla.org/language/details3/et-EE_details.xml', '', ''),
(27, 3, 0, 'Italian', '', 'pkg_it-IT', 'package', '', 0, '3.4.3.1', '', 'http://update.joomla.org/language/details3/it-IT_details.xml', '', ''),
(28, 3, 0, 'Khmer', '', 'pkg_km-KH', 'package', '', 0, '3.4.3.1', '', 'http://update.joomla.org/language/details3/km-KH_details.xml', '', ''),
(29, 3, 0, 'Korean', '', 'pkg_ko-KR', 'package', '', 0, '3.4.3.4', '', 'http://update.joomla.org/language/details3/ko-KR_details.xml', '', ''),
(30, 3, 0, 'Latvian', '', 'pkg_lv-LV', 'package', '', 0, '3.4.3.1', '', 'http://update.joomla.org/language/details3/lv-LV_details.xml', '', ''),
(31, 3, 0, 'Macedonian', '', 'pkg_mk-MK', 'package', '', 0, '3.4.3.1', '', 'http://update.joomla.org/language/details3/mk-MK_details.xml', '', ''),
(32, 3, 0, 'Norwegian Bokmal', '', 'pkg_nb-NO', 'package', '', 0, '3.4.2.1', '', 'http://update.joomla.org/language/details3/nb-NO_details.xml', '', ''),
(33, 3, 0, 'Norwegian Nynorsk', '', 'pkg_nn-NO', 'package', '', 0, '3.4.2.1', '', 'http://update.joomla.org/language/details3/nn-NO_details.xml', '', ''),
(34, 3, 0, 'Persian', '', 'pkg_fa-IR', 'package', '', 0, '3.4.3.2', '', 'http://update.joomla.org/language/details3/fa-IR_details.xml', '', ''),
(35, 3, 0, 'Polish', '', 'pkg_pl-PL', 'package', '', 0, '3.4.2.1', '', 'http://update.joomla.org/language/details3/pl-PL_details.xml', '', ''),
(36, 3, 0, 'Portuguese', '', 'pkg_pt-PT', 'package', '', 0, '3.4.1.1', '', 'http://update.joomla.org/language/details3/pt-PT_details.xml', '', ''),
(37, 3, 0, 'Russian', '', 'pkg_ru-RU', 'package', '', 0, '3.4.1.3', '', 'http://update.joomla.org/language/details3/ru-RU_details.xml', '', ''),
(38, 3, 0, 'Slovak', '', 'pkg_sk-SK', 'package', '', 0, '3.4.1.2', '', 'http://update.joomla.org/language/details3/sk-SK_details.xml', '', ''),
(39, 3, 0, 'Swedish', '', 'pkg_sv-SE', 'package', '', 0, '3.4.1.3', '', 'http://update.joomla.org/language/details3/sv-SE_details.xml', '', ''),
(40, 3, 0, 'Syriac', '', 'pkg_sy-IQ', 'package', '', 0, '3.4.3.1', '', 'http://update.joomla.org/language/details3/sy-IQ_details.xml', '', ''),
(41, 3, 0, 'Tamil', '', 'pkg_ta-IN', 'package', '', 0, '3.4.3.2', '', 'http://update.joomla.org/language/details3/ta-IN_details.xml', '', ''),
(42, 3, 0, 'Thai', '', 'pkg_th-TH', 'package', '', 0, '3.4.2.1', '', 'http://update.joomla.org/language/details3/th-TH_details.xml', '', ''),
(43, 3, 0, 'Turkish', '', 'pkg_tr-TR', 'package', '', 0, '3.4.3.1', '', 'http://update.joomla.org/language/details3/tr-TR_details.xml', '', ''),
(44, 3, 0, 'Ukrainian', '', 'pkg_uk-UA', 'package', '', 0, '3.3.3.15', '', 'http://update.joomla.org/language/details3/uk-UA_details.xml', '', ''),
(45, 3, 0, 'Uyghur', '', 'pkg_ug-CN', 'package', '', 0, '3.3.0.1', '', 'http://update.joomla.org/language/details3/ug-CN_details.xml', '', ''),
(46, 3, 0, 'Albanian', '', 'pkg_sq-AL', 'package', '', 0, '3.1.1.1', '', 'http://update.joomla.org/language/details3/sq-AL_details.xml', '', ''),
(47, 3, 0, 'Hindi', '', 'pkg_hi-IN', 'package', '', 0, '3.3.6.1', '', 'http://update.joomla.org/language/details3/hi-IN_details.xml', '', ''),
(48, 3, 0, 'Portuguese Brazil', '', 'pkg_pt-BR', 'package', '', 0, '3.4.3.1', '', 'http://update.joomla.org/language/details3/pt-BR_details.xml', '', ''),
(49, 3, 0, 'Serbian Latin', '', 'pkg_sr-YU', 'package', '', 0, '3.4.2.1', '', 'http://update.joomla.org/language/details3/sr-YU_details.xml', '', ''),
(50, 3, 0, 'Spanish', '', 'pkg_es-ES', 'package', '', 0, '3.4.3.1', '', 'http://update.joomla.org/language/details3/es-ES_details.xml', '', ''),
(51, 3, 0, 'Bosnian', '', 'pkg_bs-BA', 'package', '', 0, '3.4.1.1', '', 'http://update.joomla.org/language/details3/bs-BA_details.xml', '', ''),
(52, 3, 0, 'Serbian Cyrillic', '', 'pkg_sr-RS', 'package', '', 0, '3.4.2.1', '', 'http://update.joomla.org/language/details3/sr-RS_details.xml', '', ''),
(53, 3, 0, 'Vietnamese', '', 'pkg_vi-VN', 'package', '', 0, '3.2.1.1', '', 'http://update.joomla.org/language/details3/vi-VN_details.xml', '', ''),
(54, 3, 0, 'Bahasa Indonesia', '', 'pkg_id-ID', 'package', '', 0, '3.3.0.2', '', 'http://update.joomla.org/language/details3/id-ID_details.xml', '', ''),
(55, 3, 0, 'Finnish', '', 'pkg_fi-FI', 'package', '', 0, '3.4.2.1', '', 'http://update.joomla.org/language/details3/fi-FI_details.xml', '', ''),
(56, 3, 0, 'Swahili', '', 'pkg_sw-KE', 'package', '', 0, '3.4.3.1', '', 'http://update.joomla.org/language/details3/sw-KE_details.xml', '', ''),
(57, 3, 0, 'Montenegrin', '', 'pkg_srp-ME', 'package', '', 0, '3.3.1.1', '', 'http://update.joomla.org/language/details3/srp-ME_details.xml', '', ''),
(58, 3, 0, 'EnglishCA', '', 'pkg_en-CA', 'package', '', 0, '3.3.6.1', '', 'http://update.joomla.org/language/details3/en-CA_details.xml', '', ''),
(59, 3, 0, 'FrenchCA', '', 'pkg_fr-CA', 'package', '', 0, '3.4.3.1', '', 'http://update.joomla.org/language/details3/fr-CA_details.xml', '', ''),
(60, 3, 0, 'Welsh', '', 'pkg_cy-GB', 'package', '', 0, '3.3.0.2', '', 'http://update.joomla.org/language/details3/cy-GB_details.xml', '', ''),
(61, 3, 0, 'Sinhala', '', 'pkg_si-LK', 'package', '', 0, '3.3.1.1', '', 'http://update.joomla.org/language/details3/si-LK_details.xml', '', ''),
(62, 3, 0, 'Dari Persian', '', 'pkg_prs-AF', 'package', '', 0, '3.4.3.1', '', 'http://update.joomla.org/language/details3/prs-AF_details.xml', '', ''),
(63, 7, 10006, 'Chronoforms v5', 'Latest update of Chronoforms v5 for Joomla 1.6, 2.5 and 3.x', 'com_chronoforms5', 'component', '', 1, '5.0.10', '', 'http://www.chronoengine.com/chrono_joomla_updates/chronoforms5.xml', 'http://www.chronoengine.com', ''),
(64, 6, 0, 'Content - Custom Page Navigation', '', 'custompagenavigation', 'plugin', 'content', 0, '1.4.3', '', 'http://updates.e-max.it/joomla/plugins/custompagenavigation.xml', '', ''),
(65, 6, 0, 'Content - Social Share Buttons', '', 'socialsharebuttons', 'plugin', 'content', 0, '1.2.2', '', 'http://updates.e-max.it/joomla/plugins/socialsharebuttons.xml', '', ''),
(66, 6, 0, 'Content - Easy Open Graph', '', 'easyopengraph', 'plugin', 'content', 0, '1.1.3', '', 'http://updates.e-max.it/joomla/plugins/easyopengraph.xml', '', ''),
(67, 6, 0, 'Content - Google+ Page Badge', '', 'googlepagebadge', 'plugin', 'content', 0, '1.1.2', '', 'http://updates.e-max.it/joomla/plugins/googlepagebadge.xml', '', ''),
(68, 6, 0, 'Content - Google+ Profile Widget', '', 'googleprofilewidget', 'plugin', 'content', 0, '1.1.2', '', 'http://updates.e-max.it/joomla/plugins/googleprofilewidget.xml', '', ''),
(69, 6, 0, 'Content - Codice Fiscale', '', 'codicefiscale', 'plugin', 'content', 0, '1.0.3', '', 'http://updates.e-max.it/joomla/plugins/codicefiscale.xml', '', ''),
(70, 6, 0, 'Content - Exchange Rate Calculator', '', 'exchangeratecalculator', 'plugin', 'content', 0, '1.0.2', '', 'http://updates.e-max.it/joomla/plugins/exchangeratecalculator.xml', '', ''),
(71, 6, 0, 'Google+ Page Badge', '', 'mod_googlepagebadge', 'module', '', 0, '1.1.2', '', 'http://updates.e-max.it/joomla/modules/googlepagebadge.xml', '', ''),
(72, 6, 0, 'Google+ Profile Widget', '', 'mod_googleprofilewidget', 'module', '', 0, '1.1.2', '', 'http://updates.e-max.it/joomla/modules/googleprofilewidget.xml', '', ''),
(73, 6, 0, 'Codice Fiscale', '', 'mod_codicefiscale', 'module', '', 0, '1.0.3', '', 'http://updates.e-max.it/joomla/modules/codicefiscale.xml', '', ''),
(74, 6, 0, 'Exchange Rate Calculator', '', 'mod_exchangeratecalculator', 'module', '', 0, '1.0.2', '', 'http://updates.e-max.it/joomla/modules/exchangeratecalculator.xml', '', ''),
(75, 6, 0, 'Google Simple Authorship', '', 'pkg_googlesimpleauthorship', 'package', '', 0, '1.0.1', '', 'http://updates.e-max.it/joomla/packages/googlesimpleauthorship.xml', '', '');

-- --------------------------------------------------------

--
-- Struttura della tabella `zcp8_update_sites`
--

CREATE TABLE IF NOT EXISTS `zcp8_update_sites` (
  `update_site_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT '',
  `type` varchar(20) DEFAULT '',
  `location` text NOT NULL,
  `enabled` int(11) DEFAULT '0',
  `last_check_timestamp` bigint(20) DEFAULT '0',
  `extra_query` varchar(1000) DEFAULT '',
  PRIMARY KEY (`update_site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Update Sites' AUTO_INCREMENT=8 ;

--
-- Dump dei dati per la tabella `zcp8_update_sites`
--

INSERT INTO `zcp8_update_sites` (`update_site_id`, `name`, `type`, `location`, `enabled`, `last_check_timestamp`, `extra_query`) VALUES
(1, 'Joomla Core', 'collection', 'http://update.joomla.org/core/list.xml', 1, 1438274403, ''),
(2, 'Joomla Extension Directory', 'collection', 'http://update.joomla.org/jed/list.xml', 1, 1438274403, ''),
(3, 'Accredited Joomla! Translations', 'collection', 'http://update.joomla.org/language/translationlist_3.xml', 1, 1438274403, ''),
(4, 'WebInstaller Update Site', 'extension', 'http://appscdn.joomla.org/webapps/jedapps/webinstaller.xml', 1, 1438274403, ''),
(5, 'NoNumber Sourcerer', 'extension', 'http://download.nonumber.nl/updates.php?e=sourcerer&type=.zip', 1, 1438274403, ''),
(6, '', 'collection', 'http://updates.e-max.it/joomla/updates.xml', 1, 1438274403, ''),
(7, 'Chronoforms v5 update server', 'extension', 'http://www.chronoengine.com/chrono_joomla_updates/chronoforms5.xml', 1, 1438274403, '');

-- --------------------------------------------------------

--
-- Struttura della tabella `zcp8_update_sites_extensions`
--

CREATE TABLE IF NOT EXISTS `zcp8_update_sites_extensions` (
  `update_site_id` int(11) NOT NULL DEFAULT '0',
  `extension_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`update_site_id`,`extension_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Links extensions to update sites';

--
-- Dump dei dati per la tabella `zcp8_update_sites_extensions`
--

INSERT INTO `zcp8_update_sites_extensions` (`update_site_id`, `extension_id`) VALUES
(1, 700),
(2, 700),
(3, 600),
(4, 10000),
(6, 10004),
(7, 10006);

-- --------------------------------------------------------

--
-- Struttura della tabella `zcp8_usergroups`
--

CREATE TABLE IF NOT EXISTS `zcp8_usergroups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Adjacency List Reference Id',
  `lft` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set lft.',
  `rgt` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set rgt.',
  `title` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_usergroup_parent_title_lookup` (`parent_id`,`title`),
  KEY `idx_usergroup_title_lookup` (`title`),
  KEY `idx_usergroup_adjacency_lookup` (`parent_id`),
  KEY `idx_usergroup_nested_set_lookup` (`lft`,`rgt`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Dump dei dati per la tabella `zcp8_usergroups`
--

INSERT INTO `zcp8_usergroups` (`id`, `parent_id`, `lft`, `rgt`, `title`) VALUES
(1, 0, 1, 18, 'Public'),
(2, 1, 8, 15, 'Registered'),
(3, 2, 9, 14, 'Author'),
(4, 3, 10, 13, 'Editor'),
(5, 4, 11, 12, 'Publisher'),
(6, 1, 4, 7, 'Manager'),
(7, 6, 5, 6, 'Administrator'),
(8, 1, 16, 17, 'Super Users'),
(9, 1, 2, 3, 'Guest');

-- --------------------------------------------------------

--
-- Struttura della tabella `zcp8_users`
--

CREATE TABLE IF NOT EXISTS `zcp8_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `username` varchar(150) NOT NULL DEFAULT '',
  `email` varchar(100) NOT NULL DEFAULT '',
  `password` varchar(100) NOT NULL DEFAULT '',
  `block` tinyint(4) NOT NULL DEFAULT '0',
  `sendEmail` tinyint(4) DEFAULT '0',
  `registerDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastvisitDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `activation` varchar(100) NOT NULL DEFAULT '',
  `params` text NOT NULL,
  `lastResetTime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Date of last password reset',
  `resetCount` int(11) NOT NULL DEFAULT '0' COMMENT 'Count of password resets since lastResetTime',
  `otpKey` varchar(1000) NOT NULL DEFAULT '' COMMENT 'Two factor authentication encrypted keys',
  `otep` varchar(1000) NOT NULL DEFAULT '' COMMENT 'One time emergency passwords',
  PRIMARY KEY (`id`),
  KEY `idx_name` (`name`),
  KEY `idx_block` (`block`),
  KEY `username` (`username`),
  KEY `email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=288 ;

--
-- Dump dei dati per la tabella `zcp8_users`
--

INSERT INTO `zcp8_users` (`id`, `name`, `username`, `email`, `password`, `block`, `sendEmail`, `registerDate`, `lastvisitDate`, `activation`, `params`, `lastResetTime`, `resetCount`, `otpKey`, `otep`) VALUES
(286, 'Super User', 'Studio Leaves', 'pietro@studioleaves.com', '$P$DgPzcoN3EsNEFeKslAIJi8RQCXak9Z.', 0, 1, '2015-05-18 14:13:36', '2015-07-30 16:40:00', '0', '', '0000-00-00 00:00:00', 0, '', ''),
(287, 'Signor Wherabout', 'Signor Wherabout', 'pedro.bit@hotmail.it', '$P$DCBwLbxjRUdxNf53JhndZc0fs1lK50/', 0, 0, '2015-05-19 14:33:08', '2015-08-12 10:50:01', '', '{"admin_style":"","admin_language":"","language":"","editor":"","helpsite":"","timezone":""}', '0000-00-00 00:00:00', 0, '', '');

-- --------------------------------------------------------

--
-- Struttura della tabella `zcp8_user_keys`
--

CREATE TABLE IF NOT EXISTS `zcp8_user_keys` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `series` varchar(255) NOT NULL,
  `invalid` tinyint(4) NOT NULL,
  `time` varchar(200) NOT NULL,
  `uastring` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `series` (`series`),
  UNIQUE KEY `series_2` (`series`),
  UNIQUE KEY `series_3` (`series`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struttura della tabella `zcp8_user_notes`
--

CREATE TABLE IF NOT EXISTS `zcp8_user_notes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `catid` int(10) unsigned NOT NULL DEFAULT '0',
  `subject` varchar(100) NOT NULL DEFAULT '',
  `body` text NOT NULL,
  `state` tinyint(3) NOT NULL DEFAULT '0',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `created_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_user_id` int(10) unsigned NOT NULL,
  `modified_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `review_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `idx_user_id` (`user_id`),
  KEY `idx_category_id` (`catid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struttura della tabella `zcp8_user_profiles`
--

CREATE TABLE IF NOT EXISTS `zcp8_user_profiles` (
  `user_id` int(11) NOT NULL,
  `profile_key` varchar(100) NOT NULL,
  `profile_value` varchar(255) NOT NULL,
  `ordering` int(11) NOT NULL DEFAULT '0',
  UNIQUE KEY `idx_user_id_profile_key` (`user_id`,`profile_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Simple user profile storage table';

-- --------------------------------------------------------

--
-- Struttura della tabella `zcp8_user_usergroup_map`
--

CREATE TABLE IF NOT EXISTS `zcp8_user_usergroup_map` (
  `user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Foreign Key to #__users.id',
  `group_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Foreign Key to #__usergroups.id',
  PRIMARY KEY (`user_id`,`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `zcp8_user_usergroup_map`
--

INSERT INTO `zcp8_user_usergroup_map` (`user_id`, `group_id`) VALUES
(286, 8),
(287, 2);

-- --------------------------------------------------------

--
-- Struttura della tabella `zcp8_viewlevels`
--

CREATE TABLE IF NOT EXISTS `zcp8_viewlevels` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `title` varchar(100) NOT NULL DEFAULT '',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `rules` varchar(5120) NOT NULL COMMENT 'JSON encoded access control.',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_assetgroup_title_lookup` (`title`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dump dei dati per la tabella `zcp8_viewlevels`
--

INSERT INTO `zcp8_viewlevels` (`id`, `title`, `ordering`, `rules`) VALUES
(1, 'Public', 0, '[1]'),
(2, 'Registered', 1, '[6,2,8]'),
(3, 'Special', 2, '[6,3,8]'),
(5, 'Guest', 0, '[9]'),
(6, 'Super Users', 0, '[8]');

-- --------------------------------------------------------

--
-- Struttura della tabella `zcp8_weblinks`
--

CREATE TABLE IF NOT EXISTS `zcp8_weblinks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `catid` int(11) NOT NULL DEFAULT '0',
  `title` varchar(250) NOT NULL DEFAULT '',
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `url` varchar(250) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `hits` int(11) NOT NULL DEFAULT '0',
  `state` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(11) NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `access` int(11) NOT NULL DEFAULT '1',
  `params` text NOT NULL,
  `language` char(7) NOT NULL DEFAULT '',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) unsigned NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) unsigned NOT NULL DEFAULT '0',
  `metakey` text NOT NULL,
  `metadesc` text NOT NULL,
  `metadata` text NOT NULL,
  `featured` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'Set if link is featured.',
  `xreference` varchar(50) NOT NULL COMMENT 'A reference to enable linkages to external data sets.',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `version` int(10) unsigned NOT NULL DEFAULT '1',
  `images` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_access` (`access`),
  KEY `idx_checkout` (`checked_out`),
  KEY `idx_state` (`state`),
  KEY `idx_catid` (`catid`),
  KEY `idx_createdby` (`created_by`),
  KEY `idx_featured_catid` (`featured`,`catid`),
  KEY `idx_language` (`language`),
  KEY `idx_xreference` (`xreference`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struttura per la vista `activated_events`
--
DROP TABLE IF EXISTS `activated_events`;

CREATE VIEW `activated_events` AS select `e`.`idevent` AS `idevent`,`e`.`iduser` AS `iduser`,`e`.`idcategory` AS `idcategory`,`e`.`title` AS `title`,`e`.`description` AS `description`,`e`.`img` AS `img`,`e`.`city` AS `city`,`e`.`address` AS `address`,`e`.`date` AS `date`,`e`.`hour` AS `hour`,`e`.`sex` AS `sex`,`e`.`agemin` AS `agemin`,`e`.`agemax` AS `agemax`,`e`.`sittotal` AS `sittotal`,`e`.`sitcurrent` AS `sitcurrent`,`e`.`latitude` AS `latitude`,`e`.`longitude` AS `longitude`,`e`.`scope` AS `scope`,`e`.`creationdate` AS `creationdate`,`e`.`updatetime` AS `updatetime`,`e`.`activated` AS `activated`,`u`.`img` AS `userimg`,`u`.`job` AS `job` from (`events` `e` join `activated_users` `u` on((`e`.`iduser` = `u`.`iduser`))) where (`e`.`activated` = 1) order by `e`.`date`,`e`.`hour`;

-- --------------------------------------------------------

--
-- Struttura per la vista `activated_users`
--
DROP TABLE IF EXISTS `activated_users`;

CREATE VIEW `activated_users` AS select `users`.`iduser` AS `iduser`,`users`.`email` AS `email`,`users`.`idgoogle` AS `idgoogle`,`users`.`idfacebook` AS `idfacebook`,`users`.`name` AS `name`,`users`.`surname` AS `surname`,`users`.`img` AS `img`,`users`.`birthdate` AS `birthdate`,`users`.`sex` AS `sex`,`users`.`job` AS `job`,`users`.`interests` AS `interests`,`users`.`creationdate` AS `creationdate`,`users`.`updatetime` AS `updatetime`,`users`.`password` AS `password`,`users`.`activated` AS `activated` from `users` where (`users`.`activated` = 1);

-- --------------------------------------------------------

--
-- Struttura per la vista `av_count_created_events`
--
DROP TABLE IF EXISTS `av_count_created_events`;

CREATE VIEW `av_count_created_events` AS select `events`.`date` AS `date`,`events`.`iduser` AS `iduser`,count(`events`.`idevent`) AS `occurance` from `events` where (`events`.`activated` = 1) group by `events`.`iduser`,`events`.`date` order by `events`.`date`;

-- --------------------------------------------------------

--
-- Struttura per la vista `av_date_togo_events`
--
DROP TABLE IF EXISTS `av_date_togo_events`;

CREATE VIEW `av_date_togo_events` AS select `p`.`iduser` AS `iduser`,count(`e`.`idevent`) AS `occurrence`,`e`.`date` AS `date` from (`partecipates` `p` join `events` `e` on(((`p`.`idevent` = `e`.`idevent`) and (`p`.`confirmed` = 1)))) group by `p`.`iduser`,`e`.`date`;

-- --------------------------------------------------------

--
-- Struttura per la vista `av_my_events`
--
DROP TABLE IF EXISTS `av_my_events`;

CREATE VIEW `av_my_events` AS select `e`.`idevent` AS `idevent`,`e`.`iduser` AS `iduser`,`e`.`idcategory` AS `idcategory`,`e`.`title` AS `title`,`e`.`description` AS `description`,`e`.`img` AS `img`,`e`.`city` AS `city`,`e`.`address` AS `address`,`e`.`date` AS `date`,`e`.`hour` AS `hour`,`e`.`sex` AS `sex`,`e`.`agemin` AS `agemin`,`e`.`agemax` AS `agemax`,`e`.`sittotal` AS `sittotal`,`e`.`sitcurrent` AS `sitcurrent`,`e`.`latitude` AS `latitude`,`e`.`longitude` AS `longitude`,`e`.`scope` AS `scope`,`e`.`creationdate` AS `creationdate`,`e`.`updatetime` AS `updatetime`,`e`.`activated` AS `activated`,if(isnull(`p`.`confirmed`),999,`p`.`confirmed`) AS `partecipate`,`u`.`img` AS `userimg` from ((`events` `e` join `users` `u` on((`e`.`iduser` = `u`.`iduser`))) left join `partecipates` `p` on(((`e`.`idevent` = `p`.`idevent`) and (`e`.`iduser` = `p`.`iduser`))));

-- --------------------------------------------------------

--
-- Struttura per la vista `av_my_friends`
--
DROP TABLE IF EXISTS `av_my_friends`;

CREATE VIEW `av_my_friends` AS select `f`.`iduser` AS `iduser`,`f`.`idfriend` AS `idfriend`,`f`.`accepted` AS `accepted`,`u`.`name` AS `name`,`u`.`surname` AS `surname`,`u`.`img` AS `img` from (`user_friends` `f` join `users` `u` on((`f`.`idfriend` = `u`.`iduser`)));

-- --------------------------------------------------------

--
-- Struttura per la vista `av_togo_events`
--
DROP TABLE IF EXISTS `av_togo_events`;

CREATE VIEW `av_togo_events` AS select `p`.`iduser` AS `participant`,`p`.`idevent` AS `idevent`,`p`.`confirmed` AS `partecipate`,`e`.`iduser` AS `creator`,`e`.`sex` AS `sex`,`e`.`sittotal` AS `sittotal`,`e`.`sitcurrent` AS `sitcurrent`,`e`.`idcategory` AS `idcategory`,`e`.`title` AS `title`,`e`.`city` AS `city`,`e`.`address` AS `address`,`e`.`agemin` AS `agemin`,`e`.`agemax` AS `agemax`,`e`.`date` AS `date`,`e`.`hour` AS `hour`,`e`.`scope` AS `scope`,`e`.`latitude` AS `latitude`,`e`.`longitude` AS `longitude`,`e`.`img` AS `imgcover`,`u`.`img` AS `userimg` from ((`partecipates` `p` join `events` `e` on(((`p`.`idevent` = `e`.`idevent`) and (`p`.`confirmed` = 1)))) join `users` `u` on((`e`.`iduser` = `u`.`iduser`))) where (`e`.`activated` = 1) order by `e`.`date`;

-- --------------------------------------------------------

--
-- Struttura per la vista `confirmed_partecipates`
--
DROP TABLE IF EXISTS `confirmed_partecipates`;

CREATE VIEW `confirmed_partecipates` AS select `u`.`iduser` AS `iduser`,`u`.`name` AS `name`,`u`.`surname` AS `surname`,`u`.`img` AS `img`,`p`.`idevent` AS `idevent` from (`partecipates` `p` join `users` `u` on((`p`.`iduser` = `u`.`iduser`))) where (`p`.`confirmed` = 1);

-- --------------------------------------------------------

--
-- Struttura per la vista `cp_booked_by_users`
--
DROP TABLE IF EXISTS `cp_booked_by_users`;

CREATE VIEW `cp_booked_by_users` AS select `p`.`id` AS `id_booking`,`p`.`iduser` AS `id_partecipant`,`p`.`confirmed` AS `confirmed`,`e`.`idevent` AS `idevent`,`e`.`title` AS `title`,`e`.`date` AS `date`,`e`.`city` AS `city`,if((`c1`.`idsupercategory` is not null),concat(`c2`.`name`,' > ',`c1`.`name`),`c1`.`name`) AS `category_name` from (((`partecipates` `p` join `events` `e` on((`p`.`idevent` = `e`.`idevent`))) join `categories` `c1` on((`e`.`idcategory` = `c1`.`id`))) left join `categories` `c2` on((`c1`.`idsupercategory` = `c2`.`id`)));

-- --------------------------------------------------------

--
-- Struttura per la vista `cp_count_booked_events`
--
DROP TABLE IF EXISTS `cp_count_booked_events`;

CREATE VIEW `cp_count_booked_events` AS select `partecipates`.`iduser` AS `iduser`,count(`partecipates`.`id`) AS `partecipates_events` from `partecipates` group by `partecipates`.`iduser`;

-- --------------------------------------------------------

--
-- Struttura per la vista `cp_count_created_events`
--
DROP TABLE IF EXISTS `cp_count_created_events`;

CREATE VIEW `cp_count_created_events` AS select `events`.`iduser` AS `iduser`,count(`events`.`idevent`) AS `created_events` from `events` group by `events`.`iduser`;

-- --------------------------------------------------------

--
-- Struttura per la vista `cp_events_list`
--
DROP TABLE IF EXISTS `cp_events_list`;

CREATE VIEW `cp_events_list` AS select `e`.`idevent` AS `idevent`,`e`.`iduser` AS `iduser`,`e`.`idcategory` AS `idcategory`,`e`.`title` AS `title`,`e`.`description` AS `description`,`e`.`img` AS `img`,`e`.`city` AS `city`,`e`.`address` AS `address`,`e`.`date` AS `date`,`e`.`hour` AS `hour`,`e`.`sex` AS `sex`,`e`.`agemin` AS `agemin`,`e`.`agemax` AS `agemax`,`e`.`sittotal` AS `sittotal`,`e`.`sitcurrent` AS `sitcurrent`,`e`.`latitude` AS `latitude`,`e`.`longitude` AS `longitude`,`e`.`scope` AS `scope`,`e`.`creationdate` AS `creationdate`,`e`.`updatetime` AS `updatetime`,`e`.`activated` AS `activated`,`u`.`name` AS `name`,`u`.`surname` AS `surname`,if((`c1`.`idsupercategory` is not null),concat(`c2`.`name`,' > ',`c1`.`name`),`c1`.`name`) AS `c_name` from (((`events` `e` join `users` `u` on((`e`.`iduser` = `u`.`iduser`))) join `categories` `c1` on((`e`.`idcategory` = `c1`.`id`))) left join `categories` `c2` on((`c1`.`idsupercategory` = `c2`.`id`))) order by `e`.`idevent` desc;

-- --------------------------------------------------------

--
-- Struttura per la vista `cp_partecipants_list`
--
DROP TABLE IF EXISTS `cp_partecipants_list`;

CREATE VIEW `cp_partecipants_list` AS select `e`.`id` AS `id`,`e`.`iduser` AS `iduser`,`e`.`idevent` AS `idevent`,`e`.`confirmed` AS `confirmed`,`u`.`email` AS `email`,`u`.`name` AS `name`,`u`.`surname` AS `surname` from (`partecipates` `e` join `users` `u` on((`e`.`iduser` = `u`.`iduser`)));

-- --------------------------------------------------------

--
-- Struttura per la vista `cp_users_lists`
--
DROP TABLE IF EXISTS `cp_users_lists`;

CREATE VIEW `cp_users_lists` AS select `u`.`iduser` AS `iduser`,`u`.`email` AS `email`,`u`.`idgoogle` AS `idgoogle`,`u`.`idfacebook` AS `idfacebook`,`u`.`name` AS `name`,`u`.`surname` AS `surname`,`u`.`img` AS `img`,`u`.`birthdate` AS `birthdate`,`u`.`sex` AS `sex`,`u`.`job` AS `job`,`u`.`city` AS `city`,`u`.`interests` AS `interests`,`u`.`creationdate` AS `creationdate`,`u`.`updatetime` AS `updatetime`,`u`.`password` AS `password`,`u`.`activated` AS `activated`,if((`c`.`created_events` is not null),`c`.`created_events`,0) AS `created_events`,if((`p`.`partecipates_events` is not null),`p`.`partecipates_events`,0) AS `partecipates_events` from ((`users` `u` left join `cp_count_created_events` `c` on((`u`.`iduser` = `c`.`iduser`))) left join `cp_count_booked_events` `p` on((`u`.`iduser` = `p`.`iduser`)));

-- --------------------------------------------------------

--
-- Struttura per la vista `date_listed_created_events`
--
DROP TABLE IF EXISTS `date_listed_created_events`;

CREATE VIEW `date_listed_created_events` AS select `e`.`date` AS `date`,`e`.`iduser` AS `iduser`,count(`e`.`idevent`) AS `occurance` from `user_created_events` `e` group by `e`.`iduser`,`e`.`date`;

-- --------------------------------------------------------

--
-- Struttura per la vista `date_listed_events`
--
DROP TABLE IF EXISTS `date_listed_events`;

CREATE VIEW `date_listed_events` AS select `e`.`date` AS `date`,count(`e`.`idevent`) AS `occurance` from `public_events` `e` group by `e`.`date`;

-- --------------------------------------------------------

--
-- Struttura per la vista `date_listed_friends_created_events`
--
DROP TABLE IF EXISTS `date_listed_friends_created_events`;

CREATE VIEW `date_listed_friends_created_events` AS select `e`.`date` AS `date`,`e`.`idselecteduser` AS `idselecteduser`,count(`e`.`idevent`) AS `occurance` from `friend_created_events` `e` group by `e`.`idselecteduser`,`e`.`date`;

-- --------------------------------------------------------

--
-- Struttura per la vista `date_listed_partecipated_events`
--
DROP TABLE IF EXISTS `date_listed_partecipated_events`;

CREATE VIEW `date_listed_partecipated_events` AS select `e`.`date` AS `date`,`e`.`iduserpartecipates` AS `iduserpartecipates`,count(`e`.`idevent`) AS `occurance` from `user_partecipate_events` `e` group by `e`.`iduserpartecipates`,`e`.`date`;

-- --------------------------------------------------------

--
-- Struttura per la vista `follow_created_events`
--
DROP TABLE IF EXISTS `follow_created_events`;

CREATE VIEW `follow_created_events` AS select `e`.`img` AS `img`,`e`.`idevent` AS `idevent`,`e`.`sex` AS `sex`,`e`.`sittotal` AS `sittotal`,`e`.`sitcurrent` AS `sitcurrent`,`e`.`idcategory` AS `idcategory`,`e`.`title` AS `title`,`e`.`description` AS `description`,`e`.`city` AS `city`,`e`.`address` AS `address`,`e`.`agemin` AS `agemin`,`e`.`agemax` AS `agemax`,`e`.`date` AS `date`,`e`.`hour` AS `hour`,`e`.`scope` AS `scope`,`e`.`latitude` AS `latitude`,`e`.`longitude` AS `longitude`,`e`.`userimg` AS `userimg`,`e`.`job` AS `job`,`f`.`idfollow` AS `iduser`,`f`.`iduser` AS `idselecteduser` from (`user_follows` `f` join `public_events` `e` on((`f`.`idfollow` = `e`.`iduser`)));

-- --------------------------------------------------------

--
-- Struttura per la vista `friend_created_events`
--
DROP TABLE IF EXISTS `friend_created_events`;

CREATE VIEW `friend_created_events` AS select `e`.`idevent` AS `idevent`,`e`.`sex` AS `sex`,`e`.`sittotal` AS `sittotal`,`e`.`sitcurrent` AS `sitcurrent`,`e`.`idcategory` AS `idcategory`,`e`.`title` AS `title`,`e`.`description` AS `description`,`e`.`city` AS `city`,`e`.`address` AS `address`,`e`.`agemin` AS `agemin`,`e`.`agemax` AS `agemax`,`e`.`date` AS `date`,`e`.`hour` AS `hour`,`e`.`scope` AS `scope`,`e`.`latitude` AS `latitude`,`e`.`longitude` AS `longitude`,`e`.`img` AS `img`,`e`.`userimg` AS `userimg`,`e`.`job` AS `job`,`f`.`idfriend` AS `iduser`,`f`.`iduser` AS `idselecteduser` from (`user_accepted_friends` `f` join `activated_events` `e` on((`f`.`idfriend` = `e`.`iduser`))) where ((`e`.`scope` = 0) or (`e`.`scope` = 2));

-- --------------------------------------------------------

--
-- Struttura per la vista `public_events`
--
DROP TABLE IF EXISTS `public_events`;

CREATE VIEW `public_events` AS select `e`.`idevent` AS `idevent`,`e`.`iduser` AS `iduser`,`e`.`idcategory` AS `idcategory`,`e`.`title` AS `title`,`e`.`description` AS `description`,`e`.`img` AS `img`,`e`.`city` AS `city`,`e`.`address` AS `address`,`e`.`date` AS `date`,`e`.`hour` AS `hour`,`e`.`sex` AS `sex`,`e`.`agemin` AS `agemin`,`e`.`agemax` AS `agemax`,`e`.`sittotal` AS `sittotal`,`e`.`sitcurrent` AS `sitcurrent`,`e`.`latitude` AS `latitude`,`e`.`longitude` AS `longitude`,`e`.`scope` AS `scope`,`e`.`creationdate` AS `creationdate`,`e`.`updatetime` AS `updatetime`,`e`.`activated` AS `activated`,`e`.`userimg` AS `userimg`,`e`.`job` AS `job` from `activated_events` `e` where (`e`.`scope` = 0);

-- --------------------------------------------------------

--
-- Struttura per la vista `user_accepted_follows`
--
DROP TABLE IF EXISTS `user_accepted_follows`;

CREATE VIEW `user_accepted_follows` AS select `f`.`iduser` AS `iduser`,`f`.`idfollow` AS `idfollow`,`u1`.`name` AS `name`,`u1`.`surname` AS `surname`,`u1`.`img` AS `img` from ((`activated_users` `u1` join `user_follows` `f` on((`u1`.`iduser` = `f`.`iduser`))) join `activated_users` `u2` on((`f`.`idfollow` = `u2`.`iduser`)));

-- --------------------------------------------------------

--
-- Struttura per la vista `user_accepted_friends`
--
DROP TABLE IF EXISTS `user_accepted_friends`;

CREATE VIEW `user_accepted_friends` AS select `f`.`iduser` AS `iduser`,`f`.`idfriend` AS `idfriend`,`f`.`accepted` AS `accepted`,`u2`.`name` AS `name`,`u2`.`surname` AS `surname`,`u2`.`img` AS `img` from ((`activated_users` `u1` join `user_friends` `f` on((`u1`.`iduser` = `f`.`iduser`))) join `activated_users` `u2` on((`f`.`idfriend` = `u2`.`iduser`))) where (`f`.`accepted` = 1);

-- --------------------------------------------------------

--
-- Struttura per la vista `user_created_events`
--
DROP TABLE IF EXISTS `user_created_events`;

CREATE VIEW `user_created_events` AS select `e`.`idevent` AS `idevent`,`e`.`iduser` AS `iduser`,`e`.`sex` AS `sex`,`e`.`sittotal` AS `sittotal`,`e`.`sitcurrent` AS `sitcurrent`,`e`.`idcategory` AS `idcategory`,`e`.`title` AS `title`,`e`.`description` AS `description`,`e`.`city` AS `city`,`e`.`address` AS `address`,`e`.`agemin` AS `agemin`,`e`.`agemax` AS `agemax`,`e`.`date` AS `date`,`e`.`hour` AS `hour`,`e`.`scope` AS `scope`,`e`.`latitude` AS `latitude`,`e`.`longitude` AS `longitude`,`e`.`img` AS `img`,`u`.`img` AS `userimg`,`u`.`job` AS `job` from (`events` `e` join `activated_users` `u` on((`e`.`iduser` = `u`.`iduser`))) order by `e`.`date`,`e`.`hour`;

-- --------------------------------------------------------

--
-- Struttura per la vista `user_event_notifications`
--
DROP TABLE IF EXISTS `user_event_notifications`;

CREATE VIEW `user_event_notifications` AS select `n`.`id` AS `id`,`n`.`date` AS `date`,`n`.`type` AS `type`,`n`.`idsender` AS `idsender`,`n`.`idreceiver` AS `idreceiver`,`n`.`idevent` AS `idevent`,`u`.`name` AS `name`,`u`.`surname` AS `surname`,`u`.`img` AS `img`,`e`.`title` AS `title` from ((`notifications` `n` join `users` `u` on((`n`.`idsender` = `u`.`iduser`))) left join `events` `e` on((`n`.`idevent` = `e`.`idevent`)));

-- --------------------------------------------------------

--
-- Struttura per la vista `user_partecipate_events`
--
DROP TABLE IF EXISTS `user_partecipate_events`;

CREATE VIEW `user_partecipate_events` AS select `e`.`idevent` AS `idevent`,`e`.`iduser` AS `iduser`,`e`.`sex` AS `sex`,`e`.`sittotal` AS `sittotal`,`e`.`sitcurrent` AS `sitcurrent`,`e`.`idcategory` AS `idcategory`,`e`.`title` AS `title`,`e`.`description` AS `description`,`e`.`city` AS `city`,`e`.`address` AS `address`,`e`.`agemin` AS `agemin`,`e`.`agemax` AS `agemax`,`e`.`date` AS `date`,`e`.`hour` AS `hour`,`e`.`scope` AS `scope`,`e`.`latitude` AS `latitude`,`e`.`longitude` AS `longitude`,`e`.`userimg` AS `userimg`,`e`.`job` AS `job`,`p`.`iduser` AS `iduserpartecipates` from (`activated_events` `e` join `confirmed_partecipates` `p` on((`e`.`idevent` = `p`.`idevent`)));

--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella `chats`
--
ALTER TABLE `chats`
  ADD CONSTRAINT `chats_ibfk_2` FOREIGN KEY (`idsender`) REFERENCES `users` (`iduser`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `chats_ibfk_3` FOREIGN KEY (`idreceiver`) REFERENCES `users` (`iduser`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limiti per la tabella `events`
--
ALTER TABLE `events`
  ADD CONSTRAINT `idUser` FOREIGN KEY (`iduser`) REFERENCES `users` (`iduser`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limiti per la tabella `event_invitations`
--
ALTER TABLE `event_invitations`
  ADD CONSTRAINT `event_invitations_ibfk_1` FOREIGN KEY (`idreciver`) REFERENCES `users` (`iduser`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `event_invitations_ibfk_2` FOREIGN KEY (`idevent`) REFERENCES `events` (`idevent`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pk_sender_user` FOREIGN KEY (`idsender`) REFERENCES `users` (`iduser`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limiti per la tabella `pushcodes`
--
ALTER TABLE `pushcodes`
  ADD CONSTRAINT `pushcodes_ibfk_1` FOREIGN KEY (`iduser`) REFERENCES `users` (`iduser`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limiti per la tabella `user_friends`
--
ALTER TABLE `user_friends`
  ADD CONSTRAINT `fk_users_has_users_users3` FOREIGN KEY (`iduser`) REFERENCES `users` (`iduser`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_has_users_users4` FOREIGN KEY (`idfriend`) REFERENCES `users` (`iduser`) ON DELETE NO ACTION ON UPDATE NO ACTION;
  
  
-- FUNZIONI E PROCEDURE

DELIMITER $$

CREATE FUNCTION `distanza`(store_lat decimal(23,20), store_lon decimal(23,20), user_lat decimal(23,20), user_lon decimal(23,20)) RETURNS decimal(10,0)
    DETERMINISTIC
begin

  declare dist DECIMAL(10);
  
  select (( 1000 * 6371 * acos( cos( radians(user_lat) ) * cos( radians( store_lat ) ) * cos( radians( store_lon ) - radians(user_lon) ) + sin( radians(user_lat) ) * sin( radians( store_lat ) ) ) ) ) into dist;

  return dist;

end $$

DELIMITER ;

DELIMITER $$

CREATE PROCEDURE `confirm_bookings`(id_event_ INT, booking_list_ TEXT)
begin
declare com_list TEXT;  -- lista delle prenotazioni da confermare
declare loc INT; -- posizione del delimitatore nella lista
declare id_booking_ INT; -- pk prenotazione
declare confirmed_ INT(1); -- variabile di lettura di confirmed
declare sit_to_block_ INT; -- numero di posti che vengono bloccati
	
	SET com_list = booking_list_;
	SET sit_to_block_ = 0;
	
	WHILE LENGTH(com_list) > 0 DO  -- c'è almeno una occorrenza di prenotazione da elaborare
            
		SET loc = LOCATE(',', com_list);
		
		IF  loc > 0 THEN -- ci sono almeno due occorrenze
		
			SET id_booking_ = CAST(LEFT(com_list, loc - 1) AS SIGNED);
			SET com_list = SUBSTRING(com_list, loc + 1);
		
		ELSE  -- c'è una sola occorrenza
		
			SET id_booking_ = CAST(com_list AS SIGNED);
			SET com_list = '';
			
		END IF;

		SELECT `confirmed` INTO confirmed_ FROM `partecipates` WHERE `id` = id_booking_;
		
		-- setta la conferma solo alle prenotazioni non ancora confermate
		IF confirmed_ = 0 THEN
		
			UPDATE `partecipates` SET `confirmed` = 1 WHERE `id` = id_booking_;
			SET sit_to_block_ = sit_to_block_ + 1;
		
	    END IF;
		
	END WHILE;
	
	-- aggiornamento dei posti disponibili all'evento
	UPDATE `events` SET `sitcurrent` = `sitcurrent` + sit_to_block_ WHERE `idevent` = id_event_;
	
end $$

DELIMITER ;


DELIMITER $$

CREATE PROCEDURE `delete_old_notification`(idreceiver_ int(11), max_notification_ int(3))
begin
     
	DECLARE done INT DEFAULT FALSE;
	DECLARE a, b INT;
	DECLARE cur1 CURSOR FOR SELECT id FROM notifications where idreceiver = idreceiver_ order by id desc;
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
	
	OPEN cur1;
	SET b = 0;
	
	read_loop: LOOP
	
		SET b = b + 1;
		FETCH cur1 INTO a;
		
		IF done THEN
			LEAVE read_loop;
		END IF;
		
		if (b > max_notification_) then
			delete from notifications where id = a;
		end if;
	end LOOP;
	
	close cur1;
	
end $$

DELIMITER ;

DELIMITER $$

CREATE `remove_booking_confirmation`(id_event_ INT, unbooking_list_ TEXT)
begin
declare com_list TEXT;  -- lista delle prenotazioni da confermare
declare loc INT; -- posizione del delimitatore nella lista
declare id_booking_ INT; -- pk prenotazione
declare confirmed_ INT(1); -- variabile di lettura di confirmed
declare sit_to_free_ INT; -- numero di posti che vengono liberate
	
	SET com_list = unbooking_list_;
	SET sit_to_free_ = 0;
	
	WHILE LENGTH(com_list) > 0 DO  -- c'è almeno una occorrenza di prenotazione da elaborare
            
		SET loc = LOCATE(',', com_list);
		
		IF  loc > 0 THEN -- ci sono almeno due occorrenze
		
			SET id_booking_ = CAST(LEFT(com_list, loc - 1) AS SIGNED);
			SET com_list = SUBSTRING(com_list, loc + 1);
		
		ELSE  -- c'è una sola occorrenza
		
			SET id_booking_ = CAST(com_list AS SIGNED);
			SET com_list = '';
			
		END IF;

		SELECT `confirmed` INTO confirmed_ FROM `partecipates` WHERE `id` = id_booking_;
		
		-- setta la conferma solo alle prenotazioni non ancora confermate
		IF confirmed_ = 1 THEN
		
			UPDATE `partecipates` SET `confirmed` = 0 WHERE `id` = id_booking_;
			SET sit_to_free_ = sit_to_free_ + 1;
		
	    END IF;
		
	END WHILE;
	
	-- aggiornamento dei posti disponibili all'evento
	UPDATE `events` SET `sitcurrent` = `sitcurrent` - sit_to_free_ WHERE `idevent` = id_event_;
	
end $$

DELIMITER;

DELIMITER $$

CREATE PROCEDURE `save_chat_message`(idreceiver_ int(11), idsender_ int(11), message_ TEXT, creation_time_ varchar(45), max_notification_ int(3))
begin
     
	DECLARE done INT DEFAULT FALSE;
	DECLARE a varchar(64);
	DECLARE username_ varchar(100);
	
	DECLARE cur1 CURSOR FOR SELECT hwid FROM pushcodes where iduser = idreceiver_ and os = 0;
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

	SELECT CONCAT(`name`, ' ', `surname`) into username_ from users where iduser = idsender_;
	
	-- inserimento delle notifiche push che potrebbero andare perdute su ios
	
	OPEN cur1;
	
	read_loop: LOOP
	
		FETCH cur1 INTO a;
		
		IF done THEN
			LEAVE read_loop;
		END IF;
		
		INSERT INTO `chats`(`hwid`, `idsender`, `sendername`,`idreceiver`, `message`, `creation_time`) VALUES (a, idsender_, username_, idreceiver_, message_, creation_time_);
	
	end LOOP;
	
	close cur1;
	
    -- inserimento delle notifiche locali
	
	INSERT INTO `notifications`(`type`, `idsender`, `idreceiver`) VALUES (7, idsender_, idreceiver_);
	
	-- eliminazione delle vecchie notifiche locali
	
	call delete_old_notification(idreceiver_, max_notification_);
	
end $$

DELIMITER ;