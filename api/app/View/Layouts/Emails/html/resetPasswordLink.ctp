<?php
/**
 *
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts.Email.html
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">
<html>
<head>
	<title>Wherabout Email</title>
</head>
<body>
    <div style="width: 100%;">
        <div style="width: 70%;margin:auto;text-align: justify;">
            <div>
                <h1>Wherabout</h1>
                <h3>Hi, <?php echo strtoupper($username)?>!!!</h3>
                <p>
                    We just receveid a password reset request.
                    Now you have 15 minutes left to click on following link, so you'll receive another email containig the new auto-generated password.<br> 
                </p>
                <p>
                    <strong>Follow this link to reset password: </strong><a href=<?php echo '"'.EmailManager::EMAIL_SERVICE_URL.$iduser.'-'.$limit.'"';?> target="_blank"><?php echo '"'.EmailManager::EMAIL_SERVICE_URL.$iduser.'-'.$limit.'"'; ?></a><br>
                </p>
                <p>
                    Thanks to use Wherabout
                </p>
            <div>
        </div>
    </div>
</body>
</html>