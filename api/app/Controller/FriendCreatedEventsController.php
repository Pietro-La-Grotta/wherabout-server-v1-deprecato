<?php

/**
 * 
 *
 * @author Christian Ruggiero
 * @creation 17/11/2014
 * @status in develop
 */
App::import('Vendor', 'Events/IEventListController');

class FriendCreatedEventsController extends AppController implements IEventListController{
    private $LIST_NAME = 'FriendCreatedEvent';
    
    public function getListByMonth( $year, $month, $iduser ){
        return $this->FriendCreatedEvent->getListByMonth( $year, $month, $iduser );
    }
    
    public function getListPaged( $lastdate, $limitdate, $iduser ) {
        return $this->FriendCreatedEvent->getListPaged( $lastdate, $limitdate, $iduser );
    }
    
    public function getListName(  ){
        return $this->LIST_NAME;
    }
     
    public function getDistanceList( $iduser, $latitude, $longitude, $maxdist ) {
        $eventMappedList = array();
        $eventList = $this->FriendCreatedEvent->getDistanceList( $iduser );
        foreach ($eventList as $singleEvent) {
            if( Utils::getDistanceFromCoords($latitude, $longitude, $singleEvent[$this->LIST_NAME]["latitude"], $singleEvent[$this->LIST_NAME]["longitude"]) <= $maxdist) {
                    $eventMappedList[] = $singleEvent;
            }
        }
        
        return   $eventMappedList;
    }
    
    //filtered
    public function getFilteredListPaged( $iduser, $lastdate, $searchLimit, $filters ) {
        return  $this->FriendCreatedEvent->getFilteredListPaged( $iduser, $lastdate, $searchLimit, $filters );
    }
    
    public function getFilteredListByMonth( $iduser, $year, $month, $filters ) {
        return  $this->FriendCreatedEvent->getFilteredListByMonth( $iduser, $year, $month, $filters );
    }
 
    public function getFilteredDistanceList( $iduser, $latitude, $longitude, $maxdist, $filters ) {
        $eventMappedList = array();
        $eventList = $this->FriendCreatedEvent->getFilteredDistanceList($iduser, $filters );
        foreach ($eventList as $singleEvent) {
            if( Utils::getDistanceFromCoords($latitude, $longitude, $singleEvent[$this->LIST_NAME]["latitude"], $singleEvent[$this->LIST_NAME]["longitude"]) <= $maxdist) {
                    $eventMappedList[] = $singleEvent;
            }
        }
        return $eventMappedList;
    }
    
}