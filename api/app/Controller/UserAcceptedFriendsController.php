<?php


/**
 * PushController is the main controller for push based request and it's used by :
 *  - UserController
 *
 * @author Christian Ruggiero
 *@creation 4/11/2014
 * @status in develop
 */
class UserAcceptedFriendsController extends AppController{
    public function getFriendList( $iduser ) {
        return $this->UserAcceptedFriend->getFriendList( $iduser );
    }
    
    public function getFriendsCount( $iduser ) {
        return $this->UserAcceptedFriend->getFriendsCount( $iduser );
    }
}
