<?php

/**
 * 
 *
 * @author Christian Ruggiero
 * @creation 21/11/2014
 * @status in develop
 */
App::import('Vendor', 'Events/IEventListController');
class FollowCreatedEventsController extends AppController implements IEventListController {
    private $LIST_NAME = 'FollowCreatedEvent';
    
    public function getListByMonth( $year, $month, $iduser ){
        return $this->FollowCreatedEvent->getListByMonth(  $year, $month, $iduser );
    }
    
    public function getListPaged( $lastdate, $limitdate, $iduser ) {
        return $this->FollowCreatedEvent->getListPaged( $lastdate, $limitdate, $iduser );
    }
    
    public function getListName(  ){
        return $this->LIST_NAME;
    }
    
    //filtered
    public function getFilteredListPaged( $iduser, $lastdate, $searchLimit, $filters ) {
        return  $this->FollowCreatedEvent->getFilteredListPaged( $iduser, $lastdate, $searchLimit, $filters );
    }
    
    public function getFilteredListByMonth( $iduser, $year, $month, $filters ) {
        return  $this->FollowCreatedEvent->getFilteredListByMonth( $iduser, $year, $month, $filters );
    }
    
    public function getFilteredDistanceList( $iduser, $latitude, $longitude, $maxdist, $filters ) {

        $eventMappedList = array();
        $eventList = $this->FollowCreatedEvent->getFilteredDistanceList($iduser, $filters );
        
        foreach ($eventList as $singleEvent) {
            if( Utils::getDistanceFromCoords($latitude, $longitude, $singleEvent["FollowCreatedEvent"]["latitude"], $singleEvent["FollowCreatedEvent"]["longitude"]) <= $maxdist) {
                    $eventMappedList[] = $singleEvent;
            }
        }
        return $eventMappedList;
    }
}