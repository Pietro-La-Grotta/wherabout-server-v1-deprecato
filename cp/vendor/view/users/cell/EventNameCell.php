<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of EventNameCell
 * Titolo dell'evento con collegamnento al dettaglio
 * @author pietro@studioleaves.com
 */
class EventNameCell implements ICell{
     
    private static $menu = '?show=edit&item=';
    private static $title = 'Vai al dettaglio dell\'evento';

    private $tuple;
    private $id_label_field;
    private $title_label_field;
   
    /**
     * Inizializza le variabili di istanza
     * @param string $id_label_field - etichetta dell'attributo della tupla contenente la pk dell'evento
     * @param string $title_label_field - etichetta dell'attributo della tupla contenente il titolo dell'evento
     */
    function __construct($id_label_field, $title_label_field) {
        $this->id_label_field = $id_label_field;
        $this->title_label_field = $title_label_field;
    }

    /**
     * Stampa il contenuto html
     * @return string
     */
    public function html() {
        $html = "";
            
            $title = $this->tuple[$this->title_label_field];
            
            $html .= '<td>';
                $html .= '<a href="'.Juri::base().self::$menu.$this->tuple[$this->id_label_field].'" title="'.self::$title.'" target="_blank">';
                $html .= $title;
                $html .= '</a>';
            $html .= '</td>';
            
        return $html;
    }

    /**
     * Setter per la tupla
     * @param array $tuple
     */
    public function data(&$tuple) {
        $this->tuple = $tuple;
    }    
}

?>
