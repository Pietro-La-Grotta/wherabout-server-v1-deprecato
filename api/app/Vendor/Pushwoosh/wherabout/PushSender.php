<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PushSender
 * 
 * @author pietro@studioleaves.com
 */
class PushSender {
    
    /**
     * Parametri di configurazione delle notifiche
     * @var CreateParam 
     */
    protected $param;
    
    /**
     * IUstanza del factory per l'invio delle ntifiche
     * @var Pushwoosh
     */
    private $pushwoosh;
    
    /**
     * Caricamento delle risorse necessarie per inviare una richiesa dio notifica push
     */
    protected function __construct() {
        App::import('Vendor', 'Pushwoosh/Pushwoosh');
            
        $this->pushwoosh = Pushwoosh::create(function(){
            App::import('Vendor', 'Pushwoosh/IPushParameter');
            App::import('Vendor', 'Pushwoosh/CreateParam');
            App::import('Vendor', 'Pushwoosh/CustomData');
            App::import('Vendor', 'Pushwoosh/LanguageUpdateParam');
        });
        
        $this->param = new CreateParam();
    }
    
    /**
     * Comando di invio istantaneo delle notifiche push
     */
    protected function sendNow(){
        $this->param->timeToSend = "now";
        $this->pushwoosh->setIosBGAvailable();
        
        $this->pushwoosh->createMessage($this->param);
    }
    
    /**
     * Imposta la lista dei destinatari
     * @param array of string $receivers lista dei pushcodes dei destinatari
     */
    public function setReceivers(&$receivers){
        $this->param->receivers = $receivers;
    }
}

?>
