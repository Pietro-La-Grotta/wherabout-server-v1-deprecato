<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
App::import('Vendor', 'Events/IEventListController');
class UserPartecipateEventsController extends AppController implements IEventListController{
    private $LIST_NAME = 'UserPartecipateEvent';
    public function getListPaged( $lastdate, $limitdate, $iduser ) {
        return $this->UserPartecipateEvent->getListPaged( $lastdate, $limitdate, $iduser );
    }
    
    public function getListByMonth( $year, $month, $iduser ) {
        return  $this->UserPartecipateEvent->getListByMonth( $year, $month, $iduser );
    }
    
    public function getListName(  ){
        return $this->LIST_NAME;
    }
    
     /**
     * Seleziona le pk dei partecipanti ad un evento
     * @param int $idevent pk evento
     * @param [boolean $noPlanner] default false : non escludere l'organizzatore dalla lista, true: escludere l'organizzatore dalla vista  
     * @return list of array
     */
    public function getPartecipants($idevent, $noPlanner = false) {
        return $this->UserPartecipateEvent->getPartecipants($idevent, $noPlanner);
    }
}