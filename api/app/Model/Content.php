<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Content
 * Accede ai dati della tabella contents
 * @author pietro@studioleaves.com
 */
class Content extends AppModel{
    
    /**
     * Seleziona il testo di termini e condizioni
     * @return testo html da mostrare nella pagina termini e condizioni dell'app
     */
    public function getTermsConditions(){
        $raw = $this->find('first', array('conditions' => array('code' => 'TERMS_CONDITIONS')));
        return HTMLDecoder::decode($raw["Content"]['content']);
    }
    
    /**
     * Seleziona il testo di termini e condizioni
     * @return testo html da mostrare nella info privacy dell'app
     */
    public function getInfoPrivacy(){
        $raw = $this->find('first', array('conditions' => array('code' => 'INFO_PRIVACY')));
        return HTMLDecoder::decode($raw["Content"]['content']);
    }
}

?>
