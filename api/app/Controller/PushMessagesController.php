<?php


/**
 * PushController is the main controller for push based request and it's used by :
 *  - UserController
 *
 * @author Christian Ruggiero
 *@creation 4/11/2014
 * @status in develop
 */
class PushMessagesController extends AppController{
    
    public function getValue($key){
        $value = $this->PushMessage->find( 'first', array( 'conditions' => array('key'=> $key) ) ) ;
        if($value == null || empty( $value )) {
            $value = $key;
        }
        return $value["PushMessage"]["text"];
    }

}
