<?php

App::uses('DispatcherFilter', 'Routing');

/**
 * Controlla se la richiesta è stata effettuata tramite mobile
 *
 * @package Cake.Routing.Filter
 */
class AuthRequest extends DispatcherFilter {

/**
 * Default priority for all methods in this filter
 * This filter should run before the request gets parsed by router
 *
 * @var integer
 */
	public $priority = 1;

/**
 * Controlla se la richiesta è stata effettuata tramite mobile
 *
 * @param CakeEvent $event containing the request and response object
 * @return CakeResponse if the client is requesting a recognized asset, null otherwise
 */
	public function beforeDispatch(CakeEvent $event) {
            
            /**
                 key = primi 12 caratteri della stringa generata da md5(idUser + password)
             */
            
            App::import('Config', 'Authentication');
            
            $request = new CakeRequest();
            $input = json_decode($request->data['toServer']);
            
            $error = json_last_error();
            if ($error != JSON_ERROR_NONE) throw new JSONException($error);

            $clientAuthKey = $input->authKey;
            $idUser = $input->idUser;
            
            if (substr(md5($idUser.Authentication::$pws), 0, 12) != $clientAuthKey) throw new UnauthorizedException();
        }
}
