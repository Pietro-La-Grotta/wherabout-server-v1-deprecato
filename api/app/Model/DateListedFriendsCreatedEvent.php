<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Event
 *
 * @author Christian
 */
App::uses('AppModel', 'Model');
class DateListedFriendsCreatedEvent extends AppModel{
    public function getList( $basedate, $iduser ) {
       return $this->find( 'all', array( 'conditions' => array( 'date >' => $basedate, 'idselecteduser' => $iduser), 'order' => array('date ASC') ) );
    }
}
