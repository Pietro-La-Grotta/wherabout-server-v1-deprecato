<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Chat
 * Accede ad i dati coinvolti nello scambio di messaggi tramite chat 
 * @author pietro@studioleaves.com
 */
class Chat extends AppModel{
    
    /**
     * Esegue la procedura per la memorizzazione delle notifiche locali e per il salvataggio dei messaggi in chat che potrebbero andare persi sui dispositivi ios 
     */
    public function sendmessage(){
        
        $this->query("CALL save_chat_message(".HTMLDecoder::encode(self::$inputData->idreceiver).", 
                                             ".HTMLDecoder::encode(self::$inputData->iduser).",
                                             '".HTMLDecoder::encode(self::$inputData->message)."',
                                             '".HTMLDecoder::encode(self::$inputData->creationdate)."',
                                             ".NotificationsController::NOTIFICATION_MAX_NUMBER.")");
    }
    
    /**
     * Seleziona i messaggi che sono stati persi da parte di un dispositivo ios e li cancella 
     */
    public function getLostMessages(){
        
        // condizioni per la selezione dei messaggi
        $conditions = array('idsender' => HTMLDecoder::encode(self::$inputData->idsender),
                            'idreceiver' => HTMLDecoder::encode(self::$inputData->iduser),
                            'hwid' => self::$inputData->hwid,
                            'creation_time > ' => HTMLDecoder::encode(self::$inputData->datefrom));
//                            'creation_time < ' => HTMLDecoder::encode(self::$inputData->dateto));
//                            'creation_time BETWEEN ? AND ?' => array(intval(HTMLDecoder::encode(self::$inputData->datefrom)) + 1,
//                                                                     intval(HTMLDecoder::encode(self::$inputData->dateto)) - 1)); // estremi non inclusi nell'intervallo
        
        // selezione
        $messages = $this->find('all' , array('conditions' => $conditions, 'order' => array('creation_time asc'), 'fields' => array('idsender', 'sendername', 'creation_time', 'message')));
        
        // object filling
        $response = array();
        
        foreach ($messages as &$m){
            $response[] = array('type' => 7,
                                'idsender' => $m['Chat']['idsender'],
                                'username' => $m['Chat']['sendername'],
                                'creationdate' => $m['Chat']['creation_time'],
                                'message' => HTMLDecoder::decode($m['Chat']['message']));
        }
        
        // condizione per la cancellazione
        $conditions = array('idsender' => HTMLDecoder::encode(self::$inputData->idsender),
                            'idreceiver' => HTMLDecoder::encode(self::$inputData->iduser),
                            'hwid' => self::$inputData->hwid);
                            //'creation_time <=' => HTMLDecoder::encode(self::$inputData->dateto)
        
        // cancellazione
       $this->deleteAll($conditions);
        
        // :-) 
        return $response;
    }
}

?>
