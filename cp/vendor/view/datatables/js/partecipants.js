/* 
 * Script usati per la lista dei partecipanti ad un evento
 */

/**
 * Inizializzazione della tabella per la lista delle prenotazioni ad un evento
 */
function init_data_table(){
    jQuery(document).ready(function() {
        
       var table = jQuery('#studio_leaves').DataTable({
           
            "lengthMenu": [[100, 200, 400, -1], [100, 200, 400, "All"]],
            "aaSorting": [[1,'asc']],
            "paging":   true,
            "aoColumnDefs": [{ 'bSortable': false, 
                               'aTargets': [0, 3]
                           }]
        });
       
        table.column(0).visible(false);
        
         // selezione multipla delle righe
       jQuery('#studio_leaves tbody').on( 'click', 'tr', function (){
           jQuery(this).toggleClass('selected');
       });
       
       // Conferma delle prenotazioni
       
       jQuery('#confirm_button').click( function () {
            var x = table.rows('.selected').data().length;
            
            if (x < 1){
                alert('E\' necessario selezionare le prenotazioni da confermare');
                return;
            }
            
            if (confirm('Si è certi di volere confermare le prenotazioni selezionate?')){
                jQuery('#books_to_confirm').val(getSelectedList());
                document.confirm_booking_form.submit();  
            }
        });
        
        // Rimozione delle conferme
       
       jQuery('#remove_button').click( function () {
            var x = table.rows('.selected').data().length;
            
            if (x < 1){
                alert('E\' necessario selezionare le conferme da rimuovere');
                return;
            }
            
            if (confirm('Si è certi di voler rimuovere le conferme selezionate?')){
                jQuery('#confirmation_to_remove').val(getSelectedList());
                document.remove_confirm_post.submit();  
            }
        });
       
    });
}

init_data_table();

var comodo;

/**
 * Genera la lista degli id delle righe selezionate
 * @returns {@exp;JSON@call;stringify} lista in formato json trasformata in stringa
 */
function getSelectedList(){
    comodo = [];
    jQuery('.selected').each(function (){
        comodo[comodo.length] = jQuery(this).attr("id");
    });
    return JSON.stringify(comodo);
}