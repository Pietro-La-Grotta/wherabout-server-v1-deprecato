<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Event
 *
 * @author Christian
 */
App::uses('AppModel', 'Model');
class DateListedPartecipatedEvent extends AppModel{
    public function getList( $basedate, $iduser ) {
       return $this->find( 'all', array( 'conditions' => array( 'date >' => $basedate, 'iduserpartecipates' => $iduser), 'order' => array('date ASC') ) );
    }
}
