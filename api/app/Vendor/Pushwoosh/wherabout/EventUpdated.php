<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
App::import('Vendor', 'Pushwoosh/wherabout/PushSender');

/**
 * Description of EventCreated
 * Invio delle notifiche push nl caso di creazione evento 
 * @author pietro@studioleaves.com
 */
class EventUpdated extends PushSender{
    
    private static $shortMessage = "An event you joined has been updated";
    private static $messageType = 1;
    
    /**
     * Inizializzazione variabili di istanza per le notifiche di creazione evento
     */
    public function __construct() {
        parent::__construct();
        
        $this->param->shortContent = self::$shortMessage;
        $this->param->customData->type = self::$messageType;
    }
    
    /**
     * Invia la richiesta di spedizione delle notifiche
     */
    public function send(){
        $this->sendNow();
    }
}

?>
