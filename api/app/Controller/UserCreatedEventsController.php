<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
App::import('Vendor', 'Events/IEventListController');
class UserCreatedEventsController extends AppController implements IEventListController{
    private $LIST_NAME = 'UserCreatedEvent';
    public function getListPaged( $lastdate, $limitdate, $iduser ) {
        return $this->UserCreatedEvent->getListPaged( $lastdate, $limitdate, $iduser );
    }
    
    public function getListByMonth( $year, $month, $iduser ) {
        return  $this->UserCreatedEvent->getListByMonth( $year, $month, $iduser );
    }
    
    public function getListName(  ){
        return $this->LIST_NAME;
    }
    
    public function getSingleEventInfo( $idevent ) {
        return $this->UserCreatedEvent->getSingleEventInfo($idevent);
    }
}