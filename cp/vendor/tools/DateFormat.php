<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DateFormat
 * Gestione delle stringhe contenenti delle date
 * @author pietro@studioleaves.com
 */
class DateFormat {
    
    private $datetime;
    
    /**
     * Inizializza l'oggetto con la data estratta dal db
     * @param string $sqlDateTime es: aaaa-mm-gg hh:mm:ss
     */
    public function DateFormat($sqlDateTime){
        $this->datetime = $sqlDateTime;
    }
    
    /**
     * Seleziona la stringa contenente la data formattata
     * @param string $lang {it | en}
     */
    public function getWallDate($lang = 'it'){
        return ($lang == "it")
             ? substr($this->datetime, 8, 2)." ".substr($this->italianMonth(), 0, 3).", ".substr($this->datetime, 11, 5)
             : substr($this->englishMonth(), 0, 3)." ".substr($this->datetime, 8, 2).", ".substr($this->datetime, 11, 5);
    }
    
    /**
     * Seleziona il nome del mese in italiano
     * @return string
     */
    private function italianMonth(){
        $moths = array('Gennaio', 'Febbraio', 'Marzo', 'Aprile', 'Maggio', 'Giugno', 'Luglio', 'Agosto', 'Settembre', 'Ottobre', 'Novembre', 'Dicembre');
        return $moths[intval(substr($this->datetime, 5, 2)) - 1];
    }
    
    /**
     * Seleziona il nome del mese in inglese
     * @return string
     */
    private function englishMonth(){
        $moths = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
        return $moths[intval(substr($this->datetime, 5, 2)) - 1];
    }
    
    /**
     * Seleziona il nome del giorno della settimana in italiano
     * @return string
     */
    private function italianDay(){
        $days = array('Lunedì', 'Martedì', 'Mercoledì', 'Giovedì', 'Venerdì', 'Sabato', 'Domenica');
        $phpdate = strtotime($this->datetime);
        $day = intval(date('N', $phpdate)) - 1;
        return $days[$day];
    }
    
    /**
     * Seleziona il nome del giorno della settimana in inglese
     * @return string
     */
    private function englishDay(){
        $days = array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday');
        $phpdate = strtotime($this->datetime);
        $day = intval(date('N', $phpdate)) - 1;
        return $days[$day];
    }
    
    /**
     * Seleziona il numero del giorno del mese con il postfisso th
     */
    private function englishDayNumber(){
       $day = intval(date('j', strtotime($this->datetime)));
       
       $pf = "th";
       if ($day == 1) $pf = "st";
       if ($day == 2) $pf = "nd";
       if ($day == 3) $pf = "rd";
       
       return $day.$pf;
    }
    
    /**
     * Seleziona la stringa contenente una data in formato testuale
     * @param type $lang
     * @return type
     */
    public function getWDay($lang = 'it'){
        
        return ($lang == "it")
             ? $this->italianDay()." ".date('j', strtotime($this->datetime))." ".$this->italianMonth()." ".date('Y', strtotime($this->datetime))
             : $this->englishDay().", ".$this->englishMonth()." ".$this->englishDayNumber()." ".date('Y', strtotime($this->datetime));
    }
    
    /**
     * Seleziona l'orario di una datetime
     * @return styring hh:mm
     */
    public function getWTime(){
        return date('H:i', strtotime($this->datetime));
    }
}

?>
