<?php

//App::uses('AppModel', 'Model');
class Partecipate extends AppModel {

    /**
     * Controlla se la prenotazione esiste
     * @param int $iduser pk iduser che ha inoltrato la prenotazione
     * @param int  $idevent pk evento
     * @return boolean true se esiste una prenotazione al dato evento inoltrata dal dato utente
     */
    public function hasPartecipated( $iduser, $idevent ) {
        if ( isset($iduser) && isset($idevent) && ( intval( $this->find( 'count', array( 'conditions' => array( 'iduser' => $iduser, 'idevent' => $idevent) ) ) ) > 0  ) ) {
            return true;
        }
        else {
            return false;
        }
    }
    
    public function hasBeenConfirmed( $iduser, $idevent ) {
        if ( isset($iduser) && isset($idevent) && ( intval( $this->find( 'count', array( 'conditions' => array( 'iduser' => $iduser, 'idevent' => $idevent, "confirmed" => 1) ) ) ) > 0  ) ) {
            return true;
        }
        else {
            return false;
        }
    }
    
    public function hasBeenInvited( $iduser, $idevent ) {
        if ( isset($iduser) && isset($idevent) && ( intval( $this->find( 'count', array( 'conditions' => array( 'iduser' => $iduser, 'idevent' => $idevent, "confirmed" => 0) ) ) ) > 0  ) ) {
            return true;
        }
        else {
            return false;
        }
    }
    
    public function getPartecipationList( $iduser ) {
        if ( isset($iduser) ) {
            return $this->find( 'all', array( 'conditions' => array( 'iduser' => $iduser ) ) );
        }
        else {
            return false;
        }
    }

}