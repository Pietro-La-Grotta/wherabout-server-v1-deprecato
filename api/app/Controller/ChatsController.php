<?php

/**
 * Gestione dei messaggi scambiati nelle chat
 */
class ChatsController extends AppController {  

    /**
     * @deprecated by sendmessage2
     */
    public function sendmessage() {
        
        App::import('Controller',   'Notifications');
        App::import('Controller',   'Pushcodes');
        App::import('Controller',   'Users');
        
        if( isset( AppModel::getInputData()->iduser ) && isset( AppModel::getInputData()->idreceiver ) && isset( AppModel::getInputData()->message ) && isset( AppModel::getInputData()->creationdate )) {
            $idsender       = AppModel::getInputData()->iduser;
            $idreceiver     = AppModel::getInputData()->idreceiver;
            $message        = AppModel::getInputData()->message;
            $creationdate   = AppModel::getInputData()->creationdate;
            $minmessage     = "New Message";
            $pushcodeController     = new PushcodesController();
            $this->set( 'response',  Utils::generateSuccess( SuccessCodes::OK ) );
            $notificationsController = new NotificationsController();
            $notificationsController->addNotification   (   $idsender, 
                                                            $idreceiver, 
                                                            NotificationsController::NOTIFICATION_TYPE_CHATMESSAGE_RECEIVED,
                                                            NULL
                                                       );
            $userController  = new UsersController();
            $user = $userController->getuserinfo($idsender);
            $user = $user["User"];
            if ( $pushcodeController->sendPush(NotificationsController::NOTIFICATION_TYPE_CHATMESSAGE_RECEIVED, $idsender, $idreceiver, $message, $creationdate, $user["name"]." ".$user["surname"], $minmessage) ) {
               $this->set( 'response', Utils::generateSuccess( SuccessCodes::OK ) );
               //$this->saveMessageEntry($idsender, $idreceiver, $message);
            }
            else {
                $this->set( 'response', Utils::generateSuccess( SuccessCodes::RS_INPUT_ERROR ) );
            }
        }
        else {
            $this->set( 'response', Utils::generateSuccess( SuccessCodes::RS_INPUT_ERROR ) );
        }
    }
    
    /**
     * Short message per la notifica push di ricezione del messaggio
     */
    const CHAT_SHORT_MESSAGE = ' sent you a new message';
    //const CHAT_SHORT_MESSAGE = ' Big big big';
    
    
    /**
     * Codice identificativo della notifica push relativa alla ricezione di un messaggio in chat
     */
    const PUSH_TYPE = 7;
    
    /**
     * Viene richiamato il web service pushwoosh per l'invio dei messaggi in chat a tutti i disopositivi asssociati all'utente destinatario
     * Viene inserita la notifica locale per l'utente destinatario
     * Viene gestito il buffer delle notifiche locali
     * Il messaggio di chat viene memorizzato per non essere perso dai dispositivi ios che hanno l'app spenta 
     */
    public function sendmessage2(){
        App::import('Controller',   'Notifications');
        // accesso ai dati in input alla rest
        $input = AppModel::getInputData();
        
        // selezione dei dati associati al mittente
        $this->loadModel('User');
        $user = $this->User->getUserById();
        unset($this->User);
        
        // Selezione della lista dei destinatari
        $this->loadModel('Pushcode');
        $receivers = $this->Pushcode->find('all', array('conditions' => array('iduser' => $input->idreceiver), 
                                           'fields' => array('code')));                              
        
        unset($this->Pusncode);
        
//        App::import('Vendor', 'TCPConnectionManager');
//        TCPConnectionManager::flushAndClose(json_encode(array('Success' => SuccessCodes::OK)));        
//       
//        
//        sleep(15);
        
        // invio delle notifiche push ai destinatari
        App::import('Vendor', 'Pushwoosh/Pushwoosh');
            
        $pushwoosh = Pushwoosh::create(function(){
            App::import('Vendor', 'Pushwoosh/IPushParameter');
            App::import('Vendor', 'Pushwoosh/CreateParam');
            App::import('Vendor', 'Pushwoosh/CustomData');
            App::import('Vendor', 'Pushwoosh/LanguageUpdateParam');
        });
        
        $pushwoosh->setDebugMode();
        
        $pushwoosh->setIosBGAvailable();
        
        $params = new CreateParam();
        
        foreach($receivers as &$r){
            if (isset($r['Pushcode']['code']) 
            && !empty($r['Pushcode']['code'])){
                $params->receivers[] = $r['Pushcode']['code'];
            }
        }
        
        $params->shortContent = $user['name']." ".$user['surname'].self::CHAT_SHORT_MESSAGE;
        $params->customData->type = self::PUSH_TYPE;
        $params->customData->idsender = $input->iduser;
        $params->customData->username = $user['name']." ".$user['surname'];
        $params->customData->creationdate = $input->creationdate;
        $params->customData->message = $input->message;
        $params->timeToSend  = "now";
        
        $pushwoosh->createMessage($params);
        
        // salvataggio notifiche locali e notifiche push per i dispositivi ios
        $this->Chat->sendmessage();
        
        $this->set('response', array('Success' => SuccessCodes::OK));
    }
    
    /**
     * Seleziona i messaggi di chat persi dai dispositivi ios perchè l'app non era in esecuzione quando sono state ricevute le notifiche push ed eliminazione degli stessi
     */
    public function lost(){
        $this->set('response', array('Success' => SuccessCodes::OK,
                                     'Chat' => $this->Chat->getLostMessages()));
    }
    
}