/* 
 * Script usati per la lista degli eventi
 */

/**
 * Inizializzazione della tabella per la lista delle prenotazioni per un oggetto
 */
function init_data_table(){
    jQuery(document).ready(function() {
        
       var table = jQuery('#studio_leaves').DataTable({
           
            "lengthMenu": [[100, 200, 400, -1], [100, 200, 400, "All"]],
            "aaSorting": [[0,'desc']],
            "paging":   true,
            "aoColumnDefs": [{ 'bSortable': false, 
                               'aTargets': [7, 8]
                           }]
        });
       
        table.column(0).visible(false);
        
         // selezione multipla delle righe
       jQuery('#studio_leaves tbody').on( 'click', 'tr', function (){
           jQuery(this).toggleClass('selected');
       });
       
       // Abilitazione degli eventi selezionati
       
       jQuery('#active_button').click( function () {
            var x = table.rows('.selected').data().length;
            
            if (x < 1){
                alert('E\' necessario selezionare gli eventi da abilitare');
                return;
            }
            
            if (confirm('Si è certi di volere abilitare gli eventi selezionati?')){
                jQuery('#events_to_active').val(getSelectedList());
                document.active_events_form.submit();  
            }
        });
        
        // Disabilitazione degli eventi selezionati
       
       jQuery('#disable_button').click( function () {
            var x = table.rows('.selected').data().length;
            
            if (x < 1){
                alert('E\' necessario selezionare gli eventi da disabilitare');
                return;
            }
            
            if (confirm('Si è certi di volere disabilitare gli eventi selezionati?')){
                jQuery('#events_to_disable').val(getSelectedList());
                document.disable_events_form.submit();  
            }
        });
       
    });
}

init_data_table();

var comodo;

/**
 * Genera la lista degli id delle righe selezionate
 * @returns {@exp;JSON@call;stringify} lista in formato json trasformata in stringa
 */
function getSelectedList(){
    comodo = [];
    jQuery('.selected').each(function (){
        comodo[comodo.length] = jQuery(this).attr("id");
    });
    return JSON.stringify(comodo);
}