<?php
/**
 * Application model for CakePHP.
 *
 * This file is application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Model
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Model', 'Model');
/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       app.Model
 */
class AppModel extends Model {
    
    /**
     * Dati in input
     * @var type 
     */
    protected static $inputData;
    
    /**
     * idUser : id univoco del dispositivo
     * @var string 
     */
    protected static $idUser;
    
    /**
     * Codice di lingua inviato dal client
     */
    protected static $langauge;
     
    /**
     * Importa i dati passati in input
     * Non viene fatta alcuna codifica html
     * @param stObj $in
     */
    public static function setInput($in){
        self::$inputData = $in;
    }
    
    /**
     * Importa il codice utente
     * Non viene fatta alcuna codifica html
     * @param string $idUser
     */
    public static function setIdUser($idUser) {
        self::$idUser = $idUser;
    }
    
    /**
     * In qualsiasi momento è possibile accedere al dato in input tramite la AppModel
     * @return input object data
     */
    public static function getInputData(){
        return self::$inputData;
    }
    
    /**
     * Importa i dati passati di lingua
     * Non viene fatta alcuna codifica html
     * @param stObj $in
     */
    public static function setLanguage(&$in){
        self::$langauge = $in;
    }
    
    /*
        var dump dell'ultima query
     */
    
//    public function showLastQuery() {
//        $dbo        = $this->getDatasource();
//        $logs       = $dbo->getLog();
//        //$lastLog    = end($logs['log']);
//        //var_dump( $lastLog['query']);
//        
//        var_dump($logs['log']);
//    }
    
}
