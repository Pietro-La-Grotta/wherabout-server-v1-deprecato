<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Event
 *
 * @author Christian
 */
//App::uses('AppModel', 'Model');
class DateListedCreatedEvent extends AppModel{
    public function getList( $basedate, $iduser ) {
       return $this->find( 'all', array( 'conditions' => array( 'date >' => $basedate, 'iduser' => $iduser), 'order' => array('date ASC') ) );
    }
    
    /**
     * Seleziona il limite superiore dell'intervallo di date a cui devono appartenere gli eventi da selezionare nel caso di selezione degli eventi creati dall'utente
     * @return false se il limite superiore corrisponde a quello inferiore; string (aaa-mm-gg) data che indica il limite superiore dell'intervallo da considerare 
     * 
     * @deprecated by Model/AvCountCreatedEvent.getLastDate()
     */
    public function getLastDate(){
        $dates = $this->find( 'all', array( 'conditions' => array( 'date >' => self::$inputData->lastdate, 
                                                                   'iduser' => self::$inputData->iduser), 
                                            'order' => array('date ASC')));
        $items = count($dates);
        if ($items == 0) return false;
        
        $events = $i = 0;
        $maxevents = self::$inputData->quantity; 
        
        do {
            $lastdate = $dates[$i]['DateListedCreatedEvent']['date'];
            $events += $dates[$i]['DateListedCreatedEvent']['occurance'];
            $i++;
        } 
        while (($i < $items) and ($events < $maxevents));
        
        return $lastdate;
    }    
}
