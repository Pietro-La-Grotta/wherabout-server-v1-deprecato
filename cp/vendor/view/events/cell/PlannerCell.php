<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TitleCell
 * Cognome e nome 
 * @author pietro@studioleaves.com
 */
class PlannerCell implements ICell{
     
    private static $max_title_len = 15;
    private static $href_title = "Vai al profilo del planner";
    private static $href_params = "index.php/utenti?show=edit&item=";
    
    private $tuple;
    private $name_label_field;
    private $surname_label_field;
    private $id_label_field;
   
    /**
     * Inizializza le variabili di instanza
     * @param string $name_label_field nome del campo della tupla contenente il nome di chi ha creato l'evento
     * @param string $surname_label_field nome del campo della tupla contenente il cognome di chi ha creato l'evento
     * @param string $id_label_field nome del campo della tupla contenente la pk di chi ha creato l'evento
     */
    function __construct($name_label_field, $surname_label_field, $id_label_field) {
        $this->name_label_field = $name_label_field;
        $this->surname_label_field = $surname_label_field;
        $this->id_label_field = $id_label_field;
    }

    
    /**
     * Stampa il contenuto html
     * @return string
     */
    public function html() {
        $html = "";
            
            $planner = $this->tuple[$this->surname_label_field].' '.$this->tuple[$this->name_label_field];
            $label = (strlen($planner) > self::$max_title_len) 
                   ? substr($planner, 0, self::$max_title_len).'...'
                   : $planner;
            
            $html .= '<td>';
                $html .= '<a href="'.Juri::base().self::$href_params.$this->tuple[$this->id_label_field].'" title="'.self::$href_title.'" target="_blank">';
                $html .= $label;
                $html .= '</a>';
            $html .= '</td>';
            
        return $html;
    }

    /**
     * Setter per la tupla
     * @param array $tuple
     */
    public function data(&$tuple) {
        $this->tuple = $tuple;
    }    
}

?>
