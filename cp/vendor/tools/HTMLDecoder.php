<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of HTMLDecoder
 * Codifica e decodifica html
 * @author pietro@studioleaves.com
 */
class HTMLDecoder {
    
    /**
     * Codifica di ent_quotes per stringhe da passare al db in input
     * @param $string stringa in input per il db
     * @return stringa codificata
     */
    public static function encode($string){
        return htmlspecialchars($string, ENT_QUOTES);
    }
    
    /**
     * Decodifica delle stringhe prelevate dal database
     * @param $string stringa in output dal db
     * @return stringa decodificata
     */
    public static function decode($string){
        return htmlspecialchars_decode($string, ENT_QUOTES);
    }
    
    //public static function custom_json_encode()
    
    /**
     * Formattazione data italiana
     * @param string $date format: aaaa-mm-gg hh:mm:ss | aaaa-mm-gg
     * @return string format: gg-mm-aaaa hh:mm:ss | gg-mm-aaaa
     */
    public static function italianDateTime($date, $onlyDate = false)
    {
        if (!isset($date) or ($date == "0000-00-00 00:00:00")) return "Mai";
        
        $d_t = explode(" ", $date);
        $d = explode("-", $d_t[0]);
        
        return ($onlyDate) 
             ? $d[2]."-".$d[1]."-".$d[0]
             : $d[2]."-".$d[1]."-".$d[0]." ".(isset($d_t[1]) ? $d_t[1] : '');
    }
    
//    /**
//     * Seleziona la traduzione italiana di una label
//     * @param string $label oggetto json contenente tutte le traduzioni di una label, prelevato dal db
//     * @return stringa contenente solo la label in italiano
//     */
//    public static function italianLabel($label){
//        $obj = json_decode(self::decode($label));
//        return $obj->it;
//    }
//    
//    /**
//     * Seleziona la traduzione inglese di una label
//     * @param string $label oggetto json contenente tutte le traduzioni di una label, prelevato dal db
//     * @return stringa contenente solo la label in inglese
//     */
//    public static function englishLabel($label){
//        $obj = json_decode(self::decode($label));
//        return $obj->en;
//    }
//    
//    /**
//     * Date le versioni nelle due lingue di una label, queste vengono trasformate in un oggetto json codificato per il db
//     * @param string $it versione della label in italiano
//     * @param string $en versione della label in inglese
//     * @return string oggetto json codificato per il db
//     */
//    public static function margeLanguages($it, $en){
//        return self::encode(json_encode((object)array('it' => $it, 'en' => $en)));
//    }
    
    /**
     * Sostituisce i backslash con double backslash 
     * @param string $query
     */
    public static function mysql_specialchars($q){
        $bs = "\\"; //in realtà è solo uno 
        return str_replace($bs, $bs.$bs, $q);
    }
    
    /**
     * Trasforma una data da formato italiano a formato sql
     * @param string $date es: gg-mm-aaaa
     * @return string es: aaaa-mm--dd
     */
    public static function mysqDate($date){
        $ex = explode('-', $date);
        return $ex[2].'-'.$ex[1].'-'.$ex[0];
    }
}

?>
