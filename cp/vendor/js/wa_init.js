/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Inizializzazione di elementi custom per il pannello di amministrazione wherabout
 */


jQuery(document).ready(function(){
    jQuery('#sidebar').attr('class', 'span2');
    jQuery('#content').attr('class', 'span10');
});

/**
 * Nella pagina di dettaglio degli utenti, se l'amministratore ne dà conferma 
 * @param {type} iduser
 * @returns {unresolved}
 */
function set_default_user_image(iduser){
    if (!confirm('L\'attuale avatar dell\'utente verrà eliminato e sarà rimpiazzato con l\'avatar di default.\nContinuare?')) return;
    window.location.href = "?show=edit&action=defaultavatar&item=" + iduser;   
}

function set_email(email){
    jQuery(document).ready(function(){
        jQuery('#email').val(email);
    });
}
