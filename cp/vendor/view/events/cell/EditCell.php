<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of StatusCell
 * Collegamento a dettagli evento
 * @author pietro@studioleaves.com
 */
class EditCell implements ICell{

    private static $title = 'Dettagli evento';
    private static $icon = 'look-icon.png';
    private static $href_params = "?show=edit&item=";
    
    private $tuple;
    private $idevent_label_field;
    
    /**
     * Inizializza le variabili di istanza
     * @param string $status_label_field campo della tupla contenente lo stato dell'evento
     */
    function __construct($idevent_label_field) {
        $this->idevent_label_field = $idevent_label_field;
    }

    
    /**
     * Stampa il contenuto html
     * @return string
     */
    public function html() {
        $html = "";
            
        $html .= '<td align="center">';
            $html .= '<a href="'.Juri::base().self::$href_params.$this->tuple[$this->idevent_label_field].'" title="'.self::$title.'" target="_blank" title="'.self::$title.'">';
            $html .= '<img src="'.JUri::base().'vendor/view/events/cell/icons/'.self::$icon.'"/>';
            $html .= '</a>';
        $html .= '</td>';
            
        return $html;
    }

    /**
     * Setter per la tupla
     * @param array $tuple
     */
    public function data(&$tuple) {
        $this->tuple = $tuple;
    }    
}

?>
