-- Modifica tabella utenti
ALTER TABLE `users` ADD `up_feedback` INT NOT NULL DEFAULT '0' COMMENT 'Numero di feedback positivi' , ADD `down_feedback` INT NOT NULL DEFAULT '0' COMMENT 'Numero di feedback negativi' ;

-- Creazione della tabella dei feedback

--
-- Struttura della tabella `feedbacks`
--

CREATE TABLE IF NOT EXISTS `feedbacks` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'pk feedback',
  `idplanner` int(11) NOT NULL COMMENT 'pk planner',
  `idsender` int(11) NOT NULL COMMENT 'pk sender',
  `idevent` int(11) NOT NULL COMMENT 'pk event',
  `vote` int(1) DEFAULT NULL COMMENT '0: down_feedback, 1: up_feedback',
  `refuse` int(1) NOT NULL DEFAULT '0' COMMENT '0: normal feedback, 1: l''utente non vuole lasciare un feedback per questo evento',
  `comment` text COMMENT 'Commento associato al feedback',
  `creation_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'datatime invio del feedback',
  PRIMARY KEY (`id`),
  KEY `idplanner` (`idplanner`),
  KEY `idsender` (`idsender`),
  KEY `idevent` (`idevent`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Feedback che gli utenti lasciano ai planner' AUTO_INCREMENT=1 ;


--
-- Limiti per la tabella `feedbacks`
--
ALTER TABLE `feedbacks`
  ADD CONSTRAINT `feedbacks_ibfk_3` FOREIGN KEY (`idevent`) REFERENCES `events` (`idevent`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `feedbacks_ibfk_1` FOREIGN KEY (`idplanner`) REFERENCES `users` (`iduser`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `feedbacks_ibfk_2` FOREIGN KEY (`idsender`) REFERENCES `users` (`iduser`) ON DELETE NO ACTION ON UPDATE NO ACTION;


--
-- Vista per la selezione della lista dei feedbak ottenuti da un utente
--

DROP VIEW IF EXISTS av_user_feedbacks;

CREATE VIEW av_user_feedbacks AS
SELECT `f`.`vote`, `f`.`comment`, `f`.`creation_time`, `f`.`idplanner`, 
       `e`.`idevent`, `e`.`title` event_title, 
       `u`.`iduser` senderid, concat(`u`.`name`, ' ', `u`.`surname`) as sendername, `u`.`img`
FROM `feedbacks` f JOIN `events` e ON `f`.`refuse` = 0 AND `f`.`idevent` = `e`.`idevent`
                   JOIN `users` u ON `f`.`idsender` = `u`.`iduser`;  


--
-- Vista per la selezione degli eventi a cui è possibile lasciare un feedback
--

DROP VIEW IF EXISTS av_events_to_feedbacks;

CREATE VIEW av_events_to_feedbacks AS
select e.*, u.img as plannerimg, p.iduser as idpartecipant, f.refuse from
events e join partecipates p on e.idevent = p.idevent and e.date < date(now()) and e.activated = 1 and p.confirmed = 1
         left join feedbacks f on p.idevent = f.idevent and p.iduser = f.idsender
         join users u on e.iduser = u.iduser;