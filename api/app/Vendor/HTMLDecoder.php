<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of HTMLDecoder
 * Codifica e decodifica html
 * @author pietro@studioleaves.com
 */
class HTMLDecoder {
    
    /**
     * Codifica di ent_quotes per stringhe da passare al db in input
     * @param $string stringa in input per il db
     * @return stringa codificata
     */
    public static function encode($string){
        return htmlspecialchars($string, ENT_QUOTES);
    }
    
     /**
     * Decodifica delle stringhe prelevate dal database
     * @param $string stringa in output dal db
     * @return stringa decodificata
     */
    public static function decode($string){
        return htmlspecialchars_decode($string, ENT_QUOTES);
    }
    
    public static function hourDecode($hour) {
        return str_replace(".", ":", $hour);
    }
   
    /**
     * Formattazione data italiana
     * @param string $date format: aaaa-mm-gg hh:mm:ss | aaaa-mm-gg
     * @return string format: gg-mm-aaaa hh:mm:ss | gg-mm-aaaa
     */
    public static function italianDateTime($date, $onlyDate = false)
    {
        if (!isset($date) or ($date == "0000-00-00 00:00:00")) return "Mai";
        
        $d_t = explode(" ", $date);
        $d = explode("-", $d_t[0]);
        
        return ($onlyDate) 
             ? $d[2]."-".$d[1]."-".$d[0]
             : $d[2]."-".$d[1]."-".$d[0]." ".(isset($d_t[1]) ? $d_t[1] : '');
    }
}

?>
