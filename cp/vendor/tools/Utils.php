<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Utis
 * Strumenti di utilità per il codice
 * @author pietro@studioleaves.com
 */
class Utils {
    
    const CP_PATH = '/v1/cp/'; // cartella di installazione del pannello di amministrazione
    const API_PATH = '/v1/api/'; // cartella di installazione delle API
    const EVENTS_IMAGE_PATH = 'resources/event/'; // cartella esterna alle versioni del sistema
    const USERS_IMAGE_PATH = 'resources/user/'; // cartella esterna alle versioni del sistema
    
    /**
     * Ritrova da url la stringa identificativa della pagina
     * @param type $pos
     * @return type
     */
    public static function getPageName($pos = 0){
        $url = JRequest::getUri();
        
        if (stripos($url, '?') !== FALSE){
            $a = explode('?', $url);
            $s = $a[0];
        }
        else{
            $s = $url;
        }
        
        $b = explode('/', $url);
        $c = array();
        foreach (array_reverse($b) as $x) if (strlen($x) > 0) $c[] = $x;
        return $c[$pos];
    }
    
    /**
     * Seleziona il percorso della cartella in cui sono contenute le immagini associate agli eventi
     * @return string absolute url
     */
    public static function eventsImagePath(){
        return str_replace(self::CP_PATH, "/", JUri::base()).self::EVENTS_IMAGE_PATH;
    }
    
    /**
     * Seleziona il percorso della cartella in cui sono contenute le immagini associate agli utenti
     * @return string absolute url
     */
    public static function usersImagePath(){
        return str_replace(self::CP_PATH, "/", JUri::base()).self::USERS_IMAGE_PATH;
    }
    
    /**
     * Dato il codice del sesso contenuto sul database 
     * @param int $sex sex code contenuto sul database
     * @return array per il popolamento del checkboxgroup
     */
    public static function sexFormat($sex){
        /*
        //Sex gender sul client
        public enum SEXGENDERTYPE {
              SEX_GENDER_TYPE_M     = 0
            , SEX_GENDER_TYPE_F     = 1
            , SEX_GENDER_TYPE_G     = 2
            , SEX_GENDER_TYPE_MF     = 3
            , SEX_GENDER_TYPE_MG     = 4
            , SEX_GENDER_TYPE_FG     = 5
            , SEX_GENDER_TYPE_MFG     = 6
            , SEX_GENDER_TYPE_ALL    = -1
        }
        */
        
        // i gay sono stati eliminati
        
        switch ($sex){
            case 0 : return array(0); // solo uomini
            case 1 : return array(1); // solo donne
            
            case 3 : 
            case -1: return array(0, 1); // uomini e donne
                
            default : return array();
        }
    }
}

?>
