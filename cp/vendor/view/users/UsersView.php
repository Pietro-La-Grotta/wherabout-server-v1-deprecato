<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of EventsView
 * Elementi grafici della sezione utenti
 * @author pietro@studioleaves.com
 */
class UsersView {
    
    
    /**
     * Mostra il sottotitolo per la lista degli eventi
     */
    public static function listTitle(){
        return '<h3>Utenti registrati a Wherabout</h3>';
    }
    
    /**
     * Barra degli strumenti per la lista degli eventi
     * @return string
     */
    public static function listInstruments(){
        $html = "";
        $html .= '<div class="moduletable">
                    <div class="custom">
                        <div class="moduletable" align="right">
                            <ul class="breadcrumb">';
        $html .=              '<li class="active">
                                    <form name="active_users_form" method="POST" style="margin:0px;">
                                        <input type="hidden" name="users_to_active" id="users_to_active">  
                                        <span class="btn" title="Rimuovi il ban sgli utenti selezionati" id="active_button"><i class="icon-ok"></i>   Abilita</span>
                                    </form>  
                                </li>
                                <li class="active">
                                    <form name="disable_users_form" method="POST" style="margin:0px;">
                                        <input type="hidden" name="users_to_disable" id="users_to_disable">  
                                        <span class="btn" title="Banna gli utenti selezionati" id="disable_button"><i class="icon-unpublish"></i>   Disabilita</span>
                                    </form>  
                                </li>';
        $html .=               '<li class="active">
                                        <a class="btn" title="Imposta filtri di ricerca" id="button_search" href="'.JUri::base().'index.php/utenti?show=search"><i class="icon-search"></i>   Ricerca Avanzata</a>  
                                </li>';
        
        if (UsersController::$searchparam){
            $html .=           '<li class="active">
                                    <img title="Ci sono dei filtri attivi" src="vendor/view/events/cell/icons/ic_warning45.png" onclick="alert(\'ATTENZIONE: Alcuni filtri di ricerca avanzata sono attivi\');" style="cursor:pointer;margin-left:10px;"/>
                                </li>';
        }
                                
        $html .=            '</ul>
                        </div>
                    </div>
		</div>';
        return $html;
    }
    
    /**
     * Lista da visualizzare
     * @var lista di items estratti dal database 
     */
    public static $list = array();
    
    /**
     * Table filling e visualizzazione della lista di eventi
     */
    public static function showlist(){
        if (count(self::$list) == 0){
            JFactory::getApplication()->enqueueMessage('Lista utenti vuota', 'warning');
            return '';
        }
        
        // registrazione dei renderer delle cell
        JLoader::register('UsernameCell', 'vendor/view/users/cell/UsernameCell.php');
        JLoader::register('EmailCell', 'vendor/view/users/cell/EmailCell.php');
        JLoader::register('FacebookCell', 'vendor/view/users/cell/FacebookCell.php');
        JLoader::register('GoogleCell', 'vendor/view/users/cell/GoogleCell.php');
        JLoader::register('DateCell', 'vendor/view/users/cell/DateCell.php');
        JLoader::register('StatusCell', 'vendor/view/users/cell/StatusCell.php');
        JLoader::register('CreatedCell', 'vendor/view/users/cell/CreatedCell.php');
        JLoader::register('BookedCell', 'vendor/view/users/cell/BookedCell.php');
        
        $rules = array();
        $rules['UTENTE'] = array('type' => 'custom', 'ICell' => new UsernameCell('iduser', 'surname', 'name'), 'thtooltip' => 'Nome e Cognome degli Utenti');
        $rules['EMAIL'] = array('type' => 'custom', 'ICell' => new EmailCell('email'), 'thtooltip' => 'Indirizzo Email con cui gli Utenti si sono registrati');
        $rules['FB'] = array('type' => 'custom', 'ICell' => new FacebookCell('idfacebook'), 'thtooltip' => 'utenti registrati tramite account Facebook');
        $rules['G+'] = array('type' => 'custom', 'ICell' => new GoogleCell('idgoogle'), 'thtooltip' => 'Utenti registrati tramite account Google');
        $rules['REG. IL'] = array('type' => 'custom', 'ICell' => new DateCell('creationdate'), 'thtooltip' => 'Data di registrazione degli Utenti');
        $rules['STATO'] = array('type' => 'custom', 'ICell' => new StatusCell('activated'), 'thtooltip' => 'Stato attuale degli utenti');
        $rules['CREATI'] = array('type' => 'custom', 'ICell' => new CreatedCell('created_events', 'iduser'), 'thtooltip' => 'Numero di Eventi che gli Utenti hanno creato');
        $rules['BOOKED'] = array('type' => 'custom', 'ICell' => new BookedCell('partecipates_events', 'iduser'), 'thtooltip' => 'Numero di Eventi per cui gli Utenti hanno inviato una Prenotazione');
        
        $table = new DataTable(self::$list, $rules);
        $table->setIdSource('iduser');
        $table->setInitJS('users.js');
        
        $style = '<style></style>';
        
        return $style.$table->html();
    }
    
    /**
     * Dettagli principali dell'utente
     * @param object $user
     * @return string html
     */
    public static function mainDetails(&$user){
        JLoader::register('DateFormat', 'vendor/tools/DateFormat.php');
        $icons = array('fb-active.png', 'fb-unactive.png', 'google-active.png', 'google-unactive.png','ic_power_red.png', 'ic_power_green.png');
        
        $html = '';
        $html .= '<div class="moduletable" style="border-bottom: 1px solid #eee;">
                    <div class="custom">
                    <h3>'.$user->surname.' '.$user->name.'</h3><br>
                        <div style="height:250px;">
                            <div style="float:left;margin-right:45px;">
                                <p><img src="'.$user->img.'" style="width:128px;height:128px;"></p>
                                <p style="margin-top:20px;cursor:pointer;color:#08c;text-decoration:underline;"
                                   title="Elimia Avatar e reimpoosta quello di default" 
                                   onclick="set_default_user_image('.$user->iduser.');">Reimposta Avatar di Default</p>    
                            </div>
                            <div style="float:left;margin-top:20px;">
                                
                                <p>Registrazione con i social network:&nbsp;&nbsp;
                                   <img src="'.JUri::base().'vendor/view/users/cell/icons/'.$icons[$user->idfacebook >= 0 ? 0 : 1].'">&nbsp;&nbsp;
                                   <img src="'.JUri::base().'vendor/view/users/cell/icons/'.$icons[$user->idgoogle >= 0 ? 2 : 3].'">&nbsp;&nbsp;
                                </p>';
        
        $html .=            '<p>
                               <img src="'.JUri::base().'vendor/view/users/cell/icons/'.$icons[$user->activated < 1 ? 4 : 5].'">&nbsp;&nbsp;
                               <strong>'.(($user->activated < 1) ? 'UTENTE BANNATO' : 'UTENTE ATTIVO').'</strong>    
                             </p><br>';
                                
        $signUpDate = new DateFormat($user->creationdate);
        
        $html .=               '<p>Data di registrazione: <strong>'.$signUpDate->getWallDate().'</strong></p>';
        
        $lastUpdateDate = new DateFormat($user->updatetime);
        
        $html .=               '<p>Data ultima modifica: <strong>'.$lastUpdateDate->getWallDate().'</strong></p><br>';
        
        $urlToCreated = "index.php/utenti?show=created&item=";
        $urlToBooked = "index.php/utenti?show=partecipates&item=";
        
        $html .=               '<p>
                                   Numero di eventi creati: 
                                   <a href="'.Juri::base().$urlToCreated.$user->iduser.'" title="Visualizza la lista degli eventi creati dall&#39;" target="_blank">
                                       '.$user->created_events.'
                                   </a>
                                </p>';
                                
        $html .=               '<p>
                                   Numero di eventi prenotati: 
                                   <a href="'.Juri::base().$urlToBooked.$user->iduser.'" title="Visualizza la lista degli eventi a cui l&#39;utente ha inviato una prenotazione" target="_blank">
                                       '.$user->partecipates_events.'
                                   </a>
                                </p>';                        
        $html .=           '</div>
                        </div>
                    </div>
                </div>';
        
        return $html;
    }
    
    /**
     * Titolo per editor evento
     * @return string
     */
    public static function updateUserTitle(){
        return '<h3>Editor dell&#39;utente</h3><br><br>';
    }
    
    /**
     * Titolo per impostazione filtri su ricerca
     * @return string
     */
    public static function searchUsersTitle(){
        return '<h3>Impostazione dei filtri di ricerca</h3><p>In questa pagina è possibile impostare dei parametri per il filtraggio della lista degli utenti. I filtri rimarranno attivi finchè non saranno rimossi.<p>';
    }
    
    /**
     * Tasto per ritornare alla lista degli eventi
     * @return string
     */
    public static function backToUsersList(){
        $html = "";
        $html .= '<div class="moduletable" style="margin-bottom:35px;">
                    <div class="custom">
                        <div class="moduletable">
                            <ul class="breadcrumb">
                                <li class="active">
                                   <a title="Torna alla lista utenti" href="'.JUri::base().'index.php/utenti?show=list">&lsaquo;&lsaquo;&nbsp;Torna alla lista degli utenti</a>  
                                </li>
                                <li class="active" style="float:right;">
                                   <a  title="Rimuove tutti i filtri di ricerca" href="'.JUri::base().'index.php/utenti?show=search&action=reset">Pulisci Filtri</a>  
                                </li>
                            </ul>
                        </div>
                    </div>
		</div>';
        return $html;
    }
    
    /**
     * Lista delle prenotazioni inviate da parte di un utente
     */
    public static function showBooked(){
        if (count(self::$list) == 0){
            JFactory::getApplication()->enqueueMessage('Lista partecipanti vuota', 'warning');
            return '';
        }
        
        // registrazione dei renderer delle cell
        
        JLoader::register('DateCell', 'vendor/view/users/cell/DateCell.php');
        JLoader::register('TextCell', 'vendor/view/users/cell/TextCell.php');
        JLoader::register('EventNameCell', 'vendor/view/users/cell/EventNameCell.php');
        JLoader::register('ConfirmedCell', 'vendor/view/users/cell/ConfirmedCell.php');
        
        $rules = array();
        $rules['POS'] = array('type' => 'standard_cell', 'model' => 'id_booking');
        $rules['DATA EVENTO'] = array('type' => 'custom', 'ICell' => new DateCell('date'), 'thtooltip' => 'Giorno in cui l&#39;Evento si svolgerà');
        $rules['CATEGORIA'] = array('type' => 'custom', 'ICell' => new TextCell('category_name'), 'thtooltip' => 'Categoria e sottocategoria (se selezionata) di appartenenza dell&#39;Evento');
        $rules['TITOLO'] = array('type' => 'custom', 'ICell' => new EventNameCell('idevent', 'title'), 'thtooltip' => 'Titolo dell&#39;Evento');
        $rules["CITTA'"] = array('type' => 'custom', 'ICell' => new TextCell('city'), 'thtooltip' => 'Città in cui l&#39;Evento si svolgerà');
        $rules['CONFERMATO'] = array('type' => 'custom', 'ICell' => new ConfirmedCell('confirmed'), 'thtooltip' => 'Stato attuale dell&#39;Evento');
        
        $table = new DataTable(self::$list, $rules);
        $table->setIdSource('id_booking');
        $table->setInitJS('booked.js');
        
        $style = '<style></style>';
        
        return $style.$table->html();
    }
    
    /**
     * Titolo della pagina per la lista degli eventi creati da un utente
     * @param array $userDetail dati dell'utente planner
     * @return string html
     */
    public static function createdTitle(&$userDetail){
        return '<h3>Lista degli eventi creati dall&#39;utente
                    <a style="color:darkBlue;" target="_blank" href="'.Juri::base().'index.php/utenti?show=edit&item='.$userDetail->iduser.'">
                        '.ucwords($userDetail->name.' '.$userDetail->surname).'
                    </a>
                </h3>
                <br>';
    }
    
    /**
     * Titolo della pagina per la lista delle prenotazioni inoltrate dall'utente
     * @param array $userDetail dati dell'utente partecipante
     * @return string html
     */
    public static function bookingTitle(&$userDetail){
        return '<h3 style="line-height: 1.5em">Lista degli eventi per cui l&#39;utente
                    <a style="color:darkBlue;" target="_blank" href="'.Juri::base().'index.php/utenti?show=edit&item='.$userDetail->iduser.'">
                        '.ucwords($userDetail->name.' '.$userDetail->surname).'
                    </a> ha richiesto la prenotazione
                </h3>
                <br>';
    }
    
    /**
     * Barra degli strumenti per la lista delle prenotazioni
     * @return string
     */
    public static function bookingInstruments(){
        $html = "";
        $html .= '<div class="moduletable">
                    <div class="custom">
                        <div class="moduletable" align="right">
                            <ul class="breadcrumb">
                                <li class="active">
                                    <form name="confirm_booking_form" method="POST" style="margin:0px;">
                                        <input type="hidden" name="books_to_confirm" id="books_to_confirm">  
                                        <span class="btn" title="Conferma le prenotazioni selezionate" id="confirm_button"><i class="icon-ok"></i>   Conferma</span>
                                    </form>  
                                </li>
                                <li class="active">
                                    <form name="remove_confirm_form" method="POST" style="margin:0px;">
                                        <input type="hidden" name="confirmation_to_remove" id="confirmation_to_remove">  
                                        <span class="btn" title="Rimuovi conferma alle prenotazioni selezionate" id="remove_button"><i class="icon-unpublish"></i>   Rimuovi Conferma</span>
                                    </form>  
                                </li>
                                <li class="active">
                                    <form name="delete_booking_form" method="POST" style="margin:0px;">
                                        <input type="hidden" name="delete_booking" id="delete_booking">  
                                        <span class="btn" title="Rimuovi le prenotazioni selezionate" id="delete_button"><i class="icon-delete"></i>   Elimina</span>
                                    </form>  
                                </li>
                           </ul>
                        </div>
                    </div>
		</div>';
        return $html;
    }
}

?>
