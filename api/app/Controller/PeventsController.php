<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * PeventsController Classe di comodo per la deprecazione dei metodi della classe EventsController
 * @author pietro@studioleaves.com
 */

class PeventsController extends AppController{
    
    /**
     * Seleziona una pagina della lista degli eventi creati dall'utente connesso 
     */
    public function usercreatedevents(){
        $this->loadModel('AvCountCreatedEvent');
        
        /**
         * Se il limite superiore ed il limite inferiore dell'intervallo coincidono la rest restituisce una lista vuota
         */
        
        $sup = $this->AvCountCreatedEvent->getLastDate();
        
        if ($sup === false){
            $this->set('response', array('Success' => SuccessCodes::OK,
                                         'eventsdayslist' => array()));
            
            return;
        }
        
        unset($this->AvCountCreatedEvent);
        $this->loadModel('AvMyEvents');
        
        $this->set('response', array('Success' => SuccessCodes::OK,
                                         'eventsdayslist' => $this->AvMyEvents->select($sup)));
       
        unset($this->AvMyEvents);
    }
    
     /**
     * Seleziona una pagina della lista degli eventi a cui l'utente ha effettuato il boocking e che il creatore dell'evento ha confermato 
     */
    public function partecipatedevents(){
        $this->loadModel('AvDateTogoEvent');
        
        /**
         * Se il limite superiore ed il limite inferiore dell'intervallo coincidono la rest restituisce una lista vuota
         */
        
        $sup = $this->AvDateTogoEvent->getLastDate();
        
        if ($sup === false){
            $this->set('response', array('Success' => SuccessCodes::OK,
                                         'eventsdayslist' => array()));
            
            return;
        }
        
        unset($this->AvDateTogoEvent);
        $this->loadModel('AvTogoEvent');
        
        $this->set('response', array('Success' => SuccessCodes::OK,
                                     'eventsdayslist' => $this->AvTogoEvent->select($sup)));
       
        unset($this->AvTogoEvent);
    }
    
    /**
     * REST version 2.0 ex events::addevent()
     * Viene creato un nuovo evento ed inviate notifiche e notifiche push ai folllower se l'evento è pubblico
     * Il titolo dell'evento viene trasformato in maiuscolo
     * In output vi è il success 
     */
    public function addevent() {
        // creazione immagine
        App::import('Vendor', 'ImageManager');
        ImageManager::$folder = '../../../../'.Utils::EVENT_IMAGE_PATH;
        $tryImage = ImageManager::base64toJpg(AppModel::getInputData()->img);
        
        $imageName = Utils::URL_BASE.Utils::EVENT_IMAGE_PATH.(($tryImage === false) ? Utils::EVENT_DEFAULT_IMAGE : $tryImage);
        
        // salvataggio evento
        $this->loadModel('Event');
        $this->Event->addEvent($imageName);
        $idEvent = $this->Event->getLastInsertId();
        unset($this->Event);
        
        // invio della risposta al client
        App::import('Vendor', 'TCPConnectionManager');
        TCPConnectionManager::flushAndClose(json_encode(array('Success' => SuccessCodes::OK, 'eventid' => "".$idEvent))); 
        
        // se l'evento è pubblico i followers vengono avvisati
        if(intval(AppModel::getInputData()->scope) == 0) {
            
           App::import('Controller',   'Notifications');
            
            // invio delle notifiche locali ai followers
            $this->loadModel('Notification');
            $this->Notification->createEventNotification($idEvent, 
                                                         NotificationsController::NOTIFICATION_TYPE_FOLLOWERS_EVENT_CREATED, 
                                                         NotificationsController::NOTIFICATION_MAX_NUMBER);
            unset($this->Notification);
            
            // invio delle notifiche push ai followers
            
            $this->loadModel('AvFollowerCode');
            App::import('Vendor', 'Pushwoosh/wherabout/EventCreated');
            
            $pushwoosh = new EventCreated();
            $pushwoosh->setReceivers($this->AvFollowerCode->getFollowerCodes());
            $pushwoosh->send();
        }
        
        die();
    } 
    
    /**
     Vengono modificati i dati associati ad un evento ed inviate notifiche locali e notifiche push ai partecipanti per avvisarli
     */
    public function update() {
        App::import('Vendor', 'TCPConnectionManager');
        $idEvent = HTMLDecoder::encode(AppModel::getInputData()->idevent);
        $this->loadModel('Event');
        
        $event = $this->Event->find('first', array('conditions' => array('idevent' => $idEvent), 'fields' => array('img', 'sitcurrent')));
        
        if ($event['Event']['sitcurrent'] > AppModel::getInputData()->sittotal){
            TCPConnectionManager::flushAndClose(json_encode(array('Success' => SuccessCodes::RS_INPUT_ERROR))); 
            die();
        }
        
        App::import('Vendor', 'ImageManager');
        ImageManager::$folder = '../../../../'.Utils::EVENT_IMAGE_PATH;
        $tryImage = ImageManager::base64toJpg(AppModel::getInputData()->img);
        
        // se è stata aggiornata l'immagine
        if ($tryImage !== false){
            $imageName = Utils::URL_BASE.Utils::EVENT_IMAGE_PATH.$tryImage;
            $this->Event->updateEvent($imageName);
            
            // cancella la vecchia immagine solo se non è quella di default
            if ($event['Event']['img'] != Utils::URL_BASE.Utils::EVENT_IMAGE_PATH.Utils::EVENT_DEFAULT_IMAGE){
                ImageManager::delete($event['Event']['img']);
            }
        }
        else{ // altrimenti agggiorna evento e lascia immagine di default
            $this->Event->updateEvent();
        }
        
        unset($this->Event);
        
        // invio della risposta al client
        App::import('Vendor', 'TCPConnectionManager');
        //TCPConnectionManager::flushAndClose(json_encode(array('Success' => SuccessCodes::OK))); 
        
        // se l'evento è pubblico i followers vengono avvisati
        if(intval(AppModel::getInputData()->scope) == 0) {
            
           App::import('Controller',   'Notifications');
            
            // invio delle notifiche locali ai followers
            $this->loadModel('Notification');
            $this->Notification->updateEventNotification($idEvent, 
                                                         NotificationsController::NOTIFICATION_TYPE_PARTECIPANTS_EVENT_UPDATE, 
                                                         NotificationsController::NOTIFICATION_MAX_NUMBER);
            unset($this->Notification);
            
            // invio delle notifiche push ai partecipanti
            
            $this->loadModel('AvPartecipantCodes');
            App::import('Vendor', 'Pushwoosh/wherabout/EventUpdated');
            
            $pushwoosh = new EventUpdated();
            $pushwoosh->setReceivers($this->AvPartecipantCodes->getPartecipantCodes());
            $pushwoosh->send();
        }
        
        die('ciao');
    } 
}

?>
