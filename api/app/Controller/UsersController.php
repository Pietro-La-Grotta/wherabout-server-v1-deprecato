<?php

/**
 * UsersController is the main controller for user based request
 *
 * @author Christian Ruggiero
 * @creation 30/10/2014
 * @status in develop
 */
App::import('Controller',   'UserAcceptedFriends');
App::import('Controller',   'UserAcceptedFollows');
App::import('Controller',   'UserFollows');
App::import('Controller',   'UserFriends');
App::import('Controller',   'Notifications');
App::import('Controller',   'Pushcodes');

/*
 * TODO LIST
 * - ogni volta che viene inviato un iduser controllare se esso è un numerico, altrimenti lanciare errore 
 *  */
class UsersController extends AppController {
   
    public function index() {
        //grab all users and pass it to the view:
        $users = $this->User->find('all');
        $this->set('response', $users);
    }
 
    /**
     * Seleziona le informazioni associate al profilo utente e le restituisce al client
     */
    public function getinfo() {
        $userObject = null;
        if( isset( AppModel::getInputData()->iduser ) ) {
            $userObject = $this->User->getUserInfo( AppModel::getInputData()->iduser );
        }
        else if( isset( AppModel::getInputData()->idfacebook ) ) {
            $userObject = $this->User->getUserInfo( null, AppModel::getInputData()->idfacebook );
        }
        else if( isset( AppModel::getInputData()->idgoogle ) ) {
            $userObject = $this->User->getUserInfo( null, null, AppModel::getInputData()->idgoogle );
        }
        
        if( $userObject != null ) {
            $this->set( 'response',  $this->createUserObject( $userObject["User"] ) );
        }
        else {
            $this->set( 'response', Utils::generateSuccess( SuccessCodes::RS_INPUT_ERROR ) );
        }
    }
    
    public function getuserinfo( $iduser ) {
        if( isset( $iduser ) ) {
            return  $this->User->getUserInfo( $iduser );
        } else {
            return null;
        }
    }
    
    /**
     * Seleziona le informazioni da mostrare nel profilo utente
     */
    public function getuserprofile() { 
        $userObject = null;
        
        $iduser         = AppModel::getInputData()->iduser;
        $idusertoget    = AppModel::getInputData()->idusertoget;
        
        if( isset( $iduser ) && isset( $idusertoget ) ) {
            $userObject = $this->User->getUserInfo($idusertoget);
        }

        if( $userObject != null ) { 
            //do I follow this user?
            $followController   = new UserFollowsController();
            $isFollowing        = 0;
            if( $followController->userFollowing( $iduser, $idusertoget ) ) {
                $isFollowing    = 1;
            }
            //how many follows the user has?
            $userFollows        = $followController->userFollows( $idusertoget );
            //are we friends?
            $friendsController  = new UserFriendsController();
            $isFriend           = $friendsController->userFriendshipStatus( $iduser, $idusertoget );
            
            //how many friends the user has?
            $acceptedFriendsController = new UserAcceptedFriendsController();
            $userFriends        = $acceptedFriendsController->getFriendsCount( $idusertoget );
            
            //compose obj
            $userObj                    = $this->createUserObject( $userObject["User"] );
            
            $userObj->user->following   = $isFollowing;          
            $userObj->user->followers   = $userFollows;
            $userObj->user->friendship  = $isFriend;
            $userObj->user->friends     = $userFriends;
            
            
            
            $responseObj                = Utils::generateSuccess( SuccessCodes::OK );
            $responseObj->userinfo      = $userObj->user;
            $this->set( 'response',     $responseObj  );
        }
        else {
            $this->set( 'response', Utils::generateSuccess( SuccessCodes::RS_INPUT_ERROR ) );
        }
    }
    
    
    public function getfriendlist() {
        $iduser = AppModel::getInputData()->iduser;
        if( isset( $iduser ) ) {
            $userAcceptedFriendsController = new UserAcceptedFriendsController();
            $userFriendList = $userAcceptedFriendsController->getFriendList( $iduser );
            $successObj = Utils::generateSuccess( SuccessCodes::OK );
            $userfriendlist = array();
            foreach ($userFriendList as $singleUser) {
                $userfriendlist[] = $singleUser["UserAcceptedFriend"];
            }
            $successObj->users = $userfriendlist;
            $this->set( 'response', $successObj);
        }
        else {
            $this->set( 'response', Utils::generateSuccess( SuccessCodes::RS_INPUT_ERROR ) );
        }
    }
    
    public function getfollowlist() {
        $iduser = AppModel::getInputData()->iduser;
        if( isset( $iduser ) ) {
            $userAcceptedFollowsController = new UserAcceptedFollowsController();
            $userFollowList = $userAcceptedFollowsController->getFollowList( $iduser );
            $successObj = Utils::generateSuccess( SuccessCodes::OK );
            $userfollowlist = array();
            foreach ($userFollowList as $singleUser) {
                
                $follower = $singleUser["UserAcceptedFollow"];
                unset($follower["idfollow"]);
                
                $userfollowlist[] = $follower;
            }
            $successObj->users = $userfollowlist;
            $this->set( 'response', $successObj);
        }
        else {
            $this->set( 'response', Utils::generateSuccess( SuccessCodes::RS_INPUT_ERROR ) );
        }
    }

    /**
     * @depreated by uusers/login
     */
    public function login() {
        $userObject = null;
        $pushcode   = AppModel::getInputData()->pushcode;
        $hwid       = AppModel::getInputData()->hwid;
        
        if( isset( AppModel::getInputData()->email ) && isset( AppModel::getInputData()->password )   ) {
            
            $email      = HTMLDecoder::encode(      AppModel::getInputData()->email  );
            //$password   = AppModel::getInputData()->password;
            $password   = Utils::encodePassword(    AppModel::getInputData()->password );
            $userObject = $this->User->loginUser( $email , $password );
            
            if($userObject == null) {
                $this->set( 'response', Utils::generateSuccess( SuccessCodes::RS_LOGIN_PASSWORDWRONG ) );
            }
        }
        else if( isset( AppModel::getInputData()->idfacebook ) && AppModel::getInputData()->idfacebook != 0  ) {
            $idfacebook = AppModel::getInputData()->idfacebook;
            $userObject = $this->User->loginUser( null, null, $idfacebook );
            if($userObject == null) {
                $this->set( 'response', Utils::generateSuccess( SuccessCodes::RS_LOGIN_USERNOTEXIST ) );
            }
        }
        else if( isset( AppModel::getInputData()->idgoogle ) && AppModel::getInputData()->idgoogle != 0  ) {
            $idgoogle   = AppModel::getInputData()->idgoogle;
            $userObject = $this->User->loginUser( null, null, null, $idgoogle );
            if($userObject == null) {
                $this->set( 'response', Utils::generateSuccess( SuccessCodes::RS_LOGIN_USERNOTEXIST ) );
            }
        }
        else {
            $this->set( 'response', Utils::generateSuccess( SuccessCodes::RS_LOGIN_USERNOTEXIST ) );
        }
        
        if( $userObject != null ) {
            $userObj = $this->createLoginUserObject( $userObject["User"] ) ;
            if( isset( AppModel::getInputData()->pushcode) && isset( AppModel::getInputData()->hwid) ) {
                $pushcodeController = new PushcodesController();
                if( $pushcodeController->savePushcode( $userObj->user->iduser, $pushcode, $hwid ) ) {
                    $responseObj        = Utils::generateSuccess( SuccessCodes::OK );
                    $responseObj->User  = $userObj->user;
                    $this->set( 'response',  $responseObj );
                }
                else {
                    $this->set( 'response', Utils::generateSuccess( SuccessCodes::RS_LOGIN_PUSHCODE_ERROR ) );
                }
            }
            else {
                $this->set( 'response', Utils::generateSuccess( SuccessCodes::RS_LOGIN_PUSHCODE_ERROR ) );
            }
        }
    }    
    
    public function addpushcode() {
        $iduser     = AppModel::getInputData()->iduser;
        $pushcode   = AppModel::getInputData()->pushcode;
        $hwid       = AppModel::getInputData()->hwid;
        if( isset( $iduser ) && isset( $pushcode )  && isset( $hwid )  )  {
            $pushcodeController = new PushcodesController();
            if( $pushcodeController->savePushcode( $iduser, $pushcode, $hwid ) ) {
                $this->set('response',  Utils::generateSuccess(SuccessCodes::OK)); 
            }
            else {
                $this->set( 'response', Utils::generateSuccess( SuccessCodes::RS_REGISTRATION_PUSHCODE_ERROR ) );
            }
        }
    }
    
    /**
     * Registrazione dui un nuovo utente
     * @deprecated by /uusers/register
     */
    public function register() {
        
        $email      = AppModel::getInputData()->email;
        $name       = AppModel::getInputData()->name;
        $username   = AppModel::getInputData()->surname;
        $img        = AppModel::getInputData()->img;
        $birthdate  = AppModel::getInputData()->birthdate;
        $password   = AppModel::getInputData()->password;
        $sex        = AppModel::getInputData()->sex;
        $idfacebook = AppModel::getInputData()->idfacebook;
        $idgoogle   = AppModel::getInputData()->idgoogle;
        $pushcode   = AppModel::getInputData()->pushcode;
        $hwid       = AppModel::getInputData()->hwid;
        
        if( isset( AppModel::getInputData()->iduser ) ) {
                unset(AppModel::getInputData()->iduser);
            }
        if( !isset($email)     ||  
            !isset($name)      ||
            !isset($username)   ||
            !isset($img)       ||
            !isset($birthdate) ||
            !isset($password)  ||
            !isset($sex)
            ) {
                $this->set( 'response', Utils::generateSuccess( SuccessCodes::RS_INPUT_ERROR ) );
            }
        else {
            //$base64Img = $img;
            $img      = Utils::URL_BASE . Utils::USER_IMAGE_PATH . Utils::USER_DEFAULT_IMAGE;
            
            if ( !isset($idfacebook) || intval ($idfacebook) <= 0 || ($idfacebook == "null")) {
                AppModel::getInputData()->idfacebook = -1;
            }
            
            if ( !isset($idgoogle) || intval ($idgoogle) <= 0 || ($idgoogle == "null")) {
                AppModel::getInputData()->idgoogle = -1;
            }
            
            AppModel::getInputData()->password = Utils::encodePassword(AppModel::getInputData()->password);
            
            if ( $this->User->userExists($email) ){
                $this->set( 'response', Utils::generateSuccess( SuccessCodes::RS_REGISTRATION_DUPLICATE ) );
            }
            elseif ( isset($idfacebook) && intval ($idfacebook) > 0 && $this->User->userExists(null, intval ($idfacebook) ) ) {
                $this->set( 'response', Utils::generateSuccess( SuccessCodes::RS_REGISTRATION_DUPLICATE ) );
            }
            elseif ( isset($idgoogle) && $idgoogle > 0 && $this->User->userExists(null, null, $idgoogle) ) {
                $this->set( 'response', Utils::generateSuccess( SuccessCodes::RS_REGISTRATION_DUPLICATE ) );
            }
            else{
                
                App::import('Vendor', 'ImageManager');
                ImageManager::$folder = Utils::USER_IMAGE_PATH;
                $tryImage = ImageManager::base64toJpg(AppModel::getInputData()->img);
        
                AppModel::getInputData()->img = Utils::URL_BASE.Utils::USER_IMAGE_PATH.(($tryImage === false) ? Utils::USER_DEFAULT_IMAGE : $tryImage);
                $this->User->save(AppModel::getInputData());
                
//                $this->User->save(AppModel::getInputData());
//                $imgPath = Utils::decodeUserBase64Image( $base64Img , $this->User->id );
//                if( $imgPath == NULL )  {
//                    $imgPath = Utils::URL_BASE . Utils::USER_IMAGE_PATH . Utils::USER_DEFAULT_IMAGE;
//                }
//                $imgPath = HTMLDecoder::encode($imgPath);
//                $query = "UPDATE users SET img='".$imgPath."' WHERE iduser = ".$this->User->id;
//                $this->User->query($query);

                $this->User->clear();
                $userObj = $this->User->getUserInfo( null, null, null, $email );
                $userid = intval( $userObj["User"]["iduser"] );
                
                //momentaneamente mettiamo l'immagine con l'id
                ImageManager::duplicate((($tryImage === false) ? Utils::USER_DEFAULT_IMAGE : $tryImage), $userid);
                
                $responseObj = Utils::generateSuccess( SuccessCodes::RS_REGISTRATIONOK );
                $responseObj->iduser = $userid;
                $responseObj->img = AppModel::getInputData()->img; 
                
                //save pushcode
                $pushcodeController = new PushcodesController();
                
                if( $pushcodeController->savePushcode( $userid, $pushcode, $hwid ) ) {
                    $this->set('response',  $responseObj); 
                }
                else {
                    $this->set( 'response', Utils::generateSuccess( SuccessCodes::RS_REGISTRATION_PUSHCODE_ERROR));
                }
            } 
        }
    }
    
    
    /**
     * @deprecated by uusers/update
     */
    public function update() {
        $userObject = null;
        
        $iduser     = "";
        $email      = "";
        $name       = "";
        $surname    = "";
        $sex        = "";
        $img        = "";
        $birthdate  = "";
        $job        = "";
        $city       = "";
        $interests  = "";
        
        if( isset( AppModel::getInputData()->iduser ) ) {
            
            $iduser     = AppModel::getInputData()->iduser;
        }
        if( isset( AppModel::getInputData()->email ) ) {
            $email      = AppModel::getInputData()->email ;
        }
        if( isset( AppModel::getInputData()->name ) ) {
            $name       = AppModel::getInputData()->name ;
        }
        if( isset( AppModel::getInputData()->surname ) ) {
            $surname    = AppModel::getInputData()->surname ;
        }
        if( isset( AppModel::getInputData()->sex ) ) {
            $sex        = AppModel::getInputData()->sex ;
        }
        if( isset( AppModel::getInputData()->img ) ) {
            $img        = AppModel::getInputData()->img;
        }
        if( isset( AppModel::getInputData()->birthdate ) ) {
            $birthdate  = AppModel::getInputData()->birthdate;
        }
        if( isset( AppModel::getInputData()->job ) ) {
            $job        = AppModel::getInputData()->job;
        }
        if( isset( AppModel::getInputData()->city ) ) {
            $city       = AppModel::getInputData()->city;
        }
        if( isset( AppModel::getInputData()->interests ) ) {
            $interests  = AppModel::getInputData()->interests;
        }
        
        if( isset( $iduser ) ) {
            $userObject = $this->User->getUserInfo($iduser);
            $userObject = $userObject["User"];
        }
        
        
        if( $userObject != null ) {
            $conditions['iduser'] = $iduser;
            $this->User->iduser = $iduser;
            $oldEmail = $userObject['email'];
            
            if( !empty( $email ) )  {
                $data['email'] =     '"'.$email.'"' ;
            }
            if( isset( $name ) ) {
                $data['name'] =      '"'.$name.'"' ;    
            } 
            if( isset( $surname ) ) {
                $data['surname'] =   '"'.$surname.'"' ; 
            } 
            if( isset( $sex)) {
                $data['sex'] =       $sex ; 
            }
            
            if( !empty( $img ) ) {
                
                App::import('Vendor', 'ImageManager');
                ImageManager::$folder = Utils::USER_IMAGE_PATH;
                $tryImage = ImageManager::base64toJpg($img);
        
                $imgPath = Utils::URL_BASE.Utils::USER_IMAGE_PATH.(($tryImage === false) ? Utils::USER_DEFAULT_IMAGE : $tryImage);
                
//                $base64Img = $img;
//                $imgPath = Utils::decodeUserBase64Image( $base64Img , $iduser );
//                if( $imgPath == NULL )  {
//                    $imgPath = Utils::URL_BASE . Utils::USER_IMAGE_PATH . Utils::USER_DEFAULT_IMAGE;   
//                }
                //$imgPath = HTMLDecoder::encode($imgPath);
                $data['img'] =       '"'. $imgPath .'"' ; 
            }
            
            if( isset( $birthdate ) ) {
                $data['birthdate'] = '"'.$birthdate.'"' ;  
            }
            
            if( isset( $job )) {
                $data['job'] = '"'.$job.'"' ;  
            }
            
            if( isset( $city ) ) {
                $data['city'] = '"'.$city.'"' ;  
            }
            
            if( isset( $interests ) ) {
                $data['interests'] = '"'.$interests.'"' ;  
            }
            
            if( $this->isUserConsistent(AppModel::getInputData()) ) {
                //controllare se l'email non è stata già utilizzata
                $consistencyEmail = FALSE;
                if(!isset($email) || ($oldEmail == $email) ) {
                    $consistencyEmail = TRUE;
                } elseif ( !$this->User->userExists($email) ) {
                    $consistencyEmail = TRUE;
                } else {
                    $consistencyEmail = FALSE;
                }
                
                //die('Ciao: '. $consistencyEmail);
                
                if($consistencyEmail) {
                    $this->User->updateAll($data,$conditions);
                    $this->User->clear();
                    $this->set( 'response', Utils::generateSuccess( SuccessCodes::OK ) );
                } else {
                     //email duplicata
                    $this->set( 'response', Utils::generateSuccess( SuccessCodes::USER_USERUPDATED ) );
                }
                
            }
            else {
                $this->set( 'response', Utils::generateSuccess( SuccessCodes::RS_INPUT_ERROR ) );
            }
        }
        else {
            $this->set( 'response', Utils::generateSuccess( SuccessCodes::RS_INPUT_ERROR ) );
        }
    }
    
    public function recovery() {
        $userObject = null;
        if( isset( AppModel::getInputData()->email ) ) {
            $userObject = $this->User->getUserInfo( null, null, null, AppModel::getInputData()->email);
        }
       
        if( $userObject != null ) {
            $conditions['iduser'] = $userObject["User"]["iduser"];
            $email = $userObject["User"]["email"];
            $autoGenerated = Utils::generatePassword(rand(8, 12));
            $data['password'] = '"'.Utils::encodePassword( Utils::encodePassword( $autoGenerated )).'"' ;
            $this->User->updateAll($data,$conditions);
            $this->User->clear();
            $this->set( 'response', Utils::generateSuccess( SuccessCodes::OK ) );
            //email vars
            $emailVars = array();
            $emailVars["password"] = $autoGenerated;
            $emailVars["username"] = $userObject["User"]["name"] ." ".$userObject["User"]["surname"];
            Utils::sendEmail( Utils::EMAIL_PASSOWRD_RESET, $email, $emailVars);
        }
        else {
            $this->set( 'response', Utils::generateSuccess( SuccessCodes::RS_INPUT_ERROR ) );
        }
    }
    
    public function setnewpassword() {
        $userObject = null;
        if( isset( AppModel::getInputData()->iduser ) ) {
            $userObject = $this->User->getUserInfo( AppModel::getInputData()->iduser );
        }
        
        if( $userObject != null ) {
            $conditions['iduser'] = AppModel::getInputData()->iduser;
            $this->User->iduser = AppModel::getInputData()->iduser;
            if( !empty( AppModel::getInputData()->password     ) )  {
                $data['password'] =     '"'.Utils::encodePassword(  AppModel::getInputData()->password        ).'"' ;
            }

            $this->User->updateAll($data,$conditions);
            $this->User->clear();
            $this->set( 'response', Utils::generateSuccess( SuccessCodes::OK ) );
            
        }
        else {
            $this->set( 'response', Utils::generateSuccess( SuccessCodes::RS_INPUT_ERROR ) );
        }
    }
    
    public function follow() {
        if( isset( AppModel::getInputData()->iduser ) && isset( AppModel::getInputData()->iduserfollow )) {
            $userFollow = new UserFollowsController();
            $iduser     = HTMLDecoder::encode(AppModel::getInputData()->iduser);
            $idfollow   = HTMLDecoder::encode(AppModel::getInputData()->iduserfollow);
            if( !$userFollow->userFollowing( $iduser, $idfollow ) ) {
                if ( $userFollow->follow( $iduser, $idfollow ) ) {
                    $this->set( 'response',  Utils::generateSuccess( SuccessCodes::OK ) );
                }
                else {
                    $this->set( 'response',  Utils::generateSuccess( SuccessCodes::RS_INPUT_ERROR ) );
                }
            }
            else {
                $this->set( 'response',  Utils::generateSuccess( SuccessCodes::RS_INPUT_ERROR ) );
            }
        }
    }
    
    public function unfollow() {
        if( isset( AppModel::getInputData()->iduser ) && isset( AppModel::getInputData()->iduserfollow )) {
            $userFollow = new UserFollowsController();
            $iduser     = HTMLDecoder::encode(AppModel::getInputData()->iduser);
            $idfollow   = HTMLDecoder::encode(AppModel::getInputData()->iduserfollow);
            if( $userFollow->userFollowing( $iduser, $idfollow ) ) {
                if ( $userFollow->unfollow( $iduser, $idfollow ) ) {
                    $this->set( 'response',  Utils::generateSuccess( SuccessCodes::OK ) );
                }
                else {
                    $this->set( 'response',  Utils::generateSuccess( SuccessCodes::RS_INPUT_ERROR ) );
                }
            }
            else {
                $this->set( 'response',  Utils::generateSuccess( SuccessCodes::RS_INPUT_ERROR ) );
            }
        }
    }
    
    public function friend() { 
        if( isset( AppModel::getInputData()->iduser ) && isset( AppModel::getInputData()->iduserfriend )) {
            $userFriend = new UserFriendsController();
            $iduser     = HTMLDecoder::encode(AppModel::getInputData()->iduser);
            $idfriend   = HTMLDecoder::encode(AppModel::getInputData()->iduserfriend);
            if( !$userFriend->userFriendship( $iduser, $idfriend ) ) {
                if ( $userFriend->friend( $iduser, $idfriend ) ) {
                    $this->set( 'response',  Utils::generateSuccess( SuccessCodes::OK ) );
                    $notificationsController = new NotificationsController();
                    $notificationsController->addNotification   (   $iduser, 
                                                                    $idfriend, 
                                                                    NotificationsController::NOTIFICATION_TYPE_FRIENDSHIP_REQUEST,
                                                                    NULL
                                                                );
                    $this->sendPush( $iduser, $idfriend, "",  NotificationsController::NOTIFICATION_TYPE_FRIENDSHIP_REQUEST  );
                }
                else {
                    $this->set( 'response',  Utils::generateSuccess( SuccessCodes::RS_INPUT_ERROR ) );
                }
            }
            else {
                $this->set( 'response',  Utils::generateSuccess( SuccessCodes::RS_INPUT_ERROR ) );
            }
        }
    }
    
    public function unfriend() {
        if( isset( AppModel::getInputData()->iduser ) && isset( AppModel::getInputData()->iduserfriend )) {
            $userFriend = new UserFriendsController();
            $iduser     = HTMLDecoder::encode(AppModel::getInputData()->iduser);
            $idfriend   = HTMLDecoder::encode(AppModel::getInputData()->iduserfriend);
            if( $userFriend->userFriendship( $iduser, $idfriend ) ) {
                if ( $userFriend->unfriend( $iduser, $idfriend ) ) {
                    $this->set( 'response',  Utils::generateSuccess( SuccessCodes::OK ) );
                }
                else {
                    $this->set( 'response',  Utils::generateSuccess( SuccessCodes::RS_INPUT_ERROR ) );
                }
            }
            else {
                $this->set( 'response',  Utils::generateSuccess( SuccessCodes::RS_INPUT_ERROR ) );
            }
        }
    }
    
    /**
     * Un utente accetta una richiesta di amicizia e pertanto ...
     * 
     * @deprecated da UusersController::acceptfriend()
     */
    public function acceptfriend() {
        if( isset( AppModel::getInputData()->iduser ) && isset( AppModel::getInputData()->iduserfriend )) {
            
            $userFriend = new UserFriendsController();
            
            $iduser     = HTMLDecoder::encode(AppModel::getInputData()->iduser);
            $idfriend   = HTMLDecoder::encode(AppModel::getInputData()->iduserfriend);
            
            if( $userFriend->userFriendship( $iduser, $idfriend )  && $userFriend->userFriendshipStatus($iduser, $idfriend) == 2) {
                if ( $userFriend->accept( $iduser, $idfriend ) ) {
                    $this->set( 'response',  Utils::generateSuccess( SuccessCodes::OK ) );
                    $notificationsController = new NotificationsController();
                    $notificationsController->addNotification   (   $iduser, 
                                                                    $idfriend, 
                                                                    NotificationsController::NOTIFICATION_TYPE_FRIENDSHIP_ACCEPTED,
                                                                    NULL
                                                                );
                    $this->sendPush( $iduser, $idfriend, "",  NotificationsController::NOTIFICATION_TYPE_FRIENDSHIP_ACCEPTED);

                }
                else {
                    $this->set( 'response',  Utils::generateSuccess( SuccessCodes::RS_INPUT_ERROR ) );
                }
            }
            else {
                $this->set( 'response',  Utils::generateSuccess( SuccessCodes::RS_INPUT_ERROR ) );
            }
        }
    }

    
    
    
    private function createLoginUserObject( $userObject ) {
        $object = new stdClass();
        $object->user = new stdClass();
        //$object->user->idfacebook   = intval( $userObject[ "idfacebook" ]   );
        //$object->user->idgoogle     = intval( $userObject[ "idgoogle"   ]   );
        $object->user->email        = HTMLDecoder::decode( $userObject[ "email" ] );
        $object->user->iduser       = $userObject[ "iduser"     ];
        $object->user->img          = $userObject[ "img"        ];
        $object->user->birthdate    = $userObject[ "birthdate"  ];
        $object->user->sex          = intval( $userObject[ "sex"        ]   );
        $object->user->name         = HTMLDecoder::decode( $userObject[ "name"  ] );
        $object->user->surname      = HTMLDecoder::decode( $userObject[ "surname"  ] );
        $object->user->city         = HTMLDecoder::decode( $userObject[ "city"  ] );
        $object->user->interests    = HTMLDecoder::decode( $userObject[ "interests"  ] );
        $object->user->job          = HTMLDecoder::decode( $userObject[ "job"  ] );
        return $object;
    }
    
    /**
     * Creazione di un oggetto contenente le informazioni associate ad un utente usate nel profilo utente
     * @param array $userObject array associativo con la struttura della tabella user
     * @return \stdClass
     */
    private function createUserObject(&$userObject) {

        $object = new stdClass();
        $object->user = new stdClass();
        
        $object->user->iduser       = $userObject["iduser"];
        $object->user->img          = $userObject["img"];
        $object->user->birthdate    = $userObject["birthdate"];
        $object->user->sex          = intval($userObject["sex"]);
        $object->user->name         = HTMLDecoder::decode($userObject["name"]);
        $object->user->surname      = HTMLDecoder::decode($userObject["surname"]);
        $object->user->city         = HTMLDecoder::decode($userObject["city"]);
        $object->user->interests    = HTMLDecoder::decode($userObject["interests"]);
        $object->user->job          = HTMLDecoder::decode($userObject["job"]);
        
        $object->user->up_feedback = intval($userObject["up_feedback"]);
        $object->user->down_feedback = intval($userObject["down_feedback"]);
        $object->user->user_grade = intval($userObject["user_grade"]);
        
        return $object;
    }
    
    
    
    private function isUserConsistent($userObject) {
        //$object->user->email        = HTMLDecoder::decode( $userObject[ "email" ] );
        //$object->user->name         = HTMLDecoder::decode( $userObject[ "name"  ] );
        return true;
    }
    
    private function sendPush($idsender, $receiverlist, $message, $type) { 
        $pushcodeController     = new PushcodesController();
        return $pushcodeController->sendPush($type, $idsender, $receiverlist, $message, null, null, null);
    }
    
    
    /**
     * Seleziona gli amici che è possibile invitare ad un evento
     */
    public function invitablefriends (){
        
        $this->loadModel('AvMyFriends');
        $friends = $this->AvMyFriends->find('all', array('conditions' => array('iduser' => AppModel::getInputData()->iduser, 'accepted' => 1),
                                                         'order' => array('name ASC', 'surname ASC')));
        unset($this->AvMyFriends);
        
        // seleziona solo gli id di tutti gli utenti che partecipano all'evento
        $booked = array();
        $this->loadModel('Partecipate');
        $b = $this->Partecipate->find('all', array('conditions' => array('idevent' => AppModel::getInputData()->idevent), 'fields' => array('iduser')));
        
        foreach ($b as &$i){
            $booked[] = $i['Partecipate']['iduser'];
        }
        
        unset($this->Partecipate);
        
        // seleziona solo gli id di quelli che sono stati invitati dall'utente che effettua la richiesta
        $invited = array();
        $this->loadModel('EventInvitation');
        
        $in = $this->EventInvitation->find('all', array('conditions' => array('idevent' => AppModel::getInputData()->idevent, 
                                                                              'idsender' => AppModel::getInputData()->iduser), 
                                                        'fields' => array('idreciver')));
        
        foreach($in as &$i){
            $invited[] = $i['EventInvitation']['idreciver'];
        }
        
        unset($this->EventInvitation);
        
        // creazione della lista di risposta
        $response = array();
        
        foreach($friends as &$f){
            $response[] = array('iduser' => $f['AvMyFriends']['iduser'],
                                'idfriend' => $f['AvMyFriends']['idfriend'],
                                'accepted' => $f['AvMyFriends']['accepted'],
                                'name' => HTMLDecoder::decode($f['AvMyFriends']['name']),
                                'surname' => HTMLDecoder::decode($f['AvMyFriends']['surname']),
                                'img' => $f['AvMyFriends']['img'],
                                'booked' => in_array($f['AvMyFriends']['idfriend'], $booked),
                                'invited' => in_array($f['AvMyFriends']['idfriend'], $invited));
        }
        
        $this->set('response', array('Success' => 1,
                                     'users' => $response));
    }
    
    /**
     * Data una lista di pk su users restituisce la lista di url corrispondenti agli avatar
     */
    public function avatar(){
        $this->set('response', array('Success' => 1, 'Avatars' => $this->User->avatars()));
    }
}
