<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Event
 *
 * @author Christian
 */
class UserFriend extends AppModel{
    public function getSingleUserInfo( $iduser ) {
        if( isset($iduser)) {
            return $this->find( 'all', array( 'conditions' => array( 'iduser' => $iduser ) ) );
        }
        else {
            return null;
        }
    }
    
    public function deleteFriend( $iduser, $idfriend ) { 
        $this->query("DELETE FROM user_friends WHERE iduser = ".$iduser." AND idfriend = ".$idfriend);
        $this->query("DELETE FROM user_friends WHERE iduser = ".$idfriend." AND idfriend = ".$iduser);
        return true;
    }
    
    public function acceptFriend( $iduser, $idfriend ) { 
        $this->query("UPDATE user_friends SET accepted = 1 WHERE iduser = ".$iduser." AND idfriend = ".$idfriend);
        $this->query("UPDATE user_friends SET accepted = 1 WHERE iduser = ".$idfriend." AND idfriend = ".$iduser);
        return true;
    }
    
    /**
     * Eliminazione delle tuple relative ad una richiesta di amicizia. Avrei potuto utilizzare il metodo deleteFriend però fare due query per due tuple da eliminare dalla stessa tabella --> Christian!!!  Che cazzo !!!
     */
    public function unacceptFriend(){
        $receiver = HTMLDecoder::encode(self::$inputData->iduser);  // pk utente che ha ricevuto la richiesta di amicizia e che la sta rifiutando
        $sender = HTMLDecoder::encode(self::$inputData->iduserfriend); // pk utente che ha inviato la richiesta di amicizia e che è in attesa di accettazione
        $this->query("DELETE FROM user_friends WHERE (iduser = ".$receiver." AND idfriend = ".$sender." AND accepted = 2) OR (idfriend = ".$receiver." AND iduser = ".$sender."  AND accepted = 0)");
    }
    
    
    /**
     * Modifica la tabella friendship in modo cjhe una richiesta di amicizia venga accettata
     * @return boolean
     */
    public function acceptFriendship() { 
        
        $receiver = HTMLDecoder::encode(self::$inputData->iduser);  // pk utente che ha ricevuto la richiesta di amicizia e che la sta rifiutando
        $sender = HTMLDecoder::encode(self::$inputData->iduserfriend); // pk utente che ha inviato la richiesta di amicizia e che è in attesa di accettazione
        $this->query("UPDATE user_friends SET accepted = 1 WHERE (iduser = ".$receiver." AND idfriend = ".$sender." AND accepted = 2) OR (idfriend = ".$receiver." AND iduser = ".$sender."  AND accepted = 0)");
        
        return true;
    }
}
