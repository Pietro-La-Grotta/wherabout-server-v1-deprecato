<?php

/**
 * 
 *
 * @author Christian Ruggiero
 * @creation 17/11/2014
 * @status in develop
 */
App::import('Vendor', 'Events/IEventDateListedController');
class DateListedEventsController extends AppController implements IEventDateListedController {
    private $LIST_NAME = 'DateListedEvent';
    
    public function getList( $basedate, $iduser ){
        return $this->DateListedEvent->getList( $basedate );
    }
    
    public function getListName(  ){
        return $this->LIST_NAME;
    }
}