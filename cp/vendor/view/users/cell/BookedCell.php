<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of BookedCell
 * Numero di eventi a cui l'utente ha inviato una prenotazione
 * @author pietro@studioleaves.com
 */
class BookedCell implements ICell{

    private static $href_title = "Lista eventi con prenotazione inviata";
    private static $href_params = "index.php/utenti?show=partecipates&item=";
    
    private $tuple;
    private $booked_events_label_fields;
    private $iduser_label_fields;
   
    /**
     * Inizializza le variabili di instanza
     * @param string $created_events_label_fields nome del campo della tupla contenente il numero di eventi creati
     * @param string $iduser_label_fields nome del campo della tupla contenente pk utente
     */
    function __construct($booked_events_label_fields, $iduser_label_fields) {
        $this->booked_events_label_fields = $booked_events_label_fields;
        $this->iduser_label_fields = $iduser_label_fields;
    }

        
    /**
     * Stampa il contenuto html
     * @return string
     */
    public function html() {
        $html = "";
            
            $label = $this->tuple[$this->booked_events_label_fields];
            
            $html .= '<td align="center">';
                $html .= '<a href="'.Juri::base().self::$href_params.$this->tuple[$this->iduser_label_fields].'" title="'.self::$href_title.'" target="_blank">';
                $html .= $label;
                $html .= '</a>';
            $html .= '</td>';
            
        return $html;
    }

    /**
     * Setter per la tupla
     * @param array $tuple
     */
    public function data(&$tuple) {
        $this->tuple = $tuple;
    }    
}

?>
