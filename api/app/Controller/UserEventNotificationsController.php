<?php

/**
 * NotificationsController is a controller based in the notifications table
 * Hanlde the saving and retreiving notification list
 *
 * @author Christian Ruggiero
 * @creation 26/11/2014
 * @status in develop
 */

class UserEventNotificationsController extends AppController {  
    
    //retrieve all notification assigned to the user
    public function getAllNotifications( $idreceiver ) {
          if(  isset( $idreceiver) ) {
            return $this->UserEventNotification->getAllNotifications( $idreceiver );
          }
    }

}