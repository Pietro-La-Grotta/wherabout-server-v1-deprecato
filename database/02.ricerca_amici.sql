drop function if exists count_occurrences;
delimiter $$

/*
    Esegue il conteggio delle occorrenze di una sottostringa in una stringa
    La funzione è case sensitive

    input: 

       needles_ varchar(45) : sottostringa da cercare
       heistack_ varchar(100): stringa in cui effettuare la ricerca

    output:

        int : numero di volte che la sottostringa compare nella stringa

*/
create function count_occurrences(needles_ varchar(45), heystack_ varchar(100)) returns int(3) deterministic
begin
    return ROUND ( ( LENGTH(heystack_) - LENGTH( REPLACE ( heystack_, needles_, "") ) ) / LENGTH(needles_) );
end $$

delimiter ;

drop function if exists word_rank;
delimiter $$

/*
   Associa un peso ad una sottostringa in una data stringa
   La funzione è case free.

    input: 

       needle_ varchar(245) : sottostringa da cercare
       heistack_ varchar(245): stringa in cui effettuare la ricerca

    output:

       0 se needle_ non è contenuto in heistack_
       decimal d <= 1 se needle_ è contenuto in heistack_ con d crescente in maniera inversamente proporzionale alla posizione di inizio di needle_ in heistack_
*/

create function word_rank(needle_ varchar(100), heistack_ varchar(45)) returns decimal(40,20) deterministic
begin

    declare pos_ int(11);

    set pos_ = position( needle_ IN heistack_);
    
    if pos_ = 0 then return 0; end if;

    return ( 1 - ( (pos_ - 1) / LENGTH(heistack_) ) );

end $$

delimiter ;


drop function if exists user_rank; 

delimiter $$

/*
   Data una query string viene associato un peso in base alla presenza delle parole di cui la query string si compone nel nome e nel cognome
   La funzione è case free.

    input: 

       name_ varchar(45) : nome dell'utente;
       surname_ varchar(45): cognome dell'utente 
       needle_ varchar(100): query string

    output:

       peso associato ad un utente in quella ricerca
*/
create function user_rank (name_ varchar(45), surname_ varchar(45), needle_ varchar(100)) returns decimal(40,20) deterministic
begin 

    -- costruzione degli heistack
    declare name_surname, surname_name varchar(100);
    declare com_needle_ varchar(100);  -- lista delle parole chiave di comodo
    declare loc INT; -- posizione del delimitatore nella lista
    declare single_needle varchar(100); -- singola parola da cercare in heistack
    declare w_ decimal(40,20); -- moltiplicatore del peso in una iterata
    declare sum_weight decimal(40,20); -- somma dei pesi calcolata
   
    set name_surname = replace(concat(name_, surname_), ' ', '');
    set surname_name = replace(concat(surname_, name_), ' ', '');

    -- declare num_needle_ int(3); -- numero di parole di cui si compone la query string
 
    set needle_ = trim(needle_);
    -- select count_occurrences(" ", needle_) into num_needle_;

    set w_ = 1000;
    set sum_weight = 0;

    -- cerca in nome e cognome l'intera query
    set sum_weight = sum_weight + (word_rank(replace(needle_, ' ', ''), name_surname) * w_);
    set w_ = w_ / 10;  
    -- cerca in cognome e nome  l'intera query
    set sum_weight = sum_weight + (word_rank(replace(needle_, ' ', ''), surname_name) * w_);
    set w_ = w_ / 10;


    set com_needle_ = needle_;

    WHILE LENGTH(com_needle_) > 0 DO  -- c'è almeno una occorrenza di parola da cercare
    
        SET loc = LOCATE(" ", com_needle_);

        IF  loc > 0 THEN -- ci sono almeno due occorrenze

            SET single_needle = LEFT(com_needle_, loc - 1);
            SET com_needle_ = SUBSTRING(com_needle_, loc + 1);
		
	ELSE  -- c'è una sola occorrenza
		
            SET single_needle = com_needle_;
            SET com_needle_ = '';
			
	END IF;
    
        IF single_needle != ' ' THEN

           -- cerca in nome e cognome una sola parola della query string
            set sum_weight = sum_weight + (word_rank(single_needle, name_surname) * w_);
            set w_ = w_ / 10;  
            -- cerca in cognome e nome una sola parola della query string
            set sum_weight = sum_weight + (word_rank(single_needle, surname_name) * w_);
            set w_ = w_ / 10;

         END IF; 

    END WHILE;

    return sum_weight;

end $$

delimiter ;