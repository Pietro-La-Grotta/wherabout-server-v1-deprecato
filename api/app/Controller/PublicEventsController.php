<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
App::import('Vendor', 'Events/IEventListController');
class PublicEventsController extends AppController implements IEventListController{
    private $LIST_NAME = 'PublicEvent';
    
    public function getListName(  ){
        return $this->LIST_NAME;
    }
    
    public function getListPaged( $lastdate, $limitdate, $iduser ) {
        return $this->PublicEvent->getListPaged( $lastdate, $limitdate );
    }
    
    public function getListByMonth( $year, $month, $iduser ) {
        return  $this->PublicEvent->getListByMonth( $year, $month );
    }
    
    public function getDistanceList( $iduser, $latitude, $longitude, $maxdist ) {
        $eventMappedList = array();
        $eventList = $this->PublicEvent->getDistanceList( );
        foreach ($eventList as $singleEvent) {
            if( Utils::getDistanceFromCoords($latitude, $longitude, $singleEvent["PublicEvent"]["latitude"], $singleEvent["PublicEvent"]["longitude"]) <= $maxdist) {
                    $eventMappedList[] = $singleEvent;
            }
        }
        return   $eventMappedList;
    }
    
    //filtered
    public function getFilteredListPaged( $lastdate, $searchLimit, $filters ) {
        return  $this->PublicEvent->getFilteredListPaged( $lastdate, $searchLimit, $filters );
    }
    
    public function getFilteredListByMonth( $year, $month, $filters ) {
        return  $this->PublicEvent->getFilteredListByMonth( $year, $month, $filters );
    }
   
    public function getFilteredDistanceList( $iduser, $latitude, $longitude, $maxdist, $filters ) {
        $eventMappedList = array();
        $eventList = $this->PublicEvent->getFilteredDistanceList( $filters );
        foreach ($eventList as $singleEvent) {
            if( Utils::getDistanceFromCoords($latitude, $longitude, $singleEvent["PublicEvent"]["latitude"], $singleEvent["PublicEvent"]["longitude"]) <= $maxdist) {
                    $eventMappedList[] = $singleEvent;
            }
        }
        return $eventMappedList;
    }
}