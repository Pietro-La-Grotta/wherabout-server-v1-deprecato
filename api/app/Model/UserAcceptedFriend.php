<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Event
 *
 * @author Christian
 */
App::uses('AppModel', 'Model');
class UserAcceptedFriend extends AppModel{
    public function getFriendList( $iduser ) {
        if( isset($iduser)) {
            return $this->find( 'all', array( 'conditions' => array( 'iduser' => $iduser ),'order' => array('name ASC') ) );
        }
        else {
            return null;
        }
    }
    
    public function getFriendsCount( $iduser ) {
        if( isset($iduser)) {
            return $this->find( 'count', array( 'conditions' => array( 'iduser' => $iduser ) ) );
        }
        else {
            return 0;
        }
    }
}
