<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Event
 *
 * @author Christian
 */
App::uses('AppModel', 'Model');
class UserAcceptedFollow extends AppModel{
    public function getFollowList( $iduser ) {
        if( isset($iduser)) {
            return $this->find( 'all', array( 'conditions' => array( 'idfollow' => $iduser ),'order' => array('name ASC') ) );
        }
        else {
            return null;
        }
    }
}
