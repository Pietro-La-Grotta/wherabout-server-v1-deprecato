<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CreateParam
 * Wrrapper per i parametri da passare ad una richiesta di invio notifica
 * @author pietro@studioleaves.com
 */
class CreateParam implements IPushParameter{
//    PUSHWOOSH DEVELOPMENT SU ACCOUNT SERVECO   
//    /**
//     * API Access
//     * https://cp.pushwoosh.com/api_access
//     * @var string 
//     */
//    public $application = "2B0BB-1EF03";
//    
//    /**
//     * Application Code
//     * @var string
//     */
//    public $auth = "r75FitL5vgrLIB82PSTPcxvmme7TkFQYUXpb1ljgITtNAmJQ8EQhKIIha3zDGToXu73mXoUArsjMCgmN45c0";
//    
//    
    /**
     * API Access
     * https://cp.pushwoosh.com/api_access
     * @var string 
     */
    //public $application = "762E0-CA95D";  // prod
    public $application = "55487-BC8EA";  // dev
    /**
     * Application Code
     * @var string
     */
    public $auth = "31WXzKqKhjRTCzEOLOnZpEvYpyyW47Z79xGtzOcZh2y8s66z3lYWxquYQNuSL4T8fpmokGqOYp63EzeRF6t0";
    
    /**
     * Momento di invio del messaggio
     * {now o YYYY-MM-DD HH:mm} 
     * @var string 
     */
    public $timeToSend;
    
    /**
     * Testo da visualizzare nell'area delle notifiche per gli utenti che hanno selezionato la lingua iglese
     * @var string
     */
   // public $enShortContent;
    
    /**
     * Testo da visualizzare nell'area delle notifiche per gli utenti che hanno selezionato la lingua italiana
     * @var string
     */
    public $shortContent;
    
    /**
     * Dati personalizzati per l'app
     * @var CustomData 
     */
    public $customData;
    
    /**
     * Lista dei push code a cui inviare le notifiche
     * Parametro opzionale, se non viene settato , la notifica viene inviata a tutti
     * Il numero massimo di dispositivi inseribili deve essere < 1000
     */
    public $receivers = array();
}

?>
