<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TextCell
 * Indirizzo email del partecipante
 * @author pietro@studioleaves.com
 */
class TextCell implements ICell{ 

    private $tuple;
    private $label_field;
   
    /**
     * Inizializza le variabili di istanza
     * @param string $label_field nome dell'attributo della tupla contenente il testo da inserire nella tabella
     */
    public function __construct($label_field) {
        $this->label_field = $label_field;
    }
  
    /**
     * Stampa il contenuto html
     * @return string
     */
    public function html() {
        $html = "";
            
            $text = ucwords($this->tuple[$this->label_field]);
            
            $html .= '<td>';
                //$html .= '<a href="'.Juri::base().'index.php'.self::$menu.'&item='.$this->tuple[$this->id_field].'" title="'.self::$title.'" target="_blank">';
                $html .= $text;
                //$html .= '</a>';
            $html .= '</td>';
            
        return $html;
    }

    /**
     * Setter per la tupla
     * @param array $tuple
     */
    public function data(&$tuple) {
        $this->tuple = $tuple;
    }    
}

?>
