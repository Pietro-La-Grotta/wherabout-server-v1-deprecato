<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description DateCell
 * Data in formalismo italiano
 * @author pietro@studioleaves.com
 */
class DateCell implements ICell{

    private $tuple;
    private $label_field;
   
    /**
     * Inizializza le variabili di istanza
     * @param string $label_field nome dell'attributo della tupla contenente la data (in formato date) 
     */
    public function __construct($label_field) {
        $this->label_field = $label_field;
    }
  
    /**
     * Stampa il contenuto html
     * @return string
     */
    public function html() {
        $html = "";
        
           $label = substr($this->tuple[$this->label_field], 0, 10); 
           //$label = HTMLDecoder::italianDateTime($this->tuple[$this->label_field], true);

            $html .= '<td align="center">';
                $html .= $label;
            $html .= '</td>';
            
        return $html;
    }

    /**
     * Setter per la tupla
     * @param array $tuple
     */
    public function data(&$tuple) {
        $this->tuple = $tuple;
    }    
}

?>
