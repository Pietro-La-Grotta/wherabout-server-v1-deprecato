<?php

/*
 SQL:
 * 
 -- vista per il conteggio degli eventi creati dagli utenti
drop view if exists cp_count_created_events;

create view cp_count_created_events as
SELECT `iduser`, count(`idevent`) created_events FROM `events` group by `iduser`;

-- vista per il conteggio degli eventi a cui l'utente si è iscritto
drop view if exists cp_count_booked_events; 

create view cp_count_booked_events as 
SELECT `iduser`, count(`id`) partecipates_events FROM `partecipates` group by `iduser`;

-- vista che associa agli utenti il numero di eventi che hanno creato ed il numero di eventi a cui hanno partecipato
drop view if exists cp_users_lists;

create view cp_users_lists as 
select `u`.*, 
       if (`c`.`created_events` is not null,`c`.`created_events`, 0) created_events,
       if (`p`.`partecipates_events` is not null,`p`.`partecipates_events`, 0) partecipates_events
from `users` `u`
left join `cp_count_created_events` `c` on `u`.`iduser` = `c`.`iduser`
left join `cp_count_booked_events` `p` on `u`.`iduser` = `p`.`iduser`;

 * 
 */

/**
 * Description of EventsModel
 * Accede ai dati relativi agli eventi
 * @author pietro@studioleaves.com
 */
class UsersModel {
    
    
    /**
     *
     * @var type filtri di ricerca
     */
    private $filters = array();
    
    /**
     * Accede alle variabili di sessione e carica i filtri di ricerca precedentemente preimpostati
     */
    public function loadFilter(){
        
        //ritrova tra le variabili di sessione i parametri di ricerca
        if (!isset($_SESSION['gcore'])) return;
        if (!isset($_SESSION['gcore']['_chronoform_data_user_search_params'])) return;
        
        $sp = $_SESSION['gcore']['_chronoform_data_user_search_params'];
        
        // filtri da oggetto in sessione a attributo $this->filters
        
        $db = JFactory::getDbo();

        // stato degli utenti  
        if (!empty($sp['status']) or ($sp['status'] == '0')){
            $this->filters['activated = '] = $db->q($sp['status']);
        }
        
        // email 
        if (!empty($sp['email'])){
            $this->filters['email LIKE '] = $db->q('%'.$sp['email'].'%');
        }
        
         // nome
        if (!empty($sp['name'])){
            $this->filters['name LIKE '] = $db->q('%'.$sp['name'].'%');
        }
        
         // cognome
        if (!empty($sp['surname'])){
            $this->filters['surname LIKE '] = $db->q('%'.$sp['surname'].'%');
        }
        
         // limite inferiore data di nascita
        if (!empty($sp['birthdatefrom'])){
            $this->filters['birthdate >= '] = $db->q(HTMLDecoder::mysqDate($sp['birthdatefrom'])); 
        }
        
         // limite superiore data di nascita
        if (!empty($sp['birthdateto'])){
            $this->filters['birthdate <= '] = $db->q(HTMLDecoder::mysqDate($sp['birthdateto'])); 
        }
        
         // sesso
        if (!empty($sp['sex']) or $sp['sex'] == '0'){
            $this->filters['sex = '] = $db->q($sp['sex']);
        }
        
         // job
        if (!empty($sp['job'])){
            $this->filters['job = '] = $db->q($sp['job']);
        }
        
         // città
        if (!empty($sp['city'])){
            $this->filters['city LIKE '] = $db->q('%'.$sp['city'].'%');
        }
        
         // interessi
        if (!empty($sp['interests'])){
            $tokens = explode(',', $sp['interests']);
                $or = '(';
                $first = true;
                
                foreach ($tokens as $t){
                    
                    if (!$first) $or .= ' OR';
                    $first = false;
                    
                    $or .= ' interests LIKE '.$db->q('%'.HTMLDecoder::encode(trim($t)).'%');
                }
                
                $or .= ')';
                
                $this->filters[$or.' '] = '';
        }
        
         // limite inferiore data di registrazione
        if (!empty($sp['registrationtimefrom'])){
            $this->filters['creationdate >= '] = $db->q(HTMLDecoder::mysqDate($sp['registrationtimefrom'])); 
        }
        
         // limite superiore data di registrazione
        if (!empty($sp['registrationdateto'])){
            $this->filters['creationdate <= '] = $db->q(HTMLDecoder::mysqDate($sp['registrationdateto'])); 
        }
        
         // limite inferiore eventi creati
        if (!empty($sp['createdinf'])){ 
            $this->filters['created_events >= '] = $db->q($sp['createdinf']); 
        }
        
         // limite superiore eventi creati
        if (!empty($sp['createdsup'])){
            $this->filters['created_events <= '] = $db->q($sp['createdsup']); 
        }
        
         // limite inferiore eventi prenotati
        if (!empty($sp['bookedinf'])){ 
            $this->filters['partecipates_events >= '] = $db->q($sp['bookedinf']);
        }
        
         // limite superiore eventi prenotati
        if (!empty($sp['bookedsup'])){ 
            $this->filters['partecipates_events <= '] = $db->q($sp['bookedsup']);
        }
    }
    
    /**
     * Seleziona l'intera lista degli eventi
     * @return object list
     */
    public function select(){
        $db = JFactory::getDBO();
        
        $query = "select * from cp_users_lists";
        
        if (count($this->filters) > 0){
            $first = true;
            UsersController::$searchparam = true;
            
            foreach ($this->filters as $key => $value){
                
                if ($first){
                    $query .= ' WHERE';
                    $first = false;
                }
                else{
                    $query .= ' AND';
                }
                
                $query .= ' '.$key.' '.$value;
            }
        }
        //die($query);
        $db->setQuery($query); 
        return $db->loadObjectList();
    }
    
    /**
     * Seleziona un utente da cp_users_lists data la pk 
     * @param int $iduser pk evento
     * @return object
     */
    public function getById($iduser){
        $dbo = JFactory::getDbo();
        $query = $dbo->getQuery(true);
        $query->select('*')
              ->from($dbo->qn('cp_users_lists'))
              ->where($dbo->qn('iduser').' = '.$dbo->q(HTMLDecoder::encode($iduser)));
        
       $dbo->setQuery($query);
       return $dbo->loadObject();
    }
    
   
    /**
     * Effettua il salvataggio dei dati associati ad un utente
     * Sarebbe stato più giusto fare un controller :)
     */
    public function save(){
       
        $db = JFactory::getDBO();
        
        $fields = array();
        
        $fields[] = $db->qn('activated') .' = '.$db->q(HTMLDecoder::encode($_POST['status']));
        $fields[] = $db->qn('name') .' = '.$db->q(HTMLDecoder::encode(ucwords($_POST['name'])));
        $fields[] = $db->qn('surname') .' = '.$db->q(HTMLDecoder::encode(ucwords($_POST['surname'])));
        $fields[] = $db->qn('email') .' = '.$db->q(HTMLDecoder::encode($_POST['email']));
        $fields[] = $db->qn('birthdate') .' = '.$db->q(HTMLDecoder::mysqDate($_POST['birthdate']));
        $fields[] = $db->qn('sex') .' = '.$db->q(HTMLDecoder::encode($_POST['sex']));
        
        if (!empty($_POST['city'])){ 
            $fields[] = $db->qn('city') .' = '.$db->q(HTMLDecoder::encode(ucwords($_POST['city'])));
        }
        else{
            $fields[] = $db->qn('city') .' = '.$db->q(-1);
        }
        
        if (!empty($_POST['interests'])){
            $fields[] = $db->qn('interests') .' = '.$db->q(HTMLDecoder::encode(ucwords($_POST['interests'])));
        }
        else{
            $fields[] = $db->qn('interests') .' = '.$db->q(-1);
        }
        
        if (!empty($_POST['job'])){
            $fields[] = $db->qn('job') .' = '.$db->q(HTMLDecoder::encode($_POST['job']));
        }
        else {
            $fields[] = $db->qn('job') .' = NULL';
        }
        
        $query = $db->getQuery(true);
        $query->update($db->qn('users'))
              ->set($fields)
              ->where($db->qn('iduser').' = '.$db->q(JRequest::getVar('item')));
        
        $db->setQuery($query);
        $db->query();        
    }
    
   
    
    /**
     * Rimozione del ban ad una lista di utenti
     * @param string $users lista delle chiavi primarie degli utenti a cui rimuovere il ban
     *        es : "('0', '1', '47')"
     */
    public function activeUsers(&$users){
        $db = JFactory::getDbo();
        $qo = $db->getQuery(true);
        
        $qo->update($db->qn('users'))
           ->set(array($db->qn('activated').' = '.$db->q(1)))
           ->where(array($db->qn('iduser').' IN '.$users));
        
        $db->setQuery($qo);
        $db->query();
    }
    
    /**
     * Banna una lista di utenti
     * @param string $users lista delle chiavi primarie degli utenti da bannare
     *        es : "('0', '1', '47')"
     */
    public function disableUsers(&$users){
        $db = JFactory::getDbo();
        $qo = $db->getQuery(true);
        
        $qo->update($db->qn('users'))
           ->set(array($db->qn('activated').' = '.$db->q(0)))
           ->where(array($db->qn('iduser').' IN '.$users));
        
        $db->setQuery($qo);
        $db->query();
    }
    
    
    /**
     * Modifica l'avatar dell'utente sul database
     * @param int $iduser
     * @param string path completo nuova immagine
     */
    public function updateImage($iduser, $imagPath){
        $db = JFactory::getDbo();
        $qo = $db->getQuery(true);
        
        $qo->update($db->qn('users'))
           ->set(array($db->qn('img').' = '.$db->q($imagPath)))
           ->where(array($db->qn('iduser').' = '.$iduser));
        
        $db->setQuery($qo);
        $db->query();
    }
    
    /**
     * Seleziona la lista degli eventi creati da un utente 
     * @param int $iduser planner pk
     * @return objects list
     */
    public function getEventsByPlanner($iduser){
        $db = JFactory::getDbo();
        $qo = $db->getQuery(true);
        
        $qo->select('*')
           ->from($db->qn('cp_events_list'))
           ->where($db->qn('iduser').' = '.$db->q(HTMLDecoder::encode($iduser)));
        
        $db->setQuery($qo);
        
        return $db->loadObjectList();
    }
    
    /*
     * -- Vista per la selezione degli eventi a cui un utente ha inviato una prenotazione

        DROP VIEW IF EXISTS cp_booked_by_users;

        CREATE VIEW cp_booked_by_users as
        SELECT `p`.`id` id_booking, `p`.`iduser` id_partecipant, `p`.`confirmed`,
               `e`.idevent, e.title, e.`date`, e.city, 
               IF (c1.idsupercategory IS NOT NULL, CONCAT(c2.name, ' > ', c1.name), c1.name) category_name 
        FROM `partecipates` `p` JOIN `events` `e` ON `p`.`idevent` =  `e`.`idevent`
                                JOIN categories c1 ON e.idcategory = c1.id
                                LEFT JOIN categories c2 ON c1.idsupercategory = c2.id;						

     */
    
    /**
     * Seleziona tutte le prenotazioni di un utente
     * @param int $iduser user pk
     * @return objects list
     */
    public function getUserBookings($iduser){
        $db = JFactory::getDbo();
        $qo = $db->getQuery(true);
        
        $qo->select('*')
           ->from($db->qn('cp_booked_by_users'))
           ->where($db->qn('id_partecipant').' = '.$db->q(HTMLDecoder::encode($iduser)));
        
        $db->setQuery($qo);
        
        return $db->loadObjectList();
    }
    
    /**
     * Conferma le prenotazioni
     * @param string $users lista  di pk inviata in POST
     */
    public function confirmBooking($booking){
        $db = JFactory::getDbo();
        $qo = $db->getQuery(true);
        
        $qo->update($db->qn('partecipates'))
           ->set(array($db->qn('confirmed').' = '.$db->q(1)))
           ->where(array($db->qn('id').' IN '.str_replace(array('[', ']', '"'), array('(', ')', "'"), $booking)));
        
        $db->setQuery($qo);
        $db->query();
    }
    
    /**
     * Rimozione delle conferme alle prenotazioni
     * @param string $users lista di pk inviata in POST
     */
    public function unconfirmBooking($booking){
        $db = JFactory::getDbo();
        $qo = $db->getQuery(true);
        
        $qo->update($db->qn('partecipates'))
           ->set(array($db->qn('confirmed').' = '.$db->q(0)))
           ->where(array($db->qn('id').' IN '.str_replace(array('[', ']', '"'), array('(', ')', "'"), $booking)));
        
        $db->setQuery($qo);
        $db->query();
    }
    
     /**
     * Rimozione delle conferme alle prenotazioni
     * @param string $users lista di pk inviata in POST
     */
    public function deleteBooking($booking){
        $db = JFactory::getDbo();
        $qo = $db->getQuery(true);
        
        $qo->delete($db->qn('partecipates'))
           ->where(array($db->qn('id').' IN '.str_replace(array('[', ']', '"'), array('(', ')', "'"), $booking)));
        
        $db->setQuery($qo);
        $db->query();
    }
}

?>
