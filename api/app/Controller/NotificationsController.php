<?php

/**
 * NotificationsController is a controller based in the notifications table
 * Hanlde the saving and retreiving notification list
 *
 * @author Christian Ruggiero
 * @creation 26/11/2014
 * @status in develop
 */
App::import('Controller',   'UserEventNotifications');
class NotificationsController extends AppController {  
    const NOTIFICATION_MAX_NUMBER = 50;
    
    const NOTIFICATION_TYPE_FOLLOWERS_EVENT_CREATED         = 0;
    const NOTIFICATION_TYPE_PARTECIPANTS_EVENT_UPDATE       = 1;
    const NOTIFICATION_TYPE_EVENT_INVITATION                = 2; 
    const NOTIFICATION_TYPE_FRIENDSHIP_REQUEST              = 3;
    const NOTIFICATION_TYPE_FRIENDSHIP_ACCEPTED             = 4;
    const NOTIFICATION_TYPE_BOOKING_REQUEST                 = 5;
    const NOTIFICATION_TYPE_BOOKING_ACCEPT                  = 6;
    const NOTIFICATION_TYPE_CHATMESSAGE_RECEIVED            = 7;
    
    
    const NOTIFICATION_MESSAGE_FOLLOWERS_EVENT_CREATED     = "NOTIFICATION_MESSAGE_FOLLOWERS_EVENT_CREATED";
    const NOTIFICATION_MESSAGE_PARTECIPANTS_EVENT_UPDATE   = "NOTIFICATION_MESSAGE_PARTECIPANTS_EVENT_UPDATE";
    const NOTIFICATION_MESSAGE_EVENT_INVITATION            = "NOTIFICATION_MESSAGE_EVENT_INVITATION"; 
    const NOTIFICATION_MESSAGE_FRIENDSHIP_REQUEST          = "NOTIFICATION_MESSAGE_FRIENDSHIP_REQUEST";
    const NOTIFICATION_MESSAGE_FRIENDSHIP_ACCEPTED         = "NOTIFICATION_MESSAGE_FRIENDSHIP_ACCEPTED";
    const NOTIFICATION_MESSAGE_BOOKING_REQUEST             = "NOTIFICATION_MESSAGE_BOOKING_REQUEST";
    const NOTIFICATION_MESSAGE_BOOKING_ACCEPT              = "NOTIFICATION_MESSAGE_BOOKING_ACCEPT";
    const NOTIFICATION_MESSAGE_CHATMESSAGE_RECEIVED        = "NOTIFICATION_MESSAGE_CHATMESSAGE_RECEIVED";
    
    //retrieve all notification assigned to the user
    public function getall() {
          if(  isset( AppModel::getInputData()->iduser) ) {
            $iduser = AppModel::getInputData()->iduser;
            $userEventNotificationsController = new UserEventNotificationsController();
            $notificationObj = Utils::generateSuccess( SuccessCodes::OK );
            $list = $userEventNotificationsController->getAllNotifications( $iduser );
            $notificationObj->notificationlist = array();
            foreach ($list as $entry) {
                $notificationSingle = $entry["UserEventNotification"];
                $notificationSingle["title"]      = HTMLDecoder::decode($notificationSingle["title"] );
                $notificationSingle["name"]       = HTMLDecoder::decode($notificationSingle["name"] );
                $notificationSingle["surname"]    = HTMLDecoder::decode($notificationSingle["surname"]  );
                $notificationObj->notificationlist[] = $notificationSingle;
            }
            $this->set('response',  $notificationObj );
          }
    }
    
    public function getAllNotifications( $idreceiver ) {
        return $this->Notification->getAllNotifications( $idreceiver );
    }  
    
    
    /**
     * Aggiunge una notifica alla lista delle notifiche presenti sul database
     * @param int $idsender pk user che invia la notifica
     * @param int $idreceiver pk di chi deve ricevere la notifica
     * @param $type
     * @param type $idevent
     * @return boolean
     */
    public function addNotification( $idsender, $idreceiver, $type, $idevent ) {
        $notificationObj = new stdClass();
        $notificationObj->idsender      = $idsender;
        $notificationObj->idreceiver    = $idreceiver;
        $notificationObj->type          = $type;
        $notificationObj->idevent       = $idevent;
        $this->Notification->save($notificationObj);
        
        $this->fixNotificationSize($idreceiver);
        return true;
    }
    
    
    
    
    private function fixNotificationSize( $idreceiver ) {
        //remove the oldest notification if are > 50
        if($this->Notification->getNotificationNumber( $idreceiver ) > NotificationsController::NOTIFICATION_MAX_NUMBER) {
            $notificationList = $this->Notification->getOldestNotification( $idreceiver );
            foreach ( $notificationList as $notificationSingle ) {
                $this->Notification->delete( $notificationSingle["id"] );
            }
        }
        $this->Notification->clear();
    }
    
    public function removenotification() {
        if(  isset( AppModel::getInputData()->idnotification) ) {
            $id = AppModel::getInputData()->idnotification;
            $this->Notification->delete( $id );
            $this->set('response', Utils::generateSuccess( SuccessCodes::OK ));
        }
        else {
            $this->set('response', Utils::generateSuccess( SuccessCodes::RS_INPUT_ERROR ));
        }
    }
    
    public function removeEventNotification( $idevent ) {
        $this->Notification->deleteAll(array('idevent' => $idevent), false);
    }
}