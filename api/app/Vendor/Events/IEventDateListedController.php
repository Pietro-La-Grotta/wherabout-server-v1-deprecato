<?php

interface IEventDateListedController
{
    public function getList( $basedate, $iduser );
    public function getListName( );
}