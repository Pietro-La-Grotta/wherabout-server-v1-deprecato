<?php

/**
 * ActivatedEventsController is a controller based in the activated_event view already created in the DB
 *
 * @author Christian Ruggiero
 * @creation 30/10/2014
 * @status in develop
 */

App::import('Vendor',       'Events/EventList');
App::import('Controller',   'Partecipates');
App::import('Vendor',       'Events/IEventListController');
class ActivatedEventsController extends AppController implements IEventListController {  
    
    //IEventListController
    private $LIST_NAME = 'ActivatedEvent';
    public function getListByMonth( $year, $month, $iduser ){
        return $this->ActivatedEvent->getListByMonth( $year, $month  );
    }
    
    public function getListPaged( $lastdate, $limitdate, $iduser ) {
        return $this->ActivatedEvent->getListPaged( $lastdate, $limitdate );
    }
    
    public function getUserCreatedEventListPaged( $lastdate, $limitdate, $iduser ) {
        return $this->ActivatedEvent->getUserCreatedEventListPaged( $lastdate, $limitdate, $iduser );
    }
    
    public function getListName(  ){
        return $this->LIST_NAME;
    }

}