<?php


/**
 * PushController is the main controller for push based request and it's used by :
 *  - UserController
 *
 * @author Christian Ruggiero
 *@creation 4/11/2014
 * @status in develop
 */
class UserFollowsController extends AppController{
    public function getSingleUserInfo( $iduser ) {
        return $this->UserFollow->getSingleUserInfo( $iduser );
    }
    
    public function follow( $iduser, $iduserfollow ) {
        if( isset( $iduser ) && isset( $iduserfollow ) ) {
            $followObj = new stdClass();
            $followObj->iduser = $iduser;
            $followObj->idfollow = $iduserfollow;
            return $this->UserFollow->save($followObj);
        }
        else {
            return null;
        }
    }
    
    public function unfollow( $iduser, $iduserfollow ) {
        if( isset( $iduser ) && isset( $iduserfollow ) ) {
            return $this->UserFollow->deleteFollow( $iduser, $iduserfollow );
        }
        else {
            return false;
        }
    }
    
    public function userFollowing( $iduser, $iduserfollow ) { 
        if ( isset($iduser) && isset($iduserfollow) && ( intval( $this->UserFollow->find( 'count', array( 'conditions' => array( 'iduser' => $iduser, 'idfollow' => $iduserfollow) ) ) > 0 ) ) ) {
            return TRUE;
        } 
        else {
            return FALSE;
        }
    }
    
    public function userFollows( $iduser ) { 
        if ( isset($iduser) ) {
            return intval( $this->UserFollow->find( 'count', array( 'conditions' => array( 'idfollow' => $iduser ) ) ) ); 
        } 
        else {
            return 0;
        }
    }
    
    public function getFollowers( $iduserfollow ) {
        return $this->UserFollow->getUserFollowers( $iduserfollow );
    }
    
    
}
