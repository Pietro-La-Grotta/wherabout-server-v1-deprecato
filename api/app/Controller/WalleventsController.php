<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * API per le selezioni delle liste eventualmente filtrate in base a parametri di ricerca per la bacheca
 */
class WalleventsController extends AppController{
    
    /*
     * Tipologie di ricerca 
     */
    const PAGED = 0;
    const MONTHLY = 1;
    const DISTANCE = 2;
    
    /**
     * Formattazione delle date contenute nei parametri
     * @param int $conditions
     */
    private function dateformat(&$input){
        
        if (isset($input->lastdate) && !empty($input->lastdate)){
            $date = new DateTime($input->lastdate);
            $input->lastdate = $date->format('Y-m-d');
        }
        
        if (isset($input->datefrom) && !empty($input->datefrom)){
            $date = new DateTime($input->datefrom);
            $input->datefrom = $date->format('Y-m-d');
        }
        
        if (isset($input->dateto) && !empty($input->dateto)){
            $date = new DateTime($input->dateto);
            $input->dateto = $date->format('Y-m-d');
        }
    }

    /**
     * Seleziona il set composto da pk utente che inoltra la richiesta + pk degli amici dell'utente cheinoltra la richiesta
     * formattato per essere inserito nella query tramite costrutto IN
     * @param int $iduser pk utente che inoltra la richiesta
     * @return string eg: (12, 21, 56)
     */
    private function meAndMyFriends($iduser){
        
        $this->loadModel('UserFriend');
        $friends = $this->UserFriend->find('all', array('conditions' => array('iduser' => $iduser, 'accepted' => 1), 'fields' => array('idfriend')));
        
        $in = array($iduser);
        foreach($friends as $f) $in[] = $f['UserFriend']['idfriend'];
        
        unset($this->UserFriend);
        return $in;
    }
    
    /**
     * Seleziona una partizione di eventi aggiungendo dei filtri per la lista discover
     * Include eventi pubblici, eventi friendonly creati dagli amici e dall'utente non privati
     */
    public function filteredpagedlist(){
        $input = AppModel::getInputData();
        $this->dateformat($input);
        
        $conditions = array();
        // condizioni per la selezione degli eventi
        $conditions['date > '] = $input->lastdate; // data successiva all'ultima visualizzata
//        // Tutti gli eventi pubblici oppure gli eventi friends-only creati dall'utente che inoltra la richiesta 
//        $conditions['OR'] = array('scope' => 0, 'AND' => array('scope' => 2, 'iduser' => $input->iduser));

        // Tutti gli eventi pubblici oppure gli eventi friends-only creati dall'utente che inoltra la richiesta o da uno degli amici dell'utente che inoltra la richiesta
        $myfriends = $this->meAndMyFriends($input->iduser);
        $conditions['OR'] = array('scope' => 0, 'AND' => array('scope' => 2, 'iduser' => $myfriends));
        
        // vengono aggiunti alla query i parametri di ricerca
        $this->seachParameters($conditions, self::PAGED);
        
        $this->loadModel('ActivatedEvent');
        
        $raw = $this->ActivatedEvent->find('all', array('conditions' => $conditions, 
                                        'fields' => array('date', 'count(idevent) as occourrences'),
                                        'group' => array('date'),
                                        'order' => array('date ASC'))); 
        
        // calcolo limiti intervallo temporale
        $maxdate = $input->lastdate;
        $toload = 0;
        
        foreach ($raw as $r){
            if ($toload < $input->quantity){
                $toload += $r[0]['occourrences'];
                $maxdate = $r['ActivatedEvent']['date'];
            }
            else break; 
        }
        
        
        unset($raw);
        
         // inserimento del limite superiore per la selezione degli eventi nell'array delle condizioni
        if( !empty($input->dateto) ) {
            $conditions['date <= '] = min(array($maxdate, $input->dateto));
        }
        else{
           $conditions['date <= '] = $maxdate; 
        }
        
        // selezione degli eventi
        $rawevents = $this->ActivatedEvent->find('all', array('conditions' => $conditions, 'order' => array('date ASC', 'event_grade DESC', 'hour ASC')));  // eventi ordinati per data, ma nello stesso giorno vengono mostrati prima gli eventi business
        unset($this->ActivatedEvent);
        
        // selezione delle partecipazioni dell'utente che ha inoltrato la richiesta agli eventi
        $this->loadModel('Partecipate');
        
        $raw = $this->Partecipate->find('all', array('conditions' => array('iduser' => $input->iduser), 'fields' => array('idevent', 'confirmed')));
        unset($this->Partecipate);
        
        $booking = array();
        
        foreach($raw as $r){
            $booking[$r['Partecipate']['idevent']] = $r['Partecipate']['confirmed']; 
        }
        
        unset($raw);
        
        // lista degli eventi
        $eventsdaylist = array();
        $curday = "";
        
        // filling degli eventi
        foreach($rawevents as &$ev){
            $e = $ev['ActivatedEvent'];
            
            // se necessario istanziamo un nuovo giorno
            if ($e['date'] != $curday){

                $curday = $e['date']; 
                $eventsdaylist[] = array('date' => $curday, 'eventlistinfo' => array());
            }

            // decodifica delle stringhe
            $e['title'] = HTMLDecoder::decode($e["title"]);
            $e['city'] = HTMLDecoder::decode($e["city"]);
            $e['address'] = HTMLDecoder::decode($e["address"]);

            if (isset($e['img'])){
                $e['imgcover'] = $e['img'];
                unset($e['img']);
            }

            // partecipazione dell'utente che ha inoltrato la richiesta all'evento
            $e['partecipate'] = (isset($booking[$e['idevent']])) ? $booking[$e['idevent']] : 999;

            // inserimento dell'evento nella lista
            $eventsdaylist[count($eventsdaylist) - 1]['eventlistinfo'][] = $e; 
        }
        
        $this->set('response', array('Success' => SuccessCodes::OK, 'eventsdayslist' => $eventsdaylist));
    }
    
    /**
     * Lista degli eventi da visualizzare sul calendario
     */
    public function filteredmonthly(){
        $input = AppModel::getInputData();
        $this->dateformat($input);
       
        
        $conditions = array();
        
        // Tutti gli eventi pubblici oppure gli eventi friends-only creati dall'utente che inoltra la richiesta o da uno degli amici dell'utente che inoltra la richiesta
        $myfriends = $this->meAndMyFriends($input->iduser);
        $conditions['OR'] = array('scope' => 0, 'AND' => array('scope' => 2, 'iduser' => $myfriends));
        
        // calcolo limiti intervallo temporale
        $defDateFrom = $input->year.'-'.$input->month.'-01';
        $conditions['AND'] = array('date >=' => $defDateFrom, "`date` <= date_add('".$defDateFrom."', interval 1 month)");
         
        // vengono aggiunti alla query i parametri di ricerca
        $this->seachParameters($conditions, self::MONTHLY);
        
        // selezione degli eventi
        $this->loadModel('ActivatedEvent');
        $rawevents = $this->ActivatedEvent->find('all', array('conditions' => $conditions, 
                                                 'fields' => array('date', 'count(idevent) as occourrences'),
                                                 'group' => array('date'),
                                                 'order' => array('date ASC'))); 
        unset($this->ActivatedEvent);
        
        // lista degli eventi
        $eventsdaylist = array();
        
        // filling degli eventi
        foreach($rawevents as &$ev){
            if (($oc = intval($ev[0]['occourrences'])) > 0){
                $eventsdaylist[] = array('date' => $ev['ActivatedEvent']['date'], 'count' => $oc);
            }
        }
        
        $this->set('response', array('Success' => SuccessCodes::OK, 'eventsinadaylist' => $eventsdaylist));
    }
    
    
    /**
     * Seleziona gli eventi entro una certa distanza in metri
     */
    public function filtereddistancelist(){
        $input = AppModel::getInputData();
        $this->dateformat($input);
        
        $conditions = array();
//        // Tutti gli eventi pubblici oppure gli eventi friends-only creati dall'utente che inoltra la richiesta 
//        $conditions['OR'] = array('scope' => 0, 'AND' => array('scope' => 2, 'iduser' => $input->iduser));
        
        // Tutti gli eventi pubblici oppure gli eventi friends-only creati dall'utente che inoltra la richiesta o da uno degli amici dell'utente che inoltra la richiesta
        $myfriends = $this->meAndMyFriends($input->iduser);
        $conditions['OR'] = array('scope' => 0, 'AND' => array('scope' => 2, 'iduser' => $myfriends));
        
        // vincolo sulla distanza
        $conditions['distanza(latitude, longitude, '.$input->latitude.', '.$input->longitude.') <= '] = $input->maxdist; 
        
        // vengono aggiunti alla query i filtri di ricerca
        $this->seachParameters($conditions, self::DISTANCE);
        
         // selezione degli eventi
        $this->loadModel('ActivatedEvent');
        $rawevents = $this->ActivatedEvent->find('all', array('conditions' => $conditions, 'order' => array('date ASC')));
        unset($this->ActivatedEvent);
        
        // selezione delle partecipazioni dell'utente che ha inoltrato la richiesta agli eventi
        $this->loadModel('Partecipate');
        
        $raw = $this->Partecipate->find('all', array('conditions' => array('iduser' => $input->iduser), 'fields' => array('idevent', 'confirmed')));
        unset($this->Partecipate);
        
        $booking = array();
        
        foreach($raw as $r){
            $booking[$r['Partecipate']['idevent']] = $r['Partecipate']['confirmed']; 
        }
        
        unset($raw);
        
        // lista degli eventi
        $eventsdaylist = array();
        $curday = "";
        
        // filling degli eventi
        foreach($rawevents as &$ev){
            $e = $ev['ActivatedEvent'];
            
            // se necessario istanziamo un nuovo giorno
            if ($e['date'] != $curday){

                $curday = $e['date']; 
                $eventsdaylist[] = array('date' => $curday, 'eventlistinfo' => array());
            }

            // decodifica delle stringhe
            $e['title'] = HTMLDecoder::decode($e["title"]);
            $e['city'] = HTMLDecoder::decode($e["city"]);
            $e['address'] = HTMLDecoder::decode($e["address"]);

            if (isset($e['img'])){
                $e['imgcover'] = $e['img'];
                unset($e['img']);
            }

            // partecipazione dell'utente che ha inoltrato la richiesta all'evento
            $e['partecipate'] = (isset($booking[$e['idevent']])) ? $booking[$e['idevent']] : 999;

            // inserimento dell'evento nella lista
            $eventsdaylist[count($eventsdaylist) - 1]['eventlistinfo'][] = $e; 
        }
        
        $this->set('response', array('Success' => SuccessCodes::OK, 'eventsdayslist' => $eventsdaylist));
    }
    
    /**
     * Trasforma i parametri di ricerca ottenuti in input in condizioni da aggiungere al metodo Model::find(); 
     * @param puntatore all'array delle conditions $conditions (in e out)
     */
    private function seachParameters(&$conditions, $searchType){
        
        $filters = AppModel::getInputData();
        
        if( !empty($filters->title) ) {
            $conditions['title LIKE'] = "%".HTMLDecoder::encode($filters->title)."%";
        }
        
        if( !empty($filters->city) ) {
            $conditions['city LIKE'] = "%".HTMLDecoder::encode($filters->city)."%";
        }
        
        // categorie e sottocategorie
        if( isset($filters->idcategory) && $filters->idcategory >= 0 ) {
            
            // Se si tratta di una supercategoria la ricerca deve avvenire anche nelle sottocategorie
            $this->loadModel('Category');
            $raw = $this->Category->find('all', array('fields' => array('id', 'idsupercategory'), 'order' => array('idsupercategory ASC')));
            unset($this->Category);
            
            $cats = array();
            
            /*
             * Struttura: array associativo che ad ogni chiave di una supercategoria fa corrispondere un array contenemete la pk della supercategoria e tutte le pk delle sottocategorie
             * La costruzione della struttura è possibile grazie all'ordinamento della selezione che mette tutte le supercategorie in testa alla lista
             */
            foreach ($raw as &$r){
                if (!isset($r['Category']['idsupercategory'])){
                    $cats[$r['Category']['id']] = array($r['Category']['id']);
                }
                else{
                    $cats[$r['Category']['idsupercategory']][] = $r['Category']['id'];
                }
            }
            
            // se quella cercata è una supercategoria si include nella query la condizione che idcategory IN (....) 
            // altrimenti idcategory = pk sottocategoria
            $p = HTMLDecoder::encode($filters->idcategory);
            $conditions['idcategory'] = (isset($cats[$p])) ? $cats[$p] : $p;
        }

        if( isset($filters->sex) && $filters->sex >= 0 ) {
            $conditions['sex'] = HTMLDecoder::encode($filters->sex);
        }

        if( isset($filters->job) && $filters->job >= 0) {
            $conditions['job'] = HTMLDecoder::encode($filters->job);
        }
        
        $agematch = true;
        
        if (($filters->agemin == 0) && ($filters->agemax == 999)) $agematch = false;
        
        if ($agematch){
            
            // se è settatto agemin bisogna scartare tutti gli eventi che hanno 
            if ($filters->agemin != 0) $conditions['agemin <= '] = intval(HTMLDecoder::encode($filters->agemin));
            if ($filters->agemax == 999) $filters->agemax = 100;
            if ($filters->agemax != 100) $conditions['agemax >= '] = intval(HTMLDecoder::encode($filters->agemax));  
        }
       
        /*
         * Stato di occupazione dei posti disponibili
         * 0 : Meno del 33% dei posti disponibili sono occupati
         * 1 : Più del 33% dei posti disponibili sono occupati ma vi sono ancora dei posti disponibili
         * 2 : Non ci sono più posti disponibili
         */
        
        if( isset($filters->status) && $filters->status == 0) {
            $conditions['(sitcurrent/sittotal) <'] = 0.33;
        }

        if( isset($filters->status) && $filters->status == 1) {
            $conditions['(sitcurrent/sittotal) >='] = 0.33;
            $conditions['(sitcurrent/sittotal) <'] = 1;
        }

        if( isset($filters->status) && $filters->status == 2) {
            $conditions['(sitcurrent/sittotal)'] = 1;
        }

        // filtri sulle date in funzione della tipologia di ricerca
        
        switch($searchType){
            
            case self::PAGED:
                if( !empty($filters->datefrom) ) {
                    if ($filters->datefrom > date('Y-m-d')){
                        $conditions['date >='] = HTMLDecoder::encode($filters->datefrom);
                    }
                }
                
                break;
            
            case self::MONTHLY: break; // vengono completamente ignorati i filtri datefrom e dateto
                
            case self::DISTANCE: 
            
                if( !empty($filters->datefrom) ) {
                   $conditions['date >='] = HTMLDecoder::encode($filters->datefrom);
                }
                else{
                    $conditions['date >='] = date('Y-m-d');
                }
                
                if( !empty($filters->dateto) ) {
                    $conditions['date <='] = HTMLDecoder::encode($filters->dateto);
                }
                
                break;
                
            default : break;    
        }
        
        // Filtro eventi creati solo da utenti che si stanno seguendo
        if (isset($filters->follows)){
            
            if($filters->follows == 1) {
                
                $this->loadModel('UserFollow');
                $raw = $this->UserFollow->find('all', array('conditions' => array('iduser' => $filters->iduser), 'fields' => array('idfollow')));
                unset($this->UserFollow);

                if (count($raw) > 0){
                    foreach($raw as $r){
                        $conditions['iduser'][] = $r['UserFollow']['idfollow']; 
                    }
                }
                else{
                    /*
                     * Se nel filtro viene aggiunto il parametro followed = 1 e l'utente non sta seguendo nessuno, l'output deve essere un insieme vuoto 
                     */
                    $conditions['scope'] = 100;
                }
            }
        }
        
        // Filtro eventi business
        if (strlen($filters->event_grade) > 0){
            $conditions['event_grade'] = HTMLDecoder::encode($filters->event_grade);
        }
    }
}

?>
