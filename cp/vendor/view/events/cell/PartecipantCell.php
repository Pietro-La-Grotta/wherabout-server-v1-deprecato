<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PartecipantCell
 * Cognome e nome del partecipante 
 * @author pietro@studioleaves.com
 */
class PartecipantCell implements ICell{
    
    private static $href_title = "Vai al profilo del partecipante";
    private static $href_params = "index.php/utenti?show=edit&item=";
    
    private $tuple;
    private $name_label_field;
    private $surname_label_field;
    private $id_label_field;
   
    /**
     * Inizializza le variabili di instanza
     * @param string $name_label_field nome del campo della tupla contenente il nome del partecipante
     * @param string $surname_label_field nome del campo della tupla contenente il cognome del partecipante
     * @param string $id_label_field nome del campo della tupla contenente la pk del partecipante
     */
    function __construct($name_label_field, $surname_label_field, $id_label_field) {
        $this->name_label_field = $name_label_field;
        $this->surname_label_field = $surname_label_field;
        $this->id_label_field = $id_label_field;
    }

    
    /**
     * Stampa il contenuto html
     * @return string
     */
    public function html() {
        $html = "";
            
            $partecipant = $this->tuple[$this->surname_label_field].' '.$this->tuple[$this->name_label_field];
            
            $html .= '<td>';
                $html .= '<a href="'.Juri::base().self::$href_params.$this->tuple[$this->id_label_field].'" title="'.self::$href_title.'" target="_blank">';
                $html .= $partecipant;
                $html .= '</a>';
            $html .= '</td>';
            
        return $html;
    }

    /**
     * Setter per la tupla
     * @param array $tuple
     */
    public function data(&$tuple) {
        $this->tuple = $tuple;
    }    
}

?>
