<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AvFollowerCode
 * Accesso alla vista che associa i follower ad i loro pushcodes
 * @author pietro@studioleaves.com
 */
class AvFollowerCode extends AppModel{
    
    /**
     * Seleziona la lista dei pushcodes appartenenti ai follower dell'utente che ha inviato la richiesta
     * @return array of pushcodes
     */
    public function getFollowerCodes(){
        $raw = $this->find('all', array('conditions' => array('idfollowed' => self::$inputData->iduser), 
                                        'fields' => array('pushcode')));
        
        $followercodes = array();
        
        foreach($raw as &$r){
            $followercodes[] = $r['AvFollowerCode']['pushcode'];
        }
        
        return $followercodes;
    }
}

?>
