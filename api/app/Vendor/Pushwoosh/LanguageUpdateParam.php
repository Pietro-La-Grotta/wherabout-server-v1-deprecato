<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of LanguageUpdateParam
 * Wrapper per il passggio dei parametri necessari al cambio della lingua impostata per le notifiche
 * @author pietro@studioleaves.com
 */
class LanguageUpdateParam implements IPushParameter{
    
    /**
     * API Access
     * https://cp.pushwoosh.com/api_access
     * @var string 
     */
    public $application;
    
    /**
     * Codice push assegnato nella prima registrazione
     * @var string
     */
    public $pushToken;
    
    /**
     * Codice univoco del dispositivo
     * @var string
     */
    public $deviceCode;
    
    /**
     * Tipologia di dispositivo
     * @var int {1: ios, 2: android}
     */
    public $deviceType;
    
    /**
     * Codice della lingua da impostare {"it" | "en"}
     * @var string
     */
    public $language;
}

?>
