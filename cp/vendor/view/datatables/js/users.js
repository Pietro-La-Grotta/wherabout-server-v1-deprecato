/* 
 * Script usati per la lista degli utenti
 */

/**
 * Inizializzazione della tabella per la lista delle prenotazioni per un oggetto
 */
function init_data_table(){
    jQuery(document).ready(function() {
        
      var table = jQuery('#studio_leaves').DataTable({
           
            "lengthMenu": [[100, 200, 400, -1], [100, 200, 400, "All"]],
            "aaSorting": [[0,'asc']],
            "paging":   true,
            "aoColumnDefs": [{ 'bSortable': false, 
                               'aTargets': [2,3,5]
                           }]
       });
        
       // selezione multipla delle righe
       jQuery('#studio_leaves tbody').on( 'click', 'tr', function (){
           jQuery(this).toggleClass('selected');
       });
       
       // Abilitazione degli utenti selezionati
       
       jQuery('#active_button').click( function () {
            var x = table.rows('.selected').data().length;
            
            if (x < 1){
                alert('E\' necessario selezionare gli utenti per i quali rimuovere il ban');
                return;
            }
            
            if (confirm('Si è certi di volere rimuovere il ban agli utenti selezionati?')){
                jQuery('#users_to_active').val(getSelectedList());
                document.active_users_form.submit();  
            }
       });
        
        // Disabilitazione degli utenti selezionati
       
       jQuery('#disable_button').click( function () {
            var x = table.rows('.selected').data().length;
            
            if (x < 1){
                alert('E\' necessario selezionare gli utenti da bannare');
                return;
            }
            
            if (confirm('Si è certi di volere bannare gli utenti selezionati?')){
                jQuery('#users_to_disable').val(getSelectedList());
                document.disable_users_form.submit();  
            }
        });
       
    });
}

init_data_table();

var comodo;

/**
 * Genera la lista degli id delle righe selezionate
 * @returns {@exp;JSON@call;stringify} lista in formato json trasformata in stringa
 */
function getSelectedList(){
    comodo = [];
    jQuery('.selected').each(function (){
        comodo[comodo.length] = jQuery(this).attr("id");
    });
    return JSON.stringify(comodo);
}