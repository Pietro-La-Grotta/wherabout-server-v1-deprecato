<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CategoryModel
 * Accesso alle categorie di evento
 * @author pietro@studioleaves.com
 */
class CategoryModel {
    
    /**
     * Seleziona i dati associati ad unacategoria dati gli id
     * @param int $id pk categories
     * @return object
     */
    public function getById($id){
        
        $db = JFactory::getDbo();
        $query = $db->getQuery(true); 
        
        $query->select('*')
              ->from($db->qn('categories'))
              ->where($db->qn('id').' = '.$db->q(HTMLDecoder::encode($id)));
        
        $db->setQuery($query);
        
        return $db->loadNextObject();
    }
}

?>
