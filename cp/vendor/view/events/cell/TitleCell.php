<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TitleCell
 * Titolo dell'evento troncato
 * @author pietro@studioleaves.com
 */
class TitleCell implements ICell{
     
    private static $max_title_len = 20;
    private static $title = 'Dettagli evento';
    private static $href_params = "?show=edit&item=";

    private $tuple;
    private $label_field;
    private $idevent_label_field;
   
    /**
     * Inizializza le variabili di istanza
     * @param string $label_field nome dell'attributo della tupla contenente il titolo dell'evento
     */
    public function TitleCell($label_field, $idevent_label_field) {
        $this->label_field = $label_field;
        $this->idevent_label_field = $idevent_label_field;
    }
  
    /**
     * Stampa il contenuto html
     * @return string
     */
    public function html() {
        $html = "";
            
            $city = $this->tuple[$this->label_field];
            $label = (strlen($city) > self::$max_title_len) 
                   ? substr($city, 0, self::$max_title_len).'...'
                   : $city;
            
            $html .= '<td>';
                $html .= '<a href="'.Juri::base().self::$href_params.$this->tuple[$this->idevent_label_field].'" title="'.self::$title.'" target="_blank" title="'.self::$title.'">';
                $html .= $label;
                $html .= '</a>';
            $html .= '</td>';
            
        return $html;
    }

    /**
     * Setter per la tupla
     * @param array $tuple
     */
    public function data(&$tuple) {
        $this->tuple = $tuple;
    }    
}

?>
