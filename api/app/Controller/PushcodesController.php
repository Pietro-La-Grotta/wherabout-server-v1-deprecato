<?php


/**
 * PushController is the main controller for push based request and it's used by :
 *  - UserController
 *
 * @author Christian Ruggiero
 *@creation 4/11/2014
 * @status in develop
 */


class PushcodesController extends AppController{
    //Not in a view - these functions are called by other classes: they don't need any view class   
    public function savePushcode( $iduser, $code, $hwid , $osType) {
        $pushCodeInfo = $this->Pushcode->getPushcodeInfo($hwid);
        if($pushCodeInfo != null) {
            //se gli id dei dispositivi sono uguali e gli id degli utenti sono diversi...
            if( ( $pushCodeInfo["Pushcode"]["iduser"] != $iduser) || ($pushCodeInfo["Pushcode"]["code"] != $code)  ) {
                //update if already exists
                if( !$this->Pushcode->deleteAll(array('Pushcode.hwid' => $hwid ), false) ) {
                    //se c'è un problema con la rimozione...
                    return false;
                }
            }
            else {
                return true;
            }
        }

        //se non esiste, aggiungilo
        $pushObject = new stdClass();
        $pushObject->iduser = $iduser; 
        $pushObject->code   = $code;  
        $pushObject->hwid   = $hwid;
        // tipologia di sistema operativo di default = android
        $pushObject->os = (isset($osType) ? $osType : 1);
        
        $this->Pushcode->save($pushObject);
        $this->Pushcode->clear();
        return true;
        
        
    }
    
    //Not in a view 
    public function deleteAllPushcode($iduser) {
        if(isset($iduser)) {
            return $this->Pushcode->deleteAll(array('Pushcode.iduser' => $iduser), false) ;
        }
        else {
            return $this->Pushcode->deleteAll(array('Pushcode.idpushcodes >=' => 0), false) ;
        }
    }

    public function sendPush( $type, $iduser, $receiverlist, $message, $creationdate = null, $username = null, $minmessage = null ){
            
        App::import('Controller',   'Notifications');
        App::import('Controller',   'PushMessages');

        //invare le pushnotification agli id presento in $receiverlist
        if( isset($iduser) && isset($receiverlist) && isset($message)) {
            
            App::import('Vendor', 'Pushwoosh/Pushwoosh');
            
            $pushwoosh = Pushwoosh::create(function(){
                App::import('Vendor', 'Pushwoosh/IPushParameter');
                App::import('Vendor', 'Pushwoosh/CreateParam');
                App::import('Vendor', 'Pushwoosh/CustomData');
                App::import('Vendor', 'Pushwoosh/LanguageUpdateParam');
            });
            
            $pushcodeslist = $this->getPushcodeFromUser( $receiverlist );
            if( !empty( $pushcodeslist ) ) {
                $params                 = new CreateParam();
                $params->receivers      = $pushcodeslist;
                $pushMessagaesController = new PushMessagesController();
                
                switch ($type) {
                    case NotificationsController::NOTIFICATION_TYPE_FOLLOWERS_EVENT_CREATED :
                        $params->shortContent       = $pushMessagaesController->getValue(NotificationsController::NOTIFICATION_MESSAGE_FOLLOWERS_EVENT_CREATED);
                        $params->customData->type   = NotificationsController::NOTIFICATION_TYPE_FOLLOWERS_EVENT_CREATED;
                        break;
                    case NotificationsController::NOTIFICATION_TYPE_PARTECIPANTS_EVENT_UPDATE :
                        $params->shortContent   = $pushMessagaesController->getValue(NotificationsController::NOTIFICATION_MESSAGE_PARTECIPANTS_EVENT_UPDATE);
                        $params->customData->type   = NotificationsController::NOTIFICATION_TYPE_PARTECIPANTS_EVENT_UPDATE;
                        break;
                    case NotificationsController::NOTIFICATION_TYPE_EVENT_INVITATION :
                        $params->shortContent   = $pushMessagaesController->getValue(NotificationsController::NOTIFICATION_MESSAGE_EVENT_INVITATION);
                        $params->customData->type   = NotificationsController::NOTIFICATION_TYPE_EVENT_INVITATION;
                        break;
                    case NotificationsController::NOTIFICATION_TYPE_FRIENDSHIP_REQUEST :
                        $params->shortContent   = $pushMessagaesController->getValue(NotificationsController::NOTIFICATION_MESSAGE_FRIENDSHIP_REQUEST);
                        $params->customData->type   = NotificationsController::NOTIFICATION_TYPE_FRIENDSHIP_REQUEST;
                        break;
                    case NotificationsController::NOTIFICATION_TYPE_FRIENDSHIP_ACCEPTED :
                        $params->shortContent   = $pushMessagaesController->getValue(NotificationsController::NOTIFICATION_MESSAGE_FRIENDSHIP_ACCEPTED);
                        $params->customData->type   = NotificationsController::NOTIFICATION_TYPE_FRIENDSHIP_ACCEPTED;
                        break;
                    case NotificationsController::NOTIFICATION_TYPE_BOOKING_REQUEST :
                        $params->shortContent   = $pushMessagaesController->getValue(NotificationsController::NOTIFICATION_MESSAGE_BOOKING_REQUEST);
                        $params->customData->type   = NotificationsController::NOTIFICATION_TYPE_BOOKING_REQUEST;
                        break;
                    case NotificationsController::NOTIFICATION_TYPE_BOOKING_ACCEPT :
                        $params->shortContent   = $pushMessagaesController->getValue(NotificationsController::NOTIFICATION_MESSAGE_BOOKING_ACCEPT);
                        $params->customData->type   = NotificationsController::NOTIFICATION_TYPE_BOOKING_ACCEPT;
                        break;
                    case NotificationsController::NOTIFICATION_TYPE_CHATMESSAGE_RECEIVED :
                        // Invia un messaggio chat tramite notifica push
                        $params->shortContent               = $username . $pushMessagaesController->getValue(NotificationsController::NOTIFICATION_MESSAGE_CHATMESSAGE_RECEIVED);
                        $params->customData->type           = NotificationsController::NOTIFICATION_TYPE_CHATMESSAGE_RECEIVED;
                        $params->customData->idsender       = $iduser;
                        $params->customData->username       = $username;
                        $params->customData->creationdate   = $creationdate;
                        $params->customData->message        = $message;
                        
                        // notifica push disponiobile ai servizi in background di ios 
                        $pushwoosh->setIosBGAvailable();
                        
                        break;
                    default :
                        $params->shortContent   = $message;
                        break;
                }

                $params->timeToSend     = "now";
                $pushwoosh->createMessage($params);
                return true;
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
    }
    
    
    private function getPushcodeFromUser( $iduser ) {
        if( isset($iduser) ) {
            $pushcodeList           = $this->Pushcode->find( 'all', array( 'conditions' => array( 'iduser' => $iduser) ) );
            $pushwooshCodeList = array();
            foreach ( $pushcodeList as $pushcodeEntry ) {
                $pushwooshCodeList[] = $pushcodeEntry["Pushcode"]["code"];
            }
            return $pushwooshCodeList;
        }
        else {
            return null;
        }
    }
    
    
     
}
