<?php


/**
 * PushController is the main controller for push based request and it's used by :
 *  - UserController
 *
 * @author Christian Ruggiero
 *@creation 4/11/2014
 * @status in develop
 */
class JobsController extends AppController{
    
    public function getlist( ){
        $responseObj = Utils::generateSuccess( SuccessCodes::OK );
        $jobList = $this->Job->getJobList( );
        $jobs = array();
        foreach ( $jobList as $jobObject ){
            $jobs[] = $jobObject["Job"];
        } 
        $responseObj->jobs = $jobs;
        $this->set( 'response',  $responseObj );
    }

}
