<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of LuogoCell
 * Città in cui avverrà l'evento in maiuscolo e troncata
 * @author pietro@studioleaves.com
 */
class LuogoCell implements ICell{
     
    private static $max_title_len = 15; 

    private $tuple;
    private $label_field;
   
    /**
     * Inizializza le variabili di istanza
     * @param string $label_field nome dell'attributo della tupla contenente il titolo dell'oggetto 
     */
    public function LuogoCell($label_field) {
        $this->label_field = $label_field;
    }
  
    /**
     * Stampa il contenuto html
     * @return string
     */
    public function html() {
        $html = "";
            
            $city = $this->tuple[$this->label_field];
            $label = (strlen($city) > self::$max_title_len) 
                   ? substr($city, 0, self::$max_title_len).'...'
                   : $city;
            
            $html .= '<td>';
                //$html .= '<a href="'.Juri::base().'index.php'.self::$menu.'&item='.$this->tuple[$this->id_field].'" title="'.self::$title.'" target="_blank">';
                $html .= strtoupper($label);
                //$html .= '</a>';
            $html .= '</td>';
            
        return $html;
    }

    /**
     * Setter per la tupla
     * @param array $tuple
     */
    public function data(&$tuple) {
        $this->tuple = $tuple;
    }    
}

?>
