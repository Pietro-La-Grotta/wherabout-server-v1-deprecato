-- modifica della tabella degli utenti
ALTER TABLE `users` ADD `user_grade` INT(1) NOT NULL DEFAULT '0' COMMENT '0:standard, 1:business' ;
-- modifica della tabella degli eventi
ALTER TABLE `events` ADD `event_grade` INT(1) NOT NULL DEFAULT '0' COMMENT '0:standard, 1:business' ;
-- qualche commento non guasta mai
ALTER TABLE `user_follows` CHANGE `iduser` `iduser` INT(11) NOT NULL COMMENT 'pk del follower';
ALTER TABLE `user_follows` CHANGE `idfollow` `idfollow` INT(11) NOT NULL COMMENT 'pk fallowed';

-- stored procedure per il livellamento del buffer delle notifiche di un utente

DROP PROCEDURE IF EXISTS free_notification_buffer;

DELIMITER $$

/*
    libera le notifiche in eccesso al buffer dedicato ad un dato destinatario
    
    INPUT:
        id_receiver_ INT(11): pk del destinatario
        buffer_lenght_ INT(3): massima lunghezza del buffer
*/
CREATE PROCEDURE free_notification_buffer(id_receiver_ INT(11), buffer_lenght_ INT(3))
BEGIN

    DECLARE done INT DEFAULT FALSE;

    DECLARE buffer_occupation_ INT(3);
    DECLARE i_ INT(3) DEFAULT 0;

    DECLARE id_notification_ INT(11);

    DECLARE notifications_cursor CURSOR FOR SELECT `id` FROM `notifications` WHERE `idreceiver` = id_receiver_ ORDER BY `id` DESC; 
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

    SELECT count(`id`) INTO buffer_occupation_ FROM `notifications` WHERE `idreceiver` = id_receiver_;

    IF buffer_occupation_ >= buffer_lenght_ THEN
        
        OPEN notifications_cursor;

        read_loop: LOOP
        
            -- accesso ai dati selezionati dal cursore
            FETCH notifications_cursor INTO id_notification_;
        
            -- condizione di uscita dal loop
            IF done OR i_ = buffer_lenght_ THEN
                
                DELETE FROM `notifications` WHERE `id` < id_notification_ AND `idreceiver` = id_receiver_;
                LEAVE read_loop;

            ELSE

               set i_ = i_ + 1;

            END IF;
        
        END LOOP;

        CLOSE notifications_cursor;
        
    END IF;

END $$

DELIMITER ;


-- stored procedure per l'inserimento delle notifiche locali per i followers di un planner che crea un evento
DROP PROCEDURE IF EXISTS create_event_notifications;

DELIMITER $$
/*
    Quando un evento pubblico viene creato vengono aggiunte le notifiche locali ai follower del planner
    Viene gestita la dimensione del buffer
    
    INPUT:
        id_event_ INT(11): pk evento creato
        notification_type_ INT(2): tipologia di messaggio (purtroppo il cazzone che ha fatto le api ha messo il valore in una costante php)
        buffer_lenght_ INT(3): massima lunghezza del buffer
*/
CREATE PROCEDURE create_event_notifications(id_event_ INT(11), notification_type_ INT(2), buffer_lenght_ INT(3))
BEGIN

    DECLARE done INT DEFAULT FALSE;

    DECLARE id_planner_, id_follower_ INT(11);

    DECLARE followers_cursor CURSOR FOR SELECT `iduser` FROM `user_follows` WHERE `idfollow` = id_planner_; 
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

    -- selezione del planner
    SELECT `iduser` INTO id_planner_ FROM `events` WHERE `idevent` = id_event_;
    
    OPEN followers_cursor;

    read_loop: LOOP
        
        -- accesso ai datiselezionati dal cursore
        FETCH followers_cursor INTO id_follower_;
        
        -- condizione di uscita dal loop
        IF done THEN
            LEAVE read_loop;
        END IF;
        
        -- insermento della notifica
        INSERT INTO `notifications`(`type`, `idsender`, `idreceiver`, `idevent`) VALUES (notification_type_, id_planner_, id_follower_, id_event_);

        -- livellamento del buffer
        CALL free_notification_buffer(id_follower_, buffer_lenght_);
        
    END LOOP;

    CLOSE followers_cursor;
        
END $$

DELIMITER ;


-- vista per la selezione dei pushcodes dei follower
drop view if exists av_follower_codes;
create view av_follower_codes as 
select f.idfollow idfollowed, f.iduser idfollower, p.code pushcode
from user_follows f join pushcodes p on f.iduser = p.iduser;


-- stored procedure per l'inserimento delle notifiche locali per i partecipanti ad un evento che viene modificato
DROP PROCEDURE IF EXISTS update_event_notifications;

DELIMITER $$
/*
    Quando un evento pubblico viene modificato vengono aggiunte le notifiche locali ai partecipanti all'evnto
    Viene gestita la dimensione del buffer
    
    INPUT:
        id_event_ INT(11): pk evento creato
        notification_type_ INT(2): tipologia di messaggio (purtroppo il cazzone che ha fatto le api ha messo il valore in una costante php)
        buffer_lenght_ INT(3): massima lunghezza del buffer
*/
CREATE PROCEDURE update_event_notifications(id_event_ INT(11), notification_type_ INT(2), buffer_lenght_ INT(3))
BEGIN

    DECLARE done INT DEFAULT FALSE;

    DECLARE id_planner_, id_partecipant_ INT(11);

    DECLARE partecipants_cursor CURSOR FOR SELECT `iduser` FROM `partecipates` WHERE `idevent` = id_event_ and `iduser` != id_planner_; 
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

    -- selezione del planner
    SELECT `iduser` INTO id_planner_ FROM `events` WHERE `idevent` = id_event_;
    
    OPEN partecipants_cursor;

    read_loop: LOOP
        
        -- accesso ai dati selezionati dal cursore
        FETCH partecipants_cursor INTO id_partecipant_;
        
        -- condizione di uscita dal loop
        IF done THEN
            LEAVE read_loop;
        END IF;
        
        -- insermento della notifica
        INSERT INTO `notifications`(`type`, `idsender`, `idreceiver`, `idevent`) VALUES (notification_type_, id_planner_, id_partecipant_, id_event_);

        -- livellamento del buffer
        CALL free_notification_buffer(id_partecipant_, buffer_lenght_);
        
    END LOOP;

    CLOSE partecipants_cursor;
        
END $$

DELIMITER ;


-- vista per la selezione dei pushcodes dei partecipanti
drop view if exists av_partecipant_codes;
create view av_partecipant_codes as 
select f.iduser idpartecipant, f.idevent, f.confirmed, p.code pushcode
from partecipates f join pushcodes p on f.iduser = p.iduser;


-- ridefinizione della vista activated_user perl'inserimento di event_grade

drop view if exists activated_events;

create view activated_events as
select e.*, `u`.`img` AS `userimg`,`u`.`job` AS `job`
from events e join users u on e.iduser = u.iduser and e.activated = 1 and u.activated = 1;