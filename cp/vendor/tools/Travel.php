<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Travel
 * Simgleton per il trasporto dei riferimenti
 * @author pietro@studioleaves.com
 */
class Travel {
    
    /**
     * Struttura di trasporto dei riferimenti
     * @var array
     */
    private $vector;
    
    /**
     * Singoletto
     * @var Travel
     */
    private static $instance = null;
    
    /**
     * Costruttore privato
     */
    private function __construct() {
         $this->vector = array();
    }
    
    /**
     * Selettore del singoletto
     * @return Travel
     */
    public static function getInstance(){
        if (!isset(self::$instance)){
            self::$instance = new Travel();
        }
        
        return self::$instance;
    }
    
    /**
     * Aggiunge un riferimento ad una variabile nella singoletto
     * @param string $name identificativo
     * @param mixed $ref riferimento alla variabile
     */
    public function set($name, &$ref){
        self::$instance->vector[$name] = $ref; 
    }
    
    /**
     * Selettore di un  riferimento
     * @param $name identuficativo del riferimento
     * @return mixed riferimento selezionato o null
     */
    public function get($name){
        return isset(self::$instance->vector[$name]) ? self::$instance->vector[$name] : false; 
    }
}

?>
