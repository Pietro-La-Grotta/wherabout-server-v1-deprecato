<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
App::import('Vendor', 'Pushwoosh/wherabout/PushSender');

/**
 * Description of EventCreated
 * Invio delle notifiche push nl caso di creazione evento 
 * @author pietro@studioleaves.com
 */
class EventCreated extends PushSender{
    
    private static $shortMessage = "Someone you follow just created an event!";
    private static $messageType = 0;
    
    /**
     * Inizializzazione variabili di istanza per le notifiche di creazione evento
     */
    public function __construct() {
        parent::__construct();
        
        $this->param->shortContent = self::$shortMessage;
        $this->param->customData->type = self::$messageType;
    }
    
    /**
     * Invia la richiesta di spedizione delle notifiche
     */
    public function send(){
        $this->sendNow();
    }
}

?>
