<?php


/**
 * PushController is the main controller for push based request and it's used by :
 *  - UserController
 *
 * @author Christian Ruggiero
 *@creation 4/11/2014
 * @status in develop
 */
class CategoriesController extends AppController{
    
//    public function getlistold( ){
//        $responseObj = Utils::generateSuccess( SuccessCodes::OK );
//        $categoryList = $this->Category->getCategoryList( );
//        $categories = array();
//        foreach ( $categoryList as $categoryObject ){
//            $category = $categoryObject["Category"];
//            $subcategoryList = $this->getSubcategoryList( $category["id"] ,$categoryList);
//            if(!empty($subcategoryList)) {
//                $categories[] = $categoryObject["Category"];
//                foreach ( $subcategoryList as $subcategoryObject ){
//                     $categories[] = $subcategoryObject["Category"];
//                }
//            } 
//            else {
//                if( empty($category["idsupercategory"]) ) {
//                    $categories[] = $categoryObject["Category"];
//                }
//            }
//            
//           
//            
//        } 
//        $responseObj->categories = $categories;
//        $this->set( 'response',  $responseObj );
//    }
//    
//    private function getSubcategoryList( $id, $categoryList ) {
//        $subcategoryList = array();
//        foreach ( $categoryList as $categoryObject ){ 
//            $category = $categoryObject["Category"];
//            if ( $category["idsupercategory"] == $id ) {
//                $subcategoryList[] = $categoryObject;
//            }
//        }
//        return $subcategoryList;
//    }

    public function getlist(){
        $tree = array(); 
        // selezione categorie e sottocategorie
        $raw = $this->Category->find('all', array('order' => array('id')));
        
        // fillin delle categorie
        foreach($raw as &$r){
            if (!isset($r['Category']['idsupercategory'])){
                $tree[$r['Category']['id']] = array('sup' => $r['Category'], 'sub' => array());
            }
        }
        
        // fillin delle sottocategorie
        foreach($raw as &$r){
            if (isset($r['Category']['idsupercategory'])){
                $tree[$r['Category']['idsupercategory']]['sub'][] = $r['Category'];
            }
        }
        
        $response = array('Success' => SuccessCodes::OK, 'categories' => array());
        
        // generazione della risposta
        foreach ($tree as &$t){
            $response['categories'][] = $t['sup'];
            
            foreach ($t['sub'] as &$s){
                $response['categories'][] = $s;
            }
        }
        
        $this->set( 'response',  $response);
    }
    
}
