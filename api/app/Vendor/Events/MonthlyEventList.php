<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of EventSet
 *
 * @author Christian 
 */

App::import('Vendor', 'Events/MonthlyEventCount');
class MonthlyEventList {
    public $eventsinadaylist = array(); //array of /Vendor/EventList
    
    public function addEvent( $date ) {
        $find = false;
        foreach ( $this->eventsinadaylist as $eventListObj ) {
            if ($eventListObj->date == $date) {
                $eventListObj->count = intval( $eventListObj->count) + 1;
                $find = true;
            }   
        }
        if( !$find ){
            $eventDay = new MonthlyEventCount();
            $eventDay->date = $date;
            $eventDay->count = intval( $eventDay->count ) + 1;
            $this->eventsinadaylist[] = $eventDay;
        }
    }

}
