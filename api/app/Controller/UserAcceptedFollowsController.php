<?php


/**
 * PushController is the main controller for push based request and it's used by :
 *  - UserController
 *
 * @author Christian Ruggiero
 *@creation 4/11/2014
 * @status in develop
 */
class UserAcceptedFollowsController extends AppController{
     public function getFollowList( $iduser ) {
        return $this->UserAcceptedFollow->getFollowList( $iduser );
    }
}
