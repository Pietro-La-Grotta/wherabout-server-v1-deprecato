<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Base64
 * Gestione delle immagini ricevute in base 64
 * @author pietro@studioleaves.com
 */
class ImageManager {
    
    /**
     *
     * @var type 
     */
    public static $folder;
   
    /**
     * Data una stringa contenente una rappresentazione base64 di una immagine
     * crea un file .jpg e lo posiziona in una spcifica cartella
     * 
     * Nota: ImageManager::folder deve contenere il percorso della cartella che deve contenere l'immagine 
     *  
     * @param string $base64Img rappresentazione base64 di una immagine
     * @return nome del file creato o false se si sono verificati degli errori
     */
    public static function base64toJpg(&$base64Img){
        
        if (!isset(self::$folder)){
            return false;
        }
    
        if( !isset($base64Img) || $base64Img == null || $base64Img == "" || $base64Img == "null" || $base64Img == "-1") {
            return false;
        }
        
        // mette l'immagine in un file senza estensione
        $name = self::rand_image_name('');
        $file = new File(self::$folder.$name, true);

        if(!$file->open("w")){
            return false;
        }

        if(!$file->write(base64_decode($base64Img), "wb", true)){
            return false;
        }

        list($width, $height, $type, $attr) = getimagesize(self::$folder.$name);
        $finalname = $file->name.image_type_to_extension($type);
        
        $file->close();
        
        // mette l'estensione al file immagine
        rename(self::$folder.$name, self::$folder.$finalname);
        
        return $finalname;
    }
    
    /**
     * Genera un nome pseudocasuale per un  file
     * @param string $ext estensione da associare al nome del file
     * @return string nome del file
     */
    private static function rand_image_name(){
        return "".date("YmdHis").'_'.rand(0, 10000);
    }
    
    /**
     * Eliminazione di una immagine in una data cartella
     * @param nome dell'immagine con path $imageName
     */
    public static function delete($imageName){
        
        $iex = explode('/', $imageName);
        
        if(count($iex > 0)){
            $img = $iex[count($iex) - 1];
            unlink(self::$folder.$img);
        }
        
    }
    
    /**
     * Duplica una data immagine dato il nome e settato precedentemente il target di riferimento 
     * @param string $baseName nome dell'immagine da clonare
     * @return string nome del clone dell'immagine
     */
    public static function duplicate($original, $id){
        
        $explosion = explode('.', $original);
        $extension = $explosion[count($explosion) - 1];
        
        copy(self::$folder.'/'.$original, self::$folder.'/'.$id.'.'.$extension);
        
    }
}

?>
