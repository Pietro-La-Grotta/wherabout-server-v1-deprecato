<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description - User represent a registered user on the DB. It gives full infos about personal glimpse of user itself.
 *
 * @author Michele Addante
 * @creation 30/10/2014
 * @status in develop
 */

App::uses('AppModel', 'Model');
class User extends AppModel{
    //public $name = 'User';
    
    public function userExists( $email = null, $idfacebook = null, $idgoogle = null) { 
        if ( isset($email) && ( intval( $this->find( 'count', array( 'conditions' => array( 'email' => $email) ) ) > 0 ) ) ) {
            return TRUE;
        } 
        elseif ( isset($idfacebook) && ( intval( $this->find( 'count', array( 'conditions' => array( 'idfacebook' => $idfacebook) ) ) > 0 ) ) ) {
            return TRUE;
        } 
        elseif ( isset($idgoogle) && ( intval( $this->find( 'count', array( 'conditions' => array( 'idgoogle' => $idgoogle) ) ) > 0 ) ) ) {
            return TRUE;
        }
        else {
            return FALSE;
        }
    }
    
    
    /**
     * Seleziona le informazioni associate all'utente usando uno dei quattro metodi di accesso ad un utente (pk | idfacebook | idgoogle | email)
     * @param int $iduser pk utente (bisogna specificare null se si usa un altro metodo di accesso) 
     * @param string $idfacebook fb userid (bisogna specificare null se si usa un altro metodo di accesso) 
     * @param string $idgoogle g+ user id (bisogna specificare null se si usa un altro metodo di accesso) 
     * @param string $email email associata all'utente (bisogna specificare null se si usa un altro metodo di accesso) 
     * @return array associativo con la struttura della tabella user 
     */
    public function getUserInfo( $iduser = null, $idfacebook = null, $idgoogle = null, $email = null) {
        
        if( isset($iduser) ) {
            return $this->find( 'first', array( 'conditions' => array( 'iduser' => $iduser) ) );
        }
        elseif( isset($idfacebook) ) {
            return $this->find( 'first', array( 'conditions' => array( 'idfacebook' => $idfacebook) ) );
        }
        elseif( isset($idgoogle) ) {
            return $this->find( 'first', array( 'conditions' => array( 'idgoogle' => $idgoogle) ) );
        }
        elseif( isset($email) ) {
            return $this->find( 'first', array( 'conditions' => array( 'email' => $email) ) );
        }
        return null;
    }
    
    
    public function loginUser( $email = null, $password = null, $idfacebook = null, $idgoogle = null ) {
        if( isset($email) && isset( $password ) ) {
            return $this->find( 'first', array( 'conditions' => array( 'email' => $email, 'password' => $password) ) );
        }
        elseif( isset($idfacebook) ) {
            return $this->find( 'first', array( 'conditions' => array( 'idfacebook' => $idfacebook) ) );
        }
        elseif( isset($idgoogle) ) {
            return $this->find( 'first', array( 'conditions' => array( 'idgoogle' => $idgoogle) ) );
        }
        return null;
    }
    
    /**
     * Data la lista di pk di utenti restituisce una lista di oggetti conteneti la coppia iduser, img (url immagine avatar)
     * @return lista di oggetti
     */
    public function avatars(){
    
        $response = array();
        
        foreach ($this->find('all', array('conditions' => array('iduser' => self::$inputData->users), 
                                          'fields' => array('iduser', 'img'))) as $tuple){
            $response[] = $tuple['User'];
        }
        
        return $response;
    }
    
    /**
     * Controlla su dati in input se l'utente ha cercato di effettuare la registrazione tramite fb
     * @return boolean true -> registrazione tramite fb, false altrimenti
     */
    public function registerByFacebook(){
        $fb = self::$inputData->idfacebook;
        return isset($fb) && (intval($fb) > 0) && ($fb != 'null'); 
    }
    
    /**
     * Controlla su dati in input se l'utente ha cercato di effettuare la registrazione tramite google
     * @return boolean true -> registrazione tramite google, false altrimenti
     */
    public function registerByGoogle(){
        $google = self::$inputData->idgoogle;
        return isset($google) && (intval($google) > 0) && ($google != 'null'); 
    }
    
    /**
     * Controlla se esiste già un utente con il facebook id dato
     * return true se esiste già, false altrimenti
     */
    public function facebookExists(){
        return ($this->find('count', array('conditions' => array('idfacebook' => self::$inputData->idfacebook))) > 0);
    }
    
    /**
     * Controlla se esiste già un utente con il google id dato
     * return true se esiste già, false altrimenti
     */
    public function googleExists(){
        return ($this->find('count', array('conditions' => array('idgoogle' => self::$inputData->idgoogle))) > 0);
    }
    
    /**
     * Controlla se esiste già un utente registrato con quella email
     * return true se esiste già, false altrimenti 
     */
    public function emailExists(){
        return ($this->find('count', array('conditions' => array('email' => self::$inputData->email))) > 0);
    }
    
    /**
     * Salvataggio del nuovo utente sul database
     * @param $avatarPath imamfgine avatar creata
     * @return int pk utente appena inserito
     */
    public function addUser($avatarPath){
        
        $fields = array();
        
        $fields['email'] = self::$inputData->email;
        if ($this->registerByGoogle()) $fields['idgoogle'] = self::$inputData->idgoogle;
        if ($this->registerByFacebook()) $fields['idfacebook'] = self::$inputData->idfacebook;
        $fields['name'] = HTMLDecoder::encode(ucwords(strtolower(self::$inputData->name)));
        $fields['surname'] = HTMLDecoder::encode(ucwords(strtolower(self::$inputData->surname)));
        $fields['img'] = $avatarPath;
        $fields['birthdate'] = self::$inputData->birthdate;
        $fields['sex'] = self::$inputData->sex;
        $fields['password'] = self::$inputData->password;
        
        $fields['job'] = self::$inputData->job;
        $fields['city'] = HTMLDecoder::encode(ucwords(strtolower(self::$inputData->city)));
        $fields['interests'] = HTMLDecoder::encode(self::$inputData->interests);
        $fields['creationdate'] = date('Y-m-d H:i:s');
        
        
        $this->set($fields);
        $this->save();
        
        return $this->getLastInsertID();
    }
    
    /**
     * Decodifica gli attributi del dato grezzo prima di restituire l'oggetto
     * @param $user stClass
     * @return stdClass Description Dati utente decodificati 
     */
    private function decodeFields($user){
        $user['User']['name'] = HTMLDecoder::decode($user['User']['name']);
        $user['User']['surname'] = HTMLDecoder::decode($user['User']['surname']);
        $user['User']['city'] = HTMLDecoder::decode($user['User']['city']);
        $user['User']['interests'] = HTMLDecoder::decode($user['User']['interests']);
        $user['User']['job'] = HTMLDecoder::decode($user['User']['job']);
        
        return $user['User'];
    }
    
    /**
     * Accede ai dati dell'utente utilizzando il facebook id
     * @return boolean -> false se nessuna corrispondenza viene trovata, array('User' => stdClass{... dati utente}) se l'utente esiste 
     */ 
    public function loginByFacebook(){
        $user = $this->find('all', array('conditions' => array('idfacebook' => self::$inputData->idfacebook)));
        if (count($user) == 0) return false;
        
        return $this->decodeFields($user[0]); 
    }
    
    /**
     * Accede ai dati dell'utente utilizzando il google id
     * @return boolean -> false se nessuna corrispondenza viene trovata, array('User' => stdClass{... dati utente}) se l'utente esiste 
     */ 
    public function loginByGoogle(){
        $user = $this->find('all', array('conditions' => array('idgoogle' => self::$inputData->idgoogle)));
        if (count($user) == 0) return false;
        
        return $this->decodeFields($user[0]); 
    }
    
    /**
     * Accede ai dati dell'utente utilizzando email e password
     * @return boolean -> false se nessuna corrispondenza viene trovata, array('User' => stdClass{... dati utente}) se l'utente esiste 
     */ 
    public function loginByEmail(){
        $user = $this->find('all', array('conditions' => array('email' => self::$inputData->email, 'password' => self::$inputData->password)));
        if (count($user) == 0) return false;
        
        return $this->decodeFields($user[0]); 
    }
    
    /**
     * Accede ai dati dell'utente utilizzando la chiave primaria dell'utente
     * @return boolean -> false se nessuna corrispondenza viene trovata, array('User' => stdClass{... dati utente}) se l'utente esiste 
     */ 
    public function getUserById($decode = true){
        $user = $this->find('all', array('conditions' => array('iduser' => self::$inputData->iduser)));
        if (count($user) == 0) return false;
        
        return $decode ? $this->decodeFields($user[0]) : $user[0]['User']; 
    }
    
    /**
     * Aggiorna i dati associati ad un utente
     * @param string $image path + nome immagine | false se l'immagine non è stata modificata
     */
    public function updateUser($image){
        
        $query = "UPDATE `users` SET `job` = ".HTMLDecoder::encode(self::$inputData->job).",
                                     `city` = '".HTMLDecoder::encode(self::$inputData->city)."',
                                     `interests` = '".HTMLDecoder::encode(self::$inputData->interests)."'
                                      ".((self::$inputData->password != "") ? ", `password` = '".self::$inputData->password."'" : '')
                                       .(($image !== false) ? ", `img` =  '".Utils::URL_BASE.Utils::USER_IMAGE_PATH.$image."'" : '')." 
                 WHERE `iduser` = ".self::$inputData->iduser;
        
        $this->query($query);
    }
    
}
