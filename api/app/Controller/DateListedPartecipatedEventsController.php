<?php

/**
 * 
 *
 * @author Christian Ruggiero
 * @creation 17/11/2014
 * @status in develop
 */
App::import('Vendor', 'Events/IEventDateListedController');
class DateListedPartecipatedEventsController extends AppController implements IEventDateListedController{
    private $LIST_NAME = 'DateListedPartecipatedEvent';
    
    public function getList( $basedate, $iduser ){
        return $this->DateListedPartecipatedEvent->getList( $basedate, $iduser );
    }
    
    public function getListName(  ){
        return $this->LIST_NAME;
    }
}