<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AvCountCreatedEvent
 * SQL: 
 * 
 * drop view if exists av_count_created_events;
 * create view av_count_created_events as select `zadmin_join-me`.`events`.`date` AS `date`,`zadmin_join-me`.`events`.`iduser` AS `iduser`,count(`zadmin_join-me`.`events`.`idevent`) AS `occurance` from `zadmin_join-me`.`events` where (`zadmin_join-me`.`events`.`activated` = 1) group by `zadmin_join-me`.`events`.`iduser`,`zadmin_join-me`.`events`.`date` order by `zadmin_join-me`.`events`.`date`
 * 
 * @author pietro@studioleaves.com
 */
class AvCountCreatedEvent extends AppModel{
    
    /**
     * Seleziona il limite superiore dell'intervallo di date a cui devono appartenere gli eventi da selezionare nel caso di selezione degli eventi creati dall'utente
     * @return false se il limite superiore corrisponde a quello inferiore; string (aaa-mm-gg) data che indica il limite superiore dell'intervallo da considerare 
     */
    public function getLastDate(){
        $dates = $this->find( 'all', array( 'conditions' => array( 'date >' => self::$inputData->lastdate, 
                                                                   'iduser' => self::$inputData->iduser)));
        $items = count($dates);
        if ($items == 0) return false;
        
        $events = $i = 0;
        $maxevents = self::$inputData->quantity; 
        
        do {
            $lastdate = $dates[$i]['AvCountCreatedEvent']['date'];
            $events += $dates[$i]['AvCountCreatedEvent']['occurance'];
            $i++;
        } 
        while (($i < $items) and ($events < $maxevents));
        
        return $lastdate;
    }
}

?>
