<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of StatusCell
 * Evento Attivo o non Atiivo
 * @author pietro@studioleaves.com
 */
class StatusCell implements ICell{

    private static $titles = array('Non Attivo', 'Attivo');
    private static $icons = array('ic_power_red.png', 'ic_power_green.png');
    
    private $tuple;
    private $status_label_field;
    
    /**
     * Inizializza le variabili di istanza
     * @param string $status_label_field campo della tupla contenente lo stato dell'evento
     */
    function __construct($status_label_field) {
        $this->status_label_field = $status_label_field;
    }

    
    /**
     * Stampa il contenuto html
     * @return string
     */
    public function html() {
        $html = "";
        
        $status = $this->tuple[$this->status_label_field];
            
        $html .= '<td align="center">';
            //$html .= '<a href="'.Juri::base().'index.php'.self::$menu.$this->tuple[$this->id_field].'" title="'.self::$title.$label.'" target="_blank">';
            $html .= '<img src="'.JUri::base().'vendor/view/events/cell/icons/'.self::$icons[$status].'" title="'.self::$titles[$status].'"/>';
            //$html .= '</a>';
        $html .= '</td>';
            
        return $html;
    }

    /**
     * Setter per la tupla
     * @param array $tuple
     */
    public function data(&$tuple) {
        $this->tuple = $tuple;
    }    
}

?>
