<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UsersController
 * Strumenti per la gestione della lista degli utenti
 * @author pietro@studioleaves.com
 */
class UsersController {
    
    public static $searchparam = false;
    
    // costanti per il percorso dell'avatar dell'utente
    const API_URL_BASE = "http://www.join-me.it/web-service/dev/app/webroot/";
    const USER_IMAGE_PATH = "images/user/";
    const USER_DEFAULT_IMAGE = "avatardefault.png";
    
    /**
     * Vista del controller: 
     * list -> lista eventi
     * search -> Visualizza il form per la ricerca avanzata
     * edit -> dettaglio di un evento
     * created -> lista degli eventi creati dall'utente
     * partecipates -> lista degli eventi a cui l'utente ha partecipato
     * @var string
     */
    private $show;
    
    /**
     * Eventuali operazioni da effettuare prima della visualizzazione : 
     * @var string
     */
    private $action;
    
    /**
     * Inizializza $view e $action utilizzando i dati in get
     * Effettua il caricamento degli script necessari per l'esecuzione delle richieste
     */
    public function __construct(){
        $this->show = JRequest::getVar('show', 'list');
        $this->action = JRequest::getVar('action', 'noaction');
        
        JLoader::register('UsersModel', 'vendor/model/UsersModel.php');
        JLoader::register('UsersView', 'vendor/view/users/UsersView.php');
    }
    
    public function getShow() {
        return $this->show;
    }

    public function getAction() {
        return $this->action;
    }

    /**
     * Inizializza i campi del form
     * @param $form riferimento alla variabi9le form di chronoforms
     */
    public function initForm(&$form){
        
        $userDetail = Travel::getInstance()->get('userDetail');
        
        $form->data['status'] = $userDetail->activated;
        $form->data['name'] = HTMLDecoder::decode($userDetail->name);
        $form->data['surname'] = HTMLDecoder::decode($userDetail->surname);
        $form->data['email'] = HTMLDecoder::decode(str_replace('@', '&#64;', $userDetail->email));
        // dopo il rendering di chronoform, tramite js viene fatta la trasformata opposta sulla stringa dell'email per evitare che chronoform inserisca lo script antispam al posto dell'undirizzo email
        $form->data['birthdate'] = HTMLDecoder::italianDateTime($userDetail->birthdate, true);
        $form->data['sex'] = $userDetail->sex;
        $form->data['city'] = HTMLDecoder::decode($userDetail->city);
        $form->data['interests'] = HTMLDecoder::decode($userDetail->interests);
        $form->data['job'] = $userDetail->job;
    }
    
    /**
     * Controlla se ci sono delle operazioni da effettuare sulla lista di utenti e se ci sono le esegue
     */
    public function performListActions(){

        // utenti da abilitare
        if (isset($_POST['users_to_active'])){
            $this->activeUsers();
            return;
        }
        
        // utenti da disabilitare
        if (isset($_POST['users_to_disable'])) 
            $this->disableUsers();
    }
    
    /**
     * Rimozione ban utenti
     */
    private function activeUsers(){
        $users = str_replace(array('[', ']', '"'), array('(', ')', "'"), $_POST['users_to_active']);
        $model = new UsersModel();
        $model->activeUsers($users);
    }
    
    /**
     * Ban di utenti
     */
    private function disableUsers(){
        $users = str_replace(array('[', ']', '"'), array('(', ')', "'"), $_POST['users_to_disable']);
        $model = new UsersModel();
        $model->disableUsers($users);
    }
   
    
    /**
     * Imposta l'avatar di default e rimuove l'avatar attuale
     * @param int $iduser
     * @param string $oldimage
     */
    public function setDefaultAvatar($iduser, $oldimage){
        $model = new UsersModel();
        $model->updateImage($iduser, self::API_URL_BASE.self::USER_IMAGE_PATH.self::USER_DEFAULT_IMAGE);
        
        $iex = explode('/', $oldimage);
        $img = $iex[count($iex) - 1];
        
        if ($img != self::USER_DEFAULT_IMAGE){
            JLoader::register('ImageManager', 'vendor/tools/ImageManager.php');
            $imageManager = new ImageManager();
            $imageManager->target = USERS;
            $imageManager->delete($img);
        }
     }
     
     /**
      * Eliminazione delle impostazioni sui filtri di ricerca dalle variabili di sessione
      */
     public function clearSearchParam(){
        
        self::$searchparam = false; 
         
        if (!isset($_SESSION['gcore'])) return;
        if (!isset($_SESSION['gcore']['_chronoform_data_user_search_params'])) return;
        
        foreach ($_SESSION['gcore']['_chronoform_data_user_search_params'] as $key => $val){
            if (!in_array($key, array('show', 'chronoform', 'event', 'button17', 'user_search'))){
                $_SESSION['gcore']['_chronoform_data_user_search_params'][$key] = '';
            }
        }
    }
    
    /**
     * Controlla se ci sono delle operazioni da effettura sulla lista delle prenotazioni e se ci sono le esegue.
     */
    public function performBookingActions(){
        // conferma delle prenotazioni
        if (isset($_POST['books_to_confirm'])){
            $model = new UsersModel();
            $model->confirmBooking($_POST['books_to_confirm']);
            unset($model);
            return;
        }
        
        // rimozione delle conferme
        if (isset($_POST['confirmation_to_remove'])){
            $model = new UsersModel();
            $model->unconfirmBooking($_POST['confirmation_to_remove']);
            unset($model);
            return;
        }
        
        // eliminazione delle prenotazioni
        if (isset($_POST['delete_booking'])){
            $model = new UsersModel();
            $model->deleteBooking($_POST['delete_booking']);
            unset($model);
            return;
        }
    }
}

?>
