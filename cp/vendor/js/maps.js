/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

function initialize_mappa() {
        
    var myLatlng = new google.maps.LatLng(event_latitude, event_longitude);
    
    var mapOptions = {center: myLatlng, 
                      zoom: 13, 
                      scrollwheel: false
          };
          
    var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
    
    var marker = new google.maps.Marker({position: myLatlng, 
                                         map: map, 
                                         title: event_name,
                                         draggable:true
                                         //animation: google.maps.Animation.DROP
                                     });
    
    google.maps.event.addListener(marker, 'dragend', function(event){
        
        document.getElementById('address').value = '';
        document.getElementById('latitude').value = event.latLng.lat();
        document.getElementById('longitude').value = event.latLng.lng();
    });    
}
