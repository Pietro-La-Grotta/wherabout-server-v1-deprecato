<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of EventSet
 *
 * @author Christian
 */

App::import('Vendor', 'Events/EventDay');
class EventList {
    public $eventsdayslist = array(); //array of /Vendor/EventList
    
    public function addEvent( $date, $event, $partecipateArray ) {
        
        $find = false;
        
        /// il codice che segue è veramente brutto
        
        
        foreach ( $this->eventsdayslist as $eventListObj ) {
            
            if( isset( $partecipateArray[ $event["idevent"] ] ) ) {
                $event["partecipate"] = $partecipateArray[ $event["idevent"] ];
            } else {
                $event["partecipate"] = 999;
            }
            
            if ($eventListObj->date == $date) {
            
                $eventListObj->eventlistinfo[] = $this->generateEvent( $event );
                $find = true;
            }   
        }
        
        if( !$find ){
            if( isset( $partecipateArray[ $event["idevent"] ] ) ) {
                $event["partecipate"] = $partecipateArray[ $event["idevent"] ];
            } else {
                $event["partecipate"] = 999;
            }
            $eventDay = new EventDay();
            $eventDay->date = $date;
            $eventDay->eventlistinfo[] = $this->generateEvent( $event);
            $this->eventsdayslist[] = $eventDay;
        }
    }
    
    private function generateEvent( $event ) {
        $eventObj = new stdClass();
        $eventObj->idevent      = $event["idevent"];
        $eventObj->iduser       = $event["iduser"];
        $eventObj->sex          = $event["sex"];
        $eventObj->sittotal     = $event["sittotal"];
        $eventObj->sitcurrent   = $event["sitcurrent"];
        $eventObj->idcategory   = $event["idcategory"];  
        $eventObj->title        = HTMLDecoder::decode($event["title"]);
        $eventObj->city         = HTMLDecoder::decode($event["city"]);
        $eventObj->address      = HTMLDecoder::decode($event["address"]);
        $eventObj->agemin       = $event["agemin"];
        $eventObj->agemax       = $event["agemax"];
        $eventObj->date         = $event["date"];
        $eventObj->hour         = $event["hour"];
        $eventObj->scope        = $event["scope"];
        $eventObj->userimg      = $event["userimg"];
        $eventObj->partecipate  = $event["partecipate"];
        $eventObj->latitude     = $event["latitude"];
        $eventObj->longitude    = $event["longitude"];
        if (isset($event["img"])) $eventObj->imgcover    = $event["img"];
        return $eventObj;
    }
}
