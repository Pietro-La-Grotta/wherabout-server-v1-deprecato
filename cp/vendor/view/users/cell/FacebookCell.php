<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of FacebookCell
 * Se l'utente ha fatto la registrazione con facebook mostra icona facebook colorata altrimenti quella in scala di grigi
 * @author pietro@studioleaves.com
 */
class FacebookCell implements ICell{

    private static $icons = array('fb-active.png', 'fb-unactive.png');
    private static $titles = array('Registrazione effettuata tramite Facebook', '');
    
    private $tuple;
    private $facebook_label_field;
    
    function __construct($facebook_label_field) {
        $this->facebook_label_field = $facebook_label_field;
    }

    /**
     * Stampa il contenuto html
     * @return string
     */
    public function html() {
        $html = "";
        
        $fb = $this->tuple[$this->facebook_label_field];
        
        $html .= '<td align="center">';
            //$html .= '<a href="'.Juri::base().self::$href_params.$this->tuple[$this->idevent_label_field].'" title="'.self::$title.'" target="_blank" title="'.self::$title.'">';
            $html .= '<img src="'.JUri::base().'vendor/view/users/cell/icons/'.self::$icons[$fb >= 0 ? 0 : 1].'" title="'.self::$titles[$fb >= 0 ? 0 : 1].'"/>';
            //$html .= '</a>';
        $html .= '</td>';
            
        return $html;
    }

    /**
     * Setter per la tupla
     * @param array $tuple
     */
    public function data(&$tuple) {
        $this->tuple = $tuple;
    }    
}

?>
