<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TipoCell
 * Tipologia di evento: public, private, friends only
 * @author pietro@studioleaves.com
 */
class TipoCell implements ICell{

    private static $titles = array('Evento Pubblico', 'Evento Privato', 'Evento Friends Only');
    private static $icons = array('public_event.png', 'ic_private_events.png', 'ic_friends_event.png');
    
    private $tuple;
    private $tipo_label_field;
    
    /**
     * Inizializza le variabili di istanza
     * @param string $tipo_label_field
     */
    function __construct($tipo_label_field) {
        $this->tipo_label_field = $tipo_label_field;
    }

    
    /**
     * Stampa il contenuto html
     * @return string
     */
    public function html() {
        $html = "";
        
        $tipo = $this->tuple[$this->tipo_label_field];
            
        $html .= '<td align="center">';
            //$html .= '<a href="'.Juri::base().'index.php'.self::$menu.$this->tuple[$this->id_field].'" title="'.self::$title.$label.'" target="_blank">';
            $html .= '<img src="'.JUri::base().'vendor/view/events/cell/icons/'.self::$icons[$tipo].'" title="'.self::$titles[$tipo].'"/>';
            //$html .= '</a>';
        $html .= '</td>';
            
        return $html;
    }

    /**
     * Setter per la tupla
     * @param array $tuple
     */
    public function data(&$tuple) {
        $this->tuple = $tuple;
    }    
}

?>
