<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DataTable
 * Builder di una tabella contenente dati
 * @author pietro@studioleaves.com
 */
class DataTable {
    
    /**
     * Dati grezzi estratti dal database
     * @var array 
     */
    private $model;
    
    /**
     * Regole di costruzione della tabella
     * @var array
     */
    private $rules;
    
    /*
     * RULES EXAMPLE
     * 
     * $rules = array(head_col_name_1' => array('type' => 'standard_cell',
     *                                         'model' => 'db_col', 
     *                                         'before' => 'text to insert before each cell content of this column', 
     *                                         'after' => 'text to insert after each cell content of this column',
     *                                         'url' => 'db_col'),
     *                'head_col_name_2' => array('type' => 'delete', ......  ), ..... );
     */
    
    /**
     * Visualizzazione del footer della tabella
     * @var boolean
     */
    
    private $abledFooter = true;
    
    /**
     * Abilita/disabilita la visualizzazione del footer
     * @param boolean $abledFooter
     */
    public function setAbledFooter($abledFooter) {
        $this->abledFooter = $abledFooter;
    }
    
    /**
     * Nome della colonna del database che contiene l'id da dare alla riga della tabella
     * Default = null -> indica che le colonne non devono avere alcun id
     * @var 
     */
    private $idSource = null;
    
    /**
     * Imposta il nome del campo del db che è sorgente dell'id della riga della tabella
     * @param string $idSource
     */
    public function setIdSource($idSource) {
        $this->idSource = $idSource;
    }
    
   /**
    * patth contenente gli script di inizializzazione della tabella
    */ 
   const pathJS = 'vendor/view/datatables/js/';
    
   /**
    * Nome del file contenente la function js di inizilaizzazione del datatable e la chiamata alla stessa
    * Di default prende zero_configuration
    * @var string
    */
    private $initJS = 'default.js';
    
    /**
     * Setter nome file contenente lo script di inizializzazione e la sua chiamata
     * @param string $initscript
     */
    public function setInitJS($initscript) {
        $this->initJS = $initscript;
    }

    /**
     * Inizializzazione delle variabili di istanza
     * @param array $model dati grezzi in output dal database
     * @param array $rules regole di costruzione della tabella
     */
    public function DataTable($model, &$rules){
        $this->model = $model;
        $this->rules = $rules;
    }
    
    /**
     * Builder del codice html della tabella
     */
    public function html(){
       
        $html = "";
        
        $html .= '<div style="margin-top:30px;"><table id="studio_leaves" class="display" cellspacing="0" width="100%;" style="margin-top:35px;">';
        
        // intestazione della tabella
        
            $html .= '<thead>';
                $html .= '<tr>';
                
                    foreach ($this->rules as $col => $x){
                        $html .= '<th'.$this->TH_toolTip($x).'>'.$col.'</th>';
                    }
                    
                $html .= '</tr>';
            $html .= '</thead>';
         
        // footer della tabella    
            
        if ($this->abledFooter){
            
            $html .= '<tfoot>';
                $html .= '<tr>';
                
                    foreach ($this->rules as $col => $x){
                        $html .= '<th'.$this->TH_toolTip($x).'>'.$col.'</th>';
                    }
                    
                $html .= '</tr>';
            $html .= '</tfoot>';
        }
        
        // body della tabella
        
            $html .= '<tbody>';
            
                foreach ($this->model as $stdObj){
                    // casting dell'oggetto di tipo stdClass in array associativo
                    $tuple = (array)$stdObj;
                    $html .= $this->rowBuilder($tuple);
                }
            
            $html .= '</tbody>';
            
        $html .= '</table></div>';
        
        // inizializzazione della tabella
        
        $html .= '<script type="text/javascript" src="'.Juri::base() . self::pathJS.$this->initJS.'"></script>';
        
        return $html;
    }
    
    /**
     * Tooltip sulle celle contenute in header e footer della tabella
     */
    private function TH_toolTip(&$x){
        return (isset($x['thtooltip'])) ? ' title="'.$x['thtooltip'].'"' : '';
    }
    
    /**
     * Esegue la formattazione di un array contentenente la tupla estratta dal db in una riga della tabella
     * basandosi sulle regole contenute in $this->rules
     * @param array $tuple
     * @return string $html 
     */
    private function rowBuilder(&$tuple){
        
        $html = "";
        
            $html .= '<tr '.(isset($this->idSource) ? 'id="'.$tuple[$this->idSource].'"' : '').'>';
            
            foreach ($this->rules as $column_name => &$rules){
                
                if ($rules['type'] == 'standard_cell'){
                    $html .= $this->standard_cell($tuple, $rules);
                }
                elseif ($rules['type'] == 'custom'){
                    $rules['ICell']->data($tuple);
                    $html .= $rules['ICell']->html();
                }
                else{
                    $html .= "no cell";
                }
            }
            
            $html .= '</tr>';
        
        return $html;
    }
    
    /**
     * Builder di una riga standard
     * 
     * Formato dell'array di regole : 
     *      array('type' => 'standard_cell',
     *            'model' => 'db_col', 
     *            'before' => 'text to insert before each cell content of this column', 
     *            'after' => 'text to insert after each cell content of this column',
     *            'url' => 'db_col',
     *            'align' => 'center');
     *  
     * @param string $column_name nome del campo della tupla su cui si sta lavorando 
     * @param array $rules array di regole
     * @return string html
     */
    private function standard_cell(&$tuple, &$rules){
    
        $html = "";
            $html .= '<td'.((isset($rules['align'])) ? ' align="'.$rules['align'].'"' : '').'>';
            
                $target = (!isset($rules['target_no_blank'])) ? ' target="_blank"' : '';
                $html .= isset($rules['url']) 
                       ? '<a href="'.$tuple[$rules['url']].'"'.$target.'>' 
                       : '';
                    $html .= isset ($rules['before']) ? $rules['before'] : '';   
                    $html .= $tuple[$rules['model']];
                    $html .= isset ($rules['after']) ? $rules['after'] : '';   
                $html .= isset($rules['url']) ? '</a>' : '';
            
            $html .= '</td>';
        return $html;
    }
    
    
}

?>
