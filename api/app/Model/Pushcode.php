<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Pushcode
 *
 * @author Christian
 */
App::uses('AppModel', 'Model');
class Pushcode extends AppModel {
    
    public function pushcodeExists( $hwid ) { 
        if ( isset($hwid) && ( intval( $this->find( 'count', array( 'conditions' => array( 'hwid' => $hwid) ) ) > 0 ) ) ) {
            return TRUE;
        }
        else {
            return FALSE;
        }
    }
    
    public function getPushcodeInfo( $hwid ) {
        if( isset($hwid) ) {
            return $this->find( 'first', array( 'conditions' => array( 'hwid' => $hwid) ) );
        }
        return null;
    }

}
