<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of EventsView
 * Gestione elementi grafici della sezione eventi
 * @author pietro@studioleaves.com
 */
class EventsView {
    
    
    /**
     * Mostra il sottotitolo per la lista degli eventi
     */
    public static function listTitle(){
        return '<h3>Lista degli eventi in programma</h3>';
    }
    
    /**
     * Barra degli strumenti per la lista degli eventi
     * @return string
     */
    public static function listInstruments(){
        $html = "";
        $html .= '<div class="moduletable">
                    <div class="custom">
                        <div class="moduletable" align="right">
                            <ul class="breadcrumb">
                                <li class="active">
                                    <form name="active_events_form" method="POST" style="margin:0px;">
                                        <input type="hidden" name="events_to_active" id="events_to_active">  
                                        <span class="btn" title="Abilita eventi selezionati" id="active_button"><i class="icon-ok"></i>   Abilita</span>
                                    </form>  
                                </li>
                                <li class="active">
                                    <form name="disable_events_form" method="POST" style="margin:0px;">
                                        <input type="hidden" name="events_to_disable" id="events_to_disable">  
                                        <span class="btn" title="Disabilita eventi selezionati" id="disable_button"><i class="icon-unpublish"></i>   Disabilita</span>
                                    </form>  
                                </li>
                                <li class="active">
                                        <a class="btn" title="Imposta filtri di ricerca" id="button_search" href="'.JUri::base().'?show=search"><i class="icon-search"></i>   Ricerca Avanzata</a>  
                                </li>';
        
        if (JFactory::getSession()->has('event_search_params')){
            
            if (count(json_decode(JFactory::getSession()->get('event_search_params'), true)) > 0){
            
                $html .=       '<li class="active">
                                    <img title="Ci sono dei filtri attivi" src="vendor/view/events/cell/icons/ic_warning45.png" onclick="alert(\'ATTENZIONE: Alcuni filtri di ricerca avanzata sono attivi\');" style="cursor:pointer;margin-left:10px;"/>
                                </li>';
            }
        }
                                
        $html .=            '</ul>
                        </div>
                    </div>
		</div>';
        return $html;
    }
    
    /**
     * Lista da visualizzare
     * @var lista di items estratti dal database 
     */
    public static $list = array();
    
    /**
     * Table filling e visualizzazione della lista di eventi
     */
    public static function showlist(){
        if (count(self::$list) == 0){
            JFactory::getApplication()->enqueueMessage('Lista eventi vuota', 'warning');
            return '';
        }
        
        // registrazione dei renderer delle cell
        JLoader::register('DateCell', 'vendor/view/events/cell/DateCell.php');
        JLoader::register('LuogoCell', 'vendor/view/events/cell/LuogoCell.php');
        JLoader::register('TitleCell', 'vendor/view/events/cell/TitleCell.php');
        JLoader::register('CategoriaCell', 'vendor/view/events/cell/CategoriaCell.php');
        JLoader::register('PlannerCell', 'vendor/view/events/cell/PlannerCell.php');
        JLoader::register('FullCell', 'vendor/view/events/cell/FullCell.php');
        JLoader::register('TipoCell', 'vendor/view/events/cell/TipoCell.php');
        JLoader::register('StatusCell', 'vendor/view/events/cell/StatusCell.php');
        JLoader::register('EditCell', 'vendor/view/events/cell/EditCell.php');
        
        $rules = array();
        $rules['POS'] = array('type' => 'standard_cell', 'model' => 'idevent');
        $rules['DATA'] = array('type' => 'custom', 'ICell' => new DateCell('date'), 'thtooltip' => 'Data di svolgimento degli Eventi');
        $rules['LUOGO'] = array('type' => 'custom', 'ICell' => new LuogoCell('city'), 'thtooltip' => 'Città di svolgimento degli Eventi');
        $rules['NOME'] = array('type' => 'custom', 'ICell' => new TitleCell('title', 'idevent'), 'thtooltip' => 'Titolo degli Eventi');
        $rules['CATEG'] = array('type' => 'custom', 'ICell' => new CategoriaCell('c_name'), 'thtooltip' => 'Categoria di appartenenza degli Eventi');
        $rules['PLANNER'] = array('type' => 'custom', 'ICell' => new PlannerCell('name', 'surname', 'iduser'), 'thtooltip' => 'Utenti che hanno creato gli Eventi');
        $rules['BOOKED'] = array('type' => 'custom', 'ICell' => new FullCell('sitcurrent', 'sittotal', 'idevent'), 'thtooltip' => 'Numero di Prenotazioni / Numero di Posti Totali');
        $rules['TIPO'] = array('type' => 'custom', 'ICell' => new TipoCell('scope'), 'thtooltip' => 'Visibilità degli Eventi');
        $rules['STATO'] = array('type' => 'custom', 'ICell' => new StatusCell('activated'), 'thtooltip' => 'Stato Attuale degli Eventi');
        //$rules['EDIT'] = array('type' => 'custom', 'ICell' => new EditCell('idevent'));
        
        $table = new DataTable(self::$list, $rules);
        $table->setIdSource('idevent');
        $table->setInitJS('eventi.js');
        
        $style = '<style>
                    table#studio_leaves tr:first-child td:first-child {
                            min-width: 65px;
                    }
                  </style>';
        
        return $style.$table->html();
    }
    
    /**
     * Dettagli principali dell'evento
     * @param object $event
     * @return string html
     */
    public static function mainDetails(&$event){
        JLoader::register('DateFormat', 'vendor/tools/DateFormat.php');
        
        $html = '';
        $html .= '<div class="page-header">';
        
            $html .= '<h3>'.$event->title.'</h3>';
            $html .= '<p><strong>Categoria</strong>: '.$event->c_name.'</p>';
            
            $titles = array('Evento Pubblico', 'Evento Privato', 'Evento Friends Only', 'Non Attivo', 'Attivo');
            $icons = array('public_event.png', 'ic_private_events.png', 'ic_friends_event.png', 'ic_power_red.png', 'ic_power_green.png');
            
            $html .= '<p><img src="'.JUri::base().'vendor/view/events/cell/icons/'.$icons[$event->scope].'"/>&nbsp;&nbsp;&nbsp;<strong>'.$titles[$event->scope].'</strong><p>';
            $html .= '<p><img src="'.JUri::base().'vendor/view/events/cell/icons/'.$icons[$event->activated + 3].'"/>&nbsp;&nbsp;&nbsp;<strong>'.$titles[$event->activated + 3].'</strong><p><br>';
            
            
            $href_params = "index.php/utenti?show=edit&item=";
            $html .= '<p><strong>Evento inserito da </strong>: <a href="'.Juri::base().$href_params.$event->iduser.'" title="Vai al profilo utente"  target="_blank">'.$event->surname.' '.$event->name.'</a></p><br>';
            
            //$html .= '<p><img src="'.Utils::eventsImagePath().$event->img.'"/>'.$event->title.'</p>'; // stavo scherzando
            $html .= '<p><img src="'.$event->img.'"/></p><br>';
            
            $date = new DateFormat($event->date.' 00:00:00');
            $html .= '<p><strong>Evento previsto per </strong>: '.$date->getWDay().'</p>';
            
            $date = new DateFormat($event->creationdate);
            $html .= '<p><strong>Data di inserimento </strong>: '.$date->getWDay().' alle ore '.$date->getWTime().'</p>';
            
            $date = new DateFormat($event->updatetime);
            $html .= '<p><strong>Data ultima modifica </strong>: '.$date->getWDay().' alle ore '.$date->getWTime().'</p>';
            
            $href_params = "?show=partecipants&item=";
            $html .= '<p><strong>Partecipanti all&#39;evento</strong>: <a href="'.Juri::base().$href_params.$event->idevent.'" title="Vai alla lista dei partecipanti" target="_blank">'.$event->sitcurrent.'</a> su '.$event->sittotal.' posti disponibili</p><br>';
            
        $html .= '</div><br>';
            
        return $html;
    }
    
    /**
     * Titolo per editor evento
     * @return string
     */
    public static function updateEventTitle(){
        return '<h3>Editor dell&#39;evento</h3><br><br>';
    }
    
    /**
     * Titolo per impostazione filtri su ricerca
     * @return string
     */
    public static function searchEventTitle(){
        return '<h3>Impostazione dei filtri di ricerca</h3><p>In questa pagina è possibile impostare dei parametri per il filtraggio della lista di eventi. I filtri rimarranno attivi finchè non saranno rimossi.<p>';
    }
    
    /**
     * Tasto per ritornare alla lista degli eventi
     * @return string
     */
    public static function backToEventList(){
        $html = "";
        $html .= '<div class="moduletable" style="margin-bottom:35px;">
                    <div class="custom">
                        <div class="moduletable">
                            <ul class="breadcrumb">
                                <li class="active">
                                   <a title="Torna alla lista eventi" href="'.JUri::base().'?show=list">&lsaquo;&lsaquo;&nbsp;Torna alla lista degli eventi</a>  
                                </li>
                                <li class="active" style="float:right;">
                                   <a  title="Rimuove tutti i filtri di ricerca" href="'.JUri::base().'?show=search&action=reset">Pulisci Filtri</a>  
                                </li>
                            </ul>
                        </div>
                    </div>
		</div>';
        return $html;
    }
    
    /**
     * Lista dei partecipanti ad un evento
     */
    public static function showPartecipants(){
        if (count(self::$list) == 0){
            JFactory::getApplication()->enqueueMessage('Lista partecipanti vuota', 'warning');
            return '';
        }
        
        // registrazione dei renderer delle cell
        
        JLoader::register('PartecipantCell', 'vendor/view/events/cell/PartecipantCell.php');
        JLoader::register('ConfirmedCell', 'vendor/view/events/cell/ConfirmedCell.php');
        JLoader::register('EmailCell', 'vendor/view/events/cell/EmailCell.php');
        
        $rules = array();
        $rules['POS'] = array('type' => 'standard_cell', 'model' => 'id');
        $rules['PARTECIPANTE'] = array('type' => 'custom', 'ICell' => new PartecipantCell('name', 'surname', 'iduser'), 'thtooltip' => 'Nome e Cognome degli Utenti che hanno inviato le Prenotazioni');
        $rules['EMAIL'] = array('type' => 'custom', 'ICell' => new EmailCell('email'), 'thtooltip' => 'Indirizzo Email con cui gli Utenti si sono registrati');
        $rules['CONFERMATO'] = array('type' => 'custom', 'ICell' => new ConfirmedCell('confirmed'), 'thtooltip' => 'Stato attuale delle Prenotazioni');
        
        $table = new DataTable(self::$list, $rules);
        $table->setIdSource('id');
        $table->setInitJS('partecipants.js');
        
        $style = '<style></style>';
        
        return $style.$table->html();
    }
    
    /**
     * Titolo per editor evento
     * @return string
     */
    public static function partecipateTitle($idEvent, $eventTitle){
        return '<h3 style="line-height: 1.5em">Lista degli utenti che hanno inoltrato una prenotazione all&#39;evento:&nbsp;
                    <a style="color:darkBlue;" target="_blank" href="'.Juri::base().'?show=edit&item='.$idEvent.'">
                        '.$eventTitle.'
                    </a>
                </h3>
                <br>';
    }
    
    /**
     * Barra degli strumenti per la lista degli eventi
     * @return string
     */
    public static function partecipantsInstruments(){
        $html = "";
        $html .= '<div class="moduletable">
                    <div class="custom">
                        <div class="moduletable" align="right">
                            <ul class="breadcrumb">
                                <li class="active">
                                    <form name="confirm_booking_form" method="POST" style="margin:0px;">
                                        <input type="hidden" name="books_to_confirm" id="books_to_confirm">  
                                        <span class="btn" title="Conferma le prenotazioni selezionate" id="confirm_button"><i class="icon-ok"></i>   Conferma</span>
                                    </form>  
                                </li>
                                <li class="active">
                                    <form name="remove_confirm_post" method="POST" style="margin:0px;">
                                        <input type="hidden" name="confirmation_to_remove" id="confirmation_to_remove">  
                                        <span class="btn" title="Rimuovi conferma alle prenotazioni selezionate" id="remove_button"><i class="icon-unpublish"></i>   Rimuovi Conferma</span>
                                    </form>  
                                </li>
                           </ul>
                        </div>
                    </div>
		</div>';
        return $html;
    }
}

?>
