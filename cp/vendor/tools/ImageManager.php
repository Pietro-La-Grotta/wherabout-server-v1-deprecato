<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
define("USERS", 0);
define("EVENTS", 1);

/**
 * Description of ImageManager
 *
 * @author pietro@studioleaves.com
 */
class ImageManager {
    
    /**
     * Grandezza massima del file in KB
     */
    const MAX_SIZE = 512; 
    const MAX_WIDTH = 720;
    const MAX_HEIGHT = 426;
    
    /**
     * Destinazione dell'immagine
     * @var int 
     */
    public $target;
    
    /**
     * Chiave di ricerca in $_FILE
     * @var type 
     */
    public $input_form_name;
    
    
    private $upload_error = 0;
    
    /**
     * Nome del file caricato senza path
     * @var string
     */
    private $uploadedFileName;
    
//    /**
//     * Duplica una data immagine dato il nome e settato precedentemente il target di riferimento 
//     * @param string $baseName nome dell'immagine da clonare
//     * @return string nome del clone dell'immagine
//     */
//    public function duplicate($baseName){
//        
//        $newName = date("YmdHis").  rand(0, 1000).'.'.$this->fileExtension($baseName);
//        
//        copy('../../'.  CONFIGURATION::$web_service_folder.'/'.$this->folder_name().'/'.$baseName,
//             '../../'.  CONFIGURATION::$web_service_folder.'/'.$this->folder_name().'/'.$newName);
//        
//        return $newName;
//    }
    
    /**
     * Effettua controlli su una immagine in upload e la sposta nella cartella di destinazione
     * @return boolean
     */
    public function save()
    {
        $name = $_FILES[$this->input_form_name]['name'];
        $type = $_FILES[$this->input_form_name]['type'];
        $tname = $_FILES[$this->input_form_name]['tmp_name'];
        $error = $_FILES[$this->input_form_name]['error'];
        $size = $_FILES[$this->input_form_name]['size'];

        // errore generico
        if ($error != 0){
            $this->upload_error = 10;
            return false;
        }
        
        // file troppo grande
        if ($size > (self::MAX_SIZE * 1024)){
            $this->upload_error = 11;
            return false; 
        }
        
        // estensione non valida
        if (!$this->imgTypeCheck($type)){
            $this->upload_error = 12;
            return false;
        }
        
         // controllo grandezza
        list($width, $height, $type, $attr) = getimagesize($tname);
        if ($width > self::MAX_WIDTH || $height > self::MAX_HEIGHT){
            $this->upload_error = 13;
            return false;
        }
        
        $newname = date("YmdHis").  rand(0, 1000).'.'.$this->fileExtension($name);
        $completename = '../../'.$this->folder_name().$newname;
        //die($completename);
        move_uploaded_file($tname, $completename);   
        $this->uploadedFileName = $newname; 
        
        return true;
    }
    
    /**
     * Seleziona il nome della cartella di destinazione in base al terget
     * @return type
     */
    private function folder_name(){
        
        if ($this->target == EVENTS){
            return Utils::EVENTS_IMAGE_PATH;
        }
        
        if ($this->target == USERS){
            return Utils::USERS_IMAGE_PATH;
        }
    }
    
    /**
     * Controlla se un tipo di file inviato è un'immagine tra uno dei formati consentiti
     * @param str $imgType
     * @return boolean
     */
    private function imgTypeCheck($imgType)
    {
        $av = array('jpeg', 'jpg', 'gif', 'png');
        $imgType = strtolower($imgType);
        
        foreach ($av as $a)
            if (strstr($imgType, $a))
                return true;
            
        return false;
    }
    
    /**
     * Seleziona l'estensione di un file
     * @param type $name
     * @return type
     */
    public static function fileExtension($name)
    {
        $x = explode('.', $name);
        return $x[count($x) - 1];
    }
    
    /**
     * Seleziona il messaggio d'errore
     */
    public function messageError(){
        switch($this->upload_error){
            case 10: return "Errore generico sul server";
            case 11: return "File troppo grande";
            case 12: return "Estensione file non valida";
            case 13: return "Altezza o larghezza immagine di copertina non valide";    
            default : return "No problem";
        }
    }
    
    /**
     * Seleziona il nome dell'immagine appena uplodata senza path
     * @return type
     */
    public function getUploadedFileName() {
        return $this->uploadedFileName;
    }
    
    /**
     * Eliminazione di una immagine
     * @param $imageName
     */
    public function delete($imageName){
        unlink('../../'.Utils::$this->folder_name().$imageName);
    }
    
    public function getErrorCode(){
        return $this->upload_error;
    }
   
//    
//    /**
//     * Lista delle immagini contenute in una cartella
//     * @return \stdClass {"filename" => nome del file con estensione
//     *                    "url" => url completo del file
//     *                    "size" => dimensione del file in byte
//     *                    "width" => larghezza dell'immagine in pixel
//     *                    "height" => altezza dell'immagine in pixel
//     *                   }
//     */
//    public function lista(){
//        $lista = array();
//        $dir =  '../../'.  CONFIGURATION::$web_service_folder.'/'.$this->folder_name();
//        $path = CONFIGURATION::$dns.'/'.  CONFIGURATION::$web_service_folder.'/'.$this->folder_name();
//        $files = scandir($dir);
//        
//        foreach($files as $f){
//            if (($f != '.') and ($f != '..') and ($f != CONFIGURATION::$media_default_image)){
//                
//                $obj = new stdClass();
//                
//                $obj->filename = $f;
//                $obj->url = $path.'/'.$f;
//                
//                $obj->size = filesize($dir.'/'.$f);
//                
//                $imagesize = getimagesize($dir.'/'.$f); 
//                $obj->width = $imagesize[0];
//                $obj->height = $imagesize[1];
//                
//                $lista[] = $obj;
//            } 
//        }
//        
//        return $lista;
//    }
}

?>
