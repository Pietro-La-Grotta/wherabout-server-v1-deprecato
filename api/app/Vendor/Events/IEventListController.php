<?php

interface IEventListController {
    public function getListPaged( $lastdate, $limitdate, $iduser );
    public function getListByMonth( $year, $month, $iduser );
    public function getListName(  );
}