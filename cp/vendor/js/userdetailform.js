/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Aggiunge gli asterischi (campo obbligatorio) dove non è stato possibile autogenerarlo tramite chronoforms
 */
function asterix(){
    var labels = ['status'];
    var text = '&nbsp;<i class="fa fa-asterisk" style="color:#ff0000; font-size:9px; vertical-align:top;"></i>';
    
    jQuery.each(labels, function( index, value ) {
        jQuery("label[for='" + value + "']").append(text);
    });
    
}