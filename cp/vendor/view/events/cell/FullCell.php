<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of FullCell
 * Partecipanti/Posti Totali con collegamento alla lista dei partecipanti
 * @author pietro@studioleaves.com
 */
class FullCell implements ICell{

    private static $href_title = "Lista dei partecipanti";
    private static $href_params = "?show=partecipants&item=";
    
    private $tuple;
    private $sitcurrent_label_field;
    private $sittotal_label_field;
    private $idevent_label_fields;
   
    /**
     * Inizializza le variabili di instanza
     * @param string $sitcurrent_label_field nome del campo della tupla contenente il numero di partecipanti all'evento
     * @param string $sittotal_label_field nome del campo della tupla contenente il numero di posti totali previsti per l'evento
     * @param string $idevent_label_fields nome del campo della tupla contenente la pk dell'evento
     */
    function __construct($sitcurrent_label_field, $sittotal_label_field, $idevent_label_fields) {
        $this->sitcurrent_label_field = $sitcurrent_label_field;
        $this->sittotal_label_field = $sittotal_label_field;
        $this->idevent_label_fields = $idevent_label_fields;
    }

    
    /**
     * Stampa il contenuto html
     * @return string
     */
    public function html() {
        $html = "";
            
            $label = $this->tuple[$this->sitcurrent_label_field] .'/'. $this->tuple[$this->sittotal_label_field];
            
            $html .= '<td align="center">';
                $html .= '<a href="'.Juri::base().self::$href_params.$this->tuple[$this->idevent_label_fields].'" title="'.self::$href_title.'" target="_blank">';
                $html .= $label;
                $html .= '</a>';
            $html .= '</td>';
            
        return $html;
    }

    /**
     * Setter per la tupla
     * @param array $tuple
     */
    public function data(&$tuple) {
        $this->tuple = $tuple;
    }    
}

?>
